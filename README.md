# Subscription Admin
---
# Version 8.4.0
---
[![Generic badge](http://54.235.89.214:9000/sonar/api/project_badges/measure?project=subscriptionadmin&metric=alert_status)](http://54.235.89.214:9000/sonar/dashboard/index/subscriptionadmin)
[![Generic badge](http://54.235.89.214:9000/sonar/api/project_badges/measure?project=subscriptionadmin&metric=vulnerabilities)](http://54.235.89.214:9000/sonar/project/issues?id=subscriptionadmin&resolved=false&types=VULNERABILITY)
[![Generic badge](http://54.235.89.214:9000/sonar/api/project_badges/measure?project=subscriptionadmin&metric=bugs)](http://54.235.89.214:9000/sonar/project/issues?id=subscriptionadmin&resolved=false&types=BUG)
[![Generic badge](http://54.235.89.214:9000/sonar/api/project_badges/measure?project=subscriptionadmin&metric=code_smells)](http://54.235.89.214:9000/sonar/project/issues?id=subscriptionadmin&resolved=false&types=CODE_SMELL)
[![Generic badge](http://54.235.89.214:9000/sonar/api/project_badges/measure?project=subscriptionadmin&metric=coverage)](http://54.235.89.214:9000/sonar/project/issues?id=subscriptionadmin&resolved=false&types=CODE_SMELL)
[![Generic badge](http://54.235.89.214:9000/sonar/api/project_badges/measure?project=subscriptionadmin&metric=security_rating)](http://54.235.89.214:9000/sonar/component_measures?id=subscriptionadmin&metric=security_rating)
---