/**********************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 *
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *
 * For further information contact Skava at info@skava.com.
 *********************************************************************************/

package com.skava.subscriptionadmin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.skava.common.NavigationBuilder;
import com.skava.common.Utilities;
import com.skava.common.layout.model.MenuItem;
import com.skava.common.model.AdminRequestParams;
import com.skava.subscriptionadmin.model.SubscriptionRequestParams;

/**
 * <h1>SubscriptionAdminUtils</h1>
 * <p>SubscriptionAdminUtils has all the util functions that are required in the admin</p>
 * 
 * @author Anbu
 *
 */
public class SubscriptionAdminUtils {

  /**
   * Default constructor
   */
  SubscriptionAdminUtils() {
    // Default constructor
  }

  /**
   * <h1>urlReplaceMacros</h1>
   * <p> Build urls based on the AdminRequestParams </p>
   * 
   * @param subsAdminRequestParams Hold the request model informations
   * @param macroUrl Holds the macro url
   * @return String with macros replaced in url
   */
  public static String urlReplaceMacros(SubscriptionRequestParams subsAdminRequestParams, String macroUrl) {
    if (macroUrl != null) {

      Map<String, Object> uriVariables;
      if (subsAdminRequestParams == null) {
        uriVariables = new HashMap<>();
      } else {
        uriVariables = Utilities.convertPOJOToMap(subsAdminRequestParams);
      }

      if (uriVariables == null) {
        uriVariables = new HashMap<>();
      }

      return Utilities.replaceURLMacros(macroUrl, uriVariables);
    }
    return "";
  }

  /**
   * <h1>getSideNav</h1>
   * 
   * @param type Hold the active type
   * @param requestParams Holds the request param informations
   * @return returns navigation list items
   */
  public static List<MenuItem> getSubscriptionDetailsSideNav(String type, AdminRequestParams requestParams) {
    List<MenuItem> navMenu = new ArrayList<>();
    SubscriptionRequestParams reqParams = (SubscriptionRequestParams) requestParams;

    navMenu.add(
      new MenuItem(SubscriptionAdminConstants.SIDENAV_LABEL_HOME, SubscriptionAdminConstants.URL_GET_ALL_SUBSCRIPTIONS,
        getSideNavSelection(SubscriptionAdminConstants.SIDENAV_LABEL_HOME, type),
        SubscriptionAdminConstants.SIDENAV_ICON_HOME));
    navMenu.add(new MenuItem(SubscriptionAdminConstants.SIDENAV_LABEL_OVERVIEW,
      SubscriptionAdminConstants.URL_GET_SUBSCRIPTION_DETAILS + SubscriptionAdminConstants.TAB_SUBSCRIPTION_OVERVIEW,
      "active", null));
    /*navMenu.add(new MenuItem(SubscriptionAdminConstants.SIDENAV_LABEL_SKUS,
      SubscriptionAdminConstants.URL_GET_SUBSCRIPTION_DETAILS + SubscriptionAdminConstants.TAB_SUBSCRIPTION_SKUS,
      getSideNavSelection(SubscriptionAdminConstants.SIDENAV_LABEL_SKUS, type), null));*/
    navMenu.add(new MenuItem(SubscriptionAdminConstants.SIDENAV_LABEL_ORDERS,
      SubscriptionAdminConstants.URL_GET_SUBSCRIPTION_DETAILS + SubscriptionAdminConstants.TAB_SUBSCRIPTION_ORDERS,
      getSideNavSelection(SubscriptionAdminConstants.SIDENAV_LABEL_ORDERS, type), null));

    return new NavigationBuilder().addAllMenuItems(navMenu).build(reqParams);
  }

  /**
   * <h1>getSideNavSelection</h1>
   * 
   * @param type Hold the active type
   * @param curType Holds the current page type
   * @return return the active classes
   */
  private static String getSideNavSelection(String type, String curType) {
    String activeClasss = null;
    if (type.equals(curType)) {
      activeClasss = "active";
    }
    return activeClasss;
  }
}
