/**********************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 *  
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *   
 * For further information contact Skava at info@skava.com.
 *********************************************************************************/
package com.skava.subscriptionadmin.web;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.skava.common.view.AdminView;
import com.skava.subscriptionadmin.model.SubscriptionRequestParams;

import lombok.NoArgsConstructor;

/**
 * <h1>ListController</h1>
 * <p>This is controller which is used to map subscription list page</p>
 * @author Rajesh.S248
 *
 */
@RestController
@NoArgsConstructor
public class DetailsPageController {

  @Autowired
  @Qualifier("DetailsPageView")
  private AdminView adminView;

  /**
   * <h1>notificationLanding</h1>
   * <p>This method is used to get the subscription list view.</p>
   * 
   * @param request HTTP Request Object.
   * @param response HTTP Response Object.
   * @return {@link ModelAndView} It refers modelview Object
   * @throws IOException It refers IOException
   */
  @PreAuthorize("isSuperAdmin() or isBusinessAdmin(#requestParams.businessId)")
  @GetMapping(value = "/", params = {"subscriptionId", "userId"})
  public ModelAndView subscriptionDetails(
    HttpServletRequest request,
    HttpServletResponse response,
    @ModelAttribute SubscriptionRequestParams requestParams) throws IOException {
    return adminView.getPage(request, response, requestParams);
  }
}
