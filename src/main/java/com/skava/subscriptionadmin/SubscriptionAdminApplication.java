/**********************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 *
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *
 * For further information contact Skava at info@skava.com.
 *********************************************************************************/
package com.skava.subscriptionadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <h1>SubscriptionAdminApplication</h1>
 * <p>This is main application in the Subscription admin.</p>
 * 
 * @author rajesh
 */
@SpringBootApplication(scanBasePackages = "com.skava")
public class SubscriptionAdminApplication {
  public static void main(String[] args) {
    SpringApplication.run(SubscriptionAdminApplication.class, args);
  }
}
