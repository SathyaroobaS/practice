/**********************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 *
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *
 * For further information contact Skava at info@skava.com.
 *********************************************************************************/
package com.skava.subscriptionadmin;

import com.skava.common.Constants;

import lombok.NoArgsConstructor;

/**
 * <h1>SubscriptionAdminConstants</h1>
 * <p>This class contains all Subscription constants values.</p>
 * 
 * @author rajesh
 * 
 */
@NoArgsConstructor
public class SubscriptionAdminConstants extends Constants {
  public static final String SERVICE_NAME_ORCHESTRTION = "orchestration";

  // Page params
  private static String paramCollectionId = "collectionId={collectionId}";
  private static String paramUserInfo = "userInfo={userId}";
  private static String paramStoreId = "storeId={storeId}";
  private static String paramBusinessId = "businessId={businessId}";
  private static String paramSubscriptionId = "subscriptionId={subscriptionId}";
  private static String paramUserId = "userId={userId}";

  // API Urls
  public static final String APIURL_GET_SUBSCRIPTION_DETAILS = "/subscriptions/{subscriptionId}?" + paramCollectionId
    + "&" + paramUserInfo + "&" + paramStoreId + "&" + paramBusinessId;
  public static final String APIURL_GET_USER_ADDRESSES = "/subscriptions/{userId}/addresses?" + paramStoreId + "&"
    + paramBusinessId;
  public static final String APIURL_GET_SHIPPING_METHOD = "/subscriptions/calculateshipping?" + paramStoreId + "&"
    + paramBusinessId + "&validationType=ESTIMATE";

  // Page path
  public static final String URL_GET_ALL_SUBSCRIPTIONS = "?" + paramBusinessId + "&" + paramCollectionId + "&"
    + paramStoreId;
  public static final String URL_GET_SUBSCRIPTION_DETAILS = "?" + paramBusinessId + "&" + paramCollectionId + "&"
    + paramStoreId + "&" + paramSubscriptionId + "&" + paramUserId;

  // Side navigation icons
  public static final String SIDENAV_ICON_HOME = "home";

  // Side navigation labels
  public static final String SIDENAV_LABEL_HOME = "SIDENAV.HOME";
  public static final String SIDENAV_LABEL_OVERVIEW = "SIDENAV.OVERVIEW";
  public static final String SIDENAV_LABEL_SKUS = "SIDENAV.SKUS";
  public static final String SIDENAV_LABEL_ORDERS = "SIDENAV.ORDERS";

  /* Page Tab names */
  public static final String TAB_SUBSCRIPTION_OVERVIEW = "#overview";
  public static final String TAB_SUBSCRIPTION_SKUS = "#skus";
  public static final String TAB_SUBSCRIPTION_ORDERS = "#orders";

}
