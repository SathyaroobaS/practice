/*******************************************************************************
 * Copyright ©2002-2019 Skava - All rights reserved.
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
/**
 * 
 */
package com.skava.subscriptionadmin.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * The Class ShippingResponse
 * 
 * @author Skava Team
 */
@Accessors(chain = true)
@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "ShippingResponse", description = "Shipping information of the subscription")
public class ShippingResponse implements Serializable {

  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = -4403150762972360112L;

  /**
   * Field to hold the identifier of shipping
   */
  @ApiModelProperty(value = "${Subscription.shippingId.ApiModelProperty.value}", required = false,
    example = "1001")
  private long id;

  /**
   * Field to hold the type of shipping
   */
  @ApiModelProperty(value = "${Subscription.shippingType.ApiModelProperty.value}", required = true,
    example = "express")
  private String type;
  /**
   * Field to hold the method of shipping
   */
  @ApiModelProperty(value = "${Subscription.method.ApiModelProperty.value}", required = false,
    example = "in-store")
  private String method;
  /**
   * Field to hold the instruction of shipping
   */
  @ApiModelProperty(value = "${Subscription.instruction.ApiModelProperty.value}", required = false,
    example = "this is a gift")
  private String instruction;
  /**
   * Field to hold the delivery period of shipping
   */
  @ApiModelProperty(value = "${Subscription.deliveryperiod.ApiModelProperty.value}", required = false,
    example = "5")
  private long deliveryPeriod;

}
