/**********************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 *  
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *   
 * For further information contact Skava at info@skava.com.
 *********************************************************************************/
package com.skava.subscriptionadmin.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>The Class CustomerAdminAddressDetails is used to define response model of customer address details.</p>
 * @author Mugunthan.G
 * @since July 2018
 * @version 8.0
 */

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
/*
 * (non-Javadoc)
 * 
 * @see java.lang.Object#hashCode()
 */
// @EqualsAndHashCode(callSuper=false)
public class CustomerAdminAddressDetails {

  /**
   * This refers id
   */
  @ApiModelProperty(value = "It refers the id of the address", required = false)
  private long id;

  /** 
   * It refers the type of address (shipping/billing) 
   */
  @ApiModelProperty(value = "It refers the type of address (shipping/billing)", required = false)
  private String type;

  /**
   *It refers the first name of the customer in the address 
   */
  @ApiModelProperty(value = "It refers the first name of the customer in the address", required = true)
  private String firstName;

  /** 
   * It refers the middle name of the customer in the address 
   */
  @ApiModelProperty(value = "It refers the middle name of the customer in the address", required = false)
  private String middleName;

  /** 
   * It refers the last name of the customer in the address 
   */
  @ApiModelProperty(value = "It refers the last name of the customer in the address", required = true)
  private String lastName;

  /** 
   * It refers the street1 of the customer 
   */
  @ApiModelProperty(value = "It refers the street1 of the customer", required = true)
  private String street1;

  /** 
   * It refers the street2 of the customer 
   */
  @ApiModelProperty(value = "It refers the street2 of the customer", required = true)
  private String street2;

  /** 
   * It refers the city of the customer 
   */
  @ApiModelProperty(value = "It refers the city of the customer", required = true)
  private String city;

  /** 
   * It refers the state of the customer 
   */
  @ApiModelProperty(value = "It refers the state of the customer", required = true)
  private String state;

  /** 
   * It refers the country of the customer 
   */
  @ApiModelProperty(value = "It refers the country of the customer", required = true)
  private String country;

  /** 
   * It refers the zipcode of the customer 
   */
  @ApiModelProperty(value = "It refers the zipcode of the customer", required = true)
  private String zipCode;

  /** 
   * It refers the phoneNumber of the phoneNumber 
   */
  @ApiModelProperty(value = "It refers the phoneNumber of the customer", required = true)
  private String phoneNumber;

  /** 
   * It refers the default of the customer address
   */
  @ApiModelProperty(value = "It refers the default of the customer address", required = true)
  private boolean isDefault;

  /**
   * Default constructor
   */
  public CustomerAdminAddressDetails() {
    //Default constructor
  }
}
