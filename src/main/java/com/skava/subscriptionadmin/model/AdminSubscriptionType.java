/*******************************************************************************
 * Copyright ©2002-2019 Skava - All rights reserved.
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
/**
 * 
 */
package com.skava.subscriptionadmin.model;

/**
 * <h1>FrequencyType</h1>
 * <P>
 * This enum is used to maintain the constants for Frequency type parameter in
 * subscription.
 * </p>
 * 
 * @author Skava
 */

/*
 * The current implementation only supports below mentioned type
 * 
 * The following types will be added in future
 * FIXED_ORDER
 * FIXED_MULTI_ORDER
 * 
 */
public enum AdminSubscriptionType {
  VARIABLE_MULTI_ORDER
}
