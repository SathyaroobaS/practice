/*******************************************************************************
 * Copyright ©2002-2019 Skava - All rights reserved.
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
/**
 * 
 */
package com.skava.subscriptionadmin.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/** 
 * AdminAddressResponse class holds the admin response model of address info.
 * 
 * @since Sept 2019
 * @version 8.0
 * @author Gangasri P
 */

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "AdminAddressResponse", description = "This model contains the response of an address info in subscription, "
  + "such as identifier, type, etc.")
public class AdminAddressResponse implements Serializable {

  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = 6495648572837942590L;

  /**
   * Field to hold the identifier of subscription address
   */
  @ApiModelProperty(value = "${Address.id.ApiModelProperty.value}", required = false,
    example = "1001")
  private long id;

  /**
   * Field to hold the firstName of subscription user
   */
  @ApiModelProperty(value = "${Address.firstName.ApiModelProperty.value}", required = false,
    example = "Richard")
  private String firstName;

  /**
   * Field to hold the lastName of subscription user
   */
  @ApiModelProperty(value = "${Address.lastName.ApiModelProperty.value}", required = false,
    example = "Donald")
  private String lastName;

  /**
   * Field to hold the middleName of subscription user
   */
  @ApiModelProperty(value = "${Address.middleName.ApiModelProperty.value}", required = false,
    example = "Mac")
  private String middleName;

  /**
   * Field to hold the companyName of subscription user
   */
  @ApiModelProperty(value = "${Address.companyName.ApiModelProperty.value}", required = false,
    example = "xxxx")
  private String companyName;

  /**
   * Field to hold the address line1 of subscription user
   */
  @ApiModelProperty(value = "${Address.addressLine1.ApiModelProperty.value}", required = false,
    example = "265 Orchard Lane")
  private String addressLine1;

  /**
   * Field to hold the address line2 of subscription user
   */
  @ApiModelProperty(value = "${Address.addressLine2.ApiModelProperty.value}", required = false,
    example = "Hanover Parks")
  private String addressLine2;

  /**
   * Field to hold the address line3 of subscription user
   */
  @ApiModelProperty(value = "${Address.addressLine2.ApiModelProperty.value}", required = false)
  private String addressLine3;

  /**
   * Field to hold the city of subscription user
   */
  @ApiModelProperty(value = "${Address.city.ApiModelProperty.value}", required = false,
    example = "Rochester")
  private String city;

  /**
   * Field to hold the state of subscription user
   */
  @ApiModelProperty(value = "${Address.state.ApiModelProperty.value}", required = false,
    example = "New York")
  private String state;

  /**
   * Field to hold the county of subscription user
   */
  @ApiModelProperty(value = "${Address.county.ApiModelProperty.value}", required = false,
    example = "Monroe")
  private String county;

  /**
   * Field to hold the country of subscription user
   */
  @ApiModelProperty(value = "${Address.country.ApiModelProperty.value}", required = false,
    example = "USA")
  private String country;

  /**
   * Field to hold the zip of subscription user
   */
  @ApiModelProperty(value = "${Address.zip.ApiModelProperty.value}", required = false,
    example = "14602")
  private String zip;

  /**
   * Field to hold the email of subscription user
   */
  @ApiModelProperty(value = "${Address.email.ApiModelProperty.value}", required = false,
    example = "teat@test.com")
  private String email;

  /**
   * Field to hold the phone of subscription user
   */
  @ApiModelProperty(value = "${Address.phone.ApiModelProperty.value}", required = false,
    example = "(555) 206-3437")
  private String phone;

  /**
   * Field to hold the mobile of subscription user
   */
  @ApiModelProperty(value = "${Address.mobile.ApiModelProperty.value}", required = false,
    example = "(585) 275-2100")
  private String mobile;

  /**
   * Field to hold whether the user needs to receive sms or not for subscription
   */
  @ApiModelProperty(value = "${Address.canReceiveSMS.ApiModelProperty.value}", required = false,
    example = "false")
  private boolean canReceiveSMS;
  /**
   * Field to hold the storeId of subscription user
   */
  @ApiModelProperty(value = "${Address.storeId.ApiModelProperty.value}", required = false,
    example = "St001")
  private String storeId;

  /**
   * Field to hold the profileAddressId of subscription user
   */
  @ApiModelProperty(value = "${Address.profileAddressId.ApiModelProperty.value}", required = false,
    example = "p001")
  private String profileAddressId;

  /**
   * Field to hold the address version of subscription
   */
  @ApiModelProperty(value = "${Address.addressVersion.ApiModelProperty.value}", required = false,
    example = "1")
  private long addressVersion;

}
