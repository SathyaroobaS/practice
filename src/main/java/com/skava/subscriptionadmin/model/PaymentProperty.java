/**
 * *******************************************************************************
 *  Copyright ©2002-2018 Skava - All rights reserved.
 *
 *  All information contained herein is, and remains the property of Skava.
 *  Skava including, without limitation, all software and other elements thereof,
 *  are owned or controlled exclusively by Skava and protected by copyright, patent
 *  and other laws. Use without permission is prohibited.
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *
 *  For further information contact Skava at info@skava.com.
 * *******************************************************************************
 */
package com.skava.subscriptionadmin.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * This class defines the model for PropertyDTO (Data Transfer Object).
 * This will be child entity for Payment, CollectionProvider, PaymentItem, Transaction 
 * contains a composite key that comprises of the following
 * 1. property name
 * 2. property value
 * *
 * @author Skava Systems
 * @since 07-Aug-2018
 */

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "Property",
  description = "This model contains the Property details, such as name and value.")
public class PaymentProperty implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * name of the property
   */
  @Size(min = 1, max = 50)
  @NotNull
  // @Reference(CardDetails)
  @ApiModelProperty(value = "${PropertyDTO.name.ApiModelProperty.value}", allowableValues = "range[1, 50]",
    example = "custom_property1")
  private String name;

  /**
   * value for the property
   */
  @Size(min = 1, max = 1000)
  @ApiModelProperty(value = "${PropertyDTO.value.ApiModelProperty.value}",
    allowableValues = "range[1, 1000]", example = "custom property value")
  private String value;
}
