/**
 * ****************************************************************************
 *  Copyright ©2002-2018 Skava.
 *  All rights reserved.The Skava system, including
 *  without limitation, all software and other elements
 *  thereof, are owned or controlled exclusively by
 *  Skava and protected by copyright, patent, and
 *  other laws. Use without permission is prohibited.
 *
 *  For further information contact Skava at info@skava.com.
 * ****************************************************************************
 */
package com.skava.subscriptionadmin.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Min;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * This class implements the PaymentProviderDTO model to define the request and response PaymentProvider entity.
 * It consists of different tokens of different payments. It is connected to ProviderMaster with Many-to-One relation. 
 * This will connect to ProviderMaster and collection entities.
 * @author Skava Systems
 * @since 09-Aug-2018
 */

@Setter
@Getter
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "PaymentProvider", description = "This model contains the details of PaymentProvider.")
public class PaymentProvider implements Serializable {

  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "${PaymentProviderDTO.id.ApiModelProperty.value}", name = "id", dataType = "Long",
    required = false, example = "1", readOnly = true, allowEmptyValue = true)
  @Min(1)
  private Long id;

  /**
   * Unique Id of the Payment.
   */
  @ApiModelProperty(value = "${PaymentProviderDTO.token.ApiModelProperty.value}", name = "token", dataType = "Long",
    required = false, example = "234132543545354", readOnly = true, allowEmptyValue = true)
  private String token;

  /*
   * The unique identifier of the user who created this PaymentProvider.
   */
  @ApiModelProperty(value = "${CREATED_BY}", name = "createdBy",
    dataType = "String", required = false, example = "userName", readOnly = true, allowEmptyValue = true)
  private String createdBy;

  /*
   * PaymentProvider created time
   */
  @ApiModelProperty(value = "${CREATED_TIME}", name = "createdtime", dataType = "Date", required = false,
    example = "2018-11-01T15:46:32.522+0000", readOnly = true, allowEmptyValue = true)
  private Date createdTime;
}
