/**
 * ****************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 *
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof,
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *
 * For further information contact Skava at info@skava.com.
 * ****************************************************************************
 */
package com.skava.subscriptionadmin.model;

import java.util.List;

import com.skava.core.validation.OutputModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/** 
 * SubscriptionDetailsAdminResponse class holds model of subscription details admin response.
 * 
 * @since Sept 2019
 * @version 8.0
 * @author Gangasri P
 */

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "SubscriptionDetailsAdminResponse",
  description = "This model contains the response of a subscription details, such as identifier, type, etc.")
public class SubscriptionDetailsAdminResponse extends OutputModel {

  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = 283902794360640953L;

  /**
   * Field to hold the identifier of subscription
   */
  @ApiModelProperty(value = "Field to hold the identifier of subscription", required = false, example = "001")
  private long subscriptionId;

  @ApiModelProperty(value = "Field to hold the item corresponding to subscription",
    required = false, readOnly = false)
  private List<AdminItemResponse> itemInfo;

  /**
   * Field to hold the period of frequency.
   */
  @ApiModelProperty(value = "Field to hold the period of frequency", required = false, example = "12")
  private long frequencyPeriod;

  /**
   * Field to hold the type of frequency.
   */
  @ApiModelProperty(value = "Field to hold the type of frequency", required = false, example = "MONTHS")
  private String frequencyType;

  /**
   * Field to hold the item quantity.
   */
  @ApiModelProperty(value = "Field to hold the quantity of an item", required = false, example = "4")
  private int quantity;

  /**
   * Field to hold the last order Date.
   */
  @ApiModelProperty(value = "Field to hold the last order Date", required = false, example = "1565039546000")
  private long lastOrderDate;

  /**
   * Field to hold the next order Date.
   */
  @ApiModelProperty(value = "Field to hold the next order Date", required = false, example = "1565039546000")
  private long nextOrderDate;


  /**
   * Field to hold the shipping method of the item.
   */
  @ApiModelProperty(value = "Field to hold the shippping method of the item", required = false, example = "ECONOMY")
  private String shippingMethod;

  /**
   * The enum parameter specifies whether the subscription is active or inactive.
   */
  @ApiModelProperty(value = "The enum parameter specifies whether the subscription is active or inactive", required = true, example = "ACTIVE",
    allowEmptyValue = true)
  private String status;

  /**
   * Field to hold the image url of the product.
   */
  @ApiModelProperty(value = "Field to hold the image url of the product", required = false, example = "img1.png")
  private String imageUrl;

  /**
   * Field to hold the address info of the subscription.
   */
  @ApiModelProperty(value = "Field to hold the address info of the subscription", required = false)
  private AddressResponse shippingAddress;

  /**
   * Field to hold the payment info of the subscription.
   */
  @ApiModelProperty(value = "Field to hold the payment info of the subscription", required = false)
  private Payment paymentInfo;
}
