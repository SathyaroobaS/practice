/*******************************************************************************
 * Copyright ©2002-2019 Skava - All rights reserved.
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
/**
 * 
 */
package com.skava.subscriptionadmin.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * The Class ItemRequest
 * 
 * @author Skava Team
 */
@Accessors(chain = true)
@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "ItemResponse", description = "This model contains the subscription item response details.")
public class ItemResponse implements Serializable {

  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = 6147883223087940841L;

  /**
   * Field to hold the id of item
   */
  @ApiModelProperty(value = "${Subscription.id.ApiModelProperty.value}", required = true,
    example = "101")
  private long id;

  /**
   * Field to hold the price of item
   */
  @ApiModelProperty(value = "${Subscription.price.ApiModelProperty.value}", required = true,
    example = "100.1")
  private float price;

  /**
   * Field to hold the quantity of item
   */
  @ApiModelProperty(value = "${Subscription.quantity.ApiModelProperty.value}", required = false,
    example = "5")
  private int quantity;

  /**
   * Field to hold the preparation period of item
   */
  @ApiModelProperty(value = "${Subscription.preparationPeriod.ApiModelProperty.value}", required = false,
    example = "7")
  private long preparationPeriod;

  /*
   * list of skus
   */
  @ApiModelProperty(value = "${Subscription.skuRequest.ApiModelProperty.value}", required = false)
  private List<SkuResponse> skus;

  /*
   * list of properties
   */
  @ApiModelProperty(value = "${Subscription.propertyRequest.ApiModelProperty.value}", required = false)
  private Map<String, String> properties;

}
