/*******************************************************************************
 * Copyright ©2002-2019 Skava - All rights reserved.
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
/**
 * 
 */
package com.skava.subscriptionadmin.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author asharmohamed
 *
 */
@Accessors(chain = true)
@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "SkuResponse", description = "This model contains the sku details.")
public class SkuResponse implements Serializable {

  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = 2757747632256501187L;

  /**
   * Field to hold the sku id of subscription
   */
  @ApiModelProperty(value = "${Subscription.id.ApiModelProperty.value}", required = true, example = "1001")
  private String id;

  /**
   * Field to hold the product id of sku
   */
  @ApiModelProperty(value = "${Subscription.productId.ApiModelProperty.value}", required = false, example = "1001")
  private String productId;

  /**
   * Field to hold the name of the sku
   */
  @ApiModelProperty(value = "${Subscription.name.ApiModelProperty.value}", required = false, example = "Rich")
  private String name;

  /**
   * Field to hold the optional quantity of sku
   */
  @ApiModelProperty(value = "${Subscription.optionalQuantity.ApiModelProperty.value}",
    required = false, example = "5")
  private int optionalQuantity;

  /*
   * The enum parameter specifies whether the subscription is active or inactive.
   */
  @ApiModelProperty(value = "${Subscription.type.ApiModelProperty.value}", required = true, example = "DEFAULT",
    allowEmptyValue = true)
  private SkuType type;

}
