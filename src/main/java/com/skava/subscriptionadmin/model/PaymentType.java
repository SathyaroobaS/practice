/**********************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.

 *  
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *   
 * For further information contact Skava at info@skava.com.
 *********************************************************************************/
package com.skava.subscriptionadmin.model;

/**
 * This class defines the different supported payment types of a Payment/ProviderMaster entity.
 * This different types of payments are:
 * 1. CREDITCARD
 * 2. DEBITCARD
 * 3. GIFTCARD
 * 4. LOYALTY
 * 5. PURCHASEORDER
 * 6. CASH
 * 
 * @author Skava Systems
 * @since 07-Sep-2018
 */
public enum PaymentType {
  CREDITCARD, DEBITCARD, GIFTCARD, LOYALTY, PURCHASEORDER, CASH;
}
