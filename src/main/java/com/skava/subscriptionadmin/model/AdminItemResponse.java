/*******************************************************************************
 * Copyright ©2002-2019 Skava - All rights reserved.
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
/**
 * 
 */
package com.skava.subscriptionadmin.model;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * The Class ItemRequest
 * 
 * @author Skava Team
 */
@Accessors(chain = true)
@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "AdminItemResponse", description = "This model contains the subscription item response details.")
public class AdminItemResponse implements Serializable {

  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = 6147883223087940841L;
  /**
   * Field to hold the list of skus
   */
  @ApiModelProperty(value = "Field to hold the list of skus", required = false)
  private List<AdminSkuResponse> skus;
}
