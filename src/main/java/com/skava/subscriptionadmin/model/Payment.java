/**
 * ****************************************************************************
 *  Copyright ©2002-2018 Skava.
 *  All rights reserved.The Skava system, including
 *  without limitation, all software and other elements
 *  thereof, are owned or controlled exclusively by
 *  Skava and protected by copyright, patent, and
 *  other laws. Use without permission is prohibited.
 *
 *  For further information contact Skava at info@skava.com.
 * ****************************************************************************
 */
package com.skava.subscriptionadmin.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * The Payment model defines the request and response payment object.
 * It consists of all the details related to the payment made by the user 
 * and also the payments added to his profile. The payment consists of multiple paymentItems.
 * @author Skava systems
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "Payment", description = "This model contains the details of Payment.")
public class Payment implements Serializable {

  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = -3943160466840112370L;

  @ApiModelProperty(value = "${PaymentDTO.id.ApiModelProperty.value}", name = "id", dataType = "Long",
    required = false, example = "1", readOnly = true, allowEmptyValue = true)
  @Min(1)
  private Long id;

  /**
   * A unique id of the collection to which this paymnet is associated.
   */
  @ApiModelProperty(value = "${COLLECTIONID}", name = "collectionId",
    dataType = "Long", required = false, example = "1", readOnly = true, allowEmptyValue = true)
  private Long collectionId;

  /**
   * Name of the payment.
   * It should start with an alphabet and can be an alphaNumeric. Allowing '_' alone.
   */
  @Size(min = 1, max = 100)
  @ApiModelProperty(value = "${PaymentDTO.name.ApiModelProperty.value}", name = "paymentName",
    dataType = "String", required = true, example = "payment_name_1", readOnly = false, allowEmptyValue = false)
  @Pattern(regexp = "^[a-zA-Z][_a-zA-Z0-9]*$",
    message = "Name should start with an alphabet and can be an alphaNumeric. Allowing '_' alone.")
  private String name;

  /**
   * Default payment.
   * Flag to know if the payment is default one
   */
  @ApiModelProperty(value = "${PaymentDTO.isDefault.ApiModelProperty.value}", name = "isDefaultPayment",
    required = true, example = "true", readOnly = false, allowEmptyValue = false)
  @NotNull
  private boolean isDefault;

  /**
   * Type of payment
   * It consists of different types of payments like Creditcard, debitcard, loyalty
   */
  @ApiModelProperty(value = "${PaymentDTO.type.ApiModelProperty.value}", name = "paymentType", required = true,
    example = "CREDITCARD", readOnly = false, allowEmptyValue = false)
  private PaymentType type;

  /**
   * status of payment
   */
  @ApiModelProperty(value = "${PaymentDTO.status.ApiModelProperty.value}", name = "paymentStatus", dataType = "enum",
    required = false, example = "ACTIVE", readOnly = true, allowEmptyValue = true)
  private Status status;

  /**
   * Unique id of a user associated to a payment
   */
  @ApiModelProperty(value = "${PaymentDTO.userId.ApiModelProperty.value}", name = "userId",
    dataType = "String", required = false, example = "123", readOnly = true, allowEmptyValue = true)
  private String userId;

  @ApiModelProperty(value = "${PaymentDTO.accountId.ApiModelProperty.value}", name = "accountId", dataType = "String",
    required = false, example = "1234", readOnly = true, allowEmptyValue = true)
  private String accountId;

  @ApiModelProperty(value = "${PaymentDTO.billingAddress.ApiModelProperty.value}", name = "billingAddress",
    required = false, readOnly = false, allowEmptyValue = false)
  @Valid
  private AdminBillingAddress billingAddress;

  @ApiModelProperty(value = "${PaymentDTO.providers.ApiModelProperty.value}", name = "paymentProviders",
    required = false, readOnly = true, allowEmptyValue = true)
  @Valid
  private List<PaymentProvider> providers;

  @ApiModelProperty(value = "${PaymentDTO.paymentProperties.ApiModelProperty.value}", name = "properties",
    dataType = "List", required = false, readOnly = false, allowEmptyValue = true)
  @Valid
  @JsonInclude(value = Include.NON_NULL)
  private List<PaymentProperty> paymentProperties;

  /*
   * The unique identifier of the user who created this payment.
   */
  @ApiModelProperty(value = "${CREATED_BY}", name = "createdBy",
    dataType = "String", required = false, example = "userName", readOnly = true, allowEmptyValue = true)
  private String createdBy;

  /*
   * Payment created time
   */
  @ApiModelProperty(value = "${CREATED_TIME}", name = "createdtime", dataType = "Date", required = false,
    example = "2018-11-01T15:46:32.522+0000", readOnly = true, allowEmptyValue = true)
  private Date createdTime;

  /*
   * The unique identifier of the most recent user who updated the payment.
   */
  @ApiModelProperty(value = "${UPDATED_BY}", name = "updatedBy", dataType = "String",
    required = false, example = "userName", readOnly = true, allowEmptyValue = true)
  private String updatedBy;

  /*
   * Payment updated time
   */
  @ApiModelProperty(
    value = "${UPDATED_TIME}", name = "updatedtime", dataType = "Date",
    required = false, example = "2018-11-01T15:46:32.522+0000", readOnly = true, allowEmptyValue = true)
  private Date updatedTime;
  
  /**
   * Status - this defines the list of statuses that a patment can support
   * IN_ACTIVE : payment in inactive state
   * ACTIVE: payment in active state.
   */
  public enum Status {
    ACTIVE, INACTIVE;
  }
}
