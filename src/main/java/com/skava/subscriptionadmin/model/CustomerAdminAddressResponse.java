/**********************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 *  
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *   
 * For further information contact Skava at info@skava.com.
 *********************************************************************************/
package com.skava.subscriptionadmin.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>The Class CustomerAdminAddressResponse is used to define response model of customer address create and update.</p>
 * @author Mugunthan.G
 * @since July 2018
 * @version 8.0
 */

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerAdminAddressResponse {
  /** 
   * It refers the addresses
   */
  @ApiModelProperty(value = "It refers the addresses", required = false)
  private List<CustomerAdminAddressDetails> addresses;

  /**
   * Default constructor
   */
  public CustomerAdminAddressResponse() {
    //Default constructor
  }

}
