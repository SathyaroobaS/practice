/**
 * ****************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 *
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof,
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *
 * For further information contact Skava at info@skava.com.
 * ****************************************************************************
 */
package com.skava.subscriptionadmin.model;

import java.util.List;
import java.util.Map;

import com.skava.core.constant.DocumentationConstants;
import com.skava.core.validation.OutputModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Abishek
 *
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "SubscriptionResponse",
  description = "This model contains the response of a subscription, such as identifier, type, etc.")
public class SubscriptionDetailsResponse extends OutputModel {

  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = 283902794360640953L;

  /**
   * Field to hold the identifier of subscription
   */
  @ApiModelProperty(value = "${Subscription.id.ApiModelProperty.value}", required = false, example = "subscription001",
    readOnly = false)
  private long id;

  /**
   * Field to hold the collectionId of subscription
   */
  @ApiModelProperty(value = DocumentationConstants.COLLECTION_ID_DESC, required = false, example = "100",
    readOnly = false)
  private long collectionId;

  /**
   * Field to hold the identifier of user
   */
  @ApiModelProperty(value = "${Subscription.userId.ApiModelProperty.value}", required = false, example = "101",
    readOnly = true)
  private String userId;

  /**
   * Field to hold the identifier of account
   */
  @ApiModelProperty(value = "${Subscription.accountId.ApiModelProperty.value}", required = false, example = "acc101",
    readOnly = true)
  private String accountId;

  /**
   * Field to hold the Store Id
   */
  @ApiModelProperty(value = "${Subscription.storeId.ApiModelProperty.value}", required = false,
    example = "store001", readOnly = false)
  private String storeId;

  /*
   * The enum parameter specifies whether the subscription is active or inactive.
   */
  @ApiModelProperty(value = "${Subscription.status.ApiModelProperty.value}", required = true, example = "ACTIVE",
    allowEmptyValue = true)
  private SubscriptionStatus status;

  /*
   * The parameter specifies the type of Subscription.
   */
  @ApiModelProperty(value = "${Subscription.subscriptionType.ApiModelProperty.value}",
    required = true, example = "VARIABLE_MULTI_ORDER", allowEmptyValue = true)
  private SubscriptionType type;

  /**
   * It refers to the frequency End type
   */
  @ApiModelProperty(value = "${Subscription.frequencyEndType.ApiModelProperty.value}",
    required = true, example = "AFTER", allowEmptyValue = true)
  private FrequencyEndType frequencyEndType;
  /*
   * The parameter specifies the type of frequency.
   */
  @ApiModelProperty(value = "${Subscription.frequencyType.ApiModelProperty.value}", required = true,
    example = "DAILY", allowEmptyValue = true)
  private FrequencyType frequencyType;

  /**
   * Field to hold the period of frequency.
   */
  @ApiModelProperty(value = "${Subscription.frequencyPeriod.ApiModelProperty.value}", required = false,
    example = "12", readOnly = false)
  private long frequencyPeriod;

  /**
   * Field to hold the start date of frequency
   */
  @ApiModelProperty(value = "${Subscription.frequencyStartDate.ApiModelProperty.value}", required = false,
    example = "12356487956", readOnly = false)
  private long frequencyStartDate;

  /**
   * Field to hold the end date of frequency
   */
  @ApiModelProperty(value = "${Subscription.frequencyEndDate.ApiModelProperty.value}", required = false,
    example = "12356487956", readOnly = false)
  private long frequencyEndDate;

  /**
   * It refers to the revisedDeliveryDate;
   */
  @ApiModelProperty(value = "${Subscription.revisedDeliveryDate.ApiModelProperty.value}",
    required = false, example = "12345697125", readOnly = false)
  private long revisedDeliveryDate;

  /**
   * It refers to the firstDeliveryDate;
   */
  @ApiModelProperty(value = "${Subscription.firstDeliveryDate.ApiModelProperty.value}",
    required = false, example = "12354659788", readOnly = false)
  private long firstDeliveryDate;

  /**
   * It refers to the next delivery edit freeze date
   */
  @ApiModelProperty(value = "${Subscription.nextDeliveryEditFreezeDate.ApiModelProperty.value}",
    required = false, example = "123456878568", readOnly = false)
  private long nextDeliveryEditFreezeDate;

  /**
   * It refers to the nextNotificationDate
   */
  @ApiModelProperty(value = "${Subscription.nextNotificationDate.ApiModelProperty.value}",
    required = false, example = "123456878568", readOnly = false)
  private long nextNotificationDate;

  /**
   * It refers to the nextOrderCreationDate
   */
  @ApiModelProperty(value = "${Subscription.nextOrderCreationDate.ApiModelProperty.value}",
    required = false, example = "1235456878356", readOnly = false)
  private long nextOrderCreationDate;

  /**
   * It refers to the nextDeliveryDate
   */
  @ApiModelProperty(value = "${Subscription.nextDeliveryDate.ApiModelProperty.value}",
    required = false, example = "123456789258", readOnly = false)
  private long nextDeliveryDate;

  /**
   * It refers to the last order processed date.
   */
  @ApiModelProperty(value = "${Subscription.lastorOrderProcessedDate.ApiModelProperty.value}",
    required = false, example = "1235456878356", readOnly = false)
  private long lastOrderProcessedDate;

  /**
   * It refers to the last order id.
   */
  @ApiModelProperty(value = "${Subscription.lastorOrderId.ApiModelProperty.value}",
    required = false, example = "1235456878356", readOnly = false)
  private long lastorOrderId;

  /**
   * It refers to the current instance
   */
  @ApiModelProperty(value = "${Subscription.currentInstance.ApiModelProperty.value}",
    required = false, readOnly = false)
  private int currentInstance;

  /**
   * Field to hold the updated time for the subscription
   */
  @ApiModelProperty(value = DocumentationConstants.UPDATED_TIME_DESC, required = false, readOnly = true)
  private long updatedTime;

  /**
   * Field to hold the created time for the subscription
   */
  @ApiModelProperty(value = DocumentationConstants.CREATED_TIME_DESC, required = false, readOnly = true)
  private long createdTime;

  /**
   * Field to hold the id of the user who updated the subscription
   */
  @ApiModelProperty(value = DocumentationConstants.UPDATED_BY_DESC, required = false, readOnly = true)
  private String updatedBy;

  /**
   * Field to hold the id of the user who created the subscription
   */
  @ApiModelProperty(value = DocumentationConstants.CREATED_BY_DESC, required = false, readOnly = true)
  private String createdBy;

  @ApiModelProperty(value = "${Subscription.propertyRequest.ApiModelProperty.value}",
    required = false, readOnly = false)
  private Map<String, String> properties;

  @ApiModelProperty(value = "${Subscription.itemRequest.ApiModelProperty.value}",
    required = false, readOnly = false)
  private List<ItemResponse> itemInfo;

  @ApiModelProperty(value = "${Subscription.paymentInfo.ApiModelProperty.value}",
    required = false, readOnly = false)
  private Map<String, String> paymentInfo;

  @ApiModelProperty(value = "${Subscription.shippingInfo.ApiModelProperty.value}",
    required = false, readOnly = false)
  private ShippingResponse shippingInfo;

  @ApiModelProperty(value = "${Subscription.addressInfo.ApiModelProperty.value}",
    required = false, readOnly = false)
  private AddressResponse addressInfo;

}
