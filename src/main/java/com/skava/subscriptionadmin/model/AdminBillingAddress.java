/**
 * ****************************************************************************
 *  Copyright ©2002-2018 Skava.
 *  All rights reserved.The Skava system, including
 *  without limitation, all software and other elements
 *  thereof, are owned or controlled exclusively by
 *  Skava and protected by copyright, patent, and
 *  other laws. Use without permission is prohibited.
 *
 *  For further information contact Skava at info@skava.com.
 * ****************************************************************************
 */
package com.skava.subscriptionadmin.model;

import java.io.Serializable;

import javax.validation.constraints.Email;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "BillingAddress", description = "This model contains the details of BillingAddress.")
public class AdminBillingAddress implements Serializable {

  private static final long serialVersionUID = 1L;

  /*
   * Firstname of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.firstName.ApiModelProperty.value}", name = "firstname",
    dataType = "String", required = false, example = "William", readOnly = false, allowEmptyValue = false)
  private String firstName;

  /*
   * Lastname of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.lastName.ApiModelProperty.value}", name = "lastname",
    dataType = "String", required = false, example = "Pitt", readOnly = false, allowEmptyValue = false)
  private String lastName;

  /*
   * Address line1 of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.addressLine1.ApiModelProperty.value}", name = "addressLine1",
    dataType = "String", required = false, example = "200 E MAIN ST", readOnly = false, allowEmptyValue = false)
  private String addressLine1;

  /*
   * Address line2 of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.addressLine2.ApiModelProperty.value}", name = "addressLine2",
    dataType = "String", required = false, example = "SUITE 5A-1204", readOnly = false, allowEmptyValue = false)
  private String addressLine2;

  /*
   * Address line3 of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.addressLine3.ApiModelProperty.value}", name = "addressLine3",
    dataType = "String", required = false, example = "PHOENIX", readOnly = false, allowEmptyValue = false)
  private String addressLine3;

  /*
   * city name of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.city.ApiModelProperty.value}", name = "city", dataType = "String",
    required = false, example = "NEW YORK", readOnly = false, allowEmptyValue = false)
  private String city;

  /*
   * state name of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.state.ApiModelProperty.value}", name = "state", dataType = "String",
    required = false, example = "NEW YORK", readOnly = false, allowEmptyValue = false)
  private String state;

  /*
   * zipcode of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.zipcode.ApiModelProperty.value}", name = "zip", dataType = "String",
    required = false, example = "10001", readOnly = false, allowEmptyValue = false)
  private String zipcode;

  /*
   * country name of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.country.ApiModelProperty.value}", name = "country",
    dataType = "String",
    required = false, example = "USA", readOnly = false, allowEmptyValue = false)
  private String country;

  /*
   * county name of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.county.ApiModelProperty.value}", name = "county", dataType = "String",
    required = false, example = "Essex", readOnly = false, allowEmptyValue = false)
  private String county;

  /*
   * phone number of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.phoneNumber.ApiModelProperty.value}", name = "phoneNumber",
    dataType = "String", required = false, example = "123-310-6600", readOnly = false, allowEmptyValue = false)
  private String phoneNumber;

  /*
   * email of the user
   */
  @ApiModelProperty(value = "${BillingAddressDTO.email.ApiModelProperty.value}", name = "email", dataType = "String",
    required = false, example = "test@test.com", readOnly = false, allowEmptyValue = false)
  @Email
  private String email;
}
