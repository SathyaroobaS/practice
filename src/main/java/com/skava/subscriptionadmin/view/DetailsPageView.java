/**********************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 *  
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *   
 * For further information contact Skava at info@skava.com.
 *********************************************************************************/
package com.skava.subscriptionadmin.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.skava.common.BreadcrumbBuilder;
import com.skava.common.Constants;
import com.skava.common.layout.model.BreadCrumb;
import com.skava.common.layout.model.MenuItem;
import com.skava.common.layout.model.MicroserviceLayout;
import com.skava.common.layout.model.PageData;
import com.skava.common.model.AdminRequestParams;
import com.skava.common.view.AdminView;
import com.skava.subscriptionadmin.SubscriptionAdminConstants;
import com.skava.subscriptionadmin.SubscriptionAdminUtils;
import com.skava.subscriptionadmin.model.AddressResponse;
import com.skava.subscriptionadmin.model.AdminBillingAddress;
import com.skava.subscriptionadmin.model.CustomerAdminAddressDetails;
import com.skava.subscriptionadmin.model.CustomerAdminAddressResponse;
import com.skava.subscriptionadmin.model.Payment;
import com.skava.subscriptionadmin.model.PaymentProperty;
import com.skava.subscriptionadmin.model.SubscriptionDetailsAdminResponse;
import com.skava.subscriptionadmin.model.SubscriptionRequestParams;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@NoArgsConstructor
@Qualifier("DetailsPageView")
/**
 * <h1>ListPageView</h1>
 * 
 * <p>It refers Notification ListPage View</p>
 * @author rajesh
 *
 */
public class DetailsPageView extends AdminView {
  private static final String VIEW_TEMPLATE = "v2/pages/subscriptionDetails";

  /**
   * getViewName
   * 
   * It is used to get the page template
   * 
   * @return String template
   */
  @Override
  public String getViewName() {
    return VIEW_TEMPLATE;
  }

  /**
   * getServiceType
   * 
   * It is used to get the service type
   * 
   * @return String subscription type
   */
  @Override
  public String getServiceType() {
    return Constants.SERVICE_TYPE_SUBSCRIPTION;
  }

  /**
   * getServiceType
   * 
   * It is used to get the service type
   * 
   * @return PageData for details page
   */
  @Override
  public PageData getPageData(HttpServletRequest request, HttpServletResponse response,
    AdminRequestParams reqParams) {
    PageData model = new PageData();
    SubscriptionRequestParams requestParams = (SubscriptionRequestParams) reqParams;
    String hostRest = getZookeeperConfig().getURL(SubscriptionAdminConstants.SERVICE_NAME_ORCHESTRTION);
    SubscriptionDetailsAdminResponse subscriptionDetailsResponse = new SubscriptionDetailsAdminResponse();
    String subscriptionDetailsUrl = hostRest
      + SubscriptionAdminUtils.urlReplaceMacros(requestParams,
        SubscriptionAdminConstants.APIURL_GET_SUBSCRIPTION_DETAILS);
    try {
      subscriptionDetailsResponse = getMicroserviceLayoutUtil().getData(request, subscriptionDetailsUrl,
        SubscriptionDetailsAdminResponse.class);
    } catch (HttpClientErrorException | HttpServerErrorException e) {
      log.debug("fetchSettingsData : get Response Error {}", e);
    }

    model.addObject("subscriptionDetails", subscriptionDetailsResponse);

    AddressResponse shippingAddress = subscriptionDetailsResponse.getShippingAddress();
    Map<String, Object> shippingAddressMap = new ObjectMapper().convertValue(shippingAddress,
      Map.class);
    model.addObject("shippingAddressMap", shippingAddressMap);

    Payment paymentInfo = subscriptionDetailsResponse.getPaymentInfo();
    Map<String, Object> paymentInfoMap = new ObjectMapper().convertValue(paymentInfo,
      Map.class);
    model.addObject("paymentInfoMap", paymentInfoMap);

    if (subscriptionDetailsResponse.getPaymentInfo() != null) {
      List<PaymentProperty> paymentProperties = subscriptionDetailsResponse.getPaymentInfo().getPaymentProperties();
      model.addObject("paymentProperties", paymentProperties);
      Map<String, Object> paymentPropertiesMap = new HashMap<>();
      for (int i = 0; i < paymentProperties.size(); i++) {
        paymentPropertiesMap.put(paymentProperties.get(i).getName(), paymentProperties.get(i).getValue());
      }
      model.addObject("paymentPropertiesMap", paymentPropertiesMap);
    }

    if (paymentInfo != null) {
      AdminBillingAddress adminBillingAddress = paymentInfo.getBillingAddress();
      Map<String, Object> adminBillingAddressMap = new ObjectMapper().convertValue(adminBillingAddress,
        Map.class);
      model.addObject("adminBillingAddressMap", adminBillingAddressMap);
    }

    Map<String, Object> subscriptionDetailsResponseMap = new ObjectMapper().convertValue(subscriptionDetailsResponse,
      Map.class);
    model.addObject("subscriptionDetailsResponseMap", subscriptionDetailsResponseMap);

    CustomerAdminAddressResponse customerAdminAddressResponse = new CustomerAdminAddressResponse();
    List<CustomerAdminAddressDetails> customerAdminAddressDetailsList = null;
    String availableUserAddressUrl = hostRest
      + SubscriptionAdminUtils.urlReplaceMacros(requestParams,
        SubscriptionAdminConstants.APIURL_GET_USER_ADDRESSES);

    try {
      customerAdminAddressResponse = getMicroserviceLayoutUtil().getData(request, availableUserAddressUrl,
        CustomerAdminAddressResponse.class);
      customerAdminAddressDetailsList = customerAdminAddressResponse.getAddresses();
    } catch (HttpClientErrorException | HttpServerErrorException e) {
      log.debug("fetchSettingsData : get Response Error {}", e);
    }

    model.addObject("customerAdminAddressDetailsList", customerAdminAddressDetailsList);
    return model;
  }

  /**
   * getPageData
   * 
   * It is used to get the page data
   * 
   * @return {@link PageData}
   */
  @Override
  public List<BreadCrumb> getBreadCrumbs(AdminRequestParams requestParams, MicroserviceLayout mLayoutData) {
    /*
     * return new BreadcrumbBuilder()
     * .addBreadCrumb(getMessageSource().getMessage("SUBSCRIPTIONS.TEXT_SUBSCRIPTIONS", null, getLanguage()),
     * null, null, null)
     * .build(requestParams);
     */
    SubscriptionRequestParams subscriptionRequestParams = (SubscriptionRequestParams) requestParams;
    return new BreadcrumbBuilder()
      .addBreadCrumb(
        new BreadCrumb(getMessageSource().getMessage("SUBSCRIPTIONS.TEXT_SUBSCRIPTIONS", null, getLanguage()),
          getMessageSource().getMessage("SUBSCRIPTIONS.TEXT_SUBSCRIPTIONS", null, getLanguage()),
          SubscriptionAdminConstants.URL_GET_ALL_SUBSCRIPTIONS, null))
      .addBreadCrumb(new BreadCrumb(subscriptionRequestParams.getSubscriptionId() + "",
        null, "#", null))
      .build(requestParams);
  }

  /**
   * getBreadCrumbs
   * 
   * It is used to get the breadcrums for the page.
   * 
   * @return
   */
  @Override
  public List<MenuItem> getPageSideNavigation(AdminRequestParams requestParams, MicroserviceLayout mLayoutData) {
    return SubscriptionAdminUtils.getSubscriptionDetailsSideNav(SubscriptionAdminConstants.SIDENAV_LABEL_OVERVIEW,
      requestParams);
  }
}
