/**********************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 *  
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *   
 * For further information contact Skava at info@skava.com.
 *********************************************************************************/
package com.skava.subscriptionadmin.view;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.skava.common.BreadcrumbBuilder;
import com.skava.common.Constants;
import com.skava.common.layout.model.BreadCrumb;
import com.skava.common.layout.model.MenuItem;
import com.skava.common.layout.model.MicroserviceLayout;
import com.skava.common.layout.model.PageData;
import com.skava.common.model.AdminRequestParams;
import com.skava.common.view.AdminView;

import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
@Qualifier("ListPageView")
/**
 * <h1>ListPageView</h1>
 * 
 * <p>It refers Notification ListPage View</p>
 * @author rajesh
 *
 */
public class ListPageView extends AdminView {
  private static final String VIEW_TEMPLATE = "v2/pages/subscriptions";

  /**
   * getViewName
   * 
   * It is used to get the page template
   * 
   * @return 
   */
  @Override
  public String getViewName() {
    return VIEW_TEMPLATE;
  }

  /**
   * getServiceType
   * 
   * It is used to get the service type
   * 
   * @return 
   */
  @Override
  public String getServiceType() {
    return Constants.SERVICE_TYPE_SUBSCRIPTION;
  }

  /**
   * getServiceType
   * 
   * It is used to get the service type
   * 
   * @return 
   */
  @Override
  public PageData getPageData(HttpServletRequest request, HttpServletResponse response,
    AdminRequestParams requestParams) {
    return null;
  }

  /**
   * getPageData
   * 
   * It is used to get the page data
   * 
   * @return {@link PageData}
   */
  @Override
  public List<BreadCrumb> getBreadCrumbs(AdminRequestParams requestParams, MicroserviceLayout mLayoutData) {
    return new BreadcrumbBuilder()
      .addBreadCrumb(getMessageSource().getMessage("SUBSCRIPTIONS.TEXT_SUBSCRIPTIONS", null, getLanguage()),
        null, null, null)
      .build(requestParams);
  }

  /**
   * getBreadCrumbs
   * 
   * It is used to get the breadcrums for the page.
   * 
   * @return
   */
  @Override
  public List<MenuItem> getPageSideNavigation(AdminRequestParams requestParams, MicroserviceLayout mLayoutData) {
    return new ArrayList<>();
  }
}
