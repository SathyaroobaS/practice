FormManager.setHandler('edit-subscription-shipping',(function(){
    
    function onSubmit(evt, info){
        if (info.form.checkValidity()) {
            var props = FormManager.objectify(info.form)
                        
            var cbk = function() {
                FormManager.cancel(info.form);
                setTimeout(function() {
                    loader.destroy();
                }, 500);
            }
            console.log(props);

            physicalAddressValidation(props, cbk);
            
            var loader = new LoadBlock({target:info.form})
        } else {
            info.form.classList.add('was-validated');
        }
        
        evt.preventDefault();
        evt.stopPropagation();
        
        return false;
    }

    function physicalAddressValidation(props, cbk) {
        var addressValidationUrl = (GLOBAL_CONFIG && GLOBAL_CONFIG.URL_ORCHESTRATAION ? GLOBAL_CONFIG.URL_ORCHESTRATAION : '') + 
          '/subscriptions/addressValidation';
        var collectionId = Util.queryValues && Util.queryValues.collectionId ? Util.queryValues.collectionId : "";
        addressValidationUrl = Util.appendBasicParam(addressValidationUrl, collectionId); 

        var reqconfig = {};
        reqconfig.data = 
        {
            "addressInfo": props
        };

        var config = {
            url : addressValidationUrl,
            data : JSON.stringify(props),
            method: "POST",
            contentType: "application/json"
        }
        
        var callBack = function(response, status)
        {
            if (status == "error") {
                var message = (response ? (response.responseMessage ? response.responseMessage : (response.responseJSON && response.responseJSON.responseMessage ? response.responseJSON.responseMessage : 'Something went wrong.')) : 'Something went wrong.')
                Util.toast(message, null , status);
            }

            if(status == "success") {
              if(response && response.validationDetails && response.validationDetails[0]){
                  if(response.validationDetails[0].validated == true){
                	  getShippingMethods(props, cbk);
                  }
                  else{
                    if(response.validationDetails[0].recommendations){
                      var addrRecommendations = response.validationDetails[0].recommendations;
                      ModalManager.launch({type:'addressRecommendations', recommendationArr:addrRecommendations, givenAddr:props});
                    }
                  }
              }
            }

            if(cbk)
            {
                cbk(response, arguments);
            }
        };
        
        Util.getData(config, callBack);
      }

    function constructAvailableShippingMtds(shippingMethods){
  	  var recommendationHTML = [];
  	  recommendationHTML.push('<div class="pb-1"><b>Available Shipping Methods</b></div>');
  	  recommendationHTML.push('<div class="cls_ShippingMethodList" style="max-height: 240px; overflow: auto; -webkit-overflow-scrolling: touch;">');
  		for(var i=0; i<shippingMethods.length; i++){
  			recommendationHTML.push('<div class="col-12">');
  			if(i==0){
  				recommendationHTML.push('<input type="radio" name="shippingValue" id="idSelectedMethod'+i+'" class="form-check-input h5" value='+shippingMethods[i].shippingMethodId+' checked="checked">');
  			}
  			else{
  				recommendationHTML.push('<input type="radio" name="shippingValue" id="idSelectedMethod'+i+'" class="form-check-input h5" value='+shippingMethods[i].shippingMethodId+'>');
  			}
  			recommendationHTML.push('<label class="form-check-label" for="idSelectedMethod'+i+'">'+shippingMethods[i].shippingMethodId+'</label>');
  			recommendationHTML.push('</div>');
  		}
  		recommendationHTML.push('</div>');
  		return recommendationHTML;
    }

    function getShippingMethods(props, cbk) {
        var addressValidationUrl = (GLOBAL_CONFIG && GLOBAL_CONFIG.URL_ORCHESTRATAION ? GLOBAL_CONFIG.URL_ORCHESTRATAION : '') + 
          '/subscriptions/calculateshipping';
        var collectionId = Util.queryValues && Util.queryValues.collectionId ? Util.queryValues.collectionId : "";
        addressValidationUrl = Util.appendBasicParam(addressValidationUrl, collectionId); 
        addressValidationUrl = addressValidationUrl + "&validationType=ESTIMATE";

        var reqconfig = {};
        reqconfig.data = {
        	  "deliveryAddress": props,
        	  "deliveryType": "PHYSICAL",
        	  "itemDetails": {
        	    "shippingClass": "DEFAULT",
        	    "skuId": "string",
        	    "skuName": "string",
        	    "skuQuantity": 0
        	  }
        	}
        var config = {
            url : addressValidationUrl,
            data : JSON.stringify(reqconfig.data),
            method: "POST",
            contentType: "application/json"
        }
        
        var callBack = function(response, status)
        {
            if (status == "error") {
                var message = (response ? (response.responseMessage ? response.responseMessage : (response.responseJSON && response.responseJSON.responseMessage ? response.responseJSON.responseMessage : 'Something went wrong.')) : 'Something went wrong.')
                Util.toast(message, null , status);
            }

            if(status == "success") {
              if(response && response.shippingDetails){
                if(response.shippingDetails.cartShippingMethods){
                  var shippingMethods = response.shippingDetails.cartShippingMethods;
                  //ModalManager.launch({type:'shippingMethods', shippingMethods:shippingMethods, givenAddr:props});
                  var addrValidatedMsg = "Address validated - choose an available shipping method";
                  Util.toast(addrValidatedMsg, null, status);
                  $('.cls_subscription_shipping_Method').addClass('editable z-depth-4');
                  $('.cls_subscription_shipping_Method .save-btn').removeAttr("disabled");
                  $('.cls_shippingAddrData').data("addr", props);
                  $(".cls_subscriptionShippingMtd").html(constructAvailableShippingMtds(shippingMethods));
                }
              }
            }

            if(cbk)
            {
                cbk(response, arguments);
            }
        };
        
        Util.getData(config, callBack);
      }

    return {
        onSubmit:onSubmit
    }
    
})())