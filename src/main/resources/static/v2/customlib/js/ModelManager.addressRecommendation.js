var CONST_ADDR_DELIMITER = "...";
ModalManager.setHandler('addressRecommendations',(function(){
	var defaultConfig = {
		style:{
			size:'md'
		}
	}
	
	function delaySubmit(form,instant){
		//to do
	}
	
	function handleSubmit(form){
		//to do
	}
	
	function getRecommendationContent(config){
		//$('.cls_subscription_shipping_Info').addClass('editable z-depth-4');
		var givenAddr = config.givenAddr;
		
		var recommendationArr = config.recommendationArr;
		var recommendationHTML = [];
		recommendationHTML.push('<div class=""><b>Given Invalid Address</b></div>');
		recommendationHTML.push('<div class="col-12 col-md-6">');
		 recommendationHTML.push('<div>');
			recommendationHTML.push('<span>'+givenAddr.addressLine1+'</span><br>');
			if(givenAddr.addressLine2){
			recommendationHTML.push('<span>'+givenAddr.addressLine2+'</span><br>');
			}
			recommendationHTML.push('<span>'+givenAddr.city+'</span>, <span>'+givenAddr.state+'</span> <span>'+givenAddr.zip+'</span><br>');
			recommendationHTML.push('<span>'+givenAddr.country+'</span> <br>');
		 recommendationHTML.push('</div>');
		recommendationHTML.push('</div>');
		recommendationHTML.push('<div class=""><b>Suggested Addresses</b></div>');
		if((recommendationArr != null) && (recommendationArr.length > 0) && (recommendationArr[0].addressLine1)){
		recommendationHTML.push('<div class="cls_recommendationList" style="max-height: 240px; overflow: auto; -webkit-overflow-scrolling: touch;">');
		for(var i=0; i<recommendationArr.length; i++){
			if((i==0) || (i%2==0)){
				recommendationHTML.push('<div class="row">');
			}
			if(isLastRow(i, recommendationArr.length)){
				recommendationHTML.push('<div class="col-6">');
			}
			else{
				recommendationHTML.push('<div class="col-6 border-bottom">');
			}
				recommendationHTML.push('<div class="Xborder-bottom" role="alert">');
					recommendationHTML.push('<div class="">');
					if(i==0){
						recommendationHTML.push('<input type="radio" name="addrRecommendationValue" id="idSelectedAddr'+i+'" class="form-check-input h5 align-self-center" value="idSelectedAddrVal'+i+'" checked="checked">');
					}
					else{
						recommendationHTML.push('<input type="radio" name="addrRecommendationValue" id="idSelectedAddr'+i+'" class="form-check-input h5 align-self-center" value="idSelectedAddrVal'+i+'">');
					}
					recommendationHTML.push('<label class="form-check-label " for="idSelectedAddr'+i+'">');
					recommendationHTML.push('<div id="idSelectedAddrVal'+i+'">');
					if(recommendationArr[i].addressLine1 != undefined){
					recommendationHTML.push('<div class="">');
					recommendationHTML.push('<span class="cls_ChoosePaymentStreet">'+recommendationArr[i].addressLine1+'</span>');
					recommendationHTML.push('</div>');
					}
					if(recommendationArr[i].addressLine2 != undefined){
					recommendationHTML.push('<div class="">');
					recommendationHTML.push('<span class="cls_ChoosePaymentStreet2">'+recommendationArr[i].addressLine2+'</span>');
					recommendationHTML.push('</div>');
					}
					recommendationHTML.push('<div class="">');
					if(recommendationArr[i].city != undefined){
					recommendationHTML.push('<span class="cls_ChoosePaymentCity"> '+recommendationArr[i].city+'</span>,&nbsp;');
					}
					if(recommendationArr[i].state != undefined){
					recommendationHTML.push('<span class="cls_ChoosePaymentState">'+recommendationArr[i].state+'</span> ');
					}
					if(recommendationArr[i].zip != undefined){
					recommendationHTML.push('<span class="cls_ChoosePaymentZip">'+recommendationArr[i].zip+'</span>');
					}
					recommendationHTML.push('</div>');
					if(recommendationArr[i].country != undefined){
					recommendationHTML.push('<div class="row p-0">');
					recommendationHTML.push('<span class="cls_ChoosePaymentCountry">'+recommendationArr[i].country+'</span>');
					recommendationHTML.push('</div>');
					}
					recommendationHTML.push('</div>');

					recommendationHTML.push('</label>');
					
					recommendationHTML.push('</div>');
				recommendationHTML.push('</div>');
			recommendationHTML.push('</div>');
			if(((i!=0) && (i%2!=0)) || (i+1 == recommendationArr.length)){
				recommendationHTML.push('</div>');
			}
		}
		recommendationHTML.push('</div>');
		}
		else{
			recommendationHTML.push('<div class="row">No suggestions found</div>');
		}
		return recommendationHTML.join("");
	}

	function constructShippingAddr(elem) {
		$(".cls_subscription_shipping_Info .save-btn").removeAttr("disabled");
	    var parEle = $(".cls_subscription_shipping_Info");
	    var selectedElem;
	    var fillMapping = {
	        "firstName" : "cls_ChoosePaymentFName",
	        "lastName" : "cls_ChoosePaymentLName",
	        "addressLine1" : "cls_ChoosePaymentStreet",
	        "addressLine2" : "cls_ChoosePaymentStreet2",
	        "city" : "cls_ChoosePaymentCity",
	        "state" : "cls_ChoosePaymentState",
	        "country" : "cls_ChoosePaymentCountry",
	        "zip" : "cls_ChoosePaymentZip",
	        "phone" : "cls_ChoosePaymentPhone",
	        "email" : "cls_ChooseEmail"
	    }

	    var addrToRet = {};
	    $.each(fillMapping, function(key, value) {
	        selectedElem = $(parEle).find("input[name=" + key + "]");
	        var addrToAppend = elem.find("." + value).text();
	        if(addrToAppend != null){
	          if(addrToAppend.trim().length <= 0){
	        	  addrToAppend = selectedElem.val();
	          }
	          if(addrToAppend.includes(CONST_ADDR_DELIMITER)){
	              addrToAppend = preProcessAddressWithRange(addrToAppend);
	          }
	        }
	        addrToRet[key] = preProcessAddressWithRange(addrToAppend);
	    });

	    return addrToRet;
	}

	function preProcessAddressWithRange(givenAddrStr){
		var currAddr = "";
		var skipNext = false;
		var givenAddr = givenAddrStr.split(" ");
		for(var i = 0; i < givenAddr.length; i++){ 
			if(CONST_ADDR_DELIMITER == givenAddr[i]){
				skipNext = true;
			}
			else{
				if(skipNext == false){
					if(i != 0) currAddr = currAddr + " ";
					currAddr = currAddr+givenAddr[i];
				}
				skipNext = false;
			}
		}
		return currAddr;
	}

    function getShippingMethods(props, cbk) {
        var addressValidationUrl = (GLOBAL_CONFIG && GLOBAL_CONFIG.URL_ORCHESTRATAION ? GLOBAL_CONFIG.URL_ORCHESTRATAION : '') + 
          '/subscriptions/calculateshipping';
        var collectionId = Util.queryValues && Util.queryValues.collectionId ? Util.queryValues.collectionId : "";
        addressValidationUrl = Util.appendBasicParam(addressValidationUrl, collectionId); 
        addressValidationUrl = addressValidationUrl + "&validationType=ESTIMATE";

        var reqconfig = {};
        /*reqconfig.data = 
        {
            "addressInfo": props
        };*/
        reqconfig.data = {
        	  "deliveryAddress": props,
        	  "deliveryType": "PHYSICAL",
        	  "itemDetails": {
        	    "shippingClass": "DEFAULT",
        	    "skuId": "string",
        	    "skuName": "string",
        	    "skuQuantity": 0
        	  }
        	}
        var config = {
            url : addressValidationUrl,
            data : JSON.stringify(reqconfig.data),
            method: "POST",
            contentType: "application/json"
        }
        
        var callBack = function(response, status)
        {
            if (status == "error") {
                var message = (response ? (response.responseMessage ? response.responseMessage : (response.responseJSON && response.responseJSON.responseMessage ? response.responseJSON.responseMessage : 'Something went wrong.')) : 'Something went wrong.')
                Util.toast(message, null , status);
            }

            if(status == "success") {
              if(response && response.shippingDetails){
                if(response.shippingDetails.cartShippingMethods){
                  var shippingMethods = response.shippingDetails.cartShippingMethods;
                  //ModalManager.launch({type:'shippingMethods', shippingMethods:shippingMethods, givenAddr:props});
                  var addrValidatedMsg = "Address Validated - Choose an available shipping method";
                  Util.toast(addrValidatedMsg, null, status);
                  $('.cls_subscription_shipping_Method').addClass('editable z-depth-4');
                  $('.cls_subscription_shipping_Method .save-btn').removeAttr("disabled");
                  $('.cls_shippingAddrData').data("addr", props);
                  $(".cls_subscriptionShippingMtd").html(constructAvailableShippingMtds(shippingMethods));
                }
              }
            }

            if(cbk)
            {
                cbk(response, arguments);
            }
        };
        
        Util.getData(config, callBack);
      }

    function isLastRow(index, listLength){
      var val1 = index;
      var val2 = listLength;
      if(index < listLength){
          val1 = listLength;
          val2 = index;
      }

      if(((val1-1) == val2) || ((val2-1) == val1) ){
          return true;
      }
      return false;
    }

    function constructAvailableShippingMtds(shippingMethods){
  	  var recommendationHTML = [];
  	  recommendationHTML.push('<div class="pb-1"><b>Available Shipping Methods</b></div>');
  	  recommendationHTML.push('<div class="cls_ShippingMethodList" style="max-height: 240px; overflow: auto; -webkit-overflow-scrolling: touch;">');
  		for(var i=0; i<shippingMethods.length; i++){
  			recommendationHTML.push('<div class="col-12">');
  			if(i==0){
  				recommendationHTML.push('<input type="radio" name="shippingValue" id="idSelectedMethod'+i+'" class="form-check-input h5" value='+shippingMethods[i].shippingMethodId+' checked="checked">');
  			}
  			else{
  				recommendationHTML.push('<input type="radio" name="shippingValue" id="idSelectedMethod'+i+'" class="form-check-input h5" value='+shippingMethods[i].shippingMethodId+'>');
  			}
  			recommendationHTML.push('<label class="form-check-label" for="idSelectedMethod'+i+'">'+shippingMethods[i].shippingMethodId+'</label>');
  			recommendationHTML.push('</div>');
  		}
  		recommendationHTML.push('</div>');
  		return recommendationHTML;
    }

	function handleFreezeOption(config){
       $("#primaryModal .btn-primary").click(function(){
    	   var selectedAddrId = $('input[name="addrRecommendationValue"]:checked').val();
    	   $('.cls_subscription_shipping_Method').addClass('editable z-depth-4');
    	   $('.cls_subscription_shipping_Method .save-btn').removeAttr("disabled");
    	   //fillBillingAddressFromShippingAddress($("#"+selectedAddrId));
    	   var addrValidatedMsg = "Address Validated - Choose an available shipping method";
    	   Util.toast(addrValidatedMsg, null, status);
    	   var addrData = constructShippingAddr($("#"+selectedAddrId));
    	   var cbk = {};
    	   getShippingMethods(addrData, cbk);
       });
    }

	function render(config){
		Promise.all([
			TemplateManager.getTemplate('modal-confirm')
		])
		.then(function(response){
		    var confirmConfig = {
    	      title: "Invalid Address",
    	      message: getRecommendationContent(config),
    	      cancelLabel: "Cancel",
    	      confirmLabel: "Apply"
    	    }
		    
			ModalManager.setContent(response[0](confirmConfig))
			$('.mdb-select:not(.initialized)',ModalManager.content()).materialSelect();
		}).
		finally(function(response){
		    handleFreezeOption(config);
		})
	}
	
	return {
		defaultConfig:defaultConfig,
		render:render,
		handleSubmit:handleSubmit,
		delaySubmit:delaySubmit
	}
})())