ModalManager.setHandler('shippingMethods',(function(){
	var defaultConfig = {
		style:{
			size:'md'
		}
	}
	
	function delaySubmit(form,instant){
		//to do
	}
	
	function handleSubmit(form){
		//to do
	}
	
    function editSubscriptionDetails(modelConfig) {
        var updateUrl = (GLOBAL_CONFIG && GLOBAL_CONFIG.URL_ORCHESTRATAION ? GLOBAL_CONFIG.URL_ORCHESTRATAION : '') + 
          '/subscriptions/' + Util.getParameterByName("subscriptionId", "");
        var collectionId = Util.queryValues && Util.queryValues.collectionId ? Util.queryValues.collectionId : "";
        updateUrl = Util.appendBasicParam(updateUrl, collectionId);
        updateUrl = updateUrl + "&userInfo=" + ((Util.queryValues && Util.queryValues.userId) ? Util.queryValues.userId : "");

        var config = {
            url : updateUrl,
            data : JSON.stringify(modelConfig),
            method: "PATCH",
            contentType: "application/json"
        }
        
        var callBack = function(response, status)
        {
            var message =  message_properties["SUBSCRIPTION.UPDATE.SUCCESS.MSG"];
            if (status == "error") {
                message = (response ? (response.responseMessage ? response.responseMessage : (response.responseJSON && response.responseJSON.responseMessage ? response.responseJSON.responseMessage : 'Something went wrong.')) : 'Something went wrong.')
            }

            Util.toast(message, null , status);
            if(status == "success") {
                setTimeout(function() {
                    location.reload();
                }, 5000);
            }

            if(cbk)
            {
                cbk(response, arguments);
            }
        };
        
        Util.getData(config, callBack);
      }

	function getShippingMethodsContent(config){
		$('.cls_subscription_shipping_Info').addClass('editable z-depth-4');

		var shippingMethods = config.shippingMethods;
		var recommendationHTML = [];
		recommendationHTML.push('<div class="pb-1"><b>Available Shipping Methods</b></div>');
			recommendationHTML.push('<div class="cls_ShippingMethodList" style="max-height: 240px; overflow: auto; -webkit-overflow-scrolling: touch;">');
			for(var i=0; i<shippingMethods.length; i++){
				recommendationHTML.push('<div class="col-12">');
					recommendationHTML.push('<div class="Xborder-bottom" role="alert">');
						recommendationHTML.push('<div class="">');
						if(i==0){
							recommendationHTML.push('<input type="radio" name="shippingValue" id="idSelectedMethod'+i+'" class="form-check-input h5" value='+shippingMethods[i].shippingMethodId+' checked="checked">');
						}
						else{
							recommendationHTML.push('<input type="radio" name="shippingValue" id="idSelectedMethod'+i+'" class="form-check-input h5" value='+shippingMethods[i].shippingMethodId+'>');
						}
						recommendationHTML.push('<label class="form-check-label " for="idSelectedMethod'+i+'">');
						recommendationHTML.push(shippingMethods[i].shippingMethodId);
						recommendationHTML.push('</label>');
						
						recommendationHTML.push('</div>');
					recommendationHTML.push('</div>');
				recommendationHTML.push('</div>');
			}
			recommendationHTML.push('</div>');
		return recommendationHTML.join("");
	}

	function handleFreezeOption(config){
       $("#primaryModal .btn-primary").click(function(){
    	   var selectedShippingMethod = $('input[name="shippingValue"]:checked').val();
    	   var modelConfig = {};
    	   var shippingInfo = {};
    	      shippingInfo["method"] = selectedShippingMethod;
    	      shippingInfo["deliveryPeriod"] = 1;
    	   modelConfig["shippingInfo"] = shippingInfo;
		   modelConfig["addressInfo"] = config.givenAddr;

    	   editSubscriptionDetails(modelConfig);
       });
    }

	function render(config){
		Promise.all([
			TemplateManager.getTemplate('modal-confirm')
		])
		.then(function(response){
		    var confirmConfig = {
    	      title: "Choose a shipping method",
    	      message: getShippingMethodsContent(config),
    	      cancelLabel: "Cancel",
    	      confirmLabel: "Apply"
    	    }
		    
			ModalManager.setContent(response[0](confirmConfig))
			$('.mdb-select:not(.initialized)',ModalManager.content()).materialSelect();
		}).
		finally(function(response){
		    handleFreezeOption(config);
		})
	}
	
	return {
		defaultConfig:defaultConfig,
		render:render,
		handleSubmit:handleSubmit,
		delaySubmit:delaySubmit
	}
})())