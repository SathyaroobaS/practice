function SubscriptionUtil() {
    
}
SubscriptionUtil.getParams = function(url) {
    let search = new URLSearchParams(url).get("filters");
    let searchParams = search ? search.split("|") : [];
    var allSearchParams = {};
    var searchParam = [];
    for (var i = 0; i < searchParams.length; i++) {
      searchParam = searchParams[i].split(":");
      if(searchParams.length) {
          allSearchParams[searchParam[0]] = searchParam[1] ? searchParam[1] : '';
      }
    }
    return allSearchParams;
}

SubscriptionUtil.getSearchParams = function(url) {
    var _this = this;
    var params = _this.getParams(url);
    var search = '';
    var skuIdParam = '';
    if(params) {
        for(var key in params) {
            if(key != 'fromsearch') {
                if(key != 'fromDate' && key != 'toDate') {
                    if(key == 'skuName') {
                        search = constructSearchParam(search, key, ('*' + params[key] + '*'));
                    } else if(key == 'skuId') {
                        skuIdParam = key + '=\"' + params[key] + '\"';
                    } else {
                        search = constructSearchParam(search, key, params[key]);
                    }
                } else {
                    var timeArr = [];
                    var timeRangeArr = [];
                    params['fromDate'] ? timeRangeArr.push(params['fromDate']) : null;
                    params['toDate'] ? timeRangeArr.push(params['toDate']) : null;
                    if(search.indexOf('createdTime') == -1) {
                        if(timeRangeArr.length >= 2) {
                            timeArr.push(timeRangeArr[0]);
                            timeArr.push(timeRangeArr[1]);
                            search += (search != '' ? ' AND ' : '') + ('createdTime BETWEEN ' + JSON.stringify(timeArr));
                        }
                    }
                }
            }
        }
        if(skuIdParam) {
            search = search != '' ? search + (' AND ' + skuIdParam) : search + skuIdParam;
        }
    }
    return search;
}

SubscriptionUtil.getFrequencyType = function(frequencyType){
    return message_properties["SUBSCRIPTION.FREQUENCY."+frequencyType] ? message_properties["SUBSCRIPTION.FREQUENCY."+frequencyType] : "";
}

SubscriptionUtil.addParamsToURL = function (url, config, doNotEncode) {
  for ( var key in config) {
    if (config.hasOwnProperty (key)) {
      if (url && key && config[key] != undefined && config[key] != null && config[key] != "null" && config[key] != "") {
        var prefix = (url.indexOf ('?') >= 0 ? '&' : '?');
        url = url.concat (prefix, key, '=', (doNotEncode ? (config[key]) : encodeURIComponent (config[key]).replace (/'/g, "%27").replace (/"/g, "%22")));
      }
    }
  }
  return url;
};