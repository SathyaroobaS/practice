function getParams(key) {
  let search = new URLSearchParams(window.location.href).get("search");
  let searchParams = search ? search.split(",") : [];
  var allSearchParams = {};
  var searchParam = [];
  for (var i = 0; i < searchParams.length; i++) {
    searchParam = searchParams[i].split(":");
    allSearchParams[searchParam[0]] = searchParam[1];
  }
  return allSearchParams;
}

var params = getParams();
var dateRange = params.createdTime ? params.createdTime.split('to') : [];
var queryParams = Util.getQueryParam();
var dateFormat = "YYYY-MM-DD HH:mm:ss";

function getDatePickerValue() {
    var fromDate = (params.fromDate ? moment(parseInt(params.fromDate)).tz(moment.tz.guess()).format(dateFormat): '');
    var toDate = (params.toDate ? moment(parseInt(params.toDate)).tz(moment.tz.guess()).format(dateFormat): '');
    if(fromDate && toDate) {
        return fromDate + ' to ' + toDate;
    }
    return '';
}

SEARCH_CONFIG['subscriptions'] = {
  "ignoreHistory" : true,
  "searchElements" : {
    "id" : {
      "key" : "id",
      "label" : message_properties["SUBSCRIPTION.REQUESTID"],
      "type" : "text",
      "default" : true,
      "value" : params.id,
      "attributes" : {
        "data-qa-select" : "requestId-search",
        "data-qa" : "requestId-search"
      }
    },
    "email" : {
      "key" : "email",
      "label" : message_properties["SUBSCRIPTION.EMAIL"],
      "type" : "text",
      "default" : true,
      "value" : (queryParams && queryParams.email ? queryParams.email : ""),
      "attributes" : {
        "data-qa-select" : "emailId-search",
        "data-qa" : "emailId-search"
      }
    },
    "createdTime" : {
        "key" : "createdTime",
        "label" : message_properties["SUBSCRIPTION.REQUEST_PLACEMENT_DATE"],
        "type" : "date-range",
        "format": dateFormat,
        "default" : false,
        "fromDate": (params.fromDate ? moment(parseInt(params.fromDate)).tz(moment.tz.guess()).format(dateFormat): moment().format(dateFormat)),
        "toDate": (params.toDate ? moment(parseInt(params.toDate)).tz(moment.tz.guess()).format(dateFormat): moment().format(dateFormat)),
        "value": getDatePickerValue(),
        "attributes" : {
          "data-qa-select" : "requestplacementdate-search",
          "data-qa" : "requestplacementdate-search"
        }
    },
    /*"skuId" : {
      "key" : "skuId",
      "label" : message_properties["SUBSCRIPTION.SKUID"],
      "type" : "text",
      "default" : true,
      "value" : params.skuId,
      "attributes" : {
        "data-qa-select" : "skuId-search",
        "data-qa" : "skuId-search",
      }
    },*/
    "skuName" : {
        "key" : "skuName",
        "label" : message_properties["SUBSCRIPTION.PRODUCTNAME"],
        "type" : "text",
        "default" : true,
        "value" : params.skuName,
        "attributes" : {
          "data-qa-select" : "skuname-search",
          "data-qa" : "skuname-search",
        }
      },
      "status" : {
          "key" : "status",
          "label" : message_properties["SUBSCRIPTION.STATUS"],
          "type" : "select",
          "default" : true,
          "value" : params.status,
          "options" : [ {
            "key" : "ACTIVE",
            "label" : message_properties["SUBSCRIPTION.STATUS.ACTIVE"]
            },{
            "key" : "CANCELED",
            "label" : message_properties["SUBSCRIPTION.STATUS.CANCELED"]
            }],
          "attributes" : {
            "data-qa-filter" : "subscription-status-filter",
            "data-qa-select" : "subscription-status-list",
            "data-qa" : "subscription-status-input",
            "data-qa-checkbox" : "subscription-status-checkbox"
          }
        }
  },
  "searchCbk" : function(jsonData) {
    var skhref = Util.appendBasicParam(window.location.pathname);
    var searchParams = [];
    var emailSearchParam = "";
    var url = skhref;
    for ( var key in jsonData) {
      if (jsonData[key] && jsonData[key] != "undefined") {
        if(key == 'email') {
            emailSearchParam = jsonData[key];
        } else if(key == 'createdTime'){
            var dateRange = jsonData[key] ? jsonData[key].split('to') : [];
            var fromDate = moment(dateRange[0].trim()).utc().valueOf();
            var toDate = moment(dateRange[1].trim()).utc().valueOf();
            searchParams.push('fromDate' + ":" + fromDate);
            searchParams.push('toDate' + ":" + toDate);
        } else {
            searchParams.push(key + ":" + jsonData[key]);
        }
      }
    }
    if(searchParams.length) {
        url = Util.addNewParam("search", searchParams.join(','), url);
    }
    if(emailSearchParam) {
        url = Util.addNewParam("email", emailSearchParam, url);
    }
    window.location.href = url;
  },
  "resetCbk" : function() {
    var skhref = Util.appendBasicParam(window.location.pathname);
    window.location.href = Util.removeParams("search", skhref);
  }
};