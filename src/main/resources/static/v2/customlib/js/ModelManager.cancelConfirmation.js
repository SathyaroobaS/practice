ModalManager.setHandler('cancelSubscriptionConfirmation',(function(){
    var defaultConfig = {
        style:{
            size:'md'
        }
    }
    function cancelSubscription(subscriptionId, userId) {
        var table = $("[datatable=subscriptions]")[0];
        var tableLoader = new LoadBlock({target:table});
        var url = (GLOBAL_CONFIG && GLOBAL_CONFIG.URL_ORCHESTRATAION ? GLOBAL_CONFIG.URL_ORCHESTRATAION : '') + 
        '/subscriptions/' + subscriptionId + '/cancel';
        var collectionId = Util.queryValues && Util.queryValues.collectionId ? Util.queryValues.collectionId : "";
        var cancelUrl = Util.appendBasicParam(url, collectionId);
        var data = {
            status: 'CANCELED',
            userId: userId
          };
          var ajaxConfig = {
            url: cancelUrl,
            method: 'PATCH',
            data : JSON.stringify(data),
            contentType : "application/json",
            crossDomain : true
          }
          
          var cancelCbk = function (response, status) {
              var message =  message_properties["SUBSCRIPTION.CANCEL.SUCCESS.MSG"];
              if (status == "error") {
                  tableLoader.destroy();
                  message = (response ? (response.responseMessage ? response.responseMessage : (response.responseJSON && response.responseJSON.responseMessage ? response.responseJSON.responseMessage : 'Something went wrong.')) : 'Something went wrong.')
              }
              Util.toast(message, null , status);
              if(status == "success") {
                  setTimeout(function() {
                      location.reload();
                  }, 5000);
              }
            };
            
          Util.getData(ajaxConfig, cancelCbk);
    }
    function handleCancelSubscription(config){
       $("#primaryModal .btn-primary").click(function(){
           cancelSubscription(config.id, config.userId);
       });
    }

    function render(config){
        Promise.all([
            TemplateManager.getTemplate('modal-confirm')
        ])
        .then(function(response){
            var confirmConfig = {
              title: message_properties["SUBSCRIPTION.CANCEL.TITLE"],
              message: message_properties["SUBSCRIPTION.CANCEL.MESSAGE"],
              cancelLabel: message_properties["SUBSCRIPTION.BTN.CANCEL"],
              confirmLabel: message_properties["SUBSCRIPTION.BTN.CONFIRM"],
              dataqa: "subscription-cancel"
            }
            
            ModalManager.setContent(response[0](confirmConfig))
            $('.mdb-select:not(.initialized)',ModalManager.content()).materialSelect();
        }).
        finally(function(response){
            handleCancelSubscription(config);
        })
    }
    
    return {
        defaultConfig:defaultConfig,
        render:render
    }
})())