FormManager.setHandler('edit-subscription-details',(function(){
    
    function onSubmit(evt, info){
        if (info.form.checkValidity()) {
            var props = FormManager.objectify(info.form)
                        
            var cbk = function() {
                FormManager.cancel(info.form);
                loader.destroy();
            }
            console.log(props);
            editSubscriptionDetails(props, cbk);
            
            var loader = new LoadBlock({target:info.form})
        } else {
            info.form.classList.add('was-validated');
        }
        
        evt.preventDefault();
        evt.stopPropagation();
        
        return false;
    }
    
    function editSubscriptionDetails(props, cbk) {
      var updateUrl = (GLOBAL_CONFIG && GLOBAL_CONFIG.URL_ORCHESTRATAION ? GLOBAL_CONFIG.URL_ORCHESTRATAION : '') + 
        '/subscriptions/' + Util.getParameterByName("subscriptionId", "");
      var collectionId = Util.queryValues && Util.queryValues.collectionId ? Util.queryValues.collectionId : "";
      updateUrl = Util.appendBasicParam(updateUrl, collectionId);
      updateUrl = updateUrl + "&userInfo=" + Util.getParameterByName("userId", "");

      var shippingInfo = {};
      shippingInfo["method"] = props.shippingMethod;
      shippingInfo["deliveryPeriod"] = 1;
      var itemInfo = [];
      itemInfo[0] = {};
      itemInfo[0]["quantity"] = props.subscriptionQuantity;
      itemInfo[0]["skus"] = [];
      itemInfo[0]["skus"].push({"type" : "DEFAULT"});

      var reqconfig = {};
      reqconfig.data = 
      {
          "itemInfo": itemInfo,
          "shippingInfo": shippingInfo,
          "frequencyPeriod": $('#idSubsFrequencyPeriod').val()
      };

      var config = {
          url : updateUrl,
          data : JSON.stringify(reqconfig.data),
          method: "PATCH",
          contentType: "application/json"
      }
      
      var callBack = function(response, status)
      {
          var message =  message_properties["SUBSCRIPTION.UPDATE.SUCCESS.MSG"];
          if (status == "error") {
              message = (response ? (response.responseMessage ? response.responseMessage : (response.responseJSON && response.responseJSON.responseMessage ? response.responseJSON.responseMessage : 'Something went wrong.')) : 'Something went wrong.')
          }

          Util.toast(message, null , status);
          if(status == "success") {
              setTimeout(function() {
                  location.reload();
              }, 5000);
          }

          if(cbk)
          {
              cbk(response, arguments);
          }
      };
      
      Util.getData(config, callBack);
    }

    return {
        onSubmit:onSubmit
    }
    
})())