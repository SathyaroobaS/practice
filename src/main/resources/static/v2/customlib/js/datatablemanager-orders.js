function getParams(key) {
    let search = new URLSearchParams(window.location.href).get("search");
    let searchParams = search ? search.split(",") : [];
    var allSearchParams = {};
    var searchParam = [];
    for (var i = 0; i < searchParams.length; i++) {
      searchParam = searchParams[i].split(":");
      allSearchParams[searchParam[0]] = searchParam[1];
    }
    return allSearchParams;
}

var subscriptionOrdersUrl = (GLOBAL_CONFIG && GLOBAL_CONFIG.URL_ORCHESTRATAION ? GLOBAL_CONFIG.URL_ORCHESTRATAION : '') + '/subscriptions/';
subscriptionOrdersUrl = subscriptionOrdersUrl + Util.getParameterByName("subscriptionId", "") + '/orders';

function constructSearchParam(searchParam, key, value) {
    return searchParam + (searchParam != '' ? ' AND ' : '') + (key + '=\"' + value + '\"');
}
var params = getParams();
var paramsConfig = {};
paramsConfig["userInfo"] = Util.getParameterByName("userId", "");
paramsConfig["storeId"] = Util.getParameterByName("storeId", "");
paramsConfig["businessId"] = Util.getParameterByName("businessId", "");
paramsConfig["collectionId"] = Util.getParameterByName("collectionId", "");
subscriptionOrdersUrl = SubscriptionUtil.addParamsToURL(subscriptionOrdersUrl, paramsConfig);

DataTablesManager.setHandler('orders', {
  language : {
    emptyTable : "<center class='alert alert-warning p-4'><h6>"+  ( Util.getParameterByName("search", "") ? 'No result found.' : 'No data found.') +"</h6></center>"
  },  
  ajax : {
    contentType : 'application/json',
    url : subscriptionOrdersUrl,
    jsonp : true,
    dataSrc : function(response) {
      response.draw = response.draw;
      if (response) {
        var count = (response.instances ? response.instances.length : 0);
        var sizeVal = 10;
        var page = parseInt((Util.getParameterByName("page", "") ? Util.getParameterByName("page", "") : 1));
        var endVal = ((page*sizeVal) > count) ? count : (page*sizeVal);
        page = (sizeVal * (page - 1));
        response.instances = response.instances ? response.instances.slice(page, endVal) : response.instances;
        response["recordsTotal"] = count;
        response["recordsFiltered"] = count;
      }
      return (response["instances"] || []);
  },
    data : function(dataTableParam) {
      var paramData = {};
      paramData["draw"] = dataTableParam.draw;
      paramData["page"] = (dataTableParam.start / dataTableParam.length) + 1;
      paramData["size"] = DataTablesManager.getLimit();
      if (dataTableParam.order && dataTableParam.order.length) {
        var orderData = dataTableParam.order;
        var columnsData = dataTableParam.columns;
        var sortArray = {"sort": []};
        for (var i = 0; i < orderData.length; i++) {
            var fieldVal = columnsData[orderData[i].column].data;
            switch(fieldVal){
            case "requestid" : fieldVal = "id";
              break;
            case "requestplaceddate" : fieldVal = "createdTime";
              break;
            case "frequency" : fieldVal = "frequencyType";
              break;
            }

            var dirVal = (orderData[i].dir).toUpperCase();
            var sortField = {
              "field": fieldVal,
              "direction": dirVal
            };

            sortArray["sort"].push(sortField);

            if(sortArray["sort"].length) {
                paramData["sort"] = JSON.stringify(sortArray);
            }
        }
      }

      var search = '';
      if(params) {
          for(var key in params) {
              if(key != 'fromsearch') {
                  search = constructSearchParam(search, key, params[key]);
              }
          }
      }
      if(search.length) {
          paramData['search'] = search;
      }

      var defaultParam = DataTablesManager.getDefaultParam.apply(this, arguments);
      $.extend(true, paramData, defaultParam);

      return paramData;
    },
    xhrFields : {
      withCredentials : true
    }
  },
  columns : [
    {
        data: "instanceId", 
        title: message_properties["SUBSCRIPTION.INSTANCE.ID"],
        dataQA : "subscription-id-trigger",
        render: function(data, type, row) {
            return row.instanceId;
        }
    },
    {
        data: "orderId", 
        title: message_properties["SUBSCRIPTION.ORDER.ID"],
        dataQA : "subscription-id-trigger",
        render: function(data, type, row) {
            return row.orderId;
        }
    },
    {
        data: "expectedDeliveryDate", 
        title: message_properties["SUBSCRIPTION.INSTANCE.EXPECTEDDELIVERYDATE"], 
        dataQA : "subscription-createdon-trigger",
        render: function(data, type, row) {
            return row.expectedDeliveryDate ? DateUtil.getStandardDateTimeFormat(row.expectedDeliveryDate, null, true) : '';
        }
    },
    {
        data: "status", 
        title: message_properties["SUBSCRIPTION.INSTANCE.STATUS"],
        dataQA : "subscription-id-trigger",
        render: function(data, type, row) {
            return Util.renderBadgeByStatus(row.status);
        }
    }
  ],
  columnDefs : [{
      "targets" : ["_all"],
      "orderable": false,
      "createdCell" : function(td, cellData, rowData, row, col) {
         var qaAttributes = ["subscription-id", "subscription-name", "subscription-eventname"];
         $(td).attr("data-qa", qaAttributes[col]);
      }
  }]
});
