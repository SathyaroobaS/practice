function getParams(key) {
  let search = new URLSearchParams(window.location.href).get("search");
  let searchParams = search ? search.split(",") : [];
  var allSearchParams = {};
  var searchParam = [];
  for (var i = 0; i < searchParams.length; i++) {
    searchParam = searchParams[i].split(":");
    allSearchParams[searchParam[0]] = searchParam[1];
  }
  return allSearchParams;
}

var params = getParams();
var dateRange = params.createdTime ? params.createdTime.split('to') : [];
var queryParams = Util.getQueryParam();
var dateFormat = "YYYY-MM-DD HH:mm:ss";

function getDatePickerValue() {
    var fromDate = (params.fromDate ? moment(parseInt(params.fromDate)).tz(moment.tz.guess()).format(dateFormat): '');
    var toDate = (params.toDate ? moment(parseInt(params.toDate)).tz(moment.tz.guess()).format(dateFormat): '');
    if(fromDate && toDate) {
        return fromDate + ' to ' + toDate;
    }
    return '';
}

SEARCH_CONFIG['orders'] = {
  "ignoreHistory" : true,
  "searchElements" : {
    "instanceId" : {
      "key" : "instanceId",
      "label" : message_properties["SUBSCRIPTION.INSTANCE.ID"],
      "type" : "text",
      "default" : true,
      "value" : params.instanceId,
      "attributes" : {
        "data-qa-select" : "requestId-search",
        "data-qa" : "requestId-search"
      }
    },
    "orderId" : {
        "key" : "orderId",
        "label" : message_properties["SUBSCRIPTION.ORDER.ID"],
        "type" : "text",
        "default" : true,
        "value" : params.orderId,
        "attributes" : {
          "data-qa-select" : "skuname-search",
          "data-qa" : "skuname-search",
        }
      },
      "status" : {
          "key" : "status",
          "label" : message_properties["SUBSCRIPTION.STATUS"],
          "type" : "select",
          "default" : true,
          "value" : params.status,
          "options" : [ {
            "key" : "COMPLETED",
            "label" : message_properties["SUBSCRIPTION.INSTANCE.STATUS.COMPLETED"]
            },{
            "key" : "SKIPPED",
            "label" : message_properties["SUBSCRIPTION.INSTANCE.STATUS.SKIPPED"]
            },{
            "key" : "FAILED",
            "label" : message_properties["SUBSCRIPTION.INSTANCE.STATUS.FAILED"]
            }],
          "attributes" : {
            "data-qa-filter" : "subscription-status-filter",
            "data-qa-select" : "subscription-status-list",
            "data-qa" : "subscription-status-input",
            "data-qa-checkbox" : "subscription-status-checkbox"
          }
        }
  },
  "searchCbk" : function(jsonData) {
    var skhref = Util.appendBasicParam(window.location.pathname);

    var paramsConfig = {};
    paramsConfig["subscriptionId"] = Util.getParameterByName("subscriptionId", "");
    paramsConfig["userId"] = Util.getParameterByName("userId", "");

    skhref = SubscriptionUtil.addParamsToURL(skhref, paramsConfig);

    var searchParams = [];
    var emailSearchParam = "";
    var url = skhref;
    for ( var key in jsonData) {
      if (jsonData[key] && jsonData[key] != "undefined") {
        if(key == 'email') {
            emailSearchParam = jsonData[key];
        } else if(key == 'createdTime'){
            var dateRange = jsonData[key] ? jsonData[key].split('to') : [];
            var fromDate = moment(dateRange[0].trim()).utc().valueOf();
            var toDate = moment(dateRange[1].trim()).utc().valueOf();
            searchParams.push('fromDate' + ":" + fromDate);
            searchParams.push('toDate' + ":" + toDate);
        } else {
            searchParams.push(key + ":" + jsonData[key]);
        }
      }
    }
    if(searchParams.length) {
        url = Util.addNewParam("search", searchParams.join(','), url);
    }
    if(emailSearchParam) {
        url = Util.addNewParam("email", emailSearchParam, url);
    }

    if(window.location.hash != null){
        url = url + window.location.hash;
    }

    window.location.href = url;
  },
  "resetCbk" : function() {
    var skhref = Util.appendBasicParam(window.location.pathname);
    var paramsConfig = {};
    paramsConfig["subscriptionId"] = Util.getParameterByName("subscriptionId", "");
    paramsConfig["userId"] = Util.getParameterByName("userId", "");
    skhref = SubscriptionUtil.addParamsToURL(skhref, paramsConfig);

    if(window.location.hash != null){
    	skhref = skhref + window.location.hash;
    }
    window.location.href = Util.removeParams("search", skhref);
  }
};