var createPriceList = (function() {
  function init(){
    registerEvents();
  }

  function registerEvents(){
      $('.cls_skcancelsubscription').off('click').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          ModalManager.launch({type:'cancelSubscriptionConfirmation', id: Util.getParameterByName("subscriptionId", ""), userId: Util.getParameterByName("userId", "")});
      });

      $(".cls_skeditsubscriptionmtd").off("click").on("click", function(){
    	  var serializedForm = FormManager.objectify($('.cls_subscription_shipping_Info'));
    	  getShippingMethods(serializedForm);
      });

      $(".cls_skAddrSuggDropItem").off("click").on("click", function(){
          fillBillingAddressFromShippingAddress($(this));
      });

      $('input[type=radio][name=shippingAddrType]').change(function() {
            if (this.value == 'Defined') {
                $('.cls_customShippingAddress').addClass("d-none");
                $('.cls_definedShippingAddress').removeClass("d-none");
            }
            else if (this.value == 'Custom') {
                $('.cls_definedShippingAddress').addClass("d-none");
                $('.cls_customShippingAddress').removeClass("d-none");
            }
        });

      populateShippingMethod();
  }

  function populateShippingMethod(){
      var serializedForm = FormManager.objectify($('.cls_subscription_shipping_Info'));
      getShippingMethods(serializedForm);
  }

  function fillBillingAddressFromShippingAddress(elem) {
	    var parEle = $(".cls_subscription_shipping_Info");
	    var selectedElem;
	    var fillMapping = {
	        "firstName" : "cls_ChoosePaymentFName",
	        "lastName" : "cls_ChoosePaymentLName",
	        "addressLine1" : "cls_ChoosePaymentStreet",
	        "addressLine2" : "cls_ChoosePaymentStreet2",
	        "city" : "cls_ChoosePaymentCity",
	        "state" : "cls_ChoosePaymentState",
	        "country" : "cls_ChoosePaymentCountry",
	        "zip" : "cls_ChoosePaymentZip",
	        "phone" : "cls_ChoosePaymentPhone"
	    }

	    $.each(fillMapping, function(key, value) {
	        selectedElem = $(parEle).find("input[name=" + key + "]");
	        selectedElem.val(elem.find("." + value).text());
	        selectedElem.siblings().addClass("active");
	        selectedElem.focus().blur();
	    });
	}

  function constructAvailableShippingMtds(shippingMethods){
	  var recommendationHTML = [];
	  recommendationHTML.push('<div class="pb-1"><b>Available Shipping Methods</b></div>');
	  recommendationHTML.push('<div class="cls_ShippingMethodList" style="max-height: 240px; overflow: auto; -webkit-overflow-scrolling: touch;">');
		for(var i=0; i<shippingMethods.length; i++){
			recommendationHTML.push('<div class="col-12">');
			if(i==0){
				recommendationHTML.push('<input type="radio" name="shippingValue" id="idSelectedMethod'+i+'" class="form-check-input h5" value='+shippingMethods[i].shippingMethodId+' checked="checked">');
			}
			else{
				recommendationHTML.push('<input type="radio" name="shippingValue" id="idSelectedMethod'+i+'" class="form-check-input h5" value='+shippingMethods[i].shippingMethodId+'>');
			}
			recommendationHTML.push('<label class="form-check-label" for="idSelectedMethod'+i+'">'+shippingMethods[i].shippingMethodId+'</label>');
			recommendationHTML.push('</div>');
		}
		recommendationHTML.push('</div>');
		return recommendationHTML;
  }

  function getShippingMethods(props, cbk) {
      var addressValidationUrl = (GLOBAL_CONFIG && GLOBAL_CONFIG.URL_ORCHESTRATAION ? GLOBAL_CONFIG.URL_ORCHESTRATAION : '') + 
        '/subscriptions/calculateshipping';
      var collectionId = Util.queryValues && Util.queryValues.collectionId ? Util.queryValues.collectionId : "";
      addressValidationUrl = Util.appendBasicParam(addressValidationUrl, collectionId); 
      addressValidationUrl = addressValidationUrl + "&validationType=ESTIMATE";

      var reqconfig = {};
      /*reqconfig.data = 
      {
          "addressInfo": props
      };*/
      reqconfig.data = {
      	  "deliveryAddress": props,
      	  "deliveryType": "PHYSICAL",
      	  "itemDetails": {
      	    "shippingClass": "DEFAULT",
      	    "skuId": "string",
      	    "skuName": "string",
      	    "skuQuantity": 0
      	  }
      	}
      var config = {
          url : addressValidationUrl,
          data : JSON.stringify(reqconfig.data),
          method: "POST",
          contentType: "application/json"
      }
      
      var callBack = function(response, status)
      {
          if (status == "error") {
              var message = (response ? (response.responseMessage ? response.responseMessage : (response.responseJSON && response.responseJSON.responseMessage ? response.responseJSON.responseMessage : 'Something went wrong.')) : 'Something went wrong.')
              Util.toast(message, null , status);
          }

          if(status == "success") {
            if(response && response.shippingDetails){
              if(response.shippingDetails.cartShippingMethods){
                var shippingMethods = response.shippingDetails.cartShippingMethods;
                //ModalManager.launch({type:'shippingMethods', shippingMethods:shippingMethods, givenAddr:props});
                $(".cls_subscriptionShippingMtd").html(constructAvailableShippingMtds(shippingMethods));
              }
            }
          }

          if(cbk)
          {
              cbk(response, arguments);
          }
      };
      
      Util.getData(config, callBack);
    }

  $(document).ready(function(){
    init();
  });
})()