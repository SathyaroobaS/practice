function getParams(key) {
    let search = new URLSearchParams(window.location.href).get("search");
    let searchParams = search ? search.split(",") : [];
    var allSearchParams = {};
    var searchParam = [];
    for (var i = 0; i < searchParams.length; i++) {
      searchParam = searchParams[i].split(":");
      allSearchParams[searchParam[0]] = searchParam[1];
    }
    return allSearchParams;
}
function constructSearchParam(searchParam, key, value) {
    return searchParam + (searchParam != '' ? ' AND ' : '') + (key + '=\"' + value + '\"');
}
var params = getParams();
var subscriptionUrl = (GLOBAL_CONFIG && GLOBAL_CONFIG.URL_ORCHESTRATAION ? GLOBAL_CONFIG.URL_ORCHESTRATAION : '') + '/subscriptions/';
var collectionId = Util.queryValues && Util.queryValues.collectionId ? Util.queryValues.collectionId : "";
var email = Util.queryValues && Util.queryValues.email ? Util.queryValues.email : "";
subscriptionUrl = Util.appendBasicParam(subscriptionUrl, collectionId);
if(email) {
    subscriptionUrl += '&email=' + email;
}
DataTablesManager.setHandler('subscriptions', {
  language : {
    emptyTable : "<center class='alert alert-warning p-4'><h6>"+  ( Util.getParameterByName("search", "") ? 'No result found.' : 'No data found.') +"</h6></center>"
  },  
  ajax : {
    contentType : 'application/json',
    url : subscriptionUrl,
    dataSrc : function(response) {
      response.draw = response.draw;
      if (response) {
        var count = (response.totalCount ? response.totalCount : 0);
        response["recordsTotal"] = count;
        response["recordsFiltered"] = count;
      }
      return (response["subscriptions"] || []);
    },
    data : function(dataTableParam) {
      var paramData = {};
      paramData["draw"] = dataTableParam.draw;
      paramData["page"] = (dataTableParam.start / dataTableParam.length) + 1;
      paramData["size"] = DataTablesManager.getLimit();
      if (dataTableParam.order && dataTableParam.order.length) {
        var orderData = dataTableParam.order;
        var columnsData = dataTableParam.columns;
        var sortArray = {"sort": []};
        for (var i = 0; i < orderData.length; i++) {
            var fieldVal = columnsData[orderData[i].column].data;
            switch(fieldVal){
            case "requestid" : fieldVal = "id";
              break;
            case "requestplaceddate" : fieldVal = "createdTime";
              break;
            case "frequency" : fieldVal = "frequencyType";
              break;
            }

            var dirVal = (orderData[i].dir).toUpperCase();
            var sortField = {
              "field": fieldVal,
              "direction": dirVal
            };

            sortArray["sort"].push(sortField);

            if(sortArray["sort"].length) {
                paramData["sort"] = JSON.stringify(sortArray);
            }
        }
      }
      var search = '';
      var skuIdParam = '';
      if(params) {
          for(var key in params) {
              if(key != 'fromsearch') {
                  if(key != 'fromDate' && key != 'toDate') {
                      if(key == 'skuName') {
                          search = constructSearchParam(search, key, ('*' + params[key] + '*'));
                      } else if(key == 'skuId') {
                          skuIdParam = key + '=\"' + params[key] + '\"';
                      } else {
                          search = constructSearchParam(search, key, params[key]);
                      }
                  } else {
                      var timeArr = [];
                      var timeRangeArr = [];
                      params['fromDate'] ? timeRangeArr.push(params['fromDate']) : null;
                      params['toDate'] ? timeRangeArr.push(params['toDate']) : null;
                      if(search.indexOf('createdTime') == -1) {
                          if(timeRangeArr.length >= 2) {
                              timeArr.push(timeRangeArr[0]);
                              timeArr.push(timeRangeArr[1]);
                              search += (search != '' ? ' AND ' : '') + ('createdTime BETWEEN ' + JSON.stringify(timeArr));
                          }
                      }
                  }
              }
          }
          if(skuIdParam) {
              search = search != '' ? search + (' AND ' + skuIdParam) : search + skuIdParam;
          }
      }
      if(search.length) {
          paramData['search'] = search;
      }

      var defaultParam = DataTablesManager.getDefaultParam.apply(this, arguments);
      $.extend(true, paramData, defaultParam);

      return paramData;
    },
    xhrFields : {
      withCredentials : true
    }
  },
  columns : [
    {
        data: "username", 
        title: message_properties["SUBSCRIPTION.USERNAME"], 
        dataQA : "subscription-name-trigger",
        render: function(data, type, row) {
            var userName = row.username ? row.username.trim() : "";
            return userName ? Util.encodeHtml(userName) : '-';
        }
    },
    {data: "requestid", title: message_properties["SUBSCRIPTION.REQUESTID"], dataQA : "subscription-id-trigger", sortable : true},
    {
        data: "email", 
        title: message_properties["SUBSCRIPTION.EMAIL"], 
        dataQA : "subscription-name-trigger",
        render: function(data, type, row) {
            var userEmail = row.email ? row.email.trim() : "";
            return userEmail ? Util.encodeHtml(userEmail) : '-';
        }
    },
    /*{
        data: "skuid",
        title: message_properties["SUBSCRIPTION.SKUID"],
        dataQA : "subscription-event-trigger",
        render: function(data, type, row) {
            return row.skuid ? Util.encodeHtml(row.skuid) : '-';
        }
    },*/
    {
        data: "skuname",
        title: message_properties["SUBSCRIPTION.PRODUCTNAME"],
        dataQA : "subscription-event-trigger",
        render: function(data, type, row) {
            return row.skuname ? Util.encodeHtml(row.skuname) : '-';
        }
    },
    {
        data: "requestplaceddate", 
        title: message_properties["SUBSCRIPTION.REQUEST_PLACEMENT_DATE"], 
        dataQA : "subscription-event-trigger",
        sortable : true,
        render: function(data, type, row) {
            return row.requestplaceddate ? DateUtil.getStandardDateTimeFormat(row.requestplaceddate, null, true) : '-';
        }
    },
    {
        data: "frequency", 
        title: message_properties["SUBSCRIPTION.FREQUENCY"],
        sortable : true,
        dataQA : "subscription-name-trigger",
        render: function(data, type, row) {
            return row.frequency ? SubscriptionUtil.getFrequencyType(row.frequency) : '-';
        }
    },
    {
        data: "status",
        title: message_properties["SUBSCRIPTION.STATUS"], 
        dataQA : "subscription-status",
        sortable : true,
        render: function(data, type, row) {
            return Util.renderBadgeByStatus(data);
        }
    }/*,
    {
        data: "userId",
        render: function(data, type, row) {
            if(row.status == 'ACTIVE') {
                return '<button class="btn btn-link p-2 m-0 rounded-0 shadow-none waves-effect cls_skcancelsubscription" data-id='+ row.requestid +' data-userId='+ data +'>'+ Util.getActionIcon('CANCEL', message_properties["SUBSCRIPTION.BTN.CANCEL"]) +'</button>';
            }
        }
    }*/
  ],
  columnDefs : [{
      "targets" : ["_all"],
      "orderable": false,
      "createdCell" : function(td, cellData, rowData, row, col) {
         var qaAttributes = ["subscription-id", "subscription-name", "subscription-eventname"];
         $(td).attr("data-qa", qaAttributes[col]);
      }
  }],
  createdRow : function(row, data, dataIndex) {
    //var dt = $(this).DataTable()
    row.setAttribute("data-qa", "view-profile-trigger");
    /*row.addEventListener('click', function() {
      if(!Util.isDragged()){
        var data = dt.row(this).data()
        console.log('select', dt.row(this).data())
        window.location.href = window.location.href + "&notificationId=" + data.id;
      }
    })*/
    row.addEventListener('click', function() {
      var paramsConfig = {};
      paramsConfig["subscriptionId"] = data.requestid;
      paramsConfig["userId"] = data.userId;
      paramsConfig["storeId"] = Util.getParameterByName("storeId", "");
      paramsConfig["businessId"] = Util.getParameterByName("businessId", "");
      paramsConfig["collectionId"] = Util.getParameterByName("collectionId", "");
      window.location.href = SubscriptionUtil.addParamsToURL(window.location.pathname, paramsConfig);
    })
  },
  "drawCallback": function( settings ) {
      $('.cls_skcancelsubscription').off('click').on('click', function(event) {
          event.preventDefault();
          event.stopPropagation();
          ModalManager.launch({type:'cancelSubscriptionConfirmation', id: $(this).attr('data-id'), userId: $(this).attr('data-userId')});
      });
  }
});
