FormManager.setHandler('edit-subscription-shipping-method',(function(){
    
    function onSubmit(evt, info){
        if (info.form.checkValidity()) {
            //var props = FormManager.objectify(info.form)
            var props = $('.cls_shippingAddrData').data("addr");
                        
            var cbk = function() {
                FormManager.cancel(info.form);
                loader.destroy();
            }
            console.log(props);
            
            var selectedShippingMethod = $('input[name="shippingValue"]:checked').val();
            var modelConfig = {};
            var shippingInfo = {};
               shippingInfo["method"] = selectedShippingMethod;
               shippingInfo["deliveryPeriod"] = 1;
            modelConfig["shippingInfo"] = shippingInfo;
            modelConfig["addressInfo"] = props;

            editSubscriptionDetails(modelConfig);

            var loader = new LoadBlock({target:info.form})
        } else {
            info.form.classList.add('was-validated');
        }
        
        evt.preventDefault();
        evt.stopPropagation();
        
        return false;
    }

    function editSubscriptionDetails(modelConfig) {
        var updateUrl = (GLOBAL_CONFIG && GLOBAL_CONFIG.URL_ORCHESTRATAION ? GLOBAL_CONFIG.URL_ORCHESTRATAION : '') + 
          '/subscriptions/' + Util.getParameterByName("subscriptionId", "");
        var collectionId = Util.queryValues && Util.queryValues.collectionId ? Util.queryValues.collectionId : "";
        updateUrl = Util.appendBasicParam(updateUrl, collectionId);
        updateUrl = updateUrl + "&userInfo=" + ((Util.queryValues && Util.queryValues.userId) ? Util.queryValues.userId : "");

        var config = {
            url : updateUrl,
            data : JSON.stringify(modelConfig),
            method: "PATCH",
            contentType: "application/json"
        }
        
        var callBack = function(response, status)
        {
            var message =  message_properties["SUBSCRIPTION.UPDATE.SUCCESS.MSG"];
            if (status == "error") {
                message = (response ? (response.responseMessage ? response.responseMessage : (response.responseJSON && response.responseJSON.responseMessage ? response.responseJSON.responseMessage : 'Something went wrong.')) : 'Something went wrong.')
            }

            Util.toast(message, null , status);
            if(status == "success") {
                setTimeout(function() {
                    location.reload();
                }, 5000);
            }

            if(cbk)
            {
                cbk(response, arguments);
            }
        };
        
        Util.getData(config, callBack);
      }

    return {
        onSubmit:onSubmit
    }
    
})())