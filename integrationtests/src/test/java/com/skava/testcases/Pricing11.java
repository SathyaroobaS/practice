package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Pricing11 extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
	}
	
	@Test
	public void Pricing_Scenario_11_Create_Mulitple_Prices_To_SKU() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		launchExistingCollection(); 
		verifyUserNavigatedToPriceListPage();
		clickCreatePriceList();
		verifyUserNavigatedToCreatePriceListPage();	
		enterDetailsToCreatePriceList();
		searchCreatedPriceList();
		//verifyAndClickEditPriceList();
		verifyAndClickViewPriceList();
		verifyAndClickProjectDD();
		clickCreateProjectInEditPricingPage();
		createNewProjectInEditPricingPage();
		clickCreatedProjectFromListToEnterViaEnterButton();
		clickSKUTab();
		verifyAllSKUTabIsDisplayed();
		addNewSKUDetails();
		clickEditSKU();
		/*verifyAddedPriceIsDisplayed();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		enterAndVerifySKUMinQty();
		enterAndVerifySKUMaxQty();
		enterFloorPrice();
		scheduleDateForSKUFromAddSKUScreen();
		clickAddPricesToSKU();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		enterAndVerifySKUMinQty();
		enterAndVerifySKUMaxQty();
		enterFloorPrice();
		scheduleDateForSKUFromAddSKUScreen();
		clickAddPricesToSKU();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		enterAndVerifySKUMinQty();
		enterAndVerifySKUMaxQty();
		enterFloorPrice();
		scheduleDateForSKUFromAddSKUScreen();
		clickAddPricesToSKU();
		verifyAddedPriceIsDisplayed();*/
	/*	navigateToSKUPage();
		enterSKUDetailsAlone();
		enterAndVerifySKUMinQty();
		enterAndVerifySKUMaxQty();
		saveSKUDetails();
		clickEditSKU();
		verifyAddedPriceIsDisplayed();
		navigateToSKUPage();
		enterSKUDetailsAlone();
		scheduleDateForSKUFromAddSKUScreen();
		saveSKUDetails();
		clickEditSKU();
		verifyAddedPriceIsDisplayed();
		navigateToSKUPage();
		enterSKUDetailsAlone();
		enterAndVerifySKUMinQty();
		enterAndVerifySKUMaxQty();
		scheduleDateForSKUFromAddSKUScreen();
		saveSKUDetails();
		clickEditSKU();
		verifyAddedPriceIsDisplayed();*/
		
		searchAndSetCurrentProject();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		enterAndVerifyMinQty();
		enterAndVerifyMaxQty();
		setSKUStatusActive();
		clickSaveSKUAdditionalPrice();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		scheduleDateForSKU();
		clickSaveSKUAdditionalPrice();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		enterAndVerifyMinQty();
		enterAndVerifyMaxQty();
		scheduleDateForSKU();
		clickSaveSKUAdditionalPrice();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
