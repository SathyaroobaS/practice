package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_12_CatalogAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_12_CatalogAdmin() throws InterruptedException 
	{	
		//Login Verification
		launchUrl();
		
		//driver.navigateToUrl("https://euat.skavaone.com/admin/catalogs/?businessId=341&serviceName=catalog&collectionId=521");
		enterLoginCredentials();
		catalogsPageValidation();
		
		// Attributes creation
		verifyAttributesPageIsDisplayed();
		clickCreateAttributeBtn();
		ChooseDynamicTypeAttribute();
		selectImageandImagewithTextFieldTypeFromDropdown();		
		EnterImgLinkinAttribute();
		clickAttributeCreateButton();
		verifyCreatedAttributeIsDisplayed();

	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}

}