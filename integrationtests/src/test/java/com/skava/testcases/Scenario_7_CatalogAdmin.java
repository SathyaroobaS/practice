package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_7_CatalogAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_7_CatalogAdmin() throws InterruptedException 
	{	
		// Login Verification
		launchUrl();
		enterLoginCredentials();
		catalogsPageValidation();
		EnterIntoMasterCatalog();
		ClickProjectsDropdownAndSelectProject();
		//PancakeClick();
		SelectProductsFromPancake();
		ClickonCreateItemButton();
		AddCollectionItems();		
		//searchSKU();
		//navigateToSearchedProduct();
		//UpdateBundleNameAndSave();
		//TempNavigateBack();
		//CompareBundleAndCollectionName();
		//PancakeClick();
		ClickonCollectionTab();
		ClickonAddProductButton();
		AddApprovedProductToBundle();
		//GetProductIdFromBundleOverlay(); // In general components
		ClickonAddProductButton();
		VerifyAddedProductisDisabled();
		DeleteAddedProduct();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}

}