package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_3_BusinessAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void Scenario_3_BusinessAdmin() throws Exception 
	{
		// Login Verification
		launchUrl();
		enterLoginCredentials();
		
		/*
		 *  Commenting until 8.0 release ------------- Start
		 */
		/*// Add / Create a Business
		CreateBusinessButtonNavigation();
		EnterDetailsForCreateBusinessInformationPage();
		
		//Activate MicroService
		// **** NOT REQUIRED For now **** ActivateMultipleMicroServicesInLoop(); // Random Collection Create 
		ActivateMultipleMicroServices(); 
		VerifySuccessPopAfterCreatingBusiness();
		GetBusinessIdFromURL();*/ 
		/*
		 * ------------- END ------------ 
		 */
		
		/*
		 * Newly added for 8.0 release ----------- START
		 */
		SearchExistingBusiness();
		//BusinessIDComparision();
		NavigatetoSearchedBusiness();
		NavigateToStoresMenu();
		SearchExistingStore();
		/*
		 *  ------------ END -----------------
		 */
		
		
		// Create the store
		VerifyStoreURLDisplay();
		EnterCreateStoreInformation();
		ClickOnStoresLinkBreadcrumb(); // Breadcrumb click to add - MULTIPLE STORE
		EnterCreateStoreInformationForCanada1(); // US - Canada
		
		// Go to Store Menu - Select Microservice
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		
		// Create Multiple Collection
		VerifyMicroServiceTitle();
		SelectMicroServiceInLoop1(); // Create Collection
		
		// Associate Store
		NavigateToStoresMenu();
		MulitpleStoreToAssociationNavigation();
		
		// Launch Multiple Collection
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		LaunchMultipleServices();
				
		// Business Association
		ClickOnCreatedBusinessFromBreadcrumb();
		//BusinessOverviewPageComparision();
		
		// Store Association Validation
		NavigateToStoresMenu();
		SelectStoreOpsFromDropdown();
		ViewStoreNavigationforStoreOps();
		StoreNameComparision();
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
