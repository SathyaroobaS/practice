package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_2_InstanceAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void Scenario_2_InstanceAdmin() throws Exception 
	{
		// Login Verification
		launchUrl();
		enterLoginCredentials();
		
		// Business Overview Page
		SearchExistingBusiness();
		BusinessIDComparision();
		
		// Edit Created Business
		ClickOnEditBusinessbuttonFromAllBusinessPage();
		UpdateBusinessNameandLinksInCreateBusinessPage();
		ClickOnEditBusinessSavebutton();
		
		// Logout as business
		BusinessAdminLogout();
		
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
