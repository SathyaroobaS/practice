package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.skava.reusable.components.PromotionsComponents;



public class promo15 extends PromotionsComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void promotion13() throws Exception 
	{/*
		launchUrl();
		LogIn();
		ClickCreatePromotionGroup();
		clickEnter();
		enterGroupName();
		clickdaterange();
		selectstartdate();
		selectenddate();
		clickApplyInDatePicker();
		clickStatus();
		selectstatusactive();
		priorityvalue();
		clickSave();
		
		clickPromotionCreateButton();
		enterPromotionName();
		clickdaterange();
		selectstartdate();
		selectenddate();
		clickApplyInDatePicker();
		clickPromotionStatus();
		selectPromotionstatus();
		enterMessage();
		enterDescription();	
		clickRuletype();
		selectRuletype();
		clickActiontype();
		selectActiontype();
		promotionPriorityvalue();
		promotionSave();
		
		createCondition();
		clickRuleGroup();
		clickRule();
		rulesValue();
		clickAddrule();
		ClickANDButton();
		clickRuleGroup();
		clickRule();
		changecondition();
		rulesValue();
		ClickSave();
		updateChecking();
		ClickActionTab();
		ClickCreateAction();
		enterActionname();
		enterActiondescription();
		ClickActionClassName();
		SelectActionClassName();
		enterofferValue();
		clickRuleGroup();
		clickAction();
		actionValue();
		clickAddrule();
		ClickANDButton();
		clickRuleGroup();
		clickAction();
		actionValue();
		ClickActionSave();
		
		
			
		
	*/}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
