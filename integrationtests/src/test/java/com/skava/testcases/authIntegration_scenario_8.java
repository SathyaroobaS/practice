package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.InviteUserComponents;

public class authIntegration_scenario_8  extends InviteUserComponents {
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void scenario_8() throws Exception 
	{
		launchUrl();
		doLoginWithValidCrentials();
		clickInstanceInMenu();
		verifyInstanceTeamPage();
		userDetailDisplayed();
		tableColumnDisplayed();
		navigateToInviteUsersPage();
		fillEmailAddress();
		instanceAdminToggle();
		clickSendInvite();
		verifyInviteSuccessMsg();
		clickTeamBreadcrumb();
		searchWithFnameFilter();
		waitingLoadingGauage();
		clickMembersReset();
		waitingLoadingGauage();
		searchWithLnameFilter();
		waitingLoadingGauage();
		clickMembersReset();
		waitingLoadingGauage();
		searchWithEmailFilter(email);
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
