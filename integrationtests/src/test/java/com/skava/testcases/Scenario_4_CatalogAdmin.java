package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_4_CatalogAdmin extends TestComponents
{

	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_4_CatalogAdmin() throws InterruptedException 
	{	
		// Login Verification
		launchUrl();
		enterLoginCredentials();
		catalogsPageValidation();
		masterCatalogValidator();
		masterCatalogEditbuttonValidator();
		masterCatalogViewClick();
		masterCatalogProjectPageValidator();
		
		//create project
		verifyProjectMenuIsClicked();
	    createNewProject();
	    navigateToImportPage(); 
	      
	    //Import Data Sheet
	    verifyImportTabIsClicked();
	    uploadImportDataSheet();
	    clickImportButton();
	    verifyImportSuccessMessageIsDisplayed();    
	    
		defaultProjetPageMenuValidation();
		defaultProjetProductsMenuClick();
		defaultProjetProductPageProductsCheck();
		defaultProjetSkuMenuClick();
		defaultProjetSkuPageSkuCheck();
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}


}
