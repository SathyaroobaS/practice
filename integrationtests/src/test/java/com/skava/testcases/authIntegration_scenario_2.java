package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.InviteUserComponents;

public class authIntegration_scenario_2 extends InviteUserComponents {
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void scenario_2() throws Exception 
	{       
		launchUrl();
		doLoginWithValidCrentials();
		clickInstanceInMenu();
		verifyInstanceTeamPage();
		userDetailDisplayed();
		tableColumnDisplayed();
		navigateToInviteUsersPage();
		fillEmailAddress();
		clickBusiness();
		clickBusinessAdminToggle();
		clickSendInvite();
		verifyInviteSuccessMsg();
		
		clickTeamBreadcrumb();
		searchWithEmailFilter(email);
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
