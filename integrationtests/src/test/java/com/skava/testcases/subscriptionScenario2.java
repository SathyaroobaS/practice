package com.skava.testcases;

import java.io.IOException;
import org.testng.annotations.AfterClass; 
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.customerAdminComponents;
import com.skava.reusable.components.subscriptionAdminComponents;


public class subscriptionScenario2 extends subscriptionAdminComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void Subscription_Scenario1() throws Exception 
	{
		launchUrl();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
