package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.MerchandiseadminComponents;
import com.skava.reusable.components.TestComponents;

public class Scenario3_merchandise extends TestComponents{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void VerifyProjectSubmit() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		isDisplayedListofCategory();
		clkPrjctDropdown();
		createNewProjectButton();
		enterNewprojectname();
		clickCreateButton();
		clkDetailOfOpen();
		editProjectName();
		submitBtn();
		isDisplayedApproveBtn();
		logout();
	}

	@AfterClass(alwaysRun=true)
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
