package com.skava.testcases;

import java.io.IOException;

import org.apache.tools.ant.taskdefs.SendEmail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.skava.reusable.components.InstanceTeamComponents;
import com.skava.reusable.components.InviteUserComponents;
import com.skava.reusable.components.TestComponents;

public class authIntegration_scenario_1 extends TestComponents{
		@BeforeClass
		public void initTest() throws IOException
		{		
			driver = initiTest(this.getClass().getSimpleName());		
			driver.maximizeBrowser();
		}
		
		@Test
		public void scenario_1() throws Exception 
		{
			launchUrl();
			/*doLoginWithValidCrentials();*/
			/*clickInstanceInMenu();
			verifyInstanceTeamPage();
			userDetailDisplayed();
			tableColumnDisplayed();
			navigateToInviteUsersPage();
			fillEmailAddress();
			instanceAdminToggle();
			clickSendInvite();
			verifyInviteSuccessMsg();
			clickTeamBreadcrumb();
			searchWithEmailFilter(email);*/
			
		}
		
		@AfterClass
		public synchronized void tearDown()
		{		
			if (driver != null) 
			{
				driver.quit();
			}
		}
}
