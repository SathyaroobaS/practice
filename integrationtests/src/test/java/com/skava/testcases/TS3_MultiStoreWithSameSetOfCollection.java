package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class TS3_MultiStoreWithSameSetOfCollection extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void TS3_MultiStoreWithSameSetOfCollection() throws Exception 
	{
		
		launchUrl();
		enterLoginCredentials();
		VerifyCreateBusinessURLVerification();
		// Create Business
		UpdateBusinessNameandLinksInCreateBusinessPage();
		ActivateSingleMicroServices();
		VerifySuccessPopAfterCreatingBusiness();
		// Create the store
		VerifyStoreURLDisplay();
		EnterCreateStoreInformation();
		ClickOnStoresLinkBreadcrumb(); // Breadcrumb click to add - MULTIPLE STORE
		EnterCreateStoreInformationForCanada(); // US - Canada
		// Go to Store Menu - Select Microservice
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		
		// Add collection to the micro services
		VerifyMicroServiceTitle();
		SelectMicroServiceInLoop();
		
		// Associate Store
		NavigateToStoresMenu();
		ChooseMultipleStoreToSameCollectionInLoop();
		
		// Launch Step
		NavigateToMicroServicesMenu();
		ClickPromotionMicroServiceToUpdate();
		ClickToLaunchButton();
		LaunchURLVerification();
		
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
