package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class TS1_CreateNewBusiness extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void TS1_CreateNewBusiness() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		VerifyCreateBusinessURLVerification();
		// Create Business
		UpdateBusinessNameandLinksInCreateBusinessPage();
		ActivateSingleMicroServices();
		VerifySuccessPopAfterCreatingBusiness();
		// Create the store
		VerifyStoreURLDisplay();
		EnterCreateStoreInformation();
		// Go to Store Menu - Select Microservice - Menu navigation
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		
		// Add collection to the micro services
		VerifyMicroServiceTitle();
		SelectMicroServiceInLoop();
		
		// Associate Store
		NavigateToStoresMenu();
		ClickToStoreName();
		ClickToServiceAssociationTab();
		SelectCollectionDropdown();
		
		// Go to Store Menu - Select Microservice
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		ClickPromotionMicroServiceToUpdate();
		ClickToLaunchButton();
		LaunchURLVerification();
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
