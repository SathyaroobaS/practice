package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Pricing3 extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
	}
	
	@Test
	public void Pricing_Scenario_3_Create_Update_Project_Meta_Data() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		launchExistingCollection(); 
		verifyUserNavigatedToPriceListPage();
		clickCreatePriceList();
		verifyUserNavigatedToCreatePriceListPage();
		enterDetailsToCreatePriceList();
		searchCreatedPriceList();
		//verifyAndClickEditPriceList();
		verifyAndClickViewPriceList();
		verifyAndClickProjectDD();
		clickCreateProjectInEditPricingPage();
		createNewProjectInEditPricingPage();
		clickCreatedProjectFromListToEnterViaCheveronButton();
		verifyAndClickEditDescriptionButton();
		enterProjectNameAndSave();
		enterTargetCompletionDate();
		//verifyAndClickEditDescriptionButton();
		enterProjectDescriptionAndSave();
		clickNotesTab();
		verifyAndEnterProjectNotes();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
