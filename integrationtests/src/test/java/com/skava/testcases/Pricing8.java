package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Pricing8 extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
	}
	
	@Test
	public void Pricing_Scenario_8_Create_Approved_Content_To_Selected_Project_And_Different_Price() throws Exception 
	{
		/*
		 * PricingPreScript p1=new PricingPreScript(); p1.generatesessionId();
		 * p1.getPricingBusinessCollection(); p1.createPricingCollection();
		 * p1.createPriceListForPricing(); //generatesessionIdfoundation();
		 * p1.storeAssociateFoundation();
		 * 
		 */
		launchUrl();
		enterLoginCredentials();
		launchExistingCollection(); 
		verifyUserNavigatedToPriceListPage();
		clickCreatePriceList();
		verifyUserNavigatedToCreatePriceListPage();
		enterDetailsToCreatePriceList();
		searchCreatedPriceList();
		//verifyAndClickEditPriceList();
		verifyAndClickViewPriceList();
		verifyAndClickProjectDD();
		clickCreateProjectInEditPricingPage();
		createNewProjectInEditPricingPage();
		clickCreatedProjectFromListToEnterViaEnterButton();
		clickSKUTab();
		verifyAllSKUTabIsDisplayed();
		addNewSKUDetails();
		verifyAndClickProjectDD();
		clickCreatedProjectFromListToEnterViaCheveronButton();
		clickProjectSubmit();
		clickProjectApproval();
		navigateToSKUFromApprovedPage();
		verifyAndClickProjectDD();
		clickCreateProjectInEditPricingPage();
		createNewProjectInEditPricingPage();
		clickCreatedProjectFromListToEnterViaEnterButton();
		clickSKUTab();
		verifyAllSKUTabIsDisplayed();
		searchAndSetAllApproved();
		clickEditSKU();
		searchAndSetCurrentProject();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		enterAndVerifyMinQty();
		enterAndVerifyMaxQty();
		setSKUStatusActive();
		clickSaveSKUAdditionalPrice();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		scheduleDateForSKU();
		clickSaveSKUAdditionalPrice();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		enterAndVerifyMinQty();
		enterAndVerifyMaxQty();
		scheduleDateForSKU();
		clickSaveSKUAdditionalPrice();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
