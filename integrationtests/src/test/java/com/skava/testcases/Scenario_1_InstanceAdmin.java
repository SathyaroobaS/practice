package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_1_InstanceAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void Scenario_1_InstanceAdmin() throws Exception 
	{
		// Login Verification
		launchUrl();
		enterLoginCredentials();
		
		/*
		 *  Commenting until 8.0 release ----------- START
		 */
		/*// Business Overview Page
		AllBusinessPageValidation();
		SearchExistingBusiness();
		BusinessIDComparision();
		
		// Create New Business
		CreateBusinessButtonNavigation();
		EnterDetailsForCreateBusinessInformationPage();
		
		// Add MicroService
		ActivateMultipleMicroServices(); 
		VerifySuccessPopAfterCreatingBusiness();*/
		// -------------------------- END -------------------
		
		
		/*
		 *  newly added for 8.0 --------- START
		 */
		
		Thread.sleep(4000);
		SearchExistingBusiness();
		BusinessIDComparision();
		NavigatetoSearchedBusiness();
		
		// ----------- END ----------------
		
		
		GetBusinessIdFromURL();
		BusinessOverviewPageValidation();
		BusinessOverviewPageComparision();
		
		// Logout as business
		BusinessAdminLogout();
		
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
