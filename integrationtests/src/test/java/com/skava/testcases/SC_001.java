package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.AccountComponents;


public class SC_001 extends AccountComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void SC_001() throws Exception 
	{
		launchUrl();
		login();
		businesssearch();
		storeidsearch();
		microserviceselect();
		createaccount();
		verifyaccounttitle();
		enteraccountname();
		enterbuyeremail();
		enterfirstname();
		enterlastname();
		enteraccountsize();
		enteraccountaddress();
		entercity();
		selectaccounttype();
		selectcountry();
		enterstate();
		enterzipcode();
		enterdunsnumber();
		entertaxid();
		submitbtntrigger();
		viewoverviewpage();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
