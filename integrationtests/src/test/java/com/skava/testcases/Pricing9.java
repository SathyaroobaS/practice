package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Pricing9 extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
	}
	
	@Test
	public void Pricing_Scenario_9_Create_Project_Reopen_Deny_Project() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		launchExistingCollection(); 
		verifyUserNavigatedToPriceListPage();
		clickCreatePriceList();
		verifyUserNavigatedToCreatePriceListPage();
		enterDetailsToCreatePriceList();
		searchCreatedPriceList();
		verifyAndClickViewPriceList();
		verifyAndClickProjectDD();
		clickCreateProjectInEditPricingPage();
		createNewProjectInEditPricingPage();
		clickCreatedProjectFromListToEnterViaEnterButton();
		clickSKUTab();
		verifyAllSKUTabIsDisplayed();
		addNewSKUDetails();
		verifyAndClickProjectDD();
		clickCreatedProjectFromListToEnterViaCheveronButton();
		clickProjectSubmit();
		clickReopenProject();
		clickProjectSubmit();
		clickDenyProject();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
