package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.skava.reusable.components.PromotionsComponents;



public class promogroup2_5 extends PromotionsComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void promoGroup2Promo14() throws Exception 
	{
		
		launchUrl();
		logIn();
		
		clickListTab();
		clickCreateListBtn();
		enterListName();
		clickListStatus();
		selectListStatusInactive();
		clickListType();
		selectListType();
		clickFieldName();
		selectFieldName();
		clickSave();
		clickPromotionAddListItemButton();
		enterPromotionListLabel();
		clickPromotionAddListItemField();
		clickSave();
		
		clickPromotionListChevronButton();
		enterListName();
		clickListStatus();
		selectListStatusActive();
		clickSave();
			
		clickPromotionsTab();
		clickProject();
		clickCreatenewproject();
		enterNewProjectName();
		clickCreateProject();
		enterCreateProject();
		clickCreatePromotionGroup();
		enterGroupName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickStatus();
		selectStatusInactive();
		priorityValue();
		clickSave();
			
		clickPromotionCreateButton();
		enterPromotionName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickPromotionStatus();
		selectPromotionStatus();
		enterMessage();
		enterDescription();	
		clickRuletype();
		selectRuletype();
		clickActiontype();
		selectActiontype();
		promotionPriorityvalue();
		promotionSave();
		
		clickProject();
		chevronEnterCreateProject();
		clickSubmit();
		clickApprove();
		
		clickPromotionBreadCrumb();
		clickActionButton();
		clickActionDeleteButton();
		clickYesButton();
		clickCreatenewproject();
		enterNewProjectName();
		clickCreateProject();
		
			
		logout();
		
		
			
		
  	}
	
	@AfterClass(alwaysRun=true)
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
