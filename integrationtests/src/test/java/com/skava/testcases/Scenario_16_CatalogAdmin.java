package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_16_CatalogAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_16_CatalogAdmin() throws InterruptedException 
	{	
		//Login Verification
		launchUrl();
		//driver.navigateToUrl("https://int.skavacommerce.com/admin/catalogs/?collectionId=1536&businessId=4&storeId=52");
		
		//driver.navigateToUrl("https://euat.skavaone.com/admin/catalogs/?businessId=341&serviceName=catalog&collectionId=521");
		//driver.navigateToUrl("https://admin.skavacommerce.com/admin/catalogs/?businessId=4&serviceName=catalog&collectionId=363");
		enterLoginCredentials();
		catalogsPageValidation();
		EnterIntoMasterCatalog();
		ClickProjectsDropdownAndSelectProject();
		
		//Asset Tab selection from SKU 
		SelectProductsFromPancake();
		RandomSelectFromProductList();
		
		//SKU tab navigation
		SKUsTabSelectinAddSkuPage();
		
		//Add SKU flow
		AddSKUButtonClick();
		AddSKUfromOverlayatRightCorner();
		VerifyAddedProductisDisabled();
		//GetAddedSKUValue(); // Store added sku value
		//SaveAddedSkuList(); // Save added sku - new to this tc
		//Product and SKU tab selection
		//SelectProductsFromPancake();
		//defaultProjetSkuMenuClick();
		//Choose all approved Project
		//selectApprovedProject();
		//clickSearchButtoninOverviewPage();
		//Search from approved project
		//SearchAddedSkuIDandAssertSearchValue();
		//Delete sku item
		//SelectProductsFromPancake();
		//RandomSelectFromProductList();
		//SKUsTabSelectinAddSkuPage();
		//DeleteAddedskulist();
		
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}

}