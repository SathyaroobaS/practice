package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_1_BusinessAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void Scenario_1_BusinessAdmin() throws Exception 
	{
		// Login Verification
		launchUrl();
		enterLoginCredentials();
		
		/*
		 ****************** Commented for current 18th release - START
		 */
		
		/* 		  
		// Add / Create a Business
		CreateBusinessButtonNavigation();
		EnterDetailsForCreateBusinessInformationPage();
		
		//Activate MicroService
		// **** NOT REQUIRED For now **** ActivateMultipleMicroServicesInLoop(); // Random Collection Create 
		ActivateMultipleMicroServices(); 
		VerifySuccessPopAfterCreatingBusiness();
		GetBusinessIdFromURL();
		BusinessOverviewPageValidation();
		BusinessOverviewPageComparision();
		******************************************* END/
		
		/*
		 * Remove below 3 lines after business creation got approved
		 */
		SearchExistingBusiness();
		BusinessIDComparision();
		ClickOnEditBusinessbuttonFromAllBusinessPage();
		// ----------------------------- Remove the above later
		
		
		// Update Created business
		//ClickOnCreatedBusinessFromBreadcrumb(); //------------------ Remove this comment once business creation approved
		//ClickOnEditBusinessbutton(); // ----------------------Remove this comment once business creation approved
		UpdateBusinessNameandLinksInCreateBusinessPage();
		ClickOnEditBusinessSavebutton();
		
		// Create Store 
		NavigateToStoresMenu();
		//EnterCreateStoreInformation();
		EnterCreateStoreInformationForCanada1();
		
		// Go to Store Menu - Select microservice - Menu navigation
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		
		// Add collection to the micro services
		VerifyMicroServiceTitle();
		//ActivateMultipleMicroServices();
		SelectMicroServiceInLoop1(); 
			
		// Associate Store
		NavigateToStoresMenu();
		ClickToStoreName();
		ClickToServiceAssociationTab();
		//AssociateMultipleCollection();
		associateCatalogServices(); // Associcate catalog collection
		associateCustomersServices(); // Associcate customer collection
	
		
		// Launch Multiple Collection
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		LaunchMultipleServices();
		
		// Business Association
		ClickOnCreatedBusinessFromBreadcrumb();
		BusinessOverviewPageComparision();
		
		// Store Association Validation
		NavigateToStoresMenu();
		SelectStoreOpsFromDropdown();
		ViewStoreNavigationforStoreOps();
		StoreNameComparision();
		
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
