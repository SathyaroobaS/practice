package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_17_CatalogAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_17_CatalogAdmin() throws InterruptedException 
	{	
		//Login Verification
		launchUrl();
		
		//Standard Flow to select project
		enterLoginCredentials();
		catalogsPageValidation();
		EnterIntoMasterCatalog();
		ClickProjectsDropdownAndSelectProject();
		
		//Asset Tab selection from SKU 
		SelectProductsFromPancake();
		RandomSelectFromProductList();
		
		//Attribute tab selection
		SelectAttributesFromProductPage();
		ClickAddButtonInAttributesPage();
		verifyCancelSaveButtonsDisplay();
		SearchAttributeandEnter();
		EnterDescText();
		DeleteAddedAttributesList();
		noPropertiesAvailableTextDisplay();
		
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}

}