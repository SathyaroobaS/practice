package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_1_StoreAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_1_StoreAdmin() throws InterruptedException 
	{
		launchUrl();
		enterLoginCredentials();
		//driver.navigateToUrl("https://euat.skavaone.com/admin/foundation/stores/storeops?businessId=2035&storeId=3476");
		
		SearchExistingBusiness();
		BusinessIDComparision();
		NavigatetoSearchedBusiness();
		NavigateToStoresMenu();
		SearchExistingStore();
		
		NavigatetoSearchedStore();
		VerifyStoreOverview();
		StoreNameComparision();
		ClickonBusinessTitle();
		NavigateToStoresMenu();
//		ClickAddStoreBtn();
		EnterCreateStoreInformationForCanada1();
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		
		// Add collection to the micro services
		VerifyMicroServiceTitle();
		SelectMicroServiceInLoop1(); 
						
		// Associate Store
		NavigateToStoresMenu();
		ClickToStoreName();
		ClickToServiceAssociationTab();
		//SelectCollectionDropdown();
		//AssociateMultipleCollection();
		associateCatalogServices();
//		associateCustomersServices();
		
		
		// Go to Store Menu - Select Microservice
		NavigateToStoresMenu();		
		NavigateToMicroServicesMenu();
		ClickPromotionMicroServiceToUpdate();
//		ClickToLaunchButton();
//		LaunchURLVerification();// Associate Store
					
		 
				
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}

}
