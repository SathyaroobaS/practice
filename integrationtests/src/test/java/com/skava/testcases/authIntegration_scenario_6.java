package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.EditUserComponents;
import com.skava.reusable.components.InstanceTeamComponents;
import com.skava.reusable.components.InviteUserComponents;
import com.skava.reusable.components.TestComponents;

public class authIntegration_scenario_6 extends TestComponents 
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void scenario_6() throws Exception 
	{
		launchUrl();
        doLoginWithValidCrentials();
        clickInstanceInMenu();
        verifyInstanceTeamPage();
        userDetailDisplayed();
        tableColumnDisplayed();
        navigateToInviteUsersPage();
        fillEmailAddress();
        instanceAdminToggle();
        clickSendInvite();
        verifyInviteSuccessMsg();
        clickTeamBreadcrumb();
        searchWithEmailFilter(email);
        clickEditUser();
        instanceAdminToggle();
        
        verifyBusinessSectionEnabled();
        clickBusiness();
        clickBusinessAdminToggle();
        clickResendActivation();
        verifyResendSuccessMsg();
		
		
		
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
