package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_2_BusinessAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void Scenario_2_BusinessAdmin() throws Exception 
	{
		// Login Verification
		launchUrl();
		enterLoginCredentials();
		//driver.navigateToUrl("https://euat.skavaone.com/admin/foundation/stores?businessId=2035&id=3476");
		
		
		
		/*
		 * Remove below 3 lines after business creation got approved
		 */
		SearchExistingBusiness();
		BusinessIDComparision();
		NavigatetoSearchedBusiness();
		NavigateToStoresMenu();
		SearchExistingStore();
		
		// ----------------------------- Remove the above later
		
		
		
		// Store Association Validation
		//NavigateToStoresMenu(); // No need
		//SelectStoreOpsFromDropdown(); // No need
		ViewStoreNavigationforStoreOps();
		StoreNameComparision();
		
		// Edit Store
		/*PancakeClickFromStoreOverviewPage();// No need
		ClickOnStoreOverviewTab();*/// No need
		ClickOnEditStoreButton();
		EditStoreDetailsExceptStoreName();
		
		// Go to Store Menu - Select microservice - Menu navigation
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		
		// Add collection to the micro services
		VerifyMicroServiceTitle();
		MicroserviceURLVerification();
		//SelectMicroServiceInLoop(); // Create Collection In Loop
		
		// Edit Collection
		ClickOnMicroserviceLinkInBreadcrumb();
		EditCollectionDesc();
		
		// Associate Store
		NavigateToStoresMenu();
		ClickToStoreName();
		ClickToServiceAssociationTab();
		//AssociateMultipleCollection();
		associateCatalogServices(); // Associcate catalog collection
//		associateCustomersServices(); // Associcate customer collection
		
		// Launch Multiple Collection
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		LaunchMultipleServices();
		
		// Store Association Validation
		NavigateToStoresMenu();
		SelectStoreOpsFromDropdown();
		ViewStoreNavigationforStoreOps();
		StoreNameComparision();
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
