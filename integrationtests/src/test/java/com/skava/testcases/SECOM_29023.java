package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.skava.reusable.components.TestComponents;

public class SECOM_29023 extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		
	}
	
	@Test
	public void SECOM29023() throws Exception 
	{
		
		createCollectionomsNoorder();
		storeassocatefoundationNoorder();
		launchUrl();
		enterLoginCredentials();
		NoOrderDisplayed();
//		verifyOrderListPage();//QA attributes chnged
		orderIdFilterDisplayed();
		verifyDateRangeFilterDisplayed();
		verifyFirstNameFilterDisplayed();
		verifyEmailNameFilterDisplayed();
		verifyNoOrderDisplayed();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}