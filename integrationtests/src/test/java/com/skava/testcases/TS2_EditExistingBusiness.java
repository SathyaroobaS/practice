package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class TS2_EditExistingBusiness extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void TS2_EditExistingBusiness() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		VerifyCreateBusinessURLVerification();
		// Create Business
		UpdateBusinessNameandLinksInCreateBusinessPage();
		ActivateSingleMicroServices();
		VerifySuccessPopAfterCreatingBusiness();
		// Update Created business
		ClickOnCreatedBusinessFromBreadcrumb();
		ClickOnEditBusinessbutton();
		UpdateBusinessNameandLinksInCreateBusinessPage();
		ClickOnEditBusinessSavebutton();
		
		// Update Micro Service
		NavigateToMicroServicesMenu();
		SelectMicroServiceInLoop(); // Create collection Flow
		ClickOnConfigureButtonToUpdateCollection();
		UpdateCollection(); // Update collection
		// Create the store
		VerifyStoreURLDisplay();
		EnterCreateStoreInformation();
		// Select collection from dropdown
		SelectCollectionDropdown();
		
		// Redirection
		NavigateToStoresMenu();
		// Update Store
		ClickOnEditStoreButton();
		UpdateStoreName();
		// Redirection for launch
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		ClickPromotionMicroServiceToUpdate();
		ClickToLaunchButton();
		LaunchURLVerification();
		
		/*
		 * ------------ Above are Working fine -----------
		 */
		
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
