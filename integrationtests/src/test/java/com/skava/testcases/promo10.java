package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.skava.reusable.components.PromotionsComponents;



public class promo10 extends PromotionsComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void promotion10() throws Exception 
	{/*
		launchUrl();
		LogIn();
		clickProject();
		clickCreatenewproject();
		EnterNewprojectname();
		createproject();
		entercreateproject();
		ClickCreatePromotionGroup();
//		clickEnter();
		enterGroupName();
		clickdaterange();
		selectstartdate();
		selectenddate();
		clickApplyInDatePicker();
		clickStatus();
		selectstatusactive();
		priorityvalue();
		clickSave();
		
		clickPromotionCreateButton();
		enterPromotionName();
		clickdaterange();
		selectstartdate();
		selectenddate();
		clickApplyInDatePicker();
		clickPromotionStatus();
		selectPromotionstatus();
		enterMessage();
		enterDescription();	
		clickRuletype();
		selectRuletype();
		clickActiontype();
		selectActiontype();
		promotionPriorityvalue();
		promotionSave();
		
		promotionEdit();
		promotionEditEnterValue();
		promotionEditAndSave();
		promo_click();
		promo_delete();
		clickProject();
		chevronentercreateproject();
		//enterDetailpage();
		clickSubmit();
		clickProject();
		entercurrentpage();
		clickProject();
		entercurrentpage();
		clickReject();
		
		createCondition();
		clickRuleGroup();
		clickRule("id");
		rulesValue("rulevalue1");
		clickAddrule();
		ClickANDButton();
		clickRuleGroup();
		clickRule("color");
		rulesValue("rulevalue2");
		ClickSave();
		updateChecking();
		
		ClickActionTab();
		ClickCreateAction();
		enterActionname();
		enterActiondescription();
		ClickActionClassName();
		SelectActionClassName();
		enterofferValue();
		clickRuleGroup();
		clickAction("shippingCity");
		actionValue("actionvalue1");
		clickAddrule();
		ClickORButton();
		clickRuleGroup();
		clickAction("shippingPostalCode");
		actionValue("actionvalue2");
		clickAddrule();
		ClickANDButton();
		clickRuleGroup();
		clickAction("paymentType");
		actionValue("actionvalue3");
		ClickActionSave();
		
		
			
		
	*/}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
