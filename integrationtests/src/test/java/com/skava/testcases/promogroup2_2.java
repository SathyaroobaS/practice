package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.skava.reusable.components.PromotionsComponents;



public class promogroup2_2 extends PromotionsComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void promoGroup2Promo11() throws Exception 
	{
		
		launchUrl();
		logIn();
		clickProject();
		clickCreatenewproject();
		enterNewProjectName();
		clickCreateProject();
		enterCreateProject();
		clickCreatePromotionGroup();
		enterGroupName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickStatus();
		selectstatusactive();
		priorityValue();
		clickSave();
		
		clickPromotionCreateButton();
		enterPromotionName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickPromotionStatus();
		selectPromotionStatus();
		enterMessage();
		enterDescription();	
		clickRuletype();
		selectRuletype();
		clickActiontype();
		selectActiontype();
		promotionPriorityvalue();
		promotionSave();
		
		clickActionTab();
		clickEditAction();
//		ClickCreateAction();
		enterActionName();
		enterActionDescription();
		clickActionClassName();
		selectActionClassName();
//		enterofferValue();
		clickActionRule();
		clickRuleGroup();
		clickAction();
		actionValue();
		buyItemQuantity();
		enterGiftProductId();
		enterGiftSkuId();
		clickActionSave();
		
		clickLocale();
		selectLocale();
		enterDisplayMessage();
		clickActionAddUpdateSave();
		clickPromotionEditAndSave();
		
		clickProject();
		chevronEnterCreateProject();
		clickSubmit();
		/*clickProject();
		chevronentercurrentpage();*/
		clickReject();
			
		logout();
		
		
			
		
	}
	
	@AfterClass(alwaysRun=true)
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
