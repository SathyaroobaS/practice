package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.customerAdminComponents;

public class PrescriptforBusinessadmin extends customerAdminComponents
{
	
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
	}
	
	@Test
	public void Prescriptfor_Businessadmin() throws Exception 
	{
		generatesessionId();
//		inviteBusinessAdmin();
		inviteBusinessAdmin12();
		generateActivationParam();
		registerUser();
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
