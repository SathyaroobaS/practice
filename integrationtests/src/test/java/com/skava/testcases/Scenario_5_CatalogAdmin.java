package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_5_CatalogAdmin extends TestComponents
{

	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_5_CatalogAdmin() throws InterruptedException 
	{	
		// Login Verification
		randomNoCatlogGenerator();
		launchUrl();
		enterLoginCredentials();
		catalogsPageValidation();
		
		//create catalog
		createCatalogButtonClick();
		createCatalog();
		catalogIDSearch();
		createCatalogValidation();
		
		//edit catalog
		editCatalogButtonClick();
		editCatalog();
		catalogIDSearch();
		updatedCatalogValidation();
		viewSalesCatalog();
		
		//create project
		verifyProjectMenuIsClicked();
		createNewProject();
	    navigateToImportPage();
	    //navigateToProjectPage();
	    //PancakeClick();
		
		//run feed rules
	     
		feedMenuValidation();
		feedRulesPageRunFeed();
		defaultProjetProductsMenuClick();
		defaultProjetProductPageProductsCheck();
		defaultProjetSkuMenuClick();
		defaultProjetSkuPageSkuCheck();
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}


}
