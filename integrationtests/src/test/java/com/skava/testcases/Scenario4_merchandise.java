package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.MerchandiseadminComponents;
import com.skava.reusable.components.TestComponents;

public class Scenario4_merchandise extends TestComponents{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void VerifyCategoryEdit() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		isDisplayedListofCategory();
		clkPrjctDropdown();
		enterexistingproject();
		randomlySelectCategory();
		clkChevron();
		clkEditCategory();
		enterCategoryName();
		enterCategoryDescription();
		//clkcategoryTypeInEdit();
		clkCategoryIsVisibleInEdit();
		updateIsVisibleValueInEdit();
		mandatoryPropNameIsDisplayed();
		enterMandatoryPropValue();
		clickAddBtnProperty();
		clkPropertyName();
		selectPropertyName();
		enterOptionalPropertyValue();
		deleteProperty();
		saveCategory();
		successmsgofcategoryupdate();
		logout();
	}
	
	@AfterClass(alwaysRun=true)
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
