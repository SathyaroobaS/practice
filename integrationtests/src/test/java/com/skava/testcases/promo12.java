package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.skava.reusable.components.PromotionsComponents;



public class promo12 extends PromotionsComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void promotion12() throws Exception 
	{/*
		launchUrl();
		LogIn();
		clickProject();
		clickCreatenewproject();
		EnterNewprojectname();
		createproject();
		entercreateproject();
		ClickCreatePromotionGroup();
//		clickEnter();
		enterGroupName();
		clickdaterange();
		selectstartdate();
		selectenddate();
		clickApplyInDatePicker();
		clickStatus();
		selectstatusactive();
		priorityvalue();
		clickSave();
		
		clickPromotionCreateButton();
		enterPromotionName();
		clickdaterange();
		selectstartdate();
		selectenddate();
		clickApplyInDatePicker();
		clickPromotionStatus();
		selectPromotionstatus();
		enterMessage();
		enterDescription();	
		clickRuletype();
		selectRuletype();
		clickActiontype();
		selectActiontype();
		promotionPriorityvalue();
		promotionSave();
				
		promotionEdit();
        promotionEditEnterValue();
        promotionEditAndSave();	
        promo_click();
		promo_delete();
		logout();
		
		LogIn();
		clickProject();
		chevronentercreateproject();
		projectdetailsedit();
		enterdescription();
		enteraddnote();
		projectdetailsave();
		clickSubmit();
		clickProject();
		chevronentercurrentpage();
		clickApprove();
				
		logout();
		
				
	*/}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
