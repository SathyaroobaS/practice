package com.skava.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.reporting.BaseClass;
import com.framework.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.framework.action.ActionEngineFactory;
import com.skava.reusable.components.GeneralComponents;
import com.skava.reusable.components.FoundationComponents;
import com.skava.reusable.components.PromotionsComponents;

public class TC3_Test extends PromotionsComponents{
	
	@BeforeTest
	public void initTest() throws IOException{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}	
	
	@Test
	public void TC3() throws Exception {		
		launchUrl();
		verifyLoginSuccessful();
		
	}
	
	@AfterTest
	public synchronized void tearDown(){		
		if (driver != null) {
		driver.quit();	
		}
	}
}
