package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Pricing13 extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
	}
	
	@Test
	public void Pricing_Scenario_13_Create_Project_And_View_All_Created_Project_From_All_Project_Page() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		launchExistingCollection(); 
		verifyUserNavigatedToPriceListPage();
		clickCreatePriceList();
		verifyUserNavigatedToCreatePriceListPage();
		enterDetailsToCreatePriceList();
		searchCreatedPriceList();
		//verifyAndClickEditPriceList();
		verifyAndClickViewPriceList();
		verifyAndClickProjectDD();
		clickCreateProjectInEditPricingPage();
		createNewProjectInEditPricingPage();
		verifyCreatedProjectFromList();
		createNewProjectInEditPricingPage();
		verifyCreatedProjectFromList();
		clickViewAllProjectsFromPricelistPage();
		verifyCreatedProjectsAreDisplayedInAllProjectsPage();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
