package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_3_CatalogAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void Scenario_3_CatalogAdmin() throws Exception
	{
		// Login Verification
		launchUrl();
		enterLoginCredentials();
		
		// Catalogs Overview Page
		catalogsPageValidation();
		
		//Verify Locale
		verifyLocaleDropdown();
		verifystoreDropdown();
		
		//Edit master catalog details
		navigateToMasterCatalogPage();
		//PancakeClick();
		
		//Create New Project
		verifyProjectMenuIsClicked();
		createNewProject();
		navigateToImportPage();
		//PancakeClick();
		
		//Import Data Sheet
		verifyImportTabIsClicked();
		uploadImportDataSheet();
		clickImportButton();
		verifyImportSuccessMessageIsDisplayed();
		
		//Submit Project
		navigateToProjectPage();
		verifySubmitProjectFlow();
		
		//Reopen project
		clickProjectReopenBtn();
		verifySubmitProjectFlow();
		
		//Approve project
		verifyProjectIsApproved();
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
