package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_1_CatalogAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void Scenario_1_CatalogAdmin() throws Exception
	{
		// Login Verification
		launchUrl();
		enterLoginCredentials();
		
		// Catalogs Overview Page
		catalogsPageValidation();
		//PancakeClick();
		
		// Attributes creation
		verifyAttributesPageIsDisplayed();
		clickCreateAttributeBtn();
		selectFieldTypeFromDropdown();
		clickAttributeCreateButton();
		//verifyCreatedAttributeIsDisplayed();
		
		// Group Attribute creation
		clickGropuAttributeTab();
		clickCreateGroupAttributeBtn();
		clickAttributeCreateButton();
		
		/*// Variant Group creation
		clickVariantGropuTab();
		clickCreateVariantGroupBtn();
		clickAttributeCreateButton();
		deleteAllCreatedAttributes();*/	
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
