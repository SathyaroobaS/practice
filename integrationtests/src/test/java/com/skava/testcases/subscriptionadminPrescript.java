package com.skava.testcases;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.AfterClass; 
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.customerAdminComponents;


public class subscriptionadminPrescript extends customerAdminComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void customeradmin_Prescript() throws Exception 
	{
		generatesessionId();
		generateOrderauthToken();
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		createCollectionSubscription("Automation_Sub_Col_"+timeStamp);
		createMultiSubscription();
		updateStore();
		 
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}