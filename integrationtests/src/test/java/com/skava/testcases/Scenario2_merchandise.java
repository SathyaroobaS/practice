package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.MerchandiseadminComponents;
import com.skava.reusable.components.TestComponents;

public class Scenario2_merchandise extends TestComponents{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void Verify_Cancel_Prjct_Category_Creation() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		isDisplayedListofCategory();
		clkPrjctDropdown();
		createNewProjectButton();
		enterNewprojectname();
		cancelProjectCreation();
		createNewProjectButton();
		enterNewprojectname();
		clickCreateButton();
		successMsgOnProjectCreation();
		entercreatedproject();
		clickCreateCategoryBtn();
		cancelCategory();
		clickCreateCategoryBtn();
		categoryIdIsDisplayed();
		enterCategoryId();
		categoryNameIsDisplayed();
		enterCategoryName();
		categoryDescIsDisplayed();
		enterCategoryDescription();
		mandatoryPropNameIsDisplayed();
		enterMandatoryPropValue();
		clickAddBtnProperty();
		clkPropertyName();
		selectPropertyName();
		enterOptionalPropertyValue();
		semanticIdIsDisplayed();
		enterSemanticId();
		addSemanticId();
		metaTitleIsDisplayed();
		enterMetaTitle();
		addMetaTitle();
		metaDescIsDisplayed();
		enterMetaDescription();
		addMetaDesc();
		metakeywordIsDisplayed();
		enterMetaKeyword();
		addMetaKeyword();
		clkCreatebtn();
		clkPrjctDropdown();
		clkDetailOfOpen();
		submitBtn();
		isDisplayedApproveBtn();
		logout();
	}
	
	@AfterClass(alwaysRun=true)
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
