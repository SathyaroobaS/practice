package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_11_CatalogAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_11CatalogAdmin() throws InterruptedException 
	{	
		//Login Verification
		launchUrl();
		//driver.navigateToUrl("https://int.skavacommerce.com/admin/catalogs/?collectionId=1536&businessId=4&storeId=52");
		enterLoginCredentials();
		catalogsPageValidation();
		EnterIntoMasterCatalog();
		ClickProjectsDropdownAndSelectProject();
		
		// Review approved products
		defaultProjetSkuMenuClick();
		defaultProjetSkuPageSkuCheck();
		SelectProjectandEnter();
		ProjectIdComparisonInProjectListPage();
		
		// Create new proj to add approved items
		defaultProjetSkuMenuClick();				
		//Select recently create project from dropdown
		verifyProjectMenuIsClicked();
		//navigateToNewProjectPage();
		//defaultProjetSkuMenuClick();
		//defaultProjetSkuPageSkuCheck();
		//verifyProjectMenuIsClicked();
		//navigateToNewProjectPage();
				
		//
		/*SelectProjectDropdown();
		EnterRecentlyCreatedProject();
		defaultProjetSkuMenuClick();
				
		// Verify no data found in newly created project from dropdown
		ChooseNewlyCreatedProjFromProjectDropdown();
		clickSearchButtoninOverviewPage();
		ClickstatusDropdown();
		ChooseAllApprovedProjectDropdown();
		clickSearchButtoninOverviewPage();		*/		
				
		//Select product randomly from product list		
		//RandomSelectFromProductListfromSKU(); // Newly added for this testcase
		//alertWindowHandler();
		//basicInformationTabSelection();
		//UpdateBundleNameAndSave(); // Save button is not showing now in euat		
		//defaultProjetSkuMenuClick();
		

	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}

}