package com.skava.testcases;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.AfterClass; 
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.customerAdminComponents;


public class customeradminPrescript extends customerAdminComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void customeradmin_Prescript() throws Exception 
	{
		generatesessionId();
		generateOrderauthToken();
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		createCollectionCustomer("Automation_Cus_Col_"+timeStamp);
		createCollectionOrder("Automation_Ord_Col_"+timeStamp);
		createCollectionPayment("Automation_Pay_Col_"+timeStamp);
		updateStore();
		//createCustomerAdminUser();
		//createOrderForUser();
		 createUserAndOrder();
		 
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}