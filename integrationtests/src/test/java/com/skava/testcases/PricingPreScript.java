package com.skava.testcases;

import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class PricingPreScript extends TestComponents
{
	@Test
	public void Pricing_Pre_Script() throws Exception 
	{
		generatesessionId();
		getPricingBusinessCollection();
		createPricingCollection();
		createPriceListForPricing();
		//generatesessionIdfoundation();
		storeAssociateFoundation();
	}
}
