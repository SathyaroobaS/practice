package com.skava.testcases;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.skava.reusable.components.PromotionsComponents;



public class promotionPrescript extends PromotionsComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	
	@Test
	public void promotionadmin_Prescript() throws Exception 
	{
		generateSessionId();
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		createCollectionPromotion("Automation_Promotion_col_"+timeStamp);
		generateauthforpromotionservice("");
		promotionstoreassociate();
	
		 
	}
	
	@AfterClass(alwaysRun=true)
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
