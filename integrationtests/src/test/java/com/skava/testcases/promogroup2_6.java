package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.skava.reusable.components.PromotionsComponents;



public class promogroup2_6 extends PromotionsComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void promoGroup2Promo15() throws Exception 
	{
		
		launchUrl();
		logIn();
		
		clickProject();
		clickCreatenewproject();
		enterNewProjectName();
		clickCreateProject();
		enterCreateProject();
		clickCreatePromotionGroup();
		enterGroupName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickStatus();
		selectstatusactive();
		priorityValue();
		clickSave();
			
		clickPromotionCreateButton();
		enterPromotionName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickPromotionStatus();
		selectPromotionStatus();
		enterMessage();
		enterDescription();	
		clickRuletype();
		selectRuletype();
		clickActiontype();
		selectActiontype();
		promotionPriorityvalue();
		promotionSave();
		
		clickCloneBtn();
		clickPromotionGroupList();
		selectPromotionGroupList();
		enterGroupName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickStatus();
		selectstatusactive();
		priorityValue();
		clickCloneBtn();
		
		
			
		logout();
		
		
			
		
	}
	
	@AfterClass(alwaysRun=true)
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
