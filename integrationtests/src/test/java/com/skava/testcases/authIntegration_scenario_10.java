package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.InviteUserComponents;
import com.skava.reusable.components.TestComponents;

public class authIntegration_scenario_10 extends TestComponents {
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void scenario_10() throws Exception 
	{
		generatesessionId();
        inviteSuperAdmin();
        generateActivationParam();
        String email=registerUser();
		launchUrl();
		forgotPasswordButton();
		clickForgotPasswordLinkInLoginScreen();
		enteringUserRegisteredEmailAddress(email);
		clickingSendLinkButtonInForgotPassword();
		passwordReset(email);
		forgetPasswordDeepLink();
		enterValidNewPasswordInRestPasswordScreen();
		enterValidConfirmNewPasswordInRestPasswordScreen();
		clickSaveButtonInResetPasswordScreen();
		successMessageOnValidPasswordIsReset();
		launchUrl();
		doForgetPasswordLoginWithValidCrentials(email);
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
