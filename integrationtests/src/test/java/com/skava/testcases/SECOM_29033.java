package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class SECOM_29033 extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		
	}
	
	@Test
	public void SECOM29033() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		verifyListOfOrdersInDataTable();
		triggerViewOrder();
		verifyBreadcrumb();
		orderBreadcrumb();
		orderIdDisplayed();
		orderDateDisplayed();
//		orderValueDisplayed(); Removed since it will be part of order summary
		verifyOrderIDinDetailPage();
		orderPlacedDate();
		customerInfoDisplayed();
		customerNameDisplayed();
		customerEmailDisplayed();
		customerPhoneDisplayed();
		paymentInfoDisplayed();
		viewPaymentPopUp();
		taxDisplayed();
		orderSubTotalDisplayed();
		shippingDisplayed();
		discountDisplayed();
		orderGrandTotDisplayed();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}