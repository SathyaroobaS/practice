package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_2_StoreAdmin_UpdateStores extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_2_StoreAdmin_UpdateStores() throws InterruptedException 
	{
		/*verifyBusinessLitingScreen();
		searchNewlyCreatedBusinessFromList();
		clickViewBtn();
		VerifyStoreURLDisplay();
		searchNewlyCreatedStoresFromList();
		clickStoreEditBtn();
		clickStoreServiceAssociation();
		associateCartCheckoutServices();*/
		
		// Login
		//driver.navigateToUrl("https://euat.skavaone.com/admin/foundation/stores/storeops?businessId=2035&storeId=3476");
		launchUrl();
		enterLoginCredentials();
		
		// Search Business
		SearchExistingBusiness();
		BusinessIDComparision();
		NavigatetoSearchedBusiness();
		NavigateToStoresMenu();
		SearchExistingStore();
		
		/*// Search Store
		VerifyStoreOverview();
		BusinessNameNavigation();
		NavigateToStoresMenu();
		SearchExistingStore();*/
		
		// Edit Created Store
		ClickOnEditStorebuttonFromAllStorePage();
		EditStoreDetailsExceptStoreName();
		
		// Menu Navigation
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		
		// Add collection to the micro services
		VerifyMicroServiceTitle();
		MicroserviceURLVerification();
		
		// Edit Collection
		ClickOnMicroserviceLinkInBreadcrumb();
		EditCollectionDesc();
		
		// Associate Store
		NavigateToStoresMenu();
		ClickToStoreName();
		ClickToServiceAssociationTab();
		//AssociateMultipleCollection();
		associateCatalogServices();
//		associateCustomersServices();
		
		// Launch Multiple Collection
		NavigateToStoresMenu();
		NavigateToMicroServicesMenu();
		LaunchMultipleServices();
		
		// Store Association Validation
		NavigateToStoresMenu();
		SelectStoreOpsFromDropdown();
		ViewStoreNavigationforStoreOps();
		StoreNameComparision();
				 
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}

}