package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.AccountComponents;


@Test
public class AccountAdminPrescript extends AccountComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	public void AccountAdmin_Prescript() throws Exception 
	{
		generateaccountsessionId();
		generateauthforfoundationservice("");
		storecreate("");
		createCollectioncatalog("");
		createCollectioncustomer("");
		createCollectionpricing("");
		createCollectionaccounts("");
		accountstoreassocate();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
