package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_6_CatalogAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_6_CatalogAdmin() throws InterruptedException 
	{	
		launchUrl();
		enterLoginCredentials();
		catalogsPageValidation();
		EnterIntoMasterCatalog();
		ClickProjectsDropdownAndSelectProject();
		//PancakeClick();
		SelectProductsFromPancake();
		ClickonCreateItemButton();
		AddBundledItems();
		//verifyImportedProductsAvailable(); // Newly added to click products tab
		//searchSKU();
		//navigateToSearchedProduct();
		//UpdateBundleNameAndSave();
		//TempNavigateBack();
		//CompareBundleAndCollectionName();
		//PancakeClick();
		
		ClickonBundlesTab();
		ClickonAddProductButton();
		AddApprovedProductToBundle();
		ClickonAddProductButton();
		AddApprovedProductToBundle2();
		AddProductQuantity();
		saveButtonClick();
		verifyRelationsTabDisplay(); // Newly added method
		ClickonBundlesTab();
		ClickonAddProductButton();
		VerifyAddedProductisDisabled();
		DeleteAddedProduct();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
		  driver.quit();
		}
	}

}