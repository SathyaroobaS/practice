package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.EditUserComponents;
import com.skava.reusable.components.InstanceTeamComponents;
import com.skava.reusable.components.InviteUserComponents;
import com.skava.reusable.components.TestComponents;

public class authIntegration_scenario_3 extends TestComponents 
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void scenario_3() throws Exception 
	{
		//pre-script
		generatesessionId();
		inviteSuperAdmin();
		generateActivationParam();
        String email=registerUser();
		
		launchUrl();
		doLoginWithValidCrentials();
		clickInstanceInMenu();
		verifyInstanceTeamPage();
		searchWithEmailFilter(email);
		clickEditUser();
		verifyEditUserPage();
		clickStatusToggle();
		clickUpdateBtn();
		
		clickTeamBreadcrumb();
		verifyInstanceTeamPage();
		searchWithEmailFilter(email);
		verifyUserStatusInactive();
		clickEditUser();
		clickStatusToggle();
		clickUpdateBtn();
		
		clickTeamBreadcrumb();
		verifyInstanceTeamPage();
		searchWithEmailFilter(email);
		verifyUserStatusActive();
        
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
