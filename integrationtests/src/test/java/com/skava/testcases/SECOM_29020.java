package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.skava.reusable.components.TestComponents;

public class SECOM_29020 extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		
	}
	
	@Test
	public void SECOM29020() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
//		verifyOrderListPage();//QA attributes chnged
		orderIdFilterDisplayed();
		verifyDateRangeFilterDisplayed();
		verifyFirstNameFilterDisplayed();
		verifyEmailNameFilterDisplayed();
		verifyListOfOrdersInDataTable();
		orderIDFromTable();
		triggerOrderIdFilter();
		triggerResetButton();
		searchWithFirstName();
		searchResultForFirstName();
		triggerResetButton();
		searchWithEmail();
		searchResultForEmail();
	//	searchResultForDateRange(); //need to add code
		triggerResetButton();
		clickDateRange();
		clickMonth();
		selectMonth();
		selectYear();
		selectStartDate("1");
		selectEndDate();
		triggerApplyButton();
		clickSearchBtn();
		//triggerResetButton();
		paginationDisplayed();
		invalidOrderIdSearch();
		triggerResetButton();
		invalidFirstNameSearch();
		triggerResetButton();
		invalidEmailSearch();
		triggerResetButton();
		/*
		 * no. of rows selection removed
		 */
//		rowsDropDownDisplayed();
//		clickRowDropDown();
//   	selectRowVal(); 
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}