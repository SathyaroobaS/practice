package com.skava.testcases;

import java.io.IOException;
import org.testng.annotations.AfterClass; 
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.customerAdminComponents;


public class customerScenario2 extends customerAdminComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void customer_Scenario2() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		hoverandclickCustomers();
		clickPaymentsTab();
		clickAddNewPayments();
		verifyAddNewPaymentsSection();
		addSelectExpirationDate();
		addSelectExpirationYear();
		enterCardNumber();
		addCVVInput();
		addCardNames();
		addBillingNames();
		addVerifyBillingAddress();
		clickAndSaveBtn();
		clickDownArrow();
		clickEditButton();
		verifyRemoveBtn();
		removeOkBtn();
		
		
		
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
