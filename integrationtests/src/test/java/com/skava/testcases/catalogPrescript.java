package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class catalogPrescript extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void catalog_Prescript() throws Exception
	{
		generatesessionId();
        createCollections();
        //generatesessionIdfoundation();
        storeAssociateCatalog();
        createMasterProject();
        createMasterProduct1();
        createMasterSKU1();
        createMasterProduct2();
        createMasterSKU2();
        createMasterProduct3();
        createMasterSKU3();
        submitProject();
        approveProject();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
