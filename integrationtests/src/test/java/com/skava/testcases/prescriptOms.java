package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.skava.reusable.components.OrderListingPageComponents;

public class prescriptOms extends OrderListingPageComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		
	}
	
	@Test
	public void prescript_Oms() throws Exception 
	{
		generatesessionId();
		createCollectionpayment();
		createCollectionoms();
		generatesessionIdfoundation();
		storeassocatefoundation();
		generatesessionIdpayment();
		createCollectionproviderpayment();
		createpayment();
		createpaymentitem();
		generatesessionIdorder();
		createorder();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}