package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_10_CatalogAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_10CatalogAdmin() throws InterruptedException 
	{	
		//Login Verification
		launchUrl();
		enterLoginCredentials();
		catalogsPageValidation();
		EnterIntoMasterCatalog();
		ClickProjectsDropdownAndSelectProject();
		
		// Review approved products
		SelectProductsFromPancake();
		defaultProjetProductPageProductsCheck();
		SelectProjectandEnter();
		ProjectIdComparisonInProjectListPage();
		
		// Create new proj to add approved items
		SelectProductsFromPancake();
		verifyProjectMenuIsClicked();
		createNewProject();
		navigateToNewProjectPage();
		
		//Select product randomly from product list		
		RandomSelectFromProductList();
		alertWindowHandler();
		basicInformationTabSelection();
		UpdateBundleNameAndSave(); // Save button is not showing now in euat	
		//Compare edited values
		SelectProductsFromPancake();// Navigate to product tab

	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}

}