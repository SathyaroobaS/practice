package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.skava.reusable.components.PromotionsComponents;



public class promogroup1_6 extends PromotionsComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void promoGroup1Promo6() throws Exception 
	{
		
		launchUrl();
//		logIn();
//		ClickCreatePromotionGroup();
//		clickEnter();
		logIn();
		clickPromocodelist();
		clickCreatePromocodeList();
		enterPromocodeListName();
		clickStatus();
		selectstatusactive();
		clickSave();
		clickAddPromocode();
		enterDisplayCode();
		clickSaveFix();
		//clickPromocodeListPancakeMenu();
		clickPromotionsTab();
		
		
		clickProject();
		clickCreatenewproject();
		enterNewProjectName();
		clickCreateProject();
		enterCreateProject();
		clickCreatePromotionGroup();
		enterGroupName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickStatus();
		selectstatusactive();
		priorityValue();
		clickSave();
		
		clickPromotionCreateButton();
		enterPromotionName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickPromotionStatus();
		selectPromotionStatus();
		enterMessage();
		enterDescription();	
		clickRuletype();
		selectRuletype();
		clickActiontype();
		selectActiontype();
		promotionPriorityvalue();
		promotionSave();
		
		createCondition();
		clickAddRule();
		clickRuleGroup();
		clickRule();
		rulesValue();
		clickAddRule();
		clickANDButton();
		clickRuleGroup();
		clickRule();
		rulesValue();
		clickConditionSave();
		//updateChecking();
		
		clickActionTab();
		clickCreateAction();
		enterActionName();
		enterActionDescription();
		clickActionClassName();
		selectActionClassName();
		enterPurchaseQuantityValue();
		enterOfferValue();
		clickActionRule();
		clickRuleGroup();
		clickAction();
		actionValue();
		clickActionRule();
		clickORButton();
		clickRuleGroup();
		clickAction();
		actionValue();
		clickActionRule();
		clickANDButton();
		clickRuleGroup();
		clickAction();
		actionValue();
		clickActionSave();
		
		clickLocale();
		selectLocale();
		enterDisplayMessage();
		clickActionAddUpdateSave();
		
		clickPromoCodeTab();
		clickPromoCodeEditButton();
		clickPromocodeType();
		selectPromocodeType();
		/*enterOverallUsageCount();
		enterNoOfDays();*/
		clickPromocodeList();
		selectPromocodeList();
		clickPromocodeSave();
		clickPromotionEditAndSave();
		
		
		
		
//		clickProject();
//		enterDetailpage();
//		clickSubmit();
//		clickProject();
//		entercurrentpage();
		clickProject();
		chevronEnterCreateProject();
		clickSubmit();
		/*clickProject();
		chevronentercurrentpage();*/
		clickApprove();
		
		//VerifyPromotionId();
		
		logout();
		
		
	}
	
	@AfterClass(alwaysRun=true)
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
