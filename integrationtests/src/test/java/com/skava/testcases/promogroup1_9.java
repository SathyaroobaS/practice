package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.skava.reusable.components.PromotionsComponents;



public class promogroup1_9 extends PromotionsComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void promoGroup1Promo9() throws Exception 
	{
		
		launchUrl();
		logIn();
		
		clickProject();
		clickCreatenewproject();
		enterNewProjectName();
		clickCreateProject();
		enterCreateProject();
		clickCreatePromotionGroup();
		enterGroupName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickStatus();
		selectstatusactive();
		priorityValue();
		clickSave();
		
		
		//Promotion 1
		clickPromotionCreateButton();
		enterPromotionName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickPromotionStatus();
		selectPromotionStatus();
		enterMessage();
		enterDescription();	
		clickRuletype();
		selectRuletype();
		clickActiontype();
		selectActiontype();
		promotionPriorityvalue();
		promotionSave();
		
		clickActionTab();
		clickEditAction();
		enterActionName();
		enterActionDescription();
		clickActionClassName();
		selectActionClassName();	
		enterOfferValue();
		clickActionRule();
		clickRuleGroup();
		clickAction();
		actionValue();
		clickActionRule();
		clickORButton();
		clickRuleGroup();
		clickAction();
		actionValue();
		clickActionRule();
		clickANDButton();
		clickRuleGroup();
		clickAction();
		actionValue();
		clickActionSave();	
		
		clickLocale();
		selectLocale();
		enterDisplayMessage();
		clickActionAddUpdateSave();
		clickPromotionEditAndSave();
		/*	
		
		//Promotion 2
		promo_click();
		clickPromotionCreateButton();
		enterPromotionName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickPromotionStatus();
		selectPromotionStatus();
		enterMessage();
		enterDescription();	
		clickRuletype();
		selectRuletype();
		clickActiontype();
		selectActiontype2();
		promotionPriorityvalue();
		promotionSave();
		
		clickActionTab();
		clickEditAction();
		enterActionName();
		enterActionDescription();
		clickActionClassName();
		selectActionClassName();
		enterOfferValue();
		clickActionSave();
		
		
		
		//Promotion3
		promo_click();
		clickPromotionCreateButton();
		enterPromotionName();
		clickDateRange();
		selectStartDate();
		selectEndDate();
		clickApplyInDatePicker();
		clickPromotionStatus();
		selectPromotionStatus();
		enterMessage();
		enterDescription();	
		clickRuletype();
		selectRuletype();
		clickActiontype();
		selectActiontype3();
		promotionPriorityvalue();
		promotionSave();
		
		clickActionTab();
		clickEditAction();
		enterActionName();
		enterActionDescription();
		clickActionClassName();
		selectActionClassName();
		enterOfferValue();
		clickActionSave();
		*/
		
		clickProject();
		chevronEnterCreateProject();
		clickSubmit();
		clickApprove();
		
			
		logout();
		

		
	}
	
	@AfterClass(alwaysRun=true)
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
