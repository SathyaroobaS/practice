package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Pricing6 extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
	}
	
	@Test
	public void Pricing_Scenario_6_Create_SKU_Edit_SKU_Add_All_Price_Combination() throws Exception 
	{
		launchUrl();
		enterLoginCredentials();
		launchExistingCollection(); 
		verifyUserNavigatedToPriceListPage();
		clickCreatePriceList();
		verifyUserNavigatedToCreatePriceListPage();
		enterDetailsToCreatePriceList();
		searchCreatedPriceList();
		//verifyAndClickEditPriceList();
		verifyAndClickViewPriceList();
		verifyAndClickProjectDD();
		clickCreateProjectInEditPricingPage();
		createNewProjectInEditPricingPage();
		clickCreatedProjectFromListToEnterViaEnterButton();
		clickSKUTab();
		verifyAllSKUTabIsDisplayed();
		addNewSKUDetails();
		clickEditSKU();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		enterAndVerifyMinQty();
		enterAndVerifyMaxQty();
		setSKUStatusActive();
		clickSaveSKUAdditionalPrice();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		scheduleDateForSKU();
		clickSaveSKUAdditionalPrice();
		clickAddPricesToSKU();
		enterTransactionalPrice();
		enterAndVerifyMinQty();
		enterAndVerifyMaxQty();
		scheduleDateForSKU();
		clickSaveSKUAdditionalPrice();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
