package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.skava.reusable.components.PromotionsComponents;



public class promo6 extends PromotionsComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void promotion6() throws Exception 
	{/*
		launchUrl();
		LogIn();
		clickProject();
		clickCreatenewproject();
		EnterNewprojectname();
		createproject();
		entercreateproject();
		ClickCreatePromotionGroup();
		enterGroupName();
		clickdaterange();
		selectstartdate();
		selectenddate();
		clickApplyInDatePicker();
		clickStatus();
		selectstatusactive();
		priorityvalue();
		clickSave();
		
		clickPromotionCreateButton();
		enterPromotionName();
		clickdaterange();
		selectstartdate();
		selectenddate();
		clickApplyInDatePicker();
		clickPromotionStatus();
		selectPromotionstatus();
		enterMessage();
		enterDescription();	
		clickRuletype();
		selectRuletype();
		clickActiontype();
		selectActiontype();
		promotionPriorityvalue();
		promotionSave();
		
		createCondition();
		clickRuleGroup();
		clickRule("id");
		rulesValue("rulevalue1");
		clickAddrule();
		ClickORButton();
		clickRuleGroup();
		clickRule("categoryIds");
		rulesValue("rulevalue2");
		clickAddrule();
		ClickANDButton();
		clickRuleGroup();
		clickRule("size");
		rulesValue("rulevalue3");
		ClickSave();
		updateChecking();
		ClickActionTab();
		ClickCreateAction();
		enterActionname();
		enterActiondescription();
		ClickActionClassName();
		SelectActionClassName();
		enterofferValue();
		clickRuleGroup();
		clickAction("shippingPostalCode");
		actionValue("actionvalue1");
		clickAddrule();
		ClickORButton();
		clickRuleGroup();
		clickAction("shippingPostalCode");
		actionValue("actionvalue2");
		clickAddrule();
		ClickANDButton();
		clickRuleGroup();
		clickAction("paymentType");
		changecondition("6","not equal");
		actionValue("actionvalue3");
		ClickActionSave();
		
		clickProject();
		enterDetailpage();
		clickSubmit();
		clickProject();
		entercurrentpage();
		clickReject();
		
		
		logout();
		
		
			
		
	*/}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
