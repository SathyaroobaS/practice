package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_9_CatalogAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	@Test
	public void Scenario_9_CatalogAdmin() throws InterruptedException 
	{	
		//Login Verification
		launchUrl();
		enterLoginCredentials();
		//driver.navigateToUrl("https://euat.skavaone.com/admin/catalogs/?businessId=341&serviceName=catalog&collectionId=521");
		catalogsPageValidation();
		EnterIntoMasterCatalog();
		ClickProjectsDropdownAndSelectProject();
		//PancakeMenuSelection & Overview
		SelectProductsFromPancake();
		PancakeClick();
		VerifyStoresClickFromPancake();
		//Menuclicktoselectstores();
		TempNavigateBack();
		PancakeClick();
		Menuclicktoselectstores();
		VerifyStoresOverviewClickFromPancake();
		TempNavigateBack();
		//Businesslogonavigation
		BusinessLogoNavigation();
		
		

	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}

}