package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.TestComponents;

public class Scenario_2_CatalogAdmin extends TestComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void Scenario_2_CatalogAdmin() throws Exception
	{
		/*// Login Verification
		launchUrl();
		enterLoginCredentials();
		
		// Catalogs Overview Page
		catalogsPageValidation();
		PancakeClick();
		
		//Import Data Sheet
		verifyImportTabIsClicked();
		uploadImportDataSheet();
		clickImportButton();*/
		
		// Login Verification
        launchUrl();
        enterLoginCredentials();
        
        // Catalogs Overview Page
        catalogsPageValidation();
        verifyAttributesPageIsDisplayed();
        clickOverviewTab();
        //PancakeClick();
        navigateToMasterCatalogPage();
        navigateToImportPage();
        
        //Import Data Sheet
        verifyImportTabIsClicked();
        uploadImportDataSheet();
        clickImportButton();
        verifyImportSuccessMessageIsDisplayed();
        //PancakeClick();
        verifyImportedProductsAvailable();
        //PancakeClick();
        verifyImportedSkusAvailable();
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
