package com.framework.reporting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import org.apache.commons.net.ntp.TimeStamp;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import com.galenframework.reports.GalenTestInfo;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.framework.action.ActionEngine;
import com.skava.framework.action.ActionEngineFactory;
import com.skava.frameworkutils.Constants;
import com.skava.frameworkutils.ExcelReader;
import com.skava.frameworkutils.loggerUtils;
import net.sf.json.JSONObject;

public class BaseClass 
{	
	public ActionEngine driver;
	public static Properties properties = new Properties();
	public static Properties data = new Properties();
	public static String UserDir=Paths.get(".").toAbsolutePath().normalize().toString();
	String ConfigFilePath=Paths.get(".").toAbsolutePath().normalize().toString()+ "/Resources/Config.ini";
	public static JSONObject json = new JSONObject();
	public static String currentRunReportPath;
	//parametrization of endpoint
	public static String cmdUrl=System.getProperty("url"); 
	public static String msUrl=System.getProperty("msurl");
	public static String orchUrl=System.getProperty("orchurl");
	public static String appUrl="";
	public static List<GalenTestInfo> tests = new LinkedList<GalenTestInfo>();
	public static boolean suiteAbort=true;
	
	
	public static synchronized ActionEngine initiTest(String tcName) throws IOException 
	{		
		return ActionEngineFactory.getActionEngine(BaseClass.getBrowser(tcName));
	}
	
	public void setDriver(ActionEngine driver) 
	{
		this.driver = driver;
	}
	
	public static int getBrowser(String testCaseName) 
	{
		String strExectuionChannel=(String) json.getJSONArray(testCaseName).get(1);
		String strBrowserName=(String) json.getJSONArray(testCaseName).get(2);
		String strDeviceType=(String) json.getJSONArray(testCaseName).get(4);		
		String strDeviceName=(String) json.getJSONArray(testCaseName).get(5);
		int return_type = 0;
		
		if(strExectuionChannel.equalsIgnoreCase("LOCAL"))
		{
			switch (strBrowserName) 
			{
				case "Chrome" :
					return_type = Constants.LOCAL_BROWSER_CHROME;
					break;
					
				case "Firefox" :
					return_type = Constants.LOCAL_BROWSER_FIREFOX;
					break;
					
				case "InternetExplorer":
					return_type = Constants.LOCAL_BROWSER_IE;
					break;
					
				case "Safari":
					return_type = Constants.LOCAL_BROWSER_SAFARI;
					break;
					
				case "MicrosoftEdge":
					return_type = Constants.LOCAL_BROWSER_EDGE;
					break;
					
				default :
					return_type = Constants.LOCAL_BROWSER_CHROME;
			}
		}
		else
		{
			if(strDeviceType.equalsIgnoreCase("Desktop"))
			{
				switch (strBrowserName) 
				{
					case "Chrome" :
						return_type = Constants.SAUCE_DESKTOP_BROWSER_CHROME;
						break;
						
					case "Firefox" :
						return_type = Constants.SAUCE_DESKTOP_BROWSER_FIREFOX;
						break;
						
					case "InternetExplorer":
						return_type = Constants.SAUCE_DESKTOP_BROWSER_IE;
						break;
						
					case "Safari":
						return_type = Constants.SAUCE_DESKTOP_BROWSER_SAFARI;
						break;
						
					case "MicrosoftEdge":
						return_type = Constants.SAUCE_DESKTOP_BROWSER_EDGE;
						break;
						
					default :
						return_type = Constants.SAUCE_DESKTOP_BROWSER_CHROME;	
						break;
				}	
			}
			else if(strDeviceType.equals("Mobile"))
			{
				if(strDeviceName.contains("iPhone"))
				{
					if(strDeviceName.contains("Simulator"))
						return_type = Constants.SAUCE_MOBILE_SIMULATOR_IOS_BROWSER_SAFARI;
					else
						return_type = Constants.SAUCE_MOBILE_IOS_BROWSER_SAFARI;		
				}
				else
				{
					if(strDeviceName.contains("Emulator"))
						return_type = Constants.SAUCE_MOBILE_EMULATOR_ANDROID_BROWSER_CHROME;
					else
						return_type = Constants.SAUCE_MOBILE_ANDROID_BROWSER_CHROME;
				}
			}
			else if(strDeviceType.equalsIgnoreCase("Tablet"))
			{
				if(strDeviceName.contains("iPad"))
				{
					if(strDeviceName.contains("Simulator"))
						return_type = Constants.SAUCE_TABLET_SIMULATOR_IOS_BROWSER_SAFARI;
					else
						return_type = Constants.SAUCE_TABLET_IOS_BROWSER_SAFARI;		
				}
				else
				{
					if(strDeviceName.contains("Emulator"))
						return_type = Constants.SAUCE_TABLET_EMULATOR_ANDROID_BROWSER_CHROME;
					else
						return_type = Constants.SAUCE_TABLET_ANDROID_BROWSER_CHROME;	
				}
			}
		}
		return return_type;
	}
	
	public static synchronized JSONObject initBatchExec() throws IOException 
	{	
		Map<String, List> hashMap = new LinkedHashMap<String, List>();
		hashMap=ExcelReader.getBatchExecInfo(UserDir+properties.getProperty("TestRunnerPath"), properties.getProperty("RunConfigSheetName"));
		json.accumulateAll(hashMap);
		return json;
	}
	
	@BeforeSuite
	public void beforeSuite() throws IOException 
	{
		//Load properties file		
		 File file = new File(ConfigFilePath);		  
		 FileInputStream fileInput = null;
			try 
			{
				fileInput = new FileInputStream(file);
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}				
			
			try 
			{
				properties.load(fileInput);
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}
			//*******************************************parametrization of endpoints*******************************************
			if(cmdUrl==null)
				appUrl = properties.getProperty("ApplicationUrl");
			else
				appUrl = cmdUrl;
				properties.put("ApplicationUrl", appUrl);
			
				URI uri = null;
				try 
				{
					uri = new URI(properties.getProperty("ApplicationUrl"));
				} 
				catch (URISyntaxException e) 
				{
					e.printStackTrace();
				}
			
			String currentDomain = "https://"+uri.getHost();
	        properties.put("Domain",currentDomain);
	        
	        //Microservce url parameterization fix 
			if(msUrl==null) {
				msUrl = properties.getProperty("MSUrlDefault");
			}			
			properties.put("APIDomain", msUrl);
			//Orchestration url parameterization fix 
			if(orchUrl==null) {
				orchUrl = properties.getProperty("OrchUrlDefault");
			}			
			properties.put("Domain", orchUrl);
				
			//*******************************************parametrization of endpoints*******************************************			

				
			if(System.getProperty("suiteXmlFile")!=null)
			{
				if(System.getProperty("suiteXmlFile").equals("promotionadmin.xml"))
				{
					properties.put("ApplicationUrl", appUrl+properties.getProperty("promotionadmin").replaceAll("<<BusinessId>>", properties.getProperty("BusinessId")).replaceAll("<<StoreId>>", properties.getProperty("PromotionStoreId")));
				}
				else if(System.getProperty("suiteXmlFile").equals("commonadmin.xml"))
				{
					properties.put("ApplicationUrl", appUrl+properties.getProperty("commonadmin"));
				}
				else if(System.getProperty("suiteXmlFile").equals("pricingadmin.xml"))
				{
					String url = properties.getProperty("pricingadmin").replaceAll("<<BusinessId>>", properties.getProperty("BusinessId"));
					properties.put("ApplicationUrl", appUrl+url);
				}
				else if(System.getProperty("suiteXmlFile").equals("catalogadmin.xml"))
				{
					String url = properties.getProperty("catalogadmin").replaceAll("<<BusinessId>>", properties.getProperty("BusinessId")).replaceAll("<<StoreId>>", properties.getProperty("CatalogStoreId1"));
					properties.put("ApplicationUrl", appUrl+url);
				}
				else if(System.getProperty("suiteXmlFile").equals("merchandiseadmin.xml"))
				{
					String url = properties.getProperty("merchandiseadmin").replaceAll("<<BusinessId>>", properties.getProperty("BusinessId")).replaceAll("<<StoreId>>", properties.getProperty("MerchandiseStoreId"));
					properties.put("ApplicationUrl", appUrl+url);
				}
				else if(System.getProperty("suiteXmlFile").equals("accountadmin.xml"))
				{
					properties.put("ApplicationUrl", appUrl+properties.getProperty("commonadmin"));
					
				}
				else if(System.getProperty("suiteXmlFile").equals("orderadmin.xml"))
				{
					String url = properties.getProperty("orderadmin").replaceAll("<<BusinessId>>", properties.getProperty("BusinessId")).replaceAll("<<StoreId>>", properties.getProperty("OrderStoreId"));
					properties.put("ApplicationUrl", appUrl+url);
				}
				else if(System.getProperty("suiteXmlFile").equals("authorizationadmin.xml"))
				{
					String url = properties.getProperty("authorizationadmin");
					properties.put("ApplicationUrl", appUrl+url);
				}
				else if(System.getProperty("suiteXmlFile").equals("customeradmin.xml"))
				{
					String url =properties.getProperty("customeradmin").replaceAll("<<BusinessId>>", properties.getProperty("BusinessId")).replaceAll("<<StoreId>>", properties.getProperty("CustomerStoreId"));
					properties.put("ApplicationUrl", appUrl+url);
				}
				else if(System.getProperty("suiteXmlFile").equals("subscriptionadmin.xml"))
				{
					String url =properties.getProperty("subscriptionadmin").replaceAll("<<BusinessId>>", properties.getProperty("BusinessId")).replaceAll("<<StoreId>>", properties.getProperty("SubscriptionStoreId"));
					properties.put("ApplicationUrl", appUrl+url);
				}
			}
			
			//Read test runner file for test run configurations
			initBatchExec();
			
			//Setup reports path for current run 
			TimeStamp time=new TimeStamp(System.currentTimeMillis());	
			currentRunReportPath=UserDir+ properties.getProperty("ReportPath")+time;
			File files = new File(currentRunReportPath+"/Screenshots");
			files.mkdirs();
			ExtentManager.filePath=currentRunReportPath+"/Summary.html";
			ExtentManager.screenshotPath=currentRunReportPath+"/Screenshots";	
			loggerUtils.setLogFile();
			
			ExtentManager.sysInfo.put("Domain", properties.getProperty("Domain"));
	        if(System.getProperty("suiteXmlFile")!=null)
	        {
	        	ExtentManager.sysInfo.put("User Name", System.getProperty("suiteXmlFile"));
	        }
	}
	
	@BeforeMethod
    public void beforeMethod(Method method) 
	{
        ExtentTestManager.startTest(method.getName());
    }
    
    @AfterMethod
    protected void afterMethod(ITestResult result) 
    {
        if (result.getStatus() == ITestResult.FAILURE) 
        {
            ExtentTestManager.getTest().log(LogStatus.FAIL, result.getThrowable());
        } 
        else if (result.getStatus() == ITestResult.SKIP) 
        {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
        } 
        else 
        {
            ExtentTestManager.getTest().log(LogStatus.PASS, "Test passed");
        }
        
        ExtentManager.getReporter().endTest(ExtentTestManager.getTest());        
        ExtentManager.getReporter().flush();
    }
    
    protected String getStackTrace(Throwable t) 
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        return sw.toString();
    }
    
    
    public static void processErrorCodes(Exception e, String message) 
	{
    	try
    	{
    		switch(e.getClass().getSimpleName())
    		{
    		// handled only the top selenium exceptions
    		case "TimeoutException":
    			ExtentTestManager.getTest().log(LogStatus.FAIL, "Could not locate element '"+message+"'");
    			throw new NoSuchElementException("Could not locate element '"+message+"'");
    			
    		case "NullPointerException":
    			ExtentTestManager.getTest().log(LogStatus.FAIL, message+" value is NULL");
    			throw new NullPointerException(message+" value is NULL");
    			
    		case "ElementNotVisibleException":
    			ExtentTestManager.getTest().log(LogStatus.FAIL, message+" is not visible");
    			throw new ElementNotVisibleException(message+" is not visible");
    			
    		case "ElementNotSelectableException":
    			ExtentTestManager.getTest().log(LogStatus.FAIL, message+" is not selectable");
    			throw new ElementNotSelectableException(message+" is not selectable");
    			
    		case "NoSuchFrameException":
    			ExtentTestManager.getTest().log(LogStatus.FAIL, message+"is not available");
    			throw new NoSuchFrameException(message+" frame is not available");
    			
    		case "WebDriverException":
    			ExtentTestManager.getTest().log(LogStatus.FAIL, "Action is performed but browser is not reachable");
    			throw new WebDriverException("Action is performed but browser is not reachable");
    			
    		case "StaleElementReferenceException":
    			ExtentTestManager.getTest().log(LogStatus.FAIL, "element is no longer present in the DOM");
    			throw new StaleElementReferenceException(message+ "element is no longer present in the DOM");
    		}

    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
}
    
    // This method is use to skip the @Test and move to next testcase
    public void exitTest()
    {
    	throw new SkipException("Skipping the Test Method due to above Exception");

    }
    
    // This method skips the whole testsuite
    public void exitSuite()
    {
    		this.suiteAbort=false;
    		throw new SkipException("Skipping the whole suite due to above Exception");
    }
    
 }