package com.framework.reporting;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.galenframework.reports.GalenTestInfo;
import com.galenframework.reports.HtmlReportBuilder;
import com.galenframework.reports.model.LayoutReport;
import com.skava.framework.action.SeleniumActionEngine;

public class GalenReportManager {
	
	public static List<GalenTestInfo> testList = new LinkedList<GalenTestInfo>();
	public static void generateReport(String testCaseName, LayoutReport layoutReport) throws IOException
	{
		GalenTestInfo testcase=GalenTestInfo.fromString(testCaseName); 
		testcase.getReport().layout(layoutReport, testCaseName);
		testList.add(testcase);
		HtmlReportBuilder htmlReportBuilder = new HtmlReportBuilder();
		htmlReportBuilder.build(testList, "GalenReport");
	}

}
