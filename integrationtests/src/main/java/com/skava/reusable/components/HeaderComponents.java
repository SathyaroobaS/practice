package com.skava.reusable.components;

import com.skava.object.repository.Login_8;
import com.skava.object.repository.UserRegistration_8;

public class HeaderComponents extends ForgotPassword 
{	

	String screenHeader;

	/** Verifying Application Logo image in Login screen **/
	public void verifyApplicationLogo() {
		if(driver.isElementDisplayed(Login_8.sk_Logo, "Application Logo Image"))
			logPass("Displayed Logo Image");
		else
			logFail("Failed to Display Logo Image");
	}

	/** Verifying User Registration Screen Name **/
	public void verifyUserRegistrationScreen() {
		screenHeader = driver.getText(UserRegistration_8.sk_ScreenName, "Screen Name");
		if(driver.compareElementText(UserRegistration_8.sk_ScreenName, screenHeader, "Screen Name is displayed"))
			logPass("User Registration Screen Name is displayed");
		else
			logFail("Failed to display Screen Name");

	}


	/** Verifying Copyright content in Login screen **/
	public void verifyCopyrightContent() {

		/*String copyright = driver.getText(Login_8.txt_CopyrightContent, "Copyright content");
		driver.compareElementText(Login_8.txt_CopyrightContent, copyright, "Verified & Compared"); */

		if(driver.isElementDisplayed(Login_8.txt_CopyrightContent, "Application Logo Image"))
			logPass("Displayed Copyright content");
		else
			logFail("Failed to Display Copyright content");
	}

	/* public void navigateToSignInPage() 
	{
		driver.clickElement(HomePage.linkSignIn,"SignInLink");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Navigate to signin page");
	}
	 */
	/*
	public void enterSearchString() 
	{
		driver.enterTextAndSubmit(HomePage.searchInputField, ExcelReader.getData("General_Data","SearchString"),"searchField");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Search product");
	}
	 */

}
