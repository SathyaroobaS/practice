package com.skava.reusable.components;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;

import com.framework.reporting.BaseClass;
import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.FoundationObjects;
import com.skava.object.repository.PricingObjects;

public class PricingComponent extends AccountComponents
{
	boolean pricingCreated = false;
	Map<String,String> currentSKUPrices = new HashMap<>();
	Map<String,String> currentPriceListDetails = new HashMap<>();
	boolean toastSuccess = false;
	int currentTPCount = 0;
	int projectCount = 0;
	boolean collectionFound = false;
	String currentPricingCollectionId = "";
	String apiPriceList = "";
	int deleteCount=0;
	/*
	 * Verify Price List Page is loaded
	 */
	public void verifyUserNavigatedToPriceListPage()
	{
		driver.explicitWaitforVisibility(PricingObjects.lbl_allPriceList, "All Price List Page");
		if(driver.isElementDisplayed(PricingObjects.lbl_allPriceList, "All Price List Page"))
		{
			logPass("The user is navigate to All Price List Page.");
			//String[] expText = ExcelReader.getData("Pricing", "Title").split("<B>");
			String[] expText = "All Pricelists".split("<B>");
			String actText = driver.getText(PricingObjects.lbl_allPriceList, "All Price List Label");
			
			if(expText[0].equalsIgnoreCase(actText))
			{
				logPass("There is no mismatch in the expected and the actual text.");
			}
			else
			{
				logFail("There is some mismatch in the expected and the actual text.");
				logInfo("The expected text is : " + expText[0]);
				logInfo("The actual text is : " + actText);
			}
			
			if(driver.isElementDisplayed(PricingObjects.btn_createPriceList, "Create Pricelist Button"))
			{
				logPass("The Create Price List button is displayed in the All Price List Page.");
			}
			else
			{
				logFail("The Create Price List button is not displayed in the All Price List Page.");
			}
		}
		else
		{
			logFail("The user failed to navigate to All Price List Page.");
			skipException("Intentionally Failing the Script as the user failed to load the All Price List Page.");
		}
	}
	
	/*
	 * Verify Price List Page is loaded
	 */
	public void verifyUserNavigatedToCreatePriceListPage()
	{
		driver.explicitWaitforVisibility(PricingObjects.lbl_createPriceList, "Create Price List Page");
		if(driver.isElementDisplayed(PricingObjects.lbl_createPriceList, "Create Price List Page"))
		{
			logPass("The user is navigate to Create Price List Page.");
			String[] expText = ExcelReader.getData("Pricing", "Title").split("<B>");
			String actText = driver.getText(PricingObjects.lbl_createPriceList, "Create Price List Label");
			
			if(expText[1].equalsIgnoreCase(actText))
			{
				logPass("There is no mismatch in the expected and the actual text.");
			}
			else
			{
				logFail("There is some mismatch in the expected and the actual text.");
				logInfo("The expected text is : " + expText[1]);
				logInfo("The actual text is : " + actText);
			}
			
			if(driver.isElementDisplayed(PricingObjects.btn_savePriceList, "Save Pricelist Button"))
			{
				logPass("The Save Price List button is displayed in the Save Price List Page.");
			}
			else
			{
				logFail("The Save Price List button is not displayed in the Save Price List Page.");
			}
			
			if(driver.isElementDisplayed(PricingObjects.btn_cancelPriceList, "Cancel Pricelist Button"))
			{
				logPass("The Cancel Price List button is displayed in the Create Price List Page.");
			}
			else
			{
				logFail("The Cancel Price List button is not displayed in the Create Price List Page.");
			}
		}
		else
		{
			logFail("The user failed to navigate to Create Price List Page.");
			skipException("Intentionally Failing the Script as the user failed to load the Create Price List Page.");
		}
	}
	
	
	/*
	 * Click Create Price List
	 */
	public void clickCreatePriceList()
	{
		driver.explicitWaitforVisibility(PricingObjects.btn_createPriceList, "Create Price List");
		if(driver.isElementDisplayed(PricingObjects.btn_createPriceList, "Create Price List"))
		{
			logPass("The create Price List button is displayed in the Price List Page.");
			if(driver.jsClickElement(PricingObjects.btn_createPriceList, "Create Price List"))
			{
				logPass("The user was able to click on the create price list.");
				//verifyUserNavigatedToCreatePriceListPage();
			}
			else
			{
				logFail("The user failed to click on the create price list.");
			}
		}
		else
		{
			logFail("The create Price List button is not displayed in the Price List Page.");
		}
	}
	
	/*
	 * Enter Details in the Create Price List
	 */
	public void enterDetailsToCreatePriceList()
	{
		String priceName = ExcelReader.getData("Pricing", "PriceListName").concat(" #########");
		String randomPriceString = generateRandomAlphabetsWithNumbers(priceName);
		currentPriceListName = randomPriceString;
		if(driver.enterText(PricingObjects.txt_priceListName, currentPriceListName, "Pricing Name"))
		{
			logInfo("The entered price list name is : " + currentPriceListName);
			logPass("The user was able to enter the text in the Pricing Name List.");
			currentPriceListDetails.put("PriceListName", currentPriceListName);
		}
		else
		{
			logFail("The user failed to enter the text in the Pricing Name List.");
		}
		
		String priceDesc = ExcelReader.getData("Pricing", "PriceListDesc");
		currentProjectDesc = priceDesc+ " " +randomPriceString;
		if(driver.enterText(PricingObjects.txt_priceListDesc, currentProjectDesc, "Pricing Name"))
		{
			logInfo("The entered price desc is : " + currentProjectDesc);
			logPass("The user was able to enter the text in the Pricing Description text field.");
			currentPriceListDetails.put("PriceListDesc", currentProjectDesc);
		}
		else
		{
			logFail("The user was not able to enter the text in the Pricing Description text field.");
		}
		
		if(driver.moveToElementAndClick(PricingObjects.dd_currencyContainer, "Click ON Currency Container"))
		{
			logPass("The user was able to click on the Currency Container.");
			driver.waitForElementAttribute(PricingObjects.currencyListContainer, "style", "block", "Block Value");
			try 
			{
				Thread.sleep(2000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			String currentCurrencyValue = driver.getText(PricingObjects.currencyListValues, "Currency List Values");
			if(driver.clickElement(PricingObjects.currencyListValues, "Currency List Values"))
			{
				logInfo("The current currency value is : " + currentCurrencyValue);
				logPass("The user was able to click on the Currecncy List values.");
				currentPriceListDetails.put("PriceListCurrency", currentCurrencyValue);
			}
			else
			{
				logFail("The user failed to click on the Currecncy List Container.");
			}
		}
		else
		{
			logFail("The user failed to click on the Currency Container.");
		}
		
		if(driver.moveToElementAndClick(PricingObjects.dd_selectPriceType, "Click ON Currency Container"))
		{
			logPass("The user was able to click on the Select Price Type Container.");
			if(driver.isElementDisplayed(PricingObjects.priceTypeListValues, "Price Type Values"))
			{
				logPass("The Price Type Values is displayed.");
				int size = driver.getSize(PricingObjects.priceTypeListValues, "Price Type Values");
				if(size>0)
				{
					logPass("The Price Type is more than 1. The current size is : " + size);
					for(int i = 1; i<=size ; i++)
					{
						By priceTypeXpath = By.xpath("("+PricingObjects.priceTypeListValues.toString().replaceAll("By.xpath: ", "").concat(")[" + (i) + "]"));
						String getCurrentPriceText = driver.getText(priceTypeXpath, "Price Type Values");
						if(!(getCurrentPriceText.contains("Select"))&&!(getCurrentPriceText.contains("select")))
						{
							logInfo("The Current Price Text value is : " + getCurrentPriceText);
							if(driver.clickElement(priceTypeXpath, "Current Price Value Xpath"))
							{
								logPass("The user was able to click on the Price Value Xpath. The construcetd xpath is : " + priceTypeXpath);
								try 
								{
									Thread.sleep(2000);
								} 
								catch (InterruptedException e) 
								{
									e.printStackTrace();
								}
								currentPriceListDetails.put("PriceListPriceType", getCurrentPriceText);
								break;
							}
							else
							{
								logFail("The user failed to click on the Price Value Xpath. The construcetd xpath is : " + priceTypeXpath);
							}
						}
						else
						{
							logInfo("The Current Price Text value is : " + getCurrentPriceText);
						}
					}
				}
				else
				{
					logFail("The Price Type is less than 1. Hence the script is failed.");
				}
			}
			else
			{
				logFail("The Price Type Values is not displayed.");
			}
		}
		else
		{
			logFail("The user failed to click on the Currency Container.");
		}
		
		/*if(driver.jsClickElement(PricingObjects.priceChkBox, "Price Chk Box"))
		{
			logPass("The user was able to click on the Price Check box.");
			try 
			{
				Thread.sleep(2000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		else
		{
			logFail("The user failed to click on the Price Check box.");
		}*/
		
		if(driver.jsClickElement(PricingObjects.btn_AddPriceType, "Click Add Price"))
		{
			logPass("The user was able to click on the Add Price button.");
			try 
			{
				Thread.sleep(2000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		else
		{
			logInfo("The user failed to click on the Add Price button.");
		}
		
		if(driver.jsClickElement(PricingObjects.btn_savePriceList, "Save Price List"))
		{
			logPass("The user was able to click on the Create button in the Create Pricelist Page.");
			verifyPriceListSuccessMessageIsDisplayed();
		}
		else
		{
			logFail("The user failed to click on the Create button in the Create Pricelist Page.");
		}
	}
	
	/*
	 * Verify Price List is created
	 */
	public void verifyPriceListSuccessMessageIsDisplayed()
	{
		//driver.explicitWaitforVisibility(PricingObjects.priceListSaveCotainer, "Price List Save Container");
		if(driver.isElementDisplayed(PricingObjects.priceListSaveCotainer, "Price List Save Container"))
		{
			logPass("The Price List Container is displayed.");
			String successTitle = "Success";
			String expectedSuccessMessage = currentPriceListName;
			if(driver.isElementDisplayed(PricingObjects.priceListSaveCotainerTitle, "Save Container Success Title"))
			{
				String currentText = driver.getText(PricingObjects.priceListSaveCotainerTitle, "Save Container Success Title");
				if(currentText.equalsIgnoreCase(successTitle))
				{
					logPass("There is no mismatch in the expected and the actual text.");
				}
				else
				{
					logFail("There is some mismatch in the expected and the actual text.");
					logInfo("The actual text is : " + currentText);
					logInfo("The expected text is : " + successTitle);
				}
			}
			else
			{
				logFail("The success container title is not displayed.");
			}
			
			if(driver.isElementDisplayed(PricingObjects.priceListSaveSuccessMessage, "Save Container Success Message"))
			{
				String currentText = driver.getText(PricingObjects.priceListSaveSuccessMessage, "Save Container Success Message");
				if(currentText.contains(expectedSuccessMessage))
				{
					logPass("There is no mismatch in the expected and the actual text.");
				}
				else
				{
					logInfo("There is some mismatch in the expected and the actual text.");
					logInfo("The actual text is : " + currentText);
					logInfo("The expected text is : " + expectedSuccessMessage);
				}
			}
			else
			{
				logFail("The success container message is not displayed.");
			}
			
			if(driver.isElementDisplayed(PricingObjects.btn_priceSaveOk, "Price Save Ok Button"))
			{
				logPass("The success container Ok button is displayed.");
				closePriceListSuccessContainer();
			}
			else
			{
				logFail("The success container Ok button is not displayed.");
			}
		}
		else
		{
			logFail("The Price List Container is not displayed.");
			skipException("Intentionally failing the script as the Success Container is not displayed.");
		}
	}

	/*
	 * Click on Ok Container to close the price list.
	 */
	private void closePriceListSuccessContainer() 
	{
		if(driver.jsClickElement(PricingObjects.btn_priceSaveOk, "Price Save Ok Button"))
		{
			logPass("The user was able to click on the Ok button in the Success Container.");
			verifyUserNavigatedToPriceListPage();
		}
		else
		{
			logFail("The user failed to click on the Ok button in the Success Container.");
		}
	}
	
	/*
	 * Search Current Price List
	 */
	public void searchCreatedPriceList() 
	{
		try 
		{
			//currentPriceListName = "PI Automation Price List 563756584";
			driver.explicitWaitforVisibility(PricingObjects.priceListPageSearchRowContainer, "Search Price List Page Row");
			if(driver.isElementDisplayed(PricingObjects.priceListPageSearchRowContainer, "Search Price List Page Row"))
			{
				if (driver.isElementDisplayed(PricingObjects.btn_searchpriceList, "Search Button")) 
				{
					logPass("Price List Search Button is Displayed. ");
					if (driver.jsClickElement(PricingObjects.btn_searchpriceList, "Search Button")) 
					{
						logPass("User Able to click on Search Price List btn.");
						try 
						{
							Thread.sleep(1000);
						} 
						catch (InterruptedException e) 
						{
							e.printStackTrace();
						}
						if(driver.explicitWaitforVisibility(PricingObjects.txt_searchPricedropDown, "Price Search Text Box"))
						{
							logInfo("The Search Text Drop down is displayed.");
							try 
							{
								Thread.sleep(1000);
							} 
							catch (InterruptedException e) 
							{
								e.printStackTrace();
							}
							if(driver.enterText(PricingObjects.txt_searchPriceList, currentPriceListName ,"Current Price List"))
							{
								logPass("The user was able to enter the text om the Search Pricelist Name text field.");
								try 
								{
									Thread.sleep(1000);
								} 
								catch (InterruptedException e) 
								{
									e.printStackTrace();
								}
								if(driver.jsClickElement(PricingObjects.btn_search, "Search Button"))
								{
									logInfo("The Searched Text was : " + currentPriceListName);
									try 
									{
										Thread.sleep(1000);
									} 
									catch (InterruptedException e) 
									{
										e.printStackTrace();
									}
									logPass("The user was able to click on the Search button in the All Price List page.");
									loadAndWaitForLoadingGauage();
								}
								else
								{
									logFail("The user was not able to click on the Search button in the All Price List page.");
									skipException("Intentionally Failing the Script as the user failed to click the Search button.");
								}
							}
							else
							{
								logFail("The user failed to enter the text om the Search Pricelist Name text field.");
								skipException("Intentionally Failing the Script as the user failed to enter the Pricelist Name in the search text box.");
							}
						}
						else
						{
							logFail("The Search price name drop down is not displayed.");
							skipException("Intentionally Failing the Script as the Search Pricelist Name drop down is not displayed..");
						}
					} 
					else 
					{
						logFail("User Able to click on Search Price List btn.");
						skipException("Intentionally Failing the Script as the user failed to click on the Price Search List btn.");
					}
				} 
				else 
				{
					logFail("User Able to click on Search Price List btn.");
					skipException("Intentionally Failing the Script as the user failed to click on the Price Search List btn.");
				}
			}
			else
			{
				logFail("The Search Row container in the All Price List Page is not displayed.");
				skipException("Intentionally Failing the Script as the Search Row container in the All Price List Page is not displayed.");
			}
		}
		catch (Exception e) 
		{
			logFail("Error in search for the price List.The error was :  " +e.getMessage() );
			if (e instanceof SkipException) 
			{
				logFail("Error in search for the price List.The error was :  " +e.getMessage() );
			}
		}
	}
	
	/*
	 * Search Current Price List
	 */
	public void searchInActiveStatusProject() 
	{
		try 
		{
			By statusXpath = null;
			//currentPriceListName = "PI Automation Price List 563756584";
			driver.explicitWaitforVisibility(PricingObjects.priceListPageSearchRowContainer, "Search Price List Page Row");
			if(driver.isElementDisplayed(PricingObjects.priceListPageSearchRowContainer, "Search Price List Page Row"))
			{
				if (driver.isElementDisplayed(PricingObjects.btn_searchStatus, "Status Button")) 
				{
					logPass("Price List Search Button is Displayed. ");
					if (driver.jsClickElement(PricingObjects.btn_searchStatus, "Status Button")) 
					{
						logPass("User Able to click on Status List btn.");
						try 
						{
							Thread.sleep(1000);
						} 
						catch (InterruptedException e) 
						{
							e.printStackTrace();
						}
						if(driver.explicitWaitforVisibility(PricingObjects.txt_statusDropDown, "Status Drop Down"))
						{
							logInfo("The Staus Drop down is displayed.");
							try 
							{
								Thread.sleep(1000);
							} 
							catch (InterruptedException e) 
							{
								e.printStackTrace();
							}
							
							if(driver.isElementDisplayed(PricingObjects.txt_statusValues, "Status Value"))
							{
								int length = driver.getSize(PricingObjects.txt_statusValues, "Status Value");
								if(length>0)
								{
									boolean Inactivefound = false;
									for(int i = 1; i<=length; i++)
									{
										statusXpath = By.xpath(PricingObjects.txt_statusValues.toString().replaceAll("By.xpath: ", "").concat("[" + i + "]"));
										String txt = driver.getText(statusXpath, "Status Xpath");
										if(txt.equalsIgnoreCase("Inactive"))
										{
											Inactivefound = true;
											break;
										}
									}
									
									
									if(Inactivefound)
									{
										if(driver.jsClickElement(statusXpath, "Status Xpath"))
										{
											logPass("The user was able to click on the Inactive Status");
											searchCreatedPriceList();
											verifyAndClickEditPriceList();
											verifyUpdatedProjectName();
											verifyUpdatedProjectDesc();
										}
										else
										{
											logFail("The user was not able to click on the Inactive Status");
										}
									}
									else
									{
										logFail("The Inactive Status is not set. The used Xpath was : " + statusXpath);	
									}
								}
								else
								{
									logFail("The Status and Inactive option is wrong as the length is less than 1. The xpath used is  : " + PricingObjects.txt_statusValues);
								}
							}
							else
							{
								logFail("The Status and Inactive option is not displayed. The xpath used is  : " + PricingObjects.txt_statusValues);
							}
						}
						else
						{
							logFail("The Status List btn drop down is not displayed.");
							skipException("Intentionally Failing the Script as the Status List drop down is not displayed..");
						}
					} 
					else 
					{
						logFail("User Able to click on Status List btn.");
						skipException("Intentionally Failing the Script as the user failed to click on the Status List btn.");
					}
				} 
				else 
				{
					logFail("User Able to click on Status List btn.");
					skipException("Intentionally Failing the Script as the user failed to click on the Status List btn.");
				}
			}
			else
			{
				logFail("The Search Row container in the Status List btn is not displayed.");
				skipException("Intentionally Failing the Script as the Status List btn in the All Price List Page is not displayed.");
			}
		}
		catch (Exception e) 
		{
			logFail("Error in search for the price List.The error was :  " +e.getMessage() );
			if (e instanceof SkipException) 
			{
				logFail("Error in search for the price List.The error was :  " +e.getMessage() );
			}
		}
	}
	
	/*
	 * click Edit price List 
	 */
	public void verifyAndClickEditPriceList()
	{
		try 
		{
			/*if (driver.isElementDisplayed(PricingObjects.lnk_EditPriceList, "Edit Pricing List")) 
			{
				logPass("Edit Price List Container is Displayed. ");
				//Edit Price List Button Click.*/
				driver.waitForElementAttribute(FoundationObjects.loadingGuage, "class", "none", "Loading Guage Show");
				driver.explicitWaitforVisibility(PricingObjects.priceListRowContainer, "Price List Row Container");
				if(driver.moveToElementAndClick(PricingObjects.lnk_EditPriceList, "Click ON Edit Price List"))
				{
					loadAndWaitForLoadingGauage();
					logPass("The user was able to click on the Edit Price List.");
					if(driver.explicitWaitforVisibility(PricingObjects.lnk_EditPriceDetailsTab , "Edit Price Details Tab"))
					{
						logPass("Navigated to Edit PriceList Page.");
					}
					else
					{
						logFail("Navigation to Edit PriceList Page.");
					}
				}
				else
				{
					logFail("Unable to Click on Edit Pricelist");
				}
			/*}
			else 
			{
				logFail("Edit Price List Container is Not Displayed. ");
			}*/
		}
		catch (Exception e) 
		{
			logFail("Error in clicing the Edit price List.The error was : " +e.getMessage());
		}
	}
	
	/*
	 * View Price List
	 */
	public void verifyAndClickViewPriceList()
	{
		try 
		{
			driver.waitForElementAttribute(FoundationObjects.loadingGuage, "class", "none", "Loading Guage Show");
			driver.explicitWaitforVisibility(PricingObjects.priceListRowContainer, "Price List Row Container");
			if (driver.isElementDisplayed(PricingObjects.lnk_ViewPriceDetails, "View Pricing List")) 
			{
				logPass("View Price List Container is Displayed. ");
				//Edit Price List Button Click.
				if(driver.moveToElementAndClick(PricingObjects.lnk_ViewPriceDetails, "Click View Pricing List"))
				{
					logPass("The user was able to click on the View Pricing List.");
					loadAndWaitForLoadingGauage();
					if(driver.explicitWaitforVisibility(PricingObjects.lnk_EditSkuDetailsTab , "Price Sku Tab"))
					{
						logPass("Navigated to Edit PriceList Page .");
					}
					else
					{
						logFail("Navigation to Edit PriceList Page.");
					}
				}
				else
				{
					logFail("Unable to Click on View Pricelist");
				}
			}
			else 
			{
				logFail("View Price List Container is Not Displayed. ");
			}
		}
		catch (Exception e) 
		{
			logFail("Error in clicing the Edit price List.The error was : " +e.getMessage());
		}
	}
	
	/*
	 * View and Click on Project Tab 
	 */
	public void verifyAndClickProjectDD()
	{
		driver.explicitWaitforVisibility(PricingObjects.dd_Project, "Drop Down Project");
		if(driver.isElementDisplayed(PricingObjects.dd_Project, "Drop Down Project"))
		{
			logPass("The Project drop down is displayed.");
			if(driver.jsClickElement(PricingObjects.dd_Project, "Drop Down Project"))
			{
				logPass("The user was able to click on the Project drop down icon.");
			}
			else
			{
				logFail("The user failed to click on the Project Drop down.");
				skipException("Intentionally failing the script as the user failed to click on the Project Drop down in the Edit Page.");
			}
		}
		else
		{
			logFail("The Project drop down is not displayed.");
			skipException("Intentionally failing the script as the Project Drop down is not displayed in the Edit Page.");
		}
	}
	
	/*
	 * Wait for Create Project Container to load
	 */
	public boolean verifyCreateProjectContainer()
	{
		boolean createProjectCont = false;
		if(driver.isElementDisplayed(PricingObjects.createProjectContainer, "Create Projet Container"))
		{
			logInfo("The Create Project Container is displayed.");
			createProjectCont = true;
		}
		else
		{
			logFail("The Create Project Container is not displayed.");
		}
		return createProjectCont;
	}
	
	/* 
	 * Create New Project in Edit Price List Page
	 */
	public void clickCreateProjectInEditPricingPage()
	{
		try 
		{
			boolean contDisplayed = verifyCreateProjectContainer();
			if(contDisplayed)
			{
				if (driver.isElementDisplayed(PricingObjects.btn_CreateProject, "Create Project ")) 
				{
					logPass("Create Project is Displayed. ");
					if (driver.jsClickElement(PricingObjects.btn_CreateProject, "Create Project ")) 
					{
						logPass("Create Project is Clicked. ");
					} 
					else 
					{
						logFail("Unable to click Create Project.");
					}
				} 
				else 
				{
					logFail("Create Project is Not Displayed. ");
				}
			}
			else
			{
				logFail("The Create Projet Container is not displayed.");
			}
		}
		catch (Exception e) 
		{
			logFail("Error in clicking the create Project .The error was : " +e.getMessage());
		}
	}
	
	/*
	 * Enter New Project  in Edit Price List Page
	 */
	public void createNewProjectInEditPricingPage()
	{
		try 
		{
			projectCount = projectCount+1;
			String newProjectName = ExcelReader.getData("Pricing", "ProjectName").concat(" ######");
			currentProjectName = generateRandomAlphabetsWithNumbers(newProjectName);
			if (driver.isElementDisplayed(PricingObjects.txt_CreateProject, "Create Project ")) 
			{
				logPass("Create Project txt field is Displayed. ");
				
				if (driver.enterTextAndSubmit(PricingObjects.txt_CreateProject, currentProjectName , "Create Project "))
				{
					loadAndWaitForLoadingGauage();
					driver.waitForElementAttribute(FoundationObjects.loadingGuage, "class", "none", "Loading Guage Show");
					Thread.sleep(10000);
					logPass("Created Project is : " + currentProjectName);
					currentPriceListDetails.put("PriceListProject"+projectCount, currentProjectName);
				} 
				else 
				{
					logFail("Unable to create Project");
				}
			} 
			else 
			{
				logFail("Create Project is Not Displayed. ");
				skipException("Intentionally failing the script as the create project is not displayed. ");
			}
		}
		catch (Exception e) 
		{
			if(e instanceof SkipException)
			{
				skipException("");
			}
			logFail("Error in entering the new Project .The error was : " +e.getMessage());
		}
	}
	
	/*
	 * Validate Created Project is displayed.
	 */
	public void verifyCreatedProjectFromList()
	{
		boolean projectFound = false;
		driver.explicitWaitforVisibility(PricingObjects.createdProjectName, "Created Project Name List");
		if(driver.explicitWaitforVisibility(PricingObjects.createdProjectName, "Created Project Name List"))
		{
			logPass("The created project list is displayed.");
			int rowSize = driver.getSize(PricingObjects.createdProjectList, "Created Project List");
			for(int i = 1; i<=rowSize; i++)
			{
				By rowXpath = By.xpath(PricingObjects.createdProjectName.toString().replaceAll("By.xpath: ", "").concat("[" + (i) + "]"));
				String actText = driver.getText(rowXpath, "Current Project Name");
				if(actText.contains(currentProjectName))
				{
					projectFound = true;
					break;
				}
				else 
				{
					continue;
				}
			}
			
			if(projectFound)
			{
				logPass("The Created Project "+ currentProjectName + " is found among the List.");
			}
			else
			{
				logFail("The Created Project " + currentProjectName + " is not found among the List.");
			}
		}
		else
		{
			logFail("The created project list is not displayed.");
			skipException("Intentionally failing the script as the created project is not displayed.");
		}
	}
	
	/*
	 * Validate Created Project is displayed.
	 */
	public void clickCreatedProjectFromListToEnterViaCheveronButton()
	{
		boolean projectFound = false;
		int i = 0;
		loadAndWaitForLoadingGauage();
		try 
		{
			Thread.sleep(2500);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		driver.explicitWaitforVisibility(PricingObjects.createdProjectName, "Created Project List Name");
		if(driver.explicitWaitforVisibility(PricingObjects.createdProjectName, "Created Project List Name"))
		{
			logPass("The created project list is displayed.");
			int rowSize = driver.getSize(PricingObjects.createdProjectName, "Created Project List");
			for(i = 1; i<=rowSize; i++)
			{
				By rowXpath = By.xpath(PricingObjects.createdProjectName.toString().replaceAll("By.xpath: ", "").concat("[" + (i) + "]"));
				String actText = driver.getText(rowXpath, "Current Project Name");
				if(actText.contains(currentProjectName))
				{
					projectFound = true;
					break;
				}
				else 
				{
					continue;
				}
			}
			
			if(projectFound)
			{
				logPass("The Created Project "+ currentProjectName + " is found among the List.");
				By rowXpath = By.xpath(PricingObjects.createdProjectList.toString().replaceAll("By.xpath: ", "").concat("[" + (i) + "]")+PricingObjects.createdProjectCheveronIcon.toString().replaceAll("By.xpath: ", ""));
				if(driver.moveToElementAndClick(rowXpath, "Row Xpath"))
				{
					loadAndWaitForLoadingGauage();
					logPass("The expected cheveron icon to view is displayed.");
					verifyUserNavigatedToProjectDetailsPage();
				}
				else
				{
					logFail("The expected enter icon to view is not displayed to click.");
					skipException("Intentionally failing the script as the Cheveron button is not clicked.");
				}
			}
			else
			{
				logFail("The Created Project " + currentProjectName + " is not found among the List.");
				skipException("Intentionally failing the script as the Cheveron button is not displayed.");
			}
		}
		else
		{
			logFail("The created project list is not displayed.");
			skipException("Intentionally failing the script as the created project is not displayed.");
		}
	}
	
	/*
	 * Validate Created Project is displayed.
	 */
	public void clickCreatedProjectFromListToEnterViaEnterButton()
	{
		boolean projectFound = false;
		int i = 0;
		driver.explicitWaitforVisibility(PricingObjects.createdProjectName, "Created Project List Name");
		if(driver.explicitWaitforVisibility(PricingObjects.createdProjectName, "Created Project List Name"))
		{
			logPass("The created project list is displayed.");
			int rowSize = driver.getSize(PricingObjects.createdProjectName, "Created Project List");
			for(i = 1; i<=rowSize; i++)
			{
				By rowXpath = By.xpath(PricingObjects.createdProjectName.toString().replaceAll("By.xpath: ", "").concat("[" + (i) + "]"));
				String actText = driver.getText(rowXpath, "Current Project Name");
				if(actText.contains(currentProjectName))
				{
					projectFound = true;
					break;
				}
				else 
				{
					continue;
				}
			}
			
			if(projectFound)
			{
				logPass("The Created Project "+ currentProjectName + " is found among the List.");
				By rowXpath = By.xpath(PricingObjects.createdProjectList.toString().replaceAll("By.xpath: ", "").concat("[" + (i) + "]")+PricingObjects.createdProjectEnterExitIcon.toString().replaceAll("By.xpath: ", ""));
				if(driver.moveToElementAndClick(rowXpath, "Row Xpath"))
				{
					loadAndWaitForLoadingGauage();
					logPass("The expected enter button to view is displayed and clicked.");
					verifyUserNavigatedToProjectOverViewPage();
				}
				else
				{
					logFail("The expected enter icon to view is not displayed to click.");
					skipException("Intentionally failing the script as the Enter / Exit button is not clicked.");
				}
			}
			else
			{
				logFail("The Created Project " + currentProjectName + " is not found among the List.");
				skipException("Intentionally failing the script as the Enter / Exit button is not displayed.");
			}
		}
		else
		{
			logFail("The created project list is not displayed.");
			skipException("Intentionally failing the script as the created project is not displayed.");
		}
	}
	
	/*
	 * Wait For Project Details Container
	 */
	public void verifyUserNavigatedToProjectDetailsPage()
	{
		driver.explicitWaitforVisibility(PricingObjects.createdProjectDetailsContainer, "Created Project Details Container");
		if(driver.isElementDisplayed(PricingObjects.createdProjectDetailsContainer, "Created Project Details Container"))
		{
			logPass("The user was able to navigate to the Project Details page.");
		}
		else
		{
			logFail("The user failed to navigate to the Project Details page.");
			skipException("Intentionally failing the script as the user failed to navigate to the Project Details page.");
		}
	}
	
	/*
	 * Click on Edit Description Button
	 */
	public void verifyAndClickEditDescriptionButton()
	{
		driver.explicitWaitforVisibility(PricingObjects.btn_CreatedProjectEditDescription, "Edit Description Button");
		if(driver.isElementDisplayed(PricingObjects.btn_CreatedProjectEditDescription, "Edit Description Button"))
		{
			logPass("The Edit Description button in the Project Details Page is displayed.");
			if(driver.jsClickElement(PricingObjects.btn_CreatedProjectEditDescription, "Click Edit Description Button"))
			{
				logPass("The user was able to click on the Edit Description button in the Project Details page .");
			}
			else
			{
				logFail("The user failed to click on the Edit Description button in the Project Details page .");
				skipException("Intentionally failing the script as the user failed to click on the Edit Description button.");
			}
		}
		else
		{
			logFail("The Edit Description button in not displayed in the Project Details Page .");
			skipException("Intentionally failing the script as the Edit description button in the Project Details page.");
		}
	}

	/*
	 * Enter Description for the Created Project
	 */
	public void enterProjectDescriptionAndSave()
	{
		if(driver.explicitWaitforVisibility(PricingObjects.txt_EditDescriptionContainer, "Edit Descrption Container"))
			//&&(driver.isElementDisplayed(PricingObjects.txt_EditDescription, "Pricing Text Area")||driver.isElementDisplayed(PricingObjects.txt_EditDescription1, "Pricing Text Area")))
		{
			logPass("The edit descritpion text area is displayed.");
			String projectDesc = ExcelReader.getData("Pricing", "PriceListDesc");
			projectDesc = projectDesc+ " " +currentProjectName;
			if(driver.enterText(PricingObjects.txt_EditDescriptionContainer, projectDesc , "Edit Descrption Container")||driver.enterText(PricingObjects.txt_EditDescriptionContainer, projectDesc , "Edit Descrption Container"))
			{
				logPass("The User was able to enter the text in the Edit Description field.");
				if(driver.navigateWithKeys(Keys.TAB, 1, "Edit Description Save"))
				{
					loadAndWaitForLoadingGauage();
					logPass("The user was able to save the entered description.");
					validateCreatedDescription(projectDesc);
				}
				else
				{
					logFail("The user failed to click on the the description save button.");
				}
			}
			else
			{
				logFail("The user failed to enter the entered description.");
			}
		}
		else
		{
			logFail("The edit descritpion text area is not displayed.");
		}
	}
	
	/*
	 * Enter Description for the Created Project
	 */
	public void enterProjectNameAndSave()
	{
		loadAndWaitForLoadingGauage();
		if(driver.explicitWaitforVisibility(PricingObjects.txt_ProjectName, "Edit Project Name"))
		{
			logPass("The edit project name text area is displayed.");
			currentProjectName = currentProjectName + " " + currentProjectName;
			if(driver.enterText(PricingObjects.txt_ProjectName, currentProjectName , "Edit Project Name Container"))
			{
				logPass("The User was able to enter the text in the Project Name field.");
				if(driver.navigateWithKeys(Keys.TAB, 1, "Edit Project Name Save"))
				{
					loadAndWaitForLoadingGauage();
					logPass("The user was able to save the entered Project Name.");
					validateCreatedProjectName(currentProjectName);
				}
				else
				{
					logFail("The user failed to click on the the description save button.");
				}
			}
			else
			{
				logFail("The user failed to enter the entered description.");
			}
		}
		else
		{
			logFail("The edit descritpion text area is not displayed.");
		}
	}

	/*
	 * Validate Created Project Description
	 */
	private void validateCreatedDescription(String projectDesc) 
	{
		/*if(driver.isElementDisplayed(PricingObjects.txt_ProjectShortDesc, "Desc Value"))
		{
			String currentValue = driver.getText(PricingObjects.txt_ProjectShortDesc, "Desc Value");
			if(currentValue.contains(projectDesc))
			{
				logPass("There is no mismatch in the expected and the actual value.");
			}
			else
			{
				logFail("There is some mismatch in the expected and the actual value.");
				logInfo("The expected value was : " + projectDesc);
				logInfo("The actual value was : " + currentValue);
			}
		}
		else
		{
			logFail("The Short description in the Page is not displayed.");
		}*/
		
		if(driver.isElementDisplayed(PricingObjects.txt_EditDescriptionContainer, "Desc Value"))
		{
			String currentValue = driver.getElementAttribute(PricingObjects.txt_EditDescriptionContainer,"value", "Desc Value");
			if(currentValue.contains(projectDesc))
			{
				logPass("There is no mismatch in the expected and the actual value.");
			}
			else
			{
				logFail("There is some mismatch in the expected and the actual value.");
				logInfo("The expected value was : " + projectDesc);
				logInfo("The actual value was : " + currentValue);
			}
		}
		else
		{
			logFail("The Short description in the Page is not displayed.");
		}
	}
	
	/*
	 * Validate Created Project Description
	 */
	private void validateCreatedProjectName(String projectName) 
	{
		/*if(driver.isElementDisplayed(PricingObjects.txt_ProjectShortDesc, "Desc Value"))
		{
			String currentValue = driver.getText(PricingObjects.txt_ProjectShortDesc, "Desc Value");
			if(currentValue.contains(projectDesc))
			{
				logPass("There is no mismatch in the expected and the actual value.");
			}
			else
			{
				logFail("There is some mismatch in the expected and the actual value.");
				logInfo("The expected value was : " + projectDesc);
				logInfo("The actual value was : " + currentValue);
			}
		}
		else
		{
			logFail("The Short description in the Page is not displayed.");
		}*/
		
		if(driver.isElementDisplayed(PricingObjects.txt_ProjectName, "Desc Value"))
		{
			String currentValue = driver.getElementAttribute(PricingObjects.txt_ProjectName, "value", "Desc Value");
			if(currentValue.contains(projectName))
			{
				logPass("There is no mismatch in the expected and the actual value.");
			}
			else
			{
				logFail("There is some mismatch in the expected and the actual value.");
				logInfo("The expected value was : " + projectName);
				logInfo("The actual value was : " + currentValue);
			}
		}
		else
		{
			logFail("The Project name in the Page is not displayed.");
		}
	}
	
	/*
	 * Click and Enter Notes Tab
	 */
	public void clickNotesTab()
	{
		loadAndWaitForLoadingGauage();
		if(driver.clickElement(PricingObjects.btn_NotesTab, "Notes Tab"))
		{
			logPass("The user was able to click on the notes tab.");
			loadAndWaitForLoadingGauage();
		}
		else
		{
			logFail("The user failed to click on the notes tab.");
			skipException("Intentionally failing the script as the user failed to click on the Notes tab button.");
		}
	}
	
	/*
	 * Wait For Project Notes text area and Enter and Save
	 */
	public void verifyAndEnterProjectNotes()
	{
		if(driver.isElementDisplayed(PricingObjects.txt_ProjectNotes, "Project Notes")&&(driver.isElementDisplayed(PricingObjects.txt_ProjectNotesArea, "Pricing Text Area")))
		{
			logPass("The project notes tab not displayed.");
			String projectNotes = ExcelReader.getData("Pricing", "PriceListDesc");
			projectNotes = projectNotes+ " " +currentProjectName; 
			if(driver.enterText(PricingObjects.txt_ProjectNotesArea, projectNotes, "Project Notes"))
			{
				driver.navigateWithKeys(Keys.TAB, 1, "Tab One");
				loadAndWaitForLoadingGauage();
				logPass("The user was able to enter and submit the project description.");
				validateAddedProjectNotes(projectNotes);
			}
			else
			{
				logFail("The user failed to click on the the description save button.");
			}
		}
		else
		{
			logFail("The project notes tab is not displayed.");
			skipException("Intentionally failing the script as the project notes tab is not displayed.");
		}
	}
	
	/*
	 * Validate Created Project Description
	 */
	private void validateAddedProjectNotes(String projectDesc) 
	{
		boolean matchFound = false;
		driver.waitForElementAttribute(FoundationObjects.loadingGuage, "class", "none", "Loading Guage Show");
		try 
		{
			Thread.sleep(15000);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		
		if(driver.isElementDisplayed(PricingObjects.txt_AddedProjectNotes, "Desc Value"))
		{
			int size = driver.getSize(PricingObjects.txt_AddedProjectNotes, "Pricing Rows");
			for(int i = 1; i<=size; i++)
			{
				By addedNoteXpath = By.xpath(PricingObjects.txt_AddedProjectNotes.toString().replaceAll("By.xpath: ", "").concat("[" + (i) + "]"));
				String currentValue = driver.getText(addedNoteXpath, "Notes Value");
				if(currentValue.contains(projectDesc))
				{
					matchFound = true;
					break;
					
				}
				else
				{
					continue;
				}
			}
			
			if(matchFound)
			{
				logPass("There is no mismatch in the expected and the actual value.");
			}
			else
			{
				logFail("There is some mismatch in the expected and the actual value.");
				logInfo("The expected value was : " + projectDesc);
			}
		}
		else
		{
			logFail("The Short description in the Page is not displayed.");
		}
	}
	
	/*
	 * Wait For Project Details Container
	 */
	public void verifyUserNavigatedToProjectOverViewPage()
	{
		driver.explicitWaitforVisibility(PricingObjects.overViewSection, "Created Project Overview Container");
		if(driver.isElementDisplayed(PricingObjects.overViewSection, "Created Project Overview Container"))
		{
			logPass("The user was able to navigate to the Project Overview Details page.");
		}
		else
		{
			logFail("The user failed to navigate to the Project Overview Details page.");
			skipException("Intentionally failing the script as the user failed to navigate to the Project Overview Details page.");
		}
	}
	
	/*
	 * Click on SKU Tab
	 */
	public void clickSKUTab()
	{
		if(driver.jsClickElement(PricingObjects.btn_SkuTab, "SKU Tab"))
		{
			logPass("The user was able to click on the SKU Tab.");
			loadAndWaitForLoadingGauage();
		}
		else
		{
			logFail("The user failed to click on the SKU Tab.");
		}
	}
	
	/*
	 * Verify ALL SKU Tab is clicked
	 */
	public void verifyAllSKUTabIsDisplayed()
	{
		if(driver.explicitWaitforVisibility(PricingObjects.lbl_AllSku, "All SKU Label"))
		{
			logPass("The All Sku label is displayed.");
		}
		else
		{
			logFail("The All Sku label is not displayed.");
			skipException("Intentionally failing the script as the All SKU Tab is not opened.");
		}
	}
	
	/*
	 * Verify Add SKU Button is clicked
	 */
	public void clickAddSKU()
	{
		if(driver.explicitWaitforVisibility(PricingObjects.btn_addSKU, "Add SKU Btn"))
		{
			logPass("The Add Sku Button is displayed.");
			if(driver.jsClickElement(PricingObjects.btn_addSKU, "Add SKU Btn"))
			{
				logPass("The user was able to click on the Add SKU button in the SKU Page.");
			}
			else
			{
				logFail("The user was not able to click on the Add SKU button in the SKU Page.");
			}
		}
		else
		{
			logFail("The Add Sku Button is not displayed.");
			skipException("Intentionally failing the script as the Add Sku Button is not displayed.");
		}
	}
	
	/*
	 * Verify Add SKU Button is clicked
	 */
	public void verifyAddSKUContainer()
	{
		if(driver.explicitWaitforVisibility(PricingObjects.addSKUContainer, "Add SKU Btn Container"))
		{
			logPass("The Add Sku Container is displayed.");
		}
		else
		{
			logFail("The Add Sku Container is not displayed.");
			skipException("Intentionally failing the script as the Add Sku Container is not displayed.");
		}
	}
	
	/*
	 * Add SKU Details
	 */
	public void addNewSKUDetails()
	{
		try
		{
			String[] sKUID = ExcelReader.getData("Pricing", "SKUIDS").split("<B>");
			String[] currentTransactionPrice = ExcelReader.getData("Pricing", "TransactionPrice").split("<B>");
			int length = sKUID.length;
			for(int i = 0; i<length; i++)
			{
				clickAddSKU();
				verifyAddSKUContainer();
				if(driver.isElementDisplayed(PricingObjects.txt_SkudId, "SKU ID")||driver.isElementDisplayed(PricingObjects.txt_SkudId1, "SKU ID")&&(driver.isElementDisplayed(PricingObjects.txt_TransactionPrice, "Transaction Price")))
				{
					logPass("The user was able to enter SKU ID.");
					currentSkuId = sKUID[i];
					String skuTransactionPrice = currentTransactionPrice[i];
					if(driver.enterText(PricingObjects.txt_SkudId, currentSkuId, "SKU IDS")||driver.enterText(PricingObjects.txt_SkudId1, currentSkuId, "SKU IDS"))
					{
						logPass("The user was able to enter the SKU ID to the Add SKU Container");
						logInfo("The Added SKU was : " +currentSkuId);
					}
					else
					{
						logFail("The user failed to enter the SKU ID to the Add SKU Container");
						skipException("Intentionally failing the script as the SKU ID is not entered.");
					}
					
					if(driver.enterText(PricingObjects.txt_TransactionPrice, skuTransactionPrice, "SKU Transaction Price"))
					{
						currentSKUPrices.put("TransactionPrice", skuTransactionPrice);
						logPass("The user was able to enter the SKU Transaction Price to the Add SKU Container");
						logInfo("The Added SKU Price was : " +skuTransactionPrice);
					}
					else
					{
						logFail("The user failed to enter the SKU Transaction Price to the Add SKU Container");
						skipException("Intentionally failing the script as the SKU Transaction Price is not entered.");
					}
					
					if(driver.jsClickElement(PricingObjects.btn_AddSKUDetails, "Save SKU Details"))
					{
						waitForToastMessage();
						loadAndWaitForLoadingGauage();
						
						/*
						 * Temp Fix
						 */
						driver.pageReload();
						clickSKUTab();
						loadAndWaitForLoadingGauage();
						
						logPass("The user was able to save the SKU details");
					}
					else
					{
						logFail("The user was not able to save the SKU details");
						skipException("Intentionally failing the script as the SKU details is not saved.");
					}
				}
				else
				{
					logFail("The SKU ID is not displayed.");
					skipException("Intentionally failing the script as the SKU ID is not displayed.");
				}
			}
		}
		catch (Exception e) 
		{
			logFail("There is some issue in entering the SKU Details. The issue is : " + e.getMessage());
			skipException("Intentionally failing the script as there is some issue in SKU details.");
		}
	}
	
	/*
	 * Edit the SKU Details
	 */
	public boolean editSKUDetails()
	{
		driver.pageReload();
		boolean skuRowDisplayed = false;
		if(driver.explicitWaitforVisibility(PricingObjects.skuRowContainer, "SKU Row Container"))
		{
			logPass("The SKU Row container is displayed.");
			skuRowDisplayed = true;
		}
		else
		{
			logFail("The SKU Row container is not displayed.");
		}
		return skuRowDisplayed;
	}
	
	/*
	 * Click Edit Button in SKU Row Container
	 */
	public void clickEditSKU()
	{
		
		boolean skuDisplayed = editSKUDetails();
		if(skuDisplayed)
		{
			boolean skuFound = false;
			String clickSku = ExcelReader.getData("Pricing", "SKUIDS");
			int row = driver.getSize(PricingObjects.skuRowContainer, "Sku Row");
			int i = 0;
			By editXpath = null;
			for(i = 1;i<=row; i++)
			{
				By idValue = By.xpath("(" + PricingObjects.skuIdValue.toString().replaceAll("By.xpath: ", "").concat(")[" + (i) + "]"));
				editXpath = By.xpath("(" + PricingObjects.btn_EditSKU.toString().replaceAll("By.xpath: ", "").concat(")[" + (i) + "]"));
				String currentSKU = driver.getText(idValue, "SKU Value");
				if(clickSku.equalsIgnoreCase(currentSKU))
				{
					logInfo("The expected sku is found.");
					skuFound = true;
					break;
				}
				else
				{
					continue;
				}
			}
			
			if(skuFound)
			{
				logPass("The expected sku to edit " + clickSku + " is found.");
				driver.maximizeBrowser();
				if(driver.moveToElementAndClick(editXpath, "Edit SKU Btn"))
				{
					logPass("The button to edit the SKU is displayed and clicked.");
					loadAndWaitForLoadingGauage();
					verifyUserNavigatedToEditSKUPage();
				}
				else
				{
					logFail("The button to edit the SKU is not displayed.");
					skipException("The edir button to edit the expected SKU "  + clickSku + " is not found. Hence Skipping the test.");
				}
			}
			else
			{
				logFail("The expected sku to edit " + clickSku + " is not found.");
				skipException("The expectewd SKU "  + clickSku + " to edit is not found. Hence Skipping the test.");
			}
		}
		else
		{
			logFail("The SKU Row container is not displayed.");
			skipException("The SKU Row container is not displayed.");
		}
	}
	
	/*
	 * Verify User navigated to Edit SKU Page
	 */
	public void verifyUserNavigatedToEditSKUPage()
	{
		loadAndWaitForLoadingGauage();
		if(driver.isElementDisplayed(PricingObjects.overViewSection, "Over View Section")&&(driver.isElementDisplayed(PricingObjects.pricesTab, "Price Tab")))
		{
			logPass("The user was able to navigate to Edit SKU Page.");
		}
		else
		{
			logFail("The user failed to navigate to Edit SKU Page.");
			//skipException("Intentionally failing the script as the Edit SKU Page is not displayed.");
		}
	}
	
	/*
	 * Add Prices to SKU 
	 */
	public void clickAddPricesToSKU()
	{
		//driver.refreshPage();
		if(driver.isElementDisplayed(PricingObjects.btn_AddPricesToSKU, "Add Price SKU"))
		{
			logPass("The Add Price button is displayed.");
			if(driver.jsClickElement(PricingObjects.btn_AddPricesToSKU, "Add Price SKU"))
			{
				logPass("The user was able to click on the Add Price button in the Edit SKU Price page.");
				loadAndWaitForLoadingGauage();
			}
			else
			{
				logFail("The user failed to click on the Add Price button in the Edit SKU Price page.");
				skipException("Intentionally failing the script as the user failed to click on the Add Price button.");
			}
		}
		else
		{
			logFail("The Add Price button is not displayed.");
		}
	}
	
	/*
	 * Verify Add Price Container is displayed.
	 */
	public boolean verifyAddPriceContainerIsDisplayed()
	{
		boolean priceContainerDisplayed = false;
		if(driver.isElementDisplayed(PricingObjects.addPriceContainer, "Add Price Container"))
		{
			logPass("The add price container is displayed.");
			priceContainerDisplayed = true;
		}
		else
		{
			logFail("The add price container is not displayed.");
		}
		return priceContainerDisplayed;
	}
	
	/*
	 * Verify Add Price Container is displayed.
	 */
	public boolean verifyAddSKUPriceContainerIsDisplayed()
	{
		boolean priceContainerDisplayed = false;
		if(driver.isElementDisplayed(PricingObjects.addSKuPriceContainer, "Add SKU Price Container"))
		{
			logPass("The add SKU price container is displayed.");
			priceContainerDisplayed = true;
		}
		else
		{
			logFail("The add SKU price container is not displayed.");
		}
		return priceContainerDisplayed;
	}
	
	/*
	 * Add Min QTY
	 */
	public void enterAndVerifyMinQty()
	{
		boolean priceContaineDisplayed = verifyAddPriceContainerIsDisplayed();
		if(priceContaineDisplayed)
		{
			logPass("The add price container is displayed.");
			String minQty = ExcelReader.getData("Pricing", "MinQty");
			if(!minQty.equalsIgnoreCase("NA"))
			{
				if(driver.isElementDisplayed(PricingObjects.txt_MinQty, "Min Qty Field"))
				{
					if(driver.enterText(PricingObjects.txt_MinQty, minQty , "Min Qty Field"))
					{
						currentSKUPrices.put("MinQty", minQty);
						logPass("The user was abke to enter the value : " + minQty + "  into the min qty field.");
					}
					else
					{
						logFail("The user failed to enter the value : " + minQty  + "  into the min qty field.");
					}
				}
				else
				{
					logFail("The Min Qty is not displayed.");
				}
			}
		}
		else
		{
			logFail("The add price container is not displayed.");
			skipException("Intentionally Skipping the test as the add price container is not displayed.");
		}
	}
	
	/*
	 * Add Max QTY
	 */
	public void enterAndVerifyMaxQty()
	{
		boolean priceContaineDisplayed = verifyAddPriceContainerIsDisplayed();
		if(priceContaineDisplayed)
		{
			logPass("The add price container is displayed.");
			String maxQty = ExcelReader.getData("Pricing", "MaxQty");
			if(!maxQty.equalsIgnoreCase("NA"))
			{
				if(driver.isElementDisplayed(PricingObjects.txt_MaxQty, "Max Qty Field"))
				{
					if(driver.enterText(PricingObjects.txt_MaxQty, maxQty , "Min Qty Field"))
					{
						currentSKUPrices.put("MaxQty", maxQty);
						logPass("The user was able to enter the value : " + maxQty + "  into the Max qty field.");
					}
					else
					{
						logFail("The user failed to enter the value : " + maxQty  + "  into the Max qty field.");
					}
				}
				else
				{
					logFail("The Max Qty is not displayed.");
				}
			}
		}
		else
		{
			logFail("The add price container is not displayed.");
			skipException("Intentionally Skipping the test as the add price container is not displayed.");
		}
	}
	
	/*
	 * Click and Set Price Status as Active
	 */
	public void setSKUStatusActive()
	{
		if(driver.isElementDisplayed(PricingObjects.dd_PriceStatus, "Price Status"))
		{
			logInfo("The Price Status drop down is not displayed.");
			if(driver.moveToElementAndClick(PricingObjects.dd_PriceStatus, "Price Status"))
			{
				logPass("The user was able to click on the Status drop down.");
				
				int size = driver.getSize(PricingObjects.dd_PriceActiveStatus, "SKU Status Values");
				if(size>0)
				{
					logPass("The SKU Status is more than 1. The current size is : " + size);
					for(int i = 1; i<=size ; i++)
					{
						By skuValueXpath = By.xpath("("+PricingObjects.dd_PriceActiveStatus.toString().replaceAll("By.xpath: ", "").concat(")[" + (i) + "]"));
						String getCurrentSKUText = driver.getText(skuValueXpath, "Sku Values");
						if(!(getCurrentSKUText.contains("Status"))&&!(getCurrentSKUText.contains("status")))
						{
							logInfo("The Current Price Text value is : " + getCurrentSKUText);
							if(driver.clickElement(skuValueXpath, "Current SKU Value Xpath"))
							{
								logPass("The user was able to click on the Price Value Xpath. The construcetd xpath is : " + skuValueXpath);
								try 
								{
									Thread.sleep(2000);
								} 
								catch (InterruptedException e) 
								{
									e.printStackTrace();
								}
								break;
							}
							else
							{
								logFail("The user failed to click on the Price Value Xpath. The construcetd xpath is : " + skuValueXpath);
							}
						}
						else
						{
							logInfo("The Current SKU Text value is : " + getCurrentSKUText);
						}
					}
				}
				else
				{
					logFail("The SKU Status is less than 1. Hence the script is failed.");
				}
				
				/*if(driver.selectByValue(PricingObjects.dd_PriceActiveStatus, "Active", "Active Status"))
				{
					logPass("The user was abel to select the active status.");
				}
				else
				{
					logFail("The user failed to select the active status.");
				}*/
			}
			else
			{
				logFail("The user failed to click on the Drop Down Status.");
			}
		}
		else
		{
			logFail("The Price Status is not displayed.");
			skipException("The price status drop down is not displayed.");
		}
	}
	
	/*
	 * Click Save SKU
	 */
	public void clickSaveSKUAdditionalPrice()
	{
		
		if(driver.jsClickElement(PricingObjects.btn_SaveAdditionalPricesToSKU, "Additional Price To SKU"))
		{
			logPass("The user was able to click on the Add button in the Add Price Container.");
			waitForToastMessage();
			//if(this.getClass().getSimpleName().equalsIgnoreCase("Pricing8")&&deleteCount==0) {
			//	logInfo("number increment "+deleteCount);
				//deleteSku();
				//verifyClickDeletePrice();
			//}
			verifyAddedPriceIsDisplayed();
			
			//deleteCount++;
		}
		else
		{
			logFail("The user failed to click on the Add button in the Add Price Container.");
		}
	}
	
	/*
	 * Wait For Toast Message
	 */
	public void waitForToastMessage()
	{
		toastSuccess = false;
		
		driver.explicitWaitforVisibility(PricingObjects.toastMessage, "Toast Message");
		String txt = driver.getText(PricingObjects.toastMessage, "Toast Message");
		logInfo("The Toast Message is : " + txt);
		if(txt.contains("Success")||(txt.contains("successfully")))
		{
			toastSuccess = true;
		}
		
		try 
		{
			Thread.sleep(2500);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		
		driver.explicitWaitforInVisibility(PricingObjects.toastMessage, "Toast Message");		
	}
	
	/*
	 * Enter transactional price
	 */
	public void enterTransactionalPrice()
	{
		boolean priceContaineDisplayed = verifyAddPriceContainerIsDisplayed();
		if(priceContaineDisplayed)
		{
			currentSKUPrices.clear();
			logPass("The add price container is displayed.");
			currentTPCount = currentTPCount+1;
			String transactionalPrice = ExcelReader.getData("Pricing", "TransactionPrice"+currentTPCount);
			if(!transactionalPrice.equalsIgnoreCase("NA"))
			{
				if(driver.isElementDisplayed(PricingObjects.txt_TransactionalPrice, "TransactionPrice Field"))
				{
					if(driver.enterText(PricingObjects.txt_TransactionalPrice, transactionalPrice , "TransactionPrice Field"))
					{
						currentSKUPrices.put("TransactionPrice", transactionalPrice);
						logPass("The user was able to enter the value : " + transactionalPrice + "  into the TransactionPrice field.");
					}
					else
					{
						logFail("The user failed to enter the value : " + transactionalPrice  + "  into the TransactionPrice field.");
					}
				}
				else
				{
					logFail("The TransactionPrice is not displayed.");
				}
			}
		}
		else
		{
			logFail("The add price container is not displayed.");
			skipException("Intentionally Skipping the test as the add price container is not displayed.");
		}
	}
	
	/*
	 * Enter transactional price
	 */
	public void enterFloorPrice()
	{
		boolean priceContaineDisplayed = verifyAddPriceContainerIsDisplayed();
		if(priceContaineDisplayed)
		{
			logPass("The add price container is displayed.");
			String FloorPrice = ExcelReader.getData("Pricing", "FloorPrice");
			if(!FloorPrice.equalsIgnoreCase("NA"))
			{
				if(driver.isElementDisplayed(PricingObjects.txt_FloorPrice, "FloorPrice Field"))
				{
					if(driver.enterText(PricingObjects.txt_FloorPrice, FloorPrice , "FloorPrice Field"))
					{
						currentSKUPrices.put("FloorPrice", FloorPrice);
						logPass("The user was able to enter the value : " + FloorPrice + "  into the FloorPrice field.");
					}
					else
					{
						logFail("The user failed to enter the value : " + FloorPrice  + "  into the FloorPrice field.");
					}
				}
				else
				{
					logFail("The FloorPrice is not displayed.");
				}
			}
		}
		else
		{
			logFail("The add price container is not displayed.");
			skipException("Intentionally Skipping the test as the add price container is not displayed.");
		}
	}
	
	/*
	 * Select or Enter the date
	 */
	public void scheduleDateForSKU()
	{
		if((driver.isElementDisplayed(PricingObjects.icon_StartDate, "Start Date"))&&(driver.isElementDisplayed(PricingObjects.icon_EndDate, "End Date")))
		{
			logPass("The Start or End Date icon is displayed.");
			
			driver.scrollToElement(PricingObjects.icon_StartDate, "Start Date");
			if(driver.jsClickElement(PricingObjects.icon_StartDate, "Click Start Date"))
			{
				logInfo("The Start Date icon is clicked.");
				try 
				{
					Thread.sleep(1000);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				if(driver.isElementDisplayed(PricingObjects.datePickerContainer, "Date Picker Container"))
				{
					logInfo("The Date Picker container is displayed.");
					if(driver.isElementDisplayed(PricingObjects.btn_DateApply, "Apply Button"))
					{
						logPass("The Date Picker Apply button is displayed.");
						if(driver.jsClickElement(PricingObjects.btn_DateApply, "Apply Button"))
						{
							logPass("The Apply button is clicked for the Start Date.");
							driver.explicitWaitforInVisibility(PricingObjects.datePickerContainer, "Date Picker Container");
							try 
							{
								Thread.sleep(1000);
							} 
							catch (InterruptedException e) 
							{
								e.printStackTrace();
							}
							
							String currenDate = driver.getElementAttributeWithoutVisibility(PricingObjects.startDateValue, "submitval");
							currenDate = convertDateToExpectedFormat(currenDate);
							currentSKUPrices.put("StartDate", currenDate);
						}
						else
						{
							logFail("The Apply button is not clicked for the Start Date.");
							skipException("Intentionally failing the script as the Start Date apply button is not clicked.");
						}
					}
					else
					{
						logFail("The Date Picker Apply button is not displayed.");
						skipException("Intentionally failing the script as the Start Date icon apply button is not displayed.");
					}
				}
				else
				{
					logFail("The Date Picker container is not displayed.");
					skipException("Intentionally failing the script as the Start Date picker is not displayed.");
				}
			}
			else
			{
				logFail("The Start icon is not clicked.");
				skipException("Intentionally failing the script as the Start Date icon is not clicked.");
			}
			
			if(driver.jsClickElement(PricingObjects.icon_EndDate, "Click End Date"))
			{
				logInfo("The End Date icon is clicked.");
				try 
				{
					Thread.sleep(1000);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				if(driver.isElementDisplayed(PricingObjects.datePickerContainer, "Date Picker Container"))
				{
					logInfo("The Date Picker container is displayed.");
					if(driver.isElementDisplayed(PricingObjects.dd_YearSelect, "Year Select"))
					{
						logInfo("The Year Select is displayed.");
						int year = Calendar.getInstance().get(Calendar.YEAR);
						year = year+1;
						if(driver.selectByValue(PricingObjects.dd_YearSelect, String.valueOf(year), "Year Select"))
						{
							try 
							{
								Thread.sleep(1000);
							} 
							catch (InterruptedException e) 
							{
								e.printStackTrace();
							}
							if(driver.isElementDisplayed(PricingObjects.txt_AvailableDateSelect, "Available Date"))
							{
								logInfo("The available date is displayed.");
								String availableTxt = driver.getText(PricingObjects.txt_AvailableDateSelect, "Available Date");
								logInfo("The Selected text is : " + availableTxt);
								if(driver.clickElement(PricingObjects.txt_AvailableDateSelect, "Available Date"))
								{
									logInfo("The available date is selected.");
									if(driver.isElementDisplayed(PricingObjects.btn_DateApply, "Apply Button"))
									{
										logPass("The Date Picker Apply button is displayed.");
									
										if(driver.jsClickElement(PricingObjects.btn_DateApply, "Apply Button"))
										{
											logPass("The Apply button is clicked for the Start Date.");
											driver.explicitWaitforInVisibility(PricingObjects.datePickerContainer, "Date Picker Container");
											try 
											{
												Thread.sleep(1000);
											} 
											catch (InterruptedException e) 
											{
												e.printStackTrace();
											}
											
											String currenDate = driver.getElementAttributeWithoutVisibility(PricingObjects.endDateValue, "submitval");
											currenDate = convertDateToExpectedFormat(currenDate);
											currentSKUPrices.put("EndDate", currenDate);
										}
										else
										{
											logFail("The Apply button is not clicked for the End Date.");
											skipException("Intentionally failing the script as the End Date Appply button is not clicked.");
										}
									}
									else
									{
										logFail("The Date Picker Apply button is not displayed.");
										skipException("Intentionally failing the script as the End Date Appply button is not displayed.");
									}
								}
								else
								{
									logFail("The Available Date is not clicked.");
									skipException("Intentionally failing the script as the End available date is not clicked.");
								}
							}
							else
							{
								logFail("The available date is not displayed.");
								skipException("Intentionally failing the script as the End available date is not displayed.");
							}
						}
						else
						{
							logFail("The user failed to Select Year in the drop down.");
							skipException("Intentionally failing the script as the End available year is not selected.");
						}
					}
					else
					{
						logFail("The Year Select is not displayed.");
						skipException("Intentionally failing the script as the End available year is not displayed.");
					}
				}
				else
				{
					logFail("The Date Picker container is not displayed.");
					skipException("Intentionally failing the script as the End Date picker is not displayed.");
				}
			}
			else
			{
				logFail("The End icon is not clicked.");
				skipException("Intentionally failing the script as the Start End icon is not clicked.");
			}
		}
		else
		{
			logFail("The Start or End Date icon is not displayed.");
			skipException("Intentionally failing the script as the Start or End Date is not displayed.");
		}
	}

	/*
	 * Convert Date
	 */
	private String convertDateToExpectedFormat(String currenDate) 
	{
		String[] date = currenDate.split(" ");
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		DateFormat outputFormat = new SimpleDateFormat("dd/MM/yy", Locale.US);
		Date formateddate = null;
		String outPutDate = "";
		try 
		{
			formateddate = inputFormat.parse(date[0]);
			outPutDate = outputFormat.format(formateddate);
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		return outPutDate;
	}
	
	/*
	 * Verify created price are Displayed 
	 */
	public void verifyAddedPriceIsDisplayed()
	{
		if(driver.jsClickElement(PricingObjects.btn_search1, "Search Button"))
		{
			logInfo("The Searched Text was : " + currentPriceListName);
			try 
			{
				Thread.sleep(1000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			logPass("The user was able to click on the Search button in the All Price List page.");
			loadAndWaitForLoadingGauage();
		}
		else
		{
			logFail("The user was not able to click on the Search button in the All Price List page.");
			skipException("Intentionally Failing the Script as the user failed to click the Search button.");
		}
		
		if(!currentSKUPrices.isEmpty()&&(toastSuccess))
		{
			try 
			{
				Thread.sleep(9000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			
			logInfo("The Current SKU Price list is not empty. The list contains : " + currentSKUPrices);
			By tpXpath = null, minQtyXpath = null, maxQtyXpath = null, startTimeXpath = null, endTimeXpath = null;
			
			driver.scrollToElement(PricingObjects.lastSKUContainerRow, "Last SKU Container");
			try 
			{
				Thread.sleep(1000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			if(currentSKUPrices.containsKey("TransactionPrice"))
			{
				tpXpath = By.xpath(PricingObjects.lastSKUContainerRow.toString().replaceAll("By.xpath: ", "")+PricingObjects.txt_TransactionPriceValue.toString().replaceAll("By.xpath: ", ""));
				if(driver.isElementDisplayed(tpXpath, "Tp Value"))
				{
					logPass("The Transaction price value is displayed.");
					String actTxt = driver.getElementAttribute(tpXpath, "value", "Tp Xpath");
					String expTxt = currentSKUPrices.get("TransactionPrice");
					
					if(actTxt.contains(expTxt))
					{
						logPass("The added Transaction Price for the SKU is displayed.");
						
					}
					else
					{
						logFail("There is some mismatch in the added Transaction Price for the SKU.");
						logInfo("The expected price was : " + expTxt);
						logInfo("The actual price is : " + actTxt);
					}
				}
				else
				{
					logFail("The Transaction price value is not displayed.");
				}
			}
			
			if(currentSKUPrices.containsKey("MinQty"))
			{
				minQtyXpath = By.xpath(PricingObjects.lastSKUContainerRow.toString().replaceAll("By.xpath: ", "")+PricingObjects.txt_MinQtyValue.toString().replaceAll("By.xpath: ", ""));
				if(driver.isElementDisplayed(minQtyXpath, "Min Qty Value"))
				{
					logPass("The Min Qty value is displayed.");
					String actTxt = driver.getElementAttribute(minQtyXpath, "value", "Min Qty Xpath");
					String expTxt = currentSKUPrices.get("MinQty");
					if(actTxt.contains(expTxt))
					{
						logPass("The added Min Qty for the SKU is displayed.");
						
					}
					else
					{
						logFail("There is some mismatch in the added Transaction Price for the SKU.");
						logInfo("The expected min qty was : " + expTxt);
						logInfo("The actual price max qty is : " + actTxt);
					}
				}
				else
				{
					logFail("The Min Qty value is not displayed.");
				}
			}
			
			if(currentSKUPrices.containsKey("MaxQty"))
			{
				maxQtyXpath = By.xpath(PricingObjects.lastSKUContainerRow.toString().replaceAll("By.xpath: ", "")+PricingObjects.txt_MaxQtyValue.toString().replaceAll("By.xpath: ", ""));
				if(driver.isElementDisplayed(minQtyXpath, "Max Qty Value"))
				{
					logPass("The Min Qty value is displayed.");
					String actTxt = driver.getElementAttribute(maxQtyXpath, "value", "Max Qty Xpath");
					String expTxt = currentSKUPrices.get("MaxQty");
					if(actTxt.contains(expTxt))
					{
						logPass("The added Max Qty for the SKU is displayed.");
						
					}
					else
					{
						logFail("There is some mismatch in the added Max Qty for the SKU.");
						logInfo("The expected max qty was : " + expTxt);
						logInfo("The actual max qty is : " + actTxt);
					}
				}
				else
				{
					logFail("The Min Qty value is not displayed.");
				}
			}
			
			if(currentSKUPrices.containsKey("StartDate"))
			{
				startTimeXpath = By.xpath(PricingObjects.lastSKUContainerRow.toString().replaceAll("By.xpath: ", "")+PricingObjects.txt_StartDateValue.toString().replaceAll("By.xpath: ", ""));
				if(driver.isElementDisplayed(startTimeXpath, "Start Date Value"))
				{
					logPass("The Start Date value is displayed.");
					String actTxt = driver.getElementAttribute(startTimeXpath, "value", "Start Time Xpath");
					String expTxt = currentSKUPrices.get("StartDate");
					if(actTxt.contains(expTxt))
					{
						logPass("The added Start Date for the SKU is displayed.");
					}
					else
					{
						logFail("There is some mismatch in the added Start Date for the SKU.");
						logInfo("The expected Start Date was : " + expTxt);
						logInfo("The actual Start Date is : " + actTxt);
					}
				}
				else
				{
					logFail("The Start Date value is not displayed.");
				}
			}
			
			if(currentSKUPrices.containsKey("EndDate"))
			{
				endTimeXpath = By.xpath(PricingObjects.lastSKUContainerRow.toString().replaceAll("By.xpath: ", "")+PricingObjects.txt_EndDateValue.toString().replaceAll("By.xpath: ", ""));
				if(driver.isElementDisplayed(endTimeXpath, "End Date Value"))
				{
					logPass("The End Date value is displayed.");
					String actTxt = driver.getElementAttribute(endTimeXpath, "value", "End Time Xpath");
					String expTxt = currentSKUPrices.get("EndDate");
					if(actTxt.contains(expTxt))
					{
						logPass("The added End Date for the SKU is displayed.");
					}
					else
					{
						logFail("There is some mismatch in the added End Date for the SKU.");
						logInfo("The expected End Date was : " + expTxt);
						logInfo("The actual End Date is : " + actTxt);
					}
				}
				else
				{
					logFail("The End Date value is not displayed.");
				}
			}
		}
		else
		{
			logFail("The Added Current SKU Price "+ currentSKUPrices +" list is not displayed.");
		}
		
		currentSKUPrices.clear();
	}
	
	/*
	 * Click Submit Button 
	 */
	public void clickProjectSubmit()
	{
		if(driver.isElementDisplayed(PricingObjects.btn_ProjectSubmit, "Project Submit"))
		{
			logInfo("The Project Submit Button  is displayed.");
			if(driver.jsClickElement(PricingObjects.btn_ProjectSubmit, "Project Submit"))
			{
				loadAndWaitForLoadingGauage();
				logPass("The user was able to click on the the Submit Button.");
				try 
				{
					Thread.sleep(2500);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				driver.explicitWaitforVisibility(PricingObjects.btn_ProjectApproval, "Project Submit");
				
				int count = 0;
				boolean approved = false;
				do
				{
					String currentText = driver.hiddenGetText(PricingObjects.btn_ProjectApproval, "Approve Button");
					if(currentText.equalsIgnoreCase("Approve"))
					{
						approved = true;
						break;
					}
					
					try 
					{
						Thread.sleep(5000);
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
					
					count++;
				}while((!approved)&&(count<5));
				
				String currentText = driver.hiddenGetText(PricingObjects.btn_ProjectApproval, "Approve Button");
				if(currentText.equalsIgnoreCase("Approve"))
				{
					logPass("The Project is moved to approve state.");
				}
				else
				{
					logFail("The Project is not moved to approve state.");
					skipException("Intentionally failing the script as the project is not moved to approve state.");
				}
			}
			else
			{
				logFail("The user failed to click on the the Submit button.");
				skipException("Intentionally failing the script as the project submit button is not clicked.");
			}
		}
		else
		{
			logFail("The Project Submit Button  is not displayed.");
			skipException("Intentionally failing the script as the Submit button is not displayed.");
		}
	}
	
	/*
	 * Click Submit Button 
	 */
	public void clickProjectApproval()
	{
		if(driver.isElementDisplayed(PricingObjects.btn_ProjectApproval, "Project Submit"))
		{
			logInfo("The Project Approve Button  is displayed.");
			if(driver.jsClickElement(PricingObjects.btn_ProjectApproval, "Approve Description Save"))
			{
				logPass("The user was able to click on the the Approve Button.");
				loadAndWaitForLoadingGauage();
				
				try 
				{
					Thread.sleep(5000);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				
				int count = 0;
				boolean approved = false;
				do
				{
					
					String currentText = driver.hiddenGetText(PricingObjects.btn_ProjectApproval, "Approve Description Save");
					if(currentText.equalsIgnoreCase("Approved"))
					{
						approved = true;
						break;
					}
					count++;
					
					try 
					{
						Thread.sleep(5000);
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
					
				}while((!approved)&&(count<5));
				
		/*		if(approved)
				{
					String currentText = driver.hiddenGetText(PricingObjects.btn_ProjectApproval, "Approve Description Save");
					if(currentText.equalsIgnoreCase("Approved"))
					{
						logPass("The Project is moved to approved state.");
						boolean updated = verifyUserInUpdatedTab();
						if(updated)
						{
							logPass("The Updated SKU Tab is displayed.");
							verifyAddedSKUIsUpdated();
						}
						else
						{
							logFail("The Updated SKU Tab is not displayed.");
						}
					}
					else
					{
						logFail("The Project is not moved to approved state.");
						skipException("Intentionally failing the script as the project is not moved to approved state.");
					}
				}
				else
				{
					logFail("The Project is not moved to approved state.");
					skipException("Intentionally failing the script as the project is not moved to approved state.");
				}*/
			}
			else
			{
				logFail("The user failed to click on the the Approve save button.");
				skipException("Intentionally failing the script as the project is not moved to approved state.");
			}
		}
		else
		{
			logFail("The Project Approve Button  is not displayed.");
			skipException("Intentionally failing the script as the Approve button is not displayed.");
		}
	}
	
	/*
	 * Verify User in Updated Tab
	 */
	public boolean verifyUserInUpdatedTab()
	{
		boolean updated = false;
		if(driver.isElementDisplayed(PricingObjects.skuUpdatedTab, "SKU Updated Tab"))
		{
			logPass("The SKU Updated Tab is displayed.");
			updated = true;
		}
		else
		{
			logFail("The SKU Updated Tab is not displayed.");
		}
		return updated;
	}
	
	/*
	 * Updated Row Container
	 */
	public void verifyAddedSKUIsUpdated()
	{
		driver.scrollToElement(PricingObjects.lastUpdatedSKUContainerRow, "Last SKU Updated Container");
		try 
		{
			Thread.sleep(1000);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		if(!currentSkuId.isEmpty())
		{
			By skuXpath = By.xpath(PricingObjects.lastUpdatedSKUContainerRow.toString().replaceAll("By.xpath: ", ""));
			if(driver.isElementDisplayed(skuXpath, "SKU Value"))
			{
				logPass("The SKU value is displayed.");
				String actTxt = driver.getText(skuXpath, "SKU Xpath");
				
				if(actTxt.contains(currentSkuId))
				{
					logPass("The added SKU is updated.");
				}
				else
				{
					logFail("There is some mismatch in the added SKU.");
					logInfo("The expected SKU ID was : " + currentSkuId);
				}
			}
			else
			{
				logFail("The SKU ID row container is not displayed.");
			}
		}
		else
		{
			logFail("The SKU ID list is empty.");
		}
	}
	
	/*
	 * Click Target Completion Date
	 */
	public void enterTargetCompletionDate()
	{
		loadAndWaitForLoadingGauage();
		if(driver.clickElement(PricingObjects.icon_CompletionDatePicker, "Target Completion Date"))
		{
			logInfo("The Target Completion End Date is clicked.");
			try 
			{
				Thread.sleep(1000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			if(driver.isElementDisplayed(PricingObjects.datePickerContainer, "Date Picker Container"))
			{
				logInfo("The Date Picker container is displayed.");
				if(driver.isElementDisplayed(PricingObjects.dd_YearSelect, "Year Select"))
				{
					logInfo("The Year Select is displayed.");
					int year = Calendar.getInstance().get(Calendar.YEAR);
					year = year+1;
					if(driver.selectByValue(PricingObjects.dd_YearSelect, String.valueOf(year), "Year Select"))
					{
						try 
						{
							Thread.sleep(1000);
						} 
						catch (InterruptedException e) 
						{
							e.printStackTrace();
						}
						if(driver.isElementDisplayed(PricingObjects.txt_AvailableDateSelect, "Available Date"))
						{
							logInfo("The available date is displayed.");
							String availableTxt = driver.getText(PricingObjects.txt_AvailableDateSelect, "Available Date");
							logInfo("The Selected text is : " + availableTxt);
							if(driver.clickElement(PricingObjects.txt_AvailableDateSelect, "Available Date"))
							{
								logInfo("The available date is selected.");
								if(driver.isElementDisplayed(PricingObjects.btn_DateApply, "Apply Button"))
								{
									logPass("The Date Picker Apply button is displayed.");
								
									if(driver.jsClickElement(PricingObjects.btn_DateApply, "Apply Button"))
									{
										logPass("The Apply button is clicked for the Start Date.");
										driver.explicitWaitforInVisibility(PricingObjects.datePickerContainer, "Date Picker Container");
										try 
										{
											Thread.sleep(1000);
										} 
										catch (InterruptedException e) 
										{
											e.printStackTrace();
										}
										
										/*String currenDate = driver.getElementAttributeWithoutVisibility(PricingObjects.endDateValue, "submitval");
										currenDate = convertDateToExpectedFormat(currenDate);
										currentSKUPrices.put("EndDate", currenDate);*/
									}
									else
									{
										logFail("The Apply button is not clicked for the End Date.");
										skipException("Intentionally failing the script as the End Date Appply button is not clicked.");
									}
								}
								else
								{
									logFail("The Date Picker Apply button is not displayed.");
									skipException("Intentionally failing the script as the End Date Appply button is not displayed.");
								}
							}
							else
							{
								logFail("The Available Date is not clicked.");
								skipException("Intentionally failing the script as the End available date is not clicked.");
							}
						}
						else
						{
							logFail("The available date is not displayed.");
							skipException("Intentionally failing the script as the End available date is not displayed.");
						}
					}
					else
					{
						logFail("The user failed to Select Year in the drop down.");
						skipException("Intentionally failing the script as the End available year is not selected.");
					}
				}
				else
				{
					logFail("The Year Select is not displayed.");
					skipException("Intentionally failing the script as the End available year is not displayed.");
				}
			}
			else
			{
				logFail("The Date Picker container is not displayed.");
				skipException("Intentionally failing the script as the End Date picker is not displayed.");
			}
		}
		else
		{
			logFail("The End icon is not clicked.");
			skipException("Intentionally failing the script as the Start End icon is not clicked.");
		}
	}
	
	/*
	 * Validate Created Project is displayed.
	 */
	public void clickCreatedProjectFromListToExitViaExitButton()
	{
		boolean projectFound = false;
		int i = 0;
		driver.explicitWaitforVisibility(PricingObjects.createdProjectName, "Created Project List Name");
		if(driver.explicitWaitforVisibility(PricingObjects.createdProjectName, "Created Project List Name"))
		{
			logPass("The created project list is displayed.");
			int rowSize = driver.getSize(PricingObjects.createdProjectName, "Created Project List");
			for(i = 1; i<=rowSize; i++)
			{
				By rowXpath = By.xpath(PricingObjects.createdProjectName.toString().replaceAll("By.xpath: ", "").concat("[" + (i) + "]"));
				String actText = driver.getText(rowXpath, "Current Project Name");
				if(actText.contains(currentProjectName))
				{
					projectFound = true;
					break;
				}
				else 
				{
					continue;
				}
			}
			
			if(projectFound)
			{
				logPass("The Created Project "+ currentProjectName + " is found among the List.");
				By rowXpath = By.xpath(PricingObjects.createdProjectList.toString().replaceAll("By.xpath: ", "").concat("[" + (i) + "]")+PricingObjects.createdProjectEnterExitIcon.toString().replaceAll("By.xpath: ", ""));
				if(driver.moveToElementAndClick(rowXpath, "Row Xpath"))
				{
					loadAndWaitForLoadingGauage();
					logPass("The expected exit button to view is displayed and clicked.");
					verifyUserNavigatedToProjectOverViewPage();
				}
				else
				{
					logFail("The expected exit icon to view is not displayed to click.");
					skipException("Intentionally failing the script as the Enter / Exit button is not clicked.");
				}
			}
			else
			{
				logFail("The Created Project " + currentProjectName + " is not found among the List.");
				skipException("Intentionally failing the script as the Enter / Exit button is not displayed.");
			}
		}
		else
		{
			logFail("The created project list is not displayed.");
			skipException("Intentionally failing the script as the created project is not displayed.");
		}
	}
	
	/*
	 * Verify User Can Click on Deny or Approve Elipses 
	 */
	public boolean verifyAndClickUserReopenDenyElipses()
	{
		boolean isClicked = false;
		
		try 
		{
			Thread.sleep(2500);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		
		loadAndWaitForLoadingGauage();
		if(driver.isElementDisplayed(PricingObjects.reopenDenyElipses, "Reopen / Deny Elipses"))
		{
			logPass("The Approve / Deny Elipses is displayed.");
			if(driver.clickElement(PricingObjects.reopenDenyElipses, "Reopen / Deny Elipses"))
			{
				isClicked = true;
				logPass("The user was able to click on the User Reopen / Deny Elipses.");
			}
			else
			{
				logFail("The user failed to click on the User Reopen / Deny Elipses.");
				skipException("Intentionally Failing the script as the reopen / Deny elipses is not clicked. ");
			}
		}
		else
		{
			logFail("The Reopen / Deny Elipses is not displayed.");
			skipException("Intentionally Failing the script as the reopen / Deny elipses is not displayed. ");
		}
		return isClicked;
	}
	
	/*
	 * Verify User Can Deny the Request
	 */
	public void clickReopenProject()
	{
		boolean elipsesDisplayed = verifyAndClickUserReopenDenyElipses();
		if(elipsesDisplayed)
		{
			if(driver.isElementDisplayed(PricingObjects.btn_reOpenProject, "Reopen Project"))
			{
				logPass("The reopen Project buttton is displayed.");
				if(driver.jsClickElement(PricingObjects.btn_reOpenProject, "Reopen Project"))
				{
					logPass("The user was able to click on the Reopen Project button.");
					loadAndWaitForLoadingGauage();
				
					/*
					 * Temp Fix
					 */
					driver.pageReload();
					loadAndWaitForLoadingGauage();
					verifySubmitProjectBtnIsDisplayed();
				}
				else
				{
					logFail("The user was not able to click on the Reopen Project button.");
				}
			}
			else
			{
				logFail("The reopen Project buttton is not displayed.");
				//skipException("Intentionally Failing the script as the reopen is not displayed. ");
			}
		}
		else
		{
			logFail("The elipses to reop / deny is not displayed.");
		}
	}
	
	/*
	 * Verify User Can Deny the Request
	 */
	public void clickDenyProject()
	{
		boolean elipsesDisplayed = verifyAndClickUserReopenDenyElipses();
		if(elipsesDisplayed)
		{
			if(driver.isElementDisplayed(PricingObjects.btn_denyProject, "Deny Project"))
			{
				logPass("The deny Project buttton is displayed.");
				if(driver.jsClickElement(PricingObjects.btn_denyProject, "Deny Project"))
				{
					logPass("The user was able to click on the Deny Project button.");
					loadAndWaitForLoadingGauage();
				
					/*
					 * Temp Fix
					 */
					driver.pageReload();
					loadAndWaitForLoadingGauage();
					//verifyProjectExitBtnIsDisplayed();
				}
				else
				{
					logFail("The user was not able to click on the Reopen Project button.");
				}
			}
			else
			{
				logFail("The reopen Project buttton is not displayed.");
				//skipException("Intentionally Failing the script as the reopen is not displayed. ");
			}
		}
		else
		{
			logFail("The elipses to reop / deny is not displayed.");
		}
	}
	
	/*
	 * Verify Projet Submit button is not displayed
	 */
	private void verifySubmitProjectBtnIsDisplayed() 
	{
		if(driver.isElementDisplayed(PricingObjects.btn_ProjectSubmit, "Project Submit"))
		{
			logPass("The Project Submit Button  is displayed.");
		}
		else
		{
			logFail("The Project Submit Button  is not displayed.");
			skipException("Intentionally failing the script as the Submit button is not displayed.");
		}
	}
	
	/*
	 * Verify Projet Submit button is not displayed
	 */
	private void verifyProjectExitBtnIsDisplayed() 
	{
		if(driver.isElementDisplayed(PricingObjects.btn_ProjectDetailsExit, "Project Exit Button"))
		{
			logPass("The Project Details Exit Button  is displayed.");
		}
		else
		{
			logFail("The Project Details Exit button is not displayed.");
			skipException("Intentionally failing the script as the Project Details Exit button is not displayed.");
		}
	}
	
	/*
	 * Edit Price List 
	 */
	public void editPriceListName()
	{
		currentPriceListName = currentPriceListName + " " + currentPriceListName;
		if(driver.isElementDisplayed(PricingObjects.txt_EditPriceListName, "Pricing Name"))
		{
			if(driver.enterText(PricingObjects.txt_EditPriceListName, currentPriceListName, "Pricing Name"))
			{
				logInfo("The entered price list name is : " + currentPriceListName);
				logPass("The user was edit and enter the text in the Pricing Name List.");
			}
			else
			{
				logFail("The user failed to enter the text in the Pricing Name List.");
				skipException("Intentionally failing the script as the user failed to enter Price List Name field.");
			}
		}
		else
		{
			logFail("The edit Pricing Name List is not displayed.");
			skipException("Intentionally failing the script as the Price List Name field is not displayed.");
		}
	}
	
	/*
	 * Edit Desc
	 */
	public void editPriceDesc()
	{
		String priceDesc = ExcelReader.getData("Pricing", "PriceListDesc");
		currentProjectDesc = priceDesc+ " " + currentPriceListName + " " + currentPriceListName;
		if(driver.enterText(PricingObjects.txt_EditPriceListDesc, currentProjectDesc, "Pricing Name"))
		{
			logInfo("The entered price desc is : " + currentProjectDesc);
			logPass("The user was able to edit and enter the text in the Pricing Description text field.");
		}
		else
		{
			logFail("The user was not able to enter the text in the Pricing Description text field.");
			skipException("Intentionally failing the script as the Price Desc field is not displayed.");
		}
	}
	
	/*
	 * Validated if Current Project Name is Validated
	 */
	public void verifyUpdatedProjectName()
	{
		if(driver.isElementDisplayed(PricingObjects.txt_EditPriceListName, "Pricing Name"))
		{
			String currentText = driver.getElementAttribute(PricingObjects.txt_EditPriceListName, "value" , "Pricing Name");
			if(currentText.equalsIgnoreCase(currentPriceListName))
			{
				logPass("There is no mismatch in the Current project name.");
			}
			else
			{
				logFail("There is some mismatch in the Current project name.");
				logInfo("The expected name was : " + currentPriceListName);
				logInfo("The actual name is : " + currentText);
			}
		}
		else
		{
			logFail("The edit Pricing Name List is not displayed.");
			skipException("Intentionally failing the script as the Price List Name field is not displayed.");
		}
	}
	
	/*
	 * Validated if Current Project Name is Validated
	 */
	public void verifyUpdatedProjectDesc()
	{
		if(driver.isElementDisplayed(PricingObjects.txt_EditPriceListDesc, "Pricing Desc"))
		{
			String currentText = driver.getElementAttribute(PricingObjects.txt_EditPriceListDesc, "value" , "Pricing Name");
			
			if(currentText.equalsIgnoreCase(currentProjectDesc))
			{
				logPass("There is no mismatch in the Current project desc.");
			}
			else
			{
				logFail("There is some mismatch in the Current project desc.");
				logInfo("The expected name was : " + currentProjectDesc);
				logInfo("The actual name is : " + currentText);
			}
		}
		else
		{
			logFail("The edit Pricing desc List is not displayed.");
			skipException("Intentionally failing the script as the Price List desc field is not displayed.");
		}
	}
	
	/*
	 * Change Status
	 */
	public void changeProjectStatusToInactive()
	{
		String expText = "Off";
		boolean stateInactive = false;
		int count = 0;
		do
		{
			if(driver.jsClickElement(PricingObjects.btn_EditProjectStatus, "Edit Button Status"))
			{
				try 
				{
					Thread.sleep(1000);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				
				String currentText = driver.getText(PricingObjects.txt_ProjectStatus, "Project Status");
				if(currentText.equalsIgnoreCase(expText))
				{
					break;
				}
			}
		}while((stateInactive)&&(count<10));
	}
	
	/*
	 * Change Status
	 */
	public void changeProjectStatusToActive()
	{
		String expText = "On";
		boolean stateInactive = false;
		int count = 0;
		do
		{
			if(driver.jsClickElement(PricingObjects.btn_EditProjectStatus, "Edit Button Status"))
			{
				try 
				{
					Thread.sleep(1000);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				
				String currentText = driver.getText(PricingObjects.txt_ProjectStatus, "Project Status");
				if(currentText.equalsIgnoreCase(expText))
				{
					break;
				}
			}
		}while((stateInactive)&&(count<10));
	}
	
	/*
	 * Click Edit Save Button
	 */
	public void saveEditedPriceList()
	{
		if(driver.jsClickElement(PricingObjects.btn_editProjectSave, "Edit Save Price List"))
		{
			logPass("The user was able to click on the Create button in the Edit Pricelist Page.");
			verifyPriceListSuccessMessageIsDisplayed();
		}
		else
		{
			logFail("The user failed to click on the Create button in the Create Pricelist Page.");
		}
	}
	
	/*
	 * Verify And Navigate To Add SKU Page
	 */
	public void navigateToSKUPage()
	{
		driver.navigateBack();
		loadAndWaitForLoadingGauage();
		clickAddSKU();
		verifyAddSKUContainer();
	}
	
	/*
	 * Navigate To SKU Page
	 */
	public void navigateToSKUFromApprovedPage()
	{
		driver.navigateBack();
		loadAndWaitForLoadingGauage();
	}
	
	/*
	 * Enter Transaction Price
	 */
	public void enterSKUDetailsAlone()
	{
		try
		{
			String[] sKUID = ExcelReader.getData("Pricing", "SKUIDS").split("<B>");
			String[] currentTransactionPrice = ExcelReader.getData("Pricing", "TransactionPrice").split("<B>");
			int length = sKUID.length;
			for(int i = 0; i<length; i++)
			{
				if(driver.isElementDisplayed(PricingObjects.txt_SkudId, "SKU ID")||driver.isElementDisplayed(PricingObjects.txt_SkudId1, "SKU ID") &&(driver.isElementDisplayed(PricingObjects.txt_TransactionPrice, "Transaction Price")))
				{
					logPass("The user was able to enter SKU ID.");
					currentSkuId = sKUID[i];
					String skuTransactionPrice = currentTransactionPrice[i];
					if(driver.enterText(PricingObjects.txt_SkudId, currentSkuId, "SKU IDS")||driver.enterText(PricingObjects.txt_SkudId1, currentSkuId, "SKU IDS"))
					{
						logPass("The user was able to enter the SKU ID to the Add SKU Container");
						logInfo("The Added SKU was : " +currentSkuId);
					}
					else
					{
						logFail("The user failed to enter the SKU ID to the Add SKU Container");
						skipException("Intentionally failing the script as the SKU ID is not entered.");
					}
					
					if(driver.enterText(PricingObjects.txt_TransactionPrice, skuTransactionPrice, "SKU Transaction Price"))
					{
						currentSKUPrices.put("TransactionPrice", skuTransactionPrice);
						logPass("The user was able to enter the SKU Transaction Price to the Add SKU Container");
						logInfo("The Added SKU Price was : " +skuTransactionPrice);
					}
					else
					{
						logFail("The user failed to enter the SKU Transaction Price to the Add SKU Container");
						skipException("Intentionally failing the script as the SKU Transaction Price is not entered.");
					}
					
					/*if(driver.jsClickElement(PricingObjects.btn_AddSKUDetails, "Add Details"))
					{
						logPass("The Additional Details button is clicked.");
						Thread.sleep(1000);
					}
					else
					{
						logFail("The Additional Details button is not clicked.");
						skipException("Intentionally failing the script as the Add SKU details button is not clicked.");
					}*/
				}
				else
				{
					logFail("The SKU ID is not displayed.");
					skipException("Intentionally failing the script as the SKU ID is not displayed.");
				}
			}
		}
		catch (Exception e) 
		{
			logFail("There is some issue in entering the SKU Details. The issue is : " + e.getMessage());
			skipException("Intentionally failing the script as there is some issue in SKU details.");
		}
	}
	
	/*
	 * Save SKU
	 */
	public void saveSKUDetails()
	{
		if(driver.jsClickElement(PricingObjects.btn_AddSKUDetails, "Save SKU Details"))
		{
			waitForToastMessage();
			loadAndWaitForLoadingGauage();
			
			/*
			 * Temp Fix
			 */
			driver.pageReload();
			clickSKUTab();
			loadAndWaitForLoadingGauage();
			
			logPass("The user was able to save the SKU details");
		}
		else
		{
			logFail("The user was not able to save the SKU details");
			skipException("Intentionally failing the script as the SKU details is not saved.");
		}
	}
	
	/*
	 * Add Min QTY
	 */
	public void enterAndVerifySKUMinQty()
	{
		boolean priceContaineDisplayed = verifyAddPriceContainerIsDisplayed();
		if(priceContaineDisplayed)
		{
			logPass("The add price container is displayed.");
			String minQty = ExcelReader.getData("Pricing", "MinQty");
			if(!minQty.equalsIgnoreCase("NA"))
			{
				if(driver.isElementDisplayed(PricingObjects.txt_MinQty, "Min Qty Field"))
				{
					if(driver.enterText(PricingObjects.txt_MinQty, minQty , "Min Qty Field"))
					{
						currentSKUPrices.put("MinQty", minQty);
						logPass("The user was abke to enter the value : " + minQty + "  into the min qty field.");
					}
					else
					{
						logFail("The user failed to enter the value : " + minQty  + "  into the min qty field.");
					}
				}
				else
				{
					logFail("The Min Qty is not displayed.");
				}
			}
		}
		else
		{
			logFail("The add price container is not displayed.");
			skipException("Intentionally Skipping the test as the add price container is not displayed.");
		}
	}
	
	/*
	 * Add Max QTY
	 */
	public void enterAndVerifySKUMaxQty()
	{
		boolean priceContaineDisplayed = verifyAddPriceContainerIsDisplayed();
		if(priceContaineDisplayed)
		{
			logPass("The add price container is displayed.");
			String maxQty = ExcelReader.getData("Pricing", "MaxQty");
			if(!maxQty.equalsIgnoreCase("NA"))
			{
				if(driver.isElementDisplayed(PricingObjects.txt_MaxQty, "Max Qty Field"))
				{
					if(driver.enterText(PricingObjects.txt_MaxQty, maxQty , "Min Qty Field"))
					{
						currentSKUPrices.put("MaxQty", maxQty);
						logPass("The user was able to enter the value : " + maxQty + "  into the Max qty field.");
					}
					else
					{
						logFail("The user failed to enter the value : " + maxQty  + "  into the Max qty field.");
					}
				}
				else
				{
					logFail("The Max Qty is not displayed.");
				}
			}
		}
		else
		{
			logFail("The add price container is not displayed.");
			skipException("Intentionally Skipping the test as the add price container is not displayed.");
		}
	}
	
	/*
	 * Select or Enter the date
	 */
	public void scheduleDateForSKUFromAddSKUScreen()
	{
		if((driver.isElementDisplayed(By.xpath("(//*[@class='input-group-append dateicon'])[3]"), "Start Date"))&&(driver.isElementDisplayed(By.xpath("(//*[@class='input-group-append dateicon'])[4]"), "End Date")))
		{
			logPass("The Start or End Date icon is displayed.");
			
			driver.scrollToElement(By.xpath("(//*[@class='input-group-append dateicon'])[3]"), "Start Date");
			if(driver.jsClickElement(By.xpath("(//*[@class='input-group-append dateicon'])[3]"), "Click Start Date"))
			{
				logInfo("The Start Date icon is clicked.");
				try 
				{
					Thread.sleep(1000);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				if(driver.isElementDisplayed(PricingObjects.datePickerContainer, "Date Picker Container"))
				{
					logInfo("The Date Picker container is displayed.");
					if(driver.isElementDisplayed(PricingObjects.btn_DateApply, "Apply Button"))
					{
						logPass("The Date Picker Apply button is displayed.");
						if(driver.jsClickElement(PricingObjects.btn_DateApply, "Apply Button"))
						{
							logPass("The Apply button is clicked for the Start Date.");
							driver.explicitWaitforInVisibility(PricingObjects.datePickerContainer, "Date Picker Container");
							try 
							{
								Thread.sleep(1000);
							} 
							catch (InterruptedException e) 
							{
								e.printStackTrace();
							}
							
							String currenDate = driver.getElementAttributeWithoutVisibility(PricingObjects.startDateValue, "submitval");
							currenDate = convertDateToExpectedFormat(currenDate);
							currentSKUPrices.put("StartDate", currenDate);
						}
						else
						{
							logFail("The Apply button is not clicked for the Start Date.");
							skipException("Intentionally failing the script as the Start Date apply button is not clicked.");
						}
					}
					else
					{
						logFail("The Date Picker Apply button is not displayed.");
						skipException("Intentionally failing the script as the Start Date icon apply button is not displayed.");
					}
				}
				else
				{
					logFail("The Date Picker container is not displayed.");
					skipException("Intentionally failing the script as the Start Date picker is not displayed.");
				}
			}
			else
			{
				logFail("The Start icon is not clicked.");
				skipException("Intentionally failing the script as the Start Date icon is not clicked.");
			}
			
			if(driver.jsClickElement(By.xpath("(//*[@class='input-group-append dateicon'])[4]"), "Click End Date"))
			{
				logInfo("The End Date icon is clicked.");
				try 
				{
					Thread.sleep(1000);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				if(driver.isElementDisplayed(PricingObjects.datePickerContainer, "Date Picker Container"))
				{
					logInfo("The Date Picker container is displayed.");
					if(driver.isElementDisplayed(PricingObjects.dd_YearSelect, "Year Select"))
					{
						logInfo("The Year Select is displayed.");
						int year = Calendar.getInstance().get(Calendar.YEAR);
						year = year+1;
						if(driver.selectByValue(PricingObjects.dd_YearSelect, String.valueOf(year), "Year Select"))
						{
							try 
							{
								Thread.sleep(1000);
							} 
							catch (InterruptedException e) 
							{
								e.printStackTrace();
							}
							if(driver.isElementDisplayed(PricingObjects.txt_AvailableDateSelect, "Available Date"))
							{
								logInfo("The available date is displayed.");
								String availableTxt = driver.getText(PricingObjects.txt_AvailableDateSelect, "Available Date");
								logInfo("The Selected text is : " + availableTxt);
								if(driver.clickElement(PricingObjects.txt_AvailableDateSelect, "Available Date"))
								{
									logInfo("The available date is selected.");
									if(driver.isElementDisplayed(PricingObjects.btn_DateApply, "Apply Button"))
									{
										logPass("The Date Picker Apply button is displayed.");
									
										if(driver.jsClickElement(PricingObjects.btn_DateApply, "Apply Button"))
										{
											logPass("The Apply button is clicked for the Start Date.");
											driver.explicitWaitforInVisibility(PricingObjects.datePickerContainer, "Date Picker Container");
											try 
											{
												Thread.sleep(1000);
											} 
											catch (InterruptedException e) 
											{
												e.printStackTrace();
											}
											
											String currenDate = driver.getElementAttributeWithoutVisibility(PricingObjects.endDateValue, "submitval");
											currenDate = convertDateToExpectedFormat(currenDate);
											currentSKUPrices.put("EndDate", currenDate);
										}
										else
										{
											logFail("The Apply button is not clicked for the End Date.");
											skipException("Intentionally failing the script as the End Date Appply button is not clicked.");
										}
									}
									else
									{
										logFail("The Date Picker Apply button is not displayed.");
										skipException("Intentionally failing the script as the End Date Appply button is not displayed.");
									}
								}
								else
								{
									logFail("The Available Date is not clicked.");
									skipException("Intentionally failing the script as the End available date is not clicked.");
								}
							}
							else
							{
								logFail("The available date is not displayed.");
								skipException("Intentionally failing the script as the End available date is not displayed.");
							}
						}
						else
						{
							logFail("The user failed to Select Year in the drop down.");
							skipException("Intentionally failing the script as the End available year is not selected.");
						}
					}
					else
					{
						logFail("The Year Select is not displayed.");
						skipException("Intentionally failing the script as the End available year is not displayed.");
					}
				}
				else
				{
					logFail("The Date Picker container is not displayed.");
					skipException("Intentionally failing the script as the End Date picker is not displayed.");
				}
			}
			else
			{
				logFail("The End icon is not clicked.");
				skipException("Intentionally failing the script as the Start End icon is not clicked.");
			}
		}
		else
		{
			logFail("The Start or End Date icon is not displayed.");
			skipException("Intentionally failing the script as the Start or End Date is not displayed.");
		}
	}
	
	/*
	 * LogOut Pricing
	 */
	public void logoutFromPricing() throws InterruptedException
	{
		driver.jsClickElement(PricingObjects.createProjectContainer, "Create Project Model");
		driver.scrollToElement(PricingObjects.txt_userName, "usernamelabel");
		if(driver.explicitWaitforVisibility(PricingObjects.txt_userName, "usernamelabel"))
		{
			logPass("The user name is displayed.");
			Thread.sleep(1000);
			if(driver.jsClickElement(PricingObjects.txt_userName, "Click usernamelabel"))
			{
				logPass("The user was able to click on the user name.");
				Thread.sleep(2000);
			}
			else
			{
				logFail("The user failed to click on the user name.");
			}
		}
		else
		{
			logFail("The user user name is not displayed.");
		}
		
		if(driver.explicitWaitforVisibility(PricingObjects.logout, "logout"))
		{
			logPass("The logout button display");
			if(driver.jsClickElement(PricingObjects.logout, "Click logout"))
			{
				logPass("THe logout button is clicked.");
			}
			else
			{
				logFail("THe logout button is not clicked.");
			}
		}
		else
		{
			logFail("logout button not display");
		}
	}
	
	/*
	 * Search and Set Current Project
	 */
	public boolean searchAndSetCurrentProject()
	{
		boolean projectSet = false;
		if(driver.isElementDisplayed(PricingObjects.dd_projectList, "Project Tab Button"))
		{
			logInfo("The project list drop down tab is dispalyed.");
			if(driver.jsClickElement(PricingObjects.dd_projectList, "Project Tab Button"))
			{
				logPass("The user was able to click on the project list to search.");
				
				if(driver.isElementDisplayed(PricingObjects.dd_AllProjectContainer, "Project Tab Button"))
				{
					logPass("The Project Container is displayed.");
					if(driver.isElementDisplayed(PricingObjects.dd_AllProjectValues, "All Project Values"))
					{
						int length = driver.getSize(PricingObjects.dd_AllProjectValues, "All Project Values");
						boolean projectFound = false;
						int i = 0;
						By Projectxpath = null;
						for(i = 1; i<=length; i++)
						{
							Projectxpath = By.xpath("("+ PricingObjects.dd_AllProjectValues.toString().replaceAll("By.xpath: ", "").concat(")[" + i + "]"));
							String currentText = driver.getText(Projectxpath, "Current project Xpath");
							if(currentText.equalsIgnoreCase(currentProjectName))
							{
								logInfo("The Current project name : " + currentProjectName + " is found from the list.");
								projectFound = true;
								break;
							}
							else
							{
								continue;
							}
						}
						
						if(projectFound)
						{
							logPass("The added project is found in the list.");
							if(driver.jsClickElement(Projectxpath, "Project Xpath"))
							{
								logInfo("The Searched Project is clicked.");
								try 
								{
									Thread.sleep(1000);
								} 
								catch (InterruptedException e) 
								{
									e.printStackTrace();
								}
								
								/*
								 * if(driver.jsClickElement(PricingObjects.btn_search, "Search Button")) {
								 * logInfo("The Searched Text was : " + currentPriceListName); try {
								 * Thread.sleep(1000); } catch (InterruptedException e) { e.printStackTrace(); }
								 * logPass("The user was able to click on the Search button in the All Price List page."
								 * ); loadAndWaitForLoadingGauage(); } else {
								 * logFail("The user was not able to click on the Search button in the All Price List page."
								 * );
								 * skipException("Intentionally Failing the Script as the user failed to find the Search button."
								 * ); }
								 */
								
								driver.moveToElement(PricingObjects.btn_search1, "Search Button");
								if(driver.isElementDisplayed(PricingObjects.btn_search1, "Search Button") && driver.jsClickElement(PricingObjects.btn_search1, "Search Button"))
								{
									logInfo("The Searched Text was : " + currentPriceListName);
									try 
									{
										Thread.sleep(1000);
									} 
									catch (InterruptedException e) 
									{
										e.printStackTrace();
									}
									logPass("The user was able to click on the Search button in the All Price List page.");
									loadAndWaitForLoadingGauage();
								}
								else
								{
									logFail("The user was not able to click on the Search button in the All Price List page.");
									skipException("Intentionally Failing the Script as the user failed to find the Search button.");
								}
								projectSet = true;
							}
							else
							{
								logFail("The Searched Project is not clicked.");
								skipException("Intentionally failing the script as the project " + currentProjectName + "is not clicked.");
							}
						}
						else
						{
							logFail("The added project is not found in the list.");
							skipException("Intentionally failing the script as the added project is not found in the list.");
						}
						
					}
					else
					{
						skipException("Intentionally failing the script as the added project is not found in the list.");
					}
				}
				else
				{
					logFail("The Project Container is not clicked.");
					skipException("Intentionally failing the script as the project container is not clicked.");
				}
			}
			else
			{
				logFail("The user failed to click on the project list to search.");
				skipException("Intentionally failing the script as the project list is not clicked.");
			}
		}
		else
		{
			logInfo("The project list drop down tab is not displayed.");
		}
		return projectSet;
	}
	
	
	/*
	 * Search and Set AllApproved
	 */
	public boolean searchAndSetAllApproved()
	{
		boolean projectSet = false;
		if(driver.isElementDisplayed(PricingObjects.dd_projectList, "Project Tab Button"))
		{
			logInfo("The project list drop down tab is dispalyed.");
			if(driver.jsClickElement(PricingObjects.dd_projectList, "Project Tab Button"))
			{
				logPass("The user was able to click on the project list to search.");
				
				if(driver.isElementDisplayed(PricingObjects.dd_AllProjectContainer, "Project Tab Button"))
				{
					logPass("The Project Container is displayed.");
					if(driver.isElementDisplayed(PricingObjects.dd_AllProjectValues, "All Project Values"))
					{
						int length = driver.getSize(PricingObjects.dd_AllProjectValues, "All Project Values");
						boolean projectFound = false;
						int i = 0;
						By Projectxpath = null;
						for(i = 1; i<=length; i++)
						{
							Projectxpath = By.xpath("("+ PricingObjects.dd_AllProjectValues.toString().replaceAll("By.xpath: ", "").concat(")[" + i + "]"));
							String currentText = driver.getText(Projectxpath, "Current project Xpath");
							if(currentText.equalsIgnoreCase("All Approved"))
							{
								logInfo("The Current project name : " + currentProjectName + " is found from the list.");
								projectFound = true;
								break;
							}
							else
							{
								continue;
							}
						}
						
						if(projectFound)
						{
							logPass("The added project is found in the list.");
							if(driver.jsClickElement(Projectxpath, "Project Xpath"))
							{
								logInfo("The Searched Project is clicked.");
								try 
								{
									Thread.sleep(1000);
								} 
								catch (InterruptedException e) 
								{
									e.printStackTrace();
								}
								
								/*
								 * if(driver.jsClickElement(PricingObjects.btn_search, "Search Button")) {
								 * logInfo("The Searched Text was : " + currentPriceListName); try {
								 * Thread.sleep(1000); } catch (InterruptedException e) { e.printStackTrace(); }
								 * logPass("The user was able to click on the Search button in the All Price List page."
								 * ); loadAndWaitForLoadingGauage(); } else {
								 * logFail("The user was not able to click on the Search button in the All Price List page."
								 * );
								 * skipException("Intentionally Failing the Script as the user failed to find the Search button."
								 * ); }
								 */
								
								if(driver.jsClickElement(PricingObjects.btn_search1, "Search Button"))
								{
									logInfo("The Searched Text was : " + currentPriceListName);
									try 
									{
										Thread.sleep(1000);
									} 
									catch (InterruptedException e) 
									{
										e.printStackTrace();
									}
									logPass("The user was able to click on the Search button in the All Price List page.");
									loadAndWaitForLoadingGauage();
								}
								else
								{
									logFail("The user was not able to click on the Search button in the All Price List page.");
									skipException("Intentionally Failing the Script as the user failed to find the Search button.");
								}
								projectSet = true;
							}
							else
							{
								logFail("The Searched Project is not clicked.");
								skipException("Intentionally failing the script as the project " + currentProjectName + "is not clicked.");
							}
						}
						else
						{
							logFail("The added project is not found in the list.");
							skipException("Intentionally failing the script as the added project is not found in the list.");
						}
						
					}
					else
					{
						skipException("Intentionally failing the script as the added project is not found in the list.");
					}
				}
				else
				{
					logFail("The Project Container is not clicked.");
					skipException("Intentionally failing the script as the project container is not clicked.");
				}
			}
			else
			{
				logFail("The user failed to click on the project list to search.");
				skipException("Intentionally failing the script as the project list is not clicked.");
			}
		}
		else
		{
			logInfo("The project list drop down tab is not displayed.");
		}
		return projectSet;
	}
	
	/*
	 * Verify ALL SKU Tab is clicked
	 */
	public boolean  verifyImportTabIsDisplayed()
	{
		boolean tabDisplayed = false;
		if(driver.explicitWaitforVisibility(PricingObjects.importTab, "Import Tab"))
		{
			logPass("The Import Tab is displayed.");
			tabDisplayed = true;
		}
		else
		{
			logFail("The Import Tab is not displayed.");
			skipException("Intentionally failing the script as the import tab is not displayed.");
		}
		return tabDisplayed;
	}
	
	/*
	 * Click Import Tab
	 */
	public void clickImportTab()
	{
		boolean importTabDisplayed = verifyImportTabIsDisplayed();
		if(importTabDisplayed)
		{
			logPass("The Import Tab is displayed.");
			if(driver.jsClickElement(PricingObjects.importTab, "Import Tab"))
			{
				logPass("The import tab is clicked.");
			}
			else
			{
				logFail("The import tab is not clicked.");
				skipException("Intentionally failing the script as the import tab is not clicked.");
			}
		}
		else
		{
			logFail("The Import Tab is not displayed.");
			skipException("Intentionally failing the script as the import tab is not displayed.");
		}
	}
	
	/*
	 * Import Data From Sheet
	 */
	public void uploadImportDataSheetForPricing()
	{
		try
		{
			Thread.sleep(1500);
			WebElement browse = driver.element(PricingObjects.txt_importFile);
			String filename = BaseClass.UserDir + BaseClass.properties.getProperty("PricingDataSheetPath");
			browse.sendKeys(filename);
			Thread.sleep(1500);
		}
		catch (Exception e)
		{
			logFail("There is some issue in uploading Import Data Sheet. The Error is : " + e.getMessage());
		}
	}
	
	/*
	 * Wait For Start Import
	 */
	public boolean verifyStartImportButtonIsDisplayed()
	{
		boolean startImportDisplayed = false;
		if(driver.isElementDisplayed(PricingObjects.btn_StartImport, "Start Import"))
		{
			logPass("The Start Import button is displayed.");
			startImportDisplayed = true;
		}
		else
		{
			logFail("The Start Import button is not displayed.");
		}
		return startImportDisplayed;
	}
	
	/*
	 * Verify User able to Import 
	 */
	public void verifyUserImportSKU()
	{
		boolean startImportDisplayed = verifyStartImportButtonIsDisplayed();
		if(startImportDisplayed)
		{
			logPass("The Start Import button is displayed.");
			if(driver.jsClickElement(PricingObjects.btn_StartImport, "Start Import"))
			{
				logPass("The user was able to click on the start import button.");
				//waitForToastMessage();
				try 
				{
					Thread.sleep(2500);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				
				loadAndWaitForLoadingGauage();
			}
			else
			{
				logFail("The user failed to click on the start import button.");
				skipException("Intentionally failing the script as the user was not able to click on the Start Import button.");
			}
		}
		else
		{
			logFail("The Start Import button is not displayed.");
			skipException("Intentionally failing the script as the Start Import button is not displayed.");
		}
	}
	
	/*
	 * Verify SKU Import Successfully
	 */
	public void verifySKUImportedSuccessfully()
	{
		if(driver.isElementDisplayed(PricingObjects.importSuccessContainer, "Import Success Container"))
		{
			logPass("The import success container is displayed.");
			String expText = ExcelReader.getData("Pricing", "PricingSuccessMessage");
			String actText = driver.getText(PricingObjects.importSuccessContainer, "Import Success Container");
			
			if(expText.equalsIgnoreCase(actText))
			{
				logPass("There is no mismatch in the expected and the actual success message.");
			}
			else
			{
				logInfo("There is some mismatch in the expected and the actual success message.");
				logInfo("The expected message was : " + expText);
				logInfo("The actual message is : " + actText);
			}
			
			int count = 0;
			do
			{
				try 
				{
					Thread.sleep(5000);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				count++;
			}while(count<5);
		}
		else
		{
			logFail("The import success container is not displayed.");
		}
	}
	
	/*
	 * Verify Added SKU is displayed
	 */
	public void verifyAddedSKUIsImported()
	{
		boolean skuDisplayed = editSKUDetails();
		if(skuDisplayed)
		{
			String[] Sku = ExcelReader.getData("Pricing", "SKUToValidate").split("<b>");
			int row = driver.getSize(PricingObjects.skuRowContainer, "Sku Row");
			int i = 0;
			for(i = 1;i<=row; i++)
			{
				By idValue = By.xpath("(" + PricingObjects.skuIdValue.toString().replaceAll("By.xpath: ", "").concat(")[" + (i) + "]"));
				String currentSKU = driver.getText(idValue, "SKU Value");
				if(Sku[i-1].equalsIgnoreCase(currentSKU))
				{
					logPass("The expected sku is found.");
				}
				else
				{
					logFail("The expected sku is not found.");
					logInfo("The expected SKU was : " + Sku[i-1]);
					logInfo("The actual SKU is : " + currentSKU);
				}
			}
		}
		else
		{
			logFail("The SKU Row container is not displayed.");
			skipException("The SKU Row container is not displayed.");
		}
	}
	
	/*
	 * Create Collection For Pricing
	 */
	public void createPricingCollection()
	{
		if(!collectionFound)
		{
			String resp = "";
			List<String> res = new ArrayList<String>();
			String url =  properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("BusinessId")+"&serviceName=pricing";
			//String url = currentDomain+"/adminservices/collections/?businessId="+502+"&serviceName=pricing";
			logInfo("The Create Collection constructed url was : " + url);
			String postData = "{\"name\":\"Admin UI Pricing Collection\",\"description\":\"Admin UI Pricing Collection Desc\",\"status\":\"ACTIVE\",\"businessId\":"+properties.getProperty("BusinessId")+",\"properties\":[{\"name\":\"applicablecurrencies\",\"value\":\"USD\"},{\"name\":\"timezone\",\"value\":\"PST\"},{\"name\":\"defaultlocale\",\"value\":\"en_US\"},{\"name\":\"applicablelocales\",\"value\":\"en_US\"},{\"name\":\"defaultfacets\",\"value\":\"variableMin,variableMax\"}]}";
			//String postData = "{\"name\":\"Admin UI Pricing Collection\",\"description\":\"Admin UI Pricing Collection Desc\",\"status\":\"ACTIVE\",\"businessId\":553,\"properties\":[{\"name\":\"applicablecurrencies\",\"value\":\"USD\"},{\"name\":\"timezone\",\"value\":\"PST\"},{\"name\":\"defaultlocale\",\"value\":\"en_US\"},{\"name\":\"applicablelocales\",\"value\":\"en_US\"},{\"name\":\"defaultfacets\",\"value\":\"variableMin,variableMax\"}]}";
			logInfo("The Create Collection post data was : " + postData);
			res = api.ReadAllDataFromUrl(url, postData, sessionId);
			resp = res.get(0);
			logInfo("The Create Collection Response was was : " + resp);
			
			try
			{
				JSONObject root = new JSONObject(resp);
				if(root.has("collectionResponse"))
				{
					JSONObject collectionResponse = root.getJSONObject("collectionResponse");
					if(collectionResponse.has("id"))
					{
						currentPricingCollectionId = collectionResponse.getString("id");
						properties.put("PricingCollectionID", currentPricingCollectionId);
						if(collectionResponse.has("name"))
						{
							String actName = collectionResponse.getString("name");
							properties.put("PricingCollectionName", actName);
						}
					}
					else
					{
						logInfo("The id is not in the collection response. The response was : " + collectionResponse);
					}
				}
				else
				{
					logInfo("The collection response is not in the create collection response. The response was : " + root);
					getPricingBusinessCollection();
				}
				
			}
			catch (Exception e) 
			{
				logInfo("There is some issue in getting the Created Collection Id : " + e.getMessage());
				logInfo("The response from the stream was : " + resp);
			}
		}
		else
		{
			logInfo("The collection with the name : " + currentCollectionName);
		}
	}
	
	/*
	 * Create Price List
	 */
	public void createPriceListForPricing()
	{
		String resp = "";
		List<String> res = new ArrayList<String>();
		//"https://eintadmin.skavacommerce.com/adminservices/prices/pricelists/pricelistfacets/?x-collection-id=811"
		//String url = currentDomain+"/adminservices/collections/?businessId="+properties.getProperty("BusinessId")+"&serviceName=pricing";
		String url =  properties.getProperty("Domain")+"/admin/services/prices/pricelists/pricelistfacets/?x-collection-id="+currentPricingCollectionId;
		logInfo("The Create Pricelist constructed url was : " + url);
		String storeName = "Store PriceList 1";
		//String postData = "{\"name\":\"Admin UI Pricing Collection\",\"description\":\"Admin UI Pricing Collection Desc\",\"status\":\"ACTIVE\",\"businessId\":"+properties.getProperty("BusinessId")+",\"properties\":[{\"name\":\"applicablecurrencies\",\"value\":\"USD\"},{\"name\":\"timezone\",\"value\":\"PST\"},{\"name\":\"defaultlocale\",\"value\":\"en_US\"},{\"name\":\"applicablelocales\",\"value\":\"en_US\"},{\"name\":\"defaultfacets\",\"value\":\"variableMin,variableMax\"}]}";
		String postData = "{\"name\":\""+storeName+"\",\"description\":\"Store PriceList\",\"status\":\"ACTIVE\",\"currencySign\":\"USD\",\"priceFacets\":[]}";
		logInfo("The Create Pricelist post data was : " + postData);
		res = api.ReadAllDataFromUrl(url, postData, sessionId);
		resp = res.get(0);
		try 
		{
			JSONObject root = new JSONObject(resp);
			if(root.has("id"))
			{
				apiPriceList = root.getString("id");
			}
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		logInfo("The Create Pricelist Response was : " + resp);
	}
	
	/*
	 * Get Business Collection
	 */
	public void getPricingBusinessCollection()
	{
		String res = "";
		String url =  properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("BusinessId")+"&serviceName=pricing";
		//String url = currentDomain+"/adminservices/collections/?businessId="+502+"&serviceName=pricing";
		res = api.readResponseFromUrl(url, sessionId);
		//System.out.println(res);
		try
		{
			JSONObject root = new JSONObject(res);
			if(root.has("collectionDetails"))
			{
				logInfo("The business has collection details.");
				JSONObject collectionDetails = root.getJSONObject("collectionDetails");
				if(collectionDetails.has("collections"))
				{
					JSONArray collection = collectionDetails.getJSONArray("collections");
					int length = collection.length(); 
					if(length>0)
					{
						logInfo("The collection length greater than 0.");
						String expName = "Admin UI Pricing Collection";
						for(int i = 0; i<length; i++)
						{
							String actName = collection.getJSONObject(i).getString("name");
							if(expName.equalsIgnoreCase(actName))
							{
								collectionFound = true;
								currentPricingCollectionId = collection.getJSONObject(i).getString("id");
								properties.put("PricingCollectionFound", true);
								properties.put("PricingCollectionName", expName);
								currentCollectionName = expName;
								properties.put("PricingCollectionID", currentPricingCollectionId);
								break;
							}
							else
							{
								continue;
							}
						}
					}
					else
					{
						logInfo("The collection length is less than 0. The result is : " + collection);
					}
				}
				else
				{
					logInfo("The collection array list is not displayed.The result was : " + collectionDetails);
				}
			}
			else
			{
				logInfo("The collection details object is not in the json respones. The result was : " + root);
			}
		}
		catch (Exception e) 
		{
			logInfo("There is some issue in getting the collection id.");
		}
	}
	
	/*
	 * Click on View All Projects
	 */
	public void clickViewAllProjectsFromPricelistPage()
	{
		if(driver.isElementDisplayed(PricingObjects.btn_ViewAllProjects, "View All Projects"))
		{
			logPass("The View All Projects button is displayed.");
			if(driver.jsClickElement(PricingObjects.btn_ViewAllProjects, "View All Projects"))
			{
				logPass("The View All Projects button in the Project Create Container is clicked.");
				verifyUserNavigatedToAllProjectsPage();
			}
			else
			{
				logFail("The View All Projects button in the Project Create Container is not clicked.");
			}
		}
		else
		{
			logFail("The View All Projects button is not displayed.");
		}
	}
	
	
	/*
	 * Verify User Navigated to All Projects Page 
	 */
	public void verifyUserNavigatedToAllProjectsPage()
	{
		if(driver.isElementDisplayed(PricingObjects.txt_AllProjects, "All Projects"))
		{
			logPass("The user was navigated to All Projects Page.");
		}
		else
		{
			logFail("The user was not navigated to All Projects Page.");
			skipException("Intenitonally failing the scripts as the All Projects is not displayed");
		}
	}
	
	/*
	 * Verify The Project Row Container is displayed.
	 */
	public void verifyCreatedProjectsAreDisplayedInAllProjectsPage()
	{
		if(driver.isElementDisplayed(PricingObjects.projectRowContainer, "All Project Container"))
		{
			logPass("The All Projects Container is displayed.");
			int length = driver.getSize(PricingObjects.projectRowContainer, "All Project Container");
			logInfo("The total projects displayed in the All Projects List Page is : " + length);
			for(int i = 1; i<=length; i++)
			{
				By prjectXpath = By.xpath("("+PricingObjects.projectRowContainer.toString().replaceAll("By.xpath: ", "").concat(")[" + (i) + "]")+PricingObjects.txt_projectName.toString().replaceAll("By.xpath: ", ""));
				String currText = driver.getText(prjectXpath, "Project Xpath");
				logInfo("The Current Project Name was : " + currText);
				String count = String.valueOf(i);
				try
				{
					String mapProjectName = currentPriceListDetails.get("PriceListProject"+count);
					String expProjectName = mapProjectName;
					logInfo("The Current Project Name from map was : " + mapProjectName);
					
					if(expProjectName.equalsIgnoreCase(currText))
					{
						logPass("There is no mismatch in the expected and the actual Project Name in the All Projects Page.");
					}
					else
					{
						logFail("There is some mismatch in the expected and the actual Project Name in the All Projects Page.");
						logInfo("The expected project name was : " + expProjectName);
						logInfo("The actual project name is : " + currText);
					}
				}
				catch (Exception e) 
				{
					logFail("There is some issue in validating the Project Name in the All Project Page.");
					skipException("Intentionally failing the scripts as the Project Row Container is not displayed.");
				}
			}
		}
		else
		{
			logFail("The All Projects Container is not displayed.");
			skipException("Intentionally failing the scripts as the Project Row Container is not displayed.");
		}
	}
	
	/*
     * Associate Store with Collection Id
     */
    public void storeAssociateFoundation() throws JSONException
    {
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=api.ReadAllDataFromUrlserviceput(properties.getProperty("APIDomain")+"/foundationservices/stores/"+properties.getProperty("PricingStoreId")+"/associations","{\"associations\":[{\"collectionId\":\""+properties.getProperty("PricingCollectionID")+"\",\"defaultAssociation\":false,\"jurisdiction\":[{\"countryCode\":\"US\",\"documentId\":\""+apiPriceList+"\"}],\"name\":\"pricing\",\"properties\":[],\"type\":null}]}",foundationSessionId,"");
        resp = res.get(0);
        logPass("storeassocatefoundation "+resp);
    }
    
    public void deleteSku()
    {
    	///if(driver.isElementDisplayed(PricingObjects.price_delete_button, "delete price"))
    	//{
    	//	logPass("Delete icon is displayed");
    		driver.moveToElement(PricingObjects.deletePriceButton, "Delete");
    		if(driver.jsClickElement(By.xpath("//*[@data-qa='delete-button-trigger'])[1]"), "Delete"));
			/*
			 * { logPass("Delete icon is clicked");
			 * if(driver.isElementDisplayed(PricingObjects.price_delete_popup_submit,
			 * "delete price popup submit")) {
			 * logPass("Delete popup submit button is displayed");
			 * if(driver.moveToElementAndClick(PricingObjects.price_delete_popup_submit,
			 * "Delete popup submit")) { logPass("Delete popup submit button is clicked"); }
			 * else { logFail("Delete popup submit button not clicked"); } } else {
			 * logFail("Delete popup submit button not displayed"); } } else {
			 * logFail("Delete icon not clicked"); }
			 */
    	//}
    	//else
    	//{
    	//	logFail("Delete icon not displayed");
    	//}
    			
    	loadAndWaitForLoadingGauage();
    }

    
    /*Delete price check & click*/
	public void verifyClickDeletePrice()
	{
		driver.scrollToElement(PricingObjects.deletePriceButton, "Delete price button");
		if(driver.mouseHoverElement(By.xpath(PricingObjects.lastSKUContainerRow.toString().replace("By.xpath: ", "").concat("[1]")),"price row container") &&
				driver.isElementDisplayed(By.xpath(PricingObjects.deletePriceButton.toString().replace("By.xpath: ", "").concat("[1]")),"Delete price button"))
		//if(driver.isElementDisplayed(ProjectPricePage.deletePriceButton,60, "Sku's Lists"))
		{
			int deleteCount = driver.getSize(PricingObjects.deletePriceButton, "Delete");
			int randOfDelete = driver.generateRandomNumber(deleteCount);
			if(randOfDelete == 0)
			{
				randOfDelete = randOfDelete + 1;
			}
			driver.scrollToElement(By.xpath(PricingObjects.deletePriceButton.toString().replace("By.xpath: ", "").concat("[" +randOfDelete+ "]")), "Delete price ");
			driver.mouseHoverElement(By.xpath(PricingObjects.deletePriceButton.toString().replace("By.xpath: ", "").concat("[" +randOfDelete+ "]")), "Delete price ");
			
			if(driver.jsClickElement(By.xpath(PricingObjects.deletePriceButton.toString().replace("By.xpath: ", "").concat("[" +randOfDelete+ "]")), "SDelete price "))
			{
				deletePricePopUp("delete");
				/*
				 * if(driver.isElementDisplayed(PricingObjects.deletePriceToast,
				 * "delete price toast")) { logPass("Price delete toast message is displayed");
				 * if(driver.getText(PricingObjects.deletePriceToast,
				 * "delete price toast").toLowerCase().contains("success")) {
				 * logPass("Price delete is successfull");
				 * 
				 * if(driver.isElementDisplayed(By.xpath(PricingObjects.priceDeletedStatus.
				 * toString().replace("By.xpath: ", "").concat("[" +randOfDelete+ "]")),
				 * "Delete price status")) {
				 * logPass("Deleted Price is changed to deleted status"); } else {
				 * logFail("Deleted Price not changed to deleted status"); }
				 * 
				 * 
				 * //verifyDeltedPriceCount(deleteCount); } else {
				 * logFail("Price delete is unsuccessfull"); }
				 */
				waitForToastMessage();
				/*}
				else
				{
					logFail("Price delete toast message not displayed");
				}*/
			}
			else
			{
				logFail("Failed to click delete button");
			}
		}
		else
		{
			
			logFail("Failed to view Sku's");
		}
	}
	
	
    
    

	/*Delete price popup*/
	
	
	public void deletePricePopUp(String operation)
	{
		operation=operation.toLowerCase();
		if(driver.isElementDisplayed(PricingObjects.deletePriceContainer,  "delete container"))
		{
			logPass("Delete price container is displayed");
			
			switch (operation)
			{
				case "delete":
					
					if(driver.isElementDisplayed(PricingObjects.deletePriceContainer_Delete_Btn, "delete button"))
					{
						logPass("Delete price button is displayed");
						if(driver.clickElement(PricingObjects.deletePriceContainer_Delete_Btn, "delete button"))
						{
							logPass("Delete price button is clicked");
						}
						else
						{
							logFail("Delete price button not clicked");
						}
					}
					else
					{
						logFail("Delete price button not displayed");
					}
				break;
					
			  case "cancel":
					
					if(driver.isElementDisplayed(PricingObjects.deletePriceContainer_Cancel_Btn,  "Cancel button"))
					{
						logPass("Cancel button is displayed in delte price container");
						if(driver.clickElement(PricingObjects.deletePriceContainer_Delete_Btn, "Cancel button"))
						{
							logPass("Cancel price button is clicked");
						}
						else
						{
							logFail("Cancel price button not clicked");
						}
					}
					else
					{
						logFail("Cancel button not displayed in delte price container");
					}
					break;
			}
		}
		else
		{
			logFail("Delete price container not displayed");
		}
			
	}
	
	
	
}
