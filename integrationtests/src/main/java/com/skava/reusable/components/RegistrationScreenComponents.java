package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.UserRegistration_8;

public class RegistrationScreenComponents extends MyAccountComponents 
{

	/** Verifying Screen Name in User Registration Screen **/

	public void userRegistrationScreenName() {
		if(driver.isElementDisplayed(UserRegistration_8.sk_ScreenName,  "Screen Name"))
			logPass("User Registration Screen Name Displayed");
		else
			logFail("Failed to display the User Registration");

	}

	/** Verifying First Name Text field in User Registration Screen **/

	public void firstNameTextField() {
		if(driver.isElementDisplayed(UserRegistration_8.label_FirstName,  "First Name text field"))
		{
			String txt=driver.getText(UserRegistration_8.label_FirstName, "");
			logPass(txt+" text field is displayed ");
		}
		else
			logFail("Failed to display the First Name");
	}

	/** Verifying Last Name Text field in User Registration Screen **/

	public void lastNameTextField() {
		if(driver.isElementDisplayed(UserRegistration_8.label_LastName,  "Last Name text field"))
		{
			String txt=driver.getText(UserRegistration_8.label_LastName, "");
			logPass(txt+" text field is displayed ");
		}
		else
			logFail("Failed to display the Last Name");
	}

	/** Verifying Create Password Text field in User Registration Screen **/

	public void createPasswordTextField() {
		if(driver.isElementDisplayed(UserRegistration_8.label_CreatePassord,  "Create Password text field"))
		{
			String txt=driver.getText(UserRegistration_8.label_CreatePassord, "");
			logPass(txt+" text field is displayed ");
		}
		else
			logFail("Failed to display the create password field");
	}


	/** Verifying Confirm Password Text field in User Registration Screen **/

	public void cronfirmPasswordTextField() {
		if(driver.isElementDisplayed(UserRegistration_8.label_ConfirmPassord,  "Confirm Password text field"))
		{
			String txt=driver.getText(UserRegistration_8.label_ConfirmPassord, "");
			logPass(txt+" text field is displayed ");
		}
		else
			logFail("Failed to display the cofirm password field");	

	}

	/** Verifying register button in User Registration Screen **/
	public void registerButton() {
		if(driver.isElementDisplayed(UserRegistration_8.butn_Register,  "Register button"))
			logPass("Register Button get displayed");
		else
			logFail("Failed to display the button");
	}

	/** Verifying copyright content in User Registration screen **/
	public void copyrightContent() {
		String content = driver.getText(UserRegistration_8.txt_CopyrightContent, "Copyright Content");
		if(driver.compareElementText(UserRegistration_8.txt_CopyrightContent, content, "Copyright Content"))
			logPass("Compared & verified Copyright Content");
		else
			logFail("Failed to display Copyright content");
	}

	/** Verifying password hint in Create Password field **/
	public void passwordHint() {
		if(driver.clickElement(UserRegistration_8.txt_CreatePassword, "Create password"))
			logPass("Create pass word field click");
		else
			logFail("Create password field not display");
		
		String[] pwdhint = {"Must be atleast 8 characters","Must have: 1 number, 1 uppercase letter,","Only these special characters are allowed: @ $ ! % * ? &","Previous 8 passwords cannot be reused","Password expires after 90 days"};
		for(int i=0;i<pwdhint.length;i++)
		{
		if(driver.compareElementText(By.xpath("(//*[@data-qa='new-password-hint-txt'])["+(i+1)+"]"), pwdhint[i], "Password conditions"))
			logPass("Password Hint displayed "+pwdhint[i] );
		else
			logFail("Failed to display pasword hint");
		}
	}

	/** Blank First Name field **/
	public void firstNameField() {
		String firstnamevalue = driver.getText(UserRegistration_8.txt_FirstName, "First Name field");
		if(firstnamevalue.isEmpty()) {
			logPass("First Name field is Empty");
		}
		else
			logFail("Field is not empty ");
	}

	/** Blank Last Name field **/
	public void lastNameField() {
		String lastnamevalue = driver.getText(UserRegistration_8.txt_LastName, "Last Name field");
		if(lastnamevalue.isEmpty()) {
			logPass("Last Name field is Empty");
		}
		else
			logFail("Field is not empty ");
	}

	/** Blank Password field **/
	public void newPasswordField() {
		String newpasswordvalue = driver.getText(UserRegistration_8.txt_CreatePassword, "New Password field");
		if(newpasswordvalue.isEmpty()) {
			logPass("New Password field is Empty");
		}
		else
			logFail("Field is not empty ");
	}

	/** Blank Confirm Password field  **/
	public void confirmPasswordField() {
		String confirmpasswordvalue = driver.getText(UserRegistration_8.txt_ConfirmPassword, "New Password field");
		if(confirmpasswordvalue.isEmpty()) {
			logPass("Confirm Password field is Empty");
		}
		else
			logFail("Field is not empty ");
	}

	/** Click Register button **/
	public void clickRegister() {
		if(driver.isElementDisplayed(UserRegistration_8.butn_Register,  "Register button")) 
		{
			driver.jsClickElement(UserRegistration_8.butn_Register, "Register button");
			logPass("Register button is clicked");
		}
		else
			logFail("Register button is not clicked");
	}

	/** Validate Blank Field Error Message First name **/

	public void blankfirstNameInlineError() {
		
		if(driver.isElementDisplayed(UserRegistration_8.inerror_FirstName,  "First Name"))
		{
			String txt=driver.getText(UserRegistration_8.inerror_FirstName, "inerror_FirstName");
			logPass("Blank Field message displayed "+txt);
		}
		else
			logFail("Failed to display error message");
	}

	/** Validate Blank Field Error Message Last name **/

	public void blanklastNameInlineError() {
		if(driver.isElementDisplayed(UserRegistration_8.inerror_LastName,  "First Name"))
		{
			String txt=driver.getText(UserRegistration_8.inerror_LastName, "inerror_FirstName");
			logPass("Blank Field message displayed "+txt);
		}
		else
			logFail("Failed to display error message");
	}

	/** Validate Blank Field Error Message Password **/

	public void blankpasswordInlineError() {
		if(driver.isElementDisplayed(UserRegistration_8.inerror_CreatePassord
				,  "Create Password"))
		{
			String txt=driver.getText(UserRegistration_8.inerror_CreatePassord, "inerror_FirstName");
			logPass("Blank Field message displayed "+txt);
		}
		else
			logFail("Failed to display error message");
	}

	/** Validate Blank Field Error Message Confirm Password **/

	public void blankconfirmPasswordInlineError() {
		if(driver.isElementDisplayed(UserRegistration_8.inerror_ConfirmPassord
				,  "Confirm Password"))
		{
			String txt=driver.getText(UserRegistration_8.inerror_ConfirmPassord, "inerror_FirstName");
			logPass("Blank Field message displayed "+txt);
		}
		else
			logFail("Failed to display error message");
	}



	/** Inline error message as Please enter a valid password **/

	public void invalidPasswordInlineError() {
		if(driver.isElementDisplayed(UserRegistration_8.inerror_CreatePassord,  "Create Password"))
		{
			String txt=driver.getText(UserRegistration_8.inerror_CreatePassord, "");
			logPass("Please enter a valid password error displayed "+txt);
		}	
		else
			logFail("Failed to displayed the error message");
	}


	/** Sending Invalid Create Password **/

	public void sendInvalidPassword() {
		if(driver.enterText(UserRegistration_8.txt_CreatePassword, ExcelReader.getData("LoginCredentials", "Create Password"), "Password field"))
			logPass("Invalid Password is Entered");
		else
			logFail("Failed to entered password");
	}

	/** Entering Valid Confirm Password **/

	public void enteringvalidConfirmPassword() {
		if(driver.enterText(UserRegistration_8.txt_ConfirmPassword, ExcelReader.getData("LoginCredentials", "Confirm Password"), "Confirm Password field"))
			logPass("Confirm Password is Entered");
		else
			logFail("Failed to entered Confirm Password");
	}

	/** Entering Valid Create Password text in the field **/

	public void enteringValidCreatePassword() {
		if(driver.enterText(UserRegistration_8.txt_CreatePassword, ExcelReader.getData("LoginCredentials", "Create Password"), "Create Password field"))
			logPass("Create Password is Entered");
		else
			logFail("Failed to entered Create Password");
	}

	/** Password & Confirm Password doesn't match  **/

	public void passwordAndConfirmPasswordMismatch() {
		if(driver.isElementDisplayed(UserRegistration_8.inerror_CreatePassord,  "Password mismatch"))
		{
			String txt=driver.getText(UserRegistration_8.inerror_CreatePassord, "");
			logPass("Create Password & Confirm Password Mismatch is displayed "+txt);
		}
			
		else
			logFail("Failed to display error message");
	}

	/** Entering First Name in User Registration screen **/

	public void enterUserFirstName() {
		if(driver.enterText(UserRegistration_8.txt_FirstName, ExcelReader.getData("LoginCredentials", "FirstName"), "FirstName field"))
			logPass("First Name entered in the field");
		else
			logFail("Failed to enter first name");
	}

	/** Entering Last Name in User Registration screen **/

	public void enterUserLastName() {
		if(driver.enterText(UserRegistration_8.txt_LastName, ExcelReader.getData("LoginCredentials", "LastName"), "LastName field"))
			logPass("Last Name entered in the field");
		else
			logFail("Failed to enter last name");
	}

	/** Success Message for valid user registration  **/

	public void successUserAccountRegistrationMessage() {
		if(driver.explicitWaitforVisibility(UserRegistration_8.content_SuccessMsg, "Registration Success"))
		{
			String txt=driver.getText(UserRegistration_8.content_SuccessMsg, "");
			logPass("Successful Registration Message displayed "+txt);
		}	
		
	}




}

