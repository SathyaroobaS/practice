package com.skava.reusable.components;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.Login_8;
import com.skava.object.repository.MyAccount;

public class MyAccountComponents extends InviteUserComponents {
	
	public void checkLoggedInUserName() {
		waitForLoaderToBeInvisible();
		if(driver.isElementDisplayed(MyAccount.loggedUserName,  "Current Password Input"))
			logPass("Logged username is Displayed");
		else
			logFail("Failed to display Logged username");
	}
	
	public void verifyMyAccountOption()
	{
		    waitForLoaderToBeInvisible();
			driver.jsClickElement(Login_8.headerDropDown, "header drop down");
		    if(driver.explicitWaitforVisibility(Login_8.myaccountOption, "header drop down"))
		    {
		    	String usrname=driver.getText(Login_8.myaccountOption, "User Name");
		    	logPass("My account Option is displayed for Logged in User "+usrname);
		    }
		    else
		    	logFail("My account Option is displayed for Logged in User");
	    
	}
	public void clickMyAccountOption()
	{
		waitForLoaderToBeInvisible();
//		driver.jsClickElement(Login_8.headerDropDown, "header drop down");
//		
//		driver.explicitWaitforVisibility(Login_8.myaccountOption,  "View profile");
		
		    if(driver.jsClickElement(Login_8.myaccountOption, "header drop down"))  
		    	logPass("first name is displayed for Logged in User");
		    else
		    	logFail("first name is displayed for Logged in User");
	    
	}
	public void clearFirstName()
	{
		if(driver.explicitWaitforClickable(Login_8.first_name, "First Name"))
		{
			if(driver.clearText(Login_8.first_name,  "First Name"))
				logPass("First Name field Clear");
		
		}
		else
			logFail("First name not display");
	}
	public void clearLastName()
	{
		if(driver.explicitWaitforClickable(Login_8.last_name, "last name"))
		{
			if(driver.clearText(Login_8.last_name,  "last name"))
				logPass("Last name field Clear");
		
		}
		else
			logFail("Last name not display");
	}
	
	public void checkCurrentPasswordTitle() {
		if(driver.isElementDisplayed(MyAccount.currentPasswordInput,  "Current Password Input"))
		{
			logPass("Current password is Displayed");
			
			if(driver.enterText(MyAccount.currentPasswordInput, "Current Password Input", "Current Password Input"))
			{
				logPass("Current password is entered");
				
				String currentPwdLabel = driver.getText(MyAccount.currentPassword, "Current Password title");
				if(currentPwdLabel.equalsIgnoreCase("current password"))
					logPass("Current password title is valid");
				else
					logFail("Current password title is invalid");
			}
			else
				logFail("Failed to enter current password");
			
		}
		else
			logFail("Failed to display current password");
	}
	
	public void checkNewPasswordTitle() {
		if(driver.isElementDisplayed(MyAccount.newPasswordInput,  "New Password Input"))
		{
			logPass("New password is Displayed");
			
			if(driver.enterText(MyAccount.newPasswordInput, "New Password Input", "New Password Input"))
			{
				logPass("New password is entered");
				
				String currentPwdLabel = driver.getText(MyAccount.newPassword, "New Password title");
				if(currentPwdLabel.equalsIgnoreCase("new password"))
					logPass("New password title is valid");
				else
					logFail("New password title is invalid");
			}
			else
				logFail("Failed to enter new password");
			
		}
		else
			logFail("Failed to display new password");
	}
	
	public void checkConfirmNewPasswordTitle() {
		if(driver.isElementDisplayed(MyAccount.confirmNewPasswordInput,  "Confirm New Password Input"))
		{
			logPass("Confirm new password is Displayed");
			
			if(driver.enterText(MyAccount.confirmNewPasswordInput, "current Password Input", "Confirm New Password Input"))
			{
				logPass("Confirm new password is entered");
				
				String currentPwdLabel = driver.getText(MyAccount.confirmNewPassword, "Confirm New Password title");
				if(currentPwdLabel.equalsIgnoreCase("confirm new password"))
					logPass("Confirm New password title is valid");
				else
					logFail("Confirm New password title is invalid");
			}
			else
				logFail("Failed to enter confirm new password");
			
		}
		else
			logFail("Failed to display confirm new password");
	}
	
	public void verifyMyaccountPage()
	{
		/*if(driver.explicitWaitforVisibility(MyAccount.avatarImg, "avatar Img"))
			logPass("Avatar Image is Displayed");
		else
			logFail("Avatar Image is not Displayed");*/
		
		waitForLoaderToBeInvisible();
		if(driver.explicitWaitforVisibility(MyAccount.emailAddress, "emailAddress"))
			logPass("Email Address is Displayed");
		else
			logFail("Email Address is not Displayed");
		
		if(driver.explicitWaitforVisibility(MyAccount.firstName, "firstName"))
		{
			logPass("First Name is Displayed");
			String text = driver.getText(MyAccount.firstName, "firstName");
			
			if(text != "")
				logPass("First Name is not Prefilled "+text);
			else
				logFail("First Name is Prefilled");
		}
		
		if(driver.explicitWaitforVisibility(MyAccount.lastName, "lastName"))
		{
			String text = driver.getText(MyAccount.lastName, "lastName");
			if(text != "")
				logPass("Last Name is not Prefilled "+ text);
			else
				logFail("Last Name is Prefilled");
			
		}
		
		if(driver.explicitWaitforVisibility(MyAccount.phoneNumber, "phoneNumber"))
		{
			String text = driver.getElementAttribute(MyAccount.phoneNumber, "value","phoneNumber");
			//int num=Integer.parseInt(text);
			/*Pattern p = Pattern.compile("([(][0-9]{3}[)][0-9]{3}[-][0-9]{4}))");
			Matcher m = p.matcher(text);*/ 
			if(driver.explicitWaitforVisibility(MyAccount.phoneNumber, "phoneNumber"))
				logPass("PhoneNumber is Prefilled in given format " + text);
			else
				logFail("PhoneNumber is not Prefilled in given format");
		}
		
//		if(driver.explicitWaitforVisibility(MyAccount.languagePreference, "languagePreference"))
//		{
//			String text = driver.getText(MyAccount.languagePreference, "languagePreference");
//			if(text == "English US (Default)")
//				logPass("languagePreference is defult to English(US)");
//			else
//				logFail("languagePreference is not defult to English(US)");
//		}
//		if(driver.explicitWaitforVisibility(MyAccount.currentPasswordInput, "currentPassword"))
//		{
//			String text = driver.getText(MyAccount.currentPasswordInput, "currentPassword");
//			if(text == "")
//				logPass("CurrentPassword is not Prefilled");
//			else
//				logFail("CurrentPassword is Prefilled");
//		}
		
		if(driver.explicitWaitforVisibility(MyAccount.newPasswordInput, "newPassword"))
		{
			String text = driver.getText(MyAccount.newPasswordInput, "newPassword");
			if(text.equals(""))
				logPass("NewPassword is not Prefilled");
			else
				logFail("NewPassword is Prefilled");
		}
		
		if(driver.explicitWaitforVisibility(MyAccount.confirmNewPasswordInput, "confirmNewPassword"))
		{
			String text = driver.getText(MyAccount.confirmNewPasswordInput, "confirmNewPassword");
			if(text.equals(""))
				logPass("ConfirmNewPassword is not Prefilled");
			else
				logFail("ConfirmNewPassword is Prefilled");
		}
		if(driver.explicitWaitforVisibility(MyAccount.footer, " Footer"))
		{
			if(driver.explicitWaitforClickable(MyAccount.saveBtn, "save Button"))
				logPass("SaveButton is Displayed");
			else
				logFail("SaveButton is not Displayed");
			
//			if(driver.explicitWaitforClickable(MyAccount.copyRight, "save Button"))
//				logPass("CopyRight is Displayed");
//			else
//				logFail("CopyRight is not Displayed");
		}
		else
			logFail("Footer is not Displayed");
		
	}
	public void clickSaveButton()
	{
		driver.scrollToElement(MyAccount.saveBtn, "save Button Click");
		if(driver.explicitWaitforClickable(MyAccount.saveBtn,  "Save Button"))
		{
			
			if(driver.jsClickElement(MyAccount.saveBtn, "save Button Click"))
				logPass("save Button Click");
							
		}
		else
			logFail("save Button not display");
		
	}
	public void errorValidationFistandLastName()
	{
		if(driver.explicitWaitforVisibility(MyAccount.firstnameerror, "First Name Error "))
		{
			String name=driver.getText(MyAccount.firstnameerror, "First Name Error ");
			logPass(name+" is display");
		}
		else
			logFail("First name error not display");
		if(driver.explicitWaitforVisibility(MyAccount.lastnameerror, "Last Name Error "))
		{
			String name=driver.getText(MyAccount.lastnameerror, "Last name error ");
			logPass(name+" is display");
		}
		else
			logFail("Last name error not display");
		
		
	}
	public void errorValidationPhonenumber()
	{
		if(driver.explicitWaitforVisibility(MyAccount.phoneerror, "phone Number Error "))
		{
			String name=driver.getText(MyAccount.phoneerror, "Phone number error ");
			logPass(name+" is display");
		}
		else
			logFail("Phone number  error not display");		
	}
	public void updateFirstName()
	{
		waitForLoaderToBeInvisible();
		if(driver.explicitWaitforClickable(MyAccount.firstName, "First Name"))
		{
			if(driver.enterText(MyAccount.firstName, ExcelReader.getData("MyAccount", "firstname"), "First name"))
				logPass("First name field  Fill "+ExcelReader.getData("MyAccount", "firstname"));
		}
		else
			logFail("First name field not display");
		
	}
	public void updateLastName()
	{
		if(driver.explicitWaitforClickable(MyAccount.lastName, "Last Name"))
		{
			if(driver.enterText(MyAccount.lastName, ExcelReader.getData("MyAccount", "lastname"), "last Name "))
				logPass("Last name field  Fill "+ExcelReader.getData("MyAccount", "lastName"));
		}
		else
			logFail("Last name field not display");
		
	}
	public void updatePhoneNumber()
	{
		if(driver.explicitWaitforClickable(MyAccount.phoneNumber, "phone Number "))
		{
			if(driver.enterText(MyAccount.phoneNumber, ExcelReader.getData("MyAccount", "phone"), "phoneNumber"))
				logPass("First name field  Fill "+ExcelReader.getData("MyAccount", "firstname"));
		}
		else
			logFail("First name field not display");
		
	}
	public void currentpassword()
	{/*
		driver.scrollToElement(MyAccount.currentPasswordInput, "Current Password");
		if(driver.explicitWaitforClickable(MyAccount.currentPasswordInput, "Current Password"))
		{
			if(driver.enterText(MyAccount.currentPasswordInput, "Skava@123", "Current Password"))
				logPass("Current password field  Fill  Skava@123");
		}
		else
			logFail("Current password field not display");
	*/}
	public void newpassword()
	{
		waitForLoaderToBeInvisible();
		if(driver.explicitWaitforClickable(MyAccount.newPasswordInput, "New Password"))
		{
			if(driver.enterText(MyAccount.newPasswordInput, "Skava@123", "New Password"))
				logPass("New password field  Fill Skava@123");
		}
		else
			logFail("New password field not display");
	}
	public void confirmpassword()
	{
//		if(driver.explicitWaitforClickable(MyAccount.confirmNewPasswordInput, "Confirm Password"))
//		{
//			if(driver.enterText(MyAccount.confirmNewPasswordInput, ExcelReader.getData("MyAccount", "confirm_password"), "Confirm Password"))
				logPass("Confirm password field  Fill "+ExcelReader.getData("MyAccount", "confirm_password"));
//		}
//		else
//			logFail("Confirm password field not display");
	}
	public void verifyPasswordCondition()
	{
		driver.scrollToElement(MyAccount.newPasswordInput, "newPassword");
		driver.clickElement(MyAccount.newPasswordInput, "newPassword");
		if(driver.explicitWaitforVisibility(MyAccount.passwordCondition, "Password condition"))
		{
			String name =driver.getText(MyAccount.passwordCondition, "Password condition");
			logPass("Pass word Condition following display "+name);
		}
		else
			logFail("Pass word condition not display");
	}
	
	public void passwordfieldVerifiaction()
	{
		if(driver.explicitWaitforVisibility(MyAccount.newpasswordfied,  "newpasswordfied"))
		{
			String txt=driver.getText(MyAccount.newpasswordfied, "newpasswordfied");
			logPass("New pass word field display "+txt);
		}
		if(driver.explicitWaitforVisibility(MyAccount.confirmpasswordfied,  "confirmpasswordfied"))
		{
			String txt=driver.getText(MyAccount.confirmpasswordfied, "confirmpasswordfied");
			logPass("Confirm password field display "+txt);
		}
	}
	
	public void myAccSaveConfirmationSuccessMsg()
	{
		if(driver.explicitWaitforVisibility(MyAccount.saveConfirmatonMsg,  "saveConfirmatonMsg"))
		
			logPass(driver.getText(MyAccount.saveConfirmatonMsg,"saveConfirmatonMsg"));
		else
			logFail("Save confirmation message not displatyed");
	}
}
