package com.skava.reusable.components;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;

import com.framework.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.BusinessPageObject;
import com.skava.object.repository.LoginPageObject;
import com.skava.object.repository.MicroServiceObject;


public class BusinessComponents extends LoginComponents 
{	

	public void VerifyCreateBusinessURLVerification() 
	{
		driver.explicitWaitforVisibility(BusinessPageObject.AddBusiness_Btn, "Add a Business Button");
		if(driver.jsClickElement(BusinessPageObject.AddBusiness_Btn, "Add a Business Button"))
		{
			logPass("Add a Business Button is clicked");
			if(driver.getCurrentUrl().contains("create"))
			{
				logPass("Create Business page is navigated");
			}
			else
			{
				logFail( "Create Business page is not navigated");
			}
		}
		else
		{
			logFail( "Add a Business Button is not clicked");
		}
	}
	
	public void UpdateBusinessNameandLinksInCreateBusinessPage() 
	{
        //EnterBusinessNameRandomly();
		
		// Enter Storage Service Link
		if(driver.enterText(BusinessPageObject.Business_Service_lnk,ExcelReader.getData("BusinessPage","ServiceLink"),"Storage Servie Link"))
		{
			logInfo("Storage Servie Link is entered");
		}
		else
		{
			logFail( "Storage Servie Link is not entered");
		}
		
		// Enter CDN Service Link
		if(driver.enterText(BusinessPageObject.Business_CDN_lnk,ExcelReader.getData("BusinessPage","CDNLink"),"CDN Link"))
		{
			logInfo("CDN Link is entered");
		}
		else
		{
			logFail( "CDN Link is not entered");
		}
		
	}
	
	
	/*
	 *  verify user can able to click on Add business button
	 */
	public void CreateBusinessButtonNavigation() 
	{
		
		/*if(driver.explicitWaitforVisibility(BusinessPageObject.Add_Business_Btn, 5, "Add a Business Button"))
		{
			logPass("Add a Business button is displayed");*/
			if(driver.jsClickElement(BusinessPageObject.Add_Business_Btn, "Add a Business"))
			{
				logPass("Add a Business button is clicked");
			}
			else
			{
				logFail( "Add a Business button is not clicked");
			}
		}
		/*else
		{
			logFail( "Add a Business button is not displayed");
		}
	}*/
	public void ActivateSingleMicroServices() 
	{
		// promotion, pricing, catelog, inventory selection
		// Select Promotion Checkbox
		if(driver.clickElement(BusinessPageObject.MicroService_Promotion_Chkbox, "Promotion Checkbox"))
		{
			logPass("Promotion Checkbox is selected");
		}
		else
		{
			logFail( "Promotion Checkbox is not selected");
		}
		
		// Create micro services
		if(driver.clickElement(BusinessPageObject.MicroService_Create_Btn, "Microservice Create Button"))
		{
			logPass("Microservice Create Button is clicked");
		}
		else
		{
			logFail( "Microservice Create Button is not clicked");
		}
	}
	
	/*
	 *  Created business from breadcrumb
	 */
	public void ClickOnCreatedBusinessFromBreadcrumb() 
	{
		try {
			if(driver.clickElement(BusinessPageObject.Business_lnk_breadcrumb, "Business Link"))
			{
				logPass("Created business link is clicked from breadcrumb");
			}
			else
			{
				logInfo( "Breadcrumb is not available to click for current selection");
			}
		} catch (Exception e) {
			// TODO: handle exception
			logInfo("Breadcrumb is not available to click for current selection");
		}
		
	}

	/*
	 *  Click on Edit Business page  
	 */
	
	public void ClickOnEditBusinessbutton() 
	{
		if(driver.clickElement(BusinessPageObject.Edit_Business_btn, "Edit Business Button"))
		{
			logPass("Edit Business Button is clicked");
		}
		else
		{
			logFail( "Edit Business Button is not clicked");
		}
	}
	
	
	
	/*
	 *  Edit list page
	 */
	public void ClickOnEditBusinessbuttonFromAllBusinessPage() 
	{
		if(driver.jsClickElement(BusinessPageObject.Edit_List_Btn, "Edit Business Button"))
		{
			logPass("Edit Business Button is clicked");
		}
		else
		{
			logFail( "Edit Business Button is not clicked");
		}
	}
	
	
	/*
	 *  Click on edit business save button
	 */
	public void ClickOnEditBusinessSavebutton() 
	{
		//StoresComponents store=new StoresComponents();
		if(driver.clickElement(BusinessPageObject.Edit_Business_Savebtn, "Edit Business Save Button"))
		{
			logPass("Edit Business Save Button is clicked");
			//store.VerifySuccessPopAfterCreatingBusiness();
			if(driver.isElementDisplayed(BusinessPageObject.Business_Success_Msg, "Success Pop-up after creating the business"))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Success Pop-up is displayed after creating the business");
				String SuccessMessage = driver.getText(BusinessPageObject.Business_Success_Msg, "Success Message");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Success Message is" +SuccessMessage);
				if(driver.clickElement(BusinessPageObject.Popup_Ok_Btn, "Pop Up OK Button"))
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, "Success Pop-up OK button is clicked");
				}
				else
				{
					ExtentTestManager.getTest().log(LogStatus.FAIL, "Success Pop-up OK button is not clicked");
				}
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.INFO, "Success Pop-up is not displayed after creating the business");
			}
		}
		else
		{
			logFail( "Edit Business Save Button is not clicked");
		}
	}
	
	public void ActivateMultipleMicroServicesInLoop() 
	{
		driver.explicitWaitforVisibility(BusinessPageObject.Microservice_Multiselection_checkbox_loop, "Microservice Checkbox");
		if(driver.isElementDisplayed(BusinessPageObject.Microservice_Multiselection_checkbox_loop, "Microservice Checkbox"))
		{
			int temp=0;
			for(int i=1;i<=2;i++)
			{
				int RandomCheckboxselect = driver.generateRandomNumberWithLimit(16, 1);
				temp=RandomCheckboxselect;
				if(i==2 && temp==RandomCheckboxselect)
				{
					RandomCheckboxselect = RandomCheckboxselect+1;
					if(RandomCheckboxselect==16)
					{
						RandomCheckboxselect=RandomCheckboxselect-1;
					}
				}

				By ServiceClick = By.xpath("("+BusinessPageObject.Microservice_Multiselection_checkbox_loop.toString().replace("By.xpath: ", "").concat("/input)["+(RandomCheckboxselect) + "]"));
				if(driver.jsClickElement(ServiceClick, "Random Checkbox Click"))
				{
					logPass("Microservice checkbox is clicked randomly");
				}
				else
				{
					logFail( "Microservice checkbox is not clicked randomly");
				}
			}	
		}
		// Create micro services
		if(driver.clickElement(BusinessPageObject.MicroService_Create_Btn, "Microservice Create Button"))
		{
			logPass("Microservice Create Button is clicked");
		}
		else
		{
			logFail( "Microservice Create Button is not clicked");
		}
	}
	
	public void ActivateMultipleMicroServices() 
	{
		// promotion, pricing, catelog, inventory selection
		// Select Promotion Checkbox
		if(driver.clickElement(BusinessPageObject.MicroService_Promotion_Chkbox, "Promotion Checkbox"))
		{
			logPass("Promotion Checkbox is selected");
		}
		else
		{
			logFail( "Promotion Checkbox is not selected");
		}
		// Select Catelog Checkbox
		if(driver.clickElement(BusinessPageObject.MicroService_Catelog_Chkbox, "Catelog Checkbox"))
		{
			logPass("Catelog Checkbox is selected");
		}
		else
		{
			logFail( "Catelog Checkbox is not selected");
		}
		/*
		// Select Inventory Checkbox
		if(driver.clickElement(BusinessPageObject.MicroService_Inventory_Chkbox, "Inventory Checkbox"))
		{
			logPass("Inventory Checkbox is selected");
		}
		else
		{
			logFail( "Inventory Checkbox is not selected");
		}
		
		// Select Pricing Checkbox
		if(driver.clickElement(BusinessPageObject.MicroService_Pricing_Chkbox, "Pricing Checkbox"))
		{
			logPass("Pricing Checkbox is selected");
		}
		else
		{
			logFail( "Pricing Checkbox is not selected");
		}*/
		
		// Create micro services
		if(driver.clickElement(BusinessPageObject.MicroService_Create_Btn, "Microservice Create Button"))
		{
			logPass("Microservice Create Button is clicked");
		}
		else
		{
			logFail( "Microservice Create Button is not clicked");
		}
	}
	
	/*
	 *  Verify user can able to update the business information to create business
	 */
	
	public void EnterDetailsForCreateBusinessInformationPage() 
	{	
		if(driver.isElementDisplayed(BusinessPageObject.CreateBusiness_DOM, "Create Business Page"))
		{
			logPass("Create Business page is displayed");
			// Create business - URL navigation
			if(driver.getCurrentUrl().contains("create"))
			{
				logPass("Create Business page is displayed");
			}
			else
			{
				logFail( "Create Business page is not displayed");
			}
			
			// Create Business Title comparison
			if(driver.compareElementText(BusinessPageObject.CreateBusiness_Title, ExcelReader.getData("BusinessPage", "Title"), "Create Business Title"))
			{
				 logPass("Create Business text compared and it matches.");
			}
			else
			{
				logPass("There is some mismatch in the Create Business text.");
				logInfo("The expected text was : " + ExcelReader.getData("BusinessPage", "Title"));
				logInfo("The actual text is : " + driver.getText(BusinessPageObject.CreateBusiness_Title, "Create Business Title"));	
			}
			// ||||||||||||||||||||||||||||| Enter the business Name |||||||||||||||||||||||||||||
			String businessname = ExcelReader.getData("BusinessPage", "RandomBusinessName");
	        int randomnumber=driver.generateRandomNumber(10000);
	        String finalBusinessName = businessname+randomnumber;
	        String BusinessIdCreate=data.getProperty("finalBusinessName");
			
	        if(driver.enterText(BusinessPageObject.Business_Name_Txt,finalBusinessName,"Business Name"))
			{
				logPass("Business Name is entered");
				logInfo("Business Name is : +finalBusinessName");
			}
			else
			{
				logFail( "Business Name is not entered");
			}	        
			// Enter Storage Service Link
			if(driver.enterText(BusinessPageObject.Business_Service_lnk,ExcelReader.getData("BusinessPage","ServiceLink"),"Storage Servie Link"))
			{
				logInfo("Storage Servie Link is entered");
			}
			else
			{
				logFail( "Storage Servie Link is not entered");
			}
			
			// Enter CDN Service Link
			if(driver.enterText(BusinessPageObject.Business_CDN_lnk,ExcelReader.getData("BusinessPage","CDNLink"),"CDN Link"))
			{
				logInfo("CDN Link is entered");
			}
			else
			{
				logFail( "CDN Link is not entered");
			}
			
		}
		else
		{
			logFail( "Create Business page is not displayed");
		}
	}
	
	/*public void EnterBusinessNameRandomly() 
	{	
		if(driver.isElementDisplayed(BusinessPageObject.CreateBusiness_DOM, 5, "Create Business Page"))
		{
			logPass("Create Business page is displayed");
			// Enter the business Name
			String businessname = ExcelReader.getData("BusinessPage", "RandomBusinessName");
		    int randomnumber=driver.generateRandomNumber(10000);
		    String finalBusinessName = businessname+" "+randomnumber;
			
		    if(driver.enterText(BusinessPageObject.Business_Name_Txt,finalBusinessName,"Storage Servie Link"))
			{
				logInfo("Business Name is entered");
			}
			else
			{
				logFail( "Business Name is not entered");
			}
			}
		else
		{
			logFail( "Create Business page is not displayed");
		}
	}*/
	
	 /*
     *  Verify business overview page validation
     */
    
    public void BusinessOverviewPageValidation() 
   	{
    	driver.explicitWaitforVisibility(BusinessPageObject.Business_Overview_Tab, "Business Overview Tab");
    	if(driver.isElementDisplayed(BusinessPageObject.Business_Overview_Tab, "Business Overview Tab"))
    	{
    		logPass("Business Overview Tab is displayed");
    		if(driver.jsClickElement(BusinessPageObject.Business_Overview_Tab, "Business Overview"))
    		{
    			logPass("Business Overview Tab is clicked");
    			// URL validation
    			if(driver.getCurrentUrl().contains("businesses"))
    			{
    				logPass("Business Overview page is navigated");
    			}
    			else
    			{
    				logFail( "Business Overview page is not navigated");
    			}
    		}
    		else
    		{
    			logFail( "Business Overview Tab is not clicked");
    		}
    	}
    	else
    	{
    		logFail( "Business Overview Tab is not displayed");
    	}
   	}
    
    
    public void BusinessOverviewPageComparision() 
   	{
    	try {
    		String currenturl1 = driver.getCurrentUrl();
        	logInfo("Current Url is : " + currenturl1);  
        	String businessidsplit1[]=currenturl1.split("=");
        	String businessid1=businessidsplit1[1];
        	logInfo("Created Business Id is : " + businessid1);  
        	String businessid=properties.getProperty("BusinessId");
        	 if(businessid.equals(businessid1))
             {
        		 logPass("Business id Compared");
                 logInfo("Business id When Created : " + businessid);
                 logInfo("Business id in URL  : " + businessid1);
             }
		} catch (Exception e) 
    	{
			// TODO: handle exception
	             logFail("Business id not compared");
	         }
		}
    
    
    public void AllBusinessPageValidation() 
   	{
    	// URL validation
    	if(driver.getCurrentUrl().contains("businesses"))
    	{
    		logPass("Business Overview page is navigated");
    	}
    	else
    	{
    		logFail( "Business Overview page is not navigated");
    	}
   	}
	
    public void SearchExistingBusiness() throws InterruptedException 
	{
    	Thread.sleep(4000);
    	//driver.explicitWaitforVisibility(BusinessPageObject.Businessid_Dropdown, "Business ID Dropdown");
    	if(driver.jsClickElement(BusinessPageObject.Businessid_Dropdown, "Business ID Dropdown"))
    	{
    		logPass("Business ID Dropdown is clicked");
    		// Enter the business ID
    		if(driver.enterText(BusinessPageObject.Businessid_txtbox,ExcelReader.getData("BusinessPage","SearchID"),"Business ID"))
			{
			driver.clickElement(BusinessPageObject.Search_btn, "Search Button");
			logPass("Search Result is displayed");
			}
			else
			{
				logFail( "User not Entered valid credentials");
			}
    		
    	}
    	else
    	{
    		logFail( "Business ID Dropdown is not clicked");
    	}
	}
    
    public void BusinessIDComparision()
    {
         try 
         {
        	 String currenturl = driver.getCurrentUrl();
             logInfo("Current Url is : " + currenturl);  
             String Businessidsplit[]=currenturl.split("=");
             String Businessid=Businessidsplit[1];
             logInfo("Created Business id is : " + Businessid);
             String BusinessID = driver.getText(BusinessPageObject.Business_id, "Business id");
                if(Businessid.equals(BusinessID))
                {
                    logPass("Business id Compared");
                    logInfo("Business id : " + Businessid);
                    logInfo("Business  : " + BusinessID);
                }
                else 
                {
                    logPass("Business id not compared");
          }
		}
        catch (Exception e)
         {
			// TODO: handle exception
        	 logFail("Business Id not compared");
		}
    }
    
    
    public void BusinessNameNavigation()
    {
        if(driver.clickElement(BusinessPageObject.BusinessName_Navigation, "Business Name in Store Overview page"))
        {
            logPass("Business Name is clicked");
        }
        else
        {
            logFail("Business Name is not clicked");
        }
    }
    
    public void generateActivationParam() throws JSONException
    {
        
        String url=properties.getProperty("APIDomain")+"/userservices/users/activation?locale=en_US";
        String postData="{ \"email\":\""+notActivatedUser+"\"}";
        
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=api.ReadAllDataFromUrl1(url,postData,"","1");
        
        logInfo("Url: "+url);
        logInfo("Postdata: "+postData);
        logInfo("check: "+res);
        if(!res.isEmpty())
        {
        resp = res.get(0);
        
        System.out.println("generateActivationParam-->"+res);
        logInfo("Activation: "+resp);
        }
        
        JSONObject root = new JSONObject(resp);
        
        activationParam=root.getJSONObject("customProperties").getString("lastactivationparam");
    }
    
    String userId = "";
    
    public String registerUser() throws JSONException
    {
        String url=properties.getProperty("Domain")+"/admin/services/auth/users?activationParam="+activationParam;
        String postData="{ \"firstName\": \"test\", \"lastName\": \"automation\", \"newPassword\": \"Skava@123\", \"confirmNewPassword\": \"Skava@123\"}";    
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=api.ReadAllDataFromUrl(url,postData,"");
        
        resp = res.get(0);
        
        JSONObject root = new JSONObject(resp);
        
        String email=root.getJSONObject("user").getString("email");
        userId=root.getJSONObject("user").getString("id");
        useremail=email;
        properties.put("busemailId", email);
        System.out.println(email+"is printed");
        return email;
    }
    
    
}
