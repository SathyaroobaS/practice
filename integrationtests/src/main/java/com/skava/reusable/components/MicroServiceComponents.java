package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.framework.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.BusinessPageObject;
import com.skava.object.repository.LoginPageObject;
import com.skava.object.repository.MicroServiceObject;
import com.skava.object.repository.StoresObject;


public class MicroServiceComponents extends CatalogComponents 
{	
	public void VerifyMicroServiceTitle() 
	{
		
		if(driver.compareElementText(MicroServiceObject.MicroService_Title, ExcelReader.getData("MicroServices", "Title"), "Micro Service Title"))
    	{
			 logPass("All Microservices text compared and it matches.");   		 
    	}
    	else
    	{
    		logFail("There is some mismatch in the All Microservices text.");  
    		logInfo("The expected text was :" + ExcelReader.getData("MicroServices", "Title")); 
    		logInfo("The actual text is :" + driver.getText(MicroServiceObject.MicroService_Title, "Micro Service Title"));  
    	}
		
		{
			String MicroserviceTitle = driver.getText(MicroServiceObject.MicroService_Title, "Micro Service Title");
		}
	}
	
	public void MicroserviceURLVerification() 
	{
		if(driver.getCurrentUrl().contains("microservices"))
		{
			 logPass("Microservice page is displayed");   
		}
		else
		{
			logFail("Microservice page is not displayed");  
		}
	}
	public void SelectMicroServiceInLoop() throws InterruptedException 
	{
		Thread.sleep(6000);
		if (driver.isElementDisplayed(MicroServiceObject.MicroService_Size, "MicroService Size"))
        {
    		int MicroService_size = driver.getSize(MicroServiceObject.MicroService_Size, "MicroService Size");
    		for(int i=1;i<=MicroService_size;i++)
    			{
    				By ServiceClick = By.xpath("(//*[@data-qa='microservices-tile'])["+(i) + "]");
    				if(i==2)
    				{
    						Thread.sleep(6000);
		    				if(driver.clickElement(MicroServiceObject.Microservices_breadcrumb, "Created MicroService is clicked"))
		    				{
		    					logPass("Microservices is clicked from the breadcrumb"); 
		    					driver.clickElement(ServiceClick, "Created MicroService is clicked");
		    				}		    				
		    				else
		    				{
		    					logFail("Microservices is not clicked from the breadcrumb");  
		    				}
    				} 
    				else if(i==1)
    				{
    					if(driver.clickElement(ServiceClick, "Created MicroService is clicked"))
    					{
    						logPass("First created microservice is clicked from the tile");  
    					}
    					else
    					{
    						logFail("First created microservice is not clicked from the tile");  
    					}
    				}
    			
    				
    				// Create Collection
    				Thread.sleep(2000);
    	    		if(driver.jsClickElement(MicroServiceObject.Createcollection_Btn, "Create Collection Button"))
    	    		{
    	    			logPass("Create Collection Button is clicked");  
    	    		}
    	    		else
    	    		{
    	    			logFail("Create Collection Button is not clicked");  
    	    		}
    	    		
    	    		// ||||||||||||||||||||||||||||| Enter the Collection Name |||||||||||||||||||||||||||||
    				String collectionname = ExcelReader.getData("BusinessPage", "RandomCollectionName");
    		        int randomnumber=driver.generateRandomNumber(1000);
    		        String finalCollectionName = collectionname+randomnumber;
    				
    		        if(driver.enterText(MicroServiceObject.Collection_Name,finalCollectionName,"CollectionName Text"))
    				{
    					logPass("Collection Name is entered");
    					logInfo("Collection Name is :" +finalCollectionName);
    				}
    				else
    				{
    					logFail("Collection Name is not entered");
    				}
    	    		
    		        // ||||||||||||||||||||||||||||| Enter the Collection Desc |||||||||||||||||||||||||||||
    				String collectiondesc = ExcelReader.getData("BusinessPage", "RandomCollectionDesc");
    		        int descrandomnumber=driver.generateRandomNumber(1000);
    		        String finalCollectionDesc = collectiondesc+descrandomnumber;
    				
    		        if(driver.enterText(MicroServiceObject.Collection_Desc,finalCollectionDesc,"Collection Desc Text"))
    				{
    					logPass("Collection Desc is entered");
    					logInfo("Collection Desc is :" +finalCollectionDesc);
    				}
    				else
    				{
    					logFail("Collection Desc is not entered");
    				}
    		        
    	    		
    	    		// Select status toggle switch
    	    		if(driver.clickElement(MicroServiceObject.Status_Toggle, "Status Toggle is clicked"))
    	    		{
    	    			logPass("Status Toggle is clicked");  
    	    		}
    	    		else
    	    		{
    	    			logFail("Status Toggle is not clicked");  
    	    		}
    	    		
    	    		// Click collection create button
    	    		driver.explicitWaitforVisibility(MicroServiceObject.Collection_Create_Btn, "Collection Create Button");
    	    		if(driver.clickElement(MicroServiceObject.Collection_Create_Btn, "Collection Create Button is clicked"))
    	    		{
    	    			logPass("Collection Create Button is clicked");  
    	    		}
    	    		else
    	    		{
    	    			logFail("Collection Create Button is not clicked");  
    	    		}
    	    		VerifySuccessPopAfterCreatingBusiness();
        }
    }
    	else
        {
    		logFail("Created MicroService is not clicked");  
        }
	}
	
	/*
	 *  Click on created microservice for update
	 */
	
	public void ClickPromotionMicroServiceToUpdate() 
	{
		
		if(driver.clickElement(MicroServiceObject.MicroService_Size, "Promotion Tile in MicroService")) // https://euat.skavaone.com/admin/foundation/businesses/microservices?businessId=1856
		{
			logPass("Promotion Tile is clicked");  
		}
		else
		{
			logFail("Promotion Tile is not clicked");  
		}

	}
	
	/*
	 *  Update the created micro service by clicking on "Configure Button"
	 */
	
	public void ClickOnConfigureButtonToUpdateCollection() throws InterruptedException 
	{
		//Thread.sleep(5000);
		driver.explicitWaitforVisibility(MicroServiceObject.Configure_Btn, "Configure Button");
		if(driver.jsClickElement(MicroServiceObject.Configure_Btn, "Configure Button")) // https://euat.skavaone.com/admin/foundation/businesses/microservices?businessId=1856
		{
			logPass("Configure Button is clicked");  
		}
		else
		{
			logFail("Configure Button is not clicked");  
		}
	}
	
	/*
	 *  Update the created collection
	 */
	public void UpdateCollection() 
	{
			// Generate Random Collection Name
			try
			{
				String texttoEnter = generateRandomStrings(10);
				if(driver.enterText(MicroServiceObject.Collection_Name, texttoEnter , "Collection Name"))
				{
					logPass("Collection Name is entered");
					logInfo("The entered text was : " + texttoEnter);
					
				}
			}
			catch (Exception e)
			{
				logFail("There is some error in Entering Collection Name , The error was : ");
				
			}
			
			// Generate Random Collection Desc
			try
			{
				String texttoEnter = generateRandomStrings(10);
				if(driver.enterText(MicroServiceObject.Collection_Desc, texttoEnter , "Collection Desc"))
				{
					logPass("Collection Desc is entered");
					logInfo("The entered text was : " + texttoEnter);
					
				}
			}
			catch (Exception e)
			{
				logFail("There is some error in Entering Collection Desc , The error was : ");
				
			}
			
			// Click collection save button
			if(driver.clickElement(MicroServiceObject.Update_Collection_Save_Btn, "Collection Save / Update Button is clicked"))
			{
				logPass("Collection Save / Update Button is clicked");  
			}
			else
			{
				logFail("Collection Save / Update Button is not clicked");  
			}
			VerifySuccessPopAfterCreatingBusiness();
		}
	
	public void ClickOnMicroserviceLinkInBreadcrumb() throws InterruptedException 
	{
		Thread.sleep(4000);
		if(driver.clickElement(MicroServiceObject.edit_microservice_brdcrumb, "Microservice Link"))
		{
			logPass("Microservice Link in breadcrumb is clicked");  
		}
		else
		{
			logFail("Microservice Link in breadcrumb is not clicked");  
		}
	
	}
	public void EditCollectionDesc() throws InterruptedException 
	{
		// Select MicroService Randomly
		if(driver.isElementDisplayed(MicroServiceObject.MicroService_Size, "MicroService Size"))
			{
			logPass("Created microservice is displayed");  
			int MicroService_size = driver.getSize(MicroServiceObject.MicroService_Size, "MicroService Size");
			int Service_Random = driver.generateRandomNumberWithLimit(MicroService_size, 1);
	    	By randomSelectedMicroservice = By.xpath(MicroServiceObject.MicroService_Size.toString().replace("By.xpath: ", "").concat("[" + (Service_Random) + "]"));
	        driver.clickElement(randomSelectedMicroservice, "Microservice is selected Randomly");
			}
		else
		{
			logFail("Created microservice is not displayed");  
		}
		// Edit the collection randomly
        if(driver.isElementDisplayed(StoresObject.Collection_Row, "Collection Row"))
		{
		logPass("Created Collection is displayed");  
		int collection_size = driver.getSize(StoresObject.Configure_Btn, "Collection Size");
		int Collection_Random = driver.generateRandomNumberWithLimit(collection_size, 1);
    	By randomSelectedCollection = By.xpath(StoresObject.Configure_Btn.toString().replace("By.xpath: ", "").concat("[" + (Collection_Random) + "]"));
    	driver.jsClickElement(randomSelectedCollection, "Collection is selected Randomly");
    	
    	// ||||||||||||||||||||||||||||| Enter the Collection Desc |||||||||||||||||||||||||||||
    			String collectiondesc = ExcelReader.getData("BusinessPage", "RandomCollectionDesc");
    	        int descrandomnumber=driver.generateRandomNumber(1000);
    	        String finalCollectionDesc = collectiondesc+descrandomnumber;
    			
    	        if(driver.enterText(MicroServiceObject.Collection_Desc,finalCollectionDesc,"Collection Desc Text"))
    			{
    				logPass("Collection Desc is entered");
    				logInfo("Collection Desc is : +finalCollectionDesc");
    			}
    			else
    			{
    				logFail("Collection Desc is not entered");
    			}
    	        
    			// Click collection create button
    			driver.explicitWaitforVisibility(MicroServiceObject.UpdateCollection_Save_Btn, "Collection Create Button");
    			//Thread.sleep(2000);
    	        if(driver.moveToElementAndClick(MicroServiceObject.eint_collection_sve, "Collection Create Button is clicked"))
    			{
    				logPass("Collection Create Button is clicked");  
    			}
    			else
    			{
    				logFail("Collection Create Button is not clicked");  
    			}
    			VerifySuccessPopAfterCreatingBusiness();
    			
		}
        else
        {
        	logInfo("Created Collection is not available for curren selection");  
        	//CreateCollection();
        	
        }
        
        
	}
		
	/*
	 *  Priya -------------- added on 29 - nov -18
	 */
	
	public void CreateCollection() throws InterruptedException 
	{
		// Create Collection
		//Thread.sleep(2000);
		driver.explicitWaitforVisibility(MicroServiceObject.Createcollection_Btn, "Create Collection Button");
		if(driver.jsClickElement(MicroServiceObject.Createcollection_Btn, "Create Collection Button"))
		{
			logPass("Create Collection Button is clicked");  
		}
		else
		{
			logFail("Create Collection Button is not clicked");  
		}
		
		// ||||||||||||||||||||||||||||| Enter the Collection Name |||||||||||||||||||||||||||||
		//Thread.sleep(3000);
		String collectionname = ExcelReader.getData("BusinessPage", "RandomCollectionName");
	    int randomnumber=driver.generateRandomNumber(1000);
	    String finalCollectionName = collectionname+randomnumber;
	    logInfo("randomcollection name is" +finalCollectionName );
		
	    if(driver.enterText(MicroServiceObject.Collection_Name,finalCollectionName,"CollectionName Text"))
		{
			logPass("Collection Name is entered");
			logInfo("Collection Name is : +finalCollectionName");
		}
		else
		{
			logFail("Collection Name is not entered");
		}
		
	    // ||||||||||||||||||||||||||||| Enter the Collection Desc |||||||||||||||||||||||||||||
		String collectiondesc = ExcelReader.getData("BusinessPage", "RandomCollectionDesc");
	    int descrandomnumber=driver.generateRandomNumber(1000);
	    String finalCollectionDesc = collectiondesc+descrandomnumber;
		
	    if(driver.enterText(MicroServiceObject.Collection_Desc,finalCollectionDesc,"Collection Desc Text"))
		{
			logPass("Collection Desc is entered");
			logInfo("Collection Desc is : +finalCollectionDesc");
		}
		else
		{
			logFail("Collection Desc is not entered");
		}
	    
		
		// Select status toggle switch
		if(driver.clickElement(MicroServiceObject.Status_Toggle, "Status Toggle is clicked"))
		{
			logPass("Status Toggle is clicked");  
		}
		else
		{
			logFail("Status Toggle is not clicked");  
		}
		
		// Click collection create button
		driver.explicitWaitforVisibility(MicroServiceObject.Collection_Create_Btn, "Collection Create Button");
		if(driver.clickElement(MicroServiceObject.Collection_Create_Btn, "Collection Create Button is clicked"))
		{
			logPass("Collection Create Button is clicked");  
		}
		else
		{
			logFail("Collection Create Button is not clicked");  
		}
		VerifySuccessPopAfterCreatingBusiness();
	
	}
	
	//Paramesh
    public void SelectMicroServiceInLoop1() throws InterruptedException 
    {
        if (driver.isElementDisplayed(MicroServiceObject.MicroService_Size, "MicroService Size"))
        {
            int MicroService_size = driver.getSize(MicroServiceObject.MicroService_Size, "MicroService Size");
           // for(int i=1;i<=MicroService_size;i++) // if need to validate all the services need to add this line and comment below
            for(int i=4;i<=5;i++)
                {
            		By ServiceClick = By.xpath("(//*[@data-qa='microservices-tile'])["+(i) + "]");
            		try 
            		{
	                    By microservicetxt = By.xpath(MicroServiceObject.microservice_txt.toString().replace("By.xpath: ", "").concat("[" + (i) + "]"));
	                    String servicexpath = driver.getText(microservicetxt, "Microservice Name");
	                     if(!(servicexpath.equalsIgnoreCase("Fulfillment")))
	                     {
	                    	 Thread.sleep(3000);	             	
	                         if(driver.clickElement(ServiceClick, "Created MicroService is clicked"))
	                         {
	                             logPass("First created microservice is clicked from the tile");  
	                             // Create Collection
	                             //Thread.sleep(2000);
	                             driver.explicitWaitforVisibility(MicroServiceObject.Createcollection_Btn, "Create Collection Button");
	                             if(driver.jsClickElement(MicroServiceObject.Createcollection_Btn, "Create Collection Button"))
	                             {
	                                 logPass("Create Collection Button is clicked");  
	                                 // ||||||||||||||||||||||||||||| Enter the Collection Name |||||||||||||||||||||||||||||
	                                 //String collectionname = ExcelReader.getData("BusinessPage", "RandomCollectionName");
	                                 String collectionname = driver.generateRandomString();	                                
	                                 int randomnumber=driver.generateRandomNumber(10000);
	                                 String finalCollectionName = collectionname+randomnumber;
	                                 
	                                 if(driver.enterText(MicroServiceObject.Collection_Name,finalCollectionName,"CollectionName Text"))
	                                 {
	                                     logPass("Collection Name is entered");
	                                     logInfo("Collection Name is :" +finalCollectionName);
	                                 }
	                                 else
	                                 {
	                                     logFail("Collection Name is not entered");
	                                 }
	                                 
	                                 // ||||||||||||||||||||||||||||| Enter the Collection Desc |||||||||||||||||||||||||||||
	                                 //String collectiondesc = ExcelReader.getData("BusinessPage", "RandomCollectionDesc");
	                                 String collectiondesc = driver.generateRandomString();
	                                 int descrandomnumber=driver.generateRandomNumber(10000);
	                                 String finalCollectionDesc = collectiondesc+descrandomnumber;	                                 
	                                 if(driver.enterText(MicroServiceObject.Collection_Desc,finalCollectionDesc,"Collection Desc Text"))
	                                 {
	                                     logPass("Collection Desc is entered");
	                                     logInfo("Collection Desc is :" +finalCollectionDesc);
	                                 }
	                                 else
	                                 {
	                                     logFail("Collection Desc is not entered");
	                                 }
	                                 
	                                 
	                                 // Select status toggle switch
	                                 if(driver.clickElement(MicroServiceObject.Status_Toggle, "Status Toggle is clicked"))
	                                 {
	                                     logPass("Status Toggle is clicked");  
	                                 }
	                                 else
	                                 {
	                                     logFail("Status Toggle is not clicked");  
	                                 }
	                                 
	                                 // Click collection create button
	                                 //driver.explicitWaitforVisibility(MicroServiceObject.Collection_Create_Btn, "Collection Create Button");
	                                 Thread.sleep(3000);
	                                 if(driver.clickElement(MicroServiceObject.Collection_Create_Btn, "Collection Create Button is clicked"))
	                                 {
	                                     logPass("Collection Create Button is clicked");  
	                                 }
	                                 else
	                                 {
	                                     logFail("Collection Create Button is not clicked");  
	                                 }
	                                 VerifySuccessPopAfterCreatingBusiness();
	                             }
	                             else
	                             {
	                                 logInfo("Collection Not available for currenctly selected microservice");  
	                             }
	                             
	                             Thread.sleep(3000);
		                         
		                         if(!driver.explicitWaitforVisibility(MicroServiceObject.loading_gauge, "Loading Gauge"))
		                         {
		                         //driver.explicitWaitforVisibility(MicroServiceObject.Microservices_breadcrumb, "Created MicroService is clicked");
			                         if(driver.clickElement(MicroServiceObject.Microservices_breadcrumb, "Created MicroService is clicked"))
			                         {
			                             logPass("Microservices is clicked from the breadcrumb"); 
			                             //driver.clickElement(ServiceClick, "Created MicroService is clicked");
			                         }                           
			                         else
			                         {
			                        	 logPass("Microservices is  clicked from the breadcrumb");  
			                         }   
		                         }
		                         else
		                         {
		                        	 logInfo("Loading Gauge not closed completely");
		                         }
	                         }
	                         else
	                         {
	                             logFail("Created microservice is not clicked from the tile");  
	                         }
	                         
	                     }
	                     else if(servicexpath.equalsIgnoreCase("Fulfillment"))
	                     {
	                    	 logInfo("Fulfillment service is skipped");
	                     }
	                    
						
					} 
            		catch (Exception e)
        			{
					// TODO: handle exception
					logInfo("Fulfillment service is selected");
        			}
 
        }
    }
        else
        {
            logFail("Created MicroService is not clicked");  
        }
    }


	
	
	
}
	

