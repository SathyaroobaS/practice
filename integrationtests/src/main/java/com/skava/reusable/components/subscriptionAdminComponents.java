package com.skava.reusable.components;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.networkutils.ApiReaderUtils;
import com.skava.object.repository.customerAdminRepository;
import com.skava.object.repository.subscriptionPage;


public class subscriptionAdminComponents extends RegistrationScreenComponents  {

	public void subscriptionListPageVerification() {
		if(driver.explicitWaitforVisibility(subscriptionPage.subscriptionListHeading, "SubscriptionAdminHeading")) {
			pass("Subscription List Page Displayed Successfully ");
			
		}
		else {
			fail("Subscription List Page Not Displayed ");
		}
	}
	
	
	public void subscriptionAdminVerification() {
		if(driver.explicitWaitforVisibility(subscriptionPage.subscriptionHeading, "SubscriptionListHeading")) {
			pass("Subscription Admin Page Displayed Successfully ");
			
		}
		else {
			fail("Subscription Admin Page Not Displayed ");
		}
	}
	
	
	public void subscriptionListHeadingVerification() {
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionId, "Subscription Id"))
			pass("Subscription Id Heading is Displayed");
		else
			fail("Subscription Id Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionSkuId, "Subscription Sku Id"))
			pass("Subscription Sku Id Heading is Displayed");
		else
			fail("Subscription Sku Id Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionSkuName, "Subscription Sku Name"))
			pass("Subscription Sku Name Heading is Displayed");
		else
			fail("Subscription Sku Name Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionRequestDate, "Subscription Request Date"))
			pass("Subscription Request Date Heading is Displayed");
		else
			fail("Subscription Request Date Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionUserName, "Subscription User Name"))
			pass("Subscription User Name Heading is Displayed");
		else
			fail("Subscription User Name Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionEmail, "Subscription Email"))
			pass("Subscription Email Heading is Displayed");
		else
			fail("Subscription Email Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionFrequency, "Subscription Frequency"))
			pass("Subscription Frequency Heading is Displayed");
		else
			fail("Subscription Frequency Heading is Not Displayed");
		
	}
	
	
	
	
	public void subscriptionListSize() {
		if(driver.explicitWaitforVisibility(subscriptionPage.subscriptionRowTrigger, "SubscriptionListRow")) {
			pass("Subscription List Row is Displayed ");
		int suscriptionListSize = driver.getSize(subscriptionPage.subscriptionRowTrigger, "Subscription List Row");
		
		if(suscriptionListSize>0) {
			pass("Customer Subscription List is Available ");
		}
		
		else {
			fail("Customer Subscription List is Not Available");
		}
		
		}
		else {
			fail("Subscription Admin Page Not Displayed ");
		}
	}
	
	
	
	public void searchSubscriptionId(String subId) {
		if(driver.explicitWaitforClickable(subscriptionPage.subIDSearchClick, "SubscriptionId Search")) {
			pass("Subscription Id Search is Displayed");
			driver.jsClickElement(subscriptionPage.subIDSearchClick, "SubscriptionId Search");
		if(driver.explicitWaitforVisibility(subscriptionPage.subIDSearchClick, "SubscriptionId Search Input")) {
			pass("Subscription Id Search Input is Displayed");
			driver.enterText(subscriptionPage.subIDSearchClick, subId, "SubscriptionId Search Input");
		}
		else
			fail("Subscription Id Search Input is Not Displayed");
		}
		else
			fail("Subscription Id Search is Not Displayed");
	}
	
	
	
	public void searchSubscriptionEmail(String Email) {
		if(driver.explicitWaitforClickable(subscriptionPage.subEmailSearchClick, "Subscription Email Search")) {
			pass("Subscription Email Search is Displayed");
			driver.jsClickElement(subscriptionPage.subEmailSearchClick, "Subscription Email Search");
		if(driver.explicitWaitforVisibility(subscriptionPage.subEmailSearchInput, "Subscription Email Search Input")) {
			pass("Subscription Email Search Input is Displayed");
			driver.enterText(subscriptionPage.subEmailSearchInput, Email, "Subscription Email Search Input");
		}
		else
			fail("Subscription Email Search Input is Not Displayed");
		}
		else
			fail("Subscription Email Search is Not Displayed");
	}
	
	public void searchSubscriptionSkuId(String SkuId) {
		if(driver.explicitWaitforClickable(subscriptionPage.subSkuIdSearchClick, "Subscription SkuId Search")) {
			pass("Subscription SkuId Search is Displayed");
			driver.jsClickElement(subscriptionPage.subSkuIdSearchClick, "Subscription SkuId Search");
		if(driver.explicitWaitforVisibility(subscriptionPage.subSkuIdSearchInput, "Subscription SkuId Search Input")) {
			pass("Subscription SkuId Search Input is Displayed");
			driver.enterText(subscriptionPage.subSkuIdSearchInput, SkuId, "Subscription SkuId Search Input");
		}
		else
			fail("Subscription SkuId Search Input is Not Displayed");
		}
		else
			fail("Subscription SkuId Search is Not Displayed");
	}
	
	
	public void searchSubscriptionSkuName(String SkuName) {
		if(driver.explicitWaitforClickable(subscriptionPage.subSkuNameSearchClick, "Subscription SkuName Search")) {
			pass("Subscription SkuName Search is Displayed");
			driver.jsClickElement(subscriptionPage.subSkuNameSearchClick, "Subscription SkuName Search");
		if(driver.explicitWaitforVisibility(subscriptionPage.subSkuNameSearchInput, "Subscription SkuName Search Input")) {
			pass("Subscription SkuName Search Input is Displayed");
			driver.enterText(subscriptionPage.subSkuNameSearchInput, SkuName, "Subscription SkuName Search Input");
		}
		else
			fail("Subscription SkuName Search Input is Not Displayed");
		}
		else
			fail("Subscription SkuName Search is Not Displayed");
	}
	
	
	
	public void selectLeftYear(String year) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, "Subscription Year")) {
			pass("Subscription Year Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription Year");
			driver.selectByValue(subscriptionPage.subLeftYearClick, year, "Subscription Year");
			pass("Subscription Year is Clicked");
		}
		else
			fail("Subscription Year Element is Not Displayed");
	}
	
	
	public void selectRightYear(String year) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, "Subscription Year")) {
			pass("Subscription Year Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription Year");
			driver.selectByValue(subscriptionPage.subRightYearClick, year, "Subscription Year");
			pass("Subscription Year is Clicked");
		}
		else
			fail("Subscription Year Element is Not Displayed");
	}
	
	
	public void selectLeftMonth(String month) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, "Subscription month")) {
			pass("Subscription month Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription month");
			driver.selectByValue(subscriptionPage.subLeftMonthClick, month, "Subscription month");
			pass("Subscription month is Clicked");
		}
		else
			fail("Subscription month Element is Not Displayed");
	}
	
	
	public void selectRightMonth(String month) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, "Subscription month")) {
			pass("Subscription month Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription month");
			driver.selectByValue(subscriptionPage.subRightMonthClick, month, "Subscription month");
			pass("Subscription month is Clicked");
		}
		else
			fail("Subscription month Element is Not Displayed");
	}
	
	
	
	public void selectLeftDate(String date) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, "Subscription Date")) {
			pass("Subscription Date Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription date");
			String a ="//*[@class='drp-calendar left']//child::tbody//td[contains(@data-title,'r')][text()='"+date+"']";
			System.out.println(a);
			driver.clickElement(By.xpath(subscriptionPage.subLeftDateSelect.toString().replace("By.xpath: ", "").concat("'" +date+ "']")),"Subscription Date");
			//driver.clickElement(By.xpath("//*[@class='drp-calendar left']//child::tbody//td[contains(@data-title,'r')][text()='"+date+"']"),"Subscription Date");
			pass("Subscription Date is Clicked");
		}
		else
			fail("Subscription Date Element is Not Displayed");
	}
	
	
	
	public void selectRightDate(String date) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, "Subscription Date")) {
			pass("Subscription Date Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription date");
			driver.clickElement(By.xpath(subscriptionPage.subRightDateSelect.toString().replace("By.xpath: ", "").concat("'" +date+ "']")),"Subscription Date");
			pass("Subscription Date is Clicked");
		}
		else
			fail("Subscription Date Element is Not Displayed");
	}
	


}
	



