package com.skava.reusable.components;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.InstanceTeamPage;

public class InstanceTeamComponents extends  HomeComponents
{
	public void doLoginWithValidCrentials()
	{
		if(driver.enterText(InstanceTeamPage.emailInput, ExcelReader.getData("LoginCredentials", "EmailAddress"), "EmailAddress field"))
			logPass("Email Address is entered");
		else
			logFail("Failed to enter Emailaddress");
		if(driver.enterText(InstanceTeamPage.pwdInput, ExcelReader.getData("LoginCredentials", "Password"), "Password field"))
			logPass("Password is entered");
		else
			logFail("Failed to enter Password");
		if(driver.jsClickElement(InstanceTeamPage.signInBtn, "SignIn Button"))
		{	
			
			logPass("SignIn button is clicked");
		}
		else
			logFail("SignIn button is not displayed");
	}
	
	public void clickInstanceInMenu()
	{
		if(driver.isElementDisplayed(InstanceTeamPage.instanceTeamMenu, "Instance Team Menu")) 
		{
			driver.jsClickElement(InstanceTeamPage.instanceTeamMenu, "Instance Team Menu");
			logPass("Instance Team Menu is clicked");
			
		}
		else
			logFail("Instance Team Menu is not displayed");
	}
	
	public void verifyInstanceTeamPage()
	{
		waitForLoaderToBeInvisible();
		driver.explicitWaitforVisibility(InstanceTeamPage.instanceTeamTitle,  "Instance Team Title");
		if(driver.isElementDisplayed(InstanceTeamPage.instanceTeamTitle, "Instance Team Title"))
			logPass("Instance team page is displayed");
		else
			logFail("Instance team page is not displayed");
	}
	
	public void userDetailDisplayed()
	{
		driver.waitForPageLoad(60);
		if(driver.isElementDisplayed(InstanceTeamPage.firstNameDet, "First Name"))
			logPass("First Name is displayed");
		else
			logFail("First Name is not displayed");
		
		if(driver.isElementDisplayed(InstanceTeamPage.lastNameDet, "Last Name"))
			logPass("Last Name is displayed");
		else
			logFail("Last Name is not displayed");
		
		if(driver.isElementDisplayed(InstanceTeamPage.emailDet, "Email Details"))
			logPass("Email is displayed");
		else
			logFail("Email is not displayed");
		
		if(driver.isElementDisplayed(InstanceTeamPage.businessDet, "Business Details"))
			logPass("Business is displayed");
		else
			logFail("Business is not displayed");
	}
	
	public void tableColumnDisplayed()
	{
		if(driver.isElementDisplayed(InstanceTeamPage.firstNameCol, "First Name Column"))
			logPass("First Name Column is displayed");
		else
			logFail("First Name Column is not displayed");
		
		if(driver.isElementDisplayed(InstanceTeamPage.lastNameCol, "Last Name column"))
			logPass("Last Name Column is displayed");
		else
			logFail("Last Name Column is not displayed");
		
		if(driver.isElementDisplayed(InstanceTeamPage.emailCol, "Email Column"))
			logPass("Email Column is displayed");
		else
			logFail("Email Column is not displayed");
		
		if(driver.isElementDisplayed(InstanceTeamPage.businessCol, "Business Column"))
			logPass("Business Column is displayed");
		else
			logFail("Business Column is not displayed");
		
		if(driver.isElementDisplayed(InstanceTeamPage.statusCol, "Status Column"))
			logPass("Status Column is displayed");
		else
			logFail("Status Column is not displayed");
	}
	
	public void inviteBtnDisplayed()
	{
		if(driver.isElementDisplayed(InstanceTeamPage.inviteBtn, "Invite Team Members"))
			logPass("Invite Button is displayed");
		else
			logFail("Invite Button is not displayed");
		
	}
	
	public void searchWithFnameFilter()
	{
		if(driver.explicitWaitforClickable(InstanceTeamPage.fnameFilter, "First Name Filter"))
		{
			driver.jsClickElement(InstanceTeamPage.fnameFilter, "First Name Filter");
			if(driver.isElementDisplayed(InstanceTeamPage.fnameSearchInput, "Fname Search Input"))
			{
				logPass("Fname Search Input is displayed");
				String searchFnameInput = ExcelReader.getData("Keywords", "Search FirstName");
				if(driver.enterText(InstanceTeamPage.fnameSearchInput,  searchFnameInput, "Fname Search Input"))
				{
					logPass("Fname Search Input is entered");
					driver.isElementDisplayed(InstanceTeamPage.searchBtn, "Search Button");
					if(driver.clickElement(InstanceTeamPage.searchBtn, "Search Button"))
					{
						driver.waitForPageLoad(60);
						waitForLoaderToBeInvisible();
						if(driver.explicitWaitforVisibility(InstanceTeamPage.firstNameDet, "Fname"))
						{
							logPass("FirstName Search Result is visible");
							String fnameRes = driver.getText(InstanceTeamPage.firstNameDet, "Fname Result");
							if(fnameRes.contains(searchFnameInput) || fnameRes.equalsIgnoreCase(searchFnameInput))
								logPass("Search successfully with FirstName "+fnameRes);
							else
								logFail("Search Failed for first name");
						}
						else
							logFail("FirstName Search Result is not visible");
					}
				}
			}
			else
				logFail("Fname Search Input is not displayed");
		}
		else
			logFail("First Name Filter is not clickable");
	}
	
	public void searchWithLnameFilter()
	{
		if(driver.explicitWaitforVisibility(InstanceTeamPage.lnameFilter, "Last Name Filter"))
		{
			driver.jsClickElement(InstanceTeamPage.lnameFilter, "Last Name Filter");
			if(driver.isElementDisplayed(InstanceTeamPage.lnameSearchInput, "Lname Search Input"))
			{
				logPass("Lname Search Input is displayed");
				String searchLnameInput = ExcelReader.getData("Keywords", "Search LastName");
				if(driver.enterText(InstanceTeamPage.lnameSearchInput,  searchLnameInput, "Lname Search Input"))
				{
					logPass("Lname Search Input is entered");
					if(driver.jsClickElement(InstanceTeamPage.searchBtn, "Search Button"))
					{
						driver.waitForPageLoad(60);
						waitForLoaderToBeInvisible();
						if(driver.explicitWaitforVisibility(InstanceTeamPage.lastNameDet, "Lname"))
						{
							logPass("LastName Search Result is visible");
							String lnameRes = driver.getText(InstanceTeamPage.lastNameDet, "Lname Result").toLowerCase();
							if(lnameRes.equalsIgnoreCase(searchLnameInput))
								logPass("Search successfully with LastName "+lnameRes);
							else
								logFail("Search Failed for Last Name");
						}
						else
							logFail("LastName Search Result is not visible");
					}
				}
			}
			else
				logFail("Lname Search Input is not displayed");
		}
		else
			logFail("Last Name Filter is not clickable");
	}
	
	public void searchWithEmailFilter(String searchEmailInput)
	{
		try {
		if(searchEmailInput.equals(""))
	    searchEmailInput = ExcelReader.getData("Keywords", "Search EmailAddress");
		driver.waitForPageLoad(20);
		if(driver.explicitWaitforVisibility(InstanceTeamPage.emailFilter, "Email Filter"))
		{
			driver.jsClickElement(InstanceTeamPage.emailFilter, "Email Filter");
			if(driver.isElementDisplayed(InstanceTeamPage.emailSearchInput, "Email Search Input"))
			{
				logPass("Email Search Input is displayed");
//				String searchEmailInput = ExcelReader.getData("Keywords", "Search EmailAddress");
				
				if(driver.enterText(InstanceTeamPage.emailSearchInput,  searchEmailInput, "Email Search Input"))
				{
					Thread.sleep(4000);
					logPass("Email Search Input is entered");
					if(driver.jsClickElement(InstanceTeamPage.searchBtn, "Search Button"))
					{
						driver.waitForPageLoad(60);
						waitingLoadingGauage();
						Thread.sleep(4000);
						if(driver.explicitWaitforVisibility(InstanceTeamPage.emailDet, "Email Result"))
						{
							logPass("Email Result is visible");
							waitForLoaderToBeInvisible();
							waitingLoadingGauage();
							String emailRes = driver.getText(InstanceTeamPage.emailDet, "Email Result").toLowerCase();
							if(emailRes.equalsIgnoreCase(searchEmailInput))
								logPass("Search successfully with Email "+emailRes);
							else
								logFail("Search Failed for Email");
						}
						else
							logFail("Email Result is not visible");
					}
				}
			}
			else
				logFail("Email Search Input is not displayed");
		}
		else
			logFail("Email Filter is not clickable");
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void paginationDisplayed()
	{
		if(driver.isElementDisplayed(InstanceTeamPage.paginationCont, "Pagination"))
			logPass("Pagination is displayed");
		else
			logFail("Pagination is not displayed");
	}
	
	public void rowOptionDisplayed()
	{
		driver.explicitWaitforVisibility(InstanceTeamPage.rowDropDown,  "Rows Dropdown");
		if(driver.scrollToElement(InstanceTeamPage.rowDropDown,  "Rows Dropdown"))
		{
			logPass("Row Option is scrolled");
			if(driver.isElementDisplayed(InstanceTeamPage.rowDropDown, "Row Dropdown"))
				logPass("Row Option is displayed");
			else
				logFail("Row Option is not displayed");
		}
		else
			logFail("Row Option is not scrolled");
	}
	public void verifyRecordsUpdated() throws InterruptedException
    {
        if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='row-list rows-list']"), "Row Dropdown"))
        {
            if(driver.clickElement(By.xpath("//*[@data-qa='row-list rows-list']//input"),  "  "))
            	logPass("Row Value is clicked");
            else
            	logFail("Row Value is not clicked");//*[contains(text(),' Row')]
            int size = driver.getSize(By.xpath("//*[@data-qa='row-list rows-list']//ul//li"), "Row list size");
            logPass("Row List count Size "+size);
            
            driver.explicitWaitforVisibility(By.xpath("(//*[@data-qa='row-list rows-list']//ul//li)["+1+"]"),  "Row");
            if(driver.clickElement(By.xpath("(//*[@data-qa='row-list rows-list']//ul//li)["+1+"]"),  "  "))
            	logPass("Row Value is selected");
            else
            	logFail("unable to select row value");
            Thread.sleep(3000);
            waitForLoaderToBeInvisible();
            String recordSize = Integer.toString(driver.getSize(InstanceTeamPage.userRecordList, "User Record Row"));
            if( recordSize == ExcelReader.getData("Keywords", "RecordSize") || recordSize.equalsIgnoreCase("10") )
			{
				logPass("Size is "+ExcelReader.getData("Keywords", "RecordSize"));
			}
			else
				logFail("size is different");
        }
    }
		
	public void filterDisplayed()
	{
		if(driver.isElementDisplayed(InstanceTeamPage.fnameFilter, "Fname Filter"))
			logPass("Fname Filter is displayed");
		else
			logFail("Fname Filter is not displayed");
		
		if(driver.isElementDisplayed(InstanceTeamPage.lnameFilter, "Lname Filter"))
			logPass("Lname Filter is displayed");
		else
			logFail("Lname Filter is not displayed");
		
		if(driver.isElementDisplayed(InstanceTeamPage.emailFilter, "Email Filter"))
			logPass("Email Filter is displayed");
		else
			logFail("Email Filter is not displayed");
	}
	
	public void clickEditUser()
	{
		waitForLoaderToBeInvisible();
		driver.clickElement(InstanceTeamPage.editUser, "Edit user");
	}
	
	public void verifyUserStatusActive()
	{
		waitForLoaderToBeInvisible();
		if(driver.getText(InstanceTeamPage.userActiveStatus, "User status").equals("Active"))
			logPass("User is in active state");
		else
			logFail("User is in Inactive state");
	}
	
	public void verifyUserStatusInactive()
	{
		waitForLoaderToBeInvisible();
		if(driver.getText(InstanceTeamPage.userInactiveStatus, "User status").equals("Inactive"))
			logPass("User is in Inactive state");
		else
			logFail("User is in Active state");
	}
	
	public void verifyInstanceAdminRole()
	{
//        waitForLoaderToBeInvisible();
//		if(driver.getText(InstanceTeamPage.userRole, "User role").equals("ROLE_SUPER_ADMIN"))
//			logPass("User is instance admin");
//		else
//			logFail("User is not an instance admin");
	}
	
	public void verifyBusinessAdminRole()
	{
//		waitForLoaderToBeInvisible();
//		if(driver.getText(InstanceTeamPage.userRole, "User role").equals("ROLE_BUSINESS_ADMIN"))
//			logPass("User is business admin");
//		else
//			logFail("User is not an business admin");
	}
	
	public void verifyBusinessDetailsForNoRoleUser()
	{
		if(driver.getText(InstanceTeamPage.businessDet, "business details").equals("No Business Details"))
			logPass("No business details is displayed for user without any roles");
		else
			logFail("Failed to display 'No business details'.");
	}
	
	public void verifyBusinessDetailsForSuperAdmin()
	{
		if(driver.getText(InstanceTeamPage.businessDet, "business details").equals("SUPER_ADMIN"))
			logPass("Role name is displayed for Super admin");
		else
			logFail("Role name is not displayed for Super admin");
	}
	
	public void verifyBusinessDetailsForBusinessAdmin()
	{
		if(!driver.getText(InstanceTeamPage.businessDet, "business details").equals("SUPER_ADMIN"))
		{
			if(!driver.getText(InstanceTeamPage.businessDet, "business details").equals("No Business Details"))
			{
				logPass("Business details are displayed for the user");
			}
		}
		else
			logFail("Failed to display business details");
		
	}
	
	public void verifyViewMoreLink()
	{
		if(driver.isElementDisplayed(InstanceTeamPage.viewMoreLink,  "View more"))
		{
			logPass("View more link is displayed for business admin with more than 3 business");
			if(driver.clickElement(InstanceTeamPage.viewMoreLink, "View more"))
				logPass("Able to view more business");
			else
				logFail("Failed to click view more");
		}
		else
			logFail("Failed to display 'View more' link");
	}
	
	public void clickMembersReset()
	{
		if(driver.isElementDisplayed(InstanceTeamPage.membersResetBtn,  "membersResetBtn"))
		{
			logPass("View more link is displayed for business admin with more than 3 business");
			if(driver.clickElement(InstanceTeamPage.membersResetBtn, "membersResetBtn"))
				logPass("Members reset button clicked");
			else
				logFail("Members reset button not clicked");
		}
		else
			logFail("Failed to display Members reset button");
	}
	
	public void waitingLoadingGauage()
	{
		if(!driver.explicitWaitforInVisibility(InstanceTeamPage.loadingGauage,  "loadingGauage"))
		{
			logInfo("Loading gauage is displayed");
		}
		else
			logInfo("Loading gauage is not displayed");
			
	}
	
	
	
}

