package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.projectWorkflowObject;

public class ProjectWorkFlowComponents extends MicroServiceComponents 
{
	
	String projNameSearchInp = "";
	
	public void readProjNameVal()
	{
		projNameSearchInp = driver.getText(projectWorkflowObject.projListNameVal, "projListName");
	}
	
	public void clickProjectHeader()
	{
		if(driver.jsClickElement(projectWorkflowObject.projectDropdown, "projectDropdown"))
			pass("projectDropdown Btn is clicked");
		else
			fail("projectDropdown is not clicked");
	}
	
	public void projModelDisplayed()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projModelCont, "ProjectModel Container"))
			pass("ProjectModel Container is displayed");
		else
			fail("ProjectModel Container is not displayed");
	}
	
	public void viewAllProjBtn()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.viewAllProjectsButton, "ViewAll Project Btn"))
			pass("ViewAll Project Btn is displayed");
		else
			fail("ViewAll Project Btn is not displayed");
	}
	
	public void clickViewAllProjBtn()
	{
		if(driver.jsClickElement(projectWorkflowObject.viewAllProjectsButton, "ViewAll Project Btn"))
			pass("ViewAll Project Btn is clicked");
		else
			fail("ViewAll Project Btn is not clicked");
	}

	public void viewAllProjHeaderDisplayed()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.viewAllProjHeader, "viewAllProjHeader"))
			pass("viewAllProjHeader is displayed");
		else
			fail("viewAllProjHeader is not displayed");
	}
	
	public void createNewProjBtnDisplayed()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.CreateNewProjectButton, "CreateNew Project Btn"))
			pass("CreateNew Project Btn is displayed");
		else
			fail("CreateNew Project Btn is not displayed");
	}
	
	public void clickCreateNewProjBtn()
	{
		if(driver.jsClickElement(projectWorkflowObject.CreateNewProjectButton, "CreateNew Project Btn"))
			pass("CreateNew Project Btn is clicked");
		else
			fail("CreateNew Project Btn is not clicked");
	}
	
	public void SearchBtnDisplayed()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.searchButton, "Search Btn in Project"))
			pass("Search Btn in Project is displayed");
		else
			fail("Search Btn in Project is not displayed");
	}
	
	public void clickSearchBtn()
	{
		if(driver.jsClickElement(projectWorkflowObject.searchButton, "Search Button"))
			pass("Search Button is clicked");
		else
			fail("Search Button is not clicked");
	}
	
	public void projNameFilterDisplayed()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projNameFilter, "projNameFilter"))
			pass("projNameFilter is displayed");
		else
			fail("projNameFilter is not displayed");
	}
	
	public void stateFilterDisplayed()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.stateFilter, "stateFilter"))
			pass("stateFilter is displayed");
		else
			fail("stateFilter is not displayed");
	}
	
	public void projNameColDisplayed()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projNameCol, "projNameCol"))
			pass("projNameCol is displayed");
		else
			fail("projNameCol is not displayed");
	}
	
	public void stateColDisplayed()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.stateCol, "stateCol"))
			pass("stateCol is displayed");
		else
			fail("stateCol is not displayed");
	}
	
	public void clickProjNameFilter()
	{
		if(driver.jsClickElement(projectWorkflowObject.projNameFilter, "ProjName Filter"))
			pass("ProjName Filter is clicked");
		else
			fail("ProjName Filter is not clicked");
	}
	
	public void clickStateFilter()
	{
		if(driver.jsClickElement(projectWorkflowObject.stateFilter, "State Filter"))
			pass("State Filter is clicked");
		else
			fail("State Filter is not clicked");
	}
	
	public void projNameInpDisplayed()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projNameInp, "projNameInp"))
			pass("projNameInp is displayed");
		else
			fail("projNameInp is not displayed");
	}
	
	public void enterProjNameSearchInp()
	{
		if(driver.enterText(projectWorkflowObject.projNameInp, projNameSearchInp, "ProjectNameInp"))
			pass("Project Name Search Input is entered");
		else
			fail("Project Name Search Input is not entered");
	}
	
	public void enterInvalidProjNameSearchInp()
	{
		if(driver.enterText(projectWorkflowObject.projNameInp, ExcelReader.getData("Keywords", "ProjectName"), "ProjectNameInp"))
			pass("Project Name Search Input is entered");
		else
			fail("Project Name Search Input is not entered");
	}
	
	public void verifyProjNameSearchResult()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projListNameVal, "projListName"))
		{
			String res = driver.getText(projectWorkflowObject.projListNameVal, "projListName");
			if(res.contains(projNameSearchInp))
				pass("ProjName Searched Successfully");
			else
				fail("ProjName Search Failed");
		}
		else
			fail("ProjName Table value is not displayed");
	}
	
	public void verifyOpenStateSearchResult()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projStateVal, "projStateVal"))
		{
			String res = driver.getText(projectWorkflowObject.projStateVal, "projStateVal");
			if(res.contains("open"))
				pass("Open Searched Successfully");
			else
				fail("Open Search Failed");
		}
		else
			fail("Open State Table value is not displayed");
	}
	
	public void stateFilterDropDownDisplayed()
	{
		if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='project-status-filter']//ul//li"), "projNameInp"))
			pass("projNameInp is displayed");
		else
			fail("projNameInp is not displayed");
	}
	
	public void verifyStateFilterOptns()
	{
		int size = driver.getSize(By.xpath("//*[@data-qa='state-list']//ul//li"), "State Options");
		for(int i=1;i<=size;i++)
		{
			String val = driver.getText(By.xpath("(//*[@data-qa='state-list']//ul//li)["+i+"]"), "StateOptions");
			if(val.equalsIgnoreCase("open") || val.equalsIgnoreCase("submitted") || val.equalsIgnoreCase("approved") || val.equalsIgnoreCase("denied") || val.equalsIgnoreCase("reopen"))
			{
				pass("State Option displayed as"+val);
				if(i==size)
					break;
			}
			else if(i==size)
				break;
			else
				continue;
				
		}
	}
	
	public void selectOpenStateFilter()
	{
		int size = driver.getSize(By.xpath("//*[@data-qa='state-list']//ul//li"), "State Options");
		for(int i=1;i<=size;i++)
		{
			String val = driver.getText(By.xpath("(//*[@data-qa='state-list']//ul//li)["+i+"]"), "StateOptions");
			if(val.equalsIgnoreCase("open"))
			{
				driver.jsClickElement(By.xpath("(//*[@data-qa='state-list']//ul//li)["+i+"]"), "Open State");
				break;
			}
			else if(i==size)
				fail("Failed to click open state");
			else
				continue;
		}
	}
	
//	public void selectSubmittedStateFilter()
//	{
//		int size = driver.getSize(By.xpath("//*[@data-qa='state-list']//ul//li"), "State Options");
//		for(int i=1;i<=size;i++)
//		{
//			String val = driver.getText(By.xpath("(//*[@data-qa='state-list']//ul//li)["+i+"]"), "StateOptions");
//			if(val.equalsIgnoreCase("submitted"))
//			{
//				driver.jsClickElement(By.xpath("(//*[@data-qa='state-list']//ul//li)["+i+"]"), "Submitted State");
//				break;
//			}
//			else if(i==size)
//				fail("Failed to click submitted state");
//			else
//				continue;
//		}
//	}
	
	public void verifyNewProjNameInp()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.newProjNameInp, "newProjNameInp"))
			pass("newProjNameInp is displayed");
		else
			fail("newProjNameInp is not displayed");
	}
	
	public void verifyCancelProjBtn()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.cancelProjBtn, "cancelProjBtn"))
			pass("cancelProjBtn is displayed");
		else
			fail("cancelProjBtn is not displayed");
	}
	
	public void verifyCreateProjBtn()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.createProjBtn, "createProjBtn"))
			pass("createProjBtn is displayed");
		else
			fail("createProjBtn is not displayed");
	}
	
	public void enterNewProjName()
	{
		driver.explicitWaitforClickable(projectWorkflowObject.newProjNameInp, "NewprojectNameField");
		
		if(driver.enterText(By.xpath("//*[@data-qa='newprojectname-value']//input"), ExcelReader.getData("Keywords", "ProjectName"), "New ProjectName"))
			pass("New ProjectName is entered");
		else
			fail("New ProjectName is not ");
	}
	
	//*[@data-qa='createproject-trigger']
	/*{
		driver.explicitWaitforClickable(projectWorkflowObject.newProjNameInp, 60, "NewprojectNameField");
		if(driver.enterText(projectWorkflowObject.newProjNameInp, ExcelReader.getData("Keywords", "ProjectName"), "New ProjectName"))
			pass("New ProjectName is entered");
		else
			fail("New ProjectName is not ");
	}*/
	
	public void clickCreateProjBtn()
	{
		if(driver.jsClickElement(projectWorkflowObject.createProjBtn, "createProjBtn"))
			pass("createProjBtn is clicked");
		else
			fail("createProjBtn is not clicked");
	}
	
	public void clickCancelProjBtn()
	{
		if(driver.jsClickElement(projectWorkflowObject.cancelProjBtn, "cancelProjBtn"))
			pass("cancelProjBtn is clicked");
		else
			fail("cancelProjBtn is not clicked");
	}
	
	public void verifySuccessMsg()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.successMsg, "successMsg"))
			pass("successMsg is displayed");
		else
			fail("successMsg is not displayed");
	}
	
	public void verifyProjDetBtn()
	{
		driver.mouseHoverElement(By.xpath("//*[@data-qa='projectname-and-state-listlabel']"), "Project List");
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projDetBtn, "projDetBtn"))
			pass("projDetBtn is displayed");
		else
			fail("projDetBtn is not displayed");
	}
	
	public void verifyExitBtnDisplayed()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projExitBtn, "projExitBtn"))
			pass("projExitBtn is visible");
		else
			fail("projExitBtn is not visible");
	}
	
	public void clickExiProjBtn()
	{
		if(driver.jsClickElement(projectWorkflowObject.projExitBtn, "ProjExit Button"))
			pass("ProjExit Btn is clicked");
		else
			fail("ProjExit Btn is not clicked");
	}
	
	public void verifyExitBtnNotDisplayed()
	{
		if(driver.explicitWaitforInVisibility(projectWorkflowObject.projExitBtn, "projExitBtn"))
			pass("projExitBtn is Invisible");
		else
			fail("projExitBtn is not Invisible");
	}
	
	public void verifyProjEnterBtn()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projEnterBtn, "projEnterBtn"))
			pass("projEnterBtn is displayed");
		else
			fail("projEnterBtn is not displayed");
	}
	
	public void verifyProjEnterBtnNotDisplayed()
	{
		if(driver.explicitWaitforInVisibility(projectWorkflowObject.projEnterBtn, "projEnterBtn"))
			pass("projEnterBtn is Invisible");
		else
			fail("projEnterBtn is not Invisible");
	}
	
	public void clickProjEnterBtn()
	{
		if(driver.jsClickElement(projectWorkflowObject.projEnterBtn, "ProjEnterBtn"))
			pass("ProjEnter btn is clicked");
		else
			fail("ProjEnter btn is not clicked");
	}
	
	public void verifyProjectLists()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projListNameVal, "projListName"))
			pass("Projects Lists are displayed");
		else
			fail("Projects Lists are not displayed");
	}
	
	public void isProjCreated()
	{
		int size = driver.getSize(projectWorkflowObject.projListNameVal, "ProjectList Name");
		for(int i=1;i<=size;i++)
		{
			String name = driver.getText(By.xpath("(//*[@data-qa='projectlist-name-label'])["+i+"]"), "ProjectList Name");
			if(name!=null)
			{
				pass("Project has been created" +name);
			}
			else
				fail("Project not listed");
			
//			if(name.contains(ExcelReader.getData("Keywords", "ProjectName")))
//			{
//				pass("Project Created Successfully");
//				break;
//			}
//			else if(i==size)
//			{
//				fail("Projects are not created");	
//			}
//			else
//				continue;
		}
	}
	
	public void isProjCanceled()
	{
		int size = driver.getSize(projectWorkflowObject.projListNameVal, "ProjectList Name");
		for(int i=1;i<=size;i++)
		{
			String name = driver.getText(By.xpath("(//*[@data-qa='projectlist-name-label'])["+i+"]"), "ProjectList Name");
			if(!name.equalsIgnoreCase(ExcelReader.getData("Keywords", "ProjectName")))
			{
				pass("Project Canceled Successfully");
				break;
			}
			else if(i==size)
				fail("Projects are not Canceled");		
			else
				continue;
		}
	}
	
	public void hoverOnOpenProjects()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projStateVal, "ProjectStateVal"))
		{
			int size = driver.getSize(projectWorkflowObject.projStateVal, "projStateVal");
			for(int i=1;i<=size;i++)
			{
				String val = driver.getText(By.xpath("(//*[@data-qa='projectlist-state-label'])["+i+"]"), "projectStateVal");
				if(val.equalsIgnoreCase("open"))
				{
					driver.mouseHoverElement(By.xpath("(//*[@data-qa='projectlist-state-label'])["+i+"]"), "projectStateVal");
					break;
				}
				else if(i==size)
					fail("There is no open Projects");
				else
					continue;
			}
		}
		else
			fail("projStateVal is not displayed");
	}
	
	public void hoverOnSubmittedProjects()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projStateVal, "ProjectStateVal"))
		{
			int size = driver.getSize(projectWorkflowObject.projStateVal, "projStateVal");
			for(int i=1;i<=size;i++)
			{
				String val = driver.getText(By.xpath("(//*[@data-qa='projectlist-state-label'])["+i+"]"), "projectStateVal");
				if(val.equalsIgnoreCase("submitted"))
				{
					driver.mouseHoverElement(By.xpath("(//*[@data-qa='projectlist-state-label'])["+i+"]"), "projectStateVal");
					break;
				}
				else if(i==size)
					pass("There is no submitted projects");
				else
					continue;
			}
		}
		else
			fail("projStateVal is not displayed");
	}
	
	public void hoverOnApprovedProjects()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projStateVal, "ProjectStateVal"))
		{
			int size = driver.getSize(projectWorkflowObject.projStateVal, "projStateVal");
			for(int i=1;i<=size;i++)
			{
				String val = driver.getText(By.xpath("(//*[@data-qa='projectlist-state-label'])["+i+"]"), "projectStateVal");
				if(val.equalsIgnoreCase("approved"))
				{
					driver.mouseHoverElement(By.xpath("(//*[@data-qa='projectlist-state-label'])["+i+"]"), "projectStateVal");
					break;
				}
				else if(i==size)
					pass("There is no approved Projects");
				else
					continue;
			}
		}
		else
			fail("projStateVal is not displayed");
	}
	
	public void hoverOnDeniedProjects()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.projStateVal, "ProjectStateVal"))
		{
			int size = driver.getSize(projectWorkflowObject.projStateVal, "projStateVal");
			for(int i=1;i<=size;i++)
			{
				String val = driver.getText(By.xpath("(//*[@data-qa='projectlist-state-label'])["+i+"]"), "projectStateVal");
				if(val.equalsIgnoreCase("denied"))
				{
					driver.mouseHoverElement(By.xpath("(//*[@data-qa='projectlist-state-label'])["+i+"]"), "projectStateVal");
					break;
				}
				else if(i==size)
					pass("There is no denied Projects");
				else
					continue;
			}
		}
		else
			fail("projStateVal is not displayed");
	}
	
	public void verifyNoSearchAlert()
	{
		if(driver.explicitWaitforVisibility(projectWorkflowObject.noDataErr, "No Data Found Msg"))
			pass("No Data Found Msg is displayed");
		else
			fail("No Data Found Msg is not displayed");
	}
	
	
	

	
	
	
	
	
}
