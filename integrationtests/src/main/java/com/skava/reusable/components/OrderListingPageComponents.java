package com.skava.reusable.components;


import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.skava.frameworkutils.ExcelReader;
import com.skava.networkutils.ApiReaderUtils;
import com.skava.object.repository.orderListingPage;

public class OrderListingPageComponents extends MerchandiseadminComponents
{
	String accIdSearchInp = "", orderIdSearchInp = "", custNameSearchInp[];
	public void verifyOrderListPage()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.manageOrderTitle, "Order Title"))
			logPass("Order Page is displayed");
		else
			logFail("Order Page is not displayed");
	}
	
	public void orderIdColDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.orderIdCol, "Order Id Col"))
			logPass("Order Id Col is displayed");
		else
			logFail("Order Id Col is not displayed");
	}
	
	public void verifyListOfOrders()
	{
		int size = driver.getSize(orderListingPage.orderListRow, "orderListRow");
		if(size>=1)
			logPass("orderLists are displayed");
		else
			logFail("orderLists are not displayed");
	}
	
	/** @throws InterruptedException 
	 * @ author Dilip**/
	public void verifyListOfOrdersInDataTable() throws InterruptedException
	{
		driver.isElementDisplayed(By.xpath("//*[@data-qa='order-id-item']"),"Order Listing");
		if(driver.explicitWaitforVisibility(orderListingPage.orderListCount, "Order List"))
		{
			int size = driver.getSize(orderListingPage.orderListCount, "orderListRow");
			if(size!=0) {
				logPass("Orders displayed in the table is : " +size);
			}
			else
				logFail("No Orders found");
		}
		else
			logFail("Order List is not displayed");
	}
	
	public String orderIDFromTable()
	{
		int size = driver.getSize(orderListingPage.orderListCount, "orderListRow");
		String order_ID= null;
		for(int i =1; i<=size; i++)
		{
		order_ID=driver.getText(By.xpath("//*[@data-qa='order-id-value']["+i+"]"), "Order ID value");
		break;
		}
		return order_ID;
	}
	
	public void triggerOrderIdFilter()
   {
		String value = null;
		orderIdFilterDisplayed();
		String Orders= orderIDFromTable();
		driver.jsClickElement(orderListingPage.orderIdFilter, "Order ID Filter Trigger");
		driver.enterText(orderListingPage.orderIdSearchInp, Orders, "Enter Order id in search field");
	    driver.clickElement(orderListingPage.searchBtn, "Trigger search button");
	    if(driver.isElementDisplayed(orderListingPage.orderListCount, "Order List"))
	    {
	    	 value = driver.getText(orderListingPage.orderListCount, "Order Id value");
	    	 logPass("Order Id value is displayed");
	    }
	    if(Orders.equalsIgnoreCase(value))
	    {
	    	logPass("Order Id Search results are displayeds");
	    }
	    else
	    	logFail("Order Id Search not displayed");
   }

   public void triggerResetButton()
   {
	   resetBtnDisplayed();
	   driver.jsClickElement(orderListingPage.resetBtn, "Trigger reset button in Order data table");
	   driver.isElementDisplayed(orderListingPage.orderListCount, "Order Data table get reset");
   }
   
  public void searchWithFirstName()
  {
	  if(driver.isElementDisplayed(orderListingPage.firstName, "First Name filter"))
	  {
		  driver.jsClickElement(orderListingPage.triggerFirstName, "Click First Name filter");
		  logPass("First Name filter is clicked");
		  driver.enterText(orderListingPage.filterFirstNameInput, ExcelReader.getData("Orderadmin", "firstName"), "First Name filter");
		  driver.jsClickElement(orderListingPage.searchBtn, "Trigger Search button");
		//ExcelReader.getData(sheetName, colName)
	  }
	  else
		  logFail("FirstName filter is not displayed");
  }
  
  public void searchResultForFirstName()
  {
	  String customerName = null;
	  if(driver.isElementDisplayed(orderListingPage.customerNameTableVal, "Customer Name column"))
	  {
		  int custVal = driver.getSize(orderListingPage.customerNameTableVal, "no of customer First names");
		  for(int i = 1; i <=custVal; i++)
		  {
			   customerName = driver.getText(By.xpath("//*[@data-qa='customer-name-value']["+i+"]"), "First Name");
			  break;
		  }
		  if(customerName.contains(ExcelReader.getData("Orderadmin", "firstName")))
		  {
			  logPass("Valid First name search results get displayed");
		  }
		  else
			  logFail("logFailed to display search results");
	  }
  }
   
  public void searchWithEmail()
  {
	  if(driver.isElementDisplayed(orderListingPage.emailNameFilter, "Email filter"))
	  {
		  driver.jsClickElement(orderListingPage.emailNameFilter, "Click Email filter");
		  logPass("Email filter is clicked");
		  driver.enterText(orderListingPage.emailNameInput, ExcelReader.getData("Orderadmin", "eMail"), "Email filter");
		  driver.jsClickElement(orderListingPage.searchBtn, "Trigger Search button");
		}
	  else
		  logFail("Email filter is not displayed");
  }
  
  public void searchResultForEmail()
  {
	if(driver.isElementDisplayed(orderListingPage.emailNameFilter, "Email search"))
	{
	int emailResu = driver.getSize(orderListingPage.orderValRes, "Email Filter results");
	logPass("Total no of Email search result is :" +emailResu);
	}
	else
		logFail("Email search result not found");
  }
  
  
  public void selectstartdate()
  {
  	if(driver.explicitWaitforVisibility(By.xpath("(//*[@class='table-condensed'])[1]"), "daterange"))
  	{
  		int size=driver.getSize(By.xpath("//*[@class='drp-calendar left']//*[@class='available']"), "Start date");
  		int random=driver.generateRandomNumber(size)+1;
  		String text=driver.getText(By.xpath("(//*[@class='drp-calendar left']//*[@class='available'])["+random+"]"), "Start date");
  		if(driver.clickElement(By.xpath("(//*[@class='drp-calendar left']//*[@class='available'])["+random+"]"), "Select"+text))
  			logPass("Selected date "+text);
  		else
  			logFail("date field not clickable");
  		
  	}
  }
  public void selectenddate()
  {
  	if(driver.explicitWaitforVisibility(By.xpath("(//*[@class='table-condensed'])[2]"), "daterange"))
  	{
  		int size=driver.getSize(By.xpath("//*[@class='drp-calendar right']//*[@class='available']"), "End date");
  		int random=driver.generateRandomNumber(size)+1;
  		String text=driver.getText(By.xpath("(//*[@class='drp-calendar right']//*[@class='available'])["+random+"]"), "End date");
  		if(driver.clickElement(By.xpath("(//*[@class='drp-calendar right']//*[@class='available'])["+random+"]"), "Select"+text))
  			logPass("Selected date "+text);
  		else
  			logFail("date field not clickable");
  		
  	}
  }
  
  
//  public void startSelectDate()
//  {
//	 driver.isElementDisplayed(orderListingPage.startDateFilter, "Left Calendar picker");
//	 logPass("Left Date picker is displayed");
//	 driver.jsClickElement(orderListingPage.startDateFilter, "Select Start Date");
//	 logPass("From date is Selected");
//   }
//  
//  public void endSelectDate()
//  {
//	driver.isElementDisplayed(orderListingPage.endDateFilter, "Right Calendar picker");
//	logPass("Right Date picker is displayed");
//	driver.jsClickElement(orderListingPage.endDateFilter, "select End Date");
//	logPass("To date is selected");
//  }
  
  public void triggerApplyButton()
  {
	  driver.isElementDisplayed(orderListingPage.calendarApply, "Apply button");
	  logPass("Apply button is displayed");
      driver.jsClickElement(orderListingPage.calendarApply, "Trigger Apply button");
      logPass("Apply button is clicked");
  }
  
  public void searchResultForDateRange()
  {
	  
  }
  
  /*Invalid Data for Search Filters*/
  
  public void invalidOrderIdSearch()
  {
	  driver.isElementDisplayed(orderListingPage.orderIdFilter, "Order ID filter");
	  driver.jsClickElement(orderListingPage.orderIdFilter, "Order ID Filter Trigger");
	  driver.enterText(orderListingPage.orderIdSearchInp, ExcelReader.getData("Orderadmin", "inValidOrder"), "Enter Invalid Order id in search field");
	  clickSearchBtn();
	  driver.isElementDisplayed(orderListingPage.noDataToDisplay, "No Data Message get displayed");
	  logPass("No data found message get displayed");
  }
  
  public void invalidFirstNameSearch()
  {
	  driver.isElementDisplayed(orderListingPage.triggerFirstName, "First Name filter");
	  driver.jsClickElement(orderListingPage.triggerFirstName, "First Name Trigger");
	  driver.enterText(orderListingPage.filterFirstNameInput, ExcelReader.getData("Orderadmin", "inValidFirstName"), "Enter Invalid FirstNamein search field");
	  clickSearchBtn();
	  driver.isElementDisplayed(orderListingPage.noDataToDisplay, "No Data Message get displayed");
	  logPass("No data found message get displayed");
  }
  
  public void invalidEmailSearch()
  {
	  driver.isElementDisplayed(orderListingPage.emailNameFilter, "Email filter");
	  driver.jsClickElement(orderListingPage.emailNameFilter, "Email Trigger");
	  driver.enterText(orderListingPage.emailNameInput, ExcelReader.getData("Orderadmin", "inValidEmail"), "Enter Invalid Email search field");
	  clickSearchBtn();
	  driver.isElementDisplayed(orderListingPage.noDataToDisplay, "No Data Message get displayed");
	  logPass("No data found message get displayed");
  }
  
  public void triggerViewOrder()
  {
	  if(driver.explicitWaitforVisibility(orderListingPage.firstRowSelect, "View Detail Order"))
	  {
		  	logPass("View Button is displayed");
	  		driver.jsClickElement(orderListingPage.firstRowSelect, "Trigger View Detail Order");
	  }
	  else
			logFail("View Button is not displayed");
  }
  
   /****/
	
	public void orderDateColDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.orderDataCol, "Order Date Col"))
			logPass("Order Date Col is displayed");
		else
			logFail("Order Date Col is not displayed");
	}
	
	public void custNameColDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.custNameCol, "Customer Name Col"))
			logPass("Customer Name Col is displayed");
		else
			logFail("Customer Name Col is not displayed");
	}
	
	public void accIdColDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.accIdCol, "Account Id Col"))
			logPass("Account Id Col is displayed");
		else
			logFail("Account Id Col is not displayed");
	}
	
	public void orderValColDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.orderVal, "Order Val Col"))
			logPass("Order Val Col is displayed");
		else
			logFail("Order Val Col is not displayed");
	}
	
	public void ordersBreadCrum()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.ordersBreadCrum, "Order BreadCrum"))
			logPass("Order BreadCrum is displayed");
		else
			logFail("Order BreadCrum is not displayed");
	}
	
	public void clickOrderDataCol()
	{
		driver.explicitWaitforVisibility(orderListingPage.orderDataCol,"");
		if(driver.jsClickElement(orderListingPage.orderDataCol, "OrderDate Col"))
			logPass("OrderDate Col is displayed");
		else
			logFail("OrderDate Col is not displayed");
	}
	
	public void verifyOrderDateSortable()
	{
		if(driver.getElementAttribute(orderListingPage.orderDataCol, "class", "OrderDate Col").indexOf("sorting") != -1)
			logPass("Order Date is Sortable");
		else
			logFail("Order Date is not Sortable");
	}
	
	public void scrollToRowDropDown()
	{
		driver.explicitWaitforVisibility(orderListingPage.rowsDropDown,"Rows DropDown");
		if(driver.scrollToElement(orderListingPage.rowsDropDown, "Rows DropDown"))
			logPass("Rows Dropdown is scrolled");
		else
			logFail("logFailed to scroll to rows dropdown");
	}
	
	public void rowsDropDownDisplayed()
	{
		if(driver.isElementDisplayed(orderListingPage.rowsDropDown, "Rows DropDown"))
			logPass("Rows DropDown is displayed");
		else
			logFail("Rows DropDown is not displayed");
	}
	
	public void clickRowDropDown()
	{
		driver.scrollToElement(By.xpath("//*[@data-qa='page-length-select']"),"Dropdown");
		if(driver.jsClickElement(By.xpath("//*[@data-qa='page-length-select']//input"),"Rows Dropdown"))
			logPass("Rows DropDown is clicked");
		else
			logFail("logFailed to click Rows DropDown");
	}
	
	public void selectRowVal()
	{
		if(driver.clickElement(By.xpath("//*[@data-qa='page-length-select']//ul//li[2]"), "Row Option"))
			logPass("Row Option is selected");
		else
			logFail("Row Option is not selected");
	}
	
	public void paginationDisplayed()
	{
		if(driver.isElementDisplayed(orderListingPage.paginationCont, "Pagination"))
			logPass("Pagination is displayed");
		else
			logFail("Pagination is not displayed");
	}
	
	public void mouseHoverOrderListsIngrid()
	{
		if(driver.mouseHoverElement(orderListingPage.orderIdCol, "OrderId"))
			logPass("Order is hovered");
		else
			logFail("Order is not hovered");
	}
	
	public void viewOrderDetailPageNavigation()
	{
		driver.explicitWaitforVisibility(By.xpath("//*[@class='cls_skColumOrderId']"),"Orders");
//		int size = driver.getSize(By.xpath("//*[@class='cls_skColumOrderId']"),"Orders");
//		int random = driver.generateRandomNumber(size)+1;
		driver.mouseHoverElement(By.xpath("(//*[@class='cls_skColumOrderId'])["+1+"]/parent::td/parent::tr"),"Hovering order");
		if(driver.isElementDisplayed(By.xpath("(//*[@id='id_skViewOrderBtn'])["+1+"]"),"View button"))
		{
			logPass("View button displayed");
			driver.jsClickElement(By.xpath("(//*[@id='id_skViewOrderBtn'])["+1+"]"),"View button");
		}
		else
			logFail("View button not displayed");
	}
	
	public void viewDetailDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.viewBtn, "View Button"))
			logPass("View Button is displayed");
		else
			logFail("View Button is not displayed");
	}
	
	public void verifyOrderListUpdated()
	{
//		driver.explicitWaitforInVisibility(By.xpath("(//*[@id='id_loader'])/div/div//*[@class='circle']"),90,"Loading logo");
		driver.explicitWaitforVisibility(By.xpath("//*[@class='cls_skColumOrderId']"),"Order Row");
		
		if(driver.getSize(By.xpath("//*[@class='cls_skColumOrderId']"), "Order Row") == 25)
			logPass("Row record updated successfully");
		
		else if(driver.getSize(By.xpath("//*[@class='cls_skColumOrderId']"), "Order Row") <= 25)
		   logPass("No of order is less than row selection");
		
		else
			logFail("Row pagination not displayed");
			
		
	}
	
	public void NoOrderDisplayed()
	{
		String url = (properties.getProperty("ApplicationUrl"));
		url=url.replace(properties.getProperty("collectionId"), properties.getProperty("collectionIdNoorder"));
		driver.navigateToUrl(url);
	}
	
	public void verifyNoOrderDisplayed()
	{
		if(driver.isElementDisplayed(orderListingPage.noDataToDisplay, "CurrentPage"))
			logPass("Orders not found for this collection");
		else
			logFail("Orders found");
	}
	
	// Need to verify
	public void currentPagePagination()
	{
		if(driver.isElementDisplayed(By.xpath("//*[@data-qa='pagination-trigger']"), "CurrentPage"))
			logPass("Current Pagination is selected");
		else
			logFail("Current Pagination is not selected");
	}
	
	public void clickViewDetail()
	{
		if(driver.jsClickElement(orderListingPage.viewBtn, "View Button"))
			logPass("View Button is clicked");
		else
			logFail("logFailed to click view button");
	}
	
	public void orderDetailPageDisplayed()
	{
		if(driver.isElementDisplayed(orderListingPage.orderDetTitle, "OrderDetail"))
			logPass("OrderDetail is displayed");
		else
			logFail("OrderDetail is not displayed");
	}
	
	public void orderIdFilterDisplayed()
	{
		if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='order-id-filter']//span"), "orderIdFilter"))
			logPass("orderIdFilter is displayed");
		else
			logFail("orderIdFilter is not displayed");
	}
	
	public void clickOrderIdFilter()
	{
		if(driver.jsClickElement(orderListingPage.orderIdFilter, "orderIdFilter"))
			logPass("orderIdFilter is clicked");
		else
			logFail("logFailed to click orderIdFilter");
	}
	
	public void orderIdSearchInpDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.orderIdSearchInp, "orderIdSearchInp"))
			logPass("orderIdSearchInp is displayed");
		else
			logFail("orderIdSearchInp is not displayed");
	}
	
	public void searchBtnDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.searchBtn, "searchBtn"))
			logPass("searchBtn is displayed");
		else
			logFail("searchBtn is not displayed");
	}
	
	public void resetBtnDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.resetBtn, "resetBtn"))
			logPass("resetBtn is displayed");
		else
			logFail("resetBtn is not displayed");
	}
	
	public void accIdFilterDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.accIdFilter, "accIdFilter"))
			logPass("accIdFilter is displayed");
		else
			logFail("accIdFilter is not displayed");
	}
	
	public void clickAccIdFilter()
	{
		if(driver.jsClickElement(orderListingPage.accIdFilter, "accIdFilter"))
			logPass("accIdFilter is clicked");
		else
			logFail("accIdFilter is not clicked");
	}
	
	public void accIdSearchInpDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.accIdSearchInp, "accIdSearchInp"))
			logPass("accIdSearchInp is displayed");
		else
			logFail("accIdSearchInp is not displayed");
	}
	
	public void navigateToB2BOrderList()
	{
		String url = "http://euat.skavaone.com/admin/orders/?collectionId=1536209502390455&storeId=2&businessId=1";
		driver.navigateToUrl(url);
	}
	
	public void readAccIdVal()
	{
		accIdSearchInp = driver.getText(orderListingPage.accIdTabVal, "accIdTabVal");
	}
	
	public void readOrderIdVal()
	{
		orderIdSearchInp = driver.getText(orderListingPage.orderIdTabVal, "orderIdTabVal");
	}
	
	public void readCustNameVal()
	{
		String text = driver.getText(orderListingPage.customerNameTabVal, "firstNameTabVal");
		custNameSearchInp = text.split(" ");
	}
	
	public void enterAccIdSearchInput()
	{
		if(driver.enterText(orderListingPage.accIdSearchInp, accIdSearchInp, "accIdSearchInp"))
			logPass("accIdSearchInp is entered");
		else
			logFail("accIdSearchInp is not entered");
	}
	
	public void clickSearchBtn()
	{
		if(driver.jsClickElement(orderListingPage.searchBtn, "searchBtn"))
			logPass("searchBtn is clicked");
		else
			logFail("searchBtn is not clicked");
	}
	
	public void verifyAccIdSearchResult()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.accIdTabVal, "AccId Val"))
		{
			String res = driver.getText(orderListingPage.accIdTabVal, "accIdTabVal");
			if(res.contains(accIdSearchInp))
				logPass("AccId Searched Successfully");
			else
				logFail("AccId Search logFailed");
		}
		else
			logFail("Acc Id Table value is not displayed");
	}
	
	public void verifyOrderIdSearchResult()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.orderIdTabVal, "orderIdTabVal"))
		{
			String res = driver.getText(orderListingPage.orderIdTabVal, "orderIdTabVal");
			if(res.contains(orderIdSearchInp))
				logPass("orderIdTabVal Searched Successfully");
			else
				logFail("orderIdTabVal Search logFailed");
		}
		else
			logFail("orderIdTabVal is not displayed");
	}
	
	public void verifyOrderDateSearchResult()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.orderDateTabVal, "orderDateTabVal"))
		{
			String res = driver.getText(orderListingPage.orderDateTabVal, "orderDateTabVal");
			if(res.contains("9"))
				logPass("OrderDate Searched Successfully");
			else
				logFail("OrderDate Searched logFailed");
		}
	}
	
	public void verifyFirstNameResult()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.customerNameTabVal, "customerNameTabVal"))
		{
			String res = driver.getText(orderListingPage.customerNameTabVal, "customerNameTabVal");
			if(res.contains(custNameSearchInp[0]))
				logPass("firstName Searched Successfully");
			else
				logFail("firstName Search logFailed");
		}
		else
			logFail("customerNameTabVal is not displayed");
	}
	
	
	public void verifyLastNameResult()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.customerNameTabVal, "customerNameTabVal"))
		{
			String res = driver.getText(orderListingPage.customerNameTabVal, "customerNameTabVal");
			if(res.contains(custNameSearchInp[1]))
				logPass("lastName Searched Successfully");
			else
				logFail("lastName Search logFailed");
		}
		else
			logFail("customerNameTabVal is not displayed");
	}
	
	public void verifyDateRangeFilterDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.dateRangeFilter, "date range filter"))
			logPass("date range is displayed");
		else
			logFail("date range is not displayed");
	}
	
	public void enterOrderIdSearchInp()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.orderIdSearchInp, "orderId Input"))
		{
			if(driver.enterText(orderListingPage.orderIdSearchInp,orderIdSearchInp , "orderId Input"))
				logPass("order id is entered");
			else
				logFail("order id is not entered");
		}
	}
	
	public void verifyDateRangeFilterClicked()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.dateRangeBtn, "date range filter"))
		{
			if(driver.jsClickElement(orderListingPage.dateRangeBtn,"date range filter"))
				logPass("date range is clicked");
			else
				logFail("date range is not clicked");
		}
	}
	
	public void verifyFirstNameFilterDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.firstNameFilter, "first name filter"))
			logPass("First name filter is displayed");
		else
			logFail("First name filter is not displayed");
	}
	
	public void clickFirstNameFilterDisplayed()
	{
		if(driver.jsClickElement(orderListingPage.firstNameFilter, "first name filter"))
			logPass("First name filter is Clicked");
		else
			logFail("First name filter is not Clicked");
	}
	
	public void verifyFirstNameInput()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.firstNameInput, "first name Input"))
			logPass("First name Input is displayed");
		else
			logFail("First name Input is not displayed");
	}
	
	public void verifyLastNameFilterDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.lastNameFilter, "last name filter"))
			logPass("last name filter is displayed");
		else
			logFail("last name filter is not displayed");
	}
	
	public void clickLastNameFilterDisplayed()
	{
		if(driver.jsClickElement(orderListingPage.lastNameFilter, "last name filter"))
			logPass("Last name filter is Clicked");
		else
			logFail("Last name filter is not Clicked");
	}
	
	public void verifyLastNameInput()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.lastNameInput, "last name Input"))
			logPass("Last name Input is displayed");
		else
			logFail("Last name Input is not displayed");
	}
	
	
	public void verifyEmailNameFilterDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.emailNameFilter, "email filter"))
			logPass("Email filter is displayed");
		else
			logFail("Email filter is not displayed");
	}
	
	public void clickEmailNameFilterDisplayed()
	{
		if(driver.jsClickElement(orderListingPage.emailNameFilter, "Email filter"))
			logPass("Email filter is Clicked");
		else
			logFail("Email filter is not Clicked");
	}
	
	public void verifyEmailNameInput()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.emailNameInput, "Email Input"))
			logPass("Email Input is displayed");
		else
			logFail("Email Input is not displayed");
	}
	
	public void enterFirstNameSearchInp()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.firstNameInput, "First Name Input"))
		{
			if(driver.enterText(orderListingPage.firstNameInput, custNameSearchInp[0], "First Name Input"))
				logPass("First Name is entered");
			else
				logFail("First Name is not entered");
		}
	}
	
	public void enterLastNameSearchInp()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.lastNameInput, "Last Name Input"))
		{
			if(driver.enterText(orderListingPage.lastNameInput, custNameSearchInp[1], "Last Name Input"))
				logPass("Last Name is entered");
			else
				logFail("Last Name is not entered");
		}
	}
	
	public void enterLastEmailSearchInp()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.emailNameInput, "Email Input"))
		{
			if(driver.enterText(orderListingPage.emailNameInput, "dev@skava.com", "Email Input"))
				logPass("Email is entered");
			else
				logFail("Email is not entered");
		}
	}
	
//	public void selectMonth()
//	{
//		driver.selectByValue(By.xpath("(//select[@class='monthselect'])[1]"), "8", "Sep");
//	}
	
	public void selectFromDate()
	{
		int size = driver.getSize(By.xpath("(//table[@class='table-condensed'])[1]//td"), "Date");
		for(int i =1;i<=size;i++)
		{
			String val = driver.getText(By.xpath("((//table[@class='table-condensed'])[1]//td)["+i+"]"), "Date Value");
			if(val.equals("1"))
			{
				driver.clickElement(By.xpath("((//table[@class='table-condensed'])[1]//td)["+i+"]"), "Date 9");
				logPass("Date is selected");
				break;
			}
			else if(i==size)
				logFail("Date is not selected");
			else
				continue;
		}
	}
	
	public void selectToDate()
	{
		int size = driver.getSize(By.xpath("(//table[@class='table-condensed'])[1]//td"), "Date");
		for(int i =1;i<=size;i++)
		{
			String val = driver.getText(By.xpath("((//table[@class='table-condensed'])[2]//td)["+i+"]"), "Date Value");
			if(val.equals("10"))
			{
				driver.clickElement(By.xpath("((//table[@class='table-condensed'])[2]//td)["+i+"]"), "Date 30");
				logPass("Date is selected");
				break;
			}
			else if(i==size)
				logFail("Date is not selected");
			else
				continue;
		}
	}
	
	public void clickApplyDate()
	{
		if(driver.jsClickElement(By.xpath("(//*[contains(@class,'applyBtn')])"), "Apply"))
			logPass("Apply Button is clicked");
		else
			logFail("logFailed to click Apply Button");
	}
	
	public void clickAddMoreFilter()
	{
		if(driver.jsClickElement(orderListingPage.addMoreFilter, "addMoreFilter"))
			logPass("addMoreFilter is clicked");
		else
			logFail("addMoreFilter is not clicked");
	}
	
	public void selectLastNameFilter()
	{
		int size  = driver.getSize(By.xpath("//*[@data-qa='add-more-filter']//ul//li"), "Filter Options");
		for(int i =1; i<=size;i++)
		{
			String val = driver.getElementAttribute(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "class", "Filter Options");
			if(!val.equalsIgnoreCase("active"))
			{
				driver.jsClickElement(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "Filter Options");
				break;
			}
		}
	}
	
	public void verifyRowsVal()
	{
		int size = driver.getSize(By.xpath("//*[@data-qa='rows-dropdown']//ul//li"), "Row options");
		String[] rowVal = {"10","25","50","100"};
		for(int i=1;i<=size;i++)
		{
			String text = driver.getText(By.xpath("(//*[@data-qa='rows-dropdown']//ul//li)["+i+"]"), "Row options");
			if(text.contains(rowVal[i-1]))
			{
				if(i==size)
					logPass("Row Values is displayed in order 10,25,50,100");
				else if(i!=size)
					continue;
			}
			else
				logFail("Row Values is not displayed in order 10,25,50,100");
		}
	}
	
//	public void clickAddMoreFilter()
//	{
//		if(driver.isElementDisplayed(orderListingPage.threeAddMoreFilter, 120, "Add more filter"))
//		{
//			driver.jsClickElement(orderListingPage.threeAddMoreFilter, "trigger filter button");
//			logPass("Filter Button triggerred");
//		}
//		else
//			logFail("logFailed to triggered filter button");
//	}
	
	public void verifyFilterList()
	{
		if(driver.isElementDisplayed(orderListingPage.filterList, "Filter"))
		{
			logPass("Filter got listed");
		}
		else
			logFail("filter not get listed");
	}
	
	public void verifySKUFilter()
	{
		int size ;
		size = driver.getSize(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)"), "Filtrable list class");
		//System.out.println(size);
		for(int i = 1 ; i <= size ; i++)
		{
			
			String sku = driver.getText(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "");
			if(sku!=null && sku.equalsIgnoreCase("sku id"))
		    {
				String val= driver.getElementAttribute(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"),"class","SkuId");
				if(val.contains("active"))
					logPass("Already Sku selected");
				else
				{
					driver.jsClickElement(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "SKU ID clicked");
					logPass("SKU ID is clicked");
				}
		        break;
		    }
		    else if(i==size)
		    	logFail("SKU ID not displayed");
		    else
		    	continue;
		}
	}
	
	public void verifyCityFilter()
	{
		int size ;
		size = driver.getSize(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)"), "Filtrable list class");
		//System.out.println(size);
		for(int i = 1 ; i <= size ; i++)
		{
			
			String val = driver.getText(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "");
			if(val!=null && val.equalsIgnoreCase("city"))
		    {
				String status= driver.getElementAttribute(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"),"class","City");
				if(status.contains("active"))
					logPass("Already City selected");
				else
				{
					driver.jsClickElement(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "City clicked");
					logPass("City is clicked");
				}
		        break;
		    }
		    else if(i==size)
		    	logFail("City not displayed");
		    else
		    	continue;
		}
	}
	
	public void verifyCountryFilter()
	{
		int size ;
		size = driver.getSize(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)"), "Filtrable list class");
		//System.out.println(size);
		for(int i = 1 ; i <= size ; i++)
		{
			
			String sku = driver.getText(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "");
			if(sku!=null && sku.equalsIgnoreCase("country"))
		    {
				String val= driver.getElementAttribute(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"),"class","Country");
				if(val.contains("active"))
					logPass("Already Country selected");
				else
				{
					driver.jsClickElement(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "Country clicked");
					logPass("Country is clicked");
				}
		        break;
		    }
		    else if(i==size)
		    	logFail("Country not displayed");
		    else
		    	continue;
		}
	}
	
	
	
	public void enterInValdCnty()
	{
		if(driver.isElementDisplayed(orderListingPage.enterCountry, "Enter Country"))
		{
			driver.enterText(orderListingPage.enterCountry, "$abcde$123", "Country entries");
			logPass("valid entries");
		}
		else
			logFail("logFailed to enter country");
	}
	
	public void clickSkuIDDrpDwn()
	{
		
		if(driver.isElementDisplayed(orderListingPage.triggSkuId, "click SKU ID drop down"))
		{
			driver.jsClickElement(orderListingPage.triggSkuId, "click SKU ID drop down");
			logPass("sku id drop down displayed");
		}
		
		else
			logFail("logFailed to displayed drop down");
	}
	
	public void clickCityDrpDwn()
	{
		
		if(driver.isElementDisplayed(orderListingPage.triggCity, "click CITY drop down"))
		{
			driver.jsClickElement(orderListingPage.triggCity, "click City drop down");
			logPass("City drop down displayed");
		}
		else
			logFail("logFailed to displayed drop down");
	}
	
	public void clickCountryDrpDwn()
	{
		
		if(driver.isElementDisplayed(orderListingPage.triggCountry, "click Country drop down"))
		{
			driver.jsClickElement(orderListingPage.triggCountry, "click Country drop down");
			logPass("Country drop down displayed");
		}
		else
			logFail("logFailed to displayed drop down");
	}
	
	
	public void clickZipDrpDwn()
	{
		if(driver.isElementDisplayed(orderListingPage.triggZip, "click zip drop down"))
		{
			driver.jsClickElement(orderListingPage.triggZip, "click zip drop down");
			logPass("Zip drop down displayed");
		}
		else
			logFail("logFailed to displayed drop down");
	}
		
	
	
	
	public void enterValdSKu()
	{
		if(driver.isElementDisplayed(orderListingPage.enterSkuId, "Enter Sku id"))
		{
			driver.enterText(orderListingPage.enterSkuId, "string", "Sku Id entries");
			logPass("valid entries");
		}
		
		else
			logFail("logFailed to enter Sku id");
	}
	
	public void enterInValdSKu()
	{
		if(driver.isElementDisplayed(orderListingPage.enterSkuId, "Enter Sku id"))
		{
			driver.enterText(orderListingPage.enterSkuId, "$abcde$123", "Sku Id entries");
			logPass("valid entries");
		}
		
		else
			logFail("logFailed to enter Sku id");
	}
	
	public void enterEmptySKu()
	{
		if(driver.isElementDisplayed(orderListingPage.enterSkuId, "Enter Sku id"))
		{
			driver.enterText(orderListingPage.enterSkuId, "  ", "Sku Id entries");
			logPass("valid entries");
		}
		
		else
			logFail("logFailed to enter Sku id");
	}
	
	public void enterValdCity()
	{
		if(driver.isElementDisplayed(orderListingPage.enterCity, "Enter City"))
		{
			driver.enterText(orderListingPage.enterCity, "newyork", "City entries");
			logPass("valid entries");
		}
		else
			logFail("logFailed to enter Sku id");
	}
	
	public void enterValdCountry()
	{
		if(driver.isElementDisplayed(orderListingPage.enterCountry, "Enter City"))
		{
			driver.enterText(orderListingPage.enterCountry, "united states", "City entries");
			logPass("valid entries");
		}
		else
			logFail("logFailed to enter Sku id");
	}
	
	public void enterInValdCity()
	{
		if(driver.isElementDisplayed(orderListingPage.enterCity, "Enter City"))
		{
			driver.enterText(orderListingPage.enterCity, "$abcde$123", "City entries");
			logPass("valid entries");
		}
		else
			logFail("logFailed to enter Sku id");
	}
	
	public void enterEmptyCity()
	{
		if(driver.isElementDisplayed(orderListingPage.enterCity, "Enter City"))
		{
			driver.enterText(orderListingPage.enterCity, "  ", "City entries");
			logPass("valid entries");
		}
		else
			logFail("logFailed to enter Sku id");
	}
	
	public void enterEmptyCnty()
	{
		if(driver.isElementDisplayed(orderListingPage.enterCountry, "Enter Country"))
		{
			driver.enterText(orderListingPage.enterCountry, "  ", "Country  entries");
			logPass("valid entries");
		}
		
		else
			logFail("logFailed to enter Country code");
	}
	
	public void clickSearchbutton() {
		
		if(driver.isElementDisplayed(orderListingPage.searchButton, "Trigger Search"))
		{
			driver.jsClickElement(orderListingPage.searchButton, "Click Search");
			logPass("search icon is clicked");
		}
		else
			logFail("logFailed to click Search icon");
	}
	
	public void verifyNoSearchAlert()
	{
		if(driver.explicitWaitforVisibility(orderListingPage.noDataErr, "No Data Found Msg"))
			logPass("No Data Found Msg is displayed");
		else
			logFail("No Data Found Msg is not displayed");
	}
	
	
	
	public void verifyZipCodeFilter()
	{
		int size ;
		size = driver.getSize(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)"), "Filtrable list class");
		//System.out.println(size);
		for(int i = 1 ; i <= size ; i++)
		{
			
			String sku = driver.getText(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "");
			if(sku!=null && sku.equalsIgnoreCase("zipcode"))
		    {
				String val= driver.getElementAttribute(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"),"class","zip");
				if(val.contains("active"))
					logPass("Already Country selected");
				else
				{
					driver.jsClickElement(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "zip clicked");
					logPass("zip is clicked");
				}
		        break;
		    }
		    else if(i==size)
		    	logFail("zip not displayed");
		    else
		    	continue;
		}
	}
	
	
	public void enterValdZip()
	{
		if(driver.isElementDisplayed(orderListingPage.enterZip, "Enter Zip"))
		{
			driver.enterText(orderListingPage.enterZip, "10001", "Zip entries");
			logPass("valid entries");
		}
		else
			logFail("logFailed to enter ZipCode");
	}
	
	
	
	public void enterInValdZip()
	{
		if(driver.isElementDisplayed(orderListingPage.enterZip, "Enter Zip"))
		{
			driver.enterText(orderListingPage.enterZip, "$abcde$123", "Zip entries");
			logPass("valid entries");
		}
		else
			logFail("logFailed to enter zip");
	}
	
	public void enterEmptyZip()
	{
		if(driver.isElementDisplayed(orderListingPage.enterZip, "Enter Zip code"))
		{
			driver.enterText(orderListingPage.enterZip, "  ", "Zip code entries");
			logPass("valid entries");
		}
		
		else
			logFail("logFailed to enter Zip code");
	}
	
	public void verifyStateFilter()
	{
		int size ;
		size = driver.getSize(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)"), "Filtrable list class");
		//System.out.println(size);
		for(int i = 1 ; i <= size ; i++)
		{
			
			String sku = driver.getText(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "");
			if(sku!=null && sku.equalsIgnoreCase("State"))
		    {
				String val= driver.getElementAttribute(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"),"class","State");
				if(val.contains("active"))
					logPass("Already State selected");
				else
				{
					driver.jsClickElement(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "State clicked");
					logPass("State is clicked");
				}
		        break;
		    }
		    else if(i==size)
		    	logFail("State not displayed");
		    else
		    	continue;
		}
	}
	
	
	public void clickStateDrpDwn()
	{
		if(driver.isElementDisplayed(orderListingPage.triggState, "click State drop down"))
		{
			driver.jsClickElement(orderListingPage.triggState, "click State drop down");
			logPass("State drop down displayed");
		}
		else
			logFail("logFailed to displayed drop down");
	}
	
	

	public void enterInValdState()
	{
		if(driver.isElementDisplayed(orderListingPage.enterState, "Enter State"))
		{
			driver.enterText(orderListingPage.enterState, "$abcde$123", "State entries");
			logPass("valid entries");
		}
		else
			logFail("logFailed to enter state");
	}
	
	
	public void enterValidState()
	{
		if(driver.isElementDisplayed(orderListingPage.enterState, "Enter State"))
		{
			driver.enterText(orderListingPage.enterState, "Nevada", "State entries");
			logPass("valid entries");
		}
		else
			logFail("logFailed to enter State");
	}
	

	
	public void verifyFulfillmentFilter()
	{
		int filSize ;
		filSize = driver.getSize(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)"), "Filtrable list class");
		//System.out.println(filSize);
		for(int i = 1 ; i <= filSize ; i++)
		{
			
			String fulFil = driver.getText(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "");
			if(fulFil!=null && fulFil.equalsIgnoreCase("Fulfilment Type"))
		    {
				String val= driver.getElementAttribute(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"),"class","FulFillment");
				if(val.contains("active"))
					logPass("Already Fulfillment selected");
				else
				{
					driver.jsClickElement(By.xpath("(//*[@data-qa='add-more-filter']//ul//li)["+i+"]"), "Fulfillment");
					logPass("FulFillment is clicked");
				}
		        break;
		    }
		    else if(i==filSize)
		    	logFail("Fulfillment not displayed");
		    else
		    	continue;
		}
		
	}
	
	public void selectFulFilPhysicType()
	{
	if(driver.isElementDisplayed(orderListingPage.fulFillment, "Trigger Fulfillment"))
	{
		driver.jsClickElement(orderListingPage.fulFillment, "click Fulfillment drop down");
		logPass("Fulfillment has been clicked");
	     int ful = driver.getSize(By.xpath("//div[@elem='fulfilmentType']//ul//li"), "Fulfillment Type");	
		for(int i = 1 ; i <= ful ; i++)
		{
			String text = driver.getText(By.xpath("(//div[@elem='fulfilmentType']//ul//li)["+i+"]"), "type");
			
			if(text!=null && text.equalsIgnoreCase("physical")) 
			{
				driver.jsClickElement(By.xpath("(//div[@elem='fulfilmentType']//ul//li)["+i+"]"), "Physical clicked");
				logPass("Physical Fulfillment is selected ");
				break;
			}
			else if(text==null)
				logFail("Fulfillemt not listed");
		}
	}
	}
	
	
	public void selectFulFilDigitalType()
	{
		if(driver.isElementDisplayed(orderListingPage.fulFillment, "Trigger Fulfillment"))
		{
			driver.jsClickElement(orderListingPage.fulFillment, "click Fulfillment drop down");
			logPass("Fulfillment has been clicked");
		     int ful = driver.getSize(By.xpath("//div[@elem='fulfilmentType']//ul//li"), "Fulfillment Type");	
			for(int i = 1 ; i <= ful ; i++)
			{
				String text = driver.getText(By.xpath("(//div[@elem='fulfilmentType']//ul//li)["+i+"]"), "type");
				
				if(text!=null && text.equalsIgnoreCase("digital")) 
				{
					driver.jsClickElement(By.xpath("(//div[@elem='fulfilmentType']//ul//li)["+i+"]"), "digital clicked");
					logPass("digital Fulfillment is selected ");
					break;
				}
				else if(text==null)
					logFail("Fulfillemt not listed");
				
			}
		}
	}
	
	public void verifyPaymentDetailsPopup()
	{
		driver.clickElement(By.xpath("//*[@data-qa='view-payment-details-trigger']"), "payment details");
		if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='payment-details-title']"), "paymentpopup"))
			logPass("Payment Details Popup is Present");
		else
			logFail("Payment Details Popup is not Present");
	}
	
	
	public void verifyAmountValue()
	{
		if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='amount-value']"), "Amount"))
			logPass("Amount is Present");
		else
			logFail("Amount is not Present");
	}
	
	public void verifyCardType()
	{
		if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='amount-value']"), "cardType"))
			logPass("cardType is Present");
		else
			logFail("cardType is not Present");
	}
	
	public void verifyExpiryDate()
	{
		if(driver.explicitWaitforVisibility(By.xpath("(//*[@data-qa='description-value'])//div//div[2]//span[2]"),"expiry date"))
			logPass("ExpiryDate is Present");
		else
			logFail("ExpiryDate is not Present");
	}
	
	public void clickDateRange()
	{	
		if(driver.jsClickElement(orderListingPage.DateRangeOLP,"DateRange" ))
			logPass("DateRange is clicked");
			else
			logFail("DateRange is not clicked");
	}

	public void clickMonth()
	{
		if(driver.jsClickElement(orderListingPage.MonthRangeOLP,"Month" ))
			logPass("Month is clicked");
		else
			logFail("Month is not clicked");
	}

	public String getDate()
	{
		Date date = new Date();
		String dateString = date.toLocaleString();
		return dateString;
	}

	public String getCurrMonth()
	{
		String date = getDate();
		String[] month = date.split(" ", 0);
		return month[0].replace(",", "");
	}
	
	

	public String getCurrDate()
	{
		String date = getDate();
		String[] curr = date.split(" ", 0);
		return curr[1].replace(",","");
	}

	public String getCurrYear()
	{
		String date = getDate();
		String[] year = date.split(" ", 0);
		return year[2].replace(",", "");
	}
	
	public void selectMonth()
	{
		String Month = getCurrMonth();
		for(int i=1;i<=12;i++)
		{
			String optn = driver.getText(By.xpath("(//*[@class='drp-calendar left']//th[@class='month']//select[@class='monthselect']//option)["+i+"]"),"Option");
			if(optn.equals(Month))
			{	
				driver.clickElement(By.xpath("(//*[@class='drp-calendar left']//th[@class='month']//select[@class='monthselect']//option)["+i+"]"),"Option");
				logPass("CurrMonth selected");
				break;
			}
			else if(i==12)
				logFail("CurrMonth is not selected");
		}
	}
	
	public void selectYear()
	{
		String Year = getCurrYear();
		for(int i=1;i<=200;i++)
		{
			String optn = driver.getText(By.xpath("(//*[@class='drp-calendar left']//th[@class='month']//select[@class='yearselect']//option)["+i+"]"),"Option");
			if(optn.equals(Year))
			{	
				driver.clickElement(By.xpath("(//*[@class='drp-calendar left']//th[@class='month']//select[@class='yearselect']//option)["+i+"]"),"Option");
				logPass("CurrYear selected");
				break;
			}
			else if(i==200)
				logFail("CurrYear is not selected");
		}
	}

	public void selectStartDate(String sDate)
	{
		for(int i=1;i<=42;i++)
		{
			String dval = driver.getText(By.xpath("(//*[@class='drp-calendar left']//td)["+i+"]"),"Date Value");
			if(dval.equals(sDate))
			{	
				driver.clickElement(By.xpath("(//*[@class='drp-calendar left']//td)["+i+"]"),"Date Value");
				logPass("Date Value selected");
				break;
			}
			else if(i==42)
				logFail("Date Value is not selected");
			else
				continue;
		}
	}

	public void selectEndDate()
	{
		String eDate = getCurrDate();
		for(int i=1;i<=42;i++)
		{
			String dval = driver.getText(By.xpath("(//*[@class='drp-calendar right']//td)["+i+"]"),"Date Value");
			if(dval.equals(eDate))
			{	
				driver.clickElement(By.xpath("(//*[@class='drp-calendar right']//td)["+i+"]"),"Date Value");
				logPass("Date Value selected");
				break;
			}
			else if(i==42)
				logFail("Date Value is not selected");
			else
				continue;
		}
	}
	
	String paymentSessionId="";
	String orderSessionId="";
	String foundationSessionId="";
    String authToken="";
    String collectionid="";
    String projectid="";
    String projectname="";
    String salescatalogtype="";
    String attributeId="";
    String attributeGroupId="";
    String attributeGroupVariantId="";
    String productId="";
    String skuId="";
    
    public JSONObject createCollectionpayment() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		try {
			//res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("BusinessId")+"&serviceName=payment","{ \"name\": \"Automation_Payment"+driver.generateRandomNumber(1000000000)+"\", \"properties\": [{\"name\":\"default_locale\",\"value\":\"en_US\"},{\"name\":\"c1_prop1\",\"value\":\"c1_value1\"},{\"name\":\"c1_prop2\",\"value\":\"c1_value2\"},{\"name\":\"c1_prop3\",\"value\":\"c1_value3\"}], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId);
			res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("BusinessId")+"&serviceName=payment","{ \"name\": \"Automation_Payment"+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(1000)+"\", \"properties\": [{\"name\":\"default_locale\",\"value\":\"en_US\"}], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		resp = res.get(0);
		//System.out.println("createCollectionpayment"+resp);
		logPass("createCollectionpayment "+resp);
		
		JSONObject root = new JSONObject(resp);
		collectionid = root.getJSONObject("collection").getString("id");
		properties.put("collectionIdPayment", collectionid);
		//System.out.println("collectionid"+collectionid);
		return root; 	
	}
	
		
	public JSONObject createCollectionoms() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("BusinessId")+"&serviceName=order","{ \"name\": \"Automation_Order"+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(1000)+"\", \"properties\": [{\"name\":\"orderidlength\",\"value\":\"14\"},{\"name\":\"BPM_SubmitOrder\",\"value\":null},{\"name\":\"lookupRequiredFields\",\"value\":\"usertracking.lastname,usertracking.zip\"},{\"name\":\"isNumericOrderIdOnly\",\"value\":null},{\"name\":\"skipDefaultQueue\",\"value\":null},{\"name\":\"createOrderCustomValidator\",\"value\":null}], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId);
		resp = res.get(0);
		//System.out.println(resp);
		logPass("createCollectionoms "+resp);
		
		JSONObject root = new JSONObject(resp);
		collectionid = root.getJSONObject("collection").getString("id");
		properties.put("collectionId", collectionid);
		String confiUrl=properties.getProperty("ApplicationUrl");
		confiUrl=confiUrl.replace("<<collectionId>>", collectionid);
		properties.put("ApplicationUrl", confiUrl);
		//System.out.println("collectionid"+collectionid);
		return root; 	
	}
	
	public JSONObject createCollectionomsNoorder() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		logPass("SessionId "+sessionId);
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("BusinessId")+"&serviceName=order","{ \"name\": \"Automation_Order"+driver.generateRandomNumber(1000000000)+"\", \"properties\": [{\"name\":\"orderidlength\",\"value\":\"14\"},{\"name\":\"BPM_SubmitOrder\",\"value\":null},{\"name\":\"lookupRequiredFields\",\"value\":\"usertracking.lastname,usertracking.zip\"},{\"name\":\"isNumericOrderIdOnly\",\"value\":null},{\"name\":\"skipDefaultQueue\",\"value\":null},{\"name\":\"createOrderCustomValidator\",\"value\":null}], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",properties.getProperty("sessionIdorder"));
		resp = res.get(0);
		//System.out.println(resp);
		logPass("createCollectionomsNoorder "+resp);
		
		JSONObject root = new JSONObject(resp);
		collectionid = root.getJSONObject("collection").getString("id");
		properties.put("collectionIdNoorder", collectionid);
//		String confiUrl=properties.getProperty("ApplicationUrl");
//		confiUrl=confiUrl.replace("<<collectionId>>", collectionid);
//		properties.put("ApplicationUrl", confiUrl);
		//System.out.println("collectionid"+collectionid);
		return root; 	
	}
	
	/*public JSONObject storeassocate() throws JSONException, ClientProtocolException, IOException
    {
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=api.put(properties.getProperty("Domain")+"/adminservices/stores/"+properties.getProperty("OrderStoreId"),"{\"businessId\":"+properties.getProperty("businessId")+",\"name\":\"Order Admin\",\"status\":0,\"type\":1,\"timeZone\":\"UTC\",\"logoUrl\":\"https://www.skava.com/logo.png\",\"defaultLocale\":\"en_US\",\"defaultShippingRegion\":\"US\",\"defaultCurrency\":\"USD\",\"properties\":[{\"name\":\"prop.name\",\"value\":\"prop.val\"}],\"associations\":[{\"name\":\"payment\",\"collectionId\":"+properties.getProperty("collectionIdPayment")+"},{\"name\":\"order\",\"collectionId\":"+properties.getProperty("collectionId")+"}]}",SessionId);
        resp = res.get(0);
        System.out.println(resp);
        
        JSONObject root = new JSONObject(resp);
//        collectionid = root.getJSONObject("collection").getString("id");
//        properties.put("collectionid", collectionid);
//        System.out.println("collectionid"+collectionid);
        return root;
        
    } */
	
	public void generatesessionIdfoundation() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=	api.ReadAllDataFromUrl(properties.getProperty("APIDomain")+"/foundationservices/getTestTokens","","");
		resp = res.get(0);
		//System.out.println(resp);
		logPass("generatesessionIdfoundation "+resp);
		
		JSONObject root = new JSONObject(resp);
		foundationSessionId = root.getString("testsuperadmin"); 
		properties.put("foundationSessionId", foundationSessionId);
		
	}
	
	public void storeassocatefoundation() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrlserviceput(properties.getProperty("APIDomain")+"/foundationservices/stores/"+properties.getProperty("OrderStoreId")+"/associations","{\"associations\":[{\"name\":\"payment\",\"collectionId\":"+properties.getProperty("collectionIdPayment")+"},{\"name\":\"order\",\"collectionId\":"+properties.getProperty("collectionId")+"}]}",foundationSessionId,"");
		resp = res.get(0);
		//System.out.println(resp);
		logPass("storeassocatefoundation "+resp);
		
		
	}
	
	public void storeassocatefoundationNoorder() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrlserviceput(properties.getProperty("APIDomain")+"/foundationservices/stores/"+properties.getProperty("OrderStoreId")+"/associations","{\"associations\":[{\"name\":\"payment\",\"collectionId\":"+properties.getProperty("collectionIdPayment")+"},{\"name\":\"order\",\"collectionId\":"+properties.getProperty("collectionIdNoorder")+"}]}",properties.getProperty("foundationSessionId"),"");
		resp = res.get(0);
		//System.out.println(resp);
		logPass("storeassocatefoundation "+resp);
		
		
	}
	
	public void generatesessionIdpayment() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=	api.ReadAllDataFromUrl(properties.getProperty("APIDomain")+"/paymentservices/getTestTokens","","");
		resp = res.get(0);
		//System.out.println(resp);
		logPass("generatesessionIdpayment "+resp);
		
		JSONObject root = new JSONObject(resp);
		paymentSessionId = root.getString("testsuperadmin"); 
		
	}
	public void createCollectionproviderpayment() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrlpayment(properties.getProperty("APIDomain")+"/paymentservices/collectionproviders/","{ \"properties\": [ { \"name\": \"string\", \"value\": \"string\" } ], \"providerMasterId\": 2}",paymentSessionId,properties.getProperty("collectionIdPayment"));
		resp = res.get(0);
		//System.out.println(resp);
		logPass("createCollectionproviderpayment "+resp);
		
			
	}
	public JSONObject createpayment() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrlpayment(properties.getProperty("APIDomain")+"/paymentservices/payments/","{ \"billingAddress\": { \"addressLine1\": \"13, street 1\", \"addressLine2\": \"string\", \"addressLine3\": \"string\", \"city\": \"Albany\", \"country\": \"US\", \"county\": \"NY\", \"email\": \"test@mail.com\", \"firstName\": \"first\", \"lastName\": \"last\", \"phoneNumber\": \"1234567890\", \"state\": \"NY\", \"zipcode\": \"12084\" }, \"default\": true, \"name\": \"string\", \"paymentProperties\": [ { \"name\": \"cardNumber\", \"value\": \"378282246310005\" },{ \"name\": \"cardExpirationMonth\", \"value\": \"06\" },{ \"name\": \"cardExpirationYear\", \"value\": \"2023\" },{ \"name\": \"cardCVVNumber\", \"value\": \"123\" },{ \"name\": \"firstName\", \"value\": \"Dilip\" },{ \"name\": \"lastName\", \"value\": \"Kumar\" } ], \"type\": \"CREDITCARD\"}",paymentSessionId,properties.getProperty("collectionIdPayment"));
		resp = res.get(0);
		//System.out.println(resp);
		logPass("createpayment "+resp);
		
		
		JSONObject root = new JSONObject(resp);
		String paymentId = root.getString("id");
		properties.put("paymentId", paymentId);
		logPass("paymentId= "+paymentId);
//		String confiUrl=properties.getProperty("ApplicationUrl");
//		confiUrl=confiUrl.replace("<<collectionId>>", collectionid);
//		properties.put("ApplicationUrl", confiUrl);
//		System.out.println("collectionid"+collectionid);
		return root; 	
	}
	
	public JSONObject createpaymentitem() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrlpayment(properties.getProperty("APIDomain")+"/paymentservices/paymentitems/","{ \"currencyCode\": \"string\", \"paymentId\": "+properties.getProperty("paymentId")+", \"properties\": [ { \"name\": \"string\", \"value\": \"string\" } ], \"value\": 200000}",paymentSessionId,properties.getProperty("collectionIdPayment"));
		resp = res.get(0);
		//System.out.println(resp);
		logPass("createpaymentitem "+resp);
		
		
		JSONObject root = new JSONObject(resp);
		String paymentitemId = root.getString("id");
		properties.put("paymentitemId", paymentitemId);
		logPass("paymentitemId= "+paymentitemId);
//		String confiUrl=properties.getProperty("ApplicationUrl");
//		confiUrl=confiUrl.replace("<<collectionId>>", collectionid);
//		properties.put("ApplicationUrl", confiUrl);
//		System.out.println("collectionid"+collectionid);
		return root; 	
	}
	
	public void generatesessionIdorder() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=	api.ReadAllDataFromUrl(properties.getProperty("APIDomain")+"/omsservices/getTestTokens","","");
		resp = res.get(0);
		//System.out.println(resp);
		logPass("generatesessionIdorder "+resp);
		
		JSONObject root = new JSONObject(resp);
		orderSessionId = root.getString("testsuperadmin"); 
		
	}
	
	public void createorder() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		for(int i=1;i<=3;i++)
		{
			
			res=api.ReadAllDataFromUrlpayment(properties.getProperty("APIDomain")+"/omsservices/orders/","{\"orderdetails\":{\"discounts\":null,\"fulfilmentinfo\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"fulfilmentid\":{\"status\":\"string\",\"id\":0},\"type\":\"PHYSICAL\",\"shippingaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"Vignesh\",\"lastname\":\"C\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"Skava Systems\",\"deliveryinstruction\":\"Please call before attempting delivery\",\"address1\":\"My shipping address line 1\",\"address2\":\"My shipping address line 2\",\"address3\":\"My shipping address line 3\",\"city\":\"Coimbatore\",\"county\":\"NY\",\"state\":\"CBE\",\"zip\":\"641015\",\"country\":\"IN\"},\"shippingmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"CURRENCY\",\"conversionratio\":0.01,\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":1.35,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":15,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"CURRENCY\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":-1.5,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ORDER\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"string\",\"conversionratio\":0,\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":0,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"string\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":0,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"string\",\"deliveryinstruction\":\"string\",\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"county\":\"string\",\"state\":\"string\",\"zip\":\"string\",\"country\":\"string\"},\"shippingcontact\":{\"phone\":\"9181716151\",\"email\":\"chandran.vignesh@gmail.com\",\"preferredcontact\":\"EMAIL\"}}},\"orderinfo\":{\"channel\":\"MYCHANNEL001\",\"storeid\":\""+properties.getProperty("OrderStoreId")+"\",\"affiliateid\":\"string\"},\"orderitems\":[{\"id\":\"MYITMID001\",\"status\":\"string\",\"paymentstatus\":\"PREAUTHORIZED\",\"item\":{\"info\":{\"name\":\"My Test SKU A\",\"categoryid\":\"MYCATID001\",\"image\":\"https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcRX1vWH4rB69KBISOF7Gz3nudu0FigBkXBmlMS7XJMelgZZ_maTcoOiShVXMub4gioWo8qBMTRgL0R4Yv_MK5hc8LwbOjZMPVfH1_Y_BmXQZx6Z_YP8LRiR&usqp=CAc\",\"link\":\"www.amazon.com\",\"description\":\"My Test SKU A\",\"size\":\"8\",\"color\":\"Red\",\"style\":\"Sport\",\"fit\":\"Flexible\",\"dimension\":\"8\"},\"weightValue\":\"string\",\"weightUnits\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"minpurchasequantity\":0,\"maxpurchasequantity\":0,\"preordered\":false,\"skuid\":\"MYSKUID001\",\"pid\":\"MYPID001\",\"upc\":\"string\",\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"CURRENCY\",\"saveprice\":0,\"regprice\":0,\"saleprice\":150,\"costprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":6.75,\"taxrate\":0,\"taxable\":0},\"roundoff\":0}],\"taxcode\":\"string\",\"binid\":\"ATP_Bin\",\"vendor\":\"string\",\"exppreordershipmentdatetime\":0},\"fulfilmentinfo\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"fulfilmentid\":{\"status\":\"string\",\"id\":0},\"type\":\"PHYSICAL\",\"shippingaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"Vignesh\",\"lastname\":\"Chandran\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"Amazon\",\"deliveryinstruction\":\"Do not deliver between 11 AM and 4 PM\",\"address1\":\"My address line 1\",\"address2\":\"My address line 2\",\"address3\":\"My address line 3\",\"city\":\"NY\",\"county\":\"NY\",\"state\":\"string\",\"zip\":\"641015\",\"country\":\"USA\"},\"shippingmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"CURRENCY\",\"conversionratio\":0.01,\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.81,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":9,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"CURRENCY\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":-0.9,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"string\",\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":0,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"string\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":0,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"string\",\"deliveryinstruction\":\"string\",\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"county\":\"string\",\"state\":\"string\",\"zip\":\"string\",\"country\":\"string\"},\"shippingcontact\":{\"phone\":\"9945215370\",\"email\":\"vignesh.c@skava.com\",\"preferredcontact\":\"EMAIL\"}},\"math\":[{\"paymentid\":\""+properties.getProperty("paymentId")+"\",\"mode\":\"CURRENCY\",\"fulfilmath\":{\"estfulfilsale\":0,\"estfulfilcost\":0,\"estfulfiltax\":0,\"fulfilsale\":0,\"fulfilcost\":0,\"fulfiltax\":0,\"fulfildiscount\":0,\"totalfulfilsale\":0},\"sale\":450,\"cost\":0,\"discount\":-45,\"discountcost\":0,\"personalizationsale\":0,\"personalizationcost\":0,\"personalizationdiscount\":0,\"totalsale\":405,\"esttax\":0,\"tax\":20.25,\"taxable\":0,\"roundoff\":0,\"conversionratio\":0.01,\"estunittax\":0,\"unittax\":0}],\"mathordershipping\":[{\"paymentid\":\""+properties.getProperty("paymentId")+"\",\"mode\":\"CURRENCY\",\"fulfilmath\":{\"estfulfilsale\":0,\"estfulfilcost\":0,\"estfulfiltax\":0,\"fulfilsale\":9,\"fulfilcost\":0,\"fulfiltax\":0.81,\"fulfildiscount\":-0.9,\"totalfulfilsale\":8.1},\"sale\":0,\"cost\":0,\"discount\":0,\"discountcost\":0,\"personalizationsale\":0,\"personalizationcost\":0,\"personalizationdiscount\":0,\"totalsale\":8.1,\"esttax\":0,\"tax\":0.81,\"taxable\":0,\"roundoff\":0,\"conversionratio\":0.01,\"estunittax\":0,\"unittax\":0}],\"inventory\":{\"blocktransaction\":\"string\",\"releasetransaction\":\"string\"},\"personalization\":[{\"id\":\"string\",\"skuid\":\"string\",\"receipt\":false,\"message\":\"string\",\"wrap\":\"string\",\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"CURRENCY\",\"unitpersonalizationcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0},\"totalpersonalizationcost\":0,\"unitpersonalizationprice\":0,\"totalpersonalizationprice\":0,\"discount\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"CURRENCY\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":0,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]}],\"discounts\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"CURRENCY\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":-15,\"totaldiscount\":-45,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}],\"quantity\":3,\"returnable\":false,\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"CURRENCY\",\"saveprice\":0,\"regprice\":0,\"saleprice\":450,\"costprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":20.25,\"taxrate\":0,\"taxable\":0},\"roundoff\":0}],\"storeid\":\"850\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"timeStamp\":0,\"type\":\"SKU\"},{\"id\":\"MYITMID002\",\"status\":\"string\",\"paymentstatus\":\"PREAUTHORIZED\",\"item\":{\"info\":{\"name\":\"My Test SKU B\",\"categoryid\":\"MYCATID001\",\"image\":\"https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcRX1vWH4rB69KBISOF7Gz3nudu0FigBkXBmlMS7XJMelgZZ_maTcoOiShVXMub4gioWo8qBMTRgL0R4Yv_MK5hc8LwbOjZMPVfH1_Y_BmXQZx6Z_YP8LRiR&usqp=CAc\",\"link\":\"www.amazon.com\",\"description\":\"My Test SKU B\",\"size\":\"8\",\"color\":\"Blue\",\"style\":\"Sport\",\"fit\":\"Flexible\",\"dimension\":\"8\"},\"weightValue\":\"string\",\"weightUnits\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"minpurchasequantity\":0,\"maxpurchasequantity\":0,\"preordered\":false,\"skuid\":\"MYSKUID001\",\"pid\":\"MYPID001\",\"upc\":\"string\",\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"CURRENCY\",\"saveprice\":0,\"regprice\":0,\"saleprice\":100,\"costprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":4.5,\"taxrate\":0,\"taxable\":0},\"roundoff\":0}],\"taxcode\":\"string\",\"binid\":\"ATP_Bin\",\"vendor\":\"string\",\"exppreordershipmentdatetime\":0},\"fulfilmentinfo\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"fulfilmentid\":{\"status\":\"string\",\"id\":0},\"type\":\"PHYSICAL\",\"shippingaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"Vignesh\",\"lastname\":\"Chandran\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"Amazon\",\"deliveryinstruction\":\"Do not deliver between 11 AM and 4 PM\",\"address1\":\"My address line 1\",\"address2\":\"My address line 2\",\"address3\":\"My address line 3\",\"city\":\"NY\",\"county\":\"NY\",\"state\":\"string\",\"zip\":\"641015\",\"country\":\"USA\"},\"shippingmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"CURRENCY\",\"conversionratio\":0.01,\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.54,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":6,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"CURRENCY\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":-0.6,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"string\",\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":0,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"string\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":0,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"string\",\"deliveryinstruction\":\"string\",\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"county\":\"string\",\"state\":\"string\",\"zip\":\"string\",\"country\":\"string\"},\"shippingcontact\":{\"phone\":\"9945215370\",\"email\":\"vignesh.c@skava.com\",\"preferredcontact\":\"EMAIL\"}},\"math\":[{\"paymentid\":\""+properties.getProperty("paymentId")+"\",\"mode\":\"CURRENCY\",\"fulfilmath\":{\"estfulfilsale\":0,\"estfulfilcost\":0,\"estfulfiltax\":0,\"fulfilsale\":0,\"fulfilcost\":0,\"fulfiltax\":0,\"fulfildiscount\":0,\"totalfulfilsale\":0},\"sale\":200,\"cost\":0,\"discount\":-20,\"discountcost\":0,\"personalizationsale\":0,\"personalizationcost\":0,\"personalizationdiscount\":0,\"totalsale\":180,\"esttax\":0,\"tax\":9,\"taxable\":0,\"roundoff\":0,\"conversionratio\":0.01,\"estunittax\":0,\"unittax\":0}],\"mathordershipping\":[{\"paymentid\":\""+properties.getProperty("paymentId")+"\",\"mode\":\"CURRENCY\",\"fulfilmath\":{\"estfulfilsale\":0,\"estfulfilcost\":0,\"estfulfiltax\":0,\"fulfilsale\":6,\"fulfilcost\":0,\"fulfiltax\":0.54,\"fulfildiscount\":-0.6,\"totalfulfilsale\":5.4},\"sale\":0,\"cost\":0,\"discount\":0,\"discountcost\":0,\"personalizationsale\":0,\"personalizationcost\":0,\"personalizationdiscount\":0,\"totalsale\":5.4,\"esttax\":0,\"tax\":0.54,\"taxable\":0,\"roundoff\":0,\"conversionratio\":0.01,\"estunittax\":0,\"unittax\":0}],\"inventory\":{\"blocktransaction\":\"string\",\"releasetransaction\":\"string\"},\"personalization\":[{\"id\":\"string\",\"skuid\":\"string\",\"receipt\":false,\"message\":\"string\",\"wrap\":\"string\",\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"CURRENCY\",\"unitpersonalizationcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0},\"totalpersonalizationcost\":0,\"unitpersonalizationprice\":0,\"totalpersonalizationprice\":0,\"discount\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"CURRENCY\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":0,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]}],\"discounts\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"CURRENCY\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":-10,\"totaldiscount\":-20,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}],\"quantity\":2,\"returnable\":false,\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"CURRENCY\",\"saveprice\":0,\"regprice\":0,\"saleprice\":200,\"costprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":9,\"taxrate\":0,\"taxable\":0},\"roundoff\":0}],\"storeid\":\"850\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"timeStamp\":0,\"type\":\"SKU\"}],\"status\":[\"string\"],\"otherproperties\":{\"paymentCollectionId\":\""+properties.getProperty("collectionIdPayment")+"\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"payments\":[{\"id\":\""+properties.getProperty("paymentId")+"\",\"value\":629.1,\"type\":\"string\",\"format\":\"0\",\"mode\":\"CURRENCY\",\"paymentitems\":[{\"status\":\"PREAUTHORIZED\",\"value\":629.1,\"paymentrefid\":\""+properties.getProperty("paymentitemId")+"\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}],\"chargepriority\":0,\"refundpriority\":0,\"preauthorizations\":[]}],\"math\":[{\"conversionratio\":0.01,\"cost\":0,\"discount\":-65,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfildiscount\":-1.5,\"fulfilsale\":15,\"fulfiltax\":1.35,\"totalfulfilsale\":13.5},\"mode\":\"CURRENCY\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"paymentid\":\""+properties.getProperty("paymentId")+"\",\"sale\":650,\"tax\":30.6,\"taxable\":0,\"totalsale\":598.5,\"unittax\":0}],\"usertracking\":{\"accountid\":\"MYACC001\",\"firstname\":\"Dilip\",\"guestuser\":\"false\",\"guestuserid\":\"\",\"lastname\":\"Kumar\",\"usercontact\":{\"email\":\"dilipkumar@skava.com\",\"phone\":\"9945215370\",\"preferredcontact\":\"EMAIL\"},\"userid\":\"MYUID001\",\"zip\":\"641015\"}}",orderSessionId,properties.getProperty("collectionId"));
			//res=api.ReadAllDataFromUrlpayment(properties.getProperty("APIDomain")+"/omsservices/orders/","{\"orderdetails\":{\"discounts\":null,\"fulfilmentinfo\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"fulfilmentid\":{\"status\":\"string\",\"id\":0},\"type\":\"PHYSICAL\",\"shippingaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"Vignesh\",\"lastname\":\"C\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"Skava Systems\",\"deliveryinstruction\":\"Please call before attempting delivery\",\"address1\":\"My shipping address line 1\",\"address2\":\"My shipping address line 2\",\"address3\":\"My shipping address line 3\",\"city\":\"Coimbatore\",\"county\":\"NY\",\"state\":\"CBE\",\"zip\":\"641015\",\"country\":\"IN\"},\"shippingmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"Currency\",\"conversionratio\":0.01,\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":1.35,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":15,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"Currency\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":-1.5,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ORDER\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"string\",\"conversionratio\":0,\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":0,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"string\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":0,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"string\",\"deliveryinstruction\":\"string\",\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"county\":\"string\",\"state\":\"string\",\"zip\":\"string\",\"country\":\"string\"},\"shippingcontact\":{\"phone\":\"9181716151\",\"email\":\"chandran.vignesh@gmail.com\",\"preferredcontact\":\"EMAIL\"}}},\"orderinfo\":{\"channel\":\"MYCHANNEL001\",\"storeid\":\""+properties.getProperty("OrderStoreId")+"\",\"affiliateid\":\"string\"},\"orderitems\":[{\"id\":\"MYITMID001\",\"status\":\"string\",\"paymentstatus\":\"PREAUTHORIZED\",\"item\":{\"info\":{\"name\":\"My Test SKU A\",\"categoryid\":\"MYCATID001\",\"image\":\"https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcRX1vWH4rB69KBISOF7Gz3nudu0FigBkXBmlMS7XJMelgZZ_maTcoOiShVXMub4gioWo8qBMTRgL0R4Yv_MK5hc8LwbOjZMPVfH1_Y_BmXQZx6Z_YP8LRiR&usqp=CAc\",\"link\":\"www.amazon.com\",\"description\":\"My Test SKU A\",\"size\":\"8\",\"color\":\"Red\",\"style\":\"Sport\",\"fit\":\"Flexible\",\"dimension\":\"8\"},\"weightValue\":\"string\",\"weightUnits\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"minpurchasequantity\":0,\"maxpurchasequantity\":0,\"preordered\":false,\"skuid\":\"MYSKUID001\",\"pid\":\"MYPID001\",\"upc\":\"string\",\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"Currency\",\"saveprice\":0,\"regprice\":0,\"saleprice\":150,\"costprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":6.75,\"taxrate\":0,\"taxable\":0},\"roundoff\":0}],\"taxcode\":\"string\",\"binid\":\"ATP_Bin\",\"vendor\":\"string\",\"exppreordershipmentdatetime\":0},\"fulfilmentinfo\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"fulfilmentid\":{\"status\":\"string\",\"id\":0},\"type\":\"PHYSICAL\",\"shippingaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"Vignesh\",\"lastname\":\"Chandran\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"Amazon\",\"deliveryinstruction\":\"Do not deliver between 11 AM and 4 PM\",\"address1\":\"My address line 1\",\"address2\":\"My address line 2\",\"address3\":\"My address line 3\",\"city\":\"NY\",\"county\":\"NY\",\"state\":\"string\",\"zip\":\"641015\",\"country\":\"USA\"},\"shippingmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"Currency\",\"conversionratio\":0.01,\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.81,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":9,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"Currency\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":-0.9,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"string\",\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":0,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"string\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":0,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"string\",\"deliveryinstruction\":\"string\",\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"county\":\"string\",\"state\":\"string\",\"zip\":\"string\",\"country\":\"string\"},\"shippingcontact\":{\"phone\":\"9945215370\",\"email\":\"vignesh.c@skava.com\",\"preferredcontact\":\"EMAIL\"}},\"math\":[{\"paymentid\":\"PAYID006\",\"mode\":\"Currency\",\"fulfilmath\":{\"estfulfilsale\":0,\"estfulfilcost\":0,\"estfulfiltax\":0,\"fulfilsale\":0,\"fulfilcost\":0,\"fulfiltax\":0,\"fulfildiscount\":0,\"totalfulfilsale\":0},\"sale\":450,\"cost\":0,\"discount\":-45,\"discountcost\":0,\"personalizationsale\":0,\"personalizationcost\":0,\"personalizationdiscount\":0,\"totalsale\":405,\"esttax\":0,\"tax\":20.25,\"taxable\":0,\"roundoff\":0,\"conversionratio\":0.01,\"estunittax\":0,\"unittax\":0}],\"mathordershipping\":[{\"paymentid\":\"PAYID006\",\"mode\":\"Currency\",\"fulfilmath\":{\"estfulfilsale\":0,\"estfulfilcost\":0,\"estfulfiltax\":0,\"fulfilsale\":9,\"fulfilcost\":0,\"fulfiltax\":0.81,\"fulfildiscount\":-0.9,\"totalfulfilsale\":8.1},\"sale\":0,\"cost\":0,\"discount\":0,\"discountcost\":0,\"personalizationsale\":0,\"personalizationcost\":0,\"personalizationdiscount\":0,\"totalsale\":8.1,\"esttax\":0,\"tax\":0.81,\"taxable\":0,\"roundoff\":0,\"conversionratio\":0.01,\"estunittax\":0,\"unittax\":0}],\"inventory\":{\"blocktransaction\":\"string\",\"releasetransaction\":\"string\"},\"personalization\":[{\"id\":\"string\",\"skuid\":\"string\",\"receipt\":false,\"message\":\"string\",\"wrap\":\"string\",\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"Currency\",\"unitpersonalizationcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0},\"totalpersonalizationcost\":0,\"unitpersonalizationprice\":0,\"totalpersonalizationprice\":0,\"discount\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"Currency\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":0,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]}],\"discounts\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"Currency\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":-15,\"totaldiscount\":-45,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}],\"quantity\":3,\"returnable\":false,\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"Currency\",\"saveprice\":0,\"regprice\":0,\"saleprice\":450,\"costprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":20.25,\"taxrate\":0,\"taxable\":0},\"roundoff\":0}],\"storeid\":\"850\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"timeStamp\":0,\"type\":\"SKU\"},{\"id\":\"MYITMID002\",\"status\":\"string\",\"paymentstatus\":\"PREAUTHORIZED\",\"item\":{\"info\":{\"name\":\"My Test SKU B\",\"categoryid\":\"MYCATID001\",\"image\":\"https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcRX1vWH4rB69KBISOF7Gz3nudu0FigBkXBmlMS7XJMelgZZ_maTcoOiShVXMub4gioWo8qBMTRgL0R4Yv_MK5hc8LwbOjZMPVfH1_Y_BmXQZx6Z_YP8LRiR&usqp=CAc\",\"link\":\"www.amazon.com\",\"description\":\"My Test SKU B\",\"size\":\"8\",\"color\":\"Blue\",\"style\":\"Sport\",\"fit\":\"Flexible\",\"dimension\":\"8\"},\"weightValue\":\"string\",\"weightUnits\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"minpurchasequantity\":0,\"maxpurchasequantity\":0,\"preordered\":false,\"skuid\":\"MYSKUID001\",\"pid\":\"MYPID001\",\"upc\":\"string\",\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"Currency\",\"saveprice\":0,\"regprice\":0,\"saleprice\":100,\"costprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":4.5,\"taxrate\":0,\"taxable\":0},\"roundoff\":0}],\"taxcode\":\"string\",\"binid\":\"ATP_Bin\",\"vendor\":\"string\",\"exppreordershipmentdatetime\":0},\"fulfilmentinfo\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"fulfilmentid\":{\"status\":\"string\",\"id\":0},\"type\":\"PHYSICAL\",\"shippingaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"Vignesh\",\"lastname\":\"Chandran\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"Amazon\",\"deliveryinstruction\":\"Do not deliver between 11 AM and 4 PM\",\"address1\":\"My address line 1\",\"address2\":\"My address line 2\",\"address3\":\"My address line 3\",\"city\":\"NY\",\"county\":\"NY\",\"state\":\"string\",\"zip\":\"641015\",\"country\":\"USA\"},\"shippingmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"Currency\",\"conversionratio\":0.01,\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.54,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":6,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"Currency\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":-0.6,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnmethod\":{\"code\":\"string\",\"name\":\"string\",\"deliveryterm\":\"string\",\"deliverystore\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"priceinfo\":[{\"mode\":\"string\",\"estprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0},\"estcost\":0,\"price\":0,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"string\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":0,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]},\"returnaddress\":{\"responseCode\":\"string\",\"responseMessage\":\"string\",\"timeStamp\":0,\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"type\":\"string\",\"companyname\":\"string\",\"deliveryinstruction\":\"string\",\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"county\":\"string\",\"state\":\"string\",\"zip\":\"string\",\"country\":\"string\"},\"shippingcontact\":{\"phone\":\"9945215370\",\"email\":\"vignesh.c@skava.com\",\"preferredcontact\":\"EMAIL\"}},\"math\":[{\"paymentid\":\"PAYID006\",\"mode\":\"Currency\",\"fulfilmath\":{\"estfulfilsale\":0,\"estfulfilcost\":0,\"estfulfiltax\":0,\"fulfilsale\":0,\"fulfilcost\":0,\"fulfiltax\":0,\"fulfildiscount\":0,\"totalfulfilsale\":0},\"sale\":200,\"cost\":0,\"discount\":-20,\"discountcost\":0,\"personalizationsale\":0,\"personalizationcost\":0,\"personalizationdiscount\":0,\"totalsale\":180,\"esttax\":0,\"tax\":9,\"taxable\":0,\"roundoff\":0,\"conversionratio\":0.01,\"estunittax\":0,\"unittax\":0}],\"mathordershipping\":[{\"paymentid\":\"PAYID006\",\"mode\":\"Currency\",\"fulfilmath\":{\"estfulfilsale\":0,\"estfulfilcost\":0,\"estfulfiltax\":0,\"fulfilsale\":6,\"fulfilcost\":0,\"fulfiltax\":0.54,\"fulfildiscount\":-0.6,\"totalfulfilsale\":5.4},\"sale\":0,\"cost\":0,\"discount\":0,\"discountcost\":0,\"personalizationsale\":0,\"personalizationcost\":0,\"personalizationdiscount\":0,\"totalsale\":5.4,\"esttax\":0,\"tax\":0.54,\"taxable\":0,\"roundoff\":0,\"conversionratio\":0.01,\"estunittax\":0,\"unittax\":0}],\"inventory\":{\"blocktransaction\":\"string\",\"releasetransaction\":\"string\"},\"personalization\":[{\"id\":\"string\",\"skuid\":\"string\",\"receipt\":false,\"message\":\"string\",\"wrap\":\"string\",\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"Currency\",\"unitpersonalizationcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0},\"totalpersonalizationcost\":0,\"unitpersonalizationprice\":0,\"totalpersonalizationprice\":0,\"discount\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"Currency\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":0,\"totaldiscount\":0,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}]}]}],\"discounts\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"mode\":\"Currency\",\"discountclass\":\"string\",\"offerid\":\"string\",\"parentitem\":\"string\",\"priceinfo\":{\"unitdiscount\":-10,\"totaldiscount\":-20,\"unitcost\":0,\"totalcost\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxrate\":0,\"taxable\":0}},\"name\":\"string\",\"description\":\"string\",\"level\":\"ITEM\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}],\"quantity\":2,\"returnable\":false,\"priceinfo\":[{\"conversionratio\":0.01,\"mode\":\"Currency\",\"saveprice\":0,\"regprice\":0,\"saleprice\":200,\"costprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":9,\"taxrate\":0,\"taxable\":0},\"roundoff\":0}],\"storeid\":\"850\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"timeStamp\":0,\"type\":\"SKU\"}],\"status\":[\"string\"],\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"},\"payments\":[{\"id\":\"PAYID006\",\"value\":629.1,\"type\":\"string\",\"format\":\"0\",\"mode\":\"Currency\",\"paymentitems\":[{\"status\":\"PREAUTHORIZED\",\"value\":629.1,\"paymentrefid\":\""+properties.getProperty("paymentitemId")+"\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp3\":\"string\",\"additionalProp2\":\"string\"}}],\"chargepriority\":0,\"refundpriority\":0,\"preauthorizations\":[]}],\"math\":[{\"conversionratio\":0.01,\"cost\":0,\"discount\":-65,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfildiscount\":-1.5,\"fulfilsale\":15,\"fulfiltax\":1.35,\"totalfulfilsale\":13.5},\"mode\":\"Currency\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"paymentid\":\"PAYID006\",\"sale\":650,\"tax\":30.6,\"taxable\":0,\"totalsale\":598.5,\"unittax\":0}],\"usertracking\":{\"accountid\":\"MYACC001\",\"firstname\":\"Dilip\",\"guestuser\":\"false\",\"guestuserid\":\"\",\"lastname\":\"Kumar\",\"usercontact\":{\"email\":\"dilipkumar@skava.com\",\"phone\":\"9945215370\",\"preferredcontact\":\"EMAIL\"},\"userid\":\"MYUID001\",\"zip\":\"641015\"}}",orderSessionId,properties.getProperty("collectionId"));
			resp = res.get(0);
		//System.out.println(resp);
		logPass("createorder "+resp);
		
		
		JSONObject root = new JSONObject(resp);
		String orderId = root.getString("id");
		properties.put("orderId", orderId);
		
		logPass("orderId= "+orderId);
		}
	
	}
	
}


