package com.skava.reusable.components;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;

import com.framework.reporting.BaseClass;
import com.framework.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.frameworkutils.ExcelReader;
import com.skava.frameworkutils.loggerUtils;
import com.skava.networkutils.ApiReaderUtils;
import com.skava.object.repository.merchandiseObject;

public class MerchandiseadminComponents extends PromotionsComponents
{
	String sessionId="";
	String collectionid="";
	String projectid="";
	String attributeid="";
	String attributename="";
	String CategoryNameSearchInp = "";
	String CategoryIdSearchInp = "";
	String catidinp = "";
	String enteredprjct="";
	String createdprjct = "";
	String  categoryid="";
	String  categoryname="";
	String  semanticid="";
	String projectname="";
	//String existingproject="RTET";
	
	/*public void launchUrl() 
	{
		//driver.generateRandomString();
		driver.maximizeBrowser();
		driver.navigateToUrl(properties.getProperty("ApplicationUrl"));
		logInfo("Navigated to application url");
		loggerUtils.passLog("Logger");
	}*/

	/*public static void verifyMongoValidation()
	{
		MongoConnection.openMongoConnection();

		MongoConnection.validateProduct(ExcelReader.getData("General_Data","Product"));
	}
*/
		 
	 public void logPass(String text) 
	    {
	        ExtentTestManager.getTest().log(LogStatus.PASS, text);
	    }
	 
	 public void logFail(String text) 
	    {
	    	String currentUrl = driver.getCurrentUrl();
	        logInfo("Current Url: " + currentUrl);
	        ExtentTestManager.getTest().log(LogStatus.FAIL, text);
	        if (properties.getProperty("TakeScreenshots").equalsIgnoreCase("Yes")) 
	        {
	            String scrFile = driver.takeScreenShotReturnFilePath();
	            ExtentTestManager.getTest().log(LogStatus.FAIL, ExtentTestManager.getTest().addBase64ScreenShot(scrFile));
	        }
	    }

	 public void logInfo(String text) 
	    {
	        ExtentTestManager.getTest().log(LogStatus.INFO, text);
	    }
	 

			public void clkPrjctDropdown() throws InterruptedException
			{
				Thread.sleep(10000);
				driver.explicitWaitforVisibility(merchandiseObject.dropdownPrjct, "Project Dropdown");	
				if(driver.jsClickElement(merchandiseObject.dropdownPrjct, "Clicking on Project Dropdown"))
					logPass("Create New Project page popup is displayed");
				else
					logFail("Create New Project page popup is not displayed");
			}
			
			/*public void login()
			{
				driver.enterText(merchandiseObject.txtEmailAddress, ExcelReader.getData("merchandiseadmin", "Username"), "Email Address");
				driver.enterText(merchandiseObject.txtPassword, ExcelReader.getData("merchandiseadmin", "Password"), "Password");
				driver.jsClickElement(merchandiseObject.btnSignIn, "SignIn");
				driver.explicitWaitforVisibility(merchandiseObject.dropdownPrjct, "Project Dropdown");
				logInfo("User Logged-in Successfully");
			}*/
			
			public void logout()
			{
				driver.jsClickElement(merchandiseObject.profilemenu, "profile menu");
				driver.jsClickElement(By.xpath("//*[contains(text(),'Logout')]"), "logout");
				logInfo("User Logged-out Successfully");
					
			}
			
			ApiReaderUtils api=new ApiReaderUtils();
			public void generatesessionId() throws JSONException
			{
				 currentDomain = properties.getProperty("Domain");
				String resp = new String();
				List<String> res = new ArrayList<String>();
				res=	api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/auth/login","{\"identity\": \""+properties.getProperty("LoginUserName")+"\", \"password\": \""+properties.getProperty("LoginPassword")+"\"}","");
				resp = res.get(0);
				//System.out.println(resp);
				JSONObject root = new JSONObject(resp);
				sessionId = "SESSIONID="+root.getString("sessionId"); 
				properties.put("sessionIdorder", sessionId);
			}
		
			
			public JSONObject createCollection(String serviceName) throws JSONException
			{
				String resp = new String();
				List<String> res = new ArrayList<String>();
				res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections?businessId="+properties.getProperty("BusinessId")+"&serviceName=merchandising","{ \"name\": \"Automation_merchandise"+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(1000)+"\", \"properties\": [], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId);
				resp = res.get(0);
				//System.out.println(resp);
				logInfo("Collection Creation Response -> "+resp);
				JSONObject root = new JSONObject(resp);
				collectionid = root.getJSONObject("collectionResponse").getString("id");
				properties.put("collectionid", collectionid);
				//System.out.println("collectionid"+collectionid);
				String configURL=properties.getProperty("ApplicationUrl");
				configURL = configURL.replace("<<collectionId>>",collectionid);
				properties.put("ApplicationUrl",configURL);
				return root; 
			}
			
			
			public JSONObject mandatoryAttributeCreate() throws JSONException
		    {
		        String resp = new String();
		        List<String> res = new ArrayList<String>();
		        res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/merchandise/category/attribute?collectionId="+properties.getProperty("collectionid")+"&serviceName=merchandise","{ \"identifier\": \"Clothes\", \"status\": \"ACTIVE\", \"mandatory\": true, \"properties\": [ { \"name\": \"Option1\", \"locale\": \"en_US\" } ]}",sessionId);
		        resp = res.get(0);
		        System.out.println(resp);
		        JSONObject root = new JSONObject(resp);
		        return root; 
		    }
		    
		    	public JSONObject optionalAttributeCreate() throws JSONException
		    {
		        String resp = new String();
		        List<String> res = new ArrayList<String>();
		        res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/merchandise/category/attribute?collectionId="+properties.getProperty("collectionid")+"&serviceName=merchandise","{ \"identifier\": \"Coolers\", \"status\": \"ACTIVE\", \"mandatory\": false, \"properties\": [ { \"name\": \"Option2\", \"locale\": \"en_US\" } ]}",sessionId);
		        resp = res.get(0);																										
		        System.out.println(resp);
		        JSONObject root = new JSONObject(resp);
		        attributeid=root.getString("identifier");
		        attributename=root.getJSONArray("properties").getJSONObject(0).getString("name");
		        properties.put("attributeid",attributeid);
		        properties.put("attributename",attributename);
		        return root; 
		    }
			
			public JSONObject createProject() throws JSONException
		    {
		        String resp = new String();
		        List<String> res = new ArrayList<String>();
		        res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/merchandise/projects?collectionId="+properties.getProperty("collectionid"),"{\"status\":\"OPEN\",\"name\":\"ProjectSiX5QGS1nyCbz1co2lC85641P\",\"description\":\"string\",\"targetCompletionDate\":\"2019-10-21T17:32:28Z\"}",sessionId);
		        resp = res.get(0);																										
		        System.out.println(resp);
		        JSONObject root = new JSONObject(resp);
		     //   projectid = root.getJSONObject("project").getString("identifier");
		        projectid=root.getString("identifier");
		        properties.put("projectid", projectid);
		        System.out.println("projectid"+projectid);
		        String configURL=properties.getProperty("ApplicationUrl");
				configURL = configURL.replace("<<projectId>>",projectid);
				properties.put("ApplicationUrl",configURL);
			    projectname=root.getString("name");
		        properties.put("projectname", projectname);
		        System.out.println("projectname"+projectname);	
		        configURL=properties.getProperty("ApplicationUrl");
				configURL = configURL.replace("<<projectName>>",projectname);
				properties.put("ApplicationUrl",configURL);
		        return root; 
		    }
			
			public JSONObject createFirstCategory() throws JSONException
		    {
				String resp = new String();
		        List<String> res = new ArrayList<String>();
		        res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/merchandise/"+properties.getProperty("projectid")+"/category?collectionId="+properties.getProperty("collectionid"),"{\"identifier\":\"categoryqaShaLXBLnI\",\"status\":\"ACTIVE\",\"type\":\"STATIC\",\"visible\":\"true\",\"locked\":false,\"properties\":[{\"attributeId\":\""+properties.getProperty("attributeid")+"\",\"identifier\":\""+properties.getProperty("attributename")+"\",\"value\":\"testcategoryvalueXJzdQlHc\",\"locked\":false,\"locale\":\"en_US\",\"mandatory\":\"true\"}]}",sessionId);
		        resp = res.get(0);																										
		        System.out.println(resp);
		        JSONObject root = new JSONObject(resp);
		        projectid=root.getString("identifier");
		        String configURL=properties.getProperty("ApplicationUrl");
				configURL = configURL.replace("<<projectId>>",projectid);
		        return root; 
		    }
		    
		    public JSONObject createSecondCategory() throws JSONException
		    {
				 String resp = new String();
			        List<String> res = new ArrayList<String>();
			        res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/merchandise/"+properties.getProperty("projectid")+"/category?collectionId="+properties.getProperty("collectionid"),"{\"identifier\":\"categoryqasecond\",\"status\":\"ACTIVE\",\"type\":\"STATIC\",\"visible\":\"true\",\"locked\":false,\"properties\":[{\"attributeId\":\""+properties.getProperty("attributeid")+"\",\"identifier\":\""+properties.getProperty("attributename")+"\",\"value\":\"testcategoryvXJzdQlHc\",\"locked\":false,\"locale\":\"en_US\",\"mandatory\":\"true\"}]}",sessionId);
			        resp = res.get(0);																										
			        System.out.println(resp);
			        JSONObject root = new JSONObject(resp);
			        projectid=root.getString("identifier");
			        String configURL=properties.getProperty("ApplicationUrl");
					configURL = configURL.replace("<<projectId>>",projectid);
			        return root; 
		    }
			
			public void createNewProjectButton()
			{
				driver.explicitWaitforVisibility(merchandiseObject.btnCreateNewProject, "Create New Project button");
				if(driver.jsClickElement(merchandiseObject.btnCreateNewProject, "Clicking on Create New Project Button"))
					logPass("Clicked on Create new project button");
				else
					logFail("There is a issue on clicking create new project button");
			}
			
			public void enterNewprojectname()
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.newProject, "My New Project Textbox"))
				{
					logPass("My New Project Textbox is Present");
					String random=driver.generateRandomString();
				enteredprjct=ExcelReader.getData("merchandiseadmin", "SearchString")+random;
				if(driver.enterText(merchandiseObject.newProject, enteredprjct, "new project name"))
					logPass("New Project name  "+enteredprjct+" Entered");
				}
				else
			        logFail("new project name field not display");
			}
			
			public void entercreatedproject()
			{
				  if(driver.explicitWaitforVisibility(By.xpath("//*[text()='"+enteredprjct+"']"),"Create new project name"))
				    {
					  logPass("Create new project name "+enteredprjct+" display");
					  if (driver.jsClickElement(By.xpath("//*[text()='" + enteredprjct + "']/following::a[2]"),"Create new project name"))
							logPass("Create new project name "+enteredprjct+" Entered");
				    }
				    else
				    	logFail("Create new project name not display");
			}
			
			public void enterexistingproject()
			{
				String existingproject=properties.getProperty("projectname");
				  if(driver.explicitWaitforVisibility(By.xpath("//*[text()='"+existingproject+"']"),"Create new project name"))
				    {
					  logPass("Create new project name "+existingproject+" display");
				        if(driver.jsClickElement(By.xpath("//*[text()='"+existingproject+"']/following::a[2]"), "Create new project name"))
				        	logPass("Create new project name "+existingproject+" Entered");
				    }
				    else
				    	logFail("Create new project name not display");
			}
			
			public void enterDetailpage()
			{
			    if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='details-trigger\"']"), "chevron right"))
			    {
			        logPass("project chevron right display");
			        if(driver.jsClickElement(By.xpath("//*[@data-qa='details-trigger\"']"), "Click chevron right"));
			            logPass("project chevron right click");
			    }
			    else
			        logFail("project chevron right not display");
			}
			
			public void clickCreateButton()
			{
				if(driver.jsClickElement(merchandiseObject.createProject, "Clicking on Create button"))
					logPass("Clicked on project create button");
				else
					logFail("Error on creating project");	
			}
			
			public void successMsgOnProjectCreation()
			{
				String Successmsg=driver.getText(merchandiseObject.txtSuccessMsg, "Verifying Success Msg of Project Creation");
				logPass("Success Msg of Project Creation is:" +Successmsg);
				logInfo("Successful message is displayed");
			}
			
			public void cancelProjectCreation()
			{
				driver.explicitWaitforVisibility(merchandiseObject.btnCancel, "Cancel Button");
					logPass("Cancel Button is Present");
				driver.clickElement(merchandiseObject.btnCancel, "Click on Cancel button");
				if(driver.explicitWaitforInVisibility(merchandiseObject.createProject, "visibilty of create btn"))
					logPass("Project creation is cancelled");
				else
					logFail("There is a issue on project cancellation");
			}
			
			public void projectdropdownclick() throws InterruptedException
			{
			Thread.sleep(10000);
				driver.explicitWaitforVisibility(merchandiseObject.dropdownPrjct, "Project Dropdown");	
				if(driver.jsClickElement(merchandiseObject.dropdownPrjct, "Clicking on Project Dropdown"))
					logPass("Create New Project page popup is displayed");
				else
					logFail("Create New Project page popup is not displayed");
			}

			public void isDisplayedListofCategory() throws InterruptedException
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.listCategories, "Listing Categories"))
					logPass("List of Categories are displayed");
				else
					logFail("List of Categories are not displayed properly");
			}
			
			public void clickArrowIcon() {
				if(driver.jsClickElement(merchandiseObject.btnArrow, "Arrow Button is clicked"))
					logPass("Arrow is clicked");
				else
					logFail("Arrow Button is not Clickable");
			}
			
			public void editIconIsDisplayed() {
				if(driver.explicitWaitforVisibility(merchandiseObject.btnEdit, "Verifying the visibility of edit btn"))
					logPass("Edit is visible in edit category page");
				else
					logFail("Edit Button is not visible");
			}
			
			public void clickEditIcon() {
				if(driver.jsClickElement(merchandiseObject.btnEdit, "Edit Button is clicked"))
					logPass("Edit is clicked");
				else
					logFail("Edit Button is not Clickable");
			}
			
			public void randomlySelectCategory()
			{
				driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='categorylist-item']"), "Waiting to load the category list");
				int size = driver.getSize(By.xpath("//*[@data-qa='categorylist-item']"), "Status Options");
				int random=driver.generateRandomNumber(size);
				String mousehoveredon = driver.getText(By.xpath("(//*[@data-qa='categorylist-item'])["+random+"]//td[1]"), "Getting txt of prjct nme");
				if(driver.mouseHoverElement(By.xpath("(//*[@data-qa='categorylist-item'])"), "Randomly mousehovering the category"))
					logPass("MouseHovered on Category:" +mousehoveredon);
				else
					logFail("Issue on mousehovering");
				logInfo("MouseHovering on Category is done on:" +mousehoveredon);
			}
				
			public void clkChevron()
			{
				driver.explicitWaitforVisibility(merchandiseObject.iconGreaterthan, "Waiting to load Greaterthan icon");
				if(driver.jsClickElement(merchandiseObject.iconGreaterthan, "Clicking on Greaterthan icon"))
					logPass("Clicked on arrow icon");
				else
					logFail("There is a issue on clicking arrow icon");
			}
			
			public void deleteCategory()
			{
				driver.explicitWaitforVisibility(merchandiseObject.btnDelete, "Waiting to load delete btn");
				if(driver.jsClickElement(merchandiseObject.btnDelete, "Clicking on delete btn"))
					logPass("Delete Confirmation popup message is displayed");
				else
					logFail("Delete Confirmation popup message is not displayed");
			}
			
			public void clkYesFromDeletePopup()
			{
				if(driver.clickElement(merchandiseObject.delYesBtn, "Clicking on delete btn"))
						logPass("Clicked YES from delete popup");
					else
						logFail("Issue on deleting category");
			}
			
			public void verifyCategoryIsDeleted()
			{
					String DelSuccessmsg=driver.getText(merchandiseObject.successMsgOfDel, "Verifying Success Msg of Category deletion");
					logPass("Success Msg of Category Deletion is:" +DelSuccessmsg);
					logInfo("Successful message is displayed for Category Deletion");			
			}		
			
			public void clickCreateCategoryBtn() throws InterruptedException 
			{
				Thread.sleep(10000);
				driver.explicitWaitforVisibility(By.xpath("//a[contains(text(),'Create Category')]"), "Waiting to load create category button");
				if(driver.jsClickElement(By.xpath("//a[contains(text(),'Create Category')]"), "Clicking on Create Category button"))
					logPass("Clicked on Create Category");
				else
					logFail("There is a issue on navigating to create category page");
			}
			
			public void createCategoryIsDisplayed() throws InterruptedException 
			{
				Thread.sleep(10000);
				if(driver.explicitWaitforVisibility(By.xpath("//a[contains(text(),'Create Category')]"),"Waiting to load create category button"))
					logPass("Create btn is displayed in category listing page");
				else
					logFail("Create btn is not displayed in category listing page");
			}
		 
			public void categoryIdIsDisplayed() 
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.categoryID, "Verifying the Category ID field in create page"))	
					logPass("Category Id field is displayed");
				else
					logFail("Category Id field is not displayed");
			}
			
			public void enterCategoryId() 
			{
				String random=driver.generateRandomString();
				driver.enterText(merchandiseObject.categoryID, ExcelReader.getData("merchandiseadmin", "CategoryID")+random, "Entering Category ID");
				categoryid=ExcelReader.getData("merchandiseadmin", "CategoryID")+random;
				logPass("Text entered on Category ID Field is:" +categoryid);
			}
		
			public void categoryNameIsDisplayed() 
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.txtCategoryName, "Verifying the Category Name field in create page"))	
					logPass("Category Name field is displayed");
				else
					logFail("Category Name field is not displayed");
			}
			
			public void enterCategoryName() 
			{
				String random=driver.generateRandomString();
				driver.enterText(merchandiseObject.txtCategoryName, ExcelReader.getData("merchandiseadmin", "CategoryName")+random, "Entering Category Name");
				categoryname=ExcelReader.getData("merchandiseadmin", "CategoryName")+random;
				logPass("Text entered on Category Name Field is:" +categoryname);
			}
			
			public void categoryDescIsDisplayed()
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.txtDescription, "Verifying the Category Description field in create page"))	
					logPass("Category Description field is displayed");
				else
					logFail("Category Description field is not displayed");
			}
			
			public void enterCategoryDescription() 
			{
				driver.enterText(merchandiseObject.txtDescription, ExcelReader.getData("merchandiseadmin", "CategoryDescription"), "Entering Category Descriptin");
				String Description=ExcelReader.getData("merchandiseadmin", "Category_Description");
				logPass("Text entered on Category Name Field is:" +Description);
			}
			
			public void semanticIdIsDisplayed() 
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.txtSemanticID, "Verifying the Semantic ID field in create category page"))	
					logPass("Semantic Id field is displayed");
				else
					logFail("Semantic Id field is not displayed");
			}
			
			public void enterSemanticId() 
			{
				String random=driver.generateRandomString();
				driver.enterText(merchandiseObject.txtSemanticID, ExcelReader.getData("merchandiseadmin", "SemanticID")+random, "Entering Semantic ID");
				semanticid=ExcelReader.getData("merchandiseadmin", "Semantic_ID")+random;
				logPass("Text entered on Semantic ID Field is:" +semanticid);
			}
			
			public void metaTitleIsDisplayed() 
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.txtMetaTitle, "Verifying the Meta Title in create category page"))	
					logPass("MetaTitle field is displayed");
				else
					logFail("MetaTitle field is not displayed");
			}
			
			public void enterMetaTitle() 
			{
				driver.enterText(merchandiseObject.txtMetaTitle, ExcelReader.getData("merchandiseadmin", "Metatitle"), "Entering Meta Title");
				String metatitle=ExcelReader.getData("merchandiseadmin", "Metatitle");
				logPass("Text entered on Meta title Field is:" +metatitle);
			}
			
			public void metaDescIsDisplayed() 
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.txtMetaDesc, "Verifying the Meta Description in create category page"))	
					logPass("Meta Description field is displayed");
				else
					logFail("Meta Description field is not displayed");
			}
			
			public void enterMetaDescription() 
			{
				driver.enterText(merchandiseObject.txtMetaDesc, ExcelReader.getData("merchandiseadmin", "MetaDescription"), "Entering Meta Description");
				String metadesc=ExcelReader.getData("merchandiseadmin", "MetaDescription");
				logPass("Text entered on Meta Description Field is:" +metadesc);
			}
			
			public void metakeywordIsDisplayed() 
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.txtMetaKeywrd, "Verifying the Meta Keyword in create category page"))	
					logPass("Meta Keyword field is displayed");
				else
					logFail("Meta Keyword field is not displayed");
			}
			
			public void enterMetaKeyword() 
			{
				driver.enterText(merchandiseObject.txtMetaKeywrd, ExcelReader.getData("merchandiseadmin", "MetaKeyword"), "Entering MetaKeyword");
				String metakeyword = ExcelReader.getData("merchandiseadmin", "MetaKeyword");
				logPass("Text entered on Meta Keyword Field is:" +metakeyword);
			}
				
			public void addSemanticId()
			{
				if(driver.jsClickElement(merchandiseObject.btnAddSemanId, "Clicking Add btn of SemanticId"))
					logPass("Clicked on add button of semantic id");
					else
					logFail("Issue on clicking add button of semantic id");
			}
			
			public void addMetaTitle()
			{
				if(driver.jsClickElement(merchandiseObject.btnAddMetaTitle, "Clicking Add btn of Meta Title"))
					logPass("Clicked on add button of Meta Title");
					else
					logFail("Issue on clicking add button of Meta title");
			}
			
			public void addMetaDesc()
			{
				if(driver.jsClickElement(merchandiseObject.btnAddMetaDesc, "Clicking Add btn of Meta Description"))
					logPass("Clicked on add button of Meta Description");
					else
					logFail("Issue on clicking add button of Meta Description");
			}
			
			public void addMetaKeyword()
			{
				if(driver.jsClickElement(merchandiseObject.btnAddMetaKey, "Clicking Add btn of Meta Keyword"))
					logPass("Clicked on add button of Meta Keyword");
					else
					logFail("Issue on clicking add button of Meta Keyword");
			}
			
			public void mandatoryPropNameIsDisplayed()
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.mandPropName, "Verifying the visibility of Mandatory Property Name"))
					logPass("Mandatory property name is displayed");
				else
					logFail("Issue on displaying property name");
			}
			
			public void enterMandatoryPropValue()
			{
				if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='mandatory-prop-value']"), "Waiting to load the page for prop value"))
				{
				int size=driver.getSize(By.xpath("//*[@data-qa='mandatory-prop-value']"), "property list");
				for(int i=1;i<=size;i++)
				{
					driver.enterText(By.xpath("(//*[@data-qa='mandatory-prop-value'])["+i+"]"), "mandatorypropvalue", "Entering propvalue");
	  	 				logPass("Prop Value is entered");
	  	 			}				
			    }
			  }
			
			public void clickAddBtnProperty()
			{
				driver.jsClickElement(merchandiseObject.btnAddProperty, "Clicking on Add Button to add property");
				logPass("Property name and value fields are displayed");
			}
			
			public void clkPropertyName() 
			{
				if(driver.jsClickElement(merchandiseObject.propName, "Entering Property name"))
					logPass("Property Name  is clicked");
				else
					logFail("Property Name is not clickable");
			}
			
			public void selectPropertyName()
			{
				int size = driver.getSize(By.xpath("//*[@data-qa='optional-prop-name']//ul[1]/li[3]"), "property name Options");
				for(int i=1;i<=size;i++)
				{
				if(driver.jsClickElement(By.xpath("(//*[@data-qa='optional-prop-name']//ul[1]/li[3])["+i+"]"), "property option select"))
					logPass("Selected property name" );
				else
					logFail("Issue on selecting property name");
				break;
				}
			}
			
			public void enterOptionalPropertyValue() 
			{
				driver.enterText(merchandiseObject.propValue, ExcelReader.getData("merchandiseadmin", "Propval"), "Entering Property value");
				String propvalue = ExcelReader.getData("merchandiseadmin", "Propval");
				logPass("Property value is entered:" +propvalue);
			}
			
			public void clkCreatebtn()
			{
				driver.explicitWaitforVisibility(By.xpath("//*[@id='btnCreate']"), "Verifying the visibility of create category btn");
				if(driver.jsClickElement(By.xpath("//*[@id='btnCreate']"), "Clicking on Create Category buttton"))
					logPass("Clicked on Create Category Button");	
				else
					logFail("Issue on clicking create category button");
			}
			
			public void successmsgofcategorycreation()
			{
				String Successmsg=driver.getText(By.xpath("//*[@data-qa='create-category-success']"), "Verifying Success Msg of category Creation");
				logPass("Success Msg of Category Creation is:" +Successmsg);
				logInfo("Successful message is displayed");
			}
			
			public void cancelCategory()
			{
				if(driver.jsClickElement(By.xpath("//*[@id='btnCancel']"), "Clicking on Cancel buttton"))
					logPass("Clicked on cancel Button");
				else
					logFail("Issue on cancelling the category creation");
			}
			
			public void propNameIsMandatory()
			{//*[@data-qa='category-id']/following::label[1]
				if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='mandatory-prop-name']/following::label[1]"), "Waiting to load the page for prop name"))
				{
				int size=driver.getSize(By.xpath("//*[@data-qa='mandatory-prop-name']/following::label[1]"), "project List");
				for(int i=1;i<=size;i++)
				{
					String propname=driver.getText(By.xpath("(//*[@data-qa='mandatory-prop-name']/following::label[1])["+i+"]"), "Getting text of prop name");
	  	 			if(propname.contains("*"))
	  	 			{
	  	 				logPass("Prop Name field is Mandatory:" +propname);
	  	 			logInfo("Prop Name field is Mandatory:" +propname);	
	  	  	 			break;
	  	 			}				
				}
				
			    }
			}
			
			public void propValueIsMandatory()
			{
				if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='mandatory-prop-value']/following::label[1]"), "Waiting to load the page for prop value"))
				{
				int size=driver.getSize(By.xpath("//*[@data-qa='mandatory-prop-value']/following::label[1]"), "property list");
				for(int i=1;i<=size;i++)
				{
					String propvalue=driver.getText(By.xpath("(//*[@data-qa='mandatory-prop-value']/following::label[1])["+i+"]"), "Getting text of prop value");
	  	 			if(propvalue.contains("*"))
	  	 			{
	  	 				logPass("Prop Value field is Mandatory:" +propvalue);
	  	 			logInfo("Prop Value field is Mandatory" +propvalue);	
	  	  	 			break;
	  	 			}				
				}
				
			    }
			}
			
			public void categoryIdIsMandatory()
			{
				if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='category-id']/following::label[1]"), "Waiting to load the page for Category ID"))
				{
					String catid = driver.getText(By.xpath("//*[@data-qa='category-id']/following::label[1]"), "Getting text of Category ID");
					if(catid.contains("*"))
					{
						logPass("Category ID is Mandatory Field:" +catid);
						logInfo("Category ID field is Mandatory:" +catid);
					}	
				}
			}	
			

			public void clkEditIcon()
			{
				driver.clickElement(merchandiseObject.editIcon, "Clicking on edit icon");
				logPass("Clicked on edit icon");
			}
			
			public void clkCategoryStatusInEdit()
			{
				if(driver.clickElement(merchandiseObject.statusedit, "Clicking on Category Status field in create page"))
					logPass("Category Status field is clicked");
				else
					logFail("Category Status field is not clickable");
			}
				
			public void updateStatusToInactive() 
			{
				if(driver.jsClickElement(merchandiseObject.statusActive, "Clicking on active status"))
					logPass("Value of status is updated as Inactive");
				else
					logFail("Value of status is not updated as Inactive");
			}
			
			public void clkValueIsInactiveInEdit() 
			{
				if(driver.clickElement(merchandiseObject.statusInactive, "Clicking the value of status is selected as Inactive"))
				if(driver.explicitWaitforVisibility(merchandiseObject.statusInactive, "Verifying the value of status is selected as Inactive"))
					logPass("Value of status is selected as Inactive");
				else
					logFail("Value of status is not selected as Inactive");
			}
			

			public void clkcategoryTypeInEdit()
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.dropdownType, "Verifying Category Type field in create page"))
					logPass("Category Type field is displayed");
				else
					logFail("Category Type field is not displayed");
			}
			
					
			public void verifyDefaultValueStaticInEdit() 
			{
				if(driver.explicitWaitforVisibility(merchandiseObject.typeStatic, "Verifying the default value of type is selected as Static"))
					logPass("Default value of Type is selected as Static");
				else
					logFail("Default value of Type is not selected as Static");
			}
			
			public void updateTypeInEdit() 
			{
				if(driver.clickElement(By.xpath("//*[@data-qa='category-type']//ul//li[2]"), "Clicking the value of status is selected as Inactive"))
					logPass("Value of Type is updated");
				else
					logFail("Value of status is not updated");
			}
					
			public void clkCategoryIsVisibleInEdit()
			{
				if(driver.clickElement(merchandiseObject.dropdownIsVisible, "Clicking on IsVisible field in create page"))
					logPass("IsVisible field is displayed");
				else
					logFail("IsVisible field is not displayed");
			}
			
			public void updateIsVisibleValueInEdit() 
			{
				if(driver.clickElement(By.xpath("//*[@data-qa='category-is-visible']//ul//li[2]"), "updating isvisible value"))
					logPass("Isvisible value is updated");
				else
					logFail("Value of Isvisible is not updated");
			}
			
			public void deleteProperty()
			{
				int size=driver.getSize(By.xpath("//*[@data-qa='btn-delete-property']"), "property list to delete");
				for(int i=1;i<=size;i++)
				{
					String del=driver.getText(By.xpath("(//*[@data-qa='btn-delete-property'])["+i+"]"), "Getting text");
	  	 			if(!del.contains("*"))
	  	 			{
	  	 				driver.jsClickElement(By.xpath("//*[@data-qa='btn-delete-property']"), "Clicking on delete button");
	  	 				logPass("Clicked on delete icon");
	  	 				logInfo("Clicked on delete icon");	
	  	  	 			break;
	  	 			}				
				}
				
			 }
			
			public void clkEditCategory() throws InterruptedException
			{
				Thread.sleep(10000);
				driver.explicitWaitforVisibility(merchandiseObject.btnEdit, "Waiting to load Edit button");
				if(driver.clickElement(merchandiseObject.btnEdit, "Clicking on edit btn"))
					logPass("User navigated to edit category page");
				else
					logFail("Issue on navigating to edit category page");
			}
			
			public void saveCategory()
			{
				if(driver.jsClickElement(merchandiseObject.btnSave, "Clicking on Save Category buttton"))
					logPass("Clicked on Save Category Button");
				else
					logFail("Issue on saving category");
			}
			
			public void successmsgofcategoryupdate()
			{
				String Successmsg=driver.getText(By.xpath("//*[@data-qa='update-category-success']"), "Verifying Success Msg of category Creation");
				logPass("Success Msg of Category Creation is:" +Successmsg);
				logInfo("Successful message is displayed");
			}
			
			public void clkCancelCatCreation()
			{
				driver.explicitWaitforVisibility(merchandiseObject.cancelInEdit, "Verifying the visibility of cancelbtn in creation");
				if(driver.jsClickElement(merchandiseObject.cancelInEdit, "Clicking on Cancel buttton"))
					logPass("Clicked on cancel Button");
				else
					logFail("Issue on cancelling the category creation");
			}
			
			public void clkDetailOfOpen() {
				driver.explicitWaitforVisibility(By.xpath("(//*[@data-qa='project-Open-item'])"), "Waiting to load the page for status");
				int size=driver.getSize(By.xpath("(//*[@data-qa='project-Open-item'])"), "project List");
				for(int i=1;i<=size;i++)
				{
					String OpenStatus=driver.getText(By.xpath("(//*[@data-qa='project-Open-item'])["+i+"]"), "Getting text of status");
	  	 			if(OpenStatus.equalsIgnoreCase("OPEN"))
	  	 			{
	  	 				logPass("Projects are listed with Open Status");
	  	 				driver.mouseHoverElement(By.xpath("(//*[@data-qa='project-Open-item'])["+i+"]"), "MouseHovering on Project");
	  	  	 			if(driver.jsClickElement(By.xpath("(//*[@data-qa='details-trigger\"'])["+i+"]"), "Clicking on Details Button"))
	  	  	 				logPass("Navigated to Project Details page");
	  	  	 			else
	  	  	 				logFail("There is a issue on navigating to Project Details page");	
	  	  	 			logInfo("Navigated to Project Detail Page"); 
	  	  	 			break;
	  	 			}				
				}
			}	
				
			public void editProjectName()
				{
					driver.explicitWaitforVisibility(merchandiseObject.txtPDPProjectName, "Waiting to load project name field in pdp");
					String editedprjctname=ExcelReader.getData("merchandiseadmin", "String 2")+" "+driver.generateRandomNumber(10000);
					if(driver.enterText(merchandiseObject.txtPDPProjectName, editedprjctname, "new project name"))
					logPass("Updated profile name in Project detail page:" +editedprjctname); 
				}	
			
				
			public void submitBtn()
				{
					driver.jsClickElement(merchandiseObject.btnSubmit, "Submitting Project");
					logPass("Submitted the project in Project detail page"); 
				}		
				
			public void isDisplayedApproveBtn()
				{
					driver.explicitWaitforVisibility(merchandiseObject.btnApprove, "Waiting to load the Approve button");
					logPass("Approve btn is displayed");
				}
			
			
			public void approveBtn()
				{
					driver.jsClickElement(merchandiseObject.btnApprove, "Project Approved");
					logPass("Approved the project in Project detail page"); 
				}
				
			public void refreshpage()
				{
					driver.refreshPage();
				}
			
			public void editProjectDescription()
				{
					driver.jsClickElement(merchandiseObject.editProject, "Clicking on Edit Description button in PDP");
					driver.enterText(merchandiseObject.prjctDescription, ExcelReader.getData("merchandiseadmin", "ProjectDesc"), "Entering text on Description field");
					logPass("Text entered on Description");
				}
			
			public void clickSavePDP()
				{		
					if(driver.clickElement(merchandiseObject.btnSaveProjectdetails, "SaveProjectDetails"))
						logPass("Project Updated Successfully");
					else
						logFail("There is a issue on updating Project details");
				}
			
			public void clkEnterBtn()
			{
				if(driver.jsClickElement(By.xpath("//*[@data-qa='enter-trigger']"), "Clicking enter btn"))
	 					logPass("Enter button is displayed");
	  	 			else
	  	 				logFail("There is a issue on enter button");	
			}
			
			public void enterPrjct() 
			{
				driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='project-Open-item']"), "Waiting to load the page for status");
				int size=driver.getSize(By.xpath("//*[@data-qa='project-']"), "project List");
				for(int i=1;i<=size;i++)
				{
					String OpenStatus=driver.getText(By.xpath("(//*[@data-qa='project-Open-item'])["+i+"]"), "Getting text of status");
	  	 			if(OpenStatus.equalsIgnoreCase("OPEN"))
	  	 			{
	  	 				logPass("Projects are listed with Open Status");
	  	 				driver.mouseHoverElement(By.xpath("(//*[@data-qa='project-Open-item'])["+i+"]"), "MouseHovering on Project");
	  	 				if(driver.jsClickElement(By.xpath("//*[@data-qa='enter-trigger']"), "Clicking enter btn"))
	  	 					logPass("Enter button is displayed");
	  	  	 			else
	  	  	 				logFail("There is a issue on enter button");	
	  	  	 			logInfo("Enter button is displayed"); 
	  	  	 			break;
	  	 			}				
				}
			}
			
			public void selectExisitngPrjct() 
			{
				driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='project-name-value']"), "Waiting to load the page for status");
				int size=driver.getSize(By.xpath("//*[@data-qa='project-name-value']"), "project List");
				for(int i=1;i<=size;i++)
				{
					String prjct = driver.getText(By.xpath("(//*[@data-qa='project-name-value'])["+i+"]"), "Getting text of status");
					if(prjct.equalsIgnoreCase("exisitingprjct"))
	  	 			{
	  	 				logPass("Project is displayed");
	  	 				driver.mouseHoverElement(By.xpath("(//*[@data-qa='project-name-value'])["+i+"]"), "MouseHovering on Project");
	  	 				if(driver.jsClickElement(By.xpath("//*[@data-qa='enter-trigger']"), "Clicking enter btn"))
	  	 					logPass("Enter button is displayed");
	  	  	 			else
	  	  	 				logFail("There is a issue on enter button");	
	  	  	 			logInfo("Enter button is displayed"); 
	  	  	 			break;
	  	 			}				
				}
			}
			
			public void isDisplayedDetailBtn() {
				driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='project-Open-item']"), "Waiting to load the page for status");
				int size=driver.getSize(By.xpath("//*[@data-qa='project-Open-item']"), "project List");
				for(int i=1;i<=size;i++)
				{
					String Status=driver.getText(By.xpath("(//*[@data-qa='project-Open-item'])["+i+"]"), "Getting text of status");
	  	 			if(Status.equalsIgnoreCase("OPEN") ||Status.equalsIgnoreCase("Approved") || Status.equalsIgnoreCase("Submitted") || Status.equalsIgnoreCase("Denied"))
	  	 			{
	  	 				logPass("Projects are listed with respective Status");
	  	 				driver.mouseHoverElement(By.xpath("(//*[@data-qa='project-Open-item'])["+i+"]"), "MouseHovering on Project");
	  	 				if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='details-trigger\"']"), "Verifying detail btn"))
	  	 					logPass("Detail button is displayed for all status");
	  	  	 			else
	  	  	 				logFail("There is a issue on enter button");	
	  	  	 			logInfo("Detail button is displayed for all status"); 
	  	  	 			break;
	  	 			}				
				}
			}	 
}