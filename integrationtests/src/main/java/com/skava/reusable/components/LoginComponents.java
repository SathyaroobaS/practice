package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.framework.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.AccountAdminObjects;
import com.skava.object.repository.LogIn_Page;
import com.skava.object.repository.LoginPage;
import com.skava.object.repository.LoginPageObject;
import com.skava.object.repository.Login_8;

public class LoginComponents extends GeneralComponents 
{
	public void enterLoginCredentials()
	{
		String userName = properties.getProperty("LoginUserName");
		if(driver.enterText(LogIn_Page.txtbx_UserName, userName ,"Username"))
		{
			logInfo("The user entered login name is : " + userName);
			String userPassword = properties.getProperty("LoginPassword");
			if(driver.enterText(LogIn_Page.txtbx_Password, userPassword ,"Password"))
			{
				logInfo("The user entered login password is : " + userPassword);
				if(driver.jsClickElement(LogIn_Page.btn_LogIn,"Login button"))
				{
					logPass("The user was able to click on the Login Button.");
				}
				else
				{
					logFail("The user failed to click on the Login Button.");
					skipException("Failing the Script Intentionally as the user failed to click on the login Button.");
				}
			}
			else
			{
				logFail("The user failed to enter the enter the password in the password field.");
				skipException("Failing the Script Intentionally as the user failed to enter the password on the login screen.");
			}
		}
		else
		{
			logFail("The user failed to enter the enter the text in the User name field.");
			skipException("Failing the Script Intentionally as the user failed to enter the username on the login screen.");
		}
	}

	public void BusinessAdminLogout()
	{
		try {
			Thread.sleep(3000);
			if(driver.moveToElementAndClick(LoginPageObject.logout_drpdown, "Logout Dropdown"))
			{
				logPass("Dropdown is clicked to logout");
				// Click on logout
				Thread.sleep(3000);
				if(driver.jsClickElement(LoginPageObject.logout_btn, "Logout"))
				{
					logPass("Logout button is clicked");				
				}
				else
				{
					logFail("Logout button is not clicked");
				}
			}
			else
			{
				logFail("Dropdown is not clicked to logout");
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			logFail("Dropdown is not clicked to logout");
		}
		
	}
	
	public void verifyLoginSuccessful()
	{
		if(driver.compareElementText(LoginPage.lblSignInSuccess, ExcelReader.getData("General_Data","logInSuccessMessage"), "Login Success Message"))
			logPass("User sucessfully logged in");
		else
			logFail("User login failed");
	}
	
	/*public void closeLoginPopUp()
	{
		driver.clickElement(LoginPage.loginPopUpClose, "PopUpCloseIcon");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Login popup closed");
	}*/
	
	
	
	
	/*
	 * Account admin login methods by Muthuraghavendra R S
	 */
	
	public void login() throws InterruptedException
	{
        if(driver.explicitWaitforClickable(AccountAdminObjects.signInUsernameTextbox,"username text box"))
        {
            driver.enterText(AccountAdminObjects.signInUsernameTextbox,"superadmin@skava.com","username text box");
            driver.enterText(AccountAdminObjects.signInPasswordTextbox,"Skava@123","password text box");
            driver.jsClickElement(AccountAdminObjects.signInSubmitBtn,"Submit btn"); 
            if(driver.explicitWaitforVisibility(AccountAdminObjects.pageload,"Page load"))
                pass("Login successful ");
            else
                fail("Login failed");
        }
        else
            fail("Login page not displayed");
	}
	
	public void businesssearch()
	{
        if(driver.explicitWaitforClickable(AccountAdminObjects.businesssearchtext,  "business name textbox"))
        {
        	driver.jsClickElement(AccountAdminObjects.businesssearchtext, "business name textbox");
        	driver.enterText(AccountAdminObjects.businesssearchtextbox,properties.getProperty("BusinessId"), "businessname");
        	driver.jsClickElement(AccountAdminObjects.businesssearchbtn, "businesssearchbtn");	
        }
        if(driver.explicitWaitforClickable(AccountAdminObjects.businessitemname, "Business item name"))
        {
        	if(driver.jsClickElement(AccountAdminObjects.businessitemname, "businessitemname"))
        		pass("Business clicked");
        	else
        		fail("Business not clicked");
        }
	}
	
	/*
	 * Account admin components ends
	 */
	//Paramesh
	
	/** Verifying ForgotPassword? button **/

	public void forgotPasswordButton() {
		if(driver.isElementDisplayed(Login_8.forgot_password,  "Forgot Password button?"))
			logPass("Forgot Password? button is Present");
		else
			logFail("Failed to display Forgot Password?");
	}
}
