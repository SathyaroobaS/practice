package com.skava.reusable.components;

import org.apache.http.client.ClientProtocolException;
import org.apache.xml.resolver.Catalog;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.framework.reporting.BaseClass;
import com.framework.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.frameworkutils.ExcelReader;
import com.skava.networkutils.ApiReaderUtils;
import com.skava.object.repository.BusinessPageObject;
import com.skava.object.repository.CatalogPageObject;
import com.skava.object.repository.StoresObject;


public class CatalogComponents extends StoresComponents
{	
	String FinalBundleId;
	long cat_random; String EditedNameUI; String AddedNameUI; String get_skuid;
	
	/*
	 * Get current timestamp
	 */
    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:ms");
    public long currentTimeStamp()
    {
    	Date date= new Date();
        long TimeStamp = date.getTime();
        return TimeStamp;
    }
    
    /*
	* Verify that Attributes Tab is clicked and The Attributes page is displayed
	*/
	public void verifyAttributesPageIsDisplayed()
	{
		if(driver.isElementDisplayed(CatalogPageObject.Attributes_Tab, "Attributes Tab"))
		{
			logPass("The Attribute Tab is displayed");
			if(driver.jsClickElement(CatalogPageObject.Attributes_Tab, "Attributes Tab"))
			{
				logPass("The Attribute Tab is clicked");
				
				if(driver.isElementDisplayed(CatalogPageObject.Attributes_Title, "Attributes Title"))
				{
					logPass("The Attributes Page is displayed");
				}
				else
				{
					logFail("The Attributes Page is displayed");
				}
			}
			else
			{
				logFail("The Attribute Tab is not clicked");
			}
		}
		else
		{
			logPass("The Attribute Tab is not displayed");
		}
	}
	
	/*
	* Verify that Attributes Tab is clicked and The Attributes page is displayed
	*/
	public void clickCreateAttributeBtn()
	{
		if(driver.jsClickElement(CatalogPageObject.Create_Attribute_Btn, "Create Attribute Button"))
		{
			logPass("The Create Attribute Button is clicked");
			
			if(driver.isElementDisplayed(CatalogPageObject.Attributes_Title, "Attribute Creation Title"))
			{
				logPass("The Attribute Creation Page is displayed");
				
				String attributeName = ExcelReader.getData("CatalogPage", "Data_1");
				enterAttributeName("Attribute", attributeName);
				
				/*String attributeId = ExcelReader.getData("CatalogPage", "Data_2");
			    enterAttributeId("Attribute", attributeId);*/
				int randomnumber=driver.generateRandomNumber(1000);
		        String attributeId = ExcelReader.getData("CatalogPage", "Data_2");
		        String FinalAttrId = attributeId+randomnumber+currentTimeStamp();
		        enterAttributeId("Attribute", FinalAttrId);
			}
			else
			{
				logFail("The Attribute Creation Page is not displayed");
			}
		}
		else
		{
			logFail("The Create Attribute Button is not clicked");
		}
	}
	
	/*
	* Enter the Attributes Name
	*/
	public void enterAttributeName(String Type, String attributeName)
	{
		try
		{
			By attributeXpath = null;
			switch(Type)
			{
				case "Attribute":
					attributeXpath = CatalogPageObject.Attribute_Name_Input;
					break;
					
				case "Attribute Group":
					attributeXpath = CatalogPageObject.Attribute_Group_Name;
					break;
					
				case "Variant Group":
					attributeXpath = CatalogPageObject.Attribute_Group_Name;
					break;
			}
		
			if(driver.enterText(attributeXpath, attributeName, Type))
				{
					logPass(Type + " Name is entered. The entered " + Type + " Attribute Name is : " + attributeName);
				}
				else
				{
					logInfo("Attributes are not available to enter: " + Type + " Name");	
				}
		} 
		catch (Exception e)
		{
			logFail("There is some issue in entering the " + Type + " Name. The Error is : " + e.getMessage());
		}
	}
	
	/*
	* Enter the Attributes Id
	*/
	public void enterAttributeId(String Type, String attributeId)
	{
		try
		{
			By attributeIdXpath = null;
			switch(Type)
			{
				case "Attribute":
					attributeIdXpath = CatalogPageObject.Attribute_Id_Input;
					break;
					
				case "Attribute Group":
					attributeIdXpath = CatalogPageObject.Attribute_Group_Order_Id;
					break;
					
				case "Variant Group":
					attributeIdXpath = CatalogPageObject.Attribute_Group_Order_Id;
					break;
			}
			
			if(driver.enterText(attributeIdXpath, attributeId, Type + " Id"))
			{
				logPass(Type + " Id is entered. The entered " + Type + " Id is : " + attributeId);
			}
			else
			{
				logFail("The user failed to enter the " + Type + " Id");
			}
		} 
		catch (Exception e)
		{
			logFail("There is some issue in entering the " + Type + " Id. The Error is : " + e.getMessage());
		}
	}
    
	/*
	* Enter the Field Type
	*/
	public void selectFieldTypeFromDropdown()
	{
		try
		{
			String random_FieldType = "";
			By dropdown_Selection = null, minValueXpath = null, maxValueXpath = null;
			if(driver.jsClickElement(CatalogPageObject.Field_Type_Input, "Field Type"))
			{
				logPass("Field Type dropdown is clicked");
				int random_Field_Number = driver.generateRandomNumberWithLimit(2, 1);
				
				switch (random_Field_Number) 
				{
					case 1:
						random_FieldType = "Number";
						dropdown_Selection = CatalogPageObject.Field_Type_Number_List;
						minValueXpath = CatalogPageObject.Min_Number_Input;
						maxValueXpath = CatalogPageObject.Max_Number_Input;
						break;
						
					case 2:
						random_FieldType = "String";
						dropdown_Selection = CatalogPageObject.Field_Type_String_List;
						minValueXpath = CatalogPageObject.Min_String_Input;
						maxValueXpath = CatalogPageObject.Max_String_Input;
						break;
				}
				
				if(driver.isElementDisplayed(dropdown_Selection, "Attributes Tab"))
				{
					logPass("The Randomly selected Field Type dropdown value is displayed. The Selected Dropdown value is : " + random_FieldType);
					
					if(driver.clickElement(dropdown_Selection, "Attributes Tab"))
					{
						logPass("The Randomly selected Field Type dropdown value is clicked");
						
						//Thread.sleep(3000);
						if(driver.explicitWaitforVisibility(CatalogPageObject.Attributes_Title, "Attributes Title"))
						{
						String minNumber = ExcelReader.getData("CatalogPage", "Data_3");
						String maxNumber = ExcelReader.getData("CatalogPage", "Data_4");
						
						//Thread.sleep(4000);
						enterValidationTypeDetails(minValueXpath, maxValueXpath, minNumber, maxNumber);
						}
						else
							logFail("Attribute values are not entered");
					}
					else
					{
						logFail("The Randomly selected Field Type dropdown value is not clicked");
					}
				}
				else
				{
					logFail("The Randomly selected Field Type dropdown value is not displayed.  The Selected Dropdown value is : " + random_FieldType);
				}
			}
			else
			{
				logFail("Field Type dropdown is not clicked");
			}
		} 
		catch (Exception e)
		{
			logFail("There is some issue in clicking the Field Type dropdown. The Error is : " + e.getMessage());
		}
	}
    
	/*
	* Enter the validation Type Details Min & Max Number
	*/
	public void enterValidationTypeDetails(By minValueXpath, By maxValueXpath, String minNumber, String maxNumber)
	{
		try
		{
			Thread.sleep(3000);
			if(driver.enterText(minValueXpath, minNumber, "Min Number"))
			{
				logPass("Min Number is entered. The entered Min Number is : " + minNumber);
			}
			else
			{
				logFail("The user failed to enter the Min Number");
				
			}
			
			Thread.sleep(3000);
			if(driver.enterText(maxValueXpath, maxNumber, "Max Number"))
			{
				logPass("Max Number is entered. The entered Max Number is : " + minNumber);
			}
			else
			{
				logFail("The user failed to enter the Max Number");
				
			}
		} 
		catch (Exception e)
		{
			logFail("There is some issue in entering the Min and Max Number. The Error is : " + e.getMessage());
		}
	}
	
	/*
	 *************  @Author - Priya
	 */
    
	/*
	 *  Click on master catalog to create project
	 */
	public void EnterIntoMasterCatalog()
	{
		if(driver.clickElement(CatalogPageObject.Enter_Mastercatalog, "Master Catalog"))
		{
			logPass("Navigated to master catalog page");
		}
		else
		{
			logFail("Not Navigated to master catalog page");
		}
	}	
	
	/*
	 *  Click on projects dropdown and select the project
	 */
	public void ClickProjectsDropdownAndSelectProject()
	{
		if(driver.jsClickElement(CatalogPageObject.Projects_Dropdown, "Projects Dropdown"))
		{
			logPass("Projects Dropdown is clicked");
			// Verify project over lay is displaying 
			if(driver.isElementDisplayed(CatalogPageObject.Project_Overlay, "Project Overlay"))
			{
				logPass("Project Overlay is displayed");
				// Verify existing project overlay
				if(driver.isElementDisplayed(CatalogPageObject.Existing_project_Row, "Existing Project Row"))
				{
					logPass("Existing project overlay is displaying");
					// select the existing project
					if(driver.jsClickElement(CatalogPageObject.Project_Enter, "Enter Project"))
					{
						logPass("Project is selected");
					}
					else
					{
						logFail("Project is not selected");
					}
				}
				else
				{
					logFail("Existing project overlay is not displaying");
				}
			}
			else
			{
				logFail("Project Overlay is not displayed");
			}
		}
		else
		{
			logFail("Projects Dropdown is not clicked");
		}
	}
	
	
	/*
	*  Verify pancake click
	*/
	public void PancakeClick()
	{
		if(driver.isElementDisplayed(CatalogPageObject.Pancake, "Pancake"))
		{
			logPass("Pancake is displayed");
			// pancake click
			if(driver.moveToElementAndClick(CatalogPageObject.Pancake, "Pancake"))
			{
				logPass("Pancake is clicked");
			}
			else
			{
				logFail("Pancake is not clicked");
			}
		}
		else
		{
			logFail("Pancake is displayed");
		}
	}
	
	/*
	 *  Click on bundles tab after creating bundle
	 */
	
	public void ClickonBundlesTab()
	{
		try {
				if(driver.explicitWaitforVisibility(CatalogPageObject.Bundles_Tab, "Bundles Tab"))
				{
					if(driver.isElementDisplayed(CatalogPageObject.Bundles_Tab, "Bundles Tab"))
					{
						logPass("Bundles Tab is displayed");
						// pancake click
						if(driver.moveToElementAndClick(CatalogPageObject.Bundles_Tab, "Bundles Tab"))
						{
							logPass("Bundles Tab is clicked");
						}
						else
						{
							logFail("Bundles Tab is not clicked");
						}
					}
				}
			
		} catch (Exception e) {
			// TODO: handle exception
			logFail("Bundles Tab is displayed");
		}
		
	}
	
	/*
	 *  Click on add a button
	 */
	public void ClickonAddProductButton() throws InterruptedException
	{
		try {
			if(driver.explicitWaitforVisibility(CatalogPageObject.Add_Product_btn, "Add Product button"))
			{
			if(driver.jsClickElement(CatalogPageObject.Add_Product_btn, "Add Product button"))
			{
				logPass("Add Product button is clicked");
			}
			}
		} catch (Exception e) {
			// TODO: handle exception
			logFail("Add Product button is not clicked");
		}
			
		}
	
	public void ClickonAddProductButton2() throws InterruptedException
	{
		try {
			if(driver.explicitWaitforVisibility(CatalogPageObject.Add_Product_btn, "Add Product button"))
			{
				driver.refreshPage();
			if(driver.jsClickElement(CatalogPageObject.Add_Product_btn, "Add Product button"))
			{
				logPass("Add Product button is clicked");
			}
			}
		} catch (Exception e) {
			// TODO: handle exception
			logFail("Add Product button is not clicked");
		}
			
		}
	
	/*
	 *  Select products tab from the side menu
	 */
	
	public void SelectProductsFromPancake()
	{
		if(driver.isElementDisplayed(CatalogPageObject.ProductsTab, "Product"))
		{
			logPass("Product tab is displayed");
			// Click product
			if(driver.jsClickElement(CatalogPageObject.ProductsTab, "Product Tab"))
			{
				logPass("Product Tab is clicked");
			}
			else
			{
				logFail("Product Tab is not clicked");
			}
		}
		else
		{
			logFail("Product tab is not displayed");
		}
	}
	
	/*
	 *  Click on create item button to select bundle
	 */
	
	public void ClickonCreateItemButton()
	{
		if(driver.isElementDisplayed(CatalogPageObject.CreateItem_Btn, "Create Item"))
		{
			logPass("Product tab is displayed");
			// Click product
			if(driver.jsClickElement(CatalogPageObject.CreateItem_Btn, "Create Item Button"))
			{
				logPass("Create Item button is clicked");
			}
			else
			{
				logFail("Create Item button is not clicked");
			}
		}
		else
		{
			logFail("Product tab is not displayed");
		}
	}
	
	/*
	 *  Select bundle from the dropdown
	 */
	public void AddBundledItems()
	{
		try {
			String Bundleid = ExcelReader.getData("CatalogPage", "BundleID");
			String Bundlename = ExcelReader.getData("CatalogPage", "BundleName");
			if(driver.isElementDisplayed(CatalogPageObject.AddItemOverlay, "Add Item Overlay"))
			{
				logPass("Add Item Overlay is displayed");
				//Select bundle from dropdown
				try {
					if(driver.moveToElementAndClick(CatalogPageObject.Itemddropdown_Select, "Item Dropdown Select"))
					{
						logPass("Type dropdown is clicked");
						// Select Bundle option from the dropdown
						try {
							if(driver.jsClickElement(CatalogPageObject.Bundle_Options, "Bundle"))
							{
								logPass("Bundle is selected from the dropdown");
								AddBundleIdRandomly(Bundleid);
								AddBundleNameRandomly(Bundlename);
								AddDetailsButton();
							}
						} catch (Exception e) {
							logFail("Bundle is not selected from the dropdown");
							// TODO: handle exception
						}
						
					}
				} catch (Exception e) {
					// TODO: handle exception
					logFail("Type dropdown is not clicked");
				}
				
				
			}
		} catch (Exception e) {
			// TODO: handle exception
			logFail("Add Item Overlay is not displayed");
		}
		
	}
	
	
	/*
	 *  Add ID randomly
	 */
	
	public void AddBundleIdRandomly(String Bundleid)
	{
		// ||||||||||||||||||||||||||||| Add Bundle ID Randomly |||||||||||||||||||||||||||||
		 int randomnumber=driver.generateRandomNumber(1000);
	     FinalBundleId = Bundleid+randomnumber+currentTimeStamp();
        if(driver.enterText(CatalogPageObject.BundleId_txtbox,FinalBundleId,"Bundle ID"))
		{
			logPass("Bundle ID is entered");
			logInfo("Bundle ID is :" +FinalBundleId);
		}
		else
		{
			logFail( "Bundle ID is not entered");
		}
	}
	
	/*
	 *  Add Name Randomly 
	 */
	public void AddBundleNameRandomly(String Bundlename)
	{
		// ||||||||||||||||||||||||||||| Add Bundle Name Randomly |||||||||||||||||||||||||||||
		
        int randomnumber=driver.generateRandomNumber(1000);
        String FinalBundleName = Bundlename+randomnumber+currentTimeStamp();
		
        if(driver.enterText(CatalogPageObject.BundleName_txtbox,FinalBundleName,"Bundle Name"))
		{
			logPass("Bundle Name is entered");
			logInfo("Bundle Name is :" +FinalBundleName);
		}
		else
		{
			logFail( "Bundle Name is not entered");
		}
	}
	
	/*
	 *  Add details button click
	 */
	public void AddDetailsButton()
	{
		try {
			if(driver.clickElement(CatalogPageObject.Add_details_Btn, "Add Details Button"))
			{
				logPass("Add Details Button is clicked");
			}
		} 
		catch (Exception e) {
			logFail("Add Details Button is not clicked");
			// TODO: handle exception
		}
				
	}
	
	/*
	 *  Save the edited bundle details
	 */
	public void SaveEditedBundleDetails()
	{
		try {
			if(driver.explicitWaitforVisibility(CatalogPageObject.Edit_Bundle_Save, "Save button"))
			{
			if(driver.clickElement(CatalogPageObject.Edit_Bundle_Save, "Save button"))
			{
				logPass("Edit bundle save button is clicked");
			}
			else
			{
				logFail("Edit bundle save button is not clicked");
			}
			}
		} catch (Exception e)
		{
			// TODO: handle exception
			logFail("Edit bundle save button is not clicked");
		}
		
		
	}
	
	/*
	 *  Add approved products into the bundle
	 */
	public void AddApprovedProductToBundle()
	{
		if(driver.explicitWaitforVisibility(CatalogPageObject.Item_List_Container, "Approved product list"))
		{
			/*if(driver.isElementDisplayed(CatalogPageObject.Item_List_Container, "Approved product list"))
			{
				logPass("Approved product list is available");*/
				if(driver.jsClickElement(CatalogPageObject.Add_product_btn, "Add Product button"))
				{
					logPass("Approved product is clicked");
					// Close Overlay
					if(driver.jsClickElement(CatalogPageObject.Close_overlay, "Overlay Close"))
					{
						logPass("Overlay is closed");
					}
					else
					{
						logFail("Overlay is not closed");
					}
				}
				/*else
				{
					logInfo("Approved product is not available to add");
				}
			}*/
			else
			{
				logInfo("Approved product list is not available for current selection");
			}
		}
		
	}
	
	/*
	 *  Add approved products into the bundle
	 */
	public void AddApprovedProductToBundle2()
	{
		try {
			if(driver.explicitWaitforVisibility(CatalogPageObject.Item_List_Container, "Approved product list"))
			{
			if(driver.isElementDisplayed(CatalogPageObject.Item_List_Container, "Approved product list"))
			{
				logPass("Approved product list is available");
				if(driver.jsClickElement(CatalogPageObject.Add_product_btn2, "Add Product button"))
				{
					logPass("Approved product is clicked");
					// Close Overlay
					if(driver.jsClickElement(CatalogPageObject.Close_overlay, "Overlay Close"))
					{
						logPass("Overlay is closed");
					}
					else
					{
						logFail("Overlay is not closed");
					}
				}
				else
				{
					logInfo("Approved product is not available to add");
				}
			}
			else
			{
				logInfo("Approved product list is not available for current selection");
			}
			}
			else
			{
				logInfo("No item available to update product quantity2");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
	}
	
	/*
	 * add the quantity
	 */
	public void AddProductQuantity()
	{
		try {
			if(driver.isElementDisplayed(CatalogPageObject.add_row, "Added Row"))
			{
				logPass("Added item is available");
				if(driver.explicitWaitforVisibility(CatalogPageObject.Update_Quantity_txtbox, "Update Quantity"))
				{
					logPass("Bundle quantity is available");
					int rand = driver.generateRandomNumberWithLimit(10, 1);
					String random= Integer.toString(rand);
					
					if(driver.enterText(CatalogPageObject.Update_Quantity_txtbox, random, "RandomNumber"))
					{
						logPass("Bundle quantity is updated");
						//GetProductIdFromBundleOverlay(); // In general components
						
					}
					else
					{
						logFail("Bundle quantity is not updated");
					}
				}
				else
				{
					logInfo("Bundle quantity is not available for current selection");
				}
			}
			else
			{
				logInfo("Item row is not available for current selection");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void saveButtonClick()
	{
		try {
			if(driver.explicitWaitforVisibility(CatalogPageObject.Create_Button, "Save Button"))
			{
				if(driver.jsClickElement(CatalogPageObject.Create_Button, "Save Button"))
				{
					logPass("Quantity is saved");
				}
				else
				{
					logFail("Quantity is not saved");
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	/*
	 *  Delete the product
	 */
	public void DeleteProduct()
	{
		if(driver.clickElement(CatalogPageObject.Delete_Icon, "Delete icon"))
		{
			logPass("Delete icon is clicked");
		}
		else
		{
			logFail("Delete icon is not clicked");
		}
	}
	
	
	/*
	 *  Verify the Bundle name comparison
	 */
	
	public void CompareBundleAndCollectionName() throws InterruptedException
	{
		Thread.sleep(6000);
		if(driver.isElementDisplayed(CatalogPageObject.Breadcrumb_Bundlename, "Breadcrumb Bundle name"))
		{	
		logPass("Bundle and collection name is displayed");	
		String breadcrumb_buldlename = driver.getText(CatalogPageObject.Breadcrumb_Bundlename, "Breadcrumb Bundle name");
		//String txtbox_buldlename = driver.getText(CatalogPageObject.Edit_Bundlenametxtbox, "Textbox Bundle name");
		String txtbox_buldlename = driver.getElementAttribute(CatalogPageObject.Edit_Bundlenametxtbox, "value", "Textbox Bundle name");
		
			if(breadcrumb_buldlename.equals(txtbox_buldlename))
			{
				logPass("Bundle and collection Name's are equal");
			}
			else
			{
				logFail("Bundle and collection Name's are not equal");
			}
		}
		else
		{
			logFail("Bundle and collection name is not displayed");
		}
		
	}
	
	/*
	 * Click basic information tab
	 */
	public void basicInformationTabSelection()
	{
		try {
			if(driver.explicitWaitforVisibility(CatalogPageObject.basic_info_tab, "Basic Information Tab")) 
			{
				if(driver.moveToElementAndClick(CatalogPageObject.basic_info_tab, "Basic Information Tab"))
				{
					logPass("Basic Information Tab is clicked");
				}
				else
				{
					logFail("Basic Information Tab is not clicked");
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	/*
	 *  Update the bundle name
	 */
	
	public void UpdateBundleNameAndSave()
	{
		String Bundlename = ExcelReader.getData("CatalogPage", "BundleName");
		 int randomnumber=driver.generateRandomNumber(100);
	        String FinalBundleName = Bundlename+randomnumber+currentTimeStamp();
	        //if(driver.enterText(CatalogPageObject.BundleName_txtbox,FinalBundleName,"Bundle Name"))
	        if(driver.enterText(CatalogPageObject.Update_productname,FinalBundleName,"Bundle Name"))
			{
				logPass("Bundle Name is entered");
				logInfo("Bundle Name is : " +FinalBundleName);
			}
			else
			{
				logInfo( "Bundle product is not available for current selection");
			}
	        //GetEditedName();
	        SaveEditedBundleDetails();
	}
	
	
	
	/*
	 *  ******************************** ADDED ON 26-Nov-18 *******************************************
	 */
	
	public void DeleteAddedProduct()
	{
		
		if(driver.isElementDisplayed(CatalogPageObject.DeleteIcon, "Delete Icon"))
		{
			logPass("Delete icon is displayed");
			if(driver.jsClickElement(CatalogPageObject.DeleteIcon, "Delete icon"))
			{
				logPass("Added bundle item is deleted");
			}
			else
			{
				logFail("Added bundle item is not deleted");
			}
		}
		else
		{
			logInfo("Delete icon is not available for current selection");
		}
		
				
	}
	
	/*
	*  Verify deleted product is restored
	*/
	public void VerifyAddedProductisDisabled()
	{
		try 
		{
			driver.explicitWaitforVisibility(CatalogPageObject.Add_product_btn, "Add Product Button");
			if(driver.isElementDisplayed(CatalogPageObject.Add_product_btn, "Add Product Button"))
			{
				String attribute = driver.getElementAttribute(CatalogPageObject.Add_product_btn, "disabled", "Disabled Attribute");
				logInfo("Disabled Attribute is : " + attribute);
				logPass("Added product in bundle is disabled");
				
				// Close Overlay
				if(driver.jsClickElement(CatalogPageObject.Close_overlay, "Overlay Close"))
				{
					logPass("Overlay is closed");
				}
				else
				{
					logFail("Overlay is not closed");
				}
			}
			else
			{
				logInfo("Added product in bundle is not disabled");
			}
		}
		catch (Exception e)
		{
			logFail("There is some issue in verifying added product. The Error is : " + e.getMessage());
		}
	}
	
	/*
	 *  Temp navigate back after updating the bundle or collection details
	 */
	public void TempNavigateBack()
	{
		try 
		{
			//Thread.sleep(5000);
			driver.navigateBack();
		} 
		catch (Exception e)
		{
			logFail("There is some issue in back navigation. The Error is : " + e.getMessage());
		}
		
	}
	/*
	 *  Select Collection from the dropdown
	 */
	public void AddCollectionItems()
	{
		try {
			String Bundleid = ExcelReader.getData("CatalogPage", "BundleID");
			String Bundlename = ExcelReader.getData("CatalogPage", "BundleName");
			if(driver.isElementDisplayed(CatalogPageObject.AddItemOverlay, "Add Item Overlay"))
			{
				logPass("Add Item Overlay is displayed");
				//Select bundle from dropdown
				if(driver.moveToElementAndClick(CatalogPageObject.Itemddropdown_Select, "Item Dropdown Select"))
				{
					logPass("Type dropdown is clicked");
					// Select Bundle option from the dropdown
					try {
						
						if(driver.jsClickElement(CatalogPageObject.CollectionOption, "Collection"))
						{
							//Thread.sleep(3000);
							if(driver.explicitWaitforVisibility(CatalogPageObject.BundleId_txtbox, "Bundle Id"))
							{
							logPass("Collection is selected from the dropdown");
							AddBundleIdRandomly(Bundleid);
							AddBundleNameRandomly(Bundlename);
							AddDetailsButton();
							}
							else
								logFail("Bundle Id element is not displayed");
						}
					} catch (Exception e) {
						// TODO: handle exception
						logFail("Collection is not selected from the dropdown");
					}
					
				}
				else
				{
					logFail("Type dropdown is not clicked");
				}
			}
			else
			{
				logFail("Add Item Overlay is not displayed");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
			
		
	}
	
	/*
	 *  Click on collection tab after creating collection
	 */
	
	public void ClickonCollectionTab()
	{
		if(driver.isElementDisplayed(CatalogPageObject.Collection_Tab, "Collection Tab"))
		{
			logPass("Collection Tab is displayed");
			// pancake click
			if(driver.jsClickElement(CatalogPageObject.Collection_Tab, "Collection Tab"))
			{
				logPass("Collection Tab is clicked");
			}
			else
			{
				logFail("Collection Tab is not clicked");
			}
		}
		else
		{
			logFail("Collection Tab is displayed");
		}
	}
	
	
	/*
	 *  Choose product randomly from products overlay
	 */
	
	public void EditProductRandomlyfromPDP()
	{
		try {
			if(driver.explicitWaitforVisibility(CatalogPageObject.Prod_Container, "Product Container"))
			{
			if(driver.isElementDisplayed(CatalogPageObject.Prod_Container, "Product Container"))
			{
				logPass("Product list is shown");
				int productsize = driver.getSize(CatalogPageObject.Random_Product_Select, "Product Select");
				int productrand = driver.generateRandomNumberWithLimit(productsize, 1);		
				By RandomProductID = By.xpath("(" + CatalogPageObject.Random_Product_Select.toString().replace("By.xpath: ", "").concat(")[" + (productrand) + "]"));
				driver.jsClickElement(RandomProductID, "Product is selected randomly");
				/*if(driver.getCurrentUrl().contains("productId"))
				{
					logPass("Product page is displayed");
				}
				else
				{
					logFail("Product page is not displayed");
				}*/
			}
			else
			{
				logFail("Product list is not shown");
			}
			}
		} catch (Exception e) {
			// TODO: handle exception
			logFail("There is some issue in product selection. The Error is : " + e.getMessage());
		}
		
		
	}
	
	/*
	 *  Date picker input
	 */
	public void datepickerinputproductedit() throws InterruptedException
    {
	   // Start Date selection    
	   if(driver.jsClickElement(CatalogPageObject.StartDate, "Start Date"))
	   {
	    int Size=driver.getSize(CatalogPageObject.StartDate_Selection, "Size of start date");
	    Thread.sleep(500);
	    driver.clickElement(CatalogPageObject.Select_StartDate, "Start date selected");
	    logPass("Start Date is selected");
	   }
	   else
	   {
		logFail("Start Date is not selected");   
	   }
	   	// End Date Selection
	    if(driver.jsClickElement(CatalogPageObject.EndDate, "End Date"))
	    {
	    int EndDateSize=driver.getSize(CatalogPageObject.EndDate_Selection, "Size of end date");
	    driver.jsClickElement(CatalogPageObject.EndDate_Selection, "");
	    By enddateselect = By.xpath("(" + CatalogPageObject.EndDate_Selection.toString().replace("By.xpath: ", "").concat(")[" + (EndDateSize) + "]"));
	    driver.jsClickElement(enddateselect, "End Date Selection");
	    logPass("End Date is selected");
	    }
	    else
	    {
	    	logFail("End Date is not sselected");
	    }
        
    }  
	
	/*
	 *  Save updated product details
	 */
	public void SaveUpdatedProduct() 
    {
		if(driver.jsClickElement(CatalogPageObject.SaveUpdatedProduct, "Save Button"))
		{
			logPass("Updated product is saved");
		}
		else
		{
			logFail("Updated product is not saved");
		}
    }
	
	/*
	 *  Click on relations tab
	 */
	
	public void RelationTabselection() 
    {
		try {
			//Thread.sleep(3000);
			driver.explicitWaitforVisibility(CatalogPageObject.Relations_Tab, "Relation Tab");
			if(driver.jsClickElement(CatalogPageObject.Relations_Tab, "Relation Tab"))
			{
				logPass("Relation Tab is clicked");
				if(driver.getCurrentUrl().contains("relations"))
				{
					logPass("Relations page is displayed");
				}
				else
				{
					logFail("Relations page is not displayed");
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			logFail("Relation Tab is not clicked");
		}
		
    }
	
	/*
	 *   Verify add products in Up-sell
	 */
	public void AddProductsinUpSell() 
    { 
		try {
			if(driver.explicitWaitforVisibility(CatalogPageObject.Upsell_Addproduct_btn, "Upsell Add Product Button"))
			{
			if(driver.jsClickElement(CatalogPageObject.Upsell_Addproduct_btn, "Upsell Add Product Button"))
			{
				logPass("Add product button is clicked for upsell products");
				/*if(driver.isElementDisplayed(CatalogPageObject.prd_add_upsell, "Upsell products"))
				{
					logPass("Upsell products are available");*/
					// Random product select
					int upsell_size = driver.getSize(CatalogPageObject.prd_add_upsell, "Upsell Product");
					if(upsell_size >=25)
					{
						logPass("More than 25 products available");
						
						for(int i=1;i<=25;i++)
						{
							Thread.sleep(5000);
							if(driver.explicitWaitforVisibility(CatalogPageObject.container_list, "Upsell Container List"))
							{
							By upsellclick = By.xpath("(" + CatalogPageObject.prd_add_upsell.toString().replace("By.xpath: ", "").concat(")[" + (i) + "]"));
						    driver.jsClickElement(upsellclick, "Upsell Selection");
							}
							else
								logFail("Upsell element is not displayed");
						    
						}
						
					}
					else
					{
						Thread.sleep(5000);
						if(driver.explicitWaitforVisibility(CatalogPageObject.container_list, "Upsell"))
						{
						logInfo("More than 25 products are not available");
						logInfo("Available product size is:" +upsell_size);
						By upsellclick = By.xpath("(" + CatalogPageObject.prd_add_upsell.toString().replace("By.xpath: ", "").concat(")[" + (1) + "]"));
					    driver.jsClickElement(upsellclick, "Upsell Selection");
					    logPass("One product is added in Upsell");
						}
						else
							logFail("Upsell element is not displayed");
						
					}
				}
				/*else
				{
					logFail("Upsell products are not available");
				}*/
			}
			else
			{
				logFail("Add product button is not clicked for upsell products");
			}
			//}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
    }
	
	/*
	 *  Verify 25 products added
	 */
	
	public void VerifyAddedProducts() 
    {
		int DeleteiconCount = driver.getSize(CatalogPageObject.deleteicon_count, "Delete Icon");
		{
			if(DeleteiconCount == 25)
			{
				logPass("25 products added");
			}
			else
			{
				if(DeleteiconCount == 1)
				{
					logPass("1 product added");
					
				}
				
			}
		}
		
    }
	
	/*
	 *  Delete the added products count
	 */
	public void DeleteAddedMultipleProducts() 
    {
		try {
			if(driver.explicitWaitforVisibility(CatalogPageObject.deleteicon_count, "Delete icon"))
			{
			if(driver.isElementDisplayed(CatalogPageObject.deleteicon_count, "Delete icon"))
			{
				logPass("Delete icons available");
				// Random product select
				int upsell_size = driver.getSize(CatalogPageObject.deleteicon_count, "Delete icon");
				if(upsell_size >=25)
				{
					for(int i=1;i<=25;i++)
					{
						By deleteproduct = By.xpath("(" + CatalogPageObject.deleteicon_count.toString().replace("By.xpath: ", "").concat(")[" + (1) + "]"));
						driver.scrollToElement(deleteproduct, "Delete product");
					   /* try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}*/
						
						driver.jsClickElement(deleteproduct, "Delete added products");
					    logPass("Added product " + i + " is deleted");
					}
				}
				else
				{
					logInfo("25 products are not available to delete");
					By deleteproduct = By.xpath("(" + CatalogPageObject.deleteicon_count.toString().replace("By.xpath: ", "").concat(")[" + (1) + "]"));
					driver.jsClickElement(deleteproduct, "Delete added products");
				    logPass("One product is deleted in Upsell");
				}
			}
			else
			{
				logFail("Delete icons are not available");
			}
		} 
		}
		catch (Exception e) {
			// TODO: handle exception
		}
			
    }
	
	//Nathiya
	
	/*
	* Verify that Create button is clicked and success message is displayed
	*/
	public void clickAttributeCreateButton()
	{
		if(driver.explicitWaitforVisibility(CatalogPageObject.Create_Button, "Create Button"))
		{
			if(driver.jsClickElement(CatalogPageObject.Create_Button, "Create Button"))
			{
				logPass("The Create Button is clicked");
			}
			else
			{
				logFail("The Create Button is not clicked");
			}
		}
	}
    
	/*
	* Verify that the Recently created attribute is displayed
	*/
	public void verifyCreatedAttributeIsDisplayed()
	{
		try {
			String attributeName = ExcelReader.getData("CatalogPage", "Data_1");
			if(driver.isElementEnabled(CatalogPageObject.Recently_Created_Attribute, "Recently Created Attribute"))
			{
				logPass("Recently Created Attribute '" + attributeName + "' is displayed");	
			}
			else
			{
				logFail("Recently Created Attribute '" + attributeName + "' is not displayed");
			}
		} catch (Exception e) {
			
			// TODO: handle exception
		}
		
	}
    
	
    /*
    *  Verify Import Tab click
    */
    public void verifyImportTabIsClicked()
    {
    	try
    	{
    		//driver.pageReload();
    		Thread.sleep(10000);
	    	driver.explicitWaitforVisibility(CatalogPageObject.Import_Tab, "Import Tab");
	    	if(driver.isElementDisplayed(CatalogPageObject.Import_Tab, "Import Tab"))
	    	{
	    		logPass("Import Tab is displayed");
	    		
	    		if(driver.jsClickElement(CatalogPageObject.Import_Tab, "Import Tab"))
	    		{
	    			logPass("Import Tab is clicked");
	    		}
	    		else
	    		{
	    			logFail("Import Tab is not clicked");
	    		}
	    	}
	    	else
	    	{
	    		logFail("Import Tab is not displayed");
	    	}
    	}
    	catch (Exception e)
		{
			logFail("There is some issue in cliking Import Tab. The Error is : " + e.getMessage());
		}
    }
    
    public void uploadImportDataSheet()
    {
    	try
    	{
    		Thread.sleep(9000);
    		WebElement browse =(WebElement)driver.element(CatalogPageObject.upload_File_Input);
    		String filename = BaseClass.UserDir+ BaseClass.properties.getProperty("DataSheetPath");
            browse.sendKeys(filename);
    	}
    	catch (Exception e)
		{
			logFail("There is some issue in uploading Import Data Sheet. The Error is : " + e.getMessage());
		}
   }
    
    /*
	* Verify that Group Attributes Tab is clicked
	*/
	public void clickGropuAttributeTab()
	{
		if(driver.explicitWaitforVisibility(CatalogPageObject.grp_tab1, "Group Attribute Tab"))
		{
			logPass("Group Attribute Tab is displayed");
			if(driver.getCurrentUrl().contains("attributes"))
			{
				logPass("Attr page is displayed");
				// Grp attr click
				if(driver.jsClickElement(CatalogPageObject.grp_tab1, "Group Attribute Tab"))
				{
					logPass("Group Attribute Tab is clicked");
				}
				else
				{
					logFail("Group Attribute Tab is not clicked");
				}
			}
			else
			{
				logFail("Attr page is not displayed");
			}
		}
		else
		{
			logFail("Group Attribute Tab is not displayed");
		}
			
	}
	
	/*
	* Verify that Create Group Attribute Group Button is clicked and the Attribute Group Creation page is displayed
	*/
	public void clickCreateGroupAttributeBtn()
	{
		if(driver.explicitWaitforVisibility(CatalogPageObject.Create_Group_Btn, "Create Group Attribute Button"))
		{
			if(driver.jsClickElement(CatalogPageObject.Create_Group_Btn, "Create Group Attribute Button"))
			{
				logPass("Create Group Attribute Button is clicked");
				
				if(driver.isElementDisplayed(CatalogPageObject.Attribute_Group_Creation_Title, "Attribute Group Creation page"))
				{
					logPass("Attribute Group Creation page is displayed");
					
					String attributeName = ExcelReader.getData("CatalogPage", "Data_5");
					enterAttributeName("Attribute Group", attributeName);
					
					String orderId = Integer.toString(driver.generateRandomNumberWithLimit(99, 1));
					enterAttributeId("Attribute Group", orderId);
					//driver.jsClickElement(CatalogPageObject.Attribute_Name_Input, "Dummy Click");
				}
				else
				{
					logFail("Attribute Group Creation page is not displayed");
				}
			}
			else
			{
				logFail("Create Group Attribute Button is not clicked");
			}
		}
	}
	
	/*
	* Verify that the Variant Group Tab is displayed and clicked
	*/
	public void clickVariantGropuTab()
	{
		try {
			if(driver.explicitWaitforVisibility(CatalogPageObject.Variant_Group_Tab, "Variant Group Tab"))
			{
			if(driver.isElementDisplayed(CatalogPageObject.Variant_Group_Tab, "Variant Group Tab"))
			{
				logPass("Variant Group Tab is displayed");
				
				if(driver.jsClickElement(CatalogPageObject.Variant_Group_Tab, "Variant Group Tab"))
				{
					logPass("Variant Group Tab is clicked");
				}
				else
				{
					logFail("Variant Group Tab is not clicked");
				}
			}
			else
			{
				logInfo("Variant Group Tab is not displayed to click..");
			}
			}
		} catch (Exception e) {
			// TODO: handle exception
			logInfo("There is some issue in Variant Group selection. The Error is : " + e.getMessage());
		}
		
	}

	/*
	* Verify that Create Variant Group Button is clicked and the Variant Group Creation page is displayed
	*/
	public void clickCreateVariantGroupBtn()
	{
		if(driver.jsClickElement(CatalogPageObject.Create_Variant_Group_Btn, "Create Variant Group Button"))
		{
			logPass("Create Variant Group Button is clicked");
			
			if(driver.isElementDisplayed(CatalogPageObject.Attribute_Group_Creation_Title, "Variant Group Creation page"))
			{
				logPass("Variant Group Creation page is displayed");
				
				
				String attributeName = ExcelReader.getData("CatalogPage", "Data_6");
		        int randomnumber=driver.generateRandomNumber(1000);
		        String finalattrName = attributeName+" "+randomnumber+currentTimeStamp();
				enterAttributeName("Variant Group", finalattrName);
				
				String orderId = Integer.toString(driver.generateRandomNumberWithLimit(99, 1));
				enterAttributeId("Variant Group", orderId);
			}
			else
			{
				logFail("Variant Group Creation page is not available to display");
			}
		}
		else
		{
			logFail("Create Variant Group Button is not clicked");
		}
	}
	
	/*
	* Verify that Import Button is clicked
	*/
	public void clickImportButton()
	{
		if(driver.isElementDisplayed(CatalogPageObject.Import_Button, "Import Button"))
		{
			logPass("Import Button is displayed");
			
			if(driver.jsClickElement(CatalogPageObject.Import_Button, "Import Button"))
			{
				logPass("Import Button is clicked");
			}
			else
			{
				logFail("Import Button is not clicked");
			}
		}
		else
		{
			logFail("Import Button is not displayed");
		}
	}
	
	/*
	* Verify that the Locale dropdown is displayed
	*/
	public void verifyLocaleDropdown()
	{
		if(driver.isElementDisplayed(CatalogPageObject.Locale_Dropdown, "Locale dropdown"))
		{
			logPass("Locale dropdown is displayed");
		}
		else
		{
			logFail("Locale dropdown is not displayed");
		}
	}
	
	/*
	* Verify that the store dropdown is displayed
	*/
	public void verifystoreDropdown()
	{
		try
		{
			if(driver.isElementDisplayed(CatalogPageObject.store_Dropdown, "Store Dropdown"))
			{
				logPass("Store Dropdown is displayed");
				
				if(driver.jsClickElement(CatalogPageObject.store_Dropdown, "Store Dropdown"))
				{
					logPass("Store Dropdown is clicked");
					
					if(driver.isElementDisplayed(CatalogPageObject.store_Dropdown_Values, "Store Dropdown Value"))
					{
						logPass("Store Dropdown values are displayed");
						
						int storeDropdownSize = driver.getSize(CatalogPageObject.store_Dropdown_Values, "Store Dropdown Value");
						
						for(int i=1; i<=storeDropdownSize; i++)
						{
							if(i != 1)
							{
								driver.jsClickElement(CatalogPageObject.store_Dropdown, "Store Dropdown");
								Thread.sleep(3000);
							}
							By currentStoreXpath = By.xpath("(" + CatalogPageObject.store_Dropdown_Values.toString().replace("By.xpath: ", "").concat(")[" + (i) + "]"));
							String currentStore = driver.getText(currentStoreXpath, "Current Store");
							
							if(driver.jsClickElement(currentStoreXpath, "Current Store Xpath"))
							{
								logPass("The current store '" + currentStore + "' is clicked.");
								
								Thread.sleep(5000);
								
								if(driver.getCurrentUrl().contains("storeId"))
								{
									String storeId = driver.getCurrentUrl().split("storeId=") != null ? driver.getCurrentUrl().split("storeId=")[1] : "";
									logPass("The current store '" + currentStore + "' is navigated to its landing page. The store id is : " + storeId);
								}
								else
								{
									logFail("The current store '" + currentStore + "' is not navigated to its landing page");
								}
							}
							else
							{
								logFail("The current store " + currentStore + "'  is not clicked.");
							}
						}
					}
					else
					{
						logFail("Store Dropdown values are not displayed");
					}
				}
				else
				{
					logFail("Store Dropdown is not clicked");
				}
			}
			else
			{
				logFail("Store Dropdown is not displayed");
			}
		}
		catch (Exception e)
		{
			logFail("There is some issue in store dropdown value selection. The Error is : " + e.getMessage());
		}
	}
	
	/*
	* Verify Master Catalog page Navigation
	*/
	public void navigateToMasterCatalogPage()
	{
		try
		{
			if(driver.explicitWaitforVisibility(CatalogPageObject.Master_Catalog_Edit_Icon, "Master Catalog Edit Icon"))
			{
				if(driver.jsClickElement(CatalogPageObject.Master_Catalog_Edit_Icon, "Master Catalog Edit Icon"))
				{
					logPass("Master Catalog Edit Icon is clicked");
					
					Thread.sleep(3000);
					if(driver.isElementDisplayed(CatalogPageObject.Master_Catalog_Page, "Master Catalog page"))
					{
						logPass("Master Catalog page is displayed");
					}
					else
					{
						logFail("Master Catalog page is not displayed");
					}
				}
				else
				{
					logFail("Master Catalog Edit Icon is not clicked");
				}
		}
		}
		catch (Exception e)
		{
			logFail("There is some issue in Master Catalog page Navigation. The Error is : " + e.getMessage());
		}
	}
	
	/*
	*  Verify Master Overview Tab is displayed
	*/
	/* public void verifyMasterOverviewTabClick()
	{
		if(driver.isElementDisplayed(CatalogPageObject.Master_Overview_Tab, "Master Overview Tab"))
		{
			logPass("Master Overview Tab is displayed");
			
			if(driver.jsClickElement(CatalogPageObject.Master_Overview_Tab, "Master Overview Tab"))
			{
				logPass("Master Overview Tab is clicked");
			}
			else
			{
				logFail("Master Overview Tab is not clicked");
			}
		}
		else
		{
			logFail("Master Overview Tab is displayed");
		}
	}
	
	/*
	* Edit master catalog details
	*/
	/* public void editMasterCatelogDetails()
	{
		try
		{
			String catalogName = ExcelReader.getData("CatalogPage", "Data_1");
			int randomnumber=driver.generateRandomNumberWithLimit(1, 1000);
		    String finalCatalogName = catalogName + randomnumber;
		    
			if(driver.isElementDisplayed(CatalogPageObject.Catalog_Name_Input, "Catalog Name Input Field"))
			{
				logPass("Catalog Name Input Field is displayed");
				
				if(driver.enterText(CatalogPageObject.Catalog_Name_Input, finalCatalogName, "Catalog Name Input Field"))
				{
					logPass("Catalog Name Input is entered");
					
					clickUpdateButton();
					
					Thread.sleep(3000);
					if(driver.compareElementTextWithIgnoreCase(CatalogPageObject.Master_Catalog_Name, catalogName, "Catalog Name"))
					{
						logPass("Updated catalog Name is displayed");
					}
					else
					{
						logPass("Updated catalog Name is not displayed");
					}
				}
				else
				{
					logFail("Catalog Name Input is not entered");
				}
			}
			else
			{
				logFail("Catalog Name Input Field is not displayed");
			}
		}
		catch (Exception e)
		{
			logFail("There is some issue in editing master catalog details. The Error is : " + e.getMessage());
		}
	}
	
	/*
	* Verify that Update button is clicked and success message is displayed
	*/
	/* public void clickUpdateButton()
	{
		if(driver.clickElement(CatalogPageObject.Create_Button, "Update Button"))
		{
			logPass("The Update Button is clicked");
			
			if(driver.isElementDisplayed(CatalogPageObject.Attribute_Created_Success_Msg, "Updated Success Msg"))
			{
				logPass("The '" + driver.getText(CatalogPageObject.Attribute_Created_Success_Msg, "Updated Success Msg") + "' Success Message is displayed.");
			}
			else
			{
				logPass("The catalog edited update Success Message is not displayed.");
			}
		}
		else
		{
			logFail("The Update Button is not clicked");
		}
	} */
	
	/*
	* Verify that Project menu is clicked
	*/
	public void verifyProjectMenuIsClicked()
	{
		if(driver.jsClickElement(CatalogPageObject.Projects_Menu, "Master Catalog Edit Icon"))
		{
			logPass("Project menu is clicked");	
		}
		else
		{
			logFail("Project menu is not clicked");
		}
	}
	
	/*
	*  Verify that create new project button is clicked
	*/
	public void createNewProject()
	{
		if(driver.isElementDisplayed(CatalogPageObject.Create_New_Project_Btn, "create new project button"))
		{
			logPass("create new project button is displayed");
			
			if(driver.jsClickElement(CatalogPageObject.Create_New_Project_Btn, "create new project button"))
			{
				logPass("create new project button is clicked");
				
				enterNewcatalogProjectName();
			}
			else
			{
				logFail("create new project button is not clicked");
			}
		}
		else
		{
			logFail("create new project button is displayed");
		}
	}
	
	/*
	* Enter the Attributes Name
	*/
	public void enterNewcatalogProjectName()
	{
		try
		{
			//Thread.sleep(4000);
			if(driver.explicitWaitforVisibility(CatalogPageObject.New_Project_Name, "New Project Name"))
			{
			int randomnumber=driver.generateRandomNumberWithLimit(1000, 1);
		    String finalProjectName = "Test" + randomnumber+currentTimeStamp();
			if(driver.enterText(CatalogPageObject.New_Project_Name, finalProjectName, "New Project Name"))
			{
				logPass("New Project Name is entered. The entered Project Name is : " + finalProjectName);

				if(driver.clickElement(CatalogPageObject.Project_Create_Button, "Project Create Button"))
				{
					logPass("Project Create Button is clicked");
					
					verifyCreatedProjectIsDisplayed(finalProjectName);
				}
				else
				{
					logFail("Project Create Button is not clicked");
				}
			}
			else
			{
				logFail("The user failed to enter the Project Name");
			}
			}
			else
				logFail("Project Name element is not displayed");
		} 
		catch (Exception e)
		{
			logFail("There is some issue in entering the Project Name. The Error is : " + e.getMessage());
		}
	}
	
	/*
	* Verify that create button is clicked and success message is displayed
	*/
	public void verifyCreatedProjectIsDisplayed(String exptProjectName)
	{
		try
		{
			Thread.sleep(5000);
			driver.scrollToElement(CatalogPageObject.Newly_Created_Project_Name, "New Project Name");
			if(driver.compareElementTextWithIgnoreCase(CatalogPageObject.Newly_Created_Project_Name, exptProjectName, "New Project Name"))
			{
				logPass("The newly created project '" + exptProjectName + "' is displayed");
			}
			else
			{
				logFail("The newly created project '" + exptProjectName + "' is not displayed");
			}
		}
		catch (Exception e)
		{
			logFail("There is some issue in verifying created Project. The Error is : " + e.getMessage());
		}
	}
	
	/*
	 *  ADDED ON NOV 27th ------------------- Priya
	 */
	
	/*
	 *  Close the right corner overlay
	 */
	public void CloseProductOverlay()
	{
		// Close Overlay
		if(driver.jsClickElement(CatalogPageObject.Close_overlay, "Overlay Close"))
		{
			logPass("Overlay is closed");
		}
		else
		{
			logFail("Overlay is not closed");
		}
	}
	
	
	/*
	 *  Verfiy no items found text is displaying after deleting the products
	 */
	
	public void VerifyNoItemFoundAfterDeletion()
	{
		if(driver.isElementDisplayed(CatalogPageObject.Noitemfound, "No Item Found"))
		{
			logPass("No Item Found element is displayed");
		}
		else
		{
			logFail("No Item Found element is not displayed");
		}
		
	}
	
	
	/*
	 *   Verify add products in Cross-sell
	 */
	public void AddProductsinCrossSell() 
    {
		try {
			driver.refreshPage();
			if(driver.explicitWaitforVisibility(CatalogPageObject.crosssell_Addproduct_btn, "CrossSell Add Product Button"))
			{
			if(driver.jsClickElement(CatalogPageObject.crosssell_Addproduct_btn, "CrossSell Add Product Button"))
			{
				logPass("Add product button is clicked for CrossSell products");
				/*if(driver.isElementDisplayed(CatalogPageObject.prd_add_upsell, "CrossSell products"))
				{
					logPass("CrossSell products are available");*/
					// Random product select
					int upsell_size = driver.getSize(CatalogPageObject.prd_add_upsell, "CrossSell Product");
					if(upsell_size >=25)
					{
						logPass("More than 25 products available");
						
						for(int i=1;i<=25;i++)
						{
							By upsellclick = By.xpath("(" + CatalogPageObject.prd_add_upsell.toString().replace("By.xpath: ", "").concat(")[" + (i) + "]"));
						    driver.jsClickElement(upsellclick, "CrossSell Selection");   
						}
					}
					else
					{
						logInfo("More than 25 products are not available");
						logInfo("Available product size is:" +upsell_size);
						By upsellclick = By.xpath("(" + CatalogPageObject.prd_add_upsell.toString().replace("By.xpath: ", "").concat(")[" + (1) + "]"));
					    driver.jsClickElement(upsellclick, "CrossSell Selection");
					    logPass("One product is added in CrossSell");
					}
				}
				/*else
				{
					logFail("CrossSell products are not available");
				}*/
			}
			else
			{
				logFail("Add product button is not clicked for upsell products");
			}
		//} 
		}
		catch (Exception e)
		{
			// TODO: handle exception
			logFail("There is some issue in adding products in upsell and crosssell : " + e.getMessage());
		}
		
    }
	
	
/*@Author :Arunraj.N*/
	
	/*Verify Master Catalog in Catalog page */
	
	public void masterCatalogValidator()
	{
		
		try
		{
		
			if(driver.isElementDisplayed(CatalogPageObject.MasterCatalogContainer, "Master Catalog Container"))
			{
				logPass("Master Catalog Container is displayed in catalog page");
				
				String Master_CatalogName = ExcelReader.getData("CatalogPage", "Data_1");
				String Master_CatalogID = ExcelReader.getData("CatalogPage", "Data_2");
				String Master_CatalogStatus = ExcelReader.getData("CatalogPage", "Data_3");
				
				if(driver.isElementDisplayed(CatalogPageObject.MasterCatalogName, "Master CatalogName"))
				{
					logPass("Master Catalog Name is displayed in Master Catalog Container");
					if(Master_CatalogName.equals(driver.getText(CatalogPageObject.MasterCatalogName, "Master CatalogName")))
					{
						logPass("Master Catalog Name is displayed as :"+Master_CatalogName);
					}
					else
					{
						logPass("Master Catalog Name is invalid as :"+driver.getText(CatalogPageObject.MasterCatalogName, "Master CatalogName"));
					}
				}
				else
				{
					logFail("Master Catalog Name is not displayed in Master Catalog Container");
				}
				
				
				
				if(driver.isElementDisplayed(CatalogPageObject.MasterCatalogID, "Master CatalogID"))
				{
					logPass("Master Catalog ID is displayed in Master Catalog Container");
					if(Master_CatalogID.equals(driver.getText(CatalogPageObject.MasterCatalogID, "Master CatalogID")))
					{
						logPass("Master Catalog ID is displayed as :"+Master_CatalogID);
					}
					else
					{
						logPass("Master Catalog ID is invalid as :"+driver.getText(CatalogPageObject.MasterCatalogID, "Master CatalogID"));
					}
				}
				else
				{
					logFail("Master Catalog ID is not displayed in Master Catalog Container");
				}
				
				
				
				if(driver.isElementDisplayed(CatalogPageObject.MasterCatalogStatus, "Master Catalog Status"))
				{
					logPass("Master Catalog Status is displayed in Master Catalog Container");
					if(Master_CatalogStatus.equals(driver.getText(CatalogPageObject.MasterCatalogStatus, "Master Catalog Status")))
					{
						logPass("Master Catalog Status is displayed as :"+Master_CatalogStatus);
					}
					else
					{
						logPass("Master Catalog Status is invalid as :"+driver.getText(CatalogPageObject.MasterCatalogStatus, "Master Catalog Status"));
					}
				}
				else
				{
					logFail("Master Catalog Status is not displayed in Master Catalog Container");
				}
					
				
			}
			else
			{
				logFail("Master Catalog Container is not displayed  in catalog page");
			}
		}
		catch (Exception e)
		{
				logFail("There is some issue in master catalog page validation : " + e.getMessage());
		}
		
	}
	
	
	
	/*	Verify edit button is not displayed in Master Catalog Container*/
	
	public void masterCatalogEditbuttonValidator()
	{

		if(driver.isElementDisplayed(CatalogPageObject.MasterCatalogContainer, "Master Catalog Container"))
		{
			logPass("Master Catalog Container is displayed in catalog page");
			if(driver.isElementNotDisplayed(CatalogPageObject.MasterCatalogEditbutton," Master Catalog Edit button"))
			{
				logPass("Master Catalog edit button is not displayed in Master Catalog Container");
			}
			else
			{
				logFail("Master Catalog  edit button is displayed in Master Catalog Container");
			}
		}
		else
		{
			logFail("Master Catalog Container is not displayed  in catalog page");
			
		}
		
		
	}
	
	
	
	
	/*Viewing Master Catalog*/
	
	
	public void masterCatalogViewClick()
	{
		try
		{
		
			if(driver.isElementDisplayed(CatalogPageObject.MasterCatalogEnter," Master Catalog view button"))
			{
				logPass("Master Catalog View arrow button is displayed in Master Catalog Container");
				
				driver.scrollToElement(CatalogPageObject.MasterCatalogEnter, "Master Catalog view button");
				if(driver.clickElement(CatalogPageObject.MasterCatalogEnter, "Master Catalog view button"))
				{
					logPass("Master Catalog View arrow button is clicked");
				}
				else
				{
					logFail("Master Catalog  View arrow button is  not clicked");
				}
			}
			else
			{
				logFail("Master Catalog  View arrow button is  not displayed in Master Catalog Container");
			}
		
		}
		catch (Exception e)
		{
				logFail("There is some issue in viewing master catalog  : " + e.getMessage());
		}
	
	}
	
	
	/*Default project page validation*/
	
	public void masterCatalogProjectPageValidator()
	{
		
		try
		{
			
			if(driver.getCurrentUrl().contains("catalogId=master") && !driver.getCurrentUrl().contains("&projectId="))
			{
				if(driver.isElementDisplayed(CatalogPageObject.MasterProjectDropDown, "Master project dropdown"))
				{
					logPass("Master project dropdown is displayed");
					if(driver.getText(CatalogPageObject.MasterProjectDropDown, "Master project dropdown").equalsIgnoreCase("projects"))
					{
						logPass("Default project page is displayed");
					}
					else
					{
						logPass("Default project page  is not displayed");
					}
				}
				else
				{
					logFail("Master project dropdown is not displayed");
				}
			
			}
			else
			{
				logFail("Catalog Overview Page is not navigated");
			}
		
		}
		catch (Exception e)
		{
				logFail("There is some issue in project page validation  : " + e.getMessage());
		}
	}
	
	
	/*Default project page menu validation*/

	public void defaultProjetPageMenuValidation()
	{
		
		try
		{
			if(driver.explicitWaitforVisibility(CatalogPageObject.MasterProjectOverviewMenu, "Master Overview menu "))
			{
			if(driver.isElementDisplayed(CatalogPageObject.MasterProjectOverviewMenu, "Master Overview menu "))
			{
				logPass("Master Overview menu is displayed");
			}
			else
			{
				logFail("Master Overview menu is not displayed");
			}
			
			if(driver.isElementDisplayed(CatalogPageObject.MasterProjectProductsMenu, "Products menu "))
			{
				logPass("Products menu is displayed");
			}
			else
			{
				logFail("Products menu is not displayed");
			}
			
			if(driver.isElementDisplayed(CatalogPageObject.MasterProjectSkuMenu, "Sku menu "))
			{
				logPass("Sku menu is displayed");
			}
			else
			{
				logFail("Sku menu is not displayed");
			}
		}}
		catch (Exception e)
		{
				logFail("There is some issue in master page menu validation  : " + e.getMessage());
		}
		
	}
	
	
	/*Feed Menu validation & click*/
	public void feedMenuValidation()
	{
		if(driver.isElementDisplayed(CatalogPageObject.SalesCatalogprojectFeedMenu, "Feed menu "))
		{
			logPass("Feed menu is displayed");
			if(driver.jsClickElement(CatalogPageObject.SalesCatalogprojectFeedMenu, "Feed menu "))
			{
				logPass("Feed menu is Clicked");
			}
			else
			{
				logFail("Feed menu is not Clicked");
			}
		}
		else
		{
			logFail("Feed menu is not displayed");
		}
	}
	
	
	
	
	/* Product/sku's menu click in the default project */	
	
	public void defaultProjetProductsMenuClick()
	{
		
		try
		{
			if(driver.isElementDisplayed(CatalogPageObject.MasterProjectProductsMenu, "Products menu "))
			{
				logPass("Products menu is displayed");
				
				if(driver.jsClickElement(CatalogPageObject.MasterProjectProductsMenu, "Products menu"))
				{
					logPass("Products menu is clicked");
				}
				else
				{
					logFail("Products menu is not clicked");
				}
				
			}
			else
			{
				logFail("Products menu is not displayed");
			}
		
		}
		catch (Exception e)
		{
			logFail("There is some issue in Products menu click : " + e.getMessage());
		}
	}
	
	
	/* Product/sku's menu click in the default project */	

	public void defaultProjetSkuMenuClick()
	{
		
		try
		{
			if(driver.isElementDisplayed(CatalogPageObject.product_tab_SKU, "Sku menu ")) // MasterProjectSkuMenu - changed as product_tab_SKU
			{
				logPass("Sku menu is displayed");
				if(driver.jsClickElement(CatalogPageObject.product_tab_SKU, "Sku menu")) // MasterProjectSkuMenu - changed as product_tab_SKU
				{
					logPass("Sku menu is clicked");
				}
				else
				{
					logFail("Sku menu is not clicked");
				}
			}
			else
			{
				logFail("Sku menu is not displayed");
			}
		
		}
		catch (Exception e)
		{
			logFail("There is some issue in Sku menu click : " + e.getMessage());
		}
	}
	
	/* Product/sku's products check */	

	public void defaultProjetProductPageProductsCheck()
	{
		try 
		{
			//Thread.sleep(5000);
			if(driver.explicitWaitforVisibility(CatalogPageObject.MasterproductPageTitle, "Product page title"))
			{
			if(driver.isElementDisplayed(CatalogPageObject.MasterproductPageTitle, "Product page title") 
					&& driver.getText(CatalogPageObject.MasterproductPageTitle, "Product page title").equalsIgnoreCase("products"))
			{
				logPass("Product page is displayed");
				if(driver.isElementDisplayed(By.xpath(CatalogPageObject.MasterProductsPageProductsContainer.toString().replace("By.xpath: ", "")+"[1]"), "Sku container"))
				{
					logPass("Products container is displayed in products page");
				}
				else
				{
					logFail("Products container is not displayed in products page");
				}
			}
			else
			{
				logInfo("Product page is not displayed for current selection");
			}
			}
			else
				logFail("Product page element is not displayed for current selection");
			
		}
		catch (Exception e)
		{
			logFail("There is some issue in default Projet Product Page validation : " + e.getMessage());
		}
	}
	
	/* Product/sku's products check */	

	public void defaultProjetSkuPageSkuCheck()
	{
		
		try 
		{
			//Thread.sleep(5000);
			if(driver.explicitWaitforVisibility(CatalogPageObject.MasterSkuPageTitle, "Sku page title"))
			{
			if(driver.isElementDisplayed(CatalogPageObject.MasterSkuPageTitle, "Sku page title") 
					&& driver.getText(CatalogPageObject.MasterSkuPageTitle, "Sku page title").equalsIgnoreCase("skus"))
			{
				logPass("Sku page is displayed");
				if(driver.explicitWaitforVisibility(CatalogPageObject.MasterSkuPageProductsContainer, "SKU Container"))
				{
					if(driver.clickElement(By.xpath(CatalogPageObject.MasterSkuPageProductsContainer.toString().replace("By.xpath: ", "")+"[1]"), "Sku container"))
					{
						logPass("Sku container is displayed in Sku page");
					}
					else
					{
						logFail("Sku container is not displayed in Sku page");
					}
				}	
				else
					logFail("Sku container element is not displayed in Sku page");
			}
			
			else
			{
				logFail("Sku page is not displayed");
			}
		
		}
		}
		catch (Exception e)
		{
			logFail("There is some issue in default Projet Sku Page validation : " + e.getMessage());
		}
	}
	
	
	/*Create catalog button click*/

	public void createCatalogButtonClick()
	{
		
		try
		{
			if(driver.isElementDisplayed(CatalogPageObject.CreateCatalogButton, "Create Catalog Button"))
			{
				logPass("Create Catalog Button is displayed");
				if(driver.jsClickElement(CatalogPageObject.CreateCatalogButton, "Create Catalog Button"))
				{
					logPass("CreateCatalogButton is clicked");
				}
				else
				{
					logFail("Create Catalog Button is not clicked");
				}
			}
			else
			{
				logFail("Create Catalog Button is not displayed");
			}
		
		}
		catch (Exception e)
		{
			logFail("There is some issue in creat catalog button click : " + e.getMessage());
		}
	}
	
	
	/*Create catalog*/
	public void createCatalog()
	{
		
		try
		{
			if(driver.isElementDisplayed(CatalogPageObject.CreateCatalogPageTitle, "Create Catlog page title") && 
					driver.getText(CatalogPageObject.CreateCatalogPageTitle, "Create Catlog page title").equalsIgnoreCase("Create Sales Catalog"))
			{

				logPass("Create Catlog page is displayed");
				String CatalogName = ExcelReader.getData("CatalogPage", "Data_2");
                String CatalogID = ExcelReader.getData("CatalogPage", "Data_1")+cat_random;
                String CatalogDes = ExcelReader.getData("CatalogPage", "Data_3");
                //String CatalogStartDate = ExcelReader.getData("CatalogPage", "Data_6");
                enterCatalogID(CatalogID);
                enterCatalogName(CatalogName);
                enterCatalogDescription(CatalogDes);
                //enterCatalogDate(CatalogStartDate);
				
				if(driver.isElementDisplayed(CatalogPageObject.CreateCatalogCreateButton, "Catalog Create button"))
				{
					logPass("Create catloge page displays create button");
					driver.scrollToElement(CatalogPageObject.CreateCatalogCreateButton, " Catalog Create button");
					if(driver.jsClickElement(CatalogPageObject.CreateCatalogCreateButton, " Catalog Create button"))
					{
						logPass("Create button is clicked in catloge page");
					}
					else
					{
						logFail("Create button is not clicked in catloge page");
					}
					
					
					driver.explicitWaitforVisibility(CatalogPageObject.CreateCatalogSuccess, "Catalog sucess alert");
					if(driver.isElementDisplayed(CatalogPageObject.CreateCatalogSuccess, "Catalog sucess alert"))
					{
						logPass("Create Catalog is sucess");
					}
					else
					{
						logFail("Create Catalog is unsucess");
					}
					
				}
				else
				{
					logFail("Create button is not displaued in Create catloge page");
				}
				
				
			}
			else
			{
				logFail("Create Catlog page is not displayed");
			}
		
		}
		catch (Exception e)
		{
			logFail("There is some issue on creating a new catalog : " + e.getMessage());
		}
		
	}
	
	
	/*Enter catalog details*/
	public void enterCatalogID(String CatalogID)
	{
		try
		{
			if(driver.isElementDisplayed(CatalogPageObject.CreateCatalogID, " Catalog ID"))
			{
				logPass("Catalog ID field is displayed");
				driver.scrollToElement(CatalogPageObject.CreateCatalogID, " Catalog ID");
				if(driver.enterText(CatalogPageObject.CreateCatalogID, CatalogID,  "Catalog ID"))
				{
					logPass("Value entered in Catalog ID field ,ID"+CatalogID);
				}
				else
				{
					logFail("Value not entered in Catalog ID field ");
				}
			}
			else
			{
				logFail("Catalog ID field is not displayed");
			}
		
		}
		catch (Exception e)
		{
			logFail("There is some issue on entering catalog value : " + e.getMessage());
		}
	}
	
	/*Enter catalog details*/

	public void enterCatalogDescription(String CatalogDes)
	{
		try
		{
			if(driver.isElementDisplayed(CatalogPageObject.CreateCatalogDescription, " Catalog Description"))
			{
				logPass("Catalog Description field is displayed");
				driver.scrollToElement(CatalogPageObject.CreateCatalogDescription, " Catalog Description");
				if(driver.enterText(CatalogPageObject.CreateCatalogDescription, CatalogDes,  "Catalog Description"))
				{
					logPass("Value entered in Catalog Description field ,ID"+CatalogDes);
				}
				else
				{
					logFail("Value not entered in Catalog Description field ");
				}
			}
			else
			{
				logFail(" Catalog Description field is not displayed");
			}
		
		}
		catch (Exception e)
		{
			logFail("There is some issue on entering catalog value : " + e.getMessage());
		}
	}
	
	/*Enter catalog details*/

	public void enterCatalogName(String CatalogName)
	{
		try
		{
			if(driver.isElementDisplayed(CatalogPageObject.CreateCatalogName, " Catalog Name"))
			{
				logPass("Catalog Name field is displayed");
				driver.scrollToElement(CatalogPageObject.CreateCatalogName, " Catalog Name");
				if(driver.enterText(CatalogPageObject.CreateCatalogName, CatalogName,  "Catalog Name"))
				{
					logPass("Value entered in Catalog Name field ,ID"+CatalogName);
				}
				else
				{
					logFail("Value not entered in Catalog Name field ");
				}
			}
			else
			{
				logFail(" Catalog Name field is not displayed");
			}
		
		}
		catch (Exception e)
		{
			logFail("There is some issue on entering catalog value : " + e.getMessage());
		}
	}
	
	
	
	/*Catalog ID search*/
	
	public void catalogIDSearch()
	{

		try
		{
			if(driver.isElementDisplayed(CatalogPageObject.CatlogIDFilter, " Catalog ID Filter"))
			{
				logPass("Catalog ID Filter is displayed");
				if(driver.jsClickElement(CatalogPageObject.CatlogIDFilter, " Catalog ID Filter"))
				{
					logPass("Catalog ID Filter is Clicked");
					if(driver.isElementDisplayed(CatalogPageObject.CatlogIDFilterIputField, " Catalog ID Filter input field "))
					{
						logPass("Catalog ID Filter input field  is displayed");
						String CatalogID = ExcelReader.getData("CatalogPage", "Data_1")+cat_random;
						
						driver.enterText(CatalogPageObject.CatlogIDFilterIputField, CatalogID, "Catalog ID Filter input field ");
						clickCatalogSearchButton();
					}
					else
					{
						logFail("Catalog ID Filter input field  is not displayed");
					}
					
				}
				else
				{
					logFail("Catalog ID Filter is not clicked");
				}
				
			}
			else
			{
				logFail("Catalog ID Filter is not displayed");
			}
		
		}
		catch (Exception e)
		{
			logFail("There is some issue on searching catalog ID : " + e.getMessage());
		}
	}
	
	
	
	public void clickCatalogSearchButton()
	{

		if(driver.isElementDisplayed(CatalogPageObject.CatlogFilterSearch, " Catalog Filter search button"))
		{
			logPass("Catalog Filter search button is displayed");
			if(driver.jsClickElement(CatalogPageObject.CatlogFilterSearch, " Catalog Filter search button"))
			{
				logPass("Catalog Filter search button is Clicked");
			}
			else
			{
				logPass("Catalog Filter search button is not Clicked");
			}
								
		}
		else
		{
			logFail(" Catalog Filter search button  is not displayed");
		}
		
	}
	
	/*
	 *  Click search in overview page
	 */
	public void clickSearchButtoninOverviewPage()
	{

		if(driver.isElementDisplayed(CatalogPageObject.search_overviewpage, " Catalog Filter search button"))
		{
			logPass("Catalog Filter search button is displayed");
			if(driver.jsClickElement(CatalogPageObject.search_overviewpage, " Catalog Filter search button"))
			{
				logPass("Catalog Filter search button is Clicked");
			}
			else
			{
				logPass("Catalog Filter search button is not Clicked");
			}
								
		}
		else
		{
			logFail(" Catalog Filter search button  is not displayed");
		}
		
	}
	
	
	/*Create catalog validation*/
	public void createCatalogValidation()
	{
		
		try
		{
			Thread.sleep(5000);
			if(driver.isElementDisplayed(By.xpath(CatalogPageObject.CatalogID.toString().replace("By.xpath: ", "")+"[1]")," Catalog ID ") 
					&& driver.isElementDisplayed(By.xpath(CatalogPageObject.CatalogName.toString().replace("By.xpath: ", "")+"[1]"), " Catalog name"))
						
			{
	
				String CatalogName = ExcelReader.getData("CatalogPage", "Data_2");
				String CatalogID = ExcelReader.getData("CatalogPage", "Data_1")+cat_random;
				
				if(driver.getText(By.xpath(CatalogPageObject.CatalogID.toString().replace("By.xpath: ", "")+"[1]")," Catalog ID ").equalsIgnoreCase(CatalogID)
						&& driver.getText(By.xpath(CatalogPageObject.CatalogName.toString().replace("By.xpath: ", "")+"[1]"), " Catalog name").equalsIgnoreCase(CatalogName))
				{
					logPass("Created Catalog is same as provided data");
				}
				else
				{
					logFail("Created Catalog is not same as provided data Catalog ID ::"+CatalogID);
				}
				
				
			}
			
		}
		catch (Exception e)
		{
			logFail("There is some issue on validating catalog ID & Name : " + e.getMessage());
		}
	}
	
	/*Edited sale catalog validation */
	public void updatedCatalogValidation()
	{
		
		try
		{
			Thread.sleep(5000);
			if(driver.isElementDisplayed(By.xpath(CatalogPageObject.CatalogID.toString().replace("By.xpath: ", "")+"[1]")," Catalog ID ") 
					&& driver.isElementDisplayed(By.xpath(CatalogPageObject.CatalogName.toString().replace("By.xpath: ", "")+"[1]"), " Catalog name"))
						
			{
	
				String CatalogName = ExcelReader.getData("CatalogPage", "Data_4");
				String CatalogID = ExcelReader.getData("CatalogPage", "Data_1")+cat_random;
				
				if(driver.getText(By.xpath(CatalogPageObject.CatalogID.toString().replace("By.xpath: ", "")+"[1]")," Catalog ID ").equalsIgnoreCase(CatalogID)
						&& driver.getText(By.xpath(CatalogPageObject.CatalogName.toString().replace("By.xpath: ", "")+"[1]"), " Catalog name").equalsIgnoreCase(CatalogName))
				{
					logPass("Updated Catalog is same as provided data");
				}
				else
				{
					logFail("Updated Catalog is not same as provided data, Catalog ID ::"+CatalogID);
				}
				
				
			}
			
		}
		catch (Exception e)
		{
			logFail("There is some issue on validating Updated catalog ID & Name : " + e.getMessage());
		}
	}
	
    
	/*Edit sale catalog button click*/

	public void editCatalogButtonClick()
	{
		try {
			
			Thread.sleep(3000);
			driver.mouseHoverElement(By.xpath(CatalogPageObject.CatalogView.toString().replace("By.xpath: ", "")+"[1]"), " Catalog Edit ");
			if(driver.isElementDisplayed(By.xpath(CatalogPageObject.CatalogEdit.toString().replace("By.xpath: ", "")+"[1]")," Catalog Edit "))
			{
				logPass("Edit button is  displayed in sale catalog");
	
				if(driver.jsClickElement(By.xpath(CatalogPageObject.CatalogEdit.toString().replace("By.xpath: ", "")+"[1]")," Catalog Edit "))
				{
					logPass("Edit button is clicked  in sale catalog");
					
				}
				else
				{
					logFail("Edit button is not clicked in sale catalog");
				}
			}
			else
			{
				logFail("Edit button is not displayed in sale catalog");
			}
		} catch (Exception e) 
		{
			logFail("There is some issue on Updating a  catalog : " + e.getMessage());
		}
	}
	
	
	/*Edit sale catalog*/

	public void editCatalog()
	{
		
		try
		{
			if(driver.isElementDisplayed(CatalogPageObject.CreateCatalogPageTitle, "Update Catlog page title") && 
					driver.getText(CatalogPageObject.CreateCatalogPageTitle, "Update Catlog page title").equalsIgnoreCase("Update Sales Catalog"))
			{
				
				
				logPass("Update Catlog page is displayed");
				

				String CatalogName = ExcelReader.getData("CatalogPage", "Data_4");
				String CatalogID = ExcelReader.getData("CatalogPage", "Data_1")+cat_random;
				String CatalogDes = ExcelReader.getData("CatalogPage", "Data_5");
				//enterCatalogID(CatalogID);
				enterCatalogName(CatalogName);
				enterCatalogDescription(CatalogDes);
				
				if(driver.isElementDisplayed(CatalogPageObject.CreateCatalogCreateButton, "Catalog Update button"))
				{
					logPass("Update catloge page displays Update button");
					driver.scrollToElement(CatalogPageObject.CreateCatalogCreateButton, " Catalog Update button");
					if(driver.jsClickElement(CatalogPageObject.CreateCatalogCreateButton, " Catalog Update button"))
					{
						logPass("Update button is clicked in catloge page");
					}
					else
					{
						logFail("Update button is not clicked in catloge page");
					}
					
					
					if(driver.isElementDisplayed(CatalogPageObject.CreateCatalogSuccess, "Catalog sucess alert"))
					{
						logPass("Update Catalog is sucess");
					}
					else
					{
						logFail("Update Catalog is unsucess");
					}
					
				}
				else
				{
					logFail("Update button is not displaued in Update catloge page");
				}
				
				
			}
			else
			{
				logFail("Update Catlog page is not displayed");
			}
		
		}
		catch (Exception e)
		{
			logFail("There is some issue on Updating a  catalog : " + e.getMessage());
		}
	
	}
	
	
	/*View sales catalog*/

	public void viewSalesCatalog()
	{
		if(driver.isElementDisplayed(By.xpath(CatalogPageObject.CatalogView.toString().replace("By.xpath: ", "")+"[1]"), "Catalog sucess alert"))
		{
			logPass("View sales Catalog is displayed");
			
			if(driver.jsClickElement(By.xpath(CatalogPageObject.CatalogView.toString().replace("By.xpath: ", "")+"[1]"), "Catalog sucess alert"))
			{
				logPass("View sales Catalog is clicked");
			}
			else
			{
				logFail("View sales Catalog is not clicked");
			}
		}
		else
		{
			logFail("View sales Catalog is not displayed");
		}
	}
	
	
	/*Feed rules validation*/
	
	public void feedRulesPageValidation()
	{

		try 
		{
			
			if(driver.isElementDisplayed(CatalogPageObject.SalesCatalogprojectFeedPageTitle, "Feed page title") 
					&& driver.getText(CatalogPageObject.SalesCatalogprojectFeedPageTitle, "Feed page title").equalsIgnoreCase("Feed Rules"))
			{
				logPass("Feed Rules page is displayed");
				
			}
			else
			{
				logFail("Feed Rules page is not displayed");
			}
			
		}
		catch (Exception e)
		{
			logFail("There is some issue in Feed Rules Page validation : " + e.getMessage());
		}
	
	}
	
	/*Feed rules Run feed*/

	public void feedRulesPageRunFeed()
	{

		try 
		{
			if(driver.isElementDisplayed(CatalogPageObject.SalesCatalogprojectFeedRunFeed, "Feed run button"))
			{
				logPass("Feed Rules run button is displayed");
				//driver.scrollToElement(CatalogPageObject.SalesCatalogprojectFeedRunFeed, "Feed run button");
				if(driver.clickElement(CatalogPageObject.SalesCatalogprojectFeedRunFeed, "Feed run button"))
				{
					logPass("Feed Rules run button is clicked");
				}
				else
				{
					logFail("Feed Rules run button is not clicked");
				}
				
				//Thread.sleep(3000);
				driver.isElementDisplayed(CatalogPageObject.FeedRunInvisible, "Feed run title");
				if(driver.explicitWaitforInVisibility(CatalogPageObject.FeedRunInvisible, "Feed run Invisible")
						|| driver.explicitWaitforInVisibility(CatalogPageObject.FeedRunInvisible, "Feed run Invisible")
						|| driver.explicitWaitforInVisibility(CatalogPageObject.FeedRunInvisible, "Feed run Invisible")
						|| driver.explicitWaitforInVisibility(CatalogPageObject.FeedRunInvisible, "Feed run Invisible")
						|| driver.explicitWaitforInVisibility(CatalogPageObject.FeedRunInvisible, "Feed run Invisible")
						|| driver.explicitWaitforInVisibility(CatalogPageObject.FeedRunInvisible, "Feed run Invisible")
						|| driver.explicitWaitforInVisibility(CatalogPageObject.FeedRunInvisible, "Feed run Invisible")
						|| driver.explicitWaitforInVisibility(CatalogPageObject.FeedRunInvisible, "Feed run Invisible")
						|| driver.explicitWaitforInVisibility(CatalogPageObject.FeedRunInvisible, "Feed run Invisible")
						|| driver.explicitWaitforInVisibility(CatalogPageObject.FeedRunInvisible, "Feed run Invisible"))
				{
					logPass("Feed run is success");
				}
				else
				{
					logFail("Feed run is unsuccess");
				}
			}
			else
			{
				logFail("Feed Rules run button is not displayed");
			}
			
		}
		catch (Exception e)
		{
			logFail("There is some issue in Feed Rules run : " + e.getMessage());
		}
	
	}
	
	
	/*Enter catalog details*/
    public void enterCatalogDate(String CatalogDate)
    {
        try
        {
            if(driver.isElementDisplayed(CatalogPageObject.CreateCatalogStartDate, " Catalog Start date"))
            {
                logPass("Catalog Start date field is displayed");
                driver.scrollToElement(CatalogPageObject.CreateCatalogStartDate, " Catalog Start date");
                if(driver.enterText(CatalogPageObject.CreateCatalogStartDate, CatalogDate,  "Catalog Start date"))
                {
                    logPass("Value entered in Catalog Start date field ,ID"+CatalogDate);
                }
                else
                {
                    logFail("Value not entered in Catalog Start date field ");
                }
            }
            else
            {
                logFail("Catalog Start date field is not displayed");
            }
        
        }
        catch (Exception e)
        {
            logFail("There is some issue on entering catalog value : " + e.getMessage());
        }
    }
	
	/*
	* Verify Import page Navigation
	*/
	public void navigateToImportPage()
	{
		try
		{
			Thread.sleep(3000);
			
			if(driver.jsClickElement(CatalogPageObject.Project_Enter_Icon, "Project Enter Icon"))
			{
				logPass("Project enter Icon is clicked");
				
				Thread.sleep(3000);
				if(driver.isElementDisplayed(CatalogPageObject.Master_Catalog_Page, "Project import page"))
				{
					logPass("Project import page is displayed");
				}
				else
				{
					logFail("Project import page is not displayed");
				}
			}
			else
			{
				logFail("Project enter Icon is not clicked");
			}
		}
		catch (Exception e)
		{
			logFail("There is some issue in project import page navigation. The Error is : " + e.getMessage());
		}
	}
	
	/*
	* Verify that the File Imported Success Message is displayed
	*/
	public void verifyImportSuccessMessageIsDisplayed()
	{
		try
		{
			Thread.sleep(6000);
			
			driver.explicitWaitforInVisibility(CatalogPageObject.File_Import_In_Progress, "File Import In Progress");
			driver.explicitWaitforVisibility(CatalogPageObject.File_Imported_Success_Msg, "File Imported Success Message");
			if(driver.isElementDisplayed(CatalogPageObject.File_Imported_Success_Msg, "File Imported Success Message"))
			{
				logPass("File Imported Success Message is displayed");
			}
			else
			{
				logFail("File Imported Success Message is not displayed");
			}
		}
		catch (Exception e)
		{
			logFail("There is some issue in file import. The Error is : " + e.getMessage());
		}
	}
	
	/*
	* Verify that the imported products are displayed");
	*/
	public void verifyImportedProductsAvailable()
	{
		try
		{
			Thread.sleep(5000);
			
			if(driver.jsClickElement(CatalogPageObject.Products_Tab, "Products Tab"))
			{
				logPass("Products Tab is clicked");
				
				if(driver.isElementDisplayed(CatalogPageObject.Products_Container, "Products Container"))
				{
					logPass("Products Container is displayed");
					
					if(driver.getSize(CatalogPageObject.Products_Container, "Products Container") >= 1)
					{
						logPass("The imported products are displayed");
					}
					else
					{
						logFail("The imported products are not displayed");
					}
				}
				else
				{
					logFail("Products Container is not displayed");
				}
			}
			else
			{
				logFail("Products Tab is not clicked");
			}
		}
		catch (Exception e)
		{
			logFail("There is some issue in showing imported products. The Error is : " + e.getMessage());
		}
	}
	
	/*
	* Verify that the imported Skus are displayed");
	*/
	public void verifyImportedSkusAvailable()
	{
		try
		{
			Thread.sleep(3000);
			
			if(driver.jsClickElement(CatalogPageObject.Skus_Tab, "Skus Tab"))
			{
				logPass("Skus Tab is clicked");
				
				if(driver.isElementDisplayed(CatalogPageObject.Skus_Container, "Skus Container"))
				{
					logPass("Skus Container is displayed");
					
					if(driver.getSize(CatalogPageObject.Skus_Container, "Skus Container") >= 1)
					{
						logPass("The imported Skus are displayed");
					}
					else
					{
						logFail("The imported Skus are not displayed");
					}
				}
				else
				{
					logFail("Skus Container is not displayed");
				}
			}
			else
			{
				logFail("Skus Tab is not clicked");
			}
		}
		catch (Exception e)
		{
			logFail("There is some issue in showing imported Skus. The Error is : " + e.getMessage());
		}
	}
	
	/*
    *  Verify user can able to submit the project
    */
    public void verifySubmitProjectFlow()
    {
        if(driver.isElementDisplayed(CatalogPageObject.ProjectDetailsDOM, "Project details"))
        {
            logPass("Project details page is displayed to submit");
            // Click on submit project
            if(driver.jsClickElement(CatalogPageObject.Submit_Project_Btn, "Submit Project Button"))
            {
                logPass("Project submit button is clicked");
                
                verifyProjectSubmittedSuccessMessageIsDisplayed();
            }
            else
            {
                logFail("Project submit button is not clicked");
            }
        }
        else
        {
            logFail("Project details page is not displayed to submit");
        }
    }
    
    /*
    * Verify  Project page Navigation
    */
    public void navigateToProjectPage()
    {
    	try
    	{
    		Thread.sleep(3000);
    		
    		if(driver.jsClickElement(CatalogPageObject.Project_Edit_Icon, "Project Edit Icon"))
    		{
    			logPass("Project Edit Icon is clicked");
    			
    			Thread.sleep(3000);
    			if(driver.isElementDisplayed(CatalogPageObject.Project_Page, "Project page"))
    			{
    				logPass("Project page is displayed");
    			}
    			else
    			{
    				logFail("Project page is not displayed");
    			}
    		}
    		else
    		{
    			logFail("Project Edit Icon is not clicked");
    		}
    	}
    	catch (Exception e)
    	{
    		logFail("There is some issue in project page navigation. The Error is : " + e.getMessage());
    	}
    }
    
    /*
     *  Entering into last created proj
     */
    public void EnterRecentlyCreatedProject()
    {
    	try
    	{
    		//Thread.sleep(3000);
    		driver.scrollToElement(CatalogPageObject.enter_last_proj, "Enter Last Project");
    		if(driver.explicitWaitforVisibility(CatalogPageObject.enter_last_proj, "Enter Last Project"))
    		{
    		if(driver.moveToElementAndClick(CatalogPageObject.enter_last_proj, "Enter Last Project"))
    		{
    			logPass("Entered into Lastly created Project");
    			
    			//Thread.sleep(5000);
    			/*if(driver.isElementDisplayed(CatalogPageObject.Project_Page, "Project page"))
    			{
    				logPass("Project page is displayed");
    			}
    			else
    			{
    				logFail("Project page is not displayed");
    			}*/
    		}
    		else
    		{
    			logFail("Project Edit Icon is not clicked");
    		}
    	}
    	}
    	catch (Exception e)
    	{
    		logFail("There is some issue in Lastly created Project... The Error is : " + e.getMessage());
    	}
    }
    
    /*
    * Verify that Project submitted Success Message is displayed
    */
    public void verifyProjectSubmittedSuccessMessageIsDisplayed()
    {
    	if(driver.isElementDisplayed(CatalogPageObject.Submitted_Success_Msg, "Project submitted Success Msg"))
    	{
    		logPass("The Project submitted Success Message is displayed. The Message is : " + driver.getText(CatalogPageObject.Submitted_Success_Msg, "Project submitted Success Msg"));
    	}
    	else
    	{
    		logFail("The Project submitted Success Message is not displayed");
    	}
    }
    
    /*
    * Verify that the Project is Approved
    */
    public void verifyProjectIsApproved()
    {
    	try
    	{
    		Thread.sleep(3000);
    		String approvedProjectName = driver.getElementAttribute(CatalogPageObject.Project_Page, "value", "Approve Project Name");
	    	if(driver.jsClickElement(CatalogPageObject.Approve_Project_Btn, "Approve Project Button"))
	    	{
	    		logPass("Project Approve button is clicked");
	    		
	    		Thread.sleep(9000);
	        	
	    		driver.explicitWaitforInVisibility(CatalogPageObject.Approve_Project_Btn, "Approve Project Button");
	    		if(driver.isElementNotDisplayed(CatalogPageObject.Approve_Project_Btn, "Approve Project Button"))
	    		{
	    			logPass("The Project '" + approvedProjectName + "' is Approved");
	    		}
	    		else
	    		{
	    			logFail("The Project '" + approvedProjectName + "' is not Approved");
	    		}
	    	}
	    	else
	    	{
	    		logFail("Project Approve button is not clicked");
	    	}
    	}
    	catch (Exception e)
    	{
    		logFail("There is some issue in project approve. The Error is : " + e.getMessage());
    	}
    }
    
    /*
     * Verify that the Project is reopened
     */
     public void clickProjectReopenBtn()
     {
     	try
     	{
     		String approvedProjectName = driver.getElementAttribute(CatalogPageObject.Project_Page, "value", "Approve Project Name");
 	    	if(driver.jsClickElement(CatalogPageObject.Project_Reopen_Icon, "Project Reopen Icon"))
 	    	{
 	    		logPass("Project Reopen Icon is clicked");
 	    		
 	    		driver.explicitWaitforVisibility(CatalogPageObject.Project_Reopen_Btn, "Project Reopen Button");
 	    		if(driver.jsClickElement(CatalogPageObject.Project_Reopen_Btn, "Project Reopen Button"))
 	    		{
 	    			logPass("The Project reopen button is clicked");
 	    			
 	    			Thread.sleep(3000);
 	    			if(driver.isElementDisplayed(CatalogPageObject.Submit_Project_Btn, "Submit Project Button"))
 		    		{
 		    			logPass("The Project '" + approvedProjectName + "' is Reopened");
 		    		}
 		    		else
 		    		{
 		    			logFail("The Project '" + approvedProjectName + "' is not Reopened");
 		    		}
 	    		}
 	    		else
 	    		{
 	    			logFail("The Project reopen button is not clicked");
 	    		}
 	    	}
 	    	else
 	    	{
 	    		logFail("Project Reopen Icon is not clicked");
 	    	}
     	}
     	catch (Exception e)
     	{
     		logFail("There is some issue in project Reopen. The Error is : " + e.getMessage());
     	}
     }
     
    /*
 	* Verify Overview page is displayed
 	*/
 	public void clickOverviewTab()
 	{
 		try
 		{
 			Thread.sleep(3000);
 			if(driver.jsClickElement(CatalogPageObject.OverviewTab, "Overview Tab"))
 			{
 				logPass("Overview Tab is clicked");
 				
 				if(driver.getCurrentUrl().contains("overview"))
 				{
 					logPass("Overview page is displayed");
 				}
 				else
 				{
 					logFail("Overview page is not displayed");
 				}
 				/*Thread.sleep(3000);
 				if(driver.isElementDisplayed(CatalogPageObject.OverviewPage, "Overview Page"))
 				{
 					logPass("Overview page is displayed");
 				}
 				else
 				{
 					logFail("Overview page is not displayed");
 				}*/
 			}
 			else
 			{
 				logFail("Overview Tab is not clicked");
 			}
 		}
 		catch (Exception e)
 		{
 			logFail("There is some issue in Overview page navigation. The Error is : " + e.getMessage());
 		}
 	}

 	/*Random no generator for edit/create catalog*/
    public void randomNoCatlogGenerator()
    {
        //cat_random=driver.generateRandomNumber(100000)+currentTimeStamp();
    	cat_random=currentTimeStamp();
    }
    String authToken="";
    String collectionid="";
    String projectid="";
    String projectname="";
    String salescatalogtype="";
    String attributeId="";
    String attributeGroupId="";
    String attributeGroupVariantId="";
    String productId="";
    String skuId="";
    
    ApiReaderUtils api=new ApiReaderUtils();
    
	public void generateauthToken() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=	api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/auth/login","{\"identity\": \""+properties.getProperty("LoginUserName")+"\", \"password\": \""+properties.getProperty("LoginPassword")+"\"}","");
		resp = res.get(0);
		System.out.println(resp);
		JSONObject root = new JSONObject(resp);
		authToken = root.getString("authToken"); 
		
	}
	
	public JSONObject getCollectionAttributes() throws JSONException
	{
		String res = new String();
		res=	api.readResponseFromUrl(properties.getProperty("Domain")+"/admin/services/collections/attributes/?businessId="+properties.getProperty("BusinessId")+"&serviceName=customer",sessionId);
		System.out.println(res);
		JSONObject root = new JSONObject(res);
        return root;
		
	}
	
	public JSONObject createCollections() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections?businessId="+properties.getProperty("BusinessId")+"&serviceName=catalog","{ \"name\": \"Automation_Catalog"+currentTimeStamp()+"\", \"properties\": [], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",properties.getProperty("sessionId"));
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		collectionid = root.getJSONObject("collectionResponse").getString("id");
		properties.put("collectionId", collectionid);
		String confiUrl=properties.getProperty("ApplicationUrl");
		confiUrl=confiUrl.replace("<<collectionId>>", collectionid);
		properties.put("ApplicationUrl", confiUrl);
		System.out.println("App URL"+properties.get("ApplicationUrl"));
        System.out.println("Domain -->"+properties.get("Domain"));
        System.out.println("collectionid"+collectionid);
		return root; 	
	}
	
	
	public void createMasterProject() throws JSONException
	{
		/*String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsProject
		for(int i=1;i<=3;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects?x-collection-id="+properties.getProperty("collectionId"),"{\"name\":\"Project"+i+"\",\"status\":\"ACTIVE\",\"type\":\"MASTER\",\"state\":\"OPEN\"}",sessionId);
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		projectid=root.getJSONObject("project").getString("identifier");
		projectname=root.getJSONObject("project").getString("name");
		properties.put("MasterProjectId" +i, projectid);
		properties.put("MasterProjectName" +i, projectname);
		System.out.println("MasterProjectId" +projectid);	
		System.out.println("MasterProjectName" +projectname);
		}	*/
		//System.out.println("properties "+properties.toString());	
		
		String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsProject
		for(int i=1;i<=3;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects?x-collection-id="+properties.getProperty("collectionId"),"{\"name\":\"Project"+i+"\",\"status\":\"ACTIVE\",\"type\":\"MASTER\",\"state\":\"OPEN\"}",properties.getProperty("sessionId"));
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		projectid=root.getJSONObject("project").getString("identifier");
		projectname=root.getJSONObject("project").getString("name");
		properties.put("MasterProjectId" +i, projectid);
		properties.put("MasterProjectName" +i, projectname);
		System.out.println("MasterProjectId" +projectid);	
		System.out.println("MasterProjectName" +projectname);
		}	
		//System.out.println("properties "+properties.toString());	
	}
	
	public void createMasterProduct1() throws JSONException
	{
		/*String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsProductCreation-Project1
		for(int i=1;i<=12;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId1")+"/products?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"Product"+driver.generateRandomNumber(1000000000)+"\",\"startTime\":\"2018-11-24T00:00:00\",\"endTime\":\"2050-12-31T23:59:59\",\"properties\":[{\"locked\":\"FALSE\",\"attributeId\":\"name\",\"value\":\"Product"+driver.generateRandomNumber(1000000000)+"\",\"locale\":\"en_US\"}]}",sessionId);
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		productId=root.getJSONObject("product").getString("identifier");
		//String MasterName=root.getString("");
		//properties.put("MasterProjectId" +i, projectid);
		System.out.println("productId" +i+ " " +productId);	
		}	*/
		
		String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsProductCreation-Project1
		for(int i=1;i<=12;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId1")+"/products?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"Product"+currentTimeStamp()+"\",\"startTime\":\"2018-06-29T13:03:39.466-05:00\",\"endTime\":\"2030-06-29T13:03:39.466-05:00\",\"properties\":[{\"locked\":\"FALSE\",\"attributeId\":\"name\",\"value\":\"Product"+currentTimeStamp()+"\",\"locale\":\"en_US\"}]}",properties.getProperty("sessionId"));
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		productId=root.getJSONObject("product").getString("identifier");
		//String MasterName=root.getString("");
		//properties.put("MasterProjectId" +i, projectid);
		System.out.println("productId" +i+ " " +productId);	
		}	
		
		
		//System.out.println("properties "+properties.toString());	
	}
	
	public void createMasterSKU1() throws JSONException
	{
		/*String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsSKUCreation-Project1
		for(int i=1;i<=5;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId1")+"/skus?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"SKU"+driver.generateRandomNumber(1000000000)+"\",\"startTime\":\"2018-11-24T00:00:00\",\"endTime\":\"2050-12-31T23:59:59\",\"properties\":[{\"locked\":\"false\",\"attributeId\":\"name\",\"value\":\"SKUS"+driver.generateRandomNumber(1000000000)+"\",\"locale\":\"en_US\"}]}",sessionId);
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		skuId=root.getJSONObject("sku").getString("identifier");
		System.out.println("skuId" +i+ " " +skuId);	
		}	*/
		//System.out.println("properties "+properties.toString());	
		
		String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsSKUCreation-Project1
		for(int i=1;i<=5;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId1")+"/skus?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"SKU"+currentTimeStamp()+"\",\"startTime\":\"2018-06-29T13:03:39.466-05:00\",\"endTime\":\"2030-06-29T13:03:39.466-05:00\",\"properties\":[{\"locked\":\"false\",\"attributeId\":\"name\",\"value\":\"SKUS"+currentTimeStamp()+"\",\"locale\":\"en_US\"}]}",properties.getProperty("sessionId"));
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		skuId=root.getJSONObject("sku").getString("identifier");
		System.out.println("skuId" +i+ " " +skuId);	
		}	
	}
	
	public void submitProject() throws JSONException, ClientProtocolException, IOException
	{
		/*String resp=new String();
		List<String> res = new ArrayList<String>();
		res=api.patch(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId3")+"?x-collection-id="+properties.getProperty("collectionId"),"{\"name\":\""+properties.getProperty("MasterProjectName3")+"\",\"description\":\"\",\"targetCompletionDate\":null,\"notes\":\"\",\"state\":\"SUBMITTED\"}",sessionId);
		resp = res.get(0);
		System.out.println(resp);*/
		
		String resp=new String();
		List<String> res = new ArrayList<String>();
		res=api.patch(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId3")+"?x-collection-id="+properties.getProperty("collectionId"),"{\"name\":\""+properties.getProperty("MasterProjectName3")+"\",\"description\":\"\",\"targetCompletionDate\":null,\"notes\":\"\",\"state\":\"SUBMITTED\"}",properties.getProperty("sessionId"));
		resp = res.get(0);
		System.out.println(resp);
		
	}
	
	public void approveProject() throws JSONException
	{
		
	
		String resp=new String();
		        List<String> res = new ArrayList<String>();
		        
		        Date date= new Date();

		        long time = date.getTime();
		        System.out.println("Time in Milliseconds: " + time);
		                
		        res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/feeds/catalogapproval/jobs/"+properties.getProperty("collectionId")+"_catalogapproval/schedules?x-collection-id="+properties.getProperty("collectionId")+"&businessId="+properties.getProperty("BusinessId"),"{\"description\":\"Catalog project approval schedule based on this project status will gets updated\",\"name\":\"CatalogProjectApprovalSchedule\",\"params\":[{\"name\":\"collectionId\",\"stringValue\":\""+properties.getProperty("collectionId")+"\"},{\"name\":\"catalogId\",\"stringValue\":\"master\"},{\"name\":\"projectId\",\"stringValue\":\""+properties.getProperty("MasterProjectId3")+"\"},{\"name\":\"userId\",\"stringValue\":\"1\",\"type\":\"string\"},{\"name\":\"locale\",\"stringValue\":\"en_US\"}],\"scheduleId\":\"CatalogProjectApprovalSchedule-"+time+"\",\"status\":\"ONDEMAND\"}",properties.getProperty("sessionId"));
		        resp = res.get(0);
		        System.out.println("approveProject  "+resp);

	}
	
	public void deleteAttributes() throws JSONException
	{
		String resp=new String();
		List<String> res = new ArrayList<String>();
		
		Date date= new Date();

		long time = date.getTime();
		System.out.println("Time in Milliseconds: " + time);
				
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/catalogservices/attributes/crosssellproductids?locale=en_US","",sessionId);
		resp = res.get(0);
		System.out.println("approveProject  "+resp);
	}
	
	public void deletecatalogattribute(String name) throws JSONException
	{
	   String resp = new String();
	   List<String> res = new ArrayList<String>();
	   res=    api.deleteattribute(properties.getProperty("Domain")+"/catalogservices/attributes/"+name+"?locale=en_US",properties.getProperty("collectionId"));
	   
	   resp = res.get(0);
	   System.out.println(resp);
	   logPass("deleteCatalogAttribute: "+resp);
	}
	
	public void deletecatalogattributegroup(String name) throws JSONException
	{
	   String resp = new String();
	   List<String> res = new ArrayList<String>();
	   res=    api.deleteattribute(properties.getProperty("Domain")+"/catalogservices/attributegroup/"+name+"?locale=en_US",properties.getProperty("collectionId"));
	   
	   resp = res.get(0);
	   System.out.println(resp);
	   logPass("deleteCatalogAttributeGroup: "+resp);
	}
	
	/*
    * Associate Store with Collection Id for Catalog Admin
    */
    public void storeAssociateCatalog() throws JSONException
    {
        /*String resp = new String();
        List<String> res = new ArrayList<String>();
        for(int i=1;i<=2;i++)
        {
            res=api.ReadAllDataFromUrlserviceput(properties.getProperty("APIDomain")+"/foundationservices/stores/"+properties.getProperty("CatalogStoreId" + i)+"/associations","{\"associations\":[{\"name\":\"catalog\",\"collectionId\":"+properties.getProperty("collectionId")+"}]}",properties.getProperty("foundationSessionId"),"");
            resp = res.get(0);
            System.out.println(resp);
            logPass("storeassocatefoundation "+resp);
        }*/
    }
	
	/*
	* Delete all the created attributes
	*/
	public void deleteAllCreatedAttributes()
	{
		try 
		{
			String attributeName = ExcelReader.getData("CatalogPage", "Data_1");
			deletecatalogattribute(attributeName);
			
			String attributeGroupName = ExcelReader.getData("CatalogPage", "Data_5");
			deletecatalogattributegroup(attributeGroupName);
			
			String variantGroupName = ExcelReader.getData("CatalogPage", "Data_6");
			deletecatalogattributegroup(variantGroupName);
		} 
		catch (Exception e)
		{
			logFail("There is some issue in attributes deletion. The Error is : " + e.getMessage());
		}
	}
	
	public void createMasterProduct2() throws JSONException
	{
		/*String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsProductCreation-Project2
		for(int i=1;i<=12;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId2")+"/products?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"Product"+driver.generateRandomNumber(1000000000)+"\",\"startTime\":\"2018-11-24T00:00:00\",\"endTime\":\"2050-12-31T23:59:59\",\"properties\":[{\"locked\":\"FALSE\",\"attributeId\":\"name\",\"value\":\"Product"+driver.generateRandomNumber(1000000000)+"\",\"locale\":\"en_US\"}]}",sessionId);
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		productId=root.getJSONObject("product").getString("identifier");
		//String MasterName=root.getString("");
		//properties.put("MasterProjectId" +i, projectid);
		System.out.println("productId" +i+ " " +productId);		
		}	*/
		//System.out.println("properties "+properties.toString());	
		
		String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsProductCreation-Project2
		for(int i=1;i<=12;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId2")+"/products?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"Product"+currentTimeStamp()+"\",\"startTime\":\"2018-06-29T13:03:39.466-05:00\",\"endTime\":\"2030-06-29T13:03:39.466-05:00\",\"properties\":[{\"locked\":\"FALSE\",\"attributeId\":\"name\",\"value\":\"Product"+currentTimeStamp()+"\",\"locale\":\"en_US\"}]}",properties.getProperty("sessionId"));
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		productId=root.getJSONObject("product").getString("identifier");
		//String MasterName=root.getString("");
		//properties.put("MasterProjectId" +i, projectid);
		System.out.println("productId" +i+ " " +productId);		
		}	
	}
	

	public void createMasterSKU2() throws JSONException
	{
		/*String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsSKUCreation-Project2
		for(int i=1;i<=5;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId2")+"/skus?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"SKU"+driver.generateRandomNumber(1000000000)+"\",\"startTime\":\"2018-11-24T00:00:00\",\"endTime\":\"2050-12-31T23:59:59\",\"properties\":[{\"locked\":\"false\",\"attributeId\":\"name\",\"value\":\"SKUS"+driver.generateRandomNumber(1000000000)+"\",\"locale\":\"en_US\"}]}",sessionId);
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		skuId=root.getJSONObject("sku").getString("identifier");
		System.out.println("skuId" +i+ " " +skuId);	
		}	*/
		//System.out.println("properties "+properties.toString());	
		
		String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsSKUCreation-Project2
		for(int i=1;i<=5;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId2")+"/skus?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"SKU"+currentTimeStamp()+"\",\"startTime\":\"2018-06-29T13:03:39.466-05:00\",\"endTime\":\"2030-06-29T13:03:39.466-05:00\",\"properties\":[{\"locked\":\"false\",\"attributeId\":\"name\",\"value\":\"SKUS"+currentTimeStamp()+"\",\"locale\":\"en_US\"}]}",properties.getProperty("sessionId"));
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		skuId=root.getJSONObject("sku").getString("identifier");
		System.out.println("skuId" +i+ " " +skuId);	
		}	
	}
	
	
	public void createMasterProduct3() throws JSONException
	{
		/*String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsProductCreation-Project3
		for(int i=1;i<=15;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId3")+"/products?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"Product"+driver.generateRandomNumber(1000000000)+"\",\"startTime\":\"2018-11-24T00:00:00\",\"endTime\":\"2050-12-31T23:59:59\",\"properties\":[{\"locked\":\"FALSE\",\"attributeId\":\"name\",\"value\":\"Product"+driver.generateRandomNumber(1000000000)+"\",\"locale\":\"en_US\"}]}",sessionId);
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		productId=root.getJSONObject("product").getString("identifier");
		//String MasterName=root.getString("");
		//properties.put("MasterProjectId" +i, projectid);
		System.out.println("productId" +i+ " " +productId);	
		}	*/
		//System.out.println("properties "+properties.toString());	
		
		String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsProductCreation-Project3
		for(int i=1;i<=15;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId3")+"/products?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"Product"+driver.generateRandomNumber(1000000000)+"\",\"startTime\":\"2018-06-29T13:03:39.466-05:00\",\"endTime\":\"2030-06-29T13:03:39.466-05:00\",\"properties\":[{\"locked\":\"FALSE\",\"attributeId\":\"name\",\"value\":\"Product"+driver.generateRandomNumber(1000000000)+"\",\"locale\":\"en_US\"}]}",properties.getProperty("sessionId"));
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		productId=root.getJSONObject("product").getString("identifier");
		//String MasterName=root.getString("");
//		properties.put("MasterProjectId" +i, productId);
		System.out.println("productId" +i+ " " +productId);	
		}	
	}
	
	
	public void createMasterSKU3() throws JSONException
	{
		/*String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsSKUCreation-Project3
		for(int i=1;i<=15;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId3")+"/skus?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"SKU"+driver.generateRandomNumber(1000000000)+"\",\"startTime\":\"2018-11-24T00:00:00\",\"endTime\":\"2050-12-31T23:59:59\",\"properties\":[{\"locked\":\"false\",\"attributeId\":\"name\",\"value\":\"SKUS"+driver.generateRandomNumber(1000000000)+"\",\"locale\":\"en_US\"}]}",sessionId);
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		skuId=root.getJSONObject("sku").getString("identifier");
		System.out.println("skuId" +i+ " " +skuId);	
		}	*/
		//System.out.println("properties "+properties.toString());	
		
		String resp=new String();
		List<String> res = new ArrayList<String>();
		
		//MasterCatalogsSKUCreation-Project3
		for(int i=1;i<=15;i++)
		{
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/catalogs/master/projects/"+properties.getProperty("MasterProjectId3")+"/skus?x-collection-id="+properties.getProperty("collectionId"),"{\"identifier\":\"SKU"+currentTimeStamp()+"\",\"startTime\":\"2018-06-29T13:03:39.466-05:00\",\"endTime\":\"2030-06-29T13:03:39.466-05:00\",\"properties\":[{\"locked\":\"false\",\"attributeId\":\"name\",\"value\":\"SKUS"+currentTimeStamp()+"\",\"locale\":\"en_US\"}]}",properties.getProperty("sessionId"));
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		skuId=root.getJSONObject("sku").getString("identifier");
		System.out.println("skuId" +i+ " " +skuId);	
		}	
	}
	
	
	
	/*
	 ************************ Added on Dec 4th 2018 *****************************
	 */
	/*
	 *  Pancake menu click
	 */
	public void Menuclicktoselectstores()
	{
		if(driver.jsClickElement(CatalogPageObject.pancake_menu, "Pancake"))
		{
			logPass("Pancake is clicked");
		}
		else
		{
			logFail("Pancake is not clicked");
		}
	}
	
	
   /*
	*  Verify stores menu click from pancake 
	*/
	public void VerifyStoresClickFromPancake()
	{
		driver.pageReload();
		if(driver.isElementDisplayed(CatalogPageObject.Pancake_stores, "Stores from Pancake"))
		{
			logPass("Stores menu is displayed from Pancake");
			// pancake click
			if(driver.jsClickElement(CatalogPageObject.Pancake_stores, "Stores from Pancake"))
			{
				logPass("Stores menu is clicked from Pancake");
				// URL Navigation
				if(driver.getCurrentUrl().contains("stores"))
				{
					logPass("Stores page is displayed");
				}
				else
				{
					logFail("Stores page is not displayed");
				}
			
			}
			else
			{
				logFail("Stores menu is not clicked from Pancake");
			}
		}
		else
		{
			logFail("Stores menu is not sdisplayed from Pancake");
		}
	}
	
	/*
	 * Verify stores overview menu click from pancake
	 */
	public void VerifyStoresOverviewClickFromPancake()
	{
		try {
			//driver.pageReload();
			//Thread.sleep(5000);
			/*if(driver.isElementDisplayed(CatalogPageObject.Pancake_storeoverview, "Stores Overview from Pancake"))
			{
				logPass("Stores Overview menu is displayed from Pancake");*/
				// pancake click
			if(driver.explicitWaitforVisibility(CatalogPageObject.Pancake_stores, "Stores Overview from Pancake"))
			{
				if(driver.jsClickElement(CatalogPageObject.Pancake_stores, "Stores Overview from Pancake"))
				{
					logPass("Stores Overview menu is clicked from Pancake");
					// URL Navigation
					if(driver.getCurrentUrl().contains("stores"))
					{
						logPass("Stores page is displayed");
					}
					else
					{
						logFail("Stores page is not displayed");
					}
				
				}
				else
				{
					logFail("Stores Overview menu is not clicked from Pancake");
				}
			}
			else
				logFail("Stores Overview menu element is not clicked from Pancake");
				/*}
			else
			{
				logFail("Stores Overview menu is not displayed from Pancake");
			}*/
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	/*
	 *  Verify business logo navigation
	 */
	public void BusinessLogoNavigation()
	{
		try {
			//Thread.sleep(3000);
			if(driver.explicitWaitforVisibility(CatalogPageObject.Business_logo, "Business Logo"))
			{
			if(driver.isElementDisplayed(CatalogPageObject.Business_logo, "Business Logo"))
			{
				logPass("Business Logo is displayed");
				// pancake click
				if(driver.jsClickElement(CatalogPageObject.Business_logo, "Business Logo"))
				{
					logPass("Business Logo is clicked");
					// URL Navigation
					if(driver.getCurrentUrl().contains("stores"))
					{
						logPass("Business overview page is displayed");
					}
					else
					{
						logFail("Business overview  page is not displayed");
					}
				
				}
				else
				{
					logFail("Business Logo is not clicked");
				}
			}
			else
			{
				logFail("Business Logo is not displayed");
			}
			}
			else
				logFail("Business Logo element is not displayed");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	/*
	* Verify that Catalog Overview Page is navigated
	*/
	public void catalogsPageValidation()
	{
		try
		{
		//Thread.sleep(3000);
			if(driver.explicitWaitforVisibility(CatalogPageObject.Catalog_Title, "Catalog Title"))
			{
				if(driver.getCurrentUrl().contains("catalogs"))
				{
						if(driver.isElementDisplayed(CatalogPageObject.Catalog_Title, "Catalog Title"))
						{
							logPass("Catalog Overview Page is navigated");
						}
						else
						{
							logFail("Catalog Overview Page is not navigated");
						}
				} 
				else
				{
					logFail("Catalog Overview Page is not navigated");
				}
			}
			else
				logFail("Catalog Overview Page element is not displayed");
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			logFail("Catalog Overview Page is not navigated");
		}
		
	}
	
	
	/*
	 * Select & enter into the project
	 */
	public void SelectProjectandEnter()
	{
		try {
			//Thread.sleep(2000);
			if(driver.explicitWaitforVisibility(CatalogPageObject.Projects_Dropdown, "Projects Dropdown"))
			{
			if(driver.jsClickElement(CatalogPageObject.Projects_Dropdown, "Projects Dropdown"))
			{
				logPass("Project overlay is clicked");
				//Thread.sleep(3000);
				driver.explicitWaitforVisibility(CatalogPageObject.Enter_OpenProj, "Open Project is clicked");
				if(driver.jsClickElement(CatalogPageObject.Enter_OpenProj, "Open Project is available"))
				{
					logPass("Entered into open project");
				}
				else
				{
					logInfo("Open Project is not available for current selection");
				}
			}
			else
			{
				logFail("Project overlay is not clicked");
			}
			}
			else
				logFail("Project overlay element is not clicked");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	/*
	 *  Verify the project id comparison in project list page
	 */
	
	public void ProjectIdComparisonInProjectListPage()
	{
		try {
    		String currenturl1 = driver.getCurrentUrl();
        	logInfo("Current Url is : " + currenturl1);  
        	String projidsplit1[]=currenturl1.split("=");
        	String projid1=projidsplit1[5];
        	logInfo("Created Business Id is : " + projid1);  
        	//String ProjectID=data.getProperty("ProjectID");
        	String ProjIdUI = driver.getText(CatalogPageObject.Proj_Id_UI, "Project ID in UI");
        	 if(ProjIdUI.equals(projid1))
             {
        		 logPass("Project id Compared");
                 logInfo("Project id When Created : " + ProjIdUI);
                 logInfo("Project id in URL  : " + projid1);
             }
		} catch (Exception e) 
    	{
			// TODO: handle exception
	             logFail("Project id not compared");
	         }
	}
	
	
	 /*
	    * Verify newly created Project page Navigation
	    */
	    public void navigateToNewProjectPage()
	    {
	    	try
	    	{ 
	    		//Thread.sleep(3000);
	    		driver.scrollToElement(CatalogPageObject.Newproj_lastitem, "New Project");
	    		if(driver.explicitWaitforVisibility(CatalogPageObject.Newproj_lastitem, "New Project"))
	    		{
		    		if(driver.jsClickElement(CatalogPageObject.Newproj_lastitem, "New Project"))
		    		{
		    			logPass("Entered into newly created Project");
		    			
		    			//Thread.sleep(3000);
		    			driver.explicitWaitforVisibility(CatalogPageObject.Newproj_lastitem, "New Project");
		    			if(driver.isElementDisplayed(CatalogPageObject.Project_Page, "Project page"))
		    			{
		    				logPass("Project page is displayed");
		    			}
		    			else
		    			{
		    				logFail("Project page is not displayed");
		    			}
		    		}
		    		else
		    		{
		    			logFail("Not Entered into newly created Project");
		    		}
		    	}
	    	}
	    	catch (Exception e)
	    	{
	    		logFail("There is some issue in project page navigation. The Error is : " + e.getMessage());
	    	}
	    }
	
	
	    /*
	     *  Verify no data found for newly create project
	     */
	    
	    public void NoDataFoundCheckInPDP()
	    {
	    	try {
	    		if(driver.isElementDisplayed(CatalogPageObject.PDP_nodatafound, "No Data Found"))
		    	{
		    		logPass("No Data Found is displayed for newly created");
		    	}
		    	else
		    	{
		    		logInfo("No Data Found DOM is not avaialble for current selection");
		    	}
				
			} catch (Exception e) {
				// TODO: handle exception
				logFail("There is some issue in no data found DOM. The Error is : " + e.getMessage());
			}
	    	
	    }
	
	    /*
	     *  Choose approved project from dropdown
	     */
	    public void ChooseApprovedProjectFromDropdown()
		{
			if(driver.jsClickElement(CatalogPageObject.Projects_Dropdown, "Projects Dropdown"))
			{
				logPass("Project dropdown is selected");
				// Choose status drop down
				if(driver.jsClickElement(CatalogPageObject.Status_dropdown, "Status Dropdown"))
				{
					logPass("Status Dropdown is clicked");
					// Select approve project
					if(driver.jsClickElement(CatalogPageObject.Approved_Chkbox, "Approved Checkbox"))
					{
						logPass("Approved Checkbox is selected");
						// Check approved check box is selected
						if(driver.isElementDisplayed(CatalogPageObject.Approved_Chkbox_Selected, "Approved checkbox is selected"))
						{
							logPass("Approved checkbox is selected");
						}
						else
						{
							logFail("Approved checkbox is not selected");
						}
					}
					else
					{
						logFail("Approved Checkbox is selected");
					}
				}
				else
				{
					logFail("Status Dropdown is not clicked");
				}
				
				
			}
			else
			{
				logFail("Project dropdown is not selected");
			}
		}
	
	    /*
	     *  Select product randomly from product list page
	     */
	    public void RandomSelectFromProductList()
		{
	    	
	    	try 
	    	{
	    		if(driver.explicitWaitforVisibility(CatalogPageObject.product_active, "Product Count"))
	    		{
		    		int productsize = driver.getSize(CatalogPageObject.product_active, "Product Count");
		    		if(productsize>=1)
		    		{
			    	int randomselect = driver.generateRandomNumberWithLimit(productsize, 1);
			    	By randomSelectedProducts = By.xpath(CatalogPageObject.product_active.toString().replace("By.xpath: ", "").concat("[" + (randomselect) + "]"));
			        driver.moveToElementAndClick(randomSelectedProducts, "Random Product Select");
			        logPass("Product is selected randomly");
		    		}
		    		else
		    		{
		    			driver.jsClickElement(CatalogPageObject.product_active_1, "Product Select");
		    			logPass("Single product is available & it's selected");
		    		}
	    		}
			} catch (Exception e) 
	    	{
				// TODO: handle exception
			logFail("There is some issue in random product select. The Error is : " + e.getMessage());
				
			}
	    			
		}
	    
	    /*
	     *  Select product randomly from product list page
	     */
	    
	    public void RandomSelectFromProductListfromSKU()
		{
	    	
	    	try 
	    	{
	    		if(driver.explicitWaitforVisibility(CatalogPageObject.sku_active, "SKU Count"))
	    		{
	    		int productsize = driver.getSize(CatalogPageObject.sku_active, "SKU Count");
		    	int randomselect = driver.generateRandomNumberWithLimit(productsize, 1);
		    	By randomSelectedProducts = By.xpath(CatalogPageObject.sku_active.toString().replace("By.xpath: ", "").concat("[" + (randomselect) + "]"));
		        driver.jsClickElement(randomSelectedProducts, "Random Product Select");
		        logPass("Product is selected randomly");
	    		}
			} 
	    	catch (Exception e) 
	    	{
				// TODO: handle exception
				logFail("There is some issue in random product select. The Error is : " + e.getMessage());
				
			}
	    			
		}
	    /*
	     *  Verify no data not found 
	     */
	    /*
	     *  Verify no data found for newly create project
	     */
	    
	    public void NoDataNOTFoundCheckInPDP()
	    {
	    	if(driver.isElementNotDisplayed(CatalogPageObject.PDP_nodatafound, "No Data Found"))
	    	{
	    		logPass("Edited product is added in new project");
	    	}
	    	else
	    	{
	    		logFail("Edited product is not added in new project");
	    	}
	    }
	    
	    /*
	     * sku tab switch in product overview page
	     */
	    public void SKUtabswitchfromprojectoverviewpage()
	    {
		    try
	    	{
	    		
	    		if(driver.jsClickElement(CatalogPageObject.product_overview_sku, "SKU Tab"))
	    		{
	    			logPass("SKU Tab is selected in project overview page");
	    		}
	    		else
	    		{
	    			logFail("SKU Tab is not selected in project overview page");
	    		}
	    	}
		    catch (Exception e) 
	    	{
				// TODO: handle exception
				logFail("There is some issue in random product select. The Error is : " + e.getMessage());
				
			}
	    }
	    
	    /*
	     *************************** From Dec 5 ********************************
	     */
	    
	    /*
	     *   Choose created project from proj dropdown
	     */
	    
	    public void ChooseNewlyCreatedProjFromProjectDropdown()
	    {
	    	try {
	    		//Thread.sleep(2000);
	    		if(driver.explicitWaitforVisibility(CatalogPageObject.proj_status_dropdown, "Project Status dropdown"))
	    		{
	    		if(driver.jsClickElement(CatalogPageObject.proj_status_dropdown, "Project Status dropdown"))
		    	{
		    		logPass("Project dropdown is clicked");
		    		// Choose created project from dropdown
		    		try {
		    			if(driver.jsClickElement(CatalogPageObject.choose_created_proj, "Choose Created Project Dropdown"))
				    	{
				    		logPass("Created Project Dropdown is clicked");
				    		
				    		
				    	}
					} catch (Exception e) {
						// TODO: handle exception
						logFail("Created Project Dropdown is not clicked");
					}
		    	}
	    		}
	    		else
	    			logFail("Created Project Dropdown element is not clicked");
			} catch (Exception e) {
				// TODO: handle exception
				logFail("Project dropdown is not clicked");
			}
	    	
	    }
	    
	    /*
	     * Choose all approved proj from dropdown
	     */
	    public void ChooseAllApprovedProjectDropdown()
	    {
	    	try {
	    		if(driver.explicitWaitforVisibility(CatalogPageObject.all_approved_proj, "All Approved Project"))
	    		{
		    		if(driver.jsClickElement(CatalogPageObject.all_approved_proj, "All Approved Project"))
			    	{
			    		logPass("All Approved Project is clicked");
			    	
			    	}
			    	else
			    	{
			    		logFail("All Approved Project is not clicked");
			    	}
	    		}
			} catch (Exception e) {
				// TODO: handle exception
			}
	    	
	    }
	    
	    /*
	     * Choose all approved proj from dropdown
	     */
	    public void ChooseActiveStatusDropdown()
	    {
	    	try {
	    		if(driver.explicitWaitforVisibility(CatalogPageObject.active_status_click, "Active Dropdown"))
	    		{
		    		if(driver.jsClickElement(CatalogPageObject.active_status_click, "Active Dropdown"))
			    	{
			    		logPass("Status Active dropdown is clicked");
			    		if(driver.jsClickElement(CatalogPageObject.select_active_option, "Select Active Status"))
			    		{
			    			logPass("Active Status is selected");
			    		}
			    		else
			    			logFail("Active Status is not selected");
			    	
			    	}
			    	else
			    	{
			    		logFail("Status Active dropdown is not clicked");
			    	}
	    		}
			} catch (Exception e) {
				// TODO: handle exception
			}
	    	
	    }
	    
	    
	    /*
	     * Projects dropdown click
	     */
	    public void SelectProjectDropdown()
		{
			if(driver.jsClickElement(CatalogPageObject.Projects_Dropdown, "Projects Dropdown"))
			{
				logPass("Projects Dropdown is clicked");
			}
			else
			{
				logFail("Projects Dropdown is not clicked");
			}
		}
	    
	    /*
	     * Choose status dropdown
	     */
	    
	    public void ClickstatusDropdown()
		{
	    	try {
	    		if(driver.explicitWaitforVisibility(CatalogPageObject.overview_status_dropdown, "Projects Dropdown"))
	    		{
		    		if(driver.jsClickElement(CatalogPageObject.overview_status_dropdown, "Projects Dropdown"))
					{
						logPass("Projects Dropdown is clicked");
					}
					else
					{
						logFail("Projects Dropdown is not clicked");
					}
	    		}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
	    
	    /*
	     *  On Dec 6th
	     */
	    
	    /*
	     *  Get the edited value
	     */
	    
	    public void GetEditedName()
		{
			try
			{
				if(driver.isElementDisplayed(CatalogPageObject.Edited_item_txtbox, "Edited Item Value"))
				{
					EditedNameUI = driver.getElementAttribute(CatalogPageObject.Edited_item_txtbox, "value", "Edited Item Value");
					logPass("Edited Item Name is:" +EditedNameUI);
				}
				else
				{
					logFail("Edit item is not displayed");
				}

			} 
			catch (Exception e)
			{
				// TODO: handle exception
				logFail("There is some issue in product Edit... The Error is : " + e.getMessage());
			}
		}

	    /*
	     *  Check the added item value
	     */
	    
	    public void GetAddedItemValueInProjectTab()
		{
			try
			{
				if(driver.isElementDisplayed(CatalogPageObject.Added_item_txtbox, "Added Item Value"))
				{
					AddedNameUI = driver.getText(CatalogPageObject.Added_item_txtbox, "Added Item Value");
					logPass("Added Item Name is:" +AddedNameUI);
				}
				else
				{
					logFail("Added item is not displayed");
				}

			} 
			catch (Exception e)
			{
				// TODO: handle exception
				logFail("There is some issue in product Edit... The Error is : " + e.getMessage());
			}
		}

	    /*
	     *  check added SKU value
	     */
	    
	    public void GetAddedSKUItemValueInProjectTab()
		{
			try
			{
				Thread.sleep(3000);
				driver.scrollToElement(CatalogPageObject.Sku_Added_item_txtbox, "Added Item Value for SKU");
				if(driver.isElementDisplayed(CatalogPageObject.Sku_Added_item_txtbox, "Added Item Value for SKU"))
				{
					AddedNameUI = driver.getText(CatalogPageObject.Sku_Added_item_txtbox, "Added Item Value for SKU");
					logPass("Added SKU Item Name is:" +AddedNameUI);
				}
				else
				{
					logFail("Added SKU item is not displayed");
				}

			} 
			catch (Exception e)
			{
				// TODO: handle exception
				logFail("There is some issue in product Edit... The Error is : " + e.getMessage());
			}
		}
	    
	    /*
	     *  Compare edited & added item values
	     */
	    public void CompareAddedItemValues()
	    {
	    	if(EditedNameUI.equals(AddedNameUI))
	    	{
	    		logPass("Added Item is compared");
	    	}
	    	else
	    	{
	    		logFail("Added Item is not compared");
	    	}
	    }

	    
	    /*
	     *  Choose attribute type as dynamic
	     */
	    
	    public void ChooseDynamicTypeAttribute()
		{
	    	try 
	    	{
				if(driver.jsClickElement(CatalogPageObject.attr_type_drodwn, "Attribute Type Dropdown"))
		    	{
		    		logPass("Attribute Type Dropdown is clicked");
		    		
		    		if(driver.jsClickElement(CatalogPageObject.attr_dynamic_option, "Dynamic Type"))
		    		{
		    			logPass("Value dynamic is selected from type dropdown");
		    			//Thread.sleep(2000);
		    			if(driver.explicitWaitforVisibility(CatalogPageObject.Attribute_Name_Input, "Active Checkbox"))
		    			{
		    			driver.jsClickElement(CatalogPageObject.Attribute_Name_Input, "Active Checkbox");
		    			}
		    			else
		    				logFail("Active Checkbox element is not displayed");
		    		}
		    		else
		    		{
		    			logFail("Value dynamic is not selected from type dropdown");
		    		}
		    	}
		    	else
		    	{
		    		logFail("Attribute Type Dropdown is not clicked");
		    	}
	    	}
	    	catch (Exception e)
	    	{
			// TODO: handle exception
			logFail("Attribute Type Dropdown is not clicked");
	    	}
	    	
	    	
		}

	    /*
	     *  Enter the img URL
	     */
	    public void EnterImgLinkinAttribute()
		{
	    	if(driver.isElementDisplayed(CatalogPageObject.imgwithtext_txtbox, "Image With Text"))
			{
				//if(driver.enterText(ImagewithTextXpath, ImgUrl, "Image Link"))
				if(driver.enterTextAndSubmit(CatalogPageObject.imgwithtext_txtbox,ExcelReader.getData("CatalogPage","Data_3"),"Image With Text link"))
				{
					//logPass("Image Link is entered. The entered image link is : " + ImgUrl);
					logPass("Image Link is entered.");
				}
				else
				{
					logFail("The user failed to enter the Image Link");					
				}
			}
			
			else if (driver.isElementDisplayed(CatalogPageObject.img_txtbox, "Image"))
			{
				//if(driver.enterText(ImagewithTextXpath, ImgUrl, "Image Link"))
				if(driver.enterText(CatalogPageObject.img_txtbox,ExcelReader.getData("CatalogPage","Data_3"),"Image Link"))
				{
					//logPass("Image Link is entered. The entered image link is : " + ImgUrl);
					logPass("Image Link is entered.");
				}
				else
				{
					logFail("The user failed to enter the Image Link");					
				}
			}
	    	
		}

	    /*
		* Enter the Field Type for Image and Image with text
		*/
		public void selectImageandImagewithTextFieldTypeFromDropdown()
		{
			try
			{
				String random_FieldType = "";
				By dropdown_Selection = null, ImagewithTextXpath = null, ImageXpath = null;
				if(driver.jsClickElement(CatalogPageObject.Field_Type_Input, "Field Type"))
				{
					logPass("Field Type dropdown is clicked");
					int random_Field_Number = driver.generateRandomNumberWithLimit(2, 1);
					
					switch (random_Field_Number) 
					{
						case 1:
							random_FieldType = "ImageWithText";
							dropdown_Selection = CatalogPageObject.Field_Type_ImgWithTxt_List;
							//ImagewithTextXpath = CatalogPageObject.imgwithtext_txtbox;
							
							break;
							
						case 2:
							random_FieldType = "Image";
							dropdown_Selection = CatalogPageObject.Field_Type_Img_List;
							//ImageXpath = CatalogPageObject.img_txtbox;
							
							break;
					}
					
					if(driver.isElementDisplayed(dropdown_Selection, "Attributes Tab"))
					{
						logPass("The Randomly selected Field Type dropdown value is displayed. The Selected Dropdown value is : " + random_FieldType);
						
						if(driver.clickElement(dropdown_Selection, "Attributes Tab"))
						{
							logPass("The Randomly selected Field Type dropdown value is clicked");
							//Thread.sleep(2000);
							if(driver.explicitWaitforVisibility(CatalogPageObject.Attribute_Name_Input, "Active Checkbox"))
							{
			    			driver.jsClickElement(CatalogPageObject.Attribute_Name_Input, "Active Checkbox");
							}
							else
								logFail("Active Checkbox element is not displayed");
							//String ImgUrl = ExcelReader.getData("CatalogPage", "Data_3");
							
							//enterImgwithtextValidationTypeDetails(ImagewithTextXpath, ImageXpath, ImgUrl);
							//enterImgwithtextValidationTypeDetails(ImagewithTextXpath, ImageXpath);
							
						}
						else
						{
							logFail("The Randomly selected Field Type dropdown value is not clicked");
						}
					}
					else
					{
						logFail("The Randomly selected Field Type dropdown value is not displayed.  The Selected Dropdown value is : " + random_FieldType);
					}
				}
				else
				{
					logFail("Field Type dropdown is not clicked");
				}
			} 
			catch (Exception e)
			{
				logInfo("There is some issue in clicking the Field Type dropdown. The Error is : " + e.getMessage());
			}
		}
	    


		//public void enterImgwithtextValidationTypeDetails(By ImagewithTextXpath, By ImageXpath, String ImgUrl)
		public void enterImgwithtextValidationTypeDetails(By ImagewithTextXpath, By ImageXpath)
		{
			try
			{
				ImagewithTextXpath = CatalogPageObject.imgwithtext_txtbox;
				ImageXpath = CatalogPageObject.img_txtbox;
				
				
				Thread.sleep(3000);
				if(driver.isElementDisplayed(ImagewithTextXpath, "Image With Text"))
				{
					//if(driver.enterText(ImagewithTextXpath, ImgUrl, "Image Link"))
					if(driver.enterTextAndSubmit(ImagewithTextXpath,ExcelReader.getData("CatalogPage","Data_3"),"Image With Text link"))
					{
						//logPass("Image Link is entered. The entered image link is : " + ImgUrl);
						logPass("Image Link is entered.");
					}
					else
					{
						logFail("The user failed to enter the Image Link");					
					}
				}
				
				else if (driver.isElementDisplayed(ImageXpath, "Image"))
				{
					//if(driver.enterText(ImagewithTextXpath, ImgUrl, "Image Link"))
					if(driver.enterTextAndSubmit(ImageXpath,ExcelReader.getData("CatalogPage","Data_3"),"Image Link"))
					{
						//logPass("Image Link is entered. The entered image link is : " + ImgUrl);
						logPass("Image Link is entered.");
					}
					else
					{
						logFail("The user failed to enter the Image Link");					
					}
				}
				
			} 
			catch (Exception e)
			{
				logFail("There is some issue in entering the Img url. The Error is : " + e.getMessage());
			}
		}

		/*
		 *  Verify the assets container 
		 */
		 public void VerifyAssetsContainer(By AssetProdImg, By AssetProdTxt, By AssetProdLink)
		 {
			 if(driver.isElementDisplayed(CatalogPageObject.Assets_container, "Assets Container"))
			 {
				 logPass("Assets Container is displayed");
				
				 try {
					 AssetProdImg = CatalogPageObject.Assets_altimg;
					 AssetProdTxt = CatalogPageObject.Assets_alttext;
					 AssetProdLink = CatalogPageObject.Assets_link;
					 
					 
					 // Asset Alt Image
					 String altimg = driver.getText(AssetProdImg, "Alt Image");
					 if(altimg.contains("Alt Image"))
					 {
						 logPass("Asset Alt Image is compared");
					 }
					 else
					 {
						 logFail("Asset Alt Image is not compared");
					 }
					 
					 // Asset Alt Text
					 String alttxt = driver.getText(AssetProdTxt, "Alt Text");
					 if(alttxt.contains("Alt Text"))
					 {
						 logPass("Asset Alt Text is compared");
					 }
					 else
					 {
						 logFail("Asset Alt Text is not compared");
					 }
					 
					 // Asset Link
					 String altlink = driver.getText(AssetProdLink, "Alt Link");
					 if(altlink.contains("Link"))
					 {
						 logPass("Asset Link is compared");
					 }
					 else
					 {
						 logFail("Asset Link is not compared");
					 }
				} 
				 catch (Exception e) 
				 {
					// TODO: handle exception
					 logFail("There is some issue in assets display... The Error is : " + e.getMessage());
				}
				 
				
			 }
			 else
			 {
				 logFail("Assets Container is not displayed");
			 }
			 
		 }
		
		 /*
		  *  Click on assets tab
		  */
		 
		 public void AssetsTabClick()
		 {
			 try {
				 //Thread.sleep(4000);
				 if(driver.explicitWaitforVisibility(CatalogPageObject.Assets_tab, "Assets Tab"))
				 {
					 if(driver.jsClickElement(CatalogPageObject.Assets_tab, "Assets Tab"))
					 {
						 logPass("Assets Tab is clicked");
					 }
					 else
					 {
						 logFail("Assets Tab is not clicked");
					 }
				 }
				 else
					 logFail("Assets Tab element is not displayed");
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		 }

		 
		 /* Product/sku's menu click in the default project */	

			public void SKUsTabSelectinAddSkuPage()
			{
				
				try
				{
					if(driver.explicitWaitforVisibility(CatalogPageObject.product_tab_SKU, "Sku menu "))
					{
						Thread.sleep(3000);
						if(driver.isElementDisplayed(CatalogPageObject.product_tab_SKU, "Sku menu "))
						{
							logPass("Sku menu is displayed");
							if(driver.jsClickElement(CatalogPageObject.product_tab_SKU, "Sku menu"))
							{
								if(driver.getCurrentUrl().contains("skus"))
								{
								logPass("Sku menu is clicked");
								}
								else
									logFail("Sku menu is not clicked");
								
							}
							else
							{
								logFail("Sku menu is not clicked");
							}
						}
						else
						{
							logInfo("Sku menu is not available for current selection");
						}
					}
				
				}
				catch (Exception e)
				{
					logFail("There is some issue in Sku menu click : " + e.getMessage());
				}
			}
		 
			/*
			 *  Assets tab help text comparison
			 */
		 
			 public void VerifyHelpTextDisplay(By skuhelptxt)
			 {
				 if(driver.isElementDisplayed(CatalogPageObject.add_sku_hlptxt, "Add SKU help text"))
				 {
					 logPass("Add SKU help text is displayed");
					
					 try {
						 skuhelptxt = CatalogPageObject.add_sku_hlptxt;
						 // Asset Alt Image
						 String HELPTEXT = driver.getText(skuhelptxt, "Help Text");
						 if(HELPTEXT.contains("ADD SKU"))
						 {
							 logPass("Help Text is compared and compared text is :" +HELPTEXT);
						 }
						 else
						 {
							 logFail("Help Text is not compared");
						 }
					 }
					 catch (Exception e)
					 {
						 logFail("There is some issue in help text comparison : " + e.getMessage());
					 }
				 }
			 }
				 
		 /*
		  *  click on add SKU button
		  */
		 public void AddSKUButtonClick()
		 {
			 try 
			 {
				driver.explicitWaitforVisibility(CatalogPageObject.add_sku_btn, "Add SKU button");
				if(driver.jsClickElement(CatalogPageObject.add_sku_btn, "Add SKU button"))
				{
					logPass("Add SKU button is clicked");
					if(driver.getCurrentUrl().contains("skus"))
					{
						logPass("SKUs page is navigated");
					}
					else
					{
						logFail("SKUs page is not navigated");
					}
				}
				else
				{
					logFail("Add SKU button is not clicked");
				}
			 } 
			 catch (Exception e)
			 {
				// TODO: handle exception
				 logFail("There is some issue in add sku button click : " + e.getMessage());
			 }
		 }
		
		 
		 /*
		  *  Add the SKU in the sku's overlay window
		  */
		 public void AddSKUfromOverlayatRightCorner()
		 {
			 try
			 {
				if(driver.isElementDisplayed(CatalogPageObject.sku_item_container, "SKU item container"))
				{
					logPass("SKU item container is displayed");
					
					if(driver.isElementDisplayed(CatalogPageObject.prd_add_upsell, "Add SKU button"))
					{
						int sku_size = driver.getSize(CatalogPageObject.prd_add_upsell, "SKU Item List");
						int skurand = driver.generateRandomNumberWithLimit(sku_size, 1);
						By skuclick = By.xpath("(" + CatalogPageObject.prd_add_upsell.toString().replace("By.xpath: ", "").concat(")[" + (skurand) + "]"));
					    driver.jsClickElement(skuclick, "Add sku is clicked");  
					    logPass("Sku item is clicked");
					    
					}
					else
					{
						logFail("Sku item is not clicked");
					}
				}
				else
				{
					logFail("SKU item container is not displayed");
				}
			 } 
			 
			 catch (Exception e) 
			 {
				// TODO: handle exception
				 logFail("There is some issue in sku item container display : " + e.getMessage());
			}
		 }
		 
		 /*
		  *  Delete added sku list
		  */
		 public void DeleteAddedskulist() 
		    {
			 try {
				 if(driver.explicitWaitforVisibility(CatalogPageObject.deleteicon_count, "Delete icon"))
				 {
				 if(driver.isElementDisplayed(CatalogPageObject.deleteicon_count, "Delete icon"))
					{
						logPass("Delete icons available");
						if(driver.jsClickElement(CatalogPageObject.deleteicon_count, "Delete icon"))
						{
							logPass("Delete icon is clicked and added sku is deleted");
							SaveAddedSkuList();
						}
						else
						{
							logFail("Delete icon is not clicked and added sku is not deleted");
						}
					}
					else
					{
						logInfo("Delete icons are not available for current selection");
					}
				 }
			} catch (Exception e) {
				// TODO: handle exception
			}
					
		    }
		 
		 /*
		  *  Save the added sku
		  */
		 public void SaveAddedSkuList() 
		 {
			 if(driver.jsClickElement(CatalogPageObject.addsku_save_btn, "Add SKU save button"))
			 {
				 logPass("added sku is saved");
			 }
			 else
			 {
				 logFail("added sku is not saved");
			 }
		 }
		 
		 /*
		  *  Get the added SKU ID
		  */
		 public void GetAddedSKUValue() 
		 {
			 try
				{
					if(driver.isElementDisplayed(CatalogPageObject.get_skuid, "SKU Id"))
					{
						get_skuid = driver.getText(CatalogPageObject.get_skuid, "SKU Id");
						logPass("Added Sku id is:" +get_skuid);
					}
					else
					{
						logFail("Added sku is not displayed");
					}

				} 
				catch (Exception e)
				{
					// TODO: handle exception
					logFail("There is some issue in sku add... The Error is : " + e.getMessage());
				}
		 }
		 
		 
		 	/*
		     *  Choose approved project from dropdown
		     */
		    public void selectApprovedProject()
			{
		    	try {
		    		if(driver.explicitWaitforVisibility(CatalogPageObject.proj_dropdwn, "Status Dropdown"))
		    		{
		    		// Choose status drop down
					if(driver.jsClickElement(CatalogPageObject.proj_dropdwn, "Status Dropdown"))
					{
						logPass("Status Dropdown is clicked");
						// Select approve project
						if(driver.jsClickElement(CatalogPageObject.approved_option, "Approved Checkbox"))
						{
							logPass("Approved project is selected");
							// Check approved check box is selected
						}
						else
						{
							logFail("Approved project is not selected");
						}
					}
					else
					{
						logFail("Status Dropdown is not clicked");
					}
		    		}
				} catch (Exception e) {
					// TODO: handle exception
				}
					
					
					
				}
			/*
			 *  Search the SKU id
			 */
		    
		    public void SearchAddedSkuIDandAssertSearchValue()
			{
		    	try 
		    	{
		    		if(driver.explicitWaitforVisibility(CatalogPageObject.sku_id_drpdwn, "SKU Id dropdown"))
		    		{
		    		if(driver.jsClickElement(CatalogPageObject.sku_id_drpdwn, "SKU Id dropdown"))
			    		{
		    			//Thread.sleep(3000);
			    		logPass("SKU Id dropdown is selected");
			    		if(driver.explicitWaitforVisibility(CatalogPageObject.sku_id_txbox_fr_search, "Sku Id Search"))
			    		{
				    		if(driver.enterText(CatalogPageObject.sku_id_txbox_fr_search, get_skuid, "Sku Id Search"))
				    		{
				    			logPass("Added sku id is entered for search");
				    			if(driver.jsClickElement(CatalogPageObject.skuid_search_btn, "SKU Id Search button"))
				    			{
				    				logPass("SKU Id Search button is clicked");
				    			}
				    			else
				    			{
				    				logFail("SKU Id Search button is not clicked");
				    			}
				    			Thread.sleep(5000);
				    			
				    			// Search value assertion
				    			String searchvalue = driver.getText(CatalogPageObject.skuid_value_after_search, "Search Result of Sku Id");
				    			Thread.sleep(2000);
				    			if(searchvalue.equals(get_skuid))
		    					{
				    				logPass("Search value is equal");
		    					}
				    			else
				    			{
				    				logInfo("Search value is not equal for current search");
				    			}
				    		}
				    		else
				    		{
				    			logFail("Added sku id is not entered for search");
				    		}
			    		} 
			    		else
				    	{
				    		logFail("SKU Id dropdown is not selected");
				    	}
			    		}
		    	}
		    		
		    	}
		    		
		    		catch (Exception e) 
		    		{
					// TODO: handle exception	
		    			logFail("There is some issue in sku id search & assertion... The Error is : " + e.getMessage());
		    		}
		 
			}	 
		 
		    /*
		     * click attributes tab from product page
		     */
		    public void SelectAttributesFromProductPage()
			{
				if(driver.isElementDisplayed(CatalogPageObject.attr_tab, "Attribute"))
				{
					logPass("Attribute tab is displayed");
					// Click product
					if(driver.jsClickElement(CatalogPageObject.attr_tab, "Attribute Tab"))
					{
						logPass("Attribute Tab is clicked");
					}
					else
					{
						logFail("Attribute Tab is not clicked");
					}
				}
				else
				{
					logFail("Attribute tab is not displayed");
				}
			}
		 
		    /*
		     *  Click on add button to select attribute
		     */
		    public void ClickAddButtonInAttributesPage()
			{
		    	try {
		    		if(driver.explicitWaitforVisibility(CatalogPageObject.attr_add_btn, "Add button"))
		    		{
			    		if(driver.clickElement(CatalogPageObject.attr_add_btn, "Add button"))
				    	{
				    		logPass("Add button is clicked in attribute page");
				    	}
				    	else
				    	{
				    		logFail("Add button is not clicked in attribute page");
				    	}
		    		}
		    		else
		    		{
		    			logFail("Add button is not displayed");
		    		}
				} catch (Exception e) {
					// TODO: handle exception
				}
		    	
			}
		    
		    /*
		     *  add after adding attribute
		     */
		    public void ClickAddButtonAfteraddingAttribute()
			{
		    	if(driver.clickElement(CatalogPageObject.addbtn_sce2, "Add button"))
		    	{
		    		logPass("Add button is clicked in attribute page");
		    	}
		    	else
		    	{
		    		logFail("Add button is not clicked in attribute page");
		    	}
			}
		    
		    
		    
		    /*
		     * Search the attribute and enter
		     */
		    public void SearchAttributeandEnter()
			{ 	try {
				if(driver.explicitWaitforVisibility(CatalogPageObject.attr_txtbox, "Attr Text box"))
				{
					if(driver.enterText(CatalogPageObject.attr_txtbox, ExcelReader.getData("CatalogPage", "Data_1"), "attribute to search"))
			    	{
			    		logPass("search element is entered from excel");
				    		/*if(driver.jsClickElement(CatalogPageObject.attr_select_option, "search element"))
				    		{
				    			logPass("search element is clicked");
				    			//ClickAddButtonAfteraddingAttribute();
				    		}
				    		else
				    		{
				    			logFail("search element is not clicked");
				    		}*/
			    	}
			    	else
			    	{
			    		logFail("search element is not entered from excel");
			    	}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		    	
			}
		    
		    /*
		     *  Enter desc text
		     */
		    public void EnterDescText()
			{ 	
		    	if(driver.enterText(CatalogPageObject.desc_text, ExcelReader.getData("CatalogPage", "Data_1"), "attribute to search"))
		    	{
		    		logPass("Desc text is entered");
		    		SaveAddedSkuList();
		    	}
		    	else
		    	{
		    		logFail("Desc text is not entered");
		    	}
			}
		    
		    /*
		     *  check the cancel & save button display
		     */
		    public void verifyCancelSaveButtonsDisplay()
			{
		    	try
		    	{
		    		String cancel_btn = driver.getText(CatalogPageObject.attr_cancel_btn, "cancel button");
			    	String save_btn = driver.getText(CatalogPageObject.attr_save_btn, "save button");
			    	
				    	if(cancel_btn.contains("Cancel"))
				    	{
				    		logPass("Cancel button is displayed");
				    	}
				    	else
				    	{
				    		logFail("Cancel button is not displayed");
				    	}
				    	
				    	if(save_btn.contains("Save"))
				    	{
				    		logPass("Save button is displayed");
				    	}
				    	else
				    	{
				    		logFail("Save button is not displayed");
				    	}
				} 
		    	catch (Exception e) 
		    	{
					// TODO: handle exception
		    		logFail("There is some issue in cancel and save button display... The Error is : " + e.getMessage());
				}
		    	
		    	
			}
		    
		    /*
		     *  delete the added attribute
		     */
		    public void DeleteAddedAttributesList() 
		    {
		    	if(driver.explicitWaitforVisibility(CatalogPageObject.del_attr_btn, "Delete icon"))
		    	{
					if(driver.isElementDisplayed(CatalogPageObject.del_attr_btn, "Delete icon"))
					{
						logPass("Delete icons available");
						// Random product select
						int upsell_size = driver.getSize(CatalogPageObject.del_attr_btn, "Delete icon");
						
							for(int i=1;i<=upsell_size;i++)
							{
								By deleteproduct = By.xpath("(" + CatalogPageObject.del_attr_btn.toString().replace("By.xpath: ", "").concat(")[" + (1) + "]"));
								driver.scrollToElement(deleteproduct, "Delete product");
								driver.jsClickElement(deleteproduct, "Delete added products");
							    logPass("Added property " + i + " is deleted");
							}
					}
					else
					{
						logInfo("Delete icons are not available for current attribute selection");
					}
		    	}
		    }
		    
		    /*
		     *  check no item added text display
		     */
		    public void noPropertiesAvailableTextDisplay() 
		    {
		    	try {
		    		//Thread.sleep(2000);
		    		/*if(driver.explicitWaitforVisibility(CatalogPageObject.noproperty_txt, "No Property Text"))
		    		{*/
		    		String prop = driver.getText(CatalogPageObject.Noitemfound, "No Property Text");
			    	if(prop.contains("ADD SKUS"))
			    	{
			    		logPass("Added property is deleted");
			    	}
			    	else
			    	{
			    		logInfo("Added property is not available to delete");
			    	}
			    	/*}
		    		else
		    			logFail("Added property element is not displayed");*/
				} catch (Exception e) {
					// TODO: handle exception
				}
		    	
		    	
		    }
		    
		    /*
		     *  edit flow check in overview page
		     */
		    public void VerifyEditPageLinkandNavigationforImage() 
		    {
		    	try 
		    	{
		    		if(driver.explicitWaitforVisibility(CatalogPageObject.edit_img_asset, "Image Asset Asset"))
		    		{
			    		if(driver.jsClickElement(CatalogPageObject.edit_img_asset, "Image Asset Asset"))
				    	{
				    		logPass("Image edit icon is clicked");
				    		if(driver.getCurrentUrl().contains("assets"))
				    		{
				    			logPass("assets page is displayed");
				    			driver.navigateBack();
				    		}
				    		else
				    		{
				    			logFail("assets page is not displayed");
				    		}
				    	}
				    	else
				    	{
				    		logFail("Image edit icon is not clicked");
				    	}
		    		}
		    		
				} 
		    	catch (Exception e) 
		    	{
					// TODO: handle exception
		    		logFail("There is some issue in asset edit... The Error is : " + e.getMessage());
				}
		    	
		    }
		    
		    /*
		     *  Name
		     */
		    
		    public void VerifyEditPageLinkandNavigationforName() 
		    {
		    	try 
		    	{
		    		if(driver.jsClickElement(CatalogPageObject.edit_name_asset, "Name Asset"))
			    	{
			    		logPass("Name edit icon is clicked");
			    		if(driver.getCurrentUrl().contains("attributes"))
			    		{
			    			logPass("attributes page is displayed");
			    			driver.navigateBack();
			    		}
			    		else
			    		{
			    			logFail("attributes page is not displayed");
			    		}
			    	}
			    	else
			    	{
			    		logFail("Name edit icon is not clicked");
			    	}
				} 
		    	catch (Exception e) 
		    	{
					// TODO: handle exception
		    		logFail("There is some issue in asset edit... The Error is : " + e.getMessage());
				}
		    	
		    }
		    
		    
		    /*
		     *  Properties
		     */
		    
		    public void VerifyEditPageLinkandNavigationforProperties() 
		    {
		    	try 
		    	{
		    		if(driver.jsClickElement(CatalogPageObject.edit_prop_asset, "Properties Asset"))
			    	{
			    		logPass("Properties edit icon is clicked");
			    		if(driver.getCurrentUrl().contains("attributes"))
			    		{
			    			logPass("attributes page is displayed");
			    			driver.navigateBack();
			    		}
			    		else
			    		{
			    			logFail("attributes page is not displayed");
			    		}
			    	}
			    	else
			    	{
			    		logFail("Properties edit icon is not clicked");
			    	}
				} 
		    	catch (Exception e) 
		    	{
					// TODO: handle exception
		    		logFail("There is some issue in asset edit... The Error is : " + e.getMessage());
				}
		    	
		    }
		    
		    /*
		     * Desc
		     */
		    
		    public void VerifyEditPageLinkandNavigationforDesc() 
		    {
		    	try 
		    	{
		    		if(driver.jsClickElement(CatalogPageObject.edit_desc_asset, "Description Asset"))
			    	{
			    		logPass("Description edit icon is clicked");
			    		if(driver.getCurrentUrl().contains("attributes"))
			    		{
			    			logPass("attributes page is displayed");
			    			driver.navigateBack();
			    		}
			    		else
			    		{
			    			logFail("attributes page is not displayed");
			    		}
			    	}
			    	else
			    	{
			    		logFail("Properties edit icon is not clicked");
			    	}
				} 
		    	catch (Exception e) 
		    	{
					// TODO: handle exception
		    		logFail("There is some issue in asset edit... The Error is : " + e.getMessage());
				}
		    	
		    }
		    
		    /*
		     * Details
		     */
		    
		    public void VerifyEditPageLinkandNavigationforDetails() 
		    {
		    	try 
		    	{
		    		if(driver.jsClickElement(CatalogPageObject.edit_details_asset, "Details Asset"))
			    	{
			    		logPass("Details edit icon is clicked");
			    		if(driver.getCurrentUrl().contains("attributes"))
			    		{
			    			logPass("attributes page is displayed");
			    			driver.navigateBack();
			    		}
			    		else
			    		{
			    			logFail("attributes page is not displayed");
			    		}
			    	}
			    	else
			    	{
			    		logFail("Details edit icon is not clicked");
			    	}
				} 
		    	catch (Exception e) 
		    	{
					// TODO: handle exception
		    		logFail("There is some issue in asset edit... The Error is : " + e.getMessage());
				}
		    	
		    }
		    
		    
		    /*
		     *  Navigate to SEO tab
		     */
		    public void clickOnSEOTab() 
		    {
		    	if(driver.explicitWaitforVisibility(CatalogPageObject.SEO_tab, "SEO TAB"))
		    	{
		    	if(driver.moveToElementAndClick(CatalogPageObject.SEO_tab, "SEO TAB"))
		    	{
		    		logPass("SEO Tab is clicked");
		    		if(driver.getCurrentUrl().contains("seo"))
		    		{
		    			logPass("SEO page is displayed");
		    		}
		    		else
		    		{
		    			logFail("SEO page is not displayed");
		    		}
		    	}
		    	else
		    	{
		    		logFail("SEO Tab is not clicked");
		    	}
		    	}
		    	else
		    		logFail("SEO Tab element is not displayed");
		    }
		    
		    /*
		     *  Enter values in semantic id
		     */
		    
		    public void enterValuesinSemanticId() 
		    {
		    	try {
		    		
		    		String SEO_name = generateRandomStrings(5);
			    	if(driver.enterText(CatalogPageObject.semanticid_txtbox, SEO_name, "Semantic Id"))
			    	{
			    		logPass("Semantic Id is entered");
			    		if(driver.jsClickElement(CatalogPageObject.semanticid_enterbtn, "Enter Button"))
			    		{
			    			logPass("Enter button is clicked");
			    			if(driver.isElementDisplayed(CatalogPageObject.added_semantics_DOM, "Added Semantics"))
			    			{
			    				logPass("Semantic id is added");
			    			}
			    			else
			    			{
			    				logFail("Semantic id is not added");
			    			}
			    		}
			    		else
			    		{
			    			logFail("Enter button is not clicked");
			    		}
			    		
			    	}
			    	else
			    	{
			    		logFail("Semantic Id is not entered");
			    	}
					
				} catch (Exception e) 
		    	{
					// TODO: handle exception
					logFail("There is some issue in entering semantic id : " + e.getMessage());
				}
		    	
		    }
		    
		    /*
		     *  Enter values in keywords
		     */
		    public void enterValuesinkeywords() 
		    {
		    	try {
		    		
		    		String SEO_name = generateRandomStrings(5);
			    	if(driver.enterText(CatalogPageObject.keywrd_txtbox, SEO_name, "Semantic keyword"))
			    	{
			    		logPass("Semantic Id is entered");
			    		if(driver.jsClickElement(CatalogPageObject.keywrd_enterbtn, "Enter Button"))
			    		{
			    			logPass("Enter button is clicked");
			    			if(driver.isElementDisplayed(CatalogPageObject.keywords_DOM, "Added Keywords"))
			    			{
			    				logPass("Semantic keyword is added");
			    			}
			    			else
			    			{
			    				logFail("Semantic keyword is not added");
			    			}
			    		}
			    		else
			    		{
			    			logFail("Enter button is not clicked");
			    		}
			    		
			    	}
			    	else
			    	{
			    		logFail("Semantic keyword is not entered");
			    	}
					
				} catch (Exception e) 
		    	{
					// TODO: handle exception
					logFail("There is some issue in entering semantic id : " + e.getMessage());
				}
		    	
		    }
		    
		    /*
		     *  Enter description
		     */
		    public void enterdescinSEOpage() 
		    {
		    	try {
		    		
		    		String SEO_name = generateRandomStrings(5);
			    	if(driver.enterText(CatalogPageObject.desc_txtbox, SEO_name, "SEO description"))
			    	{
			    		logPass("SEO description is entered");
			    		
			    	}
			    	else
			    	{
			    		logFail("SEO description is not entered");
			    	}
					
				} catch (Exception e) 
		    	{
					// TODO: handle exception
					logFail("There is some issue in entering semantic id : " + e.getMessage());
				}
		    	
		    }
		    
		    /*
		     *  Save entered SEO details
		     */
		    public void saveSEOdetails() 
		    {
		    	if(driver.jsClickElement(CatalogPageObject.seo_savebtn, "SEO Save Button"))
		    	{
		    		logPass("Save button is clicked");
		    	}
		    	else
		    	{
		    		logFail("Save button is not clicked");
		    	}
		    }
		    
		    /*
		     *  Added on Mar 19th - Search the added keyword
		     */
		    /*Search Product Id */
			public void searchSKU() throws InterruptedException
			{
				if(driver.jsClickElement(CatalogPageObject.productIdList, "Product Id"))
				{
					pass("Able to click product id Filter");
					if(driver.explicitWaitforVisibility(CatalogPageObject.productIdSearchInput, "Product Id"))
					{
						if(driver.enterText(CatalogPageObject.productIdSearchInput, FinalBundleId, "Product Id"))
						{
							pass("Able to enter the product id ");
							if(driver.jsClickElement(CatalogPageObject.searchBtn, "search button"))
								pass("Able to click search button");
							else 
								fail("Not able to click Search button");
						}
						else 
							fail("Not able to enter product id ");
					}
				}
				else
					fail("Not able to click Product id filter");
			}
			
			public void navigateToSearchedProduct()
			{
				try {
					if(driver.explicitWaitforVisibility(CatalogPageObject.Prod_Container, "Product Container"))
					{
					if(driver.isElementDisplayed(CatalogPageObject.Prod_Container, "Product Container"))
					{
						logPass("Product list is shown");
						driver.jsClickElement(CatalogPageObject.Random_Product_Select, "Product Select");
						logPass("Navigated to searched item");
					}
					else
					{
						logFail("Product list is not shown");
					}
				} 
				}catch (Exception e) {
					// TODO: handle exception
					logFail("There is some issue in product selection. The Error is : " + e.getMessage());
				}	
			}
			
			/*
			 * Alert popup window handler
			 */
			public void alertWindowHandler()
			{
				try {
					if(driver.isElementDisplayed(CatalogPageObject.alrt_popup, "Alert Popup"))
					{
						driver.jsClickElement(CatalogPageObject.popup_yes, "Yes button in popup");
						logPass("Alert Pop-up is clicked");
					}
					else
					{
						logInfo("Alert window is not available to display");
					}
				} catch (Exception e) {
					// TODO: handle exception
					logFail("Alert Pop-up is not clicked");
				}
			}

			/*
			 * Added on Mar 21st by Priya
			 */
			
			public void verifyRelationsTabDisplay()
			{
				try {
					driver.explicitWaitforVisibility(CatalogPageObject.Relations_Tab, "Relations Tab");
					if(driver.isElementDisplayed(CatalogPageObject.Relations_Tab, "Relations Tab"))
					{
						logPass("Relations Tab is displayed");
					}
					else
					{
						logFail("Relations Tab is not displayed");
					}
					
				} catch (Exception e) {
					// TODO: handle exception
				}
			}


}

