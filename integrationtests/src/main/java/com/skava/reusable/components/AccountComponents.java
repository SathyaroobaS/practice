package com.skava.reusable.components;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.framework.reporting.BaseClass;
import com.skava.frameworkutils.ExcelReader;
//import com.skava.networkutils.ApiReaderUtils;
import com.skava.object.repository.AccountAdminObjects;



public class AccountComponents extends OrderDetailPageComponents 
{
	public void accounttypefilter() throws InterruptedException 
    {
		driver.explicitWaitforVisibility(AccountAdminObjects.btn_search, "Button");
		driver.jsClickElement(AccountAdminObjects.btn_search, "Element");
		driver.explicitWaitforClickable(AccountAdminObjects.acc_type,"Account Type");
		Thread.sleep(10000);
    
    	if(driver.moveToElementAndClick(AccountAdminObjects.acc_type,"Account Type"))
//    	Thread.sleep(3000);
   
    	driver.enterText(By.xpath("AccountAdminObjects.defense_txt"), "defense", "Text entered");
    	Thread.sleep(3000);
    	driver.clickElement(AccountAdminObjects.accountpagesearch, "search clicked");
    	if(driver.explicitWaitforVisibility(AccountAdminObjects.accountlistitem, "Account list item displayed"))
    	{
    		pass("accounts displayed for account type filtered");
    	}
    	else if(driver.explicitWaitforVisibility(AccountAdminObjects.accountnodataalert, "No Data found"));
    		
    }
	
	public void accountstatusfilter() throws InterruptedException 
    {
		driver.explicitWaitforVisibility(AccountAdminObjects.acc_btn, "Button");
		driver.actionClickElement(AccountAdminObjects.acc_btn, "Element");
		driver.explicitWaitforVisibility(AccountAdminObjects.accountstatusfilter,"");
		
//    	driver.moveToElementAndClick(AccountAdminObjects.accountstatusfilter, "Account status filter select");
   	
    	Thread.sleep(500);
    	driver.enterText(AccountAdminObjects.active_btn, "Active", "Text  entered");
    	driver.clickElement(AccountAdminObjects.accountpagesearch, "search clicked");
    	if(driver.explicitWaitforVisibility(AccountAdminObjects.accountlistitem, "Account list item displayed"))
    	{
    		pass("accounts displayed for account type filtered");
    	}
    	else if(driver.explicitWaitforVisibility(AccountAdminObjects.accountnodataalert, "No Data found"));
    }
	
	public void createaccount() 
	{
		if(driver.explicitWaitforVisibility(AccountAdminObjects.createAccountBtnTrigger, "Create account button"))
		{
			if(driver.jsClickElement(AccountAdminObjects.createAccountBtnTrigger, "Create Account button clicked"))
			{
				pass("Create Account clicked");
			}
			else
				fail("Create Account not clicked");
		}
		
	}
		
	public void verifyaccounttitle()
	{
		if(driver.explicitWaitforVisibility(AccountAdminObjects.accounttitle, "Account title displayed")) {
			pass("Account title displayed");
		}
		else
			fail("Account title not displayed");
	}
	
	public void enteraccountname()
	{
		if(driver.clickElement(AccountAdminObjects.createAccountNameContainer, "Account name container clicked")) {
			pass("Account name container clicked");
		}
		else 
			fail("Account name container not clicked");

		if(driver.enterText(AccountAdminObjects.createAccountNameInput, ExcelReader.getData("Accountpage", "Accountname"), "Accountname entered")) {
			pass("Account name entered");
		}
		else 
			fail("Account name not entered");
	}	
	
	public void enterbuyeremail()
	{
		if(driver.clickElement(AccountAdminObjects.createAccountBuyerEmailContainer, "Account buyer container clicked")) {
			pass("Account buyer container clicked");
		}
		else
			fail("Account buyer connatiner not clicked");
		
			String email = ExcelReader.getData("Accountpage", "Buyeremail");
			int randomnumber=driver.generateRandomNumber(1000);
			String finalbuyermail = email+Long.toString(System.currentTimeMillis())+Integer.toString(randomnumber);
			String finalemail= finalbuyermail+"@ymail.com";
		if(driver.enterText(AccountAdminObjects.createAccountBuyerEmailInput,finalemail,"Buyer email entered"))
		{
		    pass("Buyer email entered");
		}
		else
			fail("Buyer email not entered");
	}
	
	public void enterfirstname()
	{
	
		if(driver.clickElement(AccountAdminObjects.createAccountFirstNameContainer, "First name container clicked")) {
			pass("First name container clicked");
		}
		else
			fail("First name container not clicked");
		
		if(driver.enterText(AccountAdminObjects.createAccountFirstNameInput, ExcelReader.getData("Accountpage", "firstname"), "first name entered")) {
			pass("First name entered");
		}
		else
			fail("First name not entered");
	}
	
	public void enterlastname()
	{
		if(driver.clickElement(AccountAdminObjects.createAccountLastNameContainer, "Last name conntainer clicked")) {
			pass("Last name container clicked");
		}
		else
			fail("Last name container not clicked");
		
		if(driver.enterText(AccountAdminObjects.createAccountLastNameInput, ExcelReader.getData("Accountpage", "lastname"), "Last name entered")) {
			pass("Last name entered");
		}
		else 
			fail("Last name not entered");
	}
	
	public void enteraccountsize()
	{
		if(driver.clickElement(AccountAdminObjects.createAccountSizeContainer, "Account size container clicked")) {
			pass("Account size container clicked");
		}
		else
			fail("Account size not clicked");
		
		if(driver.enterText(AccountAdminObjects.createAccountSizeInput, ExcelReader.getData("Accountpage", "accountsize"), "Account size entered")) {
			pass("Acccount size entered");
		}
		else
			fail("Account  size not entered");
	}
	
	public void enteraccountaddress()
	{
		if(driver.clickElement(AccountAdminObjects.createAccountAddressContainer, "Address container clicked")) {
			pass("Account address container clicked");
		}
		else
			fail("Account address container not clicked");
		
		if(driver.enterText(AccountAdminObjects.createAccountAddressInput,ExcelReader.getData("Accountpage", "address"), "Address entered")) {
			pass("Account address entered");
		}
		else
			fail("Account address not entered");
	}
	
	public void entercity()
	{
		if(driver.clickElement(AccountAdminObjects.createAccountCityContainer, "city container clicked")) {
			pass("Account city container clicked");
		}
		else
			fail("Account city container not clicked");
		
		if(driver.enterText(AccountAdminObjects.createAccountCityInput, ExcelReader.getData("Accountpage", "city"), "city field entered")) {
			pass("Account city entered");
		}
		else
			fail("Accountt city not entered");
	}
	
	public void selectaccounttype()
	{
		if(driver.jsClickElement(AccountAdminObjects.accounttypecontainer, "Account type container clicked"))
		{
			if(driver.explicitWaitforVisibility(AccountAdminObjects.accounttypedropdownlist, "account type visibility"))
			{
			int accounttype = driver.getSize(AccountAdminObjects.accounttypedropdownlist, "accounttype list Count");
	        int Random_AccountType = driver.generateRandomNumberWithLimit(accounttype-3, 2);
	        By randomSelectedaccounttype = By.xpath(AccountAdminObjects.accounttypedropdownlist.toString().replace("By.xpath: ", "").concat("[" + (Random_AccountType) + "]"));
	        driver.jsClickElement(randomSelectedaccounttype, "Account type selection"); 
	        pass("account type selected");
			}
			else
				fail("Account type not selected");
		}
	}
	
	public void selectcountry()
	{
		if(driver.clickElement(AccountAdminObjects.accountcountrycontainer, "Account country container clicked"))
		{
			int accountcountrysize = driver.getSize(AccountAdminObjects.countrydropdownlist, "dropdown list Country");
	        int Random_country = driver.generateRandomNumberWithLimit(accountcountrysize-4, 2);
	        By randomSelectedcountry = By.xpath(AccountAdminObjects.countrydropdownlist.toString().replace("By.xpath: ", "").concat("[" + (Random_country) + "]"));
	        if(driver.jsClickElement(randomSelectedcountry, "Country selection")) {
	        	        	pass("Country is selected");
	        	      }
	        		        else
	        		        	fail("Country is not selected");
	        	 		} 
		}
	
	
	public void enterstate()
	{
		if(driver.clickElement(AccountAdminObjects.createAccountStateContainer, "state container clicked")) {
			pass("State container clicked");
		}
		else
			fail("state container not clicked");
		
		if(driver.enterText(AccountAdminObjects.createAccountStateInput, ExcelReader.getData("Accountpage", "state"), "state entered succcessfully")) {
			pass("state entered successfully");
		}
		else
			fail("state not entered ");
	}
	
	public void enterzipcode()
	{
		if(driver.clickElement(AccountAdminObjects.createAccountzipcodeContainer, "Zipcode container clicked")) {
			pass("Zipcode container clicked");
		}
		else
			fail("Zipcode container not clicked");
		
		if(driver.enterText(AccountAdminObjects.createAccountzipcodeInput, ExcelReader.getData("Accountpage", "zipcode"), "Zip code entered successfully")) {
			pass("Zipcode entered successfully");
		}
		else
			fail("Zipcode not entered");
	}
	
	public void enterdunsnumber()
	{
		if(driver.clickElement(AccountAdminObjects.createAccountDUNSNumContainer, "DUNSnumber container clicked")) {
			pass("DUNSNumber container clicked");
		}
		else 
			fail("DUNSNumber container not clicked");
		
		if(driver.enterText(AccountAdminObjects.createAccountDUNSNumInput, ExcelReader.getData("Accountpage", "DUNSnumber"), "DUNS number entered successfully")) {
			pass("DUNSNumber entered successfully");
		}
		else
			fail("DUNSNumber not entered");
	}
	
	public void entertaxid()
	{
		
		if(driver.clickElement(AccountAdminObjects.createAccountTaxIDContainer, "Tax id container clicked")) {
			pass("Tax id container clicked");
		}
		else
			fail("Tax id container not clicked");
		
		if(driver.enterText(AccountAdminObjects.createAccountTaxIDInput, ExcelReader.getData("Accountpage", "taxid"), "taxid entered successfully")) {
			pass("taxid entered successfully");
		}
		else
			fail("taxid not entered");
	}
	
	public void submitbtntrigger()
	{
		if(driver.explicitWaitforVisibility(AccountAdminObjects.submitBtnTrigger, "submit button visible"))
		{
			driver.jsClickElement(AccountAdminObjects.submitBtnTrigger, "button clicked");
			pass("Submit button clicked");
		}
		else
		    fail("Submit button not clicked");
	}
	
	public void viewoverviewpage()
	{
		if(driver.explicitWaitforVisibility(AccountAdminObjects.accountdetailpagename, "account over view page name"))
		{
			pass("Account name displayed");
		}
		else
			fail("Account name not displayed in overview page");
	}
	
	
	
	
	public void accountrepselect()
	{
		if(driver.isElementDisplayed(AccountAdminObjects.overviewmanagementpane, "Mangement title displayed"))
		{
			if(driver.explicitWaitforVisibility(AccountAdminObjects.overviewaccountrepcontainer, "Account rep dropdown visible"))
			{
				//driver.scrollToElement(AccountAdminObjects.overviewaccountrepcontainer, "account rep clicked");
				if(driver.jsClickElement(AccountAdminObjects.overviewaccountrepcontainer, "account rep dropdown clicked"))
					{
						int accountrep = driver.getSize(AccountAdminObjects.accountrepselect, "accounttype list Count");
				        int Random_Accountrep = driver.generateRandomNumberWithLimit(accountrep, 1);
				        By randomSelectedaccountrep = By.xpath(AccountAdminObjects.accountrepselect.toString().replace("By.xpath: ", "").concat("[" + (Random_Accountrep) + "]"));
				        driver.jsClickElement(randomSelectedaccountrep, "Account rep selection"); 
				        pass("Account rep selected");
					}
				else
					fail("Account rep not selected");
				
			}
			
		}
	}
	
	
	public void statuschange()
	{
		
		
			if(driver.explicitWaitforVisibility(AccountAdminObjects.overviewstatuscontainer, "status dropdown visible"))
			{
				driver.scrollToElement(AccountAdminObjects.overviewstatuscontainer, "status dropdown clicked");
				if(driver.jsClickElement(AccountAdminObjects.overviewstatuscontainer, "status dropdown clicked"))
					{
						driver.jsClickElement(AccountAdminObjects.overviewstatusActive, "Status updated");
						
					}

				pass("Active status selected");
//				driver.jsClickElement(AccountAdminObjects.saveBtnTrigger, "Save button");
			}
			else
				fail("Active status not selected");
		
	}
	
	public void selectexistingaccount()
	{		
		
	        	
	        	//Mayavel
	        	
	        	if(driver.explicitWaitforVisibility(AccountAdminObjects.accountlistitems, "account select"))
	        	{
		      
		        driver.mouseHoverElement(AccountAdminObjects.accountlistitems, "Scrolled to element");
			        if(driver.actionClickElement(AccountAdminObjects.accountlist_size, "Random account selected"))
			        {
			        pass("Existing Account selected");
					}
					else
						fail("Existing account not selected");
	        	}
	        	else
	        		fail("Random selected item is not available");
	       
	
	}
        	
		
	

public void contracttabtrigger()
	
	{
		driver.scrollToElement(AccountAdminObjects.accounttitle, "Contract tab");
		if(driver.explicitWaitforVisibility(AccountAdminObjects.contractbtntrigger, "Contract tab trigger"))
		{
			if(driver.clickElement(AccountAdminObjects.contractbtntrigger, "contracttab trigger"))
			{
				pass("Contract tab clicked");
			}
			else
				fail("Contract tab not clicked");
		}
		
	}
	
	public void selectcatalog() throws InterruptedException
	{
		if(driver.isElementDisplayed(AccountAdminObjects.contractinfotitle, "Contract information title displayed"))
		{
//			Thread.sleep(3000);
			
			if(driver.explicitWaitforVisibility(AccountAdminObjects.catalogDropdown, "Catalog dropdown displayed"))
			{
				if(driver.actionClickElement(AccountAdminObjects.catalogDropdown, "catalog dropdown clicked"))
				{
					int cataloglist = driver.getSize(AccountAdminObjects.catalogDropdownlist, "catalog list Count");
			        int Random_catalog = driver.generateRandomNumberWithLimit(cataloglist,1);
			        driver.scrollToElement(AccountAdminObjects.contractbtntrigger, "Catalog selection");
			       if(driver.jsClickElement(AccountAdminObjects.catalogDropdownlist, "Catalog selection")) 
			       {
			    	   pass("catalog slected");
			       }
			       else
						fail("catalog not selected");
				}
				
			}
			
		}
		else
			fail("Contract information title not displayed");
	}
	
	public void selectpricelist() throws InterruptedException
	{
//		Thread.sleep(3000);
		if(driver.explicitWaitforVisibility(AccountAdminObjects.pricelistDropdown, "pricelist dropdown displayed"))
		{
			if(driver.actionClickElement(AccountAdminObjects.pricelistDropdown, "pricelist dropdown clicked"))
			{
				int pricelist = driver.getSize(AccountAdminObjects.pricelistDropdownlist, "pricelist list Count");
		        int Random_pricelist = driver.generateRandomNumber(pricelist);
		        if(Random_pricelist<2)
		        	Random_pricelist++;
		        By randomSelectedpricelist = By.xpath(AccountAdminObjects.pricelistDropdownlist.toString().replace("By.xpath: ", "").concat("[" + (Random_pricelist) + "]"));
		        Thread.sleep(500);
		        if(driver.actionClickElement(AccountAdminObjects.pricelist_selection, "Pricelist selection"))
		        {
		        	pass("pricelist slected");
		        }
		        else
					fail("pricelist not selected");
			}
			
		}
		
	}
	
	public void paymentmethodselect() throws InterruptedException
	{

		if(driver.explicitWaitforVisibility(AccountAdminObjects.paymentMethoddropdown, "paymentmethod dropdown displayed"))
		{
			if(driver.actionClickElement(AccountAdminObjects.paymentMethoddropdown, "paymentmethod dropdown clicked"))
			{
				int paymentmethod = driver.getSize(AccountAdminObjects.paymentMethoddropdownlist, "paymentmethod list Count");
		        int Random_paymentmethod = driver.generateRandomNumberWithLimit(paymentmethod,1);
		        
		        driver.explicitWaitforVisibility(AccountAdminObjects.paymentMethoddropdownlist, "Payment method element visible");
		        if(driver.jsClickElement(AccountAdminObjects.payment_method, "Paymentmethod selection"))
		        {
		        	pass("paymentmethod slected");
		        }
		        else
					fail("paymentmethod not selected");
		        
			}
			
		}
		
	}
	
	public void paymentermselect() throws InterruptedException
	{
		Thread.sleep(3000);
		if(driver.explicitWaitforVisibility(AccountAdminObjects.paymentTermsdropdown, "paymentTerms dropdown displayed"))
		{
			if(driver.actionClickElement(AccountAdminObjects.paymentTermsdropdown, "paymentTerms dropdown clicked"))
			{
				int paymentTerms = driver.getSize(AccountAdminObjects.paymentTermsdropdownlist, "paymentTerms list Count");
		        int Random_paymentTerms = driver.generateRandomNumberWithLimit(paymentTerms, 1);
		        By randomSelectedpaymentTerms = By.xpath(AccountAdminObjects.paymentTermsdropdownlist.toString().replace("By.xpath: ", "").concat("[" + (Random_paymentTerms) + "]"));
		        driver.explicitWaitforVisibility(AccountAdminObjects.paymentTermsdropdownlist, "Payment term element visible");
		        if(driver.actionClickElement(AccountAdminObjects.paymentTermsdropdownlist, "Payment Term selection"))
		        {
		        	pass("paymentTerm slected");
		        }
		        else
					fail("paymentTerm not selected");
		        
			}
			
			
		}
		
	}
	
	public void creditlimitinput()
	{
		if(driver.explicitWaitforVisibility(AccountAdminObjects.creditLimit, "creditLimit displayed"))
		{
			if(driver.enterText(AccountAdminObjects.creditLimitVal, "1000", "Credit limit entered"))
			{
				pass("credit limit entered");
			}
			else
				fail("credit limit not entered");
			
		}
		
	}
	
	public void contractupload()
	{
        try
        {
            Thread.sleep(3000);
            driver.scrollToElement(AccountAdminObjects.uploadcontracttitle, "");
            WebElement browse =(WebElement)driver.element(AccountAdminObjects.uploadcontractinput);
            String filename = BaseClass.UserDir+ BaseClass.properties.getProperty("DataSheetPath");
            browse.sendKeys(filename);
        }
        catch (Exception e)
        {
            fail("There is some issue in uploading Import Data Sheet. The Error is : " + e.getMessage());
        }
   }
	
	public void datepickerinput() throws InterruptedException
	{
		
			driver.jsClickElement(AccountAdminObjects.contractcalander, "calander selected");
//			int Size=driver.getSize(By.xpath("//*[@class='picker__box']//*[@role='presentation']"), "");
			driver.explicitWaitforVisibility(AccountAdminObjects.datepicker, "Date picker visible");
			if(driver.clickElement(AccountAdminObjects.datepicker, "data selected"))
				
			{
				pass("date Selected");
			}
			else
				fail("date not selected");
			driver.scrollToElement(AccountAdminObjects.datepicker_apply, "date apply button");
			driver.clickElement(AccountAdminObjects.datepicker_apply, "date clicked");
			
		
	}
	
	public void save()
	{
				if(driver.jsClickElement(AccountAdminObjects.saveBtnTrigger, "contract Saved"))
					pass("Save button clicked");
				else
					fail("Save button not clickable");
		
		
	}
	
//	ApiReaderUtils api=new ApiReaderUtils();
//	String sessionId="";
//    String authToken="";
//    String collectionid="";
//    
//    public void generatesessionId() throws JSONException
//    {
//        String resp = new String();
//        List<String> res = new ArrayList<String>();
//        res=    api.ReadAllDataFromUrl("https://eintadmin.skavacommerce.com/adminservices/auth/login","{\"identity\": \""+properties.getProperty("Username")+"\", \"password\": \""+properties.getProperty("Password")+"\"}","");
//        resp = res.get(0);
//        System.out.println(resp);
//        
//        JSONObject root = new JSONObject(resp);
//        sessionId = "SESSIONID="+root.getString("sessionId");
//        authToken = root.getString("authToken");
//        
//    }
//    
//    
//    public JSONObject createCollection(String serviceName) throws JSONException
//    {
//        String resp = new String();
//        List<String> res = new ArrayList<String>();
//        res=api.ReadAllDataFromUrl("https://eintadmin.skavacommerce.com/adminservices/collections/?businessId="+properties.getProperty("businessId")+"&serviceName=account","{ \"name\": \"Automation_account"+driver.generateRandomNumber(100000)+"\", \"properties\": [], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId);
//        resp = res.get(0);
//        System.out.println(resp);
//        
//        JSONObject root = new JSONObject(resp);
//        collectionid = root.getJSONObject("collection").getString("id");
//        properties.put("collectionid", collectionid);
//        System.out.println("collectionid"+collectionid);
//        return root;
//        
//    }
//    public JSONObject storeassocate(String serviceName) throws JSONException, ClientProtocolException, IOException
//    {
//        String resp = new String();
//        List<String> res = new ArrayList<String>();
//        res=api.put("https://eintadmin.skavacommerce.com/adminservices/stores/"+properties.getProperty("storeId"),"{\"businessId\":"+properties.getProperty("businessId")+",\"name\":\"Account Admin\",\"status\":0,\"type\":1,\"timeZone\":\"UTC\",\"logoUrl\":\"https://www.skava.com/logo.png\",\"defaultLocale\":\"en_US\",\"defaultShippingRegion\":\"US\",\"defaultCurrency\":\"USD\",\"properties\":[{\"name\":\"prop.name\",\"value\":\"prop.val\"}],\"associations\":[{\"name\":\"customer\",\"collectionId\":"+properties.getProperty("collectionid")+"},{\"name\":\"catalog\",\"collectionId\":"+properties.getProperty("collectionid")+"},{\\\"name\\\":\\\"Account\\\",\\\"collectionId\\\":\"+properties.getProperty(\"collectionid\")+\"},{\"name\":\"pricing\",\"collectionId\":"+properties.getProperty("collectionid")+"}],\"locales\":\"en_US\",\"currencies\":\"USD\",\"shippings\":\"US\",\"storeServiceProperties\":[{\"serviceName\":\"catalog\",\"name\":\"enableInventorycheck\",\"value\":\"true\"}]}",sessionId);
//        resp = res.get(0);
//        System.out.println(resp);
//        
//        JSONObject root = new JSONObject(resp);
//        collectionid = root.getJSONObject("collection").getString("id");
//        properties.put("collectionid", collectionid);
//        System.out.println("collectionid"+collectionid);
//        return root;
//        
//    }
	
	
}
