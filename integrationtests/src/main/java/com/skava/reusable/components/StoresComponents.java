package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.framework.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.AccountAdminObjects;
import com.skava.object.repository.BusinessPageObject;
import com.skava.object.repository.CatalogPageObject;
import com.skava.object.repository.LoginPage;
import com.skava.object.repository.LoginPageObject;
import com.skava.object.repository.MicroServiceObject;
import com.skava.object.repository.StoresObject;


public class StoresComponents extends BusinessComponents 
{	
	public void VerifyStoreURLDisplay() 
	{	
		driver.explicitWaitforVisibility(StoresObject.StoreMenu, "Store Menu");
		if(driver.jsClickElement(StoresObject.StoreMenu, "Store Menu"))
		{
			logPass("Store Menu is clicked"); 
			if(driver.getCurrentUrl().contains("stores"))				
			{
				logPass("Store URL is navigated"); 
			}
			else
			{
				logFail( "Store URL is not navigated"); 
			}
		}
		else
		{
			logFail( "Store Menu is not clicked"); 
		}
		
	}
	
	public void UpdateStoreName() throws InterruptedException 
	{
				// Enter store name randomly
				try
				{
					String texttoEnter = generateRandomStrings(10);
					if(driver.enterText(StoresObject.StoreName, texttoEnter , "Stores Name Text"))
					{
						logPass("Stores Name is entered");
						logInfo("The entered text was : " + texttoEnter);
						
					}
					ClickCreateButton();
					VerifySuccessPopAfterCreatingBusiness();
				}
					catch (Exception e)
					{
						logFail( "There is some error in Entering Stores Name , The error was : ");
						
					}
	}
	
	public void StoresObjectDetailsExceptStoreName() throws InterruptedException 
	{
				// Select Time Zone
				if(driver.clickElement(StoresObject.Edit_Timezone, "Edit Timezone"))
				{
					logPass("Timezone dropdown is selected");
					// Select US - Central
					if(driver.clickElement(StoresObject.Timezone_Options_Canada, "Timezone - US Canada"))
					{
						logPass("Timezone US Canada is selected");				
					}
					else
					{
						logFail( "Timezone US Canada is not selected");
					}
				}
				else
				{
					logFail( "Timezone dropdown is not selected");
				}
				
				// Select Locales
				//Thread.sleep(2000);
				if(driver.jsClickElement(StoresObject.Locales, "Locales"))
				{
					logPass("Locales dropdown is selected");
					// Select US - Canada
					if(driver.clickElement(StoresObject.Locales_Options_Canada, "Locales - US Canada"))
					{
						logPass("United States - English (en_CA) is selected");				
					}
					else
					{
						logFail( "United States - English (en_CA) is not selected");
					}
				}
				else
				{
					logFail( "Locales dropdown is not selected");
				}
				
				// Currencies
				//Thread.sleep(5000);
				driver.explicitWaitforVisibility(StoresObject.Currencies, "Currencies");
				if(driver.jsClickElement(StoresObject.Currencies, "Currencies"))
				{
					logPass("Currencies dropdown is selected");
					// Select US - Central
					if(driver.clickElement(StoresObject.Currencies_Options_Canada, "Currencies - CAD"))
					{
						logPass("Currencies - CAD is selected");				
					}
					else
					{
						logFail( "Currencies - CAD is not selected");
					}
				}
				else
				{
					logFail( "Currencies dropdown is not selected");
				}	
				
				// Click save & success message verification
				ClickCreateButton();
				VerifySuccessPopAfterCreatingBusiness();
	}
	public void EnterCreateStoreInformation() throws InterruptedException 
	{
		// Click on Add Store Button
		driver.jsClickElement(StoresObject.AddStore_Btn, "Add Store Button");
		
		// ||||||||||||||||||||||||||||| Enter the Store Name |||||||||||||||||||||||||||||
		String storename = ExcelReader.getData("BusinessPage", "RandomStoreName");
        int randomnumber=driver.generateRandomNumber(1000);
        String finalStoreName = storename+" "+randomnumber;
		
        if(driver.enterText(StoresObject.StoreName,finalStoreName,"Store Name"))
		{
			logPass("Store Name is entered");
			logInfo("Store Name is : +finalStoreName");
		}
		else
		{
			logFail( "Store Name is not entered");
		}
			        
		
//		// Select Time Zone
//		if(driver.clickElement(StoresObject.Timezone, "Timezone"))
//		{
//			logPass("Timezone dropdown is selected");
//			// Select US - Central
//			if(driver.clickElement(StoresObject.Timezone_Options, "Timezone - US Central"))
//			{
//				logPass("Timezone US Central is selected");				
//			}
//			else
//			{
//				logFail( "Timezone US Central is not selected");
//			}
//		}
//		else
//		{
//			logFail( "Timezone dropdown is not selected");
//		}
		
		// Select Locales
	//	Thread.sleep(2000);
		//driver.explicitWaitforVisibility(StoresObject.Locales, "Locales");
//		if(driver.jsClickElement(StoresObject.Locales, "Locales"))
//		{
//			logPass("Locales dropdown is selected");
//			// Select US - Central
//			if(driver.clickElement(StoresObject.Locales_Options, "Locales - US Central"))
//			{
//				logPass("United States - English (en_US) is selected");				
//			}
//			else
//			{
//				logFail( "United States - English (en_US) is not selected");
//			}
//		}
//		else
//		{
//			logFail( "Locales dropdown is not selected");
//		}
		
//		// Ship to regions
//		if(driver.jsClickElement(StoresObject.ShipToRegion, "Ship To Region"))
//		{
//			logPass("Ship To Region dropdown is selected");
//			// Select US - Central
//			if(driver.clickElement(StoresObject.ShipToRegion_Options, "Ship To Region - United States"))
//			{
//				logPass("Ship To Region - United States is selected");				
//			}
//			else
//			{
//				logFail( "Ship To Region - United States is not selected");
//			}
//		}
//		else
//		{
//			logFail( "Ship To Region dropdown is not selected");
//		}
		
		// Currencies
//		Thread.sleep(5000);
		//driver.explicitWaitforVisibility(StoresObject.Currencies, "Currencies");
//		if(driver.jsClickElement(StoresObject.Currencies, "Currencies"))
//		{
//			logPass("Currencies dropdown is selected");
//			// Select US - Central
//			if(driver.jsClickElement(StoresObject.Currencies_Options, "Currencies - USD"))
//			{
//				logPass("Currencies - USD is selected");				
//			}
//			else
//			{
//				logFail( "Currencies - USD is not selected");
//			}
//		}
//		else
//		{
//			logFail( "Currencies dropdown is not selected");
//		}
		if(driver.isElementPresent(StoresObject.dropdownActive, "Dropdown Active Check"))
        driver.moveToElementAndClick(StoresObject.dropdownActiveClose, "Dropdown Active Closed");
		SelectToggleSwitch();
		ClickCreateButton();
		VerifySuccessPopAfterCreatingBusiness();
		
		
	}
	
	public void NavigateToStoresMenu() 
	{
		driver.explicitWaitforVisibility(StoresObject.StoreMenu, "Store Menu");
		if(driver.jsClickElement(StoresObject.StoreMenu, "Store Menu"))
		{
			logPass("Store Menu is selected");
		}
		else
		{
			logFail( "Store Menu is not selected");
		}
	}
	
	public void ClickOnStoreOverviewTab() 
	{
		
		if(driver.jsClickElement(StoresObject.Store_OverviewTab, "Store Overview"))
		{
			logPass("Store Overview is selected");
		}
		else
		{
			logFail( "Store Overview is not selected");
		}
	}
	
	
	
	public void NavigateToMicroServicesMenu() 
	{
		driver.explicitWaitforVisibility(StoresObject.Microservices_Menu, "Microservices Menu");
		if(driver.jsClickElement(StoresObject.Microservices_Menu, "Microservices Menu"))
		{
			logPass("Microservices Menu is selected");
		}
		else
		{
			logFail( "Microservices Menu is not selected");
		}
	}
	
	public void ClickToStoreName() 
	{
		if(driver.clickElement(StoresObject.StoreName_Click, "Store Name"))
		{
			logPass("Store Name is selected");
		}
		else
		{
			logFail( "Store Name is not selected");
		}
	}
	public void ClickToServiceAssociationTab() throws InterruptedException 
	{
		Thread.sleep(4000);
		//driver.explicitWaitforVisibility(StoresObject.ServiceAssociation_Tab, "Service Association Tab");
		if(driver.jsClickElement(StoresObject.ServiceAssociation_Tab, "Service Association Tab"))
		{
			logPass("Service Association Tab is selected");
		}
		else
		{
			logFail( "Service Association Tab is not selected");
		}
	}
	
	public void SelectCollectionDropdown() throws InterruptedException 
	{
		Thread.sleep(6000);
		//driver.explicitWaitforVisibility(StoresObject.SelectCollection, "Select Collection");
		if(driver.isElementDisplayed(StoresObject.SelectCollection, "Select Collection"))
		{
			logPass("Select Collection element is displayed");
			if(driver.jsClickElement(StoresObject.SelectCollection, "Select Collection"))
			{
				logPass("Select Collection dropdown is selected");
				// Select US - Central
				if(driver.clickElement(StoresObject.SelectCollection_Options, "Select Collection"))
				{
					logPass("Select Collection is selected");	
					// Save Button
					driver.clickElement(StoresObject.Associate_Savebtn, "Save Button");
					Thread.sleep(5000);
					VerifySuccessPopAfterCreatingBusiness();
				}
				else
				{
					logFail( "Select Collection is not selected");
				}
			}
			else
			{
				logFail( "Select Collection dropdown is not selected");
			}
		}
		else
		{
			logFail( "Select Collection element is not displayed");
		}
		
	}
	
	public void ClickToLaunchButton() throws InterruptedException 
	{
		Thread.sleep(5000);
		//driver.explicitWaitforVisibility(StoresObject.Launch_Btn, "Launch Button");
		if(driver.mouseHoverElement(StoresObject.collectionName, "CollectionName"))
		{	
			if(driver.jsClickElement(StoresObject.Launch_Btn, "Launch Button"))
			{
				logPass("Launch Button is clicked");
			}
			else
			{
				logInfo( "Launch Button is not available to click for current selection");
			}
		}
	}
	
	public void CheckLaunchisAvaialableForCurrentSelection() throws InterruptedException 
	{
		Thread.sleep(4000);
		//driver.explicitWaitforVisibility(StoresObject.Launch_Btn, "Launch Button");
		/*if(driver.isElementDisplayed(StoresObject.Launch_Btn, 5, "Launch Button"))
		{
			logPass("Launch Button is available for current selection");*/
			if(driver.jsClickElement(StoresObject.Launch_Btn, "Launch Button"))
			{
				logPass("Launch Button is clicked");
				LaunchURLVerification();
			}
			else
			{
				logInfo("Launch Button is not available to click for selected micro service");
			}
		}
		/*else
		{
			logInfo("Launch Button is not available for current selection");
		}
	}*/
	
	public void LaunchURLVerification() 
	{
		driver.changeFocus(1);
		if(driver.getCurrentUrl().contains("admin"))
		{
			logPass("Launch URL is navigated successfully");
			driver.changeFocusToDefaultTab();
		}
		
		else
		{
			logFail( "Launch URL is not navigated successfully");
		}
	}
	
	/*public void ClickOnStoresObjectButton() throws InterruptedException 
	{
		Thread.sleep(2000);
		if(driver.jsClickElement(StoresObject.StoresObject_Btn, "Edit Store Button"))
		{
			logPass("Edit Store Button is Buttonclicked");
		}
		else
		{
			logFail( "Edit Store Button is not Buttonclicked");
		}
	}*/
	
	/*
	 *  Click on stores link in breadcrumb to create another store
	 */
	public void ClickOnStoresLinkBreadcrumb() throws InterruptedException 
	{
		Thread.sleep(2000);
		//driver.explicitWaitforVisibility(StoresObject.Stores_Breadcrumb, "Stores Link in Breadcrumb");
		if(driver.jsClickElement(StoresObject.Stores_Breadcrumb, "Stores Link in Breadcrumb"))
		{
			logPass("Stores Link in Breadcrumb is clicked");
		}
		else
		{
			logFail( "Stores Link in Breadcrumb is not clicked");
		}
	}
	
	/*
	 *  Create store for  Canada 
	 */
	public void EnterCreateStoreInformationForCanada() throws InterruptedException 
	{
		// Click on Add Store Button
		Thread.sleep(4000);
		//driver.explicitWaitforVisibility(StoresObject.AddStore_Btn, "Add Store Button");
		driver.jsClickElement(StoresObject.AddStore_Btn, "Add Store Button");
		// Enter store name randomly
		// ||||||||||||||||||||||||||||| Enter the Store Name |||||||||||||||||||||||||||||
		String storename = ExcelReader.getData("BusinessPage", "RandomStoreName");
        int randomnumber=driver.generateRandomNumber(1000);
        String finalStoreName = storename+" "+randomnumber;
		
        if(driver.enterText(StoresObject.StoreName,finalStoreName,"Store Name"))
		{
			logPass("Store Name is entered");
			logInfo("Store Name is : +finalStoreName");
		}
		else
		{
			logFail( "Store Name is not entered");
		}
		
		// Select Time Zone
		if(driver.clickElement(StoresObject.Timezone, "Timezone"))
		{
			logPass("Timezone dropdown is selected");
			// Select US - Central
			if(driver.clickElement(StoresObject.Timezone_Options_Canada, "Timezone - US Canada"))
			{
				logPass("Timezone US Canada is selected");				
			}
			else
			{
				logFail( "Timezone US Canada is not selected");
			}
		}
		else
		{
			logFail( "Timezone dropdown is not selected");
		}
		
		// Select Locales
		Thread.sleep(2000);
		if(driver.jsClickElement(StoresObject.Locales, "Locales"))
		{
			logPass("Locales dropdown is selected");
			// Select US - Canada
			if(driver.clickElement(StoresObject.Locales_Options_Canada, "Locales - US Canada"))
			{
				logPass("United States - English (en_CA) is selected");				
			}
			else
			{
				logFail( "United States - English (en_CA) is not selected");
			}
		}
		else
		{
			logFail( "Locales dropdown is not selected");
		}
		
		// Ship to regions
		if(driver.jsClickElement(StoresObject.ShipToRegion, "Ship To Region"))
		{
			logPass("Ship To Region dropdown is selected");
			// Select US - Canada
			Thread.sleep(5000);
			//driver.explicitWaitforVisibility(StoresObject.ShipToRegion_Options, "Ship To Region - Canada");
			if(driver.moveToElementAndClick(StoresObject.ShipToRegion_Options, "Ship To Region - Canada"))
			{
				logPass("Ship To Region - Canada is selected");				
			}
			else
			{
				logFail( "Ship To Region - Canada is not selected");
			}
		}
		else
		{
			logFail( "Ship To Region dropdown is not selected");
		}
		
		// Currencies
		Thread.sleep(5000);
		if(driver.jsClickElement(StoresObject.Currencies, "Currencies"))
		{
			logPass("Currencies dropdown is selected");
			// Select US - Central
			if(driver.moveToElementAndClick(StoresObject.Currencies_Options_Canada, "Currencies - CAD"))
			{
				logPass("Currencies - CAD is selected");				
			}
			else
			{
				logFail( "Currencies - CAD is not selected");
			}
		}
		else
		{
			logFail( "Currencies dropdown is not selected");
		}		
		SelectToggleSwitch();
		ClickCreateButton();
		VerifySuccessPopAfterCreatingBusiness();		
		
	}
	
	public void SelectCreatedCollectionFromDropdown() throws InterruptedException 
	{
		Thread.sleep(6000);
		//driver.explicitWaitforVisibility(StoresObject.SelectCollection, "Select Collection");
		if(driver.jsClickElement(StoresObject.SelectCollection, "Select Collection"))
		{
			logPass("Select Collection dropdown is selected");
			// Select US - Central
			if(driver.clickElement(StoresObject.SelectCollection_Options, "Select Collection"))
			{
				logPass("Select Collection is selected");	
				// Save Button
				driver.clickElement(StoresObject.Associate_Savebtn, "Save Button");
				Thread.sleep(5000);
				VerifySuccessPopAfterCreatingBusiness();
			}
			else
			{
				logFail( "Select Collection is not selected");
			}
		}
		else
		{
			logFail( "Select Collection dropdown is not selected");
		}
	}
	
	public void LaunchMultipleServices() throws InterruptedException 
	{
		if (driver.isElementDisplayed(MicroServiceObject.MicroService_Size, "MicroService Size"))
        {
    		int MicroService_size = driver.getSize(MicroServiceObject.MicroService_Size, "MicroService Size");
    		for(int i=1;i<=MicroService_size;i++)
    			{
    				By ServiceClick = By.xpath(MicroServiceObject.MicroService_Size.toString().replace("By.xpath: ", "").concat("["+(i) + "]"));
    				String msName = driver.getText(By.xpath(MicroServiceObject.MicroService_Size.toString().replace("By.xpath: ", "").concat("["+(i) + "]//span")), "MicroService");
    				if(msName.equals("Catalog"))
    				{
    					if(driver.clickElement(ServiceClick, "Created MicroService is clicked"))
	    				{
    						logPass("Micro Service is clicked");
    						// Launch URL
		    			    if(driver.mouseHoverElement(StoresObject.collectionName, "CollectionName"))
		    			    {
			    			    if(driver.moveToElementAndClick(StoresObject.Launch_Btn, "Launch Button"))
			    				{
			    					logPass("Launch Button is clicked");
			    					LaunchURLVerification();
			    					break;
			    				}
			    				else
			    				{
			    					logInfo("Launch Button is not available to click");
			    				}
		    			    }
			    			else
			    			{
			    				logInfo("Launch Button is not available to click");	
			    			}
	    				//CheckLaunchisAvaialableForCurrentSelection();
	    				driver.navigateBack();
	    				}
	    				else
	    				{
	    					 logFail( "Micro Service is not clicked");
	    				}
    				}
    			}
        }
    		else
    		{
    			logFail( "Muliple service is not launched");
    		}
	}
	
	public void MulitpleStoreToAssociationNavigation() throws InterruptedException 
	{
		if(driver.isElementDisplayed(StoresObject.Storename, "Store Name"))
		{
			logPass("Store Name is displayed");
			int Storename_size = driver.getSize(StoresObject.Storename, "Store Name");
			for(int i=1;i<=Storename_size;i++)
			{
				By StoreName = By.xpath(StoresObject.Storename.toString().replace("By.xpath: ", "").concat("["+(i) + "]"));	
				Thread.sleep(2000);
				driver.clickElement(StoreName, "StoreName");
				ClickToServiceAssociationTab();
				//AssociateMultipleCollection();
				associateCatalogServices();
	//			NavigateToStoresMenu();
			}
		}
		else
		{
			logFail( "Store Name is not displayed");
		}
	}
	
	public void AssociateMultipleCollection() throws InterruptedException 
	{
		Thread.sleep(2000);
		//driver.explicitWaitforVisibility(StoresObject.Multiple_Collection_Size, "Multiple Collection");
		if (driver.isElementDisplayed(StoresObject.Multiple_Collection_Size, "Multiple Collection"))
        {
    		int collection_size = driver.getSize(StoresObject.Multiple_Collection_Size, "Multiple Collection Size");
    		for(int i=2;i<=3;i++)
    			{
    				By collection_dropdown = By.xpath(StoresObject.SelectCollection.toString().replace("By.xpath: ", "").concat("["+(i) + "]"));
    				Thread.sleep(2000);
    				driver.moveToElementAndClick(collection_dropdown, "Collection Dropdown");
    				logPass("Collection Dropdown is clicked");
    				// Select the collection from dropdown randomly
    				if(driver.isElementDisplayed(MicroServiceObject.Collection_Container, "Collection Container"))
    				{
    				int collection_count = driver.getSize(By.xpath(StoresObject.Collection_count_dropdown.toString().replace("By.xpath: ", "").concat("["+(i) + "]")), "Collection Count");
    				//int collection_Random = driver.generateRandomNumber(collection_count)+1;
    				int collection_Random = driver.generateRandomNumber(collection_count);
    				
	    		        if(collection_Random==1)
	    		        {
	    		        	collection_Random = 2;
	
	    		        	driver.clickElement(By.xpath(StoresObject.Collection_count_dropdown.toString().replace("By.xpath: ", "").concat("["+collection_Random+"]")), "Collection is selected");
	    		        }
	    		        else
	    		        {
	    		        	
	    		        driver.clickElement(By.xpath(StoresObject.Collection_count_dropdown.toString().replace("By.xpath: ", "").concat("["+collection_Random+"]")), "Collection is selected");
	    		        }  
    				}
    				else
    				{
    					logInfo( "Collection Container is not available to display for current selection");
    				}
    			}
    		// Click on Save button in the collection dropdown
	        driver.jsClickElement(StoresObject.Associate_Savebtn, "Associate Save Button");
	        VerifySuccessPopAfterCreatingBusiness();
    		logPass("Collection Dropdown is clicked randomly");
        }
		else
		{
			logFail( "Collection Dropdown is not clicked randomly");
		}
		
	}
	
	/*
	 *  Update collection
	 */
	public void Updatecollectsave() throws InterruptedException 
	{
		// Click on Save button in the collection dropdown
		Thread.sleep(3000);
		//driver.explicitWaitforVisibility(StoresObject.updte_collec_sve, "Associate Save Button");
        if(driver.jsClickElement(StoresObject.updte_collec_sve, "Associate Save Button"))
        {
        	logPass("Save button is clicked");
        }
        else
        {
        	logFail("Save button is not clicked");
        }
        
	}
	
	
	/*
	 * 
	 */
	public void clicksavecollection() throws InterruptedException 
	{
		// Click on Save button in the collection dropdown
		Thread.sleep(3000);
		//driver.explicitWaitforVisibility(StoresObject.Associate_Savebtn, "Associate Save Button");
        if(driver.moveToElementAndClick(StoresObject.Associate_Savebtn, "Associate Save Button"))
        {
        	logPass("Save button is clicked");
        }
        else
        {
        	logFail("Save button is not clicked");
        }
        
	}
	public void ChooseMultipleStoreToSameCollectionInLoop() throws InterruptedException 
	{
		if (driver.isElementDisplayed(StoresObject.Storename, "Stores Size"))
        {
    		int Store_size = driver.getSize(StoresObject.Storename, "Stores Size");
    		for(int i=1;i<=Store_size;i++)
    			{
    				By StoreNameLink = By.xpath(StoresObject.Storename.toString().replace("By.xpath: ", "").concat("["+(i) + "]"));		
    				driver.clickElement(StoreNameLink, "Created Store Link is clicked");
    				logPass("Created Store Link is clicked");
    				// Common Methods
    				ClickToServiceAssociationTab();
    				SelectCreatedCollectionFromDropdown();
    				NavigateToStoresMenu();
    				
    			}
    		logPass("Created store is clicked randomly");
        }
		else
		{
			logFail( "Created store is not clicked randomly");
		}
		
		
	}
	
	public void ChooseMultipleStoreToDifferentSetOfCollectionInLoop() throws InterruptedException 
	{
		if (driver.isElementDisplayed(StoresObject.Storename, "Stores Size"))
        {
    		int Store_size = driver.getSize(StoresObject.Storename, "Stores Size");
    		for(int i=1;i<=Store_size;i++)
    			{
    				By StoreNameLink = By.xpath(StoresObject.Storename.toString().replace("By.xpath: ", "").concat("["+(i) + "]"));
    				driver.clickElement(StoreNameLink, "Created Store Link is clicked");
    				logPass("Created Store Link is clicked");
    				// Common Methods
    				ClickToServiceAssociationTab();
    				//AssociateMultipleCollection(); // New method for this test case
    				associateCatalogServices(); // New method for this test case
    	//			NavigateToStoresMenu();
    				
    			}
    		logPass("Created store is clicked randomly");
        }
		else
		{
			logFail( "Created store is not clicked randomly");
		}
	}
	
	/*
	 *  Navigate to store ops from dropdown
	 */
	public void SelectStoreOpsFromDropdown() throws InterruptedException 
	{
		Thread.sleep(2000);
		driver.explicitWaitforVisibility(StoresObject.Goto_Dropdown, "Go to dropdown");
		if(driver.jsClickElement(StoresObject.Goto_Dropdown, "Go to dropdown"))
		{
			logPass("Go to dropdown is selected");
			// Store Ops selection from dropdown
			if(driver.jsClickElement(StoresObject.Storeops_Dropdown, "Store-ops dropdown"))
			{
				logPass("Store-ops dropdown is selected");
				
			}
			else
			{
				logFail( "Store-ops dropdown is not selected");
			}
		}
		else
		{
			logFail( "Go to dropdown is not selected");
		}
	}
	
	/*
	 *  CLick on view store and navigate to store ops
	 */
	public void ViewStoreNavigationforStoreOps() throws InterruptedException 
	{
		Thread.sleep(2000);
		driver.explicitWaitforVisibility(StoresObject.ViewStore_Click, "View Store");
		if(driver.clickElement(StoresObject.ViewStore_Click, "View Store"))
		{
			logPass("View Store is clicked");
			
		}
		else
		{
			logFail( "View Store is not clicked");
		}
	}
	
	/*
	 * Compare the store name from the URL
	 */
	
	public void StoreNameComparision()
    {
         try {
        	 String currenturl = driver.getCurrentUrl();
             logInfo("Current Url is : " + currenturl);  
             String Storeidsplit[]=currenturl.split("=");
             String Storeid=Storeidsplit[2];
             logInfo("Created Store id is : " + Storeid);
             String Store = driver.getText(StoresObject.Store_id, "Store id");
                if(Storeid.equals(Store))
                {
                    logPass("Store id Compared");
                    logInfo("Store id : " + Storeid);
                    logInfo("Store  : " + Store);
                }
                else {
                    logFail("Store id not compared");
                }
		} catch (Exception e) {
			// TODO: handle exception
			logFail("Store id not compared");
		}
    }
	
	/*
	 *  Navigate to Edit store page
	 */
	public void PancakeClickFromStoreOverviewPage()
    {
		if(driver.clickElement(StoresObject.Store_Pancake, "Pancake from Store Overview"))
		{
			logPass("Pancake is clicked");
		}
		else
		{
			logFail("Pancake is not clicked");
		}
    }
	
	public void VerifyStoreOverview()
    {
        if(driver.getCurrentUrl().contains("stores"))               
        {
           logPass("Store Overview is Displayed "); 
            
        }
        else
        {
            logFail( "Store Overview is Not Displayed"); 
        }
    } 
	
	public void ClickonBusinessTitle()
	{
		if(driver.isElementDisplayed(StoresObject.businessTitle, "businessTitle"))
		{
			logPass("Business Title is Displayed");

			if(driver.jsClickElement(StoresObject.businessTitle, "businessTitle"))
			{
				logPass("Business Title is Clicked");
			}
		}
		else {
			logFail("Business Title is not Displayed");
		}
	}
	
	public void ClickAddStoreBtn()
	{
		if(driver.isElementDisplayed(StoresObject.AddStore_Btn,  "AddStore_Btn"))
		{
			logPass("Add Store button is Displayed");
			if(driver.jsClickElement(StoresObject.AddStore_Btn, "AddStore_Btn"))
			{
				logPass("Add Store button is clicked");
			}
		}else {
			logFail("Add Store button is not displayed");
		}
	} 
	
	public void SearchExistingStore() throws InterruptedException 
    {
		Thread.sleep(5000);
		//driver.explicitWaitforVisibility(StoresObject.Storeid_Dropdown, "Store ID Dropdown");
        if(driver.jsClickElement(StoresObject.Storeid_Dropdown, "Store ID Dropdown"))
        {
           logPass("Store ID Dropdown is clicked");
            // Enter the business ID
            //if(driver.enterText(StoresObject.Storeid_txtbox,ExcelReader.getData("BusinessPage","StoreID"),"Store ID"))
            if(driver.enterText(StoresObject.Storeid_txtbox, properties.getProperty("FoundationStoreId"), "Store ID"))
            {
            driver.moveToElementAndClick(BusinessPageObject.Search_btn, "Search Button");
           logPass("Search Result is displayed");
            }
            else
            {
                logFail( "User not Entered valid store ID");
            }
            
        }
        else
        {
            logFail( "Business ID Dropdown is not clicked");
        }
    } 
	
	 /*public void ClickOnStoresObjectbuttonFromAllStorePage() throws InterruptedException 
	 {
		 if(driver.moveToElementAndClick(StoresObject.StoresObject_Btn, "Edit Store Button"))
			 //if(driver.jsClickElement(StoresObject.StoresObject_btn, "Edit Store Button"))
		 {
			logPass("Edit Store Button is clicked");
		 }
		 else
		 {
			 logFail( "Edit Store Button is not clicked");
		 }
	 } 
	 */
	 
	public void ClickOnEditStoreButton() throws InterruptedException
    {
        Thread.sleep(2000);
        //driver.explicitWaitforVisibility(StoresObject.EditStore_Btn, "Edit Store Button");
        if(driver.jsClickElement(StoresObject.EditStore_Btn, "Edit Store Button"))
        {
            logPass("Edit Store Button is Buttonclicked");
        }
        else
        {
            logFail( "Edit Store Button is not Buttonclicked");
        }
    }
	
	public void EditStoreDetailsExceptStoreName() throws InterruptedException
    {       
                // Select Time Zone
                if(driver.clickElement(StoresObject.Edit_Timezone, "Edit Timezone"))
                {
                    logPass("Timezone dropdown is selected");
                    // Select US - Central
                    if(driver.clickElement(StoresObject.Timezone_Options_Canada, "Timezone - US Canada"))
                    {
                        logPass("Timezone US Canada is selected");                
                    }
                    else
                    {
                        logFail( "Timezone US Canada is not selected");
                    }
                }
                else
                {
                    logFail( "Timezone dropdown is not selected");
                }
                
                // Select Locales
                //Thread.sleep(2000);
                driver.explicitWaitforVisibility(StoresObject.Locales, "Locales");
                if(driver.jsClickElement(StoresObject.Locales, "Locales"))
                {
                    logPass("Locales dropdown is selected");
                    // Select US - Canada
                    if(driver.clickElement(StoresObject.Locales_Options_Canada, "Locales - US Canada"))
                    {
                        logPass("United States - English (en_CA) is selected");                
                    }
                    else
                    {
                        logFail( "United States - English (en_CA) is not selected");
                    }
                }
                else
                {
                    logFail( "Locales dropdown is not selected");
                }
                
                // Currencies
                Thread.sleep(5000);
                if(driver.jsClickElement(StoresObject.Currencies, "Currencies"))
                {
                    logPass("Currencies dropdown is selected");
                    // Select US - Central
                    if(driver.clickElement(StoresObject.Currencies_Options_Canada, "Currencies - CAD"))
                    {
                        logPass("Currencies - CAD is selected");                
                    }
                    else
                    {
                        logFail( "Currencies - CAD is not selected");
                    }
                }
                else
                {
                    logFail( "Currencies dropdown is not selected");
                }    
                
                // Click save & success message verification
                ClickCreateButton();
                VerifySuccessPopAfterCreatingBusiness();
    }
	
	 /*
	  *  @author - Priya On Nov-28-18
	  */
	 
	 public void SearchExistingBusiness() 
	    {
		 	try {
				Thread.sleep(4000);
				if(driver.moveToElementAndClick(StoresObject.Businessid_Dropdown, "Business ID Dropdown"))
		        {
		           logPass("Business ID Dropdown is clicked");
		            // Enter the business ID
		            //if(driver.enterText(StoresObject.Businessid_Textbox,ExcelReader.getData("BusinessPage","BusinessID"),"Business ID"))
	            	if(driver.enterText(StoresObject.Businessid_Textbox, properties.getProperty("BusinessId"), "Business ID"))
	            	{
		            driver.jsClickElement(BusinessPageObject.Search_btn, "Search Button");
		            logPass("Search Result is displayed");
		            }
		            else
		            {
		                logFail( "User not Entered valid Business ID");
		            }
		            
		        }
		        else
		        {
		            logFail( "Business ID Dropdown is not clicked");
		        }
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	    } 
	 
	 /*
	  * Navigate to business name
	  */
	 
	 public void NavigatetoSearchedBusiness() throws InterruptedException 
	 {
		 Thread.sleep(6000);
		 //driver.explicitWaitforVisibility(BusinessPageObject.Click_Business, "Business Overview Page");
		 if(driver.jsClickElement(BusinessPageObject.Click_Business, "Business Overview Page"))
		 {
			 logPass("Business over page is displayed");
		 }
		 else
		 {
			 logFail("Business over page is not displayed");
		 }
	 }
	 
	 
	 /*
	  *  Navigate to searched store
	  */
	 
	 public void NavigatetoSearchedStore() throws InterruptedException 
	 {
		 Thread.sleep(6000);
		 driver.explicitWaitforVisibility(StoresObject.ViewStore_Click, "Store Overview Page");
		 if(driver.jsClickElement(StoresObject.ViewStore_Click, "Store Overview Page"))
		 {
			 logPass("Store over page is displayed");
		 }
		 else
		 {
			 logFail("Store over page is not displayed");
		 }
	 }
	 public void ClickOnEditStorebuttonFromAllStorePage() throws InterruptedException
     {
		 Thread.sleep(4000);
		 if(driver.explicitWaitforVisibility(StoresObject.storeNameVal, "Store Name Valu"))
			{
				if(driver.mouseHoverElement(StoresObject.storeNameVal, "Store Name"))
				{	
					driver.clickElement(StoresObject.EditStore_Btn, "Edit Store Button");
					logPass("Edit Store Button is clicked");
				}
				else
				
					logFail( "Edit Store Button is not clicked");
			}
		}
		 
	 /*
	  *  @author - Anushiya
	  */
	 public void associateCatalogServices() throws InterruptedException
		{
			try {
				verifyServicesDisplayed("catalog");
				clickCollectionList("catalog");
				verifyCollectionListOptn("catalog");
				associateCollection("catalog");
				driver.jsClickElement(StoresObject.Associate_Savebtn, "Associate Save Button");
			    VerifySuccessPopAfterCreatingBusiness();
			    logPass("Catalog collection associated");
			} catch (Exception e) {
				// TODO: handle exception
				logInfo("Collections already associated");
			}
		 	
		}
	 public void verifyServicesDisplayed(String service) throws InterruptedException
		{
			if(driver.isElementDisplayed(StoresObject.serviceName, "ServiceName"))
			{
				int size = driver.getSize(StoresObject.serviceName, "serviceName");
				for(int i=1;i<=size;i++)
				{
					String val = driver.getText(By.xpath(StoresObject.serviceName.toString().replace("By.xpath: ", "").concat("["+i+"]")), "ServiceName");
					if(val.equalsIgnoreCase(service))
					{
						logPass(service+" is displayed");
						break;
					}
					else if(i==size)
						logFail(service+" is not displayed");
					else
						continue;
				}
			}
		}
	 
	 public void clickCollectionList(String name)
		{
			if(driver.explicitWaitforVisibility(StoresObject.serviceName, "serviceName"))
			{	
				int size = driver.getSize(StoresObject.serviceName, "serviceName");
				for(int i=1;i<=size;i++)
				{
					String val = driver.getText(By.xpath(StoresObject.serviceName.toString().replace("By.xpath: ", "").concat("["+i+"]")), "ServiceName");
					String checkValue = val.equals("Customers")?"customer":val;
					if(checkValue.equalsIgnoreCase(name))
					{
						name = name.substring(0, 1).toLowerCase() + name.substring(1,name.length());
						logPass(name+" is displayed");
						driver.jsClickElement(By.xpath("//*[@data-qa='service-name-val'][text()='"+val+"']/parent::tr//*[@data-qa='edit-collection-icon']/a"), "Edit Icon");
						if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='collection-id-filter"+name+"']//span"), "Collection  List"));
						{
							logPass(name+" CollectionList is displayed");
		//					driver.clickElement(By.xpath("//*[@data-qa='collection-id-filter"+name+"']//span"), name+" collection List");
		//						logPass(name+" collection list is clicked");
						}
						break;
					}
					else if(i==size)
						logFail(name+" is not  displayed");
				}
			}
		}
	 
	 public void verifyCollectionListOptn(String name)
		{
			if(driver.explicitWaitforVisibility(StoresObject.serviceName, "serviceName"))
			{
				int size = driver.getSize(StoresObject.serviceName, "serviceName");
				for(int i=1;i<=size;i++)
				{
					String val = driver.getText(By.xpath(StoresObject.serviceName.toString().replace("By.xpath: ", "").concat("["+i+"]")), "ServiceName");
					val = val.equals("Cart")?"cartcheckout":val;
					if(val.equalsIgnoreCase(name))
					{
						if(driver.explicitWaitforVisibility(By.xpath("(//*[@data-qa='collec-id-option"+name+"'])"), "Collection List Option"))
						logPass(name+" CollectionList option is displayed");
						break;
					}
					else if(i==size)
						logFail(name+" is not displayed");
				}
			}
		}
	 
	 public void associateCollection(String name)
		{
			if(driver.explicitWaitforVisibility(StoresObject.serviceName, "serviceName"))
			{	
				if(driver.explicitWaitforVisibility(By.xpath("(//*[@data-qa='collec-id-option"+name+"'])"), "Collection List Option"))
				{
					int len = driver.getSize(By.xpath("//*[@data-qa='collec-id-option"+name+"']"), "Options");
					int rand = len>1 ? driver.generateRandomNumber(len) : len;
				//	String a = editStore.radiobtn.toString().replace("By.xpath: ", "").concat("["+i+"]["+(rand+1)+"]//input");
					if(driver.jsClickElement(By.xpath("(//*[@data-qa='collec-id-option"+name+"'])["+rand+"]//input"), "Collection List Option"))
					{	
						logPass("CollectionList Options are selected");
						if(driver.jsClickElement(By.xpath("//*[@data-qa='collection-id-filter"+name+"']//ancestor::div[8]//*[@data-qa='save-trigger']"), ""))
							logPass("Collection are associated");
						else
							logFail("Collections are not associated");
					}
					else
						logFail("Collection are not associated");
				}
			}
		}
	 
		public void associateCustomersServices() throws InterruptedException
	    {
			try {
				verifyServicesDisplayed("cart");
				clickCollectionList("cart");
				verifyCollectionListOptn("cartcheckout");
				associateCollection("cartcheckout");
		        driver.jsClickElement(StoresObject.Associate_Savebtn, "Associate Save Button");
			    VerifySuccessPopAfterCreatingBusiness();
			    logPass("Customer collection is associated");
			} catch (Exception e) {
				// TODO: handle exception
				 logPass("Customer collection is already associated or disabled to associate..");
			}
	        
	    }
	 
		//Paramesh
        public void EnterCreateStoreInformationForCanada1() throws InterruptedException
        {
            // Click on Add Store Button
            Thread.sleep(4000);
            //driver.explicitWaitforVisibility(StoresObject.AddStore_Btn, "Add Store Button");
            driver.jsClickElement(StoresObject.AddStore_Btn, "Add Store Button");
            // Enter store name randomly
            // ||||||||||||||||||||||||||||| Enter the Store Name |||||||||||||||||||||||||||||
            String storename = ExcelReader.getData("BusinessPage", "RandomStoreName");
            int randomnumber=driver.generateRandomNumber(1000);
            String finalStoreName = storename+" "+randomnumber;
            
            if(driver.enterText(StoresObject.StoreName,finalStoreName,"Store Name"))
            {
                logPass("Store Name is entered");
                logInfo("Store Name is : +finalStoreName");
            }
            else
            {
                logFail( "Store Name is not entered");
            }
            
            // Select Time Zone
            if(driver.clickElement(StoresObject.Timezone, "Timezone"))
            {
                logPass("Timezone dropdown is selected");
                // Select US - Central
                if(driver.clickElement(StoresObject.Timezone_Options_Canada, "Timezone - US Canada"))
                {
                    logPass("Timezone US Canada is selected");                
                }
                else
                {
                    logFail( "Timezone US Canada is not selected");
                }
            }
            else
            {
                logFail( "Timezone dropdown is not selected");
            }
            
            // Select Locales
            Thread.sleep(2000);
            if(driver.jsClickElement(StoresObject.Locales, "Locales"))
            {
                logPass("Locales dropdown is selected");
                // Select US - Canada
                if(driver.clickElement(StoresObject.Locales_Options_Canada, "Locales - US Canada"))
                {
                    logPass("United States - English (en_CA) is selected");                
                }
                else
                {
                    logFail( "United States - English (en_CA) is not selected");
                }
            }
            else
            {
                logFail( "Locales dropdown is not selected");
            }
            
            // Ship to regions
//            if(driver.jsClickElement(StoresObject.ShipToRegion, "Ship To Region"))
//            {
//                logPass("Ship To Region dropdown is selected");
//                // Select US - Canada
//                Thread.sleep(5000);
//                if(driver.moveToElementAndClick(StoresObject.ShipToRegion_Options, "Ship To Region - Canada"))
//                {
//                    logPass("Ship To Region - Canada is selected");                
//                }
//                else
//                {
//                    logFail( "Ship To Region - Canada is not selected");
//                }
//            }
//            else
//            {
//                logFail( "Ship To Region dropdown is not selected");
//            }
            
            // Currencies
            //Thread.sleep(5000);
            driver.explicitWaitforVisibility(StoresObject.Currencies, "Currencies");
            driver.scrollToElement(StoresObject.Currencies, "Currencies");
            if(driver.jsClickElement(StoresObject.Currencies, "Currencies"))
            {
                logPass("Currencies dropdown is selected");
                // Select US - Central
                if(driver.moveToElementAndClick(StoresObject.Currencies_Options_Canada, "Currencies - CAD"))
                {
                    logPass("Currencies - CAD is selected");                
                }
                else
                {
                    logFail( "Currencies - CAD is not selected");
                }
            }
            else
            {
                logFail( "Currencies dropdown is not selected");
            }    
            if(driver.isElementPresent(StoresObject.dropdownActive, "Dropdown Active Check"))
            driver.moveToElementAndClick(StoresObject.dropdownActiveClose, "Dropdown Active Closed");
            SelectToggleSwitch();
            ClickCreateButton();
            VerifySuccessPopAfterCreatingBusiness();        
            
        }
	 
        /*
         *  From General components
         */
        /*
         *  Verify success message pop-up scenario
         */
        public void VerifySuccessPopAfterCreatingBusiness() 
    	{
        	//Thread.sleep(4000);
			//driver.explicitWaitforVisibility(BusinessPageObject.Business_Success_Popup, "Success Pop-up after creating the business");
			if(driver.isElementDisplayed(BusinessPageObject.Business_Success_Popup, "Success Pop-up after creating the business"))
			{
				logPass( "Success Pop-up is displayed after creating the business");
				String SuccessMessage = driver.getText(BusinessPageObject.Business_Success_Msg, "Success Message");
				ExtentTestManager.getTest().log(LogStatus.INFO, "Success Message is" +SuccessMessage);
				if(driver.clickElement(BusinessPageObject.Popup_Ok_Btn, "Pop Up OK Button"))
				{
					logPass( "Success Pop-up OK button is clicked");
				}
				else
				{
					logFail( "Success Pop-up OK button is not clicked");
				}
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.INFO, "Success Pop-up is not displayed after creating the business");
			}
    	}
        /*
         *    // Select status toggle switch
         */
        public void SelectToggleSwitch() 
       	{
        	try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		if(driver.moveToElementAndClick(MicroServiceObject.Status_Toggle, "Status Toggle is clicked"))
    		{
    			logPass( "Status Toggle is clicked");  
    		}
    		else
    		{
    			logFail( "Status Toggle is not clicked");  
    		}
       	}
        
        /*
         *    // Select status toggle switch
         */
        public void ClickCreateButton() 
       	{
        	// Click collection create button
        	try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		if(driver.jsClickElement(MicroServiceObject.Collection_Create_Btn, "Collection Create Button is clicked"))
    		{
    			logPass( "Collection Create Button is clicked");  
    		}
    		else
    		{
    			logFail( "Collection Create Button is not clicked");  
    		}
       	}
        
        /*
         *   Verify user can able to get business ID from the URL
         */
        public void GetBusinessIdFromURL() 
       	{
        	try 
        	{
        		String currenturl = driver.getCurrentUrl();
            	ExtentTestManager.getTest().log(LogStatus.INFO, "Current Url is : " + currenturl);  
            	String businessidsplit[]=currenturl.split("=");
            	String businessid=businessidsplit[1];
            	data.put("businessid", businessid);
            	ExtentTestManager.getTest().log(LogStatus.INFO, "Created Business Id is : " + businessid);  
			} 
        	catch (Exception e)
        	{
				// TODO: handle exception
        		logInfo("Business Id is not received");
			}
        	
       	}
        
       /*
        *  Get product ID 
        */

        public void GetProductIdFromBundleOverlay()
    	{
        	try {
        		if(driver.explicitWaitforVisibility(CatalogPageObject.getproductid, "ProductId"))
        		{
        		logPass("ProductId is entered");
        		String productid = driver.getText(CatalogPageObject.getproductid, "ProductId");
        		logPass("Product Id is :" +productid);
        		data.put("ProductID", productid);
        		
        		}
        		else
        		{
        			logFail("ProductId is not entered");
        		}
			} catch (Exception e) {
				// TODO: handle exception
			}
    		
    				
    	}
	 
        /*
         * Account admin store components MuthuRaghavendra
         */
        
        public void storeidsearch()
    	{
            if(driver.explicitWaitforClickable(AccountAdminObjects.storetab, "storetab"))
            {
            	driver.jsClickElement(AccountAdminObjects.storetab, "Store tab");
            	if(driver.isElementDisplayed(AccountAdminObjects.storelabel, "store label"))
            		pass("Label Displayed");
            	else
            		fail("label not displayed");
            	if(driver.jsClickElement(AccountAdminObjects.storeidfilter, "storeid filter"))
            	{
            		pass("store id filter clicked");
            	}
            	else
            		fail("Store id filter not clicked");
            	if(driver.enterText(AccountAdminObjects.storeidfilter_txtbox, properties.getProperty("AccountstoreId"), "Store id entered"))
            	{
            		pass("store id entered");
            	}
            	else
            		fail("store id not entered");
            	if(driver.jsClickElement(AccountAdminObjects.storeidsearch, "Store id search"))
            		pass("Store id search");
            	
            	if(driver.explicitWaitforClickable(By.xpath("//*[text()='"+properties.getProperty("AccountstoreId")+"']//following::button[1]"),  "Store Trigger"))
            	{
            		if(driver.jsClickElement(By.xpath("//*[text()='"+properties.getProperty("AccountstoreId")+"']//following::button[1]"), "Store Trigger"))
            			pass("Store Trigger clicked");
            	}
            	else
        			fail("Store Trigger not clicked");
            }
            
    	}
    	
    	public void microserviceselect()
    	{
            if(driver.explicitWaitforClickable(AccountAdminObjects.storeops, "Store ops btn"))
            {
            	if(driver.jsClickElement(AccountAdminObjects.storeops, "Store ops btn"))
            	pass("Store ops btn clicked");
            }
            else
            	fail("Store ops btn not clicked");
            if(driver.isElementDisplayed(AccountAdminObjects.storeid, "Storeid"))
                   	pass("Store-id displayed");
            	else
            		fail("Store-id not displayed");
            if(driver.isElementDisplayed(AccountAdminObjects.moduleassociation,  "module name"))
            {
            	if(driver.jsClickElement(AccountAdminObjects.moduleassociation, "account module clicked"))
            	pass("Account module clicked");
            }
            else
        		fail("Account module not clicked");
//            driver.changeFocus(1);
            if(driver.explicitWaitforVisibility(AccountAdminObjects.accounttitle,  "Account title displayed"))
            {
               pass("Account title displayed");
            }
            else
            	fail("Account title not displayed");
    	}
	 
    	/*
    	 * Account admin store components ends
    	 */
	 
	 
}
