package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.framework.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.FoundationObjects;
import com.skava.object.repository.HomePage;
import com.skava.object.repository.PricingObjects;

public class FoundationComponents extends ProjectWorkFlowComponents 
{	
	public void createCollection() 
	{
		driver.navigateToUrl(ExcelReader.getData("Foundation", "URL"));
		ExtentTestManager.getTest().log(LogStatus.INFO, "Navigate to signin page");
		driver.clickElement(FoundationObjects.createCollectionBtn, "CreateCollectionButton");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Create Collection Button is clicked");
		driver.enterText(FoundationObjects.collectionName, ExcelReader.getData("Foundation", "CollectionName"), "Collection Title");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Collection Name Entered");
		driver.enterText(FoundationObjects.collectionDescription, ExcelReader.getData("Foundation", "CollectionDescription"), "Collection Description");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Collection's Description Entered");
		driver.clickElement(FoundationObjects.collectionSubmitButton, "Submit Create Collection");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Create Collection Button Submitted");
		if(driver.compareElementText(FoundationObjects.collectionCreationSuccess, ExcelReader.getData("Foundation", "ExpectedMessage"), "CollectionCreationSuccessMessage")) {
			ExtentTestManager.getTest().log(LogStatus.PASS, "Collection Created Successfully");
			
		}
		else {
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Collection Creation Failed");
			
		}
	}
	
	/*
	 * Launch existing Collection
	 */
	public void launchExistingCollection() 
	{
		loadAndWaitForLoadingGauage();
		driver.explicitWaitforVisibility(FoundationObjects.collectionList, "Collection List");
		if(driver.isElementDisplayed(FoundationObjects.collectionList, "Collection List"))
		{
			logPass("The user was able to find the existing collection from the list.");
			String collectionToLoad = ExcelReader.getData("Collection", "CollectionNameToLoad");
			searchCollectionFromList(collectionToLoad);
			int collectionList = driver.getSize(FoundationObjects.collectionList, "Collection List");
			if(collectionList>0)
			{
				By collectionRowXpath = By.xpath(FoundationObjects.collectionList.toString().replaceAll("By.xpath: ", "").concat("[" + (1) + "]"));
				if(driver.mouseHoverElement(collectionRowXpath, "Random Collection Xpath"))
				{
					logPass("The user was able to move to element. The xpath used is : " + collectionRowXpath);
					driver.explicitWaitforVisibility(FoundationObjects.btn_Launch, "Collection Launch Button");
					if(driver.jsClickElement(FoundationObjects.btn_Launch, "Collection Launch Button"))
					{
						logInfo("The user was able to click on the Launch Button.");
						loadAndWaitForLoadingGauage();
						if(driver.changeFocus(1))
						{
							logPass("The user was able to switch to new Tab.");
							String currentUrl = driver.getCurrentUrl();
							logInfo("The newly launched current url is : " + currentUrl);
						}
						else
						{
							logFail("The user failed to switch to new tab.");
						}
					}
					else
					{
						logFail("The user was not able to click on the Launch Button.");
						skipException("Intentionally failing the script as the user failed to click on the Launch button.");
					}
				}
				else
				{
					logFail("The user failed to move to element. The xpath used is : " + collectionRowXpath);
				}
			}
			else
			{
				logFail("The collection seems to be empty from the list.");
				skipException("Intentionally failing the script as the Existing Collection list is empty.");
			}
		}
		else
		{
			logFail("The user failed to find the existing collection from the list.");
			skipException("Intentionally failing the script as the user failed to find the Exisiing Collection.");
		}
	}

	/*
	 * Load and Wait For Loading Guage to disappear
	 */
	protected void loadAndWaitForLoadingGauage() 
	{
		try 
		{
			Thread.sleep(2000);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		if(driver.getSize(FoundationObjects.loadingGuage, "Loading Guage")>0)
		{
			int count = 0;
			boolean isVisible = false;
			do
			{
				isVisible = driver.getElementAttributeWithoutVisibility(FoundationObjects.loadingGuage, "class").contains("none");
				count++;
			}while(count<10);
			
			if(!isVisible)
				driver.explicitWaitforVisibility(FoundationObjects.loadingGuage, "Loading Guage");
			driver.waitForElementAttribute(FoundationObjects.loadingGuage, "class", "show", "Loading Guage Show");
			try 
			{
				Thread.sleep(1000);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		driver.waitForElementAttribute(FoundationObjects.loadingGuage, "class", "none", "Loading Guage Show");
	}
	
	public void updateCollectionAssociationAndLaunch() 
	{
		//This method updates the collection association and then launches the collection
	}
	
	public void associateStore() 
	{
		//
		
	}
	
	public void enterSearchString() 
	{
		driver.enterTextAndSubmit(HomePage.searchInputField, ExcelReader.getData("General_Data","SearchString"),"searchField");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Search product");
	}
	
	/*
	 * Search Collection
	 */
	public void searchCollectionFromList(String collectionToLoad)
	{
		if(driver.isElementDisplayed(FoundationObjects.collectionNameContainer, "Collection Name Container"))
		{
			logPass("The Collection Name to Search Container is displayed.");
			if(driver.jsClickElement(FoundationObjects.collectionNameContainer, "Collection Name Container"))
			{
				logPass("The Collection Name to Search Container is clicked.");
				currentCollectionName = collectionToLoad;
				if(driver.isElementDisplayed(FoundationObjects.txt_collectionName, "Collection Name Input"))
				{
					logPass("The Collection Name text field to Search Container is not displayed.");
					if(driver.enterText(FoundationObjects.txt_collectionName, currentCollectionName , "Collection Name Input"))
					{
						logPass("The user was able to enter the text om the Collection Name text field in Pricing Page.");
						try 
						{
							Thread.sleep(1000);
						} 
						catch (InterruptedException e) 
						{
							e.printStackTrace();
						}
						
						if(driver.jsClickElement(FoundationObjects.btn_search, "Search Button"))
						{
							logInfo("The Searched Text was : " + currentCollectionName);
							try 
							{
								Thread.sleep(2500);
							} 
							catch (InterruptedException e) 
							{
								e.printStackTrace();
							}
							logPass("The user was able to click on the Search button in the Collection Page.");
							loadAndWaitForLoadingGauage();
						}
						else
						{
							logFail("The user was not able to click on the Search button in the Collection page.");
							skipException("Intentionally Failing the Script as the user failed to click the Search button.");
						}
					}
					else
					{
						logFail("The user was not able to enter the text om the Collection Name text field in Pricing Page.");
					}
				}
				else
				{
					logFail("The Collection Name text field to Search Container is not displayed.");
					skipException("Failing the Script as the Collection Name text field  is not displayed.");
				}
			}
			else
			{
				logFail("The Collection Name to Search Container is not clicked.");
				skipException("Failing the Script as the Collection Name Container is not clicked.");
			}
		}
		else
		{
			logFail("The Collection Name to Search Container is not displayed.");
			skipException("Failing the Script as the Collection Name Container is not displayed.");
		}
	}
}
