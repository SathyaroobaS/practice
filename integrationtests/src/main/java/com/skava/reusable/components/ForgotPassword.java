package com.skava.reusable.components;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.ForgotPassword_8;
import com.skava.object.repository.Login_8;


public class ForgotPassword extends EditUserComponents 
{

	 /** Triggering  Forgot Password Link In Login Screen**/
	
	public void clickForgotPasswordLinkInLoginScreen() {
		if(driver.clickElement(ForgotPassword_8.forgotPassword_Link, "Forgot Password button"))
			logPass("Forgot Password link is triggered");
		
		else
			logFail("Failed to displayed the Forgot Password screen");
	}
	
	
	/** Verifying Forgot Password Screen title **/
	
	public void verifyForgotPasswordScreenTitle() {
		if(driver.explicitWaitforVisibility(ForgotPassword_8.forgotPassword_ScreenName, "Forgot Password Screen"))
			logPass("Forgot Password title get displayed");
		else
			logFail("Failed to display screen title");
	}

	/** Navigate to Login Screen **/
	
	public void navigateBackToLoginScreen() 
	{
		driver.navigateBack();
		if(driver.isElementDisplayed(Login_8.btn_SIGNIN, "Signin Button in Login screen"))
			logPass("Navigate back to login screen");
		else
			logFail("Failed to naviagte back page");
		
	}
	
	
	/**  Email address text field in Forgot Password Screen **/
	
	public void emailAddressTextFieldIsPresent() {
		if(driver.isElementDisplayed(ForgotPassword_8.txt_Email, "Email Address Field"))
			logPass("Email address text field is Present");
		else
			logFail("Failed to display Email address text field");
	}
	
	
	
	
	/** Entering Non registered Email Address in Forgot Password screen **/
	
	public void sendingNonRegisteredEmailAddress() {
		if(driver.enterText(ForgotPassword_8.txt_Email, ExcelReader.getData("LoginCredentials", "EmailAddress"), "Email Address field"))
			logPass("Non Registered Email Address is entered in text field");
		else
			logFail("Failed too enter Email address");
	}
	
	/** Entering invalid Email Address in Forgot Password screen **/
	
	public void sendingInvalidEmailAddress() {
		if(driver.enterText(ForgotPassword_8.txt_Email, ExcelReader.getData("LoginCredentials", "EmailAddress"), "Email Address field"))
			logPass("Invalid Email Address is entered in text field");
		else
			logFail("Failed too enter Email address");
	}
	
	
	
	/** Clicking Send Link Button in Forgot Password screen **/
	
	public void clickingSendLinkButtonInForgotPassword() {
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(driver.clickElement(ForgotPassword_8.button_SendLink, "Send Link button"))
			logPass("Send Link button is clicked");
		else
			logFail("Failed to Click Send Link Button");
	}
	
	
	/** Inline Error message for non registered email address**/
	
	public void nonRegisteredEmailAddressInlineError() {/*
		if(driver.isElementDisplayed(ForgotPassword_8.inerror_NonRegisteredEmailAddress, "Non Registered Email Address"))
			logPass("Inline error message is displayed");
		else
			logFail("Failed to display non registered error message");
	*/}
	
	
	/** Inline Error message for invalid email address**/
	
	public void invalidEmailAddressInlineError() {
		if(driver.isElementDisplayed(ForgotPassword_8.inerror_invalidEmailAddress, "Invalid Email Address"))
			logPass("Inline error message is displayed");
		else
			logFail("Failed to display non registered error message");
	
	}
	
	/** Success Message on submitting registered email address **/
	
	public void successMessageForRegisteredEmailAddress() {
		if(driver.isElementDisplayed(ForgotPassword_8.txt_SuccessMessage, "Success Messagae"))
			logPass("Success message with instructions is displayed");
		else
			logFail("Error Message displayed");
	}

	/** Entering Valid New Password Field **/
	
	public void enterValidNewPasswordInRestPasswordScreen() {
		if(driver.enterText(ForgotPassword_8.txt_NewPassord, ExcelReader.getData("LoginCredentials", "NewPassword"), "New Password text field"))
			logPass("New Password is Entered");
		else
			logFail("Failed to send text in New Password field");
	}
	
	/** Verify Password Hint in Reset Password Screen **/
	
	public void verifyPasswordHintsInResetPasswordScreen() { 
		if(driver.isElementDisplayed(ForgotPassword_8.txt_NewPasswordHint, "Forgot Password Point"))
			logPass("Password Hint is displayed");
		else
			logFail("Failed to displayed the Password hint");
	}
	
	/** Entering Valid Confirm New Password Field **/
	
	public void enterValidConfirmNewPasswordInRestPasswordScreen() {
		if(driver.enterText(ForgotPassword_8.txt_ConfirmNewPassword, ExcelReader.getData("LoginCredentials", "NewPassword"), "Confirm New Password text field"))
			logPass("New Password is Entered");
		else
			logFail("Failed to send text in New Password field");
	}
	
	/** Triggering Save button in Reset PasswordScreen **/
	
	public void clickSaveButtonInResetPasswordScreen() {
		if(driver.clickElement(ForgotPassword_8.button_Save, "Save Button"))
			logPass("Save Button is cliced in Reset Password");
		else
			logFail("Failed to Click Save button");
	}
	
	/** Success Reset Message in Reset Password Screen **/
	
	public void successMessageOnValidPasswordIsReset() {
		if(driver.explicitWaitforVisibility(ForgotPassword_8.txt_SuccessReset, "Success Content"))
		{
			String txt=driver.getText(ForgotPassword_8.txt_SuccessReset, "");
			logPass("Success Password Reset text is displayed "+txt);
		}
			
		else
			logFail("Failed to display success content");
	}
	
	/** Entering Invalid New Password in Reset Screen**/
  
	  public void enteringInvalidNewPasswordInResetScreen() {
		  if(driver.enterText(ForgotPassword_8.txt_NewPassord, ExcelReader.getData("LoginCredentials", "NewPassword"), "New Password"))
			  logPass("Invalid New Password is Entered");
		  else
			  logFail("Failed to Enter New Password");
	  }
	
	/** Entering Invalid Confirm New Password in Reset Screen **/
	  
	  public void enteringInvalidConfirmNewPassowrdInResetScreen() {
		 if(driver.enterText(ForgotPassword_8.txt_ConfirmNewPassword, ExcelReader.getData("LoginCredentials", "Confirm Password"), "Confirm New Password"))
		  logPass("Invalid Confirm New Password is Entered");
		  else
			  logFail("Failed to Enter Confirm New Password");
	  }
	
	/** Inline error for Invalid New Password in Reset Password  **/
	
	public void inlineErrorForNewPasswordField() {
	String name=driver.getText(ForgotPassword_8.inerror_NewPassword,"");
	if(name.contains("valid password"))
       logPass("Inline Error for New Password " +name);
	else
		logFail("Failed to displayed Error");
	
	}
	
	/** Check for ok button in Success Screen **/
	
	public void successMessageOkButtonCheck() {/*
		if(driver.isElementDisplayed(ForgotPassword_8.button_successOk, "Ok Button"))
			logPass("Success OK Button is displayed");
		else
			logFail("Failed to display Success OK Button");
	*/}
	
	/** Navigate to login screen **/
	
	public void isLoginScreenDisplayed() {
		if(driver.isElementDisplayed(Login_8.btn_SIGNIN, "Signin Button in Login screen"))
			logPass("Navigated to login screen");
		else
			logFail("Failed to naviagate to login screen");
	}
	
	/** Click ok button in Success Screen **/
	
	public void clickOkButtonInSuccessScreen() {/*
		if(driver.clickElement(ForgotPassword_8.button_successOk, "Ok Button"))
		{
			logPass("OK Button is cliced in Success Screen");
			
			isLoginScreenDisplayed();
		}
		else
			logFail("Failed to Click OK button");
	*/}
	
	public void displayPlaceHolderForEmail()
	{
		if(driver.explicitWaitforVisibility(ForgotPassword_8.emailPlaceHolder, "Enter Your Email PlaceHolder"))
			logPass("placeholder is presented");
		else
			logFail("placeholder is not presented");
	}
	
	public void forgetPasswordDeepLink()
	{
		
		driver.navigateToUrl(properties.getProperty("ApplicationUrl")+"admin/auth/resetpassword?resetParam="+resetParam);
		
	}
	
}

