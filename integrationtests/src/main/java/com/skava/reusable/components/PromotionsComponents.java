package com.skava.reusable.components;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.networkutils.ApiReaderUtils;
import com.skava.object.repository.LogIn_Page;
import com.skava.object.repository.promotionObject;

public class PromotionsComponents extends FoundationComponents
{
	public void generateSessionId() throws JSONException
	{
		currentDomain = properties.getProperty("Domain");
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=	api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/auth/login","{\"identity\": \""+properties.getProperty("LoginUserName")+"\", \"password\": \""+properties.getProperty("LoginPassword")+"\"}","","","","");
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		sessionId = "SESSIONID="+root.getString("sessionId"); 
		pass("generatesessionId "+sessionId);
		properties.put("sessionId", sessionId);
		
	}
	
		
	String Promotioncollectionid="";
	public JSONObject createCollectionPromotion(String collectionName) throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("BusinessId")+"&serviceName=promotion","{ \"name\": \""+collectionName+"\", \"properties\": [], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId,"","","");
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		Promotioncollectionid = root.getJSONObject("collectionResponse").getString("id");
		properties.put("Promotioncollectionid", Promotioncollectionid);
		String url=properties.getProperty("ApplicationUrl");
		url=url.replace("<<collectionId>>", Promotioncollectionid);
		properties.put("ApplicationUrl",url);
		System.out.println("PromotionCollectionUrl-->"+url);		
		System.out.println("Promotioncollectionid"+Promotioncollectionid);
		
		pass("Promotioncollectionid "+Promotioncollectionid);
		return root; 
		
	}
	String getTestTokens="";
	public void generateauthforpromotionservice(String ServiceName) throws JSONException, ClientProtocolException, IOException
    {
    	
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=    Api.ReadAllDataFromUrl(properties.getProperty("APIDomain")+"/foundationservices/getTestTokens","","");
        resp = res.get(0);
        System.out.println(resp);
        pass("generatesessionIdpayment "+resp);

        JSONObject root = new JSONObject(resp);
       
        getTestTokens = root.getString("testsuperadmin");
        properties.put("getTestTokens", getTestTokens);

    }
	  public void promotionstoreassociate() throws JSONException, ClientProtocolException, IOException
	    {
	        String resp = new String();
	        List<String> res = new ArrayList<String>();
	        res=api.put(properties.getProperty("APIDomain")+"/foundationservices/stores/"+properties.getProperty("PromotionStoreId")+"/associations","{\"associations\":[{\"name\":\"promotion\",\"collectionId\":"+properties.getProperty("Promotioncollectionid")+"}]}",getTestTokens);
	        resp = res.get(0);
	        System.out.println(resp);
	        pass("storeassocatefoundation "+resp);   
	    }
	    
	
	


	
	public void logIn() 
	{
		String userName = properties.getProperty("LoginUserName");
		if(driver.enterText(LogIn_Page.txtbx_UserName, userName ,"Username"))
		{
			logInfo("The user entered login name is : " + userName);
			String userPassword = properties.getProperty("LoginPassword");
			if(driver.enterText(LogIn_Page.txtbx_Password, userPassword ,"Password"))
			{
				logInfo("The user entered login password is : " + userPassword);
				if(driver.jsClickElement(LogIn_Page.btn_LogIn,"Login button"))
				{
					logPass("The user was able to click on the Login Button.");
				}
				else
				{
					logFail("The user failed to click on the Login Button.");
					skipException("Failing the Script Intentionally as the user failed to click on the login Button.");
				}
			}
			else
			{
				logFail("The user failed to enter the enter the password in the password field.");
				skipException("Failing the Script Intentionally as the user failed to enter the password on the login screen.");
			}
		}
		else
		{
			logFail("The user failed to enter the enter the text in the User name field.");
			skipException("Failing the Script Intentionally as the user failed to enter the username on the login screen.");
		}
	}


	public void clickCreatePromotionGroup()
	{
		if(driver.explicitWaitforClickable(promotionObject.CreatePromotionGroup, "CreatePromotionGroup"))
		{
			pass("Create Promotion Group Button clickable");
			if(driver.jsClickElement(promotionObject.CreatePromotionGroup, "CreatePromotionGroup"))
				pass("Create Promotion Group button clicked");
			else
				failAndStop(" Create Promotion Group button not clicked");
		}
		else
			failAndStop("Create Promotion Group Button not clickable");
		
	}
	
	public void clickEnter()
	{
		driver.explicitWaitforClickable(By.xpath("//*[@data-qa='project-name-value']"), "Enter");
		driver.jsClickElement(By.xpath("//*[@data-qa='projectenter-trigger']"), "Enter");
	}
	
	public void verifyPromotionGroupName()
	{
		if(driver.explicitWaitforClickable(promotionObject.groupnamevalue, "group name value"))
		{
			pass("navigated group name page");
			
			
			if(driver.explicitWaitforClickable(promotionObject.daterange, "start date"))
				pass("end time picker display");
			else
				failAndStop("end time picker not display");
			
			if(driver.explicitWaitforClickable(promotionObject.statuslist, "start date"))
				pass("status list picker display");
			else
				failAndStop("status list picker not display");
			if(driver.explicitWaitforClickable(promotionObject.priorityvalue, "start date"))
				pass("priority value picker display");
			else
				failAndStop("priority value picker not display");
			if(driver.explicitWaitforClickable(promotionObject.stopfurtherprocessinglist, "start date"))
				pass("stopfurther processing list display");
			else
				failAndStop("stopfurther processing list not display");
			if(driver.explicitWaitforClickable(promotionObject.savetrigger, "start date"))
				pass("Save button display");
			else
				failAndStop("Save button not display");
			if(driver.explicitWaitforClickable(promotionObject.canceltrigger, "start date"))
				pass("cancel button display");
			else
				failAndStop("cancel button not display");
		}
	}
	public void changePromotionGroupName()
	{
		if(driver.explicitWaitforClickable(promotionObject.groupnamevalue, "group name value"))
		{
			pass("navigated group name page");
			
			
			if(driver.explicitWaitforClickable(promotionObject.daterange, "start date"))
				pass("end time picker display");
			else
				failAndStop("end time picker not display");
			
			if(driver.explicitWaitforClickable(promotionObject.statuslist, "start date"))
				pass("status list picker display");
			else
				failAndStop("status list picker not display");
			if(driver.explicitWaitforClickable(promotionObject.priorityvalue, "start date"))
				pass("priority value picker display");
			else
				failAndStop("priority value picker not display");
			if(driver.explicitWaitforClickable(promotionObject.stopfurtherprocessinglist, "start date"))
				pass("stopfurther processing list display");
			else
				failAndStop("stopfurther processing list not display");
			if(driver.explicitWaitforClickable(promotionObject.savetrigger, "start date"))
				pass("Save button display");
			else
				failAndStop("Save button not display");
			if(driver.explicitWaitforClickable(promotionObject.canceltrigger, "start date"))
				pass("cancel button display");
			else
				failAndStop("cancel button not display");
		}
	}
	
	public void enterGroupName()
	{
		if(driver.explicitWaitforClickable(promotionObject.groupnamevalue, "group name value"))
		{
			pass("navigated group name page");
		}
		
		String name = ExcelReader.getData("Promotion", "promotiongroup"); 
		int randomname=driver.generateRandomNumber(1000);
		String finalName = name+" "+Long.toString(System.currentTimeMillis())+randomname;
		
		if(driver.enterText(By.xpath("//*[@data-qa='groupname-value']//input"), finalName, "Enter Group Name"))
		
			pass("Group name is entered");
		else
			failAndStop("Group name is not entered");
		
	}
	
	public void enterPromocodeListName1()
	{
		if(driver.explicitWaitforClickable(promotionObject.groupnamevalue, "group name value"))
		{
			pass("navigated group name page");
		}
		
		String name = ExcelReader.getData("Promotion", "promotiongroup"); 
		int randomname=driver.generateRandomNumber(10000);
		String finalName = name+" "+Long.toString(System.currentTimeMillis())+randomname;
		
		if(driver.enterText(By.xpath("//*[@data-qa='groupname-value']//input"), finalName, "Enter Group Name"))
		
			pass("Group name is entered");
		else
			failAndStop("Group name is not entered");
		
	}
	
	public void clickSave()
	{
		
		if(driver.explicitWaitforClickable(promotionObject.savetrigger, "start date"))
		{
			pass("Save button display");
//			driver.scrollToElement(promotionObject.pancakemenu, "");
			if(driver.jsClickElement(promotionObject.savetrigger, "savetrigger"))
				pass("Save Button clicked");
			else
				failAndStop("Save Button not click");
		}	
		else
			failAndStop("Save button not display");
		String promotionListWaitingForSave=promotionObject.createPromotionButton.toString().replace("By.xpath: ", "")+
				"|"+promotionObject.createListButton.toString().replace("By.xpath: ", "")+"|"+promotionObject.createPromocodeList.toString().replace("By.xpath: ", "")+
				"|"+promotionObject.promotionAddListItemBtn.toString().replace("By.xpath: ", "")+"|"+promotionObject.AddPromocode.toString().replace("By.xpath: ", "");
		driver.isElementDisplayed(By.xpath(promotionListWaitingForSave), "Waiting for List Should be Saved");
	}
	public void clickSaveFix()
    {
        
        if(driver.explicitWaitforClickable(promotionObject.savetrigger, "start date"))
        {
            pass("Save button display");
            driver.scrollToElement(promotionObject.pancakemenu, "");
            if(driver.jsClickElement(promotionObject.savetrigger, "savetrigger"))
                pass("Save Button clicked");
            else
                failAndStop("Save Button not click");
        }    
        else
            failAndStop("Save button not display");
        driver.isElementDisplayed(promotionObject.promotionListSaveWaiting, "Waiting for Save");
        
    }
	public void clickCancel()
	{
		if(driver.explicitWaitforClickable(promotionObject.canceltrigger, "start date"))
		{
			pass("Save button display");
			if(driver.jsClickElement(promotionObject.canceltrigger, "savetrigger"))
				pass("cancel Button clicked");
		}	
		else
			failAndStop("cancel  button not display");
		
	}
	public void verifySucess()
	{
		if(driver.explicitWaitforVisibility(By.xpath("//*[contains(@class,'toast-success')]"), "toast message"))
			pass("successfully saved");
		else
			failAndStop("success msg is not displayed");
		
		
	}
	public void clickDateRange() throws InterruptedException
	{
		if(driver.explicitWaitforVisibility(promotionObject.daterange, "daterange"))
		{
			if(driver.jsClickElement(promotionObject.daterange, "daterange"))
				pass("daterange clicked");
		}	
		
	}
	String firstDate="";
	boolean flag=false;
	public void selectStartDate() throws InterruptedException
	{
		if(driver.explicitWaitforVisibility(promotionObject.leftsiderange, "daterange"))
		{
			int size=driver.getSize(promotionObject.leftsideavail, "Start date");
			if(size==0)
				flag=true;
			else
			{
			int random=0;
			if(size==1)
				random=1;
			else
				random=size/2;
				
 			firstDate=driver.getText(By.xpath("(//*[@class='drp-calendar left']//td[contains(@class,'available')])["+random+"]"), "Start date");
			pass("select start date "+firstDate);
			driver.clickElement(By.xpath("(//*[@class='drp-calendar left']//td[contains(@class,'available')])["+random+"]"), "Element");
			
			}

		}
	}
	
	public void selectEndDate() throws InterruptedException
	{
		if(!flag)
		{
		if(driver.explicitWaitforVisibility(promotionObject.rightsiderange, "daterange"))
		{
			int size=driver.getSize(promotionObject.rightsideavail, "Start date");
			int random=0;
			if(size==1)
				random=1;
			else
				random=size/2;
			String SecondDate=driver.getText(By.xpath("(//*[@class='drp-calendar right']//td[contains(@class,'available')])["+random+"]"), "Start date");
			if(firstDate.equals(SecondDate))
				random=random+1;
			pass("select end date "+SecondDate);
			driver.mouseHoverElement(By.xpath("(//*[@class='drp-calendar right']//td[contains(@class,'available')])["+random+"]"), "");
			driver.clickElement(By.xpath("(//*[@class='drp-calendar right']//td[contains(@class,'available')])["+random+"]"), "rightside");
			
		}
		}
		else
		{
			int size=driver.getSize(promotionObject.rightsideavail, "Start date");
			for(int i=1;i<=2;i++)
			{	
				if(i==2)
					i=size;
				String SecondDate=driver.getText(By.xpath("(//*[@class='drp-calendar right']//td[contains(@class,'available')])["+i+"]"), "Start date");
				pass("select end date "+SecondDate);
				driver.mouseHoverElement(By.xpath("(//*[@class='drp-calendar right']//td[contains(@class,'available')])["+i+"]"), "");
				driver.clickElement(By.xpath("(//*[@class='drp-calendar right']//td[contains(@class,'available')])["+i+"]"), "rightside");
			}
			
		}
	}
	
	public void clickApplyInDatePicker()
	{
		if(driver.jsClickElement(promotionObject.applyButtonDatePicker, "Click Apply button"))
			pass("Apply button is clicked");
		else
			failAndStop("Apply button is not clicked");
	}
	
	
	
	public void clickStatus()
	{
		if(driver.explicitWaitforVisibility(promotionObject.statuslist, "Status"))
		{
			if(driver.jsClickElement(By.xpath("//*[@data-qa='status-list']//input"), "Status"))
				pass("Status field click");
		}
		
	}
	public void  selectStatus()
	{
		if(waitUntilLoadingCompleteAndClick())
		{
			if(driver.explicitWaitforClickable(promotionObject.statuslist, "Status list"))
			{
				driver.jsClickElement(By.xpath("//*[@data-qa='status-list']//input"), "click JS");
				int size=driver.getSize(By.xpath("//*[@data-qa='status-list']//ul//li"), "Status list");
				int random=driver.generateRandomNumber(size)+1;
				String txt=driver.getText(By.xpath("(//*[@data-qa='status-list']//ul//li)["+random+"]"), "Str");
				if(driver.jsClickElement(By.xpath("(//*[@data-qa='status-list']//ul//li)["+random+"]"), "Status list"))
					pass("select "+txt);
				else
					failAndStop("Not selectable");
			}
		}
		
	}
	
	
	public void displayStatusActive()
	{
		if(driver.explicitWaitforClickable(promotionObject.statuslist, "Status list"))
		{
			int size=driver.getSize(By.xpath("//*[@data-qa='status-list']//ul//li"), "Status list");
			//int random=driver.generateRandomNumber(size)+1;
			for(int i=1;i<=size;i++)
			{
				String txt=driver.getText(By.xpath("(//*[@data-qa='status-list']//ul//li)["+i+"]"), "Str");
				if(txt.equalsIgnoreCase("Active"))
					pass("Active status is displayed");
				else 
					failAndStop("Active status is not displayed");			
			}
		}
	}
	
	public void displayStatusInActive()
	{
		if(driver.explicitWaitforClickable(promotionObject.statuslist, "Status list"))
		{
			int size=driver.getSize(By.xpath("//*[@data-qa='status-list']//ul//li"), "Status list");
			//int random=driver.generateRandomNumber(size)+1;
			for(int i=1;i<=size;i++)
			{
				String txt=driver.getText(By.xpath("(//*[@data-qa='status-list']//ul//li)["+i+"]"), "Str");
				if(txt.equalsIgnoreCase("InActive"))
					pass("Active status is displayed");
				else 
					failAndStop("Active status is not displayed");			
			}
		}
	}
	
	public boolean waitUntilLoadingCompleteAndClick()
	{
		return driver.explicitWaitforInVisibility(By.xpath("//*[@id='id_loader']"), "loading gauge");
	}
	
	public void selectstatusactive()
	{
		if(waitUntilLoadingCompleteAndClick())
		{
			if(driver.explicitWaitforClickable(promotionObject.statuslist, "Status list"))
			{
				String txt=driver.getText(promotionObject.statusactive, "statusactive");
				if(txt.equalsIgnoreCase("Active"))
				{
					if(driver.clickElement(promotionObject.statusactive, "Status list"))
						pass("select "+txt);
					else
						failAndStop("Not selectable");						
				}
			}
		}
	}
	public void selectStatusInactive()
	{
		if(waitUntilLoadingCompleteAndClick())
		{			
			if(driver.explicitWaitforClickable(promotionObject.statuslist, "Status list"))
			{
				int size=driver.getSize(By.xpath("//*[@data-qa='status-list']//ul//li"), "Status list");
				//int random=driver.generateRandomNumber(size)+1;
				for(int i=1;i<=size;i++)
				{
					String txt=driver.getText(By.xpath("(//*[@data-qa='status-list']//ul//li)["+i+"]"), "Str");
					if(txt.equalsIgnoreCase("InActive"))
					{
						if(driver.jsClickElement(By.xpath("(//*[@data-qa='status-list']//ul//li)["+i+"]"), "Status list"))
							pass("select "+txt);
						else
							failAndStop("Not selectable");
						break;
					}		
				}
			}
		}
	}
		
	
	public void priorityClick()
	{
		if(driver.jsClickElement(promotionObject.priorityvalue, "Priority Value"))
			pass("priority clicked");
		else
			fail("Priority not clicked");
		
	}
	
	
	public void priorityValue()
	{
		if(driver.enterText(promotionObject.priorityvalue, ExcelReader.getData("Promotion","priorityvalue"), "Priority Value"))
			pass("priority value "+ExcelReader.getData("Promotion","priorityvalue")+" entered");
		else
			failAndStop("not enterable");
		
	}
	
	public void selectStopFurtherProcessing()
	{
		if(driver.jsClickElement(By.xpath("//*[@data-qa='stopfurtherprocessing-list']//input"), "stopfurtherprocessing-list"))
			pass("StopFurtherProcessing dropdown is clicked ");
		else
			failAndStop("Stop further processing dropdown is not clicked");
		
	}
	
	
	public void  selectNeverStopFurtherProcessinglist()
	{
		if(driver.explicitWaitforClickable(promotionObject.stopfurtherprocessinglist, "stopfurtherprocessing-list"))
		{
			if(driver.jsClickElement(By.xpath("(//*[@data-qa='stopfurtherprocessing-list']//ul//li)[2]"), "stopfurtherprocessing-list"))
				pass("never is select ");
			else
				failAndStop("Not selectable");
		}
		
	}
	
	public void  selectAlwaysStopFurtherProcessinglist()
	{
		if(driver.explicitWaitforClickable(promotionObject.stopfurtherprocessinglist, "stopfurtherprocessing-list"))
		{
			if(driver.jsClickElement(By.xpath("(//*[@data-qa='stopfurtherprocessing-list']//ul//li)[3]"), "stopfurtherprocessing-list"))
				pass("Always is select ");
			else
				failAndStop("Not selectable");
		}
		
	}
	
	public void  selectIfStopConditionStopFurtherProcessinglist()
	{
		if(driver.explicitWaitforClickable(promotionObject.stopfurtherprocessinglist, "stopfurtherprocessing-list"))
		{
			if(driver.jsClickElement(By.xpath("(//*[@data-qa='stopfurtherprocessing-list']//ul//li)[4]"), "stopfurtherprocessing-list"))
				pass("If Stop Condition is select ");
			else
				failAndStop("Not selectable");
		}
		
	}
	
	public void clickPromotionRuleTypeList()
	{
		if(driver.explicitWaitforClickable(promotionObject.promotionruletypelist, "promotionruletypelist"))
		{
			if(driver.jsClickElement(promotionObject.promotionruletypelist, "promotionruletypelist"))
				pass("promotionruletypelist click");
			else
				fail("promotion rule type list not clicked");
		}
		else
			fail("promotion rule type list not found");
	}                                     
	
	public void  selectPromotionRuleTypeList()
	{
		if(driver.explicitWaitforClickable(promotionObject.promotionruletypelist, "promotionruletypelist"))
		{
			int size=driver.getSize(By.xpath("//*[@data-qa='promotionruletype-list']//ul//li"), "promotionruletype-list");
			int random=driver.generateRandomNumber(size)+1;
			String txt=driver.getText(By.xpath("(//*[@data-qa='promotionruletype-list']//ul//li)["+random+"]"), "promotionruletype-list");
			if(driver.jsClickElement(By.xpath("(//*[@data-qa='promotionruletype-list']//ul//li)["+random+"]"), "promotionruletype-list"))
				pass("select "+txt);
			else
				failAndStop("Not selectable");
		}
		else
			fail("Promotion rule type not clicked");
		
	}
	
	public void clickPromotionActionTypeList()
	{
		if(driver.explicitWaitforClickable(promotionObject.promotionactiontypelist, "promotionactiontypelist"))
		{
			if(driver.jsClickElement(promotionObject.promotionactiontypelist, "promotionactiontypelist"))
				pass("promotionactiontypelist click");
			else
				fail("promotion action type not clicked");
		}
		else
			fail("promotion action type list is not present");
	}
	
	public void  selectPromotionActionTypeList()
	{
		if(driver.explicitWaitforClickable(promotionObject.promotionactiontypelist, "promotionactiontypelist"))
		{
			int size=driver.getSize(By.xpath("//*[@data-qa='promotionactiontype-list']//ul//li"), "promotionactiontype-list");
			int random=driver.generateRandomNumber(size)+1;
			String txt=driver.getText(By.xpath("(//*[@data-qa='promotionactiontype-list']//ul//li)["+random+"]"), "promotionactiontype-list");
			if(driver.jsClickElement(By.xpath("(//*[@data-qa='promotionactiontype-list']//ul//li)["+random+"]"), "promotionactiontype-list"))
				pass("select "+txt);
			else
				failAndStop("Not selectable");
		}
		
	}
	
public void promotionViewListPage()
{
	
	if(driver.explicitWaitforVisibility(promotionObject.promotionGroupListHeader, "PromotionGroupLIstHeader"))
		pass("PromotionGroup List Header is Displayed");
	else
		failAndStop("PromotionGroup List Header is not Displayed");
	
}

//promotion creation 

/*public void PromotionCreateButtonDisplayed()
{
	if(driver.explicitWaitforClickable(By.xpath("//*[@id='id_createPromotion']"), "promotionacreatebutton"))

		pass("Promotion creation button is displayed");
	else
		failAndStop("Promotion creation button is not displayed");		
	
}*/

public void clickPromotionCreateButton()
{
	if(driver.explicitWaitforVisibility(promotionObject.createPromotionButton, "promotion create button"))
	{
		if(driver.jsClickElement(promotionObject.createPromotionButton, "promotion create button"))
			pass("Promotion Create button is clicked");
		else
			failAndStop("Promotion creation button is not clicked");	
	}
	
}


public void clickPromotionNameField()
{
	if(driver.clickElement(promotionObject.promotionnamelabel, "Promotionnamefield"))
		pass("Promotion Name Field is clicked");
	else
		failAndStop("Promotion Name Field is not clicked");
}

public void enterPromotionName()
{
	if(driver.explicitWaitforClickable(promotionObject.promotionnamelabel, "promotion name value"))
	{
		pass("navigated Promotion page");
	}
	
	String name = ExcelReader.getData("Promotion", "promotionname"); 
	int randomname=driver.generateRandomNumber(10000);
	String finalName = name+" "+Long.toString(System.currentTimeMillis())+randomname;
	
	if(driver.enterText(promotionObject.promotionnameinput, finalName, "Enter promotion Name"))
		pass("Promotion name is entered");
	else
		failAndStop("Promotion name is not entered");
	
}


public void enterPromotionNameNumber()
{
	if(driver.explicitWaitforClickable(promotionObject.promotionnamelabel, "promotion name value"))
	{
		pass("navigated Promotion page");
	}
	
	String name = "12323432434"; 
	int randomname=driver.generateRandomNumber(10000);
	String finalName = name+Long.toString(System.currentTimeMillis())+randomname;
	
	if(driver.enterText(By.xpath("//*[@data-qa='groupname-value']//input"), finalName, "Enter promotion Name"))
	
		pass("Promotion name is entered");
	else
		failAndStop("Promotion name is not entered");
	
}

public void enterPromotionNameSpecialCharacters()
{
	if(driver.explicitWaitforClickable(promotionObject.promotionnamelabel, "Promotion name value"))
	{
		pass("navigated Promotion page");
	}
	
	String name = "Skava!@#$"; 
	int randomname=driver.generateRandomNumber(10000);
	String finalName = name+Long.toString(System.currentTimeMillis())+randomname;
	
	if(driver.enterText(By.xpath("//*[@data-qa='groupname-value']//input"), finalName, "Enter Promotion Name"))
	
		pass("Promotion name is entered");
	else
		failAndStop("Promotion name is not entered");
		
}

/*//following need to 
public void clickAddGroup()
{
	int size = driver.getSize(By.xpath("//div[@id='id_skQueryBuilder']//button[@data-add='group']"),"AddGroup");
	if(driver.jsClickElement(By.xpath("(//div[@id='id_skQueryBuilder']//button[@data-add='group'])["+size+"]"), "Filter"))
		pass("Add Group clicked");
	else
		failAndStop("Add Group not clicked");
}


public void clickRecentKeyFilter()
{
	int size = driver.getSize(By.xpath("//div[contains(@class,'rule-filter-container')]//input"),"Filter");
	if(driver.jsClickElement(By.xpath("(//div[contains(@class,'rule-filter-container')]//input)["+size+"]"), "Filter"))
		pass("");
	else
		failAndStop("");
}

public void clickRecentKeyOperator()
{
	int size = driver.getSize(By.xpath("//div[contains(@class,'rule-operator-container')]"),"Filter");
	if(driver.clickElement(By.xpath("(//div[contains(@class,'rule-operator-container')])["+size+"]"), "Filter"))
		pass("");
	else
		failAndStop("");
}


public void selectKeyOption(String opn)
{
	int contLen = driver.getSize(By.xpath("//div[contains(@class,'rule-filter-container')]//input"),"Filter");
	int size = driver.getSize(By.xpath("(//div[contains(@class,'rule-filter-container')])["+contLen+"]//ul//li"), "KeyOptions");
	for(int i=1;i<=size;i++)
	{
		String val = driver.getText(By.xpath("((//div[contains(@class,'rule-filter-container')])["+contLen+"]//ul//li)["+i+"]"), "KeyOptions").toLowerCase();
		if(val.equals(opn))
		{
			driver.jsClickElement(By.xpath("((//div[contains(@class,'rule-filter-container')])["+contLen+"]//ul//li)["+i+"]"), "KeyOptions");
			pass("KeyOption is selected "+opn);
			break;
		}
		else if(i==size)
			failAndStop("KeyOption is not present "+opn);
		else
			continue;
	}
}

public void enterKeyValueOperator(String opn)
{
	int contLen = driver.getSize(By.xpath("//div[contains(@class,'rule-operator-container')]"),"Filter");
	int size = driver.getSize(By.xpath("(//div[contains(@class,'rule-operator-container')])["+contLen+"]//ul//li"), "KeyOptions");
	for(int i=1;i<=size;i++)
	{
		String val = driver.getText(By.xpath("((//div[contains(@class,'rule-operator-container')])["+contLen+"]//ul//li)["+i+"]"), "KeyOptions").toLowerCase();
		if(val.equals(opn))
		{
			driver.jsClickElement(By.xpath("((//div[contains(@class,'rule-operator-container')])["+contLen+"]//ul//li)["+i+"]"), "KeyOptions");
			pass("KeyValueOption is selected "+opn);
			break;
		}
		else if(i==size)
			failAndStop("KeyValueOption is not present "+opn);
		else
			continue;
	}
}





public void clickNOTOperator()
{
	int size = driver.getSize(By.xpath("//div[@id='id_skQueryBuilder']//button[@data-not='group']"),"NOT Operator");
	if(driver.jsClickElement(By.xpath("(//div[@id='id_skQueryBuilder']//button[@data-not='group'])["+size+"]"), "NOT Operator"))
		pass("NOT Operator clicked");
	else
		failAndStop("NOT Operator not clicked");
}

public void clickANDOperator()
{
	int size = driver.getSize(By.xpath("//div[@id='id_skQueryBuilder']//input[@value='AND']"),"Filter");
	if(driver.jsClickElement(By.xpath("(//div[@id='id_skQueryBuilder']//input[@value='AND'])["+size+"]"), "Filter"))
		pass("AND Operator Clicked");
	else
		failAndStop("AND Operator not Clicked");
}

public void clickOROperator()
{
	int size = driver.getSize(By.xpath("//div[@id='id_skQueryBuilder']//input[@value='OR']"),"Filter");
	if(driver.clickElement(By.xpath("(//div[@id='id_skQueryBuilder']//input[@value='OR']/parent::label)["+size+"]"), "Filter"))
		pass("OR Operator Clicked");
	else
		failAndStop("OR Operator not Clicked");
}
	
public void clickDeleteInGroup()
{
	int size = driver.getSize(By.xpath("//div[@id='id_skQueryBuilder']//button[@data-delete='group']"),"Filter");
	if(driver.jsClickElement(By.xpath("(//div[@id='id_skQueryBuilder']//button[@data-delete='group'])["+size+"]"), "Filter"))
		pass("");
	else
		failAndStop("");
}

public void clickDeleteInRule()
{
	int size = driver.getSize(By.xpath("//div[@id='id_skQueryBuilder']//button[@data-delete='rule']"),"Filter");
	if(driver.jsClickElement(By.xpath("(//div[@id='id_skQueryBuilder']//button[@data-delete='rule'])["+size+"]"), "Filter"))
		pass("");
	else
		failAndStop("");
}

//NOT //div[@id='id_skQueryBuilder']//button[@data-not='group']
//AND //div[@id='id_skQueryBuilder']//input[@value='AND']
//OR //div[@id='id_skQueryBuilder']//input[@value='OR']

//Delete Group //div[@id='id_skQueryBuilder']//button[@data-delete='group']
//Delete Rule //div[@id='id_skQueryBuilder']//button[@data-delete='rule']

public void verifyInnerAddGroupButton()
{
	int size = driver.getSize(By.xpath("//div[@id='id_skQueryBuilder']//div[@class='rules-group-body']//button[@data-add='group']"),"Filter");
	if(size==1)
		pass("Inner AddGroup Button is not displayed");
	else
		failAndStop("Inner AddGroup Button is displayed");
}

public void clickKeyValueField()
{
	int size = driver.getSize(By.xpath("//div[contains(@class,'rule-value-container')]"),"Filter");
	if(driver.jsClickElement(By.xpath("(//div[contains(@class,'rule-value-container')])["+size+"]"), "Filter"))
		pass("");
	else
		failAndStop("");
}

public void entertextInKeyValue(String inp)
{
	int size = driver.getSize(By.xpath("//div[contains(@class,'rule-value-container')]//input"),"Filter");
	if(driver.enterText(By.xpath("(//div[contains(@class,'rule-value-container')]//input)["+size+"]"), inp, "entervalue"))
		pass("value is entered");
	else
		failAndStop("value is not entered");
}

public void clickGenerateConditionButton()
{
	if(driver.jsClickElement(By.xpath("//*[@id='id_skStopConditionsQBGenerate']"), "GenerateCondition"))
		pass("Condition generate Succuess");
	else
		failAndStop("Condition generate not Succuess");
}

public void clickGenerateConditionButton1()
{
	if(driver.clickElement(By.xpath("//*[@id='id_skStopConditionsQBGenerate']"), "GenerateCondition"))
		pass("Condition generate Succuess");
	else
		failAndStop("Condition generate not Succuess");
}


public void displayGenerateResult()
{
	if(driver.isElementDisplayed(By.xpath("//div[contains(@class,'cls_skStopConditionsQBAlert')]//div[contains(@class,'alert')]"), "GenerateConditionResult"))
		pass("result displayed");
	else
		pass("result not displayed");
}*/

public void clickPromotionStatus()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionstatuslabel, "Status"))
	{
		if(driver.jsClickElement(promotionObject.promotionstatusdropdown, "Status"))
			pass("Status field click");
	}
	
}
public void  selectPromotionStatus() throws InterruptedException
{
		driver.explicitWaitforClickable(By.xpath("(//*[@data-qa='promotionstatus-list']//ul//li)[2]"), "Str");
		String txt=driver.getText(By.xpath("(//*[@data-qa='promotionstatus-list']//ul//li)[2]"), "Str");
		if(driver.jsClickElement(By.xpath("(//*[@data-qa='promotionstatus-list']//ul//li)[2]"), "Status list"))
			pass("select "+txt);
		else
			failAndStop("Not selectable");
	
	
}
public void enterMessage()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionmessagelabel, "Message"))
	{
		if(driver.enterText(promotionObject.promotionmessagetextbox, ExcelReader.getData("Promotion", "message"), "message"))
			pass("Message "+ExcelReader.getData("Promotion", "message"));
	}
	else
		failAndStop("Message field not display");
}

public void enterDescription()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotiondescriptionlabel, "description"))
	{
		if(driver.enterText(promotionObject.promotiondescriptiontextbox, ExcelReader.getData("Promotion", "description"), "description"))
			pass("Message "+ExcelReader.getData("Promotion", "description"));
	}
	else
		failAndStop("description field not display");
}
public void clickRuletype()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionruletypelist, "promotion ruletype list"))
	{
		if(driver.jsClickElement(promotionObject.promotionruletypelistinput, "promotion rule type list"))
			pass("promotion rule type list field click");
	}
	
}

public void  selectRuletype()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionruletypelist, "promotion ruletype list"))
	{
		driver.explicitWaitforClickable(By.xpath("//*[contains(text(),'Offer On')]/preceding::ul[1]//li//*[text()='"+ExcelReader.getData("Promotion", "ruletype")+"']"), "promotion ruletype list");
		String txt=driver.getText(By.xpath("//*[contains(text(),'Offer On')]/preceding::ul[1]//li//*[text()='"+ExcelReader.getData("Promotion", "ruletype")+"']"), "promotion ruletype list");
		if(driver.jsClickElement(By.xpath("//*[contains(text(),'Offer On')]/preceding::ul[1]//li//*[text()='"+ExcelReader.getData("Promotion", "ruletype")+"']"), "promotion ruletype list"))
			pass("select "+txt);
		else
			failAndStop("Not selectable");
	}
}
public void clickActiontype()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionactiontypelist, "promotion action type list"))
	{
		if(driver.jsClickElement(promotionObject.promotionactiontypelistinput, "promotion action type list"))
			pass("promotion action type list field click");
	}
	
}
public void  selectActiontype()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionactiontypelist, "promotion ruletype list"))
	{
		
		String txt=driver.getText(By.xpath("//*[contains(text(),'Offer Type')]/preceding::ul[1]//li//*[text()='"+ExcelReader.getData("Promotion", "actiontype")+"']"), "promotion ruletype list");
		if(driver.jsClickElement(By.xpath("//*[contains(text(),'Offer Type')]/preceding::ul[1]//li//*[text()='"+ExcelReader.getData("Promotion", "actiontype")+"']"), "promotion ruletype list"))
			pass("select "+txt);
		else
			failAndStop("Not selectable");
	}
}

public void  selectActiontype2()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionactiontypelist, "promotion ruletype list"))
	{
		
		String txt=driver.getText(By.xpath("//*[contains(text(),'Offer Type')]/preceding::ul[1]//li//*[text()='"+ExcelReader.getData("Promotion", "actiontype2")+"']"), "promotion ruletype list");
		if(driver.jsClickElement(By.xpath("//*[contains(text(),'Offer Type')]/preceding::ul[1]//li//*[text()='"+ExcelReader.getData("Promotion", "actiontype2")+"']"), "promotion ruletype list"))
			pass("select "+txt);
		else
			failAndStop("Not selectable");
	}
}
public void  selectActiontype3()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionactiontypelist, "promotion ruletype list"))
	{
		
		String txt=driver.getText(By.xpath("//*[contains(text(),'Offer Type')]/preceding::ul[1]//li//*[text()='"+ExcelReader.getData("Promotion", "actiontype3")+"']"), "promotion ruletype list");
		if(driver.jsClickElement(By.xpath("//*[contains(text(),'Offer Type')]/preceding::ul[1]//li//*[text()='"+ExcelReader.getData("Promotion", "actiontype")+"']"), "promotion ruletype list"))
			pass("select "+txt);
		else
			failAndStop("Not selectable");
	}
}


public void promotionPriorityvalue()
{
	if(driver.enterText(promotionObject.promotionprioritytextbox, ExcelReader.getData("Promotion","promotionpriorityvalue"), "Priority Value"))
		pass("priority value "+ExcelReader.getData("Promotion","promotionpriorityvalue")+" entered");
	else
		failAndStop("not enterable");
	
}
public void promotionSave()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionsave, "promotion save"))
	{
		pass("Save button display");
		if(driver.jsClickElement(promotionObject.promotionsave, "save trigger"))
			pass("Save Button clicked");
		
		if(driver.explicitWaitforVisibility(promotionObject.promotionEditButton, "edit"))
			pass("after promotion save change edit button");
	}	
	else
		failAndStop("Save button not display");
	
}
public void createCondition() throws InterruptedException
{	
	driver.scrollToElement(promotionObject.promotionconditions, "promotion conditions tab");
	if(driver.explicitWaitforVisibility(promotionObject.conditionEdit, "Edit Button"))
	{
		driver.jsClickElement(promotionObject.conditionEdit, "Edit Button");
		pass("Edit button clicked");
	}
	else
		fail("Condition tab Edit button not found");
}

public void clickEditAction() throws InterruptedException
{
	
	driver.scrollToElement(promotionObject.promotionconditions, "promotion conditions tab");
	if(driver.explicitWaitforVisibility(promotionObject.actionsEdit, "Actions Edit Button"))
	{
		driver.jsClickElement(promotionObject.actionsEdit, "Edit Button");
		pass("Edit button clicked");
	}
	else
		fail("Condition tab Edit button not found");
}

public void clickRuleGroup()
{
	if(driver.explicitWaitforVisibility(promotionObject.rulesgroup, "rulesgroup"))
	{
		pass("rules group list display");
		if(driver.jsClickElement(promotionObject.rulesgroup, "rulesgroup"))
			pass("rules group button clicked");
	}
	else
		failAndStop("rules group field not display");
}
public void clickActionGroup()
{
	if(driver.explicitWaitforVisibility(promotionObject.actiongroup, "rulesgroup"))
	{
		pass("rules group list display");
		if(driver.jsClickElement(promotionObject.actiongroup, "rulesgroup"))
			pass("rules group button clicked");
	}
	else
		failAndStop("rules group field not display");
}
int clickrule=1;
int rulename=1;
public void clickRule()
{
	driver.scrollToElement(By.xpath("(//*[@class='rules-group-body']//ul)["+clickrule+"]/li//*[text()='"+ExcelReader.getData("Promotion", "rulename"+rulename+"")+"']"),  "rules group");
	if(driver.jsClickElement(By.xpath("(//*[@class='rules-group-body']//ul)["+clickrule+"]/li//*[text()='"+ExcelReader.getData("Promotion", "rulename"+rulename+"")+"']"),  "rules group"))
	{
		pass("Rule name "+ExcelReader.getData("Promotion", "rulename"+rulename+"")+" clicked ");
		clickrule=clickrule+2;
		rulename=rulename+1;
	}
	else
		failAndStop("This field not display "+ExcelReader.getData("Promotion", "rulename"+rulename+""));
}

public void clickStopFurtherRule()
{
	driver.scrollToElement(By.xpath("(//*[@class='rules-group-body']//ul)["+clickrule+"]/li//*[text()='"+ExcelReader.getData("Promotion", "rulename"+rulename+"")+"']"),  "rules group");
	if(driver.jsClickElement(By.xpath("(//*[@class='rules-group-body']//ul)["+clickrule+"]/li//*[text()='"+ExcelReader.getData("Promotion", "rulename"+rulename+"")+"']"),  "rules group"))
	{
		pass("Rule name "+ExcelReader.getData("Promotion", "rulename"+rulename+"")+" clicked ");
		
	}
	else
		failAndStop("This field not display "+ExcelReader.getData("Promotion", "rulename"+rulename+""));
}
public void clickStopFurtherRuleValue()
{
	if(driver.enterText(By.xpath("(//*[@class='rules-group-body']//input)["+rulevalue+"]"), ExcelReader.getData("Promotion", "rulevalue"+valuecount+""), "rules value"))
	{
		pass("Entered rule value "+ExcelReader.getData("Promotion", "rulevalue"+valuecount+""));
		
	}
	else
		failAndStop("This field not display "+ExcelReader.getData("Promotion", "rulevalue"+valuecount+""));
	
}
int clickaction=1;
int actionname=1;
public void clickAction() throws InterruptedException
{	
	/*driver.scrollToElement(By.xpath("((//*[@class='rules-group-body']//ul)/li//*[text()='"+ExcelReader.getData("Promotion", "actionname"+actionname+"")+"'])["+clickaction+"]"),  "rules group");
	if(driver.jsClickElement(By.xpath("((//*[@class='rules-group-body']//ul)/li//*[text()='"+ExcelReader.getData("Promotion", "actionname"+actionname+"")+"'])["+clickaction+"]"),  "rules group"))
	{
		pass("Rule name "+ExcelReader.getData("Promotion", "actionname"+actionname+"")+" clicked ");
		clickaction=clickaction+1;
		actionname=actionname+1;
	}
	else
		failAndStop("This field not display "+ExcelReader.getData("Promotion", "actionname"+actionname+""));*/
	driver.scrollToElement(By.xpath("(//*[@class='rules-group-body']//*[contains(@class,'dropdown-content select-dropdown active')]//*[text()='"+ExcelReader.getData("Promotion", "actionname"+actionname+"")+"'])[1]"),  "rules group");
	if(driver.jsClickElement(By.xpath("(//*[@class='rules-group-body']//*[contains(@class,'dropdown-content select-dropdown active')]//*[text()='"+ExcelReader.getData("Promotion", "actionname"+actionname+"")+"'])[1]"),  "rules group"))
	{
		pass("Rule name "+ExcelReader.getData("Promotion", "actionname"+actionname+"")+" clicked ");
		clickaction=clickaction+1;
		actionname=actionname+1;
	}
	else
		failAndStop("This field not display "+ExcelReader.getData("Promotion", "actionname"+actionname+""));
}
int rulevalue=3;
int valuecount=1;
public void rulesValue()
{
	if(driver.enterText(By.xpath("(//*[@class='rules-group-body']//input)["+rulevalue+"]"), ExcelReader.getData("Promotion", "rulevalue"+valuecount+""), "rules value"))
	{
		pass("Entered rule value "+ExcelReader.getData("Promotion", "rulevalue"+valuecount+""));
		rulevalue=rulevalue+3;
		valuecount=valuecount+1;
	}
	else
		failAndStop("This field not display "+ExcelReader.getData("Promotion", "rulevalue"+valuecount+""));
	
}
int betweencount=1;
public void changeRuleBetween()
{
	int size=driver.getSize(By.xpath("(//*[@class='rules-group-body']//input)"), "");
	if(driver.enterText(By.xpath("(//*[@class='rules-group-body']//input)["+size+"]"), ExcelReader.getData("Promotion", "between"+betweencount+""), "between value"))
	{
		pass("Entered rule value "+ExcelReader.getData("Promotion", "between"+betweencount+""));
				
	}
	else
		failAndStop("This field not display "+ExcelReader.getData("Promotion", "rulevalue"+valuecount+""));
	
}
int actionvalue=3;
int actioncount=1;
public void actionValue()
{
	//if(driver.enterText(By.xpath("(//*[@class='rules-group-body']//input)["+actionvalue+"]"), ExcelReader.getData("Promotion", "actionvalue"+actioncount+""), "rules value"))
	if(driver.enterText(By.xpath("(//*[@class='rules-group-body'])[2]//following::input["+actionvalue+"]"), ExcelReader.getData("Promotion", "actionvalue"+actioncount+""), "rules value"))
	{
		pass("Entered rule value "+ExcelReader.getData("Promotion", "actionvalue"+actioncount+""));
		actionvalue=actionvalue+3;
		actioncount=actioncount+1;
	}
	else
		failAndStop("This field not display "+ExcelReader.getData("Promotion", "actionvalue"+actioncount+""));
	
}
public void clickAddRule()
{
	if(driver.explicitWaitforVisibility(promotionObject.addrule, "Add Rule"))
	{
		pass("Add Rule display");
		if(driver.jsClickElement(promotionObject.addrule, "Add Rule"))
			pass("Add Rule clicked ");
	}
}
public void clickActionRule()
{
	if(driver.explicitWaitforVisibility(promotionObject.actionrule, "Add Rule"))
	{
		pass("Add Rule display");
		if(driver.jsClickElement(promotionObject.actionrule, "Add Rule"))
			pass("Add Rule clicked ");
	}
}

public void clickORButton()
{  
	
	
		if(driver.jsClickElement(promotionObject.ORButton, "ORButton"))
			pass("OR button clicked ");
		else
			failAndStop("OR button Not display");
	
}
public void clickActionORButton()
{
	
		if(driver.jsClickElement(promotionObject.actionORButton, "ORButton"))
			pass("OR button clicked ");
	else
		failAndStop("OR button Not display");
	
}
public void clickANDButton()
{
	
	
		if(driver.jsClickElement(promotionObject.ANDButton, "AND Button"))
			pass("AND Button clicked ");
	else
		failAndStop("AND button Not display");
}
public void clickActionANDButton()
{
	
		if(driver.jsClickElement(promotionObject.actionANDButton, "AND Button"))
			pass("AND Button clicked ");
		else
			failAndStop("AND button Not display");
	
}

public void clickConditionSave() throws InterruptedException
{
		/*driver.scrollToElement(promotionObject.promotionconditions, "Save");
		pass("Save display");
		if(driver.clickElement(promotionObject.Save, "Save"))
			pass("Save clicked ");
		else
			failAndStop("Save button not display");*/
	
}
public void updateChecking()
{
	/*if(driver.explicitWaitforVisibility(promotionObject.Update, "update"))
	{
		String update=driver.getText(promotionObject.Updatevalue, "Updatevalue");
		pass("Display value "+update);
	}
	fail("Update field not displayed");
		*/
}
//*[text()='Update']
public void clickActionSave()
{
	/*driver.scrollToElement(promotionObject.promotionconditions,  "Save actions");
		if(driver.jsClickElement(promotionObject.Save, "Save"))
			pass("Save clicked ");
		else
			failAndStop("save button not display");*/
	
}

public void clickActionTab()
{
		
		if(driver.jsClickElement(promotionObject.promotionactions, "promotion actions"))
			pass("promotion actions clicked ");
	else
		failAndStop("promotion actions not display");
}
public void clickCreateAction()
{
	if(driver.explicitWaitforVisibility(promotionObject.createactions, "create actions"))
	{
		pass("create actions display");
		if(driver.jsClickElement(promotionObject.createactions, "create actions"))
			pass("create actions clicked ");
	}
	else
		failAndStop("create actions button not display");
}


public void enterActionName()
{
	driver.scrollToElement(promotionObject.actionname, "action name");
	if(driver.explicitWaitforVisibility(promotionObject.actionname, "action name"))
	{
		if(driver.enterText(promotionObject.actionname, ExcelReader.getData("Promotion", "actionname"), "action name"))
			pass("Entered action name "+ExcelReader.getData("Promotion", "actionname"));
		
	}
	else
		failAndStop("Action name field not diaplay");
	
}
public void enterActionDescription()
{
	/*if(driver.explicitWaitforVisibility(promotionObject.actiondescription, "action description"))
	{
		if(driver.enterText(promotionObject.actiondescription, ExcelReader.getData("Promotion", "actiondescription"), "action description"))
			pass("Entered action description "+ExcelReader.getData("Promotion", "actiondescription"));
		
	}
	else
		failAndStop("action description field not diaplay");*/
	
}
public void clickActionClassName()
{
		if(driver.clickElement(promotionObject.actionClassName, "action Class Name"))
			pass("action Class Name clicked ");
		else
			failAndStop("action Class Name not display");
}
public void selectActionClassName()
{
	if(waitUntilLoadingCompleteAndClick())
	{
		if(driver.explicitWaitforVisibility(promotionObject.actionClassName, "action Class Name"))
		{
			pass("action Class Name display");
			if(driver.jsClickElement(By.xpath("(//*[text()='Action Type']/preceding::ul[1]//li)//*[text()='"+ExcelReader.getData("Promotion", "actionclassname")+"']"), "action Class Name"))
				pass("action Class Name clicked "+ExcelReader.getData("Promotion", "actionclassname"));
			else
				failAndStop(ExcelReader.getData("Promotion", "actionclassname") + " Not Available");
		}
		else
			failAndStop("Action Class Not Available");
	}
}

public void enterOfferValue()
{
	driver.scrollToElement(promotionObject.offerValue, "offerValue");
	if(driver.explicitWaitforVisibility(promotionObject.offerValue, "offer Value"))
	{
		if(driver.enterText(promotionObject.offerValue, ExcelReader.getData("Promotion", "offerValue"), "offer Value"))
			pass("Entered offer Value "+ExcelReader.getData("Promotion", "offerValue"));
		
	}
	else
		failAndStop("offer Value field not diaplay");
	
}
int changefield=1;
public void changeCondition()
{
	if(driver.jsClickElement(By.xpath("(//*[@class='rules-group-body']//ul)["+ExcelReader.getData("Promotion", "changefield"+changefield+"")+"]/preceding::input[1]"), ""))
		pass("Element clicked");
	else
		failAndStop("field not display");
	
	if(driver.jsClickElement(By.xpath("(//*[@class='rules-group-body']//ul)["+ExcelReader.getData("Promotion", "changefield"+changefield+"")+"]/li//*[text()='"+ExcelReader.getData("Promotion", "changevalue"+changefield+"")+"']"),  "rules group"))
	{
		pass("Rule name "+ExcelReader.getData("Promotion", "changefield"+changefield+"")+" clicked ");
		changefield=changefield+1;
	}
	else
		failAndStop("This field not display ");
}
public void clickStopFurtherProcessing()
{
	if(driver.explicitWaitforVisibility(promotionObject.stopfurtherprocessing, "stop further processing"))
	{
		pass("stop further processing field display");
		if(driver.jsClickElement(promotionObject.stopfurtherprocessing, "stopfurtherprocessing"))
			pass("stop further processing clicked");
	}
	else
		failAndStop("stop further processing not display ");
	
}
public void selectStopFurtherProcessing1()
{
	if(driver.explicitWaitforVisibility(promotionObject.stopfurtherprocessing, "stop further processing"))
	{
		pass("stop further processing field display");
		if(driver.clickElement(By.xpath("(//*[text()='Stop Further Processing']/preceding::ul[1]/li)//*[text()='"+ExcelReader.getData("Promotion", "stopfurther")+"']"), "stopfurtherprocessing"))
			pass("stop further processing clicked");
	}
	else
		failAndStop("stop further processing not display ");
	
}

public void clickPromoCodeTab()
{
		
		if(driver.jsClickElement(promotionObject.promotionPromoCodes, "Promocode"))
			pass("Promocode tab clicked ");
	else
		failAndStop("promocode tab not display");
}

public void clickPromoCodeEditButton()
{
		
		if(driver.jsClickElement(promotionObject.promocodeEditButton, "Promocode Edit"))
			pass("Promocode Edit button is clicked ");
	else
		failAndStop("promocode edit button not display");
}

public void clickPromocodeType()
{
	driver.scrollToElement(By.xpath("//*[@data-qa='promocodetype-list']"), "");
	if(driver.explicitWaitforVisibility(promotionObject.promocodeType, "Promocode Type"))
	{
		pass("Promocode type is displayed");
		if(driver.jsClickElement(promotionObject.promocodeType, "Promocode Type"))
			pass("Promocode type is clicked");
	}
	else
		failAndStop("Promocode type is not displayed");	
}

String PromocodeType="";
public void  selectPromocodeType()
{
	if(driver.explicitWaitforClickable(promotionObject.promocodeType, "promocode Type"))
	{
		PromocodeType = ExcelReader.getData("Promotion","PromoCodeType");
		if(driver.jsClickElement(By.xpath("(//*[@data-qa='promocodetype-list']//li)/span[contains(text(),'"+PromocodeType+"')][1]"), PromocodeType))
			pass("select "+PromocodeType);
		else
			failAndStop("Not selectable");
	}
	
}

public void clickPromocodeList()
{
	driver.scrollToElement(By.xpath("//*[@data-qa='promoCodeFromList-list']"), "");
	if(driver.explicitWaitforVisibility(promotionObject.promocodeList, "Promocode List"))
	{
		pass("Promocode list is displayed");
		if(driver.jsClickElement(promotionObject.promocodeList, "Promocode List"))
			pass("Promocode list is clicked");
	}
	else
		failAndStop("Promocode list is not displayed");	
}

public void  selectPromocodeList()
{
	if(driver.explicitWaitforClickable(promotionObject.promocodeList, "promocode List"))
	{		
		if(driver.jsClickElement(By.xpath("(//*[@data-qa='promoCodeFromList-list']//li)/span[contains(text(),'"+PromocodeListName+"')][1]"), "Promocode List"))
			pass("select "+PromocodeListName);
		else
			failAndStop("Not selectable");
	}
	
}

String OverallUsageCount="";
public void enterOverallUsageCount()
{
	if(driver.explicitWaitforVisibility(promotionObject.promocodemaxusage, "Max Usage Count"))
	{
		OverallUsageCount=ExcelReader.getData("Promotion", "MaxUsageCount");
		if(driver.enterText(promotionObject.promocodemaxusage, OverallUsageCount, "Max Usage Count"))
			pass("Max Usage Count entered");
	}
	else
		failAndStop("Max usage field not display");
}

String NoOfDays="";
public void enterNoOfDays()
{
	if(driver.explicitWaitforVisibility(promotionObject.promocodeNoOfDays, "No Of Days"))
	{
		NoOfDays=ExcelReader.getData("Promotion", "NoOfDays");
		if(driver.enterText(promotionObject.promocodeNoOfDays, NoOfDays, "No Of Days"))
			pass("No Of days entered");
	}
	else
		failAndStop("No Of days field not display");
}
public void clickPromocodeSave()
{
   /* if(driver.jsClickElement(promotionObject.promocodeSave,"Promocode Save"))	
    	pass(" Promocode save is clicked ");
    else
    	failAndStop(" Promocode save not display ");*/


}


public void clickCreatePromocodeList()
{
	if(driver.explicitWaitforVisibility(promotionObject.createPromocodeList, "create Promocode List"))
	{
		pass("create Promocode List display");
		if(driver.jsClickElement(promotionObject.createPromocodeList, "create Promocode List"))
			pass("create Promocode List is clicked");
	}
	else
		failAndStop("create Promocode List is not displayed");
}


String PromocodeListName="";
public void enterPromocodeListName()
{
	if(driver.explicitWaitforVisibility(promotionObject.PromocodeListName, "Promocode List Name"))
	{
		PromocodeListName=ExcelReader.getData("Promotion", "PromocodeListName")+" "+driver.generateRandomNumber(10000);
		if(driver.enterText(promotionObject.PromocodeListName, PromocodeListName, "Promocode List Name"))
			pass("Promocode List Name "+PromocodeListName+" Entered");
	}
	else
		failAndStop("Promocode List name field not display");
}
public void clickAddPromocode()
{
	if(driver.explicitWaitforClickable(promotionObject.AddPromocode, "AddPromocode Radio Button"))
	{
		if(driver.jsClickElement(promotionObject.AddPromocode, "Add Promocode Radio Button"))
			pass("AddPromocodeRadio click");
	}
	
}
String NewDisplayCode="";
public void enterDisplayCode()
{
	if(driver.explicitWaitforVisibility(promotionObject.DisplayCode, "Display Code"))
	{	
		NewDisplayCode=ExcelReader.getData("Promotion", "DisplayCode")+" "+driver.generateRandomNumber(10000);
		if(driver.enterText(promotionObject.DisplayCode, NewDisplayCode, "Display Code"))
			pass("New Display Code  "+NewDisplayCode+" Entered");
	}
	else
		failAndStop("new display code field not display");
}


public void clickCreatenewproject()
{
	if(driver.explicitWaitforVisibility(promotionObject.createnewproject, "create new project"))
	{
		pass("create new project display");
		if(driver.jsClickElement(promotionObject.createnewproject, "createnewproject"))
			pass("create new project clicked");
	}
	else
		failAndStop("create new project not display");
}


String NewProjectName="";
public void enterNewProjectName()
{
	if(driver.explicitWaitforVisibility(promotionObject.newprojectname, "new project name"))
	{
		NewProjectName=ExcelReader.getData("Promotion", "projectname")+" "+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(10000);
		if(driver.enterText(promotionObject.newprojectname, NewProjectName, "new project name"))
			pass("New Project name  "+NewProjectName+" Entered");
	}
	else
		failAndStop("new project name field not display");
}

public void clickPromocodeListPancakeMenu()
{
	if(driver.explicitWaitforVisibility(promotionObject.promocodelistpancakemenu, "promocode list pancake menu"))
	{
		pass("promocode list pancake menu");
		if(driver.jsClickElement(promotionObject.promocodelistpancakemenu, "promocode list pancake menu"))
			pass("Promocode List Pancake menu is clicked");
	}
	else
		failAndStop("Promocode List Pancake menu is not clicked");
}

/*
 * Deprecated and renamed the method from clickPromotionsTab due to chrome driver Click random issue
 * Added the same method with picking href URL and navigate to the same  
 */

@Deprecated
public void clickPromotionsTabold()
{
	String PromotionTabURL = driver.getElementAttribute(promotionObject.promotionstab, "href", "Promotions tab");
	if(driver.scrollToElement(promotionObject.header,"Header Tab") && 
			driver.explicitWaitforClickable(promotionObject.promotionstab, "Promotions tab"))
	{
		pass("Promotions tab clickable");
//		driver.navigateToUrl(PromotionTabURL);
		boolean timeout = false;
		long currtime = System.currentTimeMillis();
		do
		{
			if(System.currentTimeMillis() - currtime > 60000) {
				timeout = true;
				failAndStop("Promotions tab is not clicked");
			}
			if(driver.jsClickElement(promotionObject.promotionstab, "Promotions tab"))
				pass("Promotions tab is clicked");
		}while(!driver.getCurrentUrl().equalsIgnoreCase(PromotionTabURL) || timeout);
		if(timeout)
			failAndStop("Promotions tab is not clicked");
	}
	else
		failAndStop("Promotions tab is not clickable");
}

public void clickPromotionsTab()
{
	driver.isElementDisplayed(promotionObject.promotionListSaveWaiting, "Waiting for page load");
	String PromotionTabURL = properties.getProperty("ApplicationUrl");
	driver.navigateToUrl(PromotionTabURL);
	if(driver.getCurrentUrl().equalsIgnoreCase(PromotionTabURL))
		pass("Navigated to Promotions tab");
	else
		failAndStop("Couldn't Navigate to Promotions tab");
}

ApiReaderUtils Api=new ApiReaderUtils();
public void verifyPromotionId() throws JSONException
{
	
	String promotionId = driver.getText(By.xpath("//*[contains(text(),'Promotion ID')]']"),"promotionId");
	String[] promotionsplit=promotionId.split("#");
	String PromotionIdFinal=promotionsplit[1]+"_0";
	
	String resp = new String();
    List<String> res = new ArrayList<String>();
    res=    Api.ReadAllDataFromUrlserviceput("https://euat.skavaone.com/promotionservice/offers/apply","{ \"items\": { \"sku\": [ { \"itemId\": 1, \"id\": 2, \"productId\": \"SECOM-16720\", \"categoryIds\": [ \"t-shirt\" ], \"unitPrice\": 1000, \"brand\": \"Nike\", \"quantity\": 1, \"personalizations\": [ { \"type\": \"product\", \"price\": 100 }, { \"type\": \"carving\", \"price\": 600 } ] } ] }, \"ruleType\": [ \"item\" ]}","","1");
    resp = res.get(0);

    JSONObject root = new JSONObject(resp);
    String arrayId= "";
    arrayId = root.getJSONArray("sku").getJSONObject(0).getJSONArray("personalizations").getJSONObject(0).getJSONArray("offers").getJSONObject(0).getString("offerId");
    
    
    if(PromotionIdFinal.contentEquals(arrayId))
    {
    	pass("Offer Applied");
        	
    }
    else
    {
    	failAndStop("Offer not Applied");
    }
	
}

public void clickCreateProject()
{
	if(driver.explicitWaitforVisibility(promotionObject.createprojectBtn, "create project"))
	{
		pass("create project button display");
		if(driver.jsClickElement(promotionObject.createprojectBtn, "create project"))
			pass("create project clicked");
	}
	else
		failAndStop("create project button not display");
}

public void enterCreateProject()
{
	if(driver.explicitWaitforVisibility(By.xpath("//*[text()='"+NewProjectName+"']"), "Create new project name"))
	{
		pass("Create new project name "+NewProjectName+" display");
		if(driver.jsClickElement(By.xpath("//*[text()='"+NewProjectName+"']/following::a[1]"), "Create new project name"))
			pass("Create new project name "+NewProjectName+" Entered");
	}
	else
		failAndStop("Create new project name not display");
}

public void clickProject()
{
	driver.scrollToElement(promotionObject.header,"Header Tab");
	if(driver.explicitWaitforVisibility(promotionObject.projectnamelabel, "projectnamelabel"))
	{
		pass("project name label display");
		if(driver.jsClickElement(promotionObject.projectnamelabel, "Click projectnamelabel"))
			pass("project name label clicked");
	}
	else
		failAndStop("project name not display");
	
}

public void clickPromocodelist()
{
	driver.scrollToElement(By.xpath("//*[@data-qa='servicename-label']"), "");
	if(driver.explicitWaitforVisibility(promotionObject.promocodelistmenu, "promocodelistlabel"))
	{
		pass("project name label display");
		if(driver.jsClickElement(promotionObject.promocodelistmenu, "Click promocodelistlabel"))
			pass("promocode list label clicked");
	}
	else
		failAndStop("promocode list not display");
	
}


public void enterDetailPage()
{
	if(driver.explicitWaitforVisibility(promotionObject.chevron_right, "chevron right"))
	{
		pass("project chevron right display");
		if(driver.clickElement(promotionObject.chevron_right, "Click chevron right"))
			pass("project chevron right click");
	}
	else
		failAndStop("project chevron right not display");
	
}

public void clickSubmit() throws InterruptedException
{
	if(driver.explicitWaitforVisibility(promotionObject.submitProject, "Submit Project"))
	{
		driver.scrollToElement(promotionObject.header, "header");
		pass("submit Project button display");
		if(driver.jsClickElement(promotionObject.submitProject, "Click submit Project"))
			pass("submit Project clicked");
		else
			fail("Submit not clickable");
		toastMeassgeCapture();
	/*String toastmessage=driver.getText(By.xpath("//*[@class='toast-message']"), "toast-message");
	if(toastmessage.contains("Error"))
		failAndStop("submit throw error "+toastmessage);
	else
		pass("submit successfully"+toastmessage); */
	}
	else
		failAndStop("submit Project button not display");	

}
public void enterCurrentPage()
{
	if(driver.explicitWaitforVisibility(promotionObject.projectdetails, "project details"))
	{
		pass("project details display");
		if(driver.clickElement(promotionObject.projectdetails, "Click project details"))
			pass("project details clicked");
		
	}
	else
		failAndStop("project details not display");
	
}

public void clickApprove()
{
	if(driver.explicitWaitforVisibility(promotionObject.approvedProject, "approved Project"))
	{
		pass("approved Project display");
		if(driver.jsClickElement(promotionObject.approvedProject, "Click approved Project"))
			toastMeassgeCapture();
			pass("approved Project clicked");
		
		/*String toastmessage=driver.getText(By.xpath("//*[@class='toast-message']"), "toast-message");
		if(toastmessage.contains("Error"))
			failAndStop("Approve throw: "+toastmessage);
		else
			pass("Approve successfully"+toastmessage);*/
		
		if(driver.explicitWaitforInVisibility(promotionObject.approvedProject, ""))
			pass("Action completed  reload home page");
	}
	else
		failAndStop("approved Project not display");
	
}

public void clickReject()
{
	if(driver.explicitWaitforClickable(promotionObject.deny, "Reject Project"))
	{
		pass("Reject Project display");
		if(driver.jsClickElement(promotionObject.deny, "Click Reject Project"))
			pass("Reject Project clicked");
		
		if(driver.explicitWaitforInVisibility(promotionObject.deny, ""))
			pass("Action completed  reload home page");
	}
	else
		failAndStop("Reject Project not display");
	
}

public void clickReopen()
{
	if(driver.explicitWaitforVisibility(promotionObject.reopen, "Reopen Project"))
	{
		pass("Reopen Project display");
		if(driver.jsClickElement(promotionObject.reopen, "Click Reopen Project"))
			pass("Reopen Project clicked");
		if(driver.explicitWaitforInVisibility(promotionObject.reopen, ""))
			pass("Action completed  reload home page");
	}
	else
		failAndStop("Reopen Project not display");
	
}


public void chevronEnterAgain()
{
	if(driver.explicitWaitforVisibility(By.xpath("//*[text()='"+NewProjectName+"']"), "Create new project name"))
	{
		pass("Create new project name "+NewProjectName+" display");
		if(driver.jsClickElement(By.xpath("(//*[text()='"+NewProjectName+"']/following::a[2])"), "Create new project name"))
			pass("Create new project name "+NewProjectName+" Entered");
	}
	else
		failAndStop("Create new project name not display");
}

public void chevronEnterCurrentPage()
{
	if(driver.explicitWaitforVisibility(By.xpath("//*[text()='"+NewProjectName+"']"), "Create new project name"))
	{
		pass("Create new project name "+NewProjectName+" display");
		if(driver.jsClickElement(By.xpath("//*[text()='"+NewProjectName+"']/following::a[1]"), "Create new project name"))
			pass("Create new project name "+NewProjectName+" Entered");
	}
	else
		failAndStop("Create new project name not display");
}


public void projectDetailsEdit()
{
	if(driver.explicitWaitforVisibility(promotionObject.projectdetailsedit, "projectdetailsedit"))
	{
		pass("projectdetails edit button display");
		if(driver.jsClickElement(promotionObject.projectdetailsedit, "projectdetailsedit"))
			pass("projectdetailsedit clicked");
		
	}
	else
		failAndStop("projectdetails edit not dispaly");
	
}
public void enterEditDescription() throws InterruptedException
{
	if(driver.explicitWaitforVisibility(promotionObject.description_input, "description input"))
	{
		pass("description_input field display");
		if(driver.enterText(promotionObject.description_input, "updated description", "description_input"))
			pass("description_input entered");
	}
	else
		failAndStop("description_input not display");
	Thread.sleep(10000);
}

public void enterAddNote()
{/*
	if(driver.explicitWaitforVisibility(promotionObject.addnote, "addnote input"))
	{
		pass("addnote field display");
		if(driver.jsClickElement(promotionObject.addnote, "add note"))
			pass("add notes clicked");
		else
			failAndStop("add notes not clicked");
		
		if(driver.explicitWaitforVisibility(promotionObject.addnotevalue, "addnotevalue"))
			pass("Add note field display");
		
		driver.scrollToElement(promotionObject.addnotevalue, "addnotevalue");
		if(driver.enterText(promotionObject.addnotevalue, "updated addnote", "addnote"))
			pass("addnote entered");
		else
			failAndStop("add note field not dispaly");
		
		if(driver.jsClickElement(promotionObject.addnotes, "Click add notes"))
			pass("add notes button clicked");
		else
			failAndStop("add notes button not clickable");
		
	}
	else
		failAndStop("addnote not display");
*/}
public void projectDetailSave()
{
	if(driver.explicitWaitforVisibility(promotionObject.projectdetailsave, "projectdetail save"))
	{
		pass("projectdetail save button display");
		if(driver.jsClickElement(promotionObject.projectdetailsave, "projectdetailsave"))
			pass("projectdetail save clicked");
		
	}
	else
		failAndStop("projectdetail save not dispaly");
	
}


//*[text()='IntTest Proj3 5283']/following::a[1]
public void logout() throws InterruptedException
{
	driver.scrollToElement(promotionObject.HeaderHi, "Hi drop down");
	if(driver.explicitWaitforVisibility(promotionObject.HeaderHi, "Hi drop down"))
	{
		pass("Hi drop down display");
		if(driver.jsClickElement(promotionObject.HeaderHi, "Hi drop down"))
			pass("hi drop down clicked");
		else
			fail("hi drop down not clicked");
	}
	else
		failAndStop("hi drop down not display");
	
	
	if(driver.explicitWaitforVisibility(promotionObject.logout, "logout"))
	{
		pass("logout button display");
		if(driver.jsClickElement(promotionObject.logout, "Click logout"))
			pass("logout button clicked");
	}
	else
	{
		driver.jsClickElement(promotionObject.HeaderHi, "Hi drop down");
		driver.explicitWaitforInVisibility(promotionObject.logout, "logout");
		if(driver.jsClickElement(promotionObject.logout, "Click logout"))
			pass("logout button clicked");
		else
			fail("logout not display");
	}
		

		
	
}



public void promotionEditEnterValue()
{
    try {
    if(driver.explicitWaitforVisibility(promotionObject.promtionMessage, "promotion edit button"))
    {
        driver.enterText(promotionObject.promtionMessage,ExcelReader.getData("Promotion", "promtionMessage") , "promtionMessage");
        driver.enterText(promotionObject.promtionDescription,ExcelReader.getData("Promotion", "promtionDescription") , "promtionDescription");
        driver.clickElement(promotionObject.promtionStatusSelect , "promtionStatusSelect");
        driver.clickElement(promotionObject.pmInactiveSelect , "pmInactiveSelect");
    }
    else
        failAndStop("promtion Message not displayed");
    }catch (Exception e) {
        // TODO: handle exception
    }
}



public void promo_click() throws InterruptedException
{
	driver.scrollToElement(promotionObject.promo_click, "promo_click");
	if(driver.jsClickElement(promotionObject.promo_click, "Actions click"))
	{
		pass("Promotions is clicked");
	}
	else
	{
		failAndStop("Promotions is not clicked");
	}
}

	public void promo_delete()
	{
		driver.explicitWaitforVisibility(promotionObject.action_click, "Actions click");
		if(driver.clickElement(promotionObject.action_click, "Actions click"))
		{
			pass("Actions is clicked");
		}
		else
		{
			failAndStop("Actions is not clicked");
		}
		driver.explicitWaitforVisibility(promotionObject.delete_promotion, "Delete promotion");
		if(driver.clickElement(promotionObject.delete_promotion, "Delete promotion"))
		{
			pass("Promotion deleted successfully");
		}
		else
		{
			failAndStop("Promotion not deleted");
		}	
	}
	
	
	public void chevronEnterCreateProject()
	{
	    if(driver.explicitWaitforVisibility(By.xpath("//*[text()='"+NewProjectName+"']"), "Create new project name"))
	    {
	        pass("Create new project name "+NewProjectName+" display");
	        if(driver.jsClickElement(By.xpath("(//*[text()='"+NewProjectName+"']/following::a[2])[2]"), "Create new project name"))
	            pass("Create new project name "+NewProjectName+" Entered");
	    }
	    else
	        failAndStop("Create new project name not display");
	}
	
	

	
	
	

public void promotionEdit()
{
	try {
	if(driver.explicitWaitforVisibility(promotionObject.promotionEditButton, "promotion edit button"))
	{
		pass("stop further processing field display");
		if(driver.clickElement(promotionObject.promotionEditButton, "promotion edit button"))
			pass("stop further processing clicked");
	}
	else
		failAndStop("stop further processing not display ");
	}catch (Exception e) {
		// TODO: handle exception
	}
}



public void promotionEditAndSave()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionEditSave, "promotionEditSave"))
	{
		pass("stop further processing field display");
		if(driver.clickElement(promotionObject.promotionEditSave, "promotionEditSave"))
			pass("Promotion edit and saved");
	}
	else
		failAndStop("Promotion updation failed");
	
}



public void submit_notes()
{
	try 
	{
		driver.explicitWaitforVisibility(promotionObject.submit_notes, "submit_notes");
		if(driver.clickElement(promotionObject.submit_notes, "submit_notes"))
		{
			pass("Add notes is clicked");
		}
		else
		{
			failAndStop("Add notes is not clicked");
		}
		driver.enterText(promotionObject.submit_notes, ExcelReader.getData("Promotion", "SubmitNotes"), "Submit notes");
	 }
     catch (Exception e) 
	{
            // TODO: handle exception
    }
}
public void buyItemQuantity()
{
	driver.scrollToElement(promotionObject.buyitemquantity, "buyitemquantity");
	if(driver.enterText(promotionObject.buyitemquantity, ExcelReader.getData("Promotion", "BIQvalue"), "buyitemquantity"))
	{
		pass("Entered buy item quantity "+ExcelReader.getData("Promotion", "BIQvalue"));
	}
	else
		failAndStop("buy item quantity not display ");
}
public void getItemQuantity()
{
	driver.scrollToElement(promotionObject.getitemquantity, "getitemquantity");
	if(driver.enterText(promotionObject.getitemquantity, ExcelReader.getData("Promotion", "GIQvalue"), "getitemquantity"))
	{
		pass("Entered get item quantity "+ExcelReader.getData("Promotion", "GIQvalue"));
	}
	else
		failAndStop("get item quantity not display ");
}
public void enterGiftProductId()
{
	driver.scrollToElement(promotionObject.giftproductid, "giftproductid");
	if(driver.enterText(promotionObject.giftproductid, ExcelReader.getData("Promotion", "actionvalue"+actioncount+""), "giftproductid"))
	{
		pass("Entered gift product id "+ExcelReader.getData("Promotion", "actionvalue"+actioncount+""));
		actioncount=actioncount+1;
	}
	else
		failAndStop("gift product id not display ");
}
public void enterGiftSkuId()
{
	driver.scrollToElement(promotionObject.giftskuid, "giftproductid");
	if(driver.enterText(promotionObject.giftskuid, ExcelReader.getData("Promotion", "actionvalue"+actioncount+""), "giftskuid"))
	{
		pass("Entered gift sku id "+ExcelReader.getData("Promotion", "actionvalue"+actioncount+""));
		actioncount=actioncount+1;
	}
	else
		failAndStop("gift sku id not display ");
}
public void buyXItemQuantity()
{
	driver.scrollToElement(promotionObject.buyXItemQuantity, "buyXItemQuantity");
	if(driver.enterText(promotionObject.buyXItemQuantity, ExcelReader.getData("Promotion", "BIQvalue"), "buyXItemQuantity"))
	{
		pass("Entered buy X item quantity "+ExcelReader.getData("Promotion", "BIQvalue"));
	}
	else
		failAndStop("buy X item quantity not display ");
}
public void buyYItemQuantity()
{
	driver.scrollToElement(promotionObject.buyYItemQuantity, "buyYItemQuantity");
	if(driver.enterText(promotionObject.buyYItemQuantity, ExcelReader.getData("Promotion", "GIQvalue"), "buyYItemQuantity"))
	{
		pass("Entered buy Y item quantity "+ExcelReader.getData("Promotion", "GIQvalue"));
	}
	else
		failAndStop("buy Y item quantity not display ");
}

//Promotion List

public void clickListTab()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionList, "List tab"))
	{
		pass("List tab displayed");
		if(driver.jsClickElement(promotionObject.promotionList, "Promotions tab"))
			pass("List tab is clicked");
		else
			fail("List tab not clicked");
	}
	else
		failAndStop("List tab is not displayed");
}

public void clickCreateListBtn()
{
	if(driver.explicitWaitforVisibility(promotionObject.createListButton, "Create List Button"))
	{
		pass("Create list button displayed");
		if(driver.jsClickElement(promotionObject.createListButton, "Create List Button"))
			pass("Create List button clicked");
		else
			fail("Create List button not clicked");
	}
	else
		failAndStop("Create List Button not displayed");
		
}


String ListName="";
public void enterListName()
{
	if(driver.explicitWaitforVisibility(promotionObject.newListName, "List Name"))
	{
		ListName=ExcelReader.getData("Promotion", "listname")+" "+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(10000);
		if(driver.enterText(promotionObject.newListName, ListName, "List Name"))
			pass("List Name "+ListName+" Entered");
	}
	else
		failAndStop("List name field not display");
}


public void clickListStatus()
{
	if(driver.explicitWaitforVisibility(promotionObject.listStatus, "Status"))
	{
		if(driver.jsClickElement(By.xpath("//*[@data-qa='promotionliststatus-list']//input"), "Status"))
			pass("List Status field click");
	}
	else
		fail("list status field not display");
}

public void  selectListStatus()
{
	if(waitUntilLoadingCompleteAndClick())
	{
		if(driver.explicitWaitforClickable(promotionObject.listStatus, "Status list"))
		{
			driver.jsClickElement(By.xpath("//*[@data-qa='promotionliststatus-list']//input"), "click JS");
			int size=driver.getSize(By.xpath("//*[@data-qa='promotionliststatus-list']//ul//li"), "Status list");
			int random=driver.generateRandomNumber(size)+1;
			String txt=driver.getText(By.xpath("(//*[@data-qa='promotionliststatus-list']//ul//li)["+random+"]"), "Str");
			if(driver.jsClickElement(By.xpath("(//*[@data-qa='promotionliststatus-list']//ul//li)["+random+"]"), "Status list"))
				pass("select "+txt);
			else
				failAndStop("Not selectable");
		}
	}
	
}

public void displayListStatusActive()
{
	if(driver.explicitWaitforClickable(promotionObject.listStatus, "Status list"))
	{
		int size=driver.getSize(By.xpath("//*[@data-qa='promotionliststatus-list']//ul//li"), "Status list");
		//int random=driver.generateRandomNumber(size)+1;
		for(int i=1;i<=size;i++)
		{
			String txt=driver.getText(By.xpath("(//*[@data-qa='promotionliststatus-list']//ul//li)["+i+"]"), "Str");
			if(txt.equalsIgnoreCase("Active"))
				pass("Active status is displayed");
			else 
				failAndStop("Active status is not displayed");			
		}
	}
}

public void displayListStatusInActive()
{
	if(driver.explicitWaitforClickable(promotionObject.listStatus, "Status list"))
	{
		int size=driver.getSize(By.xpath("//*[@data-qa='promotionliststatus-list']//ul//li"), "Status list");
		//int random=driver.generateRandomNumber(size)+1;
		for(int i=1;i<=size;i++)
		{
			String txt=driver.getText(By.xpath("(//*[@data-qa='promotionliststatus-list']//ul//li)["+i+"]"), "Str");
			if(txt.equalsIgnoreCase("InActive"))
				pass("Active status is displayed");
			else 
				failAndStop("Active status is not displayed");			
		}
	}
}

public void selectListStatusActive()
{
	if(waitUntilLoadingCompleteAndClick())
	{
		if(driver.explicitWaitforClickable(promotionObject.listStatus, "Status list"))
		{
			String txt=driver.getText(promotionObject.Liststatusactive, "statusactive");
			if(txt.equalsIgnoreCase("Active"))
			{
				if(driver.clickElement(promotionObject.Liststatusactive, "Status list"))
					pass("select "+txt);
				else
					failAndStop("Not selectable");						
			}
		}
	}
}
public void selectListStatusInactive()
{
	if(waitUntilLoadingCompleteAndClick())
	{			
		if(driver.explicitWaitforClickable(promotionObject.listStatus, "Status list"))
		{
			int size=driver.getSize(By.xpath("//*[@data-qa='promotionliststatus-list']//ul//li"), "Status list");
			//int random=driver.generateRandomNumber(size)+1;
			for(int i=1;i<=size;i++)
			{
				String txt=driver.getText(By.xpath("(//*[@data-qa='promotionliststatus-list']//ul//li)["+i+"]"), "Str");
				if(txt.equalsIgnoreCase("InActive"))
				{
					if(driver.jsClickElement(By.xpath("(//*[@data-qa='promotionliststatus-list']//ul//li)["+i+"]"), "Status list"))
						pass("select "+txt);
					else
						failAndStop("Not selectable");
					break;
				}		
			}
		}
	}
}

public void clickListType()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionlisttype, "promotion list type"))
	{
		if(driver.clickElement(promotionObject.promotionlisttype, "promotion list type"))
			pass("promotion list type click");
		else
			fail("promotion list type not clicked");
	}
	else
		fail("promotion list type not display");
	
}
public void  selectListType()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionlisttype, "promotion list type"))
	{
		
		String txt=driver.getText(By.xpath("//*[@data-qa='listtype-list']//ul/li//*[text()='"+ExcelReader.getData("Promotion", "listtype")+"']"), "promotion list type");
		if(driver.jsClickElement(By.xpath("//*[@data-qa='listtype-list']//ul/li//*[text()='"+ExcelReader.getData("Promotion", "listtype")+"']"), "promotion list type"))
			pass("select "+txt);
		else
			failAndStop(" List Type Not selectable");
	}
}

public void clickFieldName()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionfieldname, "promotion field name"))
	{
		if(driver.clickElement(promotionObject.promotionfieldname, "promotion field name"))
			pass("promotion field name click");
		else
			fail("promotion field name not clicked");
	}
	else
		fail("promotion field name not display");
	
}
public void  selectFieldName()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotionfieldname, "promotion field name"))
	{
		
		String txt=driver.getText(By.xpath("//*[@data-qa='fieldname-list']//ul/li//*[text()='"+ExcelReader.getData("Promotion", "fieldname")+"']"), "promotion field name");
		if(driver.jsClickElement(By.xpath("//*[@data-qa='fieldname-list']//ul/li//*[text()='"+ExcelReader.getData("Promotion", "fieldname")+"']"), "promotion field name"))
			pass("select "+txt);
		else
			failAndStop(" field name not selectable");
	}
}

public void clickCloneBtn()
{
	if(driver.explicitWaitforClickable(promotionObject.promotionCloneBtn, "Promotion Clone Button"))
	{
		if(driver.jsClickElement(promotionObject.promotionCloneBtn, "Promotion Clone Btn"))
			pass("Promotion Clone Button Clicked");
		else
			fail("Promotion clone button not clicked");
	}
	else
		failAndStop("Promotion Clone button not display");

}

public void clickPromotionGroupList() throws InterruptedException
{
	if (driver.explicitWaitforClickable(promotionObject.promotiongrouplist, "Promotion group list trigger"))
	{
		Thread.sleep(1000);
		if(driver.actionClickElement(promotionObject.promotiongrouplist, "Promotion group list page"))
			pass("Promotion group list item clicked");
		else
			failAndStop("Promotion group list trigger not clicked");
				
	}
	else
		fail("promotion group list not display");

}

public void selectPromotionGroupList()
{
	if(driver.explicitWaitforVisibility(promotionObject.promotiongrouplist, "Promotion group list trigger"))
	{
		String txt = driver.getText(By.xpath("//*[@data-qa='promotiongroup-list']//ul//li//*[text()='"+ExcelReader.getData("Promotion", "promotiongrouplist")+"']"),"Promotion group list trigger");
		if(driver.jsClickElement(By.xpath("//*[@data-qa='promotiongroup-list']//ul//li//*[text()='"+ExcelReader.getData("Promotion","promotiongrouplist")+"']"),"Promotion group list trigger"))
			pass("Select "+txt);
		else
			failAndStop("Promotion Group List not selected");
		
	}
	else
		failAndStop("Promotion group list cannot be selectable");


}

public void clickPromotionListChevronButton()
{
	if(driver.explicitWaitforClickable(promotionObject.promotionlistchevron, "Promotion List chevron"))
	{
		if(driver.jsClickElement(promotionObject.promotionlistchevron, "Promotion list name"))
			pass("Promotion list chevron clicked");
		else
			failAndStop("Promotion list chevron not clicked");
	}
	else
		failAndStop("Promotion list chevron not present");

}

public void clickPromotionAddListItemButton()
{
	if(driver.explicitWaitforClickable(promotionObject.promotionAddListItemBtn, "Promotion Add List Item"))
	{
		if(driver.jsClickElement(promotionObject.promotionAddListItemBtn, "Promotion add list item"))
			pass("Promotion add list item radio button clicked");
		else
			failAndStop("Promotion add list item radio button clicked");
	}
	else
		failAndStop("Promotion add list item not present");

}

public void enterPromotionListLabel()
{
	if(driver.explicitWaitforClickable(promotionObject.promotionlistItem, "Promotion List Item"))
	{
		pass("Promotion List Item");
	}
	
	String name = ExcelReader.getData("Promotion", "listitem"); 
	int randomname=driver.generateRandomNumber(10000);
	String finalName = name+" "+randomname;
	
	if(driver.enterText(promotionObject.promotionlistItem, finalName, "Promotion List Item"))
	
		pass("Promotion List Item is entered");
	else
		failAndStop("Promotion list item is not entered");

}

public void clickPromotionAddListItemField()
{
	if(driver.explicitWaitforClickable(promotionObject.promotionAddistItemField, "Promotion Add List Item Field")) 
	{
		if(driver.jsClickElement(promotionObject.promotionAddistItemField, "Promotion Add List Item Field"))
			pass("Promotion Add list item clicked");
		else
			failAndStop("Promotion Add List item not clicked");
	}
	else
		failAndStop("Promotion Add list item field not present");

}

public void clickPromotionBreadCrumb()
{
	if(driver.explicitWaitforClickable(promotionObject.promotionBreadCrumb, "Proomotion bread crumb click"))
	{
		if(driver.jsClickElement(promotionObject.promotionBreadCrumb, "Promotion Bread crumb"))
			pass("Promotin bread crumb clicked");
		else
			fail("Promotion bread crumb not clicked");
				
	}
	else
		failAndStop("Promotion Bread Crumb not display");
}

public void clickActionButton()
{
	if(driver.explicitWaitforClickable(promotionObject.actionButton, "Action Button"))
	{
		if(driver.jsClickElement(promotionObject.actionButton, "Action Button"))
			pass("Action button clicked");
		else
			fail("Action button not clicked");
	}
	else
		failAndStop("Action button not display");

}

public void clickActionDeleteButton()
{
	if(driver.explicitWaitforClickable(promotionObject.actiondeleteButton, "action delete button"))
	{
		if(driver.jsClickElement(promotionObject.actiondeleteButton, "Action delete button"))
			pass("Delete button clicked");
		else
			fail("Delete button not clicked");
	}
	else
		failAndStop("Delete button not present");
}

public void clickYesButton()
{
	if(driver.explicitWaitforClickable(promotionObject.deleteYesButton, "Yes bUtton"))
	{
		if(driver.jsClickElement(promotionObject.deleteYesButton, "Yes button"))
			pass("Yes button clicked");
		else
			fail("Yes button not clicked");
		
	}
	else
		failAndStop("Yes button not display");
}

public void clickPromotionEditAndSave()
{
	driver.scrollToElement(promotionObject.promotionEditAndSave,  "Save actions");
		if(driver.jsClickElement(promotionObject.promotionEditAndSave, "Save"))
			pass("Save clicked ");
		else
			failAndStop("save button not display");
		driver.isElementDisplayed(promotionObject.promtionEditBtn, "Promotion Waiting for save");
	
}

public void enterPurchaseQuantityValue()
{
	driver.scrollToElement(promotionObject.purchaseQuantity, "Purchase Quantity Value");
	if(driver.explicitWaitforVisibility(promotionObject.purchaseQuantity, "Purchase Quantity Value"))
	{
		if(driver.enterText(promotionObject.purchaseQuantity, ExcelReader.getData("Promotion", "PurchaseQuantity"), "Purchase Quantity Value"))
			pass("Entered Purchase Quantity Value "+ExcelReader.getData("Promotion", "PurchaseQuantity"));
		
	}
	else
		failAndStop("Purchase Quantity Value field not diaplay");
	
}

public void clickActionAddUpdateSave()
{
	driver.explicitWaitforClickable(promotionObject.actionAddUpdateButton, "promotion actions Add Update");
		if(driver.jsClickElement(promotionObject.actionAddUpdateButton, "promotion actions Add Update"))
			pass("promotion action Add Uppdate clicked ");
	else
		failAndStop("promotion action Add Uppdate not clicked");
}

public void clickLocale()
{
	driver.scrollToElement(promotionObject.localeDropDown, "Locale Drop Down");
	if(driver.explicitWaitforVisibility(promotionObject.localeDropDown, "Locale Drop Down"))
	{
		if(driver.jsClickElement(promotionObject.localeDropDown, "Locale Drop Down"))
			pass("promotion Locale Drop Down field click");
	}	
}

public void  selectLocale()
{
	if(driver.explicitWaitforVisibility(By.xpath("(//*[contains(@class,'dropdown-content select-dropdown active')]//*[contains(@class,'filtrable')])[2]"), "Locale Drop Down Select"))
	{
		driver.explicitWaitforClickable(By.xpath("(//*[contains(@class,'dropdown-content select-dropdown active')]//*[contains(@class,'filtrable')])[2]"), "Locale Drop Down Select");
		String txt=driver.getText(By.xpath("(//*[contains(@class,'dropdown-content select-dropdown active')]//*[contains(@class,'filtrable')])[2]"), "Locale Drop Down Select");
		if(driver.actionClickElement(By.xpath("(//*[contains(@class,'dropdown-content select-dropdown active')]//*[contains(@class,'filtrable')])[2]"), "Locale Drop Down Select"))
			pass("select "+txt);
		else
			failAndStop("Not selectable");
	}
}

public void enterDisplayMessage()
{
	driver.scrollToElement(promotionObject.localeDisplayMessage, "Locale Display Message");
	if(driver.explicitWaitforVisibility(promotionObject.localeDisplayMessage, "Locale Display Message"))
	{
		if(driver.enterText(promotionObject.localeDisplayMessage, ExcelReader.getData("Promotion", "localedisplaymessage"), "Locale Display Message"))
			pass("Entered Locale Display Message "+ExcelReader.getData("Promotion", "localedisplaymessage"));
		
	}
	else
		failAndStop("Locale Display Message field not diaplay");
	
}

public void toastMeassgeCapture() {
	if(driver.explicitWaitforInVisibility(promotionObject.toastMessage, "Toast Message")) {
	/*String toastmessage=driver.getText(promotionObject.toastMessage, "toast-message");
	if(toastmessage.contains("Error"))
		failAndStop("Approve throw: "+toastmessage);
	else*/
		pass("Approve successfully");
	}else {
		logInfo("Toast Message hided");
	}
}

}