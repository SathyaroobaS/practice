package com.skava.reusable.components;

import java.util.Random;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.testng.SkipException;

import com.framework.reporting.BaseClass;
import com.framework.reporting.ExtentTestManager;
import com.github.javafaker.Faker;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.frameworkutils.ExcelReader;
import com.skava.frameworkutils.loggerUtils;
import com.skava.networkutils.ApiReaderUtils;
import com.skava.object.repository.LoginPage;
import com.skava.object.repository.BusinessPageObject;
import com.skava.object.repository.CatalogPageObject;
import com.skava.object.repository.InstanceTeamPage;
import com.skava.object.repository.LogIn_Page;
import com.skava.object.repository.MicroServiceObject;

public class GeneralComponents extends BaseClass
{
	
	public String currentUTCTime = "", deviceType = "", currentOS = "", sheetOS = "", currentPriceListName = "", currentProjectName = "", currentSkuId = "", 
			currentProjectDesc = "", currentCollectionName = "", sessionId = "", currentDomain = "", foundationSessionId = "";
	
	public ApiReaderUtils api = new ApiReaderUtils();
	
	/*
	 * Get current timestamp
	 */
    public long currentTimeStamp()
    {
    	Date date= new Date();
        long TimeStamp = date.getTime();
        return TimeStamp;
    }
    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:ms");
	/**
     * Launch the url based on inputs in config.ini
     */
    public void launchUrl() 
    {
        Map<String, String> browserDetails = driver.getBrowserDetails();
        currentOS = System.getProperty("os.name");
        deviceType = ExcelReader.getData(properties.getProperty("RunConfigSheetName"), "Device_Type");
        sheetOS = ExcelReader.getData(properties.getProperty("RunConfigSheetName"), "OS");
        
        logInfo("Browser Name : " + browserDetails.get("Browser Name") + " Browser Version : " + browserDetails.get("Browser Version")
        	+" Execution Port : " + browserDetails.get("Port") + " The Current OS : " + currentOS);
        //properties.put("ApplicationUrl", properties.getProperty("url"));// ------------ Temporarily  added - Remove before commit --------------
        if (deviceType.equalsIgnoreCase("Mobile")) 
        {
        	//Driver set size method
        	driver.driverSize(432, 812);
        	logInfo("Device Name: " + ExcelReader.getData(properties.getProperty("RunConfigSheetName"), "Device_Name"));
		}
        else if (deviceType.equalsIgnoreCase("Tablet")) 
        {
			//Driver set size method
        	driver.driverSize(786, 1024);
        	logInfo("Device Name: " + ExcelReader.getData(properties.getProperty("RunConfigSheetName"), "Device_Name"));
		}
        else
		{
//        	driver.driverSize(1280, 768);
        	driver.driverSize(1366,768);
        	//driver.driverSize(1920, 1080);
		}
        
        driver.navigateToUrl(properties.getProperty("ApplicationUrl"));
        //driver.navigateToUrl("https://estage-beta.skavaone.com/admin/catalogs/?collectionId=1726&businessId=80&storeId=52");
        
        
        logInfo("Navigated to application url. The entered url was: " + properties.getProperty("ApplicationUrl"));
        driver.explicitWaitforVisibility(LogIn_Page.btn_LogIn,"Login button");
    }
    
    
	public void verifyLoginSuccessful()
	{
		driver.enterText(LoginPage.txtEmailId,ExcelReader.getData("General_Data","Username"),"Email");
		driver.enterTextAndSubmit(LoginPage.txtPassword,ExcelReader.getData("General_Data","Password"),"Password" );
		ExtentTestManager.getTest().log(LogStatus.INFO, "Entered valid credentials");
		if(driver.compareElementText(LoginPage.lblSignInSuccess, ExcelReader.getData("General_Data","logInSuccessMessage"), "Login Success Message"))
			ExtentTestManager.getTest().log(LogStatus.PASS, "User sucessfully logged in");
		else
			ExtentTestManager.getTest().log(LogStatus.FAIL, "User login failed");
	}
	
	public void pass(String message)
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, message +" URL : "+driver.getCurrentUrl());
	}
	
	public void failAndStop(String message)
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, message +" URL : "+driver.getCurrentUrl());
		System.out.println("FAIL ---> "+message);
		if(properties.getProperty("screenshots").equals("true"))
        {
            String scrFile = driver.takeScreenShotReturnFilePath();
            ExtentTestManager.getTest().log(LogStatus.FAIL,ExtentTestManager.getTest().addBase64ScreenShot(scrFile));
        }
		throw new SkipException(message);
	}
	
	public void fail(String message)
	{
		ExtentTestManager.getTest().log(LogStatus.FAIL, message +" URL : "+driver.getCurrentUrl());
		System.out.println("FAIL ---> "+message);
		if(properties.getProperty("screenshots").equals("true"))
        {
            String scrFile = driver.takeScreenShotReturnFilePath();
            ExtentTestManager.getTest().log(LogStatus.FAIL,ExtentTestManager.getTest().addBase64ScreenShot(scrFile));
        }
	}
	
	/*
     * Log the fail into extent report with comments
     *
     * @param text
     */
    public void logFatal(String text) 
    {
        ExtentTestManager.getTest().log(LogStatus.FATAL, text+" URL : "+driver.getCurrentUrl());
    }
    
    /*
     * Log the info into extent report with comments
     *
     * @param text
     */
    public void logInfo(String text) 
    {
        ExtentTestManager.getTest().log(LogStatus.INFO, text+" URL : "+driver.getCurrentUrl());
    }

    /*
     * Log the Warning into Extent report with comments
     *
     * @param text
     */
    public void logWarning(String text) 
    {
        ExtentTestManager.getTest().log(LogStatus.WARNING, text);
    }

    /*
     * Log the fail into extent report with comments
     *
     * @param text
     */
    public void logFail(String text) 
    {
    	String currentUrl = driver.getCurrentUrl();
        logInfo("Current Url: " + currentUrl);
        ExtentTestManager.getTest().log(LogStatus.FAIL, text+" URL : "+driver.getCurrentUrl());
        if (properties.getProperty("TakeScreenshots").equalsIgnoreCase("Yes")) 
        {
            String scrFile = driver.takeScreenShotReturnFilePath();
            ExtentTestManager.getTest().log(LogStatus.FAIL, ExtentTestManager.getTest().addBase64ScreenShot(scrFile));
        }
    }
    
    /*
     * Log the pass into Extent report with comments
     *
     * @param text
     */
    public void logPass(String text) 
    {
    	String currentUrl = driver.getCurrentUrl();
        logInfo("Current Url: " + currentUrl);
        ExtentTestManager.getTest().log(LogStatus.PASS, text);
    }
       

    /*
     * Throw New Skip Exception
     */
    public void skipException(String exception)
    {
    	throw new SkipException(exception);
    }

	/*
     *  Generate Random Strings
     */
    public String generateRandomStrings(int length)
    {
        String SALTCHARS = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) { 
                int index = (int) (rnd.nextFloat() * SALTCHARS.length());
                salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
    
    /*
     *  Verify success message pop-up scenario
     */
    public void VerifySuccessPopAfterCreatingBusiness() 
	{
		if(driver.isElementDisplayed(BusinessPageObject.Business_Success_Popup, "Success Pop-up after creating the business"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Success Pop-up is displayed after creating the business");
			String SuccessMessage = driver.getText(BusinessPageObject.Business_Success_Msg, "Success Message");
			ExtentTestManager.getTest().log(LogStatus.INFO, "Success Message is" +SuccessMessage);
			if(driver.clickElement(BusinessPageObject.Popup_Ok_Btn, "Pop Up OK Button"))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, "Success Pop-up OK button is clicked");
			}
			else
			{
				ExtentTestManager.getTest().log(LogStatus.FAIL, "Success Pop-up OK button is not clicked");
			}
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Success Pop-up is not displayed after creating the business");
		}
	}
    /*
     *    // Select status toggle switch
     */
    public void SelectToggleSwitch() 
   	{
		if(driver.clickElement(MicroServiceObject.Status_Toggle, "Status Toggle is clicked"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Status Toggle is clicked");  
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Status Toggle is not clicked");  
		}
   	}
    
    /*
     *    // Select status toggle switch
     */
    public void ClickCreateButton() 
   	{
    	// Click collection create button
		if(driver.clickElement(MicroServiceObject.Collection_Create_Btn, "Collection Create Button is clicked"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, "Collection Create Button is clicked");  
		}
		else
		{
			ExtentTestManager.getTest().log(LogStatus.FAIL, "Collection Create Button is not clicked");  
		}
   	}
    
    /*
     *   Verify user can able to get business ID from the URL
     */
    public void GetBusinessIdFromURL() 
   	{
    	String currenturl = driver.getCurrentUrl();
    	ExtentTestManager.getTest().log(LogStatus.INFO, "Current Url is : " + currenturl);  
    	String businessidsplit[]=currenturl.split("=");
    	String businessid=businessidsplit[1];
    	data.put("businessid", businessid);
    	ExtentTestManager.getTest().log(LogStatus.INFO, "Created Business Id is : " + businessid);  
    	
   	}
    
    /*
     * Generate Random #
     */
    public String generateRandomAlphabetsWithNumbers(String userName)
    {
    	Faker faker = new Faker();
    	String randomName = "";
    	try
    	{
    		randomName = faker.numerify(userName);
    		logInfo("The generated random User Name is : " + randomName);
    	}
    	catch (Exception e) 
    	{
			e.getMessage();
		}
        return randomName;
    }
    
    /*
     *  PRIYA --------- Added on Nov 26th
     */
    
   /*
    *  Get product ID 
    */

    public void GetProductIdFromBundleOverlay()
	{
		String productid = driver.getText(CatalogPageObject.getproductid, "ProductId");
		logPass("Product Id is :" +productid);
		data.put("ProductID", productid);
				
	}
    
    /*
     * Generate and Get Session ID
     */
    public void generatesessionId() throws JSONException
    {
        String resp = new String();
        List<String> res = new ArrayList<String>();
        System.out.println("properties "+properties);
        currentDomain = properties.getProperty("Domain");
        String url = currentDomain+"/admin/services/auth/login";
        String credentials = "{\"identity\": \""+properties.getProperty("LoginUserName")+"\", \"password\": \""+properties.getProperty("LoginPassword")+"\"}";
        //System.out.println(credentials);
        //System.out.println(url);
        res =  api.ReadAllDataFromUrl(url,credentials,"");
        
        //res =  api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/adminservices/auth/login","{\"identity\": \""+properties.getProperty("Username")+"\", \"password\": \""+properties.getProperty("Password")+"\"}","");
        resp = res.get(0);
        logInfo("The Session Respsone is : " + resp);
        JSONObject root = new JSONObject(resp);
        sessionId = "SESSIONID="+root.getString("sessionId");
        properties.put("sessionIdorder", sessionId);
    }
    
    
    
    /*
     * Generate Foundation Session Id
     */
    public void generatesessionIdfoundation() throws JSONException
    {
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=    api.ReadAllDataFromUrl(properties.getProperty("APIDomain")+"/foundationservices/getTestTokens","","");
        resp = res.get(0);
        //System.out.println(resp);
        logPass("generatesessionIdfoundation "+resp);
        
        JSONObject root = new JSONObject(resp);
        foundationSessionId = root.getString("testsuperadmin");
        properties.put("foundationSessionId", foundationSessionId);
    }
    
    /*
     * Account admin methods for prescript
     */
    
    ApiReaderUtils Api=new ApiReaderUtils();
	String SessionId="";
	String sessionid="";
    String authToken="";
    String collectionid="";
    
    public void generateaccountsessionId() throws JSONException
    {
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=    Api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/auth/login","{\"identity\": \""+properties.getProperty("LoginUserName")+"\", \"password\": \""+properties.getProperty("LoginPassword")+"\"}"," ");
        resp = res.get(0);
        System.out.println(resp);
        
        JSONObject root = new JSONObject(resp);
        SessionId = "SESSIONID="+root.getString("sessionId");
        authToken = root.getString("authToken");
        
    }
    
    public void generateauthforfoundationservice(String ServiceName) throws JSONException, ClientProtocolException, IOException
    {
    	
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=    Api.ReadAllDataFromUrl(properties.getProperty("APIDomain")+"/foundationservices/getTestTokens","","");
        resp = res.get(0);
        System.out.println(resp);
        pass("generatesessionIdpayment "+resp);

        JSONObject root = new JSONObject(resp);
       
        sessionid = root.getString("testsuperadmin");
        properties.put("sessionid", sessionid);

    }
    
    public JSONObject storecreate(String serviceName) throws JSONException, ClientProtocolException, IOException
    {
    	String AccountstoreId ="";
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=Api.post(properties.getProperty("APIDomain")+"/foundationservices/stores","{ \"allowPreview\": false, \"businessId\": "+properties.getProperty("BusinessId")+", \"currencies\": [ \"USD\" ], \"defaultCurrency\": \"USD\", \"defaultLocale\": \"en_US\", \"defaultShippingRegion\": \"US\", \"id\": 0, \"launchUrl\": \"https://www.store.com\", \"locales\": [ \"en_US\" ], \"logoUrl\": \"https://www.store.com/logo.png\", \"name\": \"StoreA\", \"previewUrl\": \"https://www.storepreview.com\", \"properties\": [ { \"description\": \"Maintenance date of every month\", \"name\": \"prop_maintenancedate\", \"value\": 25 } ], \"shippings\": [ \"US\" ], \"status\": 1, \"timeZone\": \"UTC\", \"type\": 1}",properties.getProperty("sessionid"));
        resp = res.get(0);
        System.out.println(resp);
        
        JSONObject root = new JSONObject(resp);
        AccountstoreId = root.getString("id");
        properties.put("AccountstoreId", AccountstoreId);
        System.out.println("AccountstoreId"+AccountstoreId);
        return root;
        
    }
    
    public JSONObject createCollectioncatalog(String serviceName) throws JSONException
    {
    	String catalogcollectionid="";
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=Api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections?businessId="+properties.getProperty("BusinessId")+"&serviceName=catalog","{ \"name\": \"Automation_account"+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(1000)+"\", \"properties\": [], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId);
        resp = res.get(0);
        System.out.println(resp);
        
        JSONObject root = new JSONObject(resp);
        catalogcollectionid = root.getJSONObject("collectionResponse").getString("id");
        properties.put("catalogcollectionid", catalogcollectionid);
        System.out.println("catalogcollectionid"+catalogcollectionid);
        return root;
        
    }
    
    public JSONObject createCollectioncustomer(String serviceName) throws JSONException
    {
    	String customercollectionid="";
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=Api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections?businessId="+properties.getProperty("BusinessId")+"&serviceName=customer","{ \"name\": \"Automation_account"+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(1000)+"\", \"properties\": [], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId);
        resp = res.get(0);
        System.out.println(resp);
        
        JSONObject root = new JSONObject(resp);
        customercollectionid = root.getJSONObject("collection").getString("id");
        properties.put("customercollectionid", customercollectionid);
        System.out.println("customercollectionid"+customercollectionid);
        return root;
        
    }
    
    public JSONObject createCollectionpricing(String serviceName) throws JSONException
    {
    	String pricingcollectionid ="";
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=Api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections?businessId="+properties.getProperty("BusinessId")+"&serviceName=pricing","{ \"name\": \"Automation_account"+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(1000)+"\", \"properties\": [], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId);
        resp = res.get(0);
        System.out.println(resp);
        
        JSONObject root = new JSONObject(resp);
        pricingcollectionid = root.getJSONObject("collectionResponse").getString("id");
        properties.put("pricingcollectionid", pricingcollectionid);
        System.out.println("pricingcollectionid"+pricingcollectionid);
        return root;
        
    }
    
    public JSONObject createCollectionaccounts(String serviceName) throws JSONException
    {
    	String Accountcollectionid ="";
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=Api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections?businessId="+properties.getProperty("BusinessId")+"&serviceName=account","{ \"name\": \"Automation_account"+System.currentTimeMillis()+driver.generateRandomNumber(1000)+"\", \"description\": \"collectionTest12\",\"properties\": [ { \"name\": \"creditStanding\", \"value\": \"current\" }, { \"name\": \"defaultBudgetExpiryTimeMS\", \"value\": \"31536000000\" }, { \"name\": \"paymentMethod\", \"value\": \"PO\" }, { \"name\": \"defaultContractExpiryTimeMS\", \"value\": \"31536000000\" }, { \"name\": \"defaultBudgetValue\", \"value\": \"1000\" } ], \"status\": \"ACTIVE\"}",sessionId);
        resp = res.get(0);
        System.out.println(resp);
        
        JSONObject root = new JSONObject(resp);
        Accountcollectionid = root.getJSONObject("collection").getString("id");
        properties.put("Accountcollectionid", Accountcollectionid);
        System.out.println("Accountcollectionid"+Accountcollectionid);
        return root;
        
    }
    
    public void accountstoreassocate() throws JSONException, ClientProtocolException, IOException
    {
        String resp = new String();
        List<String> res = new ArrayList<String>();
        res=api.put(properties.getProperty("APIDomain")+"/foundationservices/stores/"+properties.getProperty("AccountstoreId")+"/associations","{\"associations\":[{\"name\":\"account\",\"collectionId\":"+properties.getProperty("Accountcollectionid")+"},{\"name\":\"catalog\",\"collectionId\":"+properties.getProperty("catalogcollectionid")+"},{\"name\":\"customer\",\"collectionId\":"+properties.getProperty("customercollectionid")+"},{\"name\":\"pricing\",\"collectionId\":"+properties.getProperty("pricingcollectionid")+"}]}",sessionid);
        resp = res.get(0);
        System.out.println(resp);
        pass("storeassocatefoundation "+resp);   
    }
    
    /*
     * Account admin components ends
     */
    
    
    //Paramesh
    public void waitForLoaderToBeInvisible()
	{
		driver.explicitWaitforInvisibilityWithoutLocating(By.id("id_loader"), 60, "Loader");
	}
    
    String notActivatedUser="";
	String activationParam="";
	String useremail="";
	String resetParam="";
	
	
	public void doLoginAsBusinessAdmin(String username)
	{
		if(driver.enterText(InstanceTeamPage.emailInput,username , "EmailAddress field"))
			logPass("Email Address is entered");
		else
			logFail("Failed to enter Emailaddress");
		if(driver.enterText(InstanceTeamPage.pwdInput, "Skava@123", "Password field"))
			logPass("Password is entered");
		else
			logFail("Failed to enter Password");
		if(driver.jsClickElement(InstanceTeamPage.signInBtn, "SignIn Button"))
		{	
			
			logPass("SignIn button is clicked");
		}
		else
			logFail("SignIn button is not displayed");
	}

	/*public static void verifyMongoValidation()
	{
		MongoConnection.openMongoConnection();

		MongoConnection.validateProduct(ExcelReader.getData("General_Data","Product"));
	}
*/
	/*
	public void login() 
	{
		driver.maximizeBrowser();
		driver.enterText(Home_Page.emailField,ExcelReader.getData("General_Data","Username"),"Email");
		ExtentTestManager.getTest().log(LogStatus.INFO, "Navigated to application url");
		loggerUtils.passLog("Logger");
	}
	*/
	
	
	
	

	
	public void generateauthToken() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrlAI(properties.getProperty("Domain")+"/admin/services/auth/login","{\"identity\": \""+properties.getProperty("Username")+"\", \"password\": \""+properties.getProperty("Password")+"\"}","","");
		resp= res.get(0);
		System.out.println(resp);
		JSONObject root = new JSONObject(resp);
		authToken = root.getString("authToken"); 
		
	}
	public JSONObject getCollectionAttributes(String serviceName) throws JSONException
	{
		String res = new String();
		res=	api.readResponseFromUrl(properties.getProperty("Domain")+"/admin/services/collections/attributes/?businessId="+properties.getProperty("businessId")+"&serviceName=customer",sessionId);
		System.out.println(res);
		JSONObject root = new JSONObject(res);
        return root;
		
	}
	public JSONObject createCollection(String serviceName) throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrlAI(properties.getProperty("Domain")+"/admin/services/collections?businessId="+properties.getProperty("businessId")+"&serviceName=customer","{ \"name\": \"Automation_Customer\", \"properties\": [], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId,"");
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		collectionid = root.getJSONObject("collection").getString("id");
		properties.put("collectionid", collectionid);
		System.out.println("collectionid"+collectionid);
		return root; 
		
	}
	
	
	public String inviteSuperAdmin() throws JSONException
	{
	    String email="superadmin"+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(9999)+"@gmail.com";
		String resp = new String();
		List<String> res = new ArrayList<String>();
		String url=properties.getProperty("Domain")+"/admin/services/auth/users/invite?inviteUserFlag=true";
		String postData="{\"users\": [ { \"email\":\""+email+"\",\"firstName\": \"automation\", \"lastName\": \"test\" } ], \"userRoles\": [ { \"roleName\": \"ROLE_SUPER_ADMIN\", \"category\": \"STANDARD\", \"businessId\": 0, \"collectionId\": 0, \"serviceCollectionId\": 0 } ]}";
		res=api.ReadAllDataFromUrlAI(url,postData,sessionId,"");
		resp = res.get(0);
		System.out.println("InviteResponse:"+ resp);
		JSONObject root = new JSONObject(resp);
		notActivatedUser=root.getJSONArray("createdUsers").getJSONObject(0).getString("email");
		
		return email;
	}
	
	public void inviteNoRoleUser() throws JSONException
	{
	    String email="norole"+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(9999)+"@gmail.com";
		String resp = new String();
		List<String> res = new ArrayList<String>();
		String url=properties.getProperty("Domain")+"/admin/services/auth/users/invite?inviteUserFlag=true";
		String postData="{\"users\": [ { \"email\":\""+email+"\",\"firstName\": \"automation\", \"lastName\": \"test\" } ]}";
		res=api.ReadAllDataFromUrlAI(url,postData,sessionId,"");
		resp = res.get(0);
		System.out.println("InviteResponse:"+ resp);
		JSONObject root = new JSONObject(resp);
		notActivatedUser=root.getJSONArray("createdUsers").getJSONObject(0).getString("email");
		System.out.println("Not act "+notActivatedUser);
	}
	
	public void generateActivationParam() throws JSONException
	{

		String url=properties.getProperty("Domain")+"/user/users/activation?locale=en_US";
		String postData="{ \"email\":\""+notActivatedUser+"\"}";
		
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrlAI(url,postData,"","1");
		
		resp = res.get(0);
		
		System.out.println("Activation: "+resp);
		JSONObject root = new JSONObject(resp);
		if(root.has("users")) {
		activationParam=root.getJSONArray("users").getJSONObject(0).getJSONObject("customProperties").getString("lastactivationparam");
		}else {
		activationParam=root.getJSONObject("customProperties").getString("lastactivationparam");
		}
//		System.out.println("Param: "+activationParam);
	}
	
	public String registerUser() throws JSONException
	{
		try {
		String url=properties.getProperty("Domain")+"/admin/services/auth/users?activationParam="+activationParam;
	    String postData="{ \"newPassword\": \"Skava@123\", \"confirmNewPassword\": \"Skava@123\"}";	
	    String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrlAI(url,postData,"","");
		
        resp = res.get(0);
		
		System.out.println("Register: "+resp);
		JSONObject root = new JSONObject(resp);
		String email="";
		if(root.has("user")) {
		email=root.getJSONObject("user").getString("email");
		}else {
		email=root.getJSONArray("users").getJSONObject(0).getString("email");
		}
		
		return email;
		}catch (Exception e) {
			// TODO: handle exception
		}
		return "";
	}
	
	public String inviteBusinessAdmin12() throws JSONException
    {
        String email="businessadmin"+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(100)+"@skava.com";
        String resp = new String();
        List<String> res = new ArrayList<String>();
        String url=properties.getProperty("Domain")+"/admin/services/auth/users/invite?inviteUserFlag=true";
        String postData="{\"users\": [ { \"email\":\""+email+"\"}], \"userRoles\": [ { \"roleName\": \"ROLE_BUSINESS_ADMIN\", \"category\": \"STANDARD\", \"businessId\": 4}]}";
        //String postData="{\"users\": [ { \"email\":\""+email+"\",\"firstName\": \"automation\", \"lastName\": \"test\" } ], \"userRoles\": [ { \"roleName\": \"ROLE_BUSINESS_ADMIN\", \"category\": \"STANDARD\", \"businessId\": 1}]}";
        res=api.ReadAllDataFromUrlAI(url,postData,properties.getProperty("sessionId"),"");
        resp = res.get(0);
        JSONObject root = new JSONObject(resp);
        System.out.println("inviteBusinessAdmin-->"+res);
        notActivatedUser=root.getJSONArray("createdUsers").getJSONObject(0).getString("email");
        
        return notActivatedUser;
    }
	
	public String inviteBusinessAdmin() throws JSONException
	{
	    String email="businessadmin"+Long.toString(System.currentTimeMillis())+driver.generateRandomNumber(9999)+"@gmail.com";
		String resp = new String();
		List<String> res = new ArrayList<String>();
		String url=properties.getProperty("Domain")+"/admin/services/auth/users/invite?inviteUserFlag=true";
		String postData="{\"users\": [ { \"email\":\""+email+"\" } ], \"userRoles\": [ { \"roleName\": \"ROLE_BUSINESS_ADMIN\", \"category\": \"STANDARD\", \"businessId\": \"1\", \"collectionId\": 0, \"serviceCollectionId\": 0 } ]}";
		res=api.ReadAllDataFromUrlAI(url,postData,sessionId,"");
		resp = res.get(0);
		System.out.println("InviteResponse:"+ resp);
		JSONObject root = new JSONObject(resp);
		notActivatedUser=root.getJSONArray("createdUsers").getJSONObject(0).getString("email");
		
		return notActivatedUser;
	}
	
	public String passwordReset(String email) throws JSONException
	{
		
		
		String resp = new String();
		List<String> res = new ArrayList<String>();
		String url=properties.getProperty("Domain")+"/user/users/password?locale=en_US";
		String postData="{\"email\":\""+email+"\"}";
		res=api.ReadAllDataFromUrlAI(url,postData,sessionId,"1");
		resp = res.get(0);
		System.out.println("InviteResponse:"+ resp);
		JSONObject root = new JSONObject(resp);
		resetParam=root.getString("resetParam");
		
		return resetParam;
	}
	
	
}