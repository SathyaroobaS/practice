package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.EditUserPage;

public class EditUserComponents extends PricingComponent{
	
	public  void  verifyEditUserPage()
	{
		if(driver.isElementDisplayed(EditUserPage.email,  "User email"))
		logPass("User email is displayed");
		else
		logFail("Failed to display user email");
//		
//		if(driver.isElementDisplayed(EditUserPage.firstname, 10, "Firstname"))
//		logPass("Firstname field is displayed");
//		else
//			logFail("Failed to display firstname field");
		
//		if(driver.isElementDisplayed(EditUserPage.lastname, 10, "lastname"))
//			logPass("Lastname field is displayed");
//			else
//				logFail("Failed to display lastname field");
		
	}
	
	public void clickStatusToggle()
	{
		waitForLoaderToBeInvisible();
		if(driver.jsClickElement(EditUserPage.statusToggleBtn, "Activate/Deactivate"))
			logPass("User activate/deactivate btn is clicked");
		else
			logFail("Failed to click user activate/deactivate btn");
	}
	
	public void clickUpdateBtn()
	{
		driver.scrollToElement(EditUserPage.updateUserBtn, "Update button");
		driver.explicitWaitforInvisibilityWithoutLocating(By.id("id_loader"), 10, "loader");
		if(driver.clickElement(EditUserPage.updateUserBtn, "Update button"))
			logPass("Update button is clicked");
		else
			logFail("Failed to click update button ");
	}

	public void verifyUserUpdateSuccessMsg()
	{
		driver.scrollToElement(EditUserPage.editUserSuccessMsg, "Update success messgae");
		if(driver.isElementDisplayed(EditUserPage.editUserSuccessMsg, "Update success messgae"))
			logPass("Update user success message is displayed");
		else 
			logFail("Failed to display user update success message");
	}
	
	public void verifyInvalidRequestErrorMsg()
	{
		if(driver.getText(EditUserPage.editUserErrorMsg, "Error msg").equals("User Is Not Allowed To Update"))
			logPass("Not allowed to update NOT activated user role");
		else
			logFail("User not allowed to update msg is not displayed");
	}
	
	public void clickResendActivation()
	{
		waitForLoaderToBeInvisible();
		driver.scrollToElement(EditUserPage.resendActivationBtn, "resend activation button");
		
		if(driver.clickElement(EditUserPage.resendActivationBtn, "resend activation button"))
			logPass("Resend activation is clicked");
		else
			logFail("Failed to click resend activation");
	}
	
	public void verifyResendSuccessMsg()
	{
		waitForLoaderToBeInvisible();
		driver.scrollToElement(EditUserPage.resendActivationSuccessMsg, "resend activation Success message");
		
		if(driver.getText(EditUserPage.resendActivationSuccessMsg, "Resend activation Success message").equals("Successfully re-invited the user"))
			logPass("Resend activation success message displayed");
		else
			logFail("Failed to re-send activation");
	}
	
	public void navigateToEditPage()
	{
		driver.navigateToUrl("");
	}
	
	public void isActiveDeactiveLeverNotDisplayed()
	{
		if(driver.isElementNotDisplayed(EditUserPage.statusToggleBtn, "Status toggle"))
			logPass("Active/Deactive toggle is not displayed for Not activate user");
		else
			logFail("Active/Deactive toggle is displayed for Not activate user");
	}
	
	public void isResendActivationButtonNotDisplayed()
	{
		waitForLoaderToBeInvisible();
		if(driver.isElementNotDisplayed(EditUserPage.resendActivationBtn, "resend activation"))
			logPass("Resend activation button not displayed");
		else
			logFail("Resend activation button is displayed");
		
	}
	
	public void clickStandardRole()
	{
	   driver.jsClickElement(EditUserPage.standardRolesAccordian, "standard role accordian");
	}
	
	public void clickAllCollection()
	{
		driver.clickElement(EditUserPage.collectionAllCheckbox, "All collection checkbox");
	}
	
	public void clickCollection()
	{
		driver.clickElement(EditUserPage.rolesCollectionCheckbox, "role collection checkbox");
	}
	
	public void verifyUserUpdateSuccessMessage()
	{
		waitForLoaderToBeInvisible();
//		driver.explicitWaitforVisibility(EditUserPage.editUserSuccessMsg, 10, "Success message");
//		driver.scrollToElement(EditUserPage.editUserSuccessMsg, "Update success messgae");
		if(driver.isElementDisplayed(EditUserPage.resendActivationSuccessMsg, "Update success messgae"))
			logPass("Update user success message is displayed");
		else 
			logFail("Failed to display user update success message");
	}
	
}
