package com.skava.reusable.components;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.networkutils.ApiReaderUtils;
import com.skava.object.repository.customerAdminRepository;


public class customerAdminComponents extends RegistrationScreenComponents  {

	public void displayaddCustomertrigger()
	{
		
		if(driver.explicitWaitforClickable(customerAdminRepository.addcustomerTrigger,  "addCustomerTriggerDisplay"))
		{
			pass("Add Customer button displayed");
		}
		else
		{
			fail("Add Customer button not displayed");
		}
	}
	
	public void clickaddCustomerTrigger()
	{
		if(driver.jsClickElement(customerAdminRepository.addcustomerTrigger, "addCustomerTrigger"))
		{
			pass("Add Customer button is clicked");
		}
		else
		{
			fail("Add Customer button is not clicked");
		}
		
	}
	
	public void displayCustomerFirstNameForAddCustomer()
	{
		
		if(driver.explicitWaitforClickable(customerAdminRepository.firstnameInput,  "displayCustomerFirstName"))
		{
			pass("First Name field is displayed");
		}
		else
		{
			fail("First Name field is not displayed");
		}
		
	}
	
	public void enterCustomerFirstNameForAddCustomer()
	{
		String firstName="skava";
		if(driver.enterText(customerAdminRepository.firstnameInput, firstName, "firstNameInput"))
		{
			pass("First Name is entered");
		}
		else
		{
			fail("First Name is not entered");
		}
		
	}
	
	public void displayCustomerLastNameForAddCustomer()
	{
		
		if(driver.explicitWaitforClickable(customerAdminRepository.lastnameInput,  "displayCustomerLastName"))
		{
			pass("Last Name field is displayed");
		}
		else
		{
			fail("Last Name field is not displayed");
		}
		
	}
	
	public void enterCustomerLastNameForAddCustomer()
	{
		String lastName="test";
		if(driver.enterText(customerAdminRepository.lastnameInput, lastName, "lastNameInput"))
		{
			pass("Last Name is entered");
		}
		else
		{
			fail("Last Name is not entered");
		}
		
	}
	
	public void displayCustomerEmailForAddCustomer()
	{
		
		if(driver.explicitWaitforClickable(customerAdminRepository.emailInput,  "displayCustomerEmail"))
		{
			pass("Email field is displayed");
		}
		else
		{
			fail("Email field is not displayed");
		}
		
	}
	
	public void enterCustomerEmailForAddCustomer()
	{
		
		driver.explicitWaitforClickable(customerAdminRepository.emailInput,  "emilaInput");
		email="skava"+driver.generateRandomNumber(10000)+"@gmail.com";
		if(driver.enterText(customerAdminRepository.emailInput, email, "emilaInput"))
		{
			pass("Email address is entered");
		}
		else
		{
			fail("Email address is not entered");
		}
		
	}
	
	public void clickSendInviteButton()
	{
		
		if(driver.jsClickElement(customerAdminRepository.sendInviteTrigger, "sendInviteTrigger"))
		{
			pass("Send Invite button is clicked");
		}
		else
			fail("Send Invite button is not clicked");
		
	}
	
	public void hoverandclickCustomers()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.emailValue,  "view button"))
		{
			int size = driver.getSize(customerAdminRepository.emailValue, "view btn");
			int rand = driver.generateRandomNumber(size);
			if(rand == 0)
				rand = rand + 1;
			if(driver.jsClickElement(By.xpath("(//*[@data-qa='view-profile-trigger'])[1]"), "view profile button"))
				pass("View Button is Clicked");
			else
				fail("View Button is not Clicked");
				
		}
	}
	
	public void verifycustomerAdminRepository() {
		if(driver.isElementDisplayed(customerAdminRepository.customerId,  "Customer ID")) {
			logPass("Customer ID is Readonly field");
		}
		else {
			logFail("Customer ID is not readonly field");
		}
	}
	
	public void clickEditProfile()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.editProfileTrigger,  "edit profile "))
		{	
			pass("Edit Profile Button is displayed");
			if(driver.jsClickElement(customerAdminRepository.editProfileTrigger, "edit profile"))
				pass("Edit Profile Button is Clicked");
			else
				fail("Edit Profile Button is not Clicked");
		}
		else
			fail("Edit Profile Button is displayed");
	}
	
	public void changeStatusFreeze()
	{
		if(driver.jsClickElement(By.xpath("//*[@data-qa='status-container']//input"), "status"))
			pass("Status dropdown is clicked");
		else
			fail("Status dropdown is not clicked");
		if(driver.clickElement(By.xpath("(//*[@data-qa='status-container']//ul/li)//*[text()='Freeze']"), "Freeze"))
			pass("Freeze is clicked");
		else
			fail("Freeze is not clicked");
	}
	
	public void clickSaveChangesButton1()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.saveTrigger1,  "status"))
		{
			if(driver.clickElement(customerAdminRepository.saveTrigger1, "status"))
				pass("Save Changes is clicked");
			else
				fail("Save Changes is not clicked");
		}
	}
	
	public void clickFreezeAccount() {
		if(driver.explicitWaitforVisibility(customerAdminRepository.FreeeAccountButton,  "freezeAccount"))
		{
			if(driver.clickElement(customerAdminRepository.FreeeAccountButton, "freezeAccount"))
				pass("Freeze Account button is clicked");
			else
				fail("Freeze Account button is not clicked");
		}
		}
	public void changeStatusunFreeze()
	{
		driver.jsClickElement(By.xpath("(//*[contains (@class,'cls_profile_mdb-select')]//input)[2]"), "status");
		int size = driver.getSize(By.xpath("(//*[contains (@class,'cls_profile_mdb-select')])[3]//li"),"status");
		for(int i=1;i<=size;i++)
		{	
			String text = driver.getText(By.xpath("((//*[contains (@class,'cls_profile_mdb-select')])[3]//li)["+i+"]"), "status");
			if(text.equalsIgnoreCase("active"))
			{		
				if(driver.jsClickElement(By.xpath("((//*[contains (@class,'cls_profile_mdb-select')])[3]//li)["+i+"]//span"), "status"))
					pass("unFreeze is clicked");
				else
					fail("unFreeze is not clicked");
			}
		}
	}
	
	public void changeStatusDelete() {
	if(driver.explicitWaitforVisibility(customerAdminRepository.deleteAccountTrigger,  "status"))
	{	
		if(driver.jsClickElement(customerAdminRepository.deleteAccountTrigger, "status"))
			pass("Delete Button is clicked");
		else
			fail("Delete Button is not clicked");
	}
	}
	
	public void clickPaymentsTab()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.paymentsTab,  "Payments tab"))
		{
			if(driver.jsClickElement(customerAdminRepository.paymentsTab, "Payments tab"))
				pass("Payments Tab is Clicked");
			else
				fail("Payments Tab is not clicked");
		}
	}
	public void clickAddNewPayments() throws InterruptedException
	{
		Thread.sleep(4000);
		if(driver.explicitWaitforVisibility(customerAdminRepository.addnewPaymentMethod,  "Payments Method"))
		{	driver.scrollToElement(customerAdminRepository.addnewPaymentMethod, "Payments Method");
			if(driver.jsClickElement(customerAdminRepository.addnewPaymentMethod, "Payments Method"))
				pass("Add New Payment Method is Clicked");
			else
				fail("Add New Payment Method is not Clicked");
		}
	}
	public void verifyAddNewPaymentsSection()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.addCreditCardNumberLabel,  "add new credit card label"))
			pass("Add New Payment Section is displayed");
		else
			fail("Add New Payment Section is not displayed");
	}
	
	public void addSelectExpirationDate()
	{
		if(driver.jsClickElement(By.xpath("(//*[@class='caret']//span)[3]"), "expiration date"))
		{	
			pass("Edit Expiration Date is clicked"); 
//			int size = driver.getSize(By.xpath("(//*[@data-qa='add-expiration-month'])//ul//li"), "expiration date");
			if(driver.jsClickElement(By.xpath("(//*[contains (@class,'select-wrapper cls_skPaymentTabDropDown cls_skPaymentExpMonth')])[1]//ul//li[1]"),"expiration date"))
				pass("Expiration Date is  selected");
			else
				fail("Expiration Date is not selected");
		}
		else
			fail("Edit Expiration date is not clicked");
		
	}
	
	public void addSelectExpirationYear()
	{
		if(driver.jsClickElement(By.xpath("(//*[@class='caret']//span)[4]"), "expiration date"))
		{	
			pass("Edit Expiration Date is clicked");
			
			if(driver.jsClickElement(By.xpath("(//*[contains (@class,'select-wrapper cls_skPaymentTabDropDown cls_skPaymentExpYear')])[1]//ul//li[3]"),"expiration date"))
				pass("Expiration Date is  selected");
			else
				fail("Expiration Date is not selected");
		}
		else
			fail("Edit Expiration date is not clicked");
	}
	
	public void enterCardNumber()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.addCreditCardNumberInput,  "add card Number"))
		{
			if(driver.enterText(customerAdminRepository.addCreditCardNumberInput, "378282246310005", "add card Number"))
				pass("Card Number is entered");
			else
				fail("Card Number is not entered");
		}
	}
	
	public void addCVVInput()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.addCvvInput,  "add Cvv"))
		{
			if(driver.enterText(customerAdminRepository.addCvvInput, "123", "Cvv"))
				pass("Cvv is added");
			else
				fail("Cvv is not added");
		}
	}
	public void addCardNames()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.addCardFirstNameInput,  "Card Name"))
		{
			if(driver.enterText(customerAdminRepository.addCardFirstNameInput, "Skava", "Card Name"))
				pass("Card Name is added");
			else
				fail("Card Name is not added");
		}
		if(driver.explicitWaitforVisibility(customerAdminRepository.addCardLastNameInput,  "Card Name"))
		{
			if(driver.enterText(customerAdminRepository.addCardLastNameInput, "Skava", "Card Name"))
				pass("Card Name is added");
			else
				fail("Card Name is not added");
		}
	}
	
	public void addBillingNames()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.addFirstNameInput,  "Card Name"))
		{
			if(driver.enterText(customerAdminRepository.addFirstNameInput, "first", "Card Name"))
				pass("Card Name is added");
			else
				fail("Card Name is not added");
		}
		if(driver.explicitWaitforVisibility(customerAdminRepository.addLastNameInput,  "Card Name"))
		{
			if(driver.enterText(customerAdminRepository.addLastNameInput, "last", "Card Name"))
				pass("Card Name is added");
			else
				fail("Card Name is not added");
		}
	}
	
	public void addVerifyBillingAddress()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.addAddressInput,  "addaddress"))
		{
			if(driver.enterText(customerAdminRepository.addAddressInput, "13, street 1", "address input"))
				pass("Edit Address is Editable");
			else
				fail("Edit Address is not Editable");
		}
		if(driver.explicitWaitforVisibility(customerAdminRepository.addAddress2Input,  "addaddress"))
		{
			if(driver.enterText(customerAdminRepository.addAddress2Input, "address2", "address input"))
				pass("add Address2 is Editable");
			else
				fail("add Address2 is not Editable");
		}
		if(driver.explicitWaitforVisibility(customerAdminRepository.addCityInput,  "addaddress"))
		{
			if(driver.enterText(customerAdminRepository.addCityInput, "Albany", "address input"))
				pass("add City is Editable");
			else
				fail("add City is not Editable");
		}
		if(driver.explicitWaitforVisibility(customerAdminRepository.addZipcodeInput,  "addaddress"))
		{
			if(driver.enterText(customerAdminRepository.addZipcodeInput, "12084", "address input"))
				pass("add Zipcode is Editable");
			else
				fail("add Zipcode is not Editable");
		}
		if(driver.explicitWaitforVisibility(customerAdminRepository.addStateDropdown,  "addaddress"))
		{
			if(driver.enterText(customerAdminRepository.addStateDropdown, "NY", "address input"))
				pass("add State is Editable");
			else
				fail("add State is not Editable");
		}
		if(driver.explicitWaitforVisibility(customerAdminRepository.addCountryDropdown,  "addaddress"))
		{
			if(driver.enterText(customerAdminRepository.addCountryDropdown, "US", "address input"))
				pass("add Country is Editable");
			else
				fail("add Country is not Editable");
		}
		if(driver.explicitWaitforVisibility(customerAdminRepository.addPhoneNumberInput,  "addaddress"))
		{
			if(driver.enterText(customerAdminRepository.addPhoneNumberInput, "1234567890", "address input"))
				pass("add PhoneNumber is Editable");
			else
				fail("add PhoneNumber is not Editable");
		}
		
	}
	
	public void clickAndSaveBtn() throws InterruptedException
	{
		if(driver.explicitWaitforClickable(customerAdminRepository.addsaveChangesTrigger,  "save changes Button"))
		{
//		Thread.sleep(30);	
		if(driver.clickElement(customerAdminRepository.addsaveChangesTrigger, "save changes Button"))
			pass("save changes Btn is Clicked");
		else
			
			fail("save changes Btn is not Clicked");
		}
	}
	
	public void clickDownArrow()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.downArrow,  "downArrow"))
		{
			if(driver.jsClickElement(customerAdminRepository.downArrow, "downArrow"))
				pass("Down Arrow is Clicked");
			else
				fail("Down Arrow is not Clicked");
		}
	}
	
	public void clickEditButton()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.editBtn,  "Edit Button"))
		{
			pass("Edit button is displayed");
			if(driver.jsClickElement(customerAdminRepository.editBtn, "Edit Btn"))
				pass("Edit button is clicked");
			else
				fail("Edit button is not clicked");
		}
	}
	
	public void verifyRemoveBtn()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.removeBtn,  "remove btn"))
		{
			if(driver.jsClickElement(customerAdminRepository.removeBtn, "remove Btn"))
				pass("Remove Btn is clicked");
			else
				fail("Remove Btn is Not clicked");
		}
	}
	
	public void removeOkBtn()
	{
		if(driver.explicitWaitforClickable(customerAdminRepository.removeOkBtn,  "Remove"))
		{
			pass("click delete payment");
			driver.jsClickElement(customerAdminRepository.removeOkBtn, "Remove");
		}
			
		else
			fail("payment is not deleted");
	}
	
	public void clickCartTab()
	{
		if(driver.explicitWaitforVisibility(customerAdminRepository.customerCartTab,  "Cart Tab "))
		{	
			pass("Cart Tab Option is displayed");
			if(driver.clickElement(By.xpath("//a[@data-qa='cart-tab-trigger']"), "Cart Tab")) {
				pass("Cart Tab Option is Clicked");
			}
			else {
				fail("Cart Tab Option is not Clicked");
			}
		}
		else {
			fail("Cart Tab Option is not displayed");
		}
	}
	
	public void clickCartViewProfileButton() {
		
		if(driver.explicitWaitforVisibility(customerAdminRepository.customerCartViewProfileButton,  "View Profile "))
		{	
			pass("View Profile Option is displayed");
			if(driver.jsClickElement(customerAdminRepository.customerCartViewProfileButton, "View Profile")) {
				pass("View Profile Option is Clicked");
			}
			else {
				fail("View Profile Option is not Clicked");
			}
		}
		else {
			fail("View Profile Option is not displayed");
		}
		
	}

	public void clickOrdersTab()
	{
	if(driver.explicitWaitforVisibility(customerAdminRepository.ordersTab,  "orders tab"))
	{
		if(driver.jsClickElement(customerAdminRepository.ordersTab, "orders tab"))
			pass("Orders Tab is Clicked");
		else
			fail("Orders Tab is not clicked");
	}
	}
	public void verifyOrderList() throws InterruptedException
	{
		Thread.sleep(4000);
		if(driver.explicitWaitforVisibility(customerAdminRepository.orderValue,  "order list"))
			pass("Order List is displayed");
		else
			fail("Order List is not displayed");
	}
	
	public void addressTab() {
		if(driver.isElementDisplayed(customerAdminRepository.addressTab,  "Address Tab gets displayed")) {
			logPass("Address Tab gets displayed");
		}
		else
		{
			logFail("Address Tab not displayed");
		}
	}
	
	public void loadAddressScreen() {
		if(driver.isElementDisplayed(customerAdminRepository.addressTab,  "Address Tab gets displayed"))
		{
			if(driver.jsClickElement(customerAdminRepository.addressTab, "Address Tab")) {

				logPass("Address tab gets clicked");

				if(driver.isElementDisplayed(customerAdminRepository.savedAddressTile,  "Address Screen"))
				{
					logPass("Address Screen gets displayed");
				}
				else
				{
					driver.isElementDisplayed(customerAdminRepository.addAddressTitle,  "Address screen");
					logPass("Address screen gets displayed");
				}
			}
			else
				logFail("Failed to display Add Address & Save address tile");
		}
		else 
			logFail("Failed to display address tab");

	}	
	
	public void savedAddressTile() {
		if(driver.isElementDisplayed(customerAdminRepository.savedAddressTile,  "Existing Address details")) 
		{
			if(driver.explicitWaitforVisibility(customerAdminRepository.editTrigger,  "Edit icon is displayed")) 
			{
				logPass("Edit icon is Present");
			}
			else 
			{
				logFail("Failed to displayed Edit icon");
			}
			if(driver.explicitWaitforVisibility(customerAdminRepository.deleteTrigger,  "Delete icon is displayed"))
				logPass("Delete icon is Present");
			else {
				logFail("Failed to display the delete icon");
			}
		}
		else
		{
			logFail("No address tile is displayed");
		}
	}
	
	public void clickAddAddressButton() {
		if(driver.explicitWaitforClickable(customerAdminRepository.addNewAddress,  "Add New Address button")){
			if(driver.clickElement(customerAdminRepository.addNewAddress, "Add New Address button")) {
				if(driver.isElementDisplayed(customerAdminRepository.addAddressTitle,  "Add Address pop-up")) {
					logPass("Add Address pop-up gets displayed");
				}
				else {
					logFail("Pop-up not displayed");
				}
			}
		}
		else {
			logFail("Failed to displayed add New address button");
		}
	}
	
	public void enteringAllMandatoryFieldsInAddAddress() {
		if(driver.isElementDisplayed(customerAdminRepository.addFirstName,  "First Name field")) {
			driver.enterText(customerAdminRepository.addFirstName, ExcelReader.getData("customerAdmin", "FirstName"), "FirstName");
			logPass("First Name field is entered");
		}
		else {
			logFail("First Name field is not entered");
		}
		if(driver.isElementDisplayed(customerAdminRepository.addLastName,  "Last Name field")) {
			driver.enterText(customerAdminRepository.addLastName, ExcelReader.getData("customerAdmin", "LastName"), "LastName");
			logPass("Last Name field is entered");
		}
		else {
			logFail("Last Name field is not entered");
		}
		if(driver.isElementDisplayed(customerAdminRepository.addAddress1,  "Address 1 field")) {
			driver.enterText(customerAdminRepository.addAddress1, ExcelReader.getData("customerAdmin", "Address1"), "Address1");
			logPass("Address 1 field is entered");
		}
		else {
			logFail("Address 1 field is not entered");
		}
		if(driver.isElementDisplayed(customerAdminRepository.addAddress2,  "Address 2 field")) {
			driver.enterText(customerAdminRepository.addAddress2, ExcelReader.getData("customerAdmin", "Address2"), "Address2");
			logPass("Address 2 field is entered");
		}
		else {
			logFail("Address 2 field is not entered");
		}
		if(driver.isElementDisplayed(customerAdminRepository.addCity,  "City field")) {
			driver.enterText(customerAdminRepository.addCity, ExcelReader.getData("customerAdmin", "City"), "City");
			logPass("City field is entered");
		}
		else {
			logFail("City field is not entered");
		}
		if(driver.isElementDisplayed(customerAdminRepository.addState,  "State field")) {
			driver.enterText(customerAdminRepository.addState, ExcelReader.getData("customerAdmin", "State"), "State");
			logPass("State field is entered");
		}
		else {
			logFail("State field is not entered");
		}
		if(driver.isElementDisplayed(customerAdminRepository.addCountry,  "Country field")) {
			driver.enterText(customerAdminRepository.addCountry, ExcelReader.getData("customerAdmin", "Country"), "Country");
			logPass("Country field is entered");
		}
		else {
			logFail("Country field is not entered");
		}
		if(driver.isElementDisplayed(customerAdminRepository.addZipCode,  "Zip code field")) {
			driver.enterText(customerAdminRepository.addZipCode, ExcelReader.getData("customerAdmin", "Zipcode"), "Zip code");
			logPass("ZipCode field is entered");
		}
		else {
			logFail("Zipcode field is not entered");
		}
		if(driver.isElementDisplayed(customerAdminRepository.addPhoneNo,  "Phone no field")) {
			driver.enterText(customerAdminRepository.addPhoneNo, ExcelReader.getData("customerAdmin", "Phoneno"), "Phone no");
			logPass("Phone No field is entered");
		}
		else {
			logFail("Phone No field is not entered");
		}

	}
	
	public void triggerSave() {
		if(driver.explicitWaitforClickable(customerAdminRepository.addSaveTrigger,  "Save Button")) {
			if(driver.jsClickElement(customerAdminRepository.addSaveTrigger, "Save button")) {
				logPass("Save button is clicked");
			}
			else
				logFail("Failed to click");
		}
		else
			logFail("Failed to display Save button");
	}
	
	public void deleteSavedAddress() {
		if(driver.explicitWaitforClickable(customerAdminRepository.deleteTrigger,  "Delete icon in address tile"))
		{
			if(driver.jsClickElement(customerAdminRepository.deleteTrigger, "Delete icon clicked")) 
			{
				logPass("Delete icon is clicked");
			}
			else
			{
				logFail("Failed to display delete icon");
			}
//			if(driver.isElementDisplayed(customerAdminRepository.deletePopUp, 10, "Confirmation pop-up"))
//			{
				driver.jsClickElement(customerAdminRepository.okTrigger, "OK Button");
				driver.isElementDisplayed(customerAdminRepository.deleteSuccessText,  "Success Message displayed");
				logPass ("Success Message get displayed");
//			}
//			else {
//				logFail("Confirmation pop-up not displayed");
//			}
		}
	}
	public void verifyUserProfilePage() {
		if(driver.isElementDisplayed(customerAdminRepository.customerId,  "Customer ID")) {
			logPass("Customer ID is Readonly field");
		}
		else {
			logFail("Customer ID is not readonly field");
		}
	}
	ApiReaderUtils api=new ApiReaderUtils();
	public void generatesessionId() throws JSONException
	{
		currentDomain = properties.getProperty("Domain");
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=	api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/auth/login","{\"identity\": \""+properties.getProperty("LoginUserName")+"\", \"password\": \""+properties.getProperty("LoginPassword")+"\"}","","","","");
		try
		{
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		sessionId = "SESSIONID="+root.getString("sessionId"); 
		logPass("generatesessionId "+sessionId);
		properties.put("sessionId", sessionId);
		}
		catch(Exception e) {
			logFail("Failed Response::::"+resp);
		}
		
	}
	public void generateauthToken() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=	api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/auth/login","{\"identity\": \""+properties.getProperty("LoginUserName")+"\", \"password\": \""+properties.getProperty("LoginPassword")+"\"}","","","","");
		resp = res.get(0);
		System.out.println(resp);
		JSONObject root = new JSONObject(resp);
		authToken = root.getString("authToken"); 
		
	}
	String orderauthToken="";
	public void generateOrderauthToken() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=	api.ReadAllDataFromUrl(properties.getProperty("APIDomain")+"/omsservices/getTestTokens","","","","","");
		resp = res.get(0);
		System.out.println(resp);
		JSONObject root = new JSONObject(resp);
		orderauthToken = root.getString("testsuperadmin"); 
		pass("orderauthToken "+orderauthToken);
	}
	
	public JSONObject getCollectionAttributes(String serviceName) throws JSONException
	{
		String res = new String();
		res=	api.readResponseFromUrl(properties.getProperty("Domain")+"/admin/services/collections/attributes/?businessId="+properties.getProperty("businessId")+"&serviceName=customer",sessionId);
		System.out.println(res);
		JSONObject root = new JSONObject(res);
        return root;
		
	}
	String Cuscollectionid="";
	public JSONObject createCollectionCustomer(String collectionName) throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("BusinessId")+"&serviceName=customer","{ \"name\": \""+collectionName+"\", \"properties\": [], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId,"","","");
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		Cuscollectionid = root.getJSONObject("collection").getString("id");
		properties.put("Customercollectionid", Cuscollectionid);
		String url=properties.getProperty("ApplicationUrl");
		url=url.replace("<<collectionId>>", Cuscollectionid);
		properties.put("ApplicationUrl",url);
		System.out.println("CustomerCollectionUrl-->"+url);		
		System.out.println("Customercollectionid"+Cuscollectionid);
		System.out.println("Customercollectionid--->"+properties.get("Customercollectionid"));
		pass("Customercollectionid "+Cuscollectionid);
		return root; 
		
	}
	String Ordcollectionid="";
	public JSONObject createCollectionOrder(String collectionName) throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("BusinessId")+"&serviceName=order","{\"properties\":[{\"name\":\"orderidlength\",\"value\":7},{\"name\":\"BPM_SubmitOrder\",\"value\":null},{\"name\":\"lookupRequiredFields\",\"value\":null},{\"name\":\"isNumericOrderIdOnly\",\"value\":null},{\"name\":\"skipDefaultQueue\",\"value\":null},{\"name\":\"createOrderCustomValidator\",\"value\":null}],\"description\":\"OrderCollectionTesting\",\"name\":\""+collectionName+"\",\"status\":\"ACTIVE\"}",sessionId,"","","");
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		Ordcollectionid = root.getJSONObject("collection").getString("id");
		properties.put("Ordercollectionid", Ordcollectionid);
		System.out.println("Ordercollectionid"+Ordcollectionid);
		System.out.println("Ordercollectionid--->"+properties.get("Ordercollectionid"));
		pass("Ordcollectionid "+Ordcollectionid);
		return root; 
		
	}
	String Paycollectionid="";
	public JSONObject createCollectionPayment(String collectionName) throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("BusinessId")+"&serviceName=payment","{\"name\":\""+collectionName+"\",\"description\":\"tpayment\",\"status\":\"ACTIVE\",\"properties\":[{\"name\":\"default_locale\",\"value\":\"en_US\"}]}",sessionId,"","","");
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		Paycollectionid = root.getJSONObject("collection").getString("id");
		properties.put("Paymentcollectionid", Paycollectionid);
		System.out.println("Paymentcollectionid"+Paycollectionid);
		pass("Paycollectionid "+Paycollectionid);
		return root; 
		
	}
	
	
	public void cacheClear() throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/userservices/cache/clear","","","","","");
		resp = res.get(0);
		System.out.println(resp);
		
	}

	public JSONObject updateStore() throws JSONException
	{
		String resp = new String();
		
		List<String> res = new ArrayList<String>();
		
		res=api.ReadAllPutDataFromUrl(properties.getProperty("Domain")+"/admin/services/stores/"+properties.getProperty("CustomerStoreId"),"{\"businessId\":"+properties.getProperty("BusinessId")+",\"name\":\"Customer Admin\",\"status\":0,\"type\":2,\"timeZone\":\"IST\",\"previewUrl\":\"http://skava.com\",\"launchUrl\":\"http://skava.com\",\"logoUrl\":\"http://skava.com\",\"defaultLocale\":\"en_US\",\"defaultShippingRegion\":\"US\",\"defaultCurrency\":\"USD\",\"properties\":[{\"name\":\"Budget\",\"value\":\"1000$\",\"description\":\"Store Budget\",\"custom\":true}],\"associations\":[{\"name\":\"customer\",\"collectionId\":"+properties.getProperty("Customercollectionid")+",\"properties\":[]},{\"name\":\"payment\",\"collectionId\":"+properties.getProperty("Paymentcollectionid")+",\"properties\":[]},{\"name\":\"order\",\"collectionId\":"+properties.getProperty("Ordercollectionid")+",\"properties\":[]}],\"locales\":[\"en_CA\"],\"currencies\":[\"USD\"],\"shippings\":[\"US\"]}",sessionId,"","v8");
		resp = res.get(0);
		System.out.println(resp);
		pass("updateStore "+resp);
		
		JSONObject root = new JSONObject(resp);
		return root; 
		
	}
	
	String orderId="";
	String userId="";
	public JSONObject createCustomerAdminUser() throws JSONException
	{
		String resp = new String();
		String storeId ="";
		List<String> res = new ArrayList<String>();
		if(System.getProperty("suiteXmlFile")!=null) {
	          if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority1.xml"))
	          {
	        	  storeId = properties.getProperty("storeId1");
	          }
	          else if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority2.xml"))
	          {
	        	  storeId =  properties.getProperty("storeId2");
	          }
	          else if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority3.xml"))
	          {
	        	  storeId =  properties.getProperty("storeId3"); 
	          }
	          }
	          else {
	        	  storeId=  properties.getProperty("CustomerStoreId");
	          }
		String emailId = getSaltString()+"@gmail.com";
		long number = generateID();
		System.out.println("RandomEmailId-->"+emailId);
		System.out.println("number--->"+number);
		res=api.ReadAllDataFromUrl(properties.getProperty("APIDomain")+"/userservices/users/?inviteUserFlag=false","{\"users\":[{\"addresses\":[{\"city\":\"CBE\",\"country\":\"IN\",\"county\":\"CBE\",\"createdDate\":\"2018-10-12T11:16:39.076Z\",\"default\":true,\"email\":\"arunskava1@mail.com\",\"firstName\":\"johni\",\"id\":0,\"lastName\":\"smithy\",\"middleName\":\"johni\",\"overriden\":false,\"phone\":1234567891,\"state\":\"TN\",\"street1\":\"tidel1\",\"street2\":\"hopes1\",\"street3\":\"peelamedu1\",\"type\":\"BillingAddreess\",\"updatedDate\":\"2018-10-12T11:16:39.076Z\",\"validated\":false,\"validatedOn\":1,\"validationType\":\"string\",\"zipCode\":641015}],\"credentials\":{\"password\":\"Skava@123\",\"securityQuestions\":[{\"answer\":\"Company name is skava\",\"question\":\"What is your company name ?\"}]},\"customProperties\":{\"segment\":\"gold\"},\"dateOfBirth\":\"08-10-1990\",\"email\":\""+emailId+"\",\"firstName\":\"john\",\"gender\":\"male\",\"lastName\":\"smith\",\"phoneNumber\":\""+number+"\",\"photo\":\"string_url\",\"preferences\":\"email\"}]}",sessionId,properties.getProperty("Customercollectionid"),"","");
		resp = res.get(0);
		System.out.println("createCustomerAdminUser"+resp);
		
		JSONObject root = new JSONObject(resp);
		userId = root.getJSONArray("createdUsers").getJSONObject(0).getString("id");
		properties.put("customerUserId", userId);
		System.out.println("customerUserId"+properties.get("customerUserId"));
		return root; 
		
	}
	
	public JSONObject createOrderForUser() throws JSONException
	{
		String resp = new String();
		String storeId ="";
		List<String> res = new ArrayList<String>();
		if(System.getProperty("suiteXmlFile")!=null) {
	          if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority1.xml"))
	          {
	        	  storeId = properties.getProperty("storeId1");
	          }
	          else if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority2.xml"))
	          {
	        	  storeId =  properties.getProperty("storeId2");
	          }
	          else if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority3.xml"))
	          {
	        	  storeId =  properties.getProperty("storeId3"); 
	          }
	          }
	          else {
	        	  storeId=  properties.getProperty("CustomerStoreId");
	          }
		String emailId = getSaltString()+"@gmail.com";
		long number = generateID();
		System.out.println("RandomEmailId-->"+emailId);
		System.out.println("number--->"+number);
		//res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/oms/orders/","{\"math\":[{\"conversionratio\":0.01,\"cost\":0,\"discount\":-20,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfildiscount\":-2,\"fulfilsale\":20,\"fulfiltax\":0.02,\"totalfulfilsale\":18},\"mode\":\"Currency\",\"personalizationcost\":0,\"personalizationdiscount\":-2,\"personalizationsale\":10,\"roundoff\":0,\"paymentid\":\"123\",\"sale\":800,\"tax\":0.09,\"taxable\":0,\"totalsale\":806,\"unittax\":0},{\"conversionratio\":0.01,\"cost\":0,\"discount\":-8,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfildiscount\":0,\"fulfilsale\":20,\"fulfiltax\":0.02,\"totalfulfilsale\":20},\"mode\":\"Points\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"paymentid\":\"134\",\"sale\":800,\"tax\":0.1,\"taxable\":0,\"totalsale\":812,\"unittax\":0}],\"orderinfo\":{\"affiliateid\":\"string\",\"channel\":\"string\",\"storeid\":"+properties.getProperty("storeId")+"},\"orderitems\":[{\"discounts\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ORDER\",\"mode\":\"Currency\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":-12,\"unitcost\":0,\"unitdiscount\":0}},{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"Points\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":-4,\"unitcost\":0,\"unitdiscount\":0}}],\"fulfilmentinfo\":{\"fulfilmentid\":{\"id\":0,\"status\":\"string\"},\"responseCode\":\"string\",\"responseMessage\":\"string\",\"returnaddress\":{\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"companyname\":\"string\",\"country\":\"string\",\"county\":\"string\",\"deliveryinstruction\":\"string\",\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"responseCode\":\"string\",\"responseMessage\":\"string\",\"state\":\"string\",\"timeStamp\":0,\"type\":\"string\",\"zip\":\"string\"},\"returnmethod\":{\"code\":\"string\",\"deliverystore\":\"string\",\"deliveryterm\":\"string\",\"name\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"priceinfo\":[{\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"string\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"string\",\"price\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0}}]},\"shippingcontact\":{\"email\":\"dkrvdilip@gmail.com\",\"phone\":\"9870543210\",\"preferredcontact\":\"EMAIL\"},\"shippingaddress\":{\"address1\":\"5thstreet\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"NY\",\"companyname\":\"Amazon\",\"country\":\"USA\",\"county\":\"NY\",\"deliveryinstruction\":\"string\",\"firstname\":\"Dilip\",\"lastname\":\"Kumar\",\"middlename\":\"string\",\"responseCode\":\"string\",\"responseMessage\":\"string\",\"state\":\"string\",\"timeStamp\":0,\"type\":\"string\",\"zip\":\"AZ09OP\"},\"shippingmethod\":{\"code\":\"string\",\"deliverystore\":\"string\",\"deliveryterm\":\"string\",\"name\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"priceinfo\":[{\"conversionratio\":0.01,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"Currency\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":-1,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"Currency\",\"price\":10,\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0}},{\"conversionratio\":0.01,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"Points\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"Points\",\"price\":10,\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0}}]},\"timeStamp\":0,\"type\":\"PHYSICAL\"},\"id\":\"Oitem1\",\"inventory\":{\"blocktransaction\":\"string\",\"releasetransaction\":\"string\"},\"item\":{\"binid\":\"12asdfg34\",\"exppreordershipmentdatetime\":0,\"info\":{\"categoryid\":\"CAT01\",\"color\":\"Red\",\"description\":\"Redcolorsportsshoe\",\"dimension\":\"9inch\",\"fit\":\"Flexible\",\"image\":\"https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcRX1vWH4rB69KBISOF7Gz3nudu0FigBkXBmlMS7XJMelgZZ_maTcoOiShVXMub4gioWo8qBMTRgL0R4Yv_MK5hc8LwbOjZMPVfH1_Y_BmXQZx6Z_YP8LRiR&usqp=CAc\",\"link\":\"www.amazon.com\",\"name\":\"Redshoe\",\"size\":\"9inch\",\"style\":\"Flexible\"},\"maxpurchasequantity\":0,\"minpurchasequantity\":0,\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"pid\":\"Pid01\",\"preordered\":false,\"priceinfo\":[{\"conversionratio\":0.01,\"costprice\":0,\"mode\":\"Currency\",\"regprice\":0,\"roundoff\":0,\"saleprice\":200,\"saveprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0}},{\"conversionratio\":0.01,\"costprice\":0,\"mode\":\"Points\",\"regprice\":0,\"roundoff\":0,\"saleprice\":200,\"saveprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0}}],\"skuid\":\"Sku01\",\"taxcode\":\"string\",\"upc\":\"string\",\"vendor\":\"string\",\"weightUnits\":\"string\",\"weightValue\":\"string\"},\"math\":[{\"conversionratio\":0.01,\"paymentid\":\"123\",\"cost\":0,\"discount\":-12,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":0,\"totalfulfilsale\":0,\"fulfiltax\":0},\"mode\":\"Currency\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":400,\"tax\":0.01,\"taxable\":0,\"totalsale\":388,\"unittax\":0},{\"conversionratio\":0.01,\"paymentid\":\"134\",\"cost\":0,\"discount\":-4,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":0,\"totalfulfilsale\":0,\"fulfiltax\":0},\"mode\":\"Points\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":400,\"tax\":0.02,\"taxable\":0,\"totalsale\":396,\"unittax\":0}],\"mathordershipping\":[{\"conversionratio\":0.01,\"paymentid\":\"123\",\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":-1,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":10,\"totalfulfilsale\":9,\"fulfiltax\":0.01},\"mode\":\"Currency\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":0,\"tax\":0.01,\"taxable\":0,\"totalsale\":9,\"unittax\":0},{\"conversionratio\":0.01,\"paymentid\":\"134\",\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":10,\"totalfulfilsale\":10,\"fulfiltax\":0.01},\"mode\":\"Points\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":0,\"tax\":0.01,\"taxable\":0,\"totalsale\":10,\"unittax\":0}],\"mathordertax\":[{\"conversionratio\":0.01,\"paymentid\":\"123\",\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":0,\"totalfulfilsale\":0,\"fulfiltax\":0},\"mode\":\"Currency\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":0,\"tax\":0.03,\"taxable\":0,\"totalsale\":0,\"unittax\":0},{\"conversionratio\":0.01,\"paymentid\":\"134\",\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":0,\"totalfulfilsale\":0,\"fulfiltax\":0},\"mode\":\"Points\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":0,\"tax\":0.02,\"taxable\":0,\"totalsale\":0,\"unittax\":0}],\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"paymentstatus\":\"PREAUTHORIZED\",\"personalization\":[{\"id\":\"string\",\"message\":\"string\",\"priceinfo\":[{\"conversionratio\":0.01,\"discount\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"Currency\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"mode\":\"Currency\",\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalpersonalizationcost\":0,\"totalpersonalizationprice\":0,\"unitpersonalizationcost\":0,\"unitpersonalizationprice\":0}],\"receipt\":false,\"skuid\":\"string\",\"wrap\":\"string\"}],\"priceinfo\":[{\"conversionratio\":0.01,\"costprice\":0,\"mode\":\"Currency\",\"regprice\":0,\"roundoff\":0,\"saleprice\":400,\"saveprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.02,\"taxable\":0,\"taxrate\":0}},{\"conversionratio\":0.01,\"costprice\":0,\"mode\":\"Points\",\"regprice\":0,\"roundoff\":0,\"saleprice\":400,\"saveprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.02,\"taxable\":0,\"taxrate\":0}}],\"quantity\":2,\"returnable\":false,\"status\":\"string\",\"storeid\":\"0\",\"timeStamp\":0,\"type\":\"SKU\"},{\"discounts\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ORDER\",\"mode\":\"Currency\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":-8,\"unitcost\":0,\"unitdiscount\":0}},{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"Points\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":-4,\"unitcost\":0,\"unitdiscount\":0}}],\"fulfilmentinfo\":{\"fulfilmentid\":{\"id\":0,\"status\":\"string\"},\"responseCode\":\"string\",\"responseMessage\":\"string\",\"returnaddress\":{\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"companyname\":\"string\",\"country\":\"string\",\"county\":\"string\",\"deliveryinstruction\":\"string\",\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"responseCode\":\"string\",\"responseMessage\":\"string\",\"state\":\"string\",\"timeStamp\":0,\"type\":\"string\",\"zip\":\"string\"},\"returnmethod\":{\"code\":\"string\",\"deliverystore\":\"string\",\"deliveryterm\":\"string\",\"name\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"priceinfo\":[{\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"string\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"string\",\"price\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0}}]},\"shippingcontact\":{\"email\":\"dkrvdilip@gmail.com\",\"phone\":\"9870543210\",\"preferredcontact\":\"EMAIL\"},\"shippingaddress\":{\"address1\":\"5thstreet\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"NY\",\"companyname\":\"Amazon\",\"country\":\"USA\",\"county\":\"NY\",\"deliveryinstruction\":\"string\",\"firstname\":\"Dilip\",\"lastname\":\"Kumar\",\"middlename\":\"string\",\"responseCode\":\"string\",\"responseMessage\":\"string\",\"state\":\"string\",\"timeStamp\":0,\"type\":\"string\",\"zip\":\"AZ09OP\"},\"shippingmethod\":{\"code\":\"string\",\"deliverystore\":\"string\",\"deliveryterm\":\"string\",\"name\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"priceinfo\":[{\"conversionratio\":0.01,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"Currency\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":-1,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"Currency\",\"price\":10,\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0}},{\"conversionratio\":0.01,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"Points\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"Points\",\"price\":10,\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0}}]},\"timeStamp\":0,\"type\":\"PHYSICAL\"},\"id\":\"Oitem2\",\"inventory\":{\"blocktransaction\":\"string\",\"releasetransaction\":\"string\"},\"item\":{\"binid\":\"12asdfg34\",\"exppreordershipmentdatetime\":0,\"info\":{\"categoryid\":\"CAT01\",\"color\":\"Red\",\"description\":\"Redcolorsportsshoe\",\"dimension\":\"9inch\",\"fit\":\"Flexible\",\"image\":\"https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcRX1vWH4rB69KBISOF7Gz3nudu0FigBkXBmlMS7XJMelgZZ_maTcoOiShVXMub4gioWo8qBMTRgL0R4Yv_MK5hc8LwbOjZMPVfH1_Y_BmXQZx6Z_YP8LRiR&usqp=CAc\",\"link\":\"www.amazon.com\",\"name\":\"Redshoe\",\"size\":\"9inch\",\"style\":\"Flexible\"},\"maxpurchasequantity\":0,\"minpurchasequantity\":0,\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"pid\":\"Pid02\",\"preordered\":false,\"priceinfo\":[{\"conversionratio\":0.01,\"costprice\":0,\"mode\":\"Currency\",\"regprice\":0,\"roundoff\":0,\"saleprice\":200,\"saveprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0}},{\"conversionratio\":0.01,\"costprice\":0,\"mode\":\"Points\",\"regprice\":0,\"roundoff\":0,\"saleprice\":200,\"saveprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0}}],\"skuid\":\"Sku02\",\"taxcode\":\"string\",\"upc\":\"string\",\"vendor\":\"string\",\"weightUnits\":\"string\",\"weightValue\":\"string\"},\"math\":[{\"conversionratio\":0.01,\"paymentid\":\"123\",\"cost\":0,\"discount\":-8,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":0,\"totalfulfilsale\":0,\"fulfiltax\":0},\"mode\":\"Currency\",\"personalizationcost\":0,\"personalizationdiscount\":-2,\"personalizationsale\":10,\"roundoff\":0,\"sale\":400,\"tax\":0.01,\"taxable\":0,\"totalsale\":400,\"unittax\":0},{\"conversionratio\":0.01,\"paymentid\":\"134\",\"cost\":0,\"discount\":-4,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":0,\"totalfulfilsale\":0,\"fulfiltax\":0},\"mode\":\"Points\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":400,\"tax\":0.02,\"taxable\":0,\"totalsale\":396,\"unittax\":0}],\"mathordershipping\":[{\"conversionratio\":0.01,\"paymentid\":\"123\",\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":-1,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":10,\"totalfulfilsale\":9,\"fulfiltax\":0.01},\"mode\":\"Currency\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":0,\"tax\":0.01,\"taxable\":0,\"totalsale\":9,\"unittax\":0},{\"conversionratio\":0.01,\"paymentid\":\"134\",\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":10,\"totalfulfilsale\":10,\"fulfiltax\":0.01},\"mode\":\"Points\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":0,\"tax\":0.01,\"taxable\":0,\"totalsale\":10,\"unittax\":0}],\"mathordertax\":[{\"conversionratio\":0.01,\"paymentid\":\"123\",\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":0,\"totalfulfilsale\":0,\"fulfiltax\":0},\"mode\":\"Currency\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":0,\"tax\":0.02,\"taxable\":0,\"totalsale\":0,\"unittax\":0},{\"conversionratio\":0.01,\"paymentid\":\"134\",\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":0,\"totalfulfilsale\":0,\"fulfiltax\":0},\"mode\":\"Points\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":0,\"tax\":0.02,\"taxable\":0,\"totalsale\":0,\"unittax\":0}],\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"paymentstatus\":\"PREAUTHORIZED\",\"personalization\":[{\"id\":\"string\",\"message\":\"string\",\"priceinfo\":[{\"conversionratio\":0.01,\"discount\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"Currency\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":-2,\"unitcost\":0,\"unitdiscount\":0}}],\"mode\":\"Currency\",\"taxinfo\":{\"esttax\":0,\"tax\":0.01,\"taxable\":0,\"taxrate\":0},\"totalpersonalizationcost\":0,\"totalpersonalizationprice\":10,\"unitpersonalizationcost\":0,\"unitpersonalizationprice\":0}],\"receipt\":false,\"skuid\":\"string\",\"wrap\":\"string\"}],\"priceinfo\":[{\"conversionratio\":0.01,\"costprice\":0,\"mode\":\"Currency\",\"regprice\":0,\"roundoff\":0,\"saleprice\":400,\"saveprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.02,\"taxable\":0,\"taxrate\":0}},{\"conversionratio\":0.01,\"costprice\":0,\"mode\":\"Points\",\"regprice\":0,\"roundoff\":0,\"saleprice\":400,\"saveprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0.02,\"taxable\":0,\"taxrate\":0}}],\"quantity\":2,\"returnable\":true,\"status\":\"string\",\"storeid\":\"0\",\"timeStamp\":0,\"type\":\"SKU\"}],\"orderdetails\":{\"discounts\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ORDER\",\"mode\":\"Currency\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":-20,\"unitcost\":0,\"unitdiscount\":0}}],\"fulfilmentinfo\":{\"fulfilmentid\":{\"id\":0,\"status\":\"string\"},\"responseCode\":\"string\",\"responseMessage\":\"string\",\"returnaddress\":{\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"companyname\":\"string\",\"country\":\"string\",\"county\":\"string\",\"deliveryinstruction\":\"string\",\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"responseCode\":\"string\",\"responseMessage\":\"string\",\"state\":\"string\",\"timeStamp\":0,\"type\":\"string\",\"zip\":\"string\"},\"returnmethod\":{\"code\":\"string\",\"deliverystore\":\"string\",\"deliveryterm\":\"string\",\"name\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"priceinfo\":[{\"conversionratio\":0,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"string\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"string\",\"price\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0}}]},\"shippingaddress\":{\"address1\":\"250street\",\"address2\":\"6thmile\",\"address3\":\"2ndcut\",\"city\":\"Newyork\",\"companyname\":\"skava\",\"country\":\"US\",\"county\":\"NY\",\"deliveryinstruction\":\"string\",\"firstname\":\"dilip\",\"lastname\":\"kumar\",\"middlename\":\"string\",\"responseCode\":\"string\",\"responseMessage\":\"string\",\"state\":\"NY\",\"timeStamp\":0,\"type\":\"string\",\"zip\":\"MXV5CL0\"},\"shippingcontact\":{\"email\":\"dkrvdilip@gmail.com\",\"phone\":\"+91-9087654321\",\"preferredcontact\":\"EMAIL\"},\"shippingmethod\":{\"code\":\"string\",\"deliverystore\":\"string\",\"deliveryterm\":\"string\",\"name\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"priceinfo\":[{\"conversionratio\":0.01,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ORDER\",\"mode\":\"Currency\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":-2,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"Currency\",\"price\":20,\"taxinfo\":{\"esttax\":0,\"tax\":0.02,\"taxable\":0,\"taxrate\":0}},{\"conversionratio\":0.01,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ORDER\",\"mode\":\"Points\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"Points\",\"price\":20,\"taxinfo\":{\"esttax\":0,\"tax\":0.02,\"taxable\":0,\"taxrate\":0}}]},\"timeStamp\":0,\"type\":\"PHYSICAL\"},\"ordertaxinfo\":[{\"mode\":\"Currency\",\"taxcode\":\"string\",\"taxinfo\":{\"esttax\":0,\"tax\":0.05,\"taxable\":0,\"taxrate\":0}},{\"mode\":\"Points\",\"taxcode\":\"string\",\"taxinfo\":{\"esttax\":0,\"tax\":0.04,\"taxable\":0,\"taxrate\":0}}]},\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"payments\":[{\"id\":\"123\",\"chargepriority\":0,\"format\":\"0\",\"mode\":\"Currency\",\"paymentitems\":[{\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"paymentrefid\":100,\"status\":\"PREAUTHORIZED\",\"value\":806.09}],\"preauthorizations\":[],\"refundpriority\":0,\"type\":\"string\",\"value\":806.09},{\"id\":\"134\",\"chargepriority\":0,\"format\":\"0\",\"mode\":\"Points\",\"paymentitems\":[{\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"paymentrefid\":100,\"status\":\"PREAUTHORIZED\",\"value\":812.1}],\"preauthorizations\":[],\"refundpriority\":0,\"type\":\"string\",\"value\":812.1}],\"status\":[\"string\"],\"usertracking\":{\"accountid\":\"A01\",\"firstname\":\"Dilip\",\"guestuser\":\"false\",\"guestuserid\":\"\",\"lastname\":\"Kumar\",\"usercontact\":{\"email\":\"dilipkumar@skava.com\",\"phone\":\"9600904325\",\"preferredcontact\":\"EMAIL\"},\"userid\":"+properties.getProperty("customerUserId")+",\"zip\":\"AB001B\"}}",sessionId,properties.getProperty("Ordercollectionid"));
		res=api.ReadAllDataFromUrl(properties.getProperty("APIDomain")+"/omsservices/orders/","{\"math\":[{\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":1,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfildiscount\":0,\"fulfilsale\":1,\"fulfiltax\":0,\"totalfulfilsale\":1},\"mode\":\"creditcard\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"paymentid\":\"string\",\"sale\":10000,\"tax\":1,\"taxable\":0,\"totalsale\":10001,\"unittax\":0}],\"orderinfo\":{\"affiliateid\":\"string\",\"channel\":\"string\",\"storeid\":\""+storeId+"\"},\"orderitems\":[{\"discounts\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"creditcard\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"fulfilmentinfo\":{\"fulfilmentid\":{\"id\":0,\"status\":\"string\"},\"returnaddress\":{\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"companyname\":\"string\",\"country\":\"string\",\"county\":\"string\",\"deliveryinstruction\":\"string\",\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"state\":\"string\",\"type\":\"string\",\"zip\":\"string\"},\"returnmethod\":{\"code\":\"string\",\"deliverystore\":\"string\",\"deliveryterm\":\"string\",\"name\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"priceinfo\":[{\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"string\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"string\",\"price\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"conversionratio\":1}]},\"shippingcontact\":{\"email\":\"sowmya@skava.com\",\"phone\":\"string\",\"preferredcontact\":\"EMAIL\"},\"shippingaddress\":{\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"companyname\":\"string\",\"country\":\"string\",\"county\":\"string\",\"deliveryinstruction\":\"string\",\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"state\":\"string\",\"type\":\"string\",\"zip\":\"string\"},\"shippingmethod\":{\"code\":\"string\",\"deliverystore\":\"string\",\"deliveryterm\":\"string\",\"name\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"priceinfo\":[{\"conversionratio\":1,\"cost\":0,\"discountinfo\":[{\"confirmationcode\":\"string\",\"code\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"creditcard\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"creditcard\",\"price\":1,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0}}]},\"type\":\"PHYSICAL\"},\"id\":\"string\",\"inventory\":{\"blocktransaction\":\"string\",\"releasetransaction\":\"string\"},\"item\":{\"binid\":\"string\",\"exppreordershipmentdatetime\":0,\"info\":{\"categoryid\":\"string\",\"color\":\"string\",\"description\":\"string\",\"dimension\":\"string\",\"fit\":\"string\",\"image\":\"string\",\"link\":\"string\",\"name\":\"string\",\"size\":\"string\",\"style\":\"string\"},\"maxpurchasequantity\":0,\"minpurchasequantity\":0,\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"pid\":\"string\",\"preordered\":false,\"priceinfo\":[{\"conversionratio\":1,\"costprice\":0,\"mode\":\"creditcard\",\"regprice\":0,\"roundoff\":0,\"saleprice\":10000,\"saveprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0}}],\"skuid\":\"string\",\"taxcode\":\"string\",\"upc\":\"string\",\"vendor\":\"string\",\"weightUnits\":\"string\",\"weightValue\":\"string\"},\"math\":[{\"paymentid\":\"string\",\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":0,\"totalfulfilsale\":0,\"fulfiltax\":0},\"mode\":\"creditcard\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":10000,\"tax\":0,\"taxable\":0,\"totalsale\":10000,\"unittax\":0}],\"mathordershipping\":[{\"paymentid\":\"string\",\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":1,\"totalfulfilsale\":1,\"fulfiltax\":0},\"mode\":\"creditcard\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":0,\"tax\":0,\"taxable\":0,\"totalsale\":1,\"unittax\":0}],\"mathordertax\":[{\"paymentid\":\"string\",\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":1,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"fulfildiscount\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfilsale\":0,\"totalfulfilsale\":0,\"fulfiltax\":0},\"mode\":\"creditcard\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":0,\"tax\":1,\"taxable\":0,\"totalsale\":0,\"unittax\":0}],\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"paymentstatus\":\"PREAUTHORIZED\",\"personalization\":[{\"id\":\"string\",\"message\":\"string\",\"priceinfo\":[{\"conversionratio\":1,\"discount\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"creditcard\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"mode\":\"creditcard\",\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalpersonalizationcost\":0,\"totalpersonalizationprice\":0,\"unitpersonalizationcost\":0,\"unitpersonalizationprice\":0}],\"receipt\":false,\"skuid\":\"string\",\"wrap\":\"string\"}],\"priceinfo\":[{\"conversionratio\":1,\"costprice\":0,\"mode\":\"creditcard\",\"regprice\":0,\"roundoff\":0,\"saleprice\":10000,\"saveprice\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0}}],\"quantity\":1,\"returnable\":false,\"status\":\"string\",\"storeid\":\"string\",\"type\":\"SKU\"}],\"orderdetails\":{\"fulfilmentinfo\":{\"fulfilmentid\":{\"id\":0,\"status\":\"string\"},\"returnaddress\":{\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"companyname\":\"string\",\"country\":\"string\",\"county\":\"string\",\"deliveryinstruction\":\"string\",\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"state\":\"string\",\"type\":\"string\",\"zip\":\"string\"},\"returnmethod\":{\"code\":\"string\",\"deliverystore\":\"string\",\"deliveryterm\":\"string\",\"name\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"priceinfo\":[{\"conversionratio\":1,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"string\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"string\",\"price\":0,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0}}]},\"shippingaddress\":{\"address1\":\"string\",\"address2\":\"string\",\"address3\":\"string\",\"city\":\"string\",\"companyname\":\"string\",\"country\":\"string\",\"county\":\"string\",\"deliveryinstruction\":\"string\",\"firstname\":\"string\",\"lastname\":\"string\",\"middlename\":\"string\",\"state\":\"string\",\"type\":\"string\",\"zip\":\"string\"},\"shippingcontact\":{\"email\":\"sowmya@skava.com\",\"phone\":\"string\",\"preferredcontact\":\"EMAIL\"},\"shippingmethod\":{\"code\":\"string\",\"deliverystore\":\"string\",\"deliveryterm\":\"string\",\"name\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"priceinfo\":[{\"conversionratio\":1,\"cost\":0,\"discountinfo\":[{\"code\":\"string\",\"confirmationcode\":\"string\",\"description\":\"string\",\"discountclass\":\"string\",\"level\":\"ITEM\",\"mode\":\"creditcard\",\"name\":\"string\",\"offerid\":\"string\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"parentitem\":\"string\",\"priceinfo\":{\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0},\"totalcost\":0,\"totaldiscount\":0,\"unitcost\":0,\"unitdiscount\":0}}],\"estcost\":0,\"estprice\":0,\"mode\":\"creditcard\",\"price\":1,\"taxinfo\":{\"esttax\":0,\"tax\":0,\"taxable\":0,\"taxrate\":0}}]},\"type\":\"PHYSICAL\"},\"ordertaxinfo\":[{\"mode\":\"creditcard\",\"taxcode\":\"string\",\"taxinfo\":{\"esttax\":1,\"tax\":1,\"taxable\":0,\"taxrate\":0}}]},\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"payments\":[{\"id\":\"string\",\"chargepriority\":0,\"format\":\"string\",\"mode\":\"creditcard\",\"paymentitems\":[{\"id\":\"id123\",\"otherproperties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"paymentrefid\":\"123\",\"status\":\"PREAUTHORIZED\",\"value\":10002}],\"preauthorizations\":[{\"math\":{\"cost\":0,\"discount\":0,\"discountcost\":0,\"esttax\":0,\"estunittax\":0,\"fulfilmath\":{\"estfulfilcost\":0,\"estfulfilsale\":0,\"estfulfiltax\":0,\"fulfilcost\":0,\"fulfildiscount\":0,\"fulfilsale\":0,\"fulfiltax\":0,\"totalfulfilsale\":0},\"mode\":\"string\",\"personalizationcost\":0,\"personalizationdiscount\":0,\"personalizationsale\":0,\"roundoff\":0,\"sale\":0,\"tax\":0,\"taxable\":0,\"totalsale\":0,\"unittax\":0}}],\"refundpriority\":0,\"type\":\"string\",\"value\":10002}],\"status\":[\"string\"],\"usertracking\":{\"accountid\":\"string\",\"firstname\":\"string\",\"guestuser\":false,\"guestuserid\":\"\",\"lastname\":\"string\",\"usercontact\":{\"email\":\"sowmya@skava.com\",\"phone\":\"string\",\"preferredcontact\":\"EMAIL\"},\"userid\":\""+properties.getProperty("customerUserId")+"\",\"zip\":\"string\"}}","",properties.getProperty("Ordercollectionid"),"",orderauthToken);
		resp = res.get(0);
		System.out.println(resp);
		
		JSONObject root = new JSONObject(resp);
		orderId = root.getString("id");
		properties.put("customerOrderId", orderId);
		System.out.println("customerOrderId"+properties.get("customerOrderId"));
		return root; 
		
	}
	
	public void createUserAndOrder() throws JSONException{
		for(int i=1; i<=8;i++) {
			createCustomerAdminUser();
			//cacheClear();
			for(int j=1; j<=5;j++) {
			createOrderForUser();
			}
			
		}
	}
	
	
	public Date getTimeStamp()
    {
    	Date date = null;
    	try
    	{
    		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'SSS", Locale.US);
    		format.setTimeZone(TimeZone.getTimeZone("UTC"));
    		date = new Date();
    		currentUTCTime = format.format(date);
    	}
    	catch (Exception e) 
    	{
    		logFail("There is some issue in getting the current time stamp.");
		}
    	return date;
    }
	
	public String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
	
	
	public long generateID() { 
	    Random rnd = new Random();
	    char [] digits = new char[11];
	    digits[0] = (char) (rnd.nextInt(9) + '1');
	    for(int i=1; i<digits.length; i++) {
	        digits[i] = (char) (rnd.nextInt(10) + '0');
	    }
	    return Long.parseLong(new String(digits));
	    
	}
	
	String Subcollectionid="";
	
	public JSONObject createCollectionSubscription(String collectionName) throws JSONException
	{
		String resp = new String();
		List<String> res = new ArrayList<String>();
		//res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("businessId")+"&serviceName=subscription","{ \"name\": \""+collectionName+"\", \"properties\": [], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId,"","","");
		res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("businessId")+"&serviceName=subscription","{\"name\":\""+collectionName+"\",\"properties\":[{\"name\":\"calendarType\",\"value\":\"NORMAL\"},{\"name\":\"dailyFrequencyMax\",\"value\":\"90\"},{\"name\":\"dailyFrequencyMin\",\"value\":\"1\"},{\"name\":\"defaultLocale\",\"value\":\"en_US\"},{\"name\":\"intervalToNotifyUser\",\"value\":\"2\"},{\"name\":\"intervalToProcessFirstOrder\",\"value\":\"5\"},{\"name\":\"intervalToProcessForthComingOrder\",\"value\":\"5\"},{\"name\":\"intervalType\",\"value\":\"MONTHS\"},{\"name\":\"monthlyFrequencyMax\",\"value\":\"12\"},{\"name\":\"monthlyFrequencyMin\",\"value\":\"1\"},{\"name\":\"weeklyFrequencyMax\",\"value\":\"12\"},{\"name\":\"weeklyFrequencyMin\",\"value\":\"1\"},{\"name\":\"yearlyFrequencyMax\",\"value\":\"2\"},{\"name\":\"yearlyFrequencyMin\",\"value\":\"1\"}],\"description\":\"SubscriptionDec6YCs1Z291565782535021\",\"status\":\"ACTIVE\"}",sessionId,"","","");
		resp = res.get(0);
		System.out.println(resp);
		logInfo("createCollectionSubscription: "+resp);
		JSONObject root = new JSONObject(resp);
		Subcollectionid = root.getJSONObject("collection").getString("id");
		properties.put("Subscriptioncollectionid", Subcollectionid);
		String url=properties.getProperty("ApplicationUrl");
		url=url.replace("<<collectionId>>", Subcollectionid);
		properties.put("ApplicationUrl",url);
		System.out.println("SubscriptionCollectionUrl-->"+url);		
		System.out.println("Subscriptioncollectionid"+Subcollectionid);
		System.out.println("Subscriptioncollectionid--->"+properties.get("Subscriptioncollectionid"));
		return root; 
		
	}
	
	
	
	 public JSONObject createSubscription(String skuName, String skuId) throws JSONException
		{
			String resp = new String();
			String storeId ="";
			List<String> res = new ArrayList<String>();
			if(System.getProperty("suiteXmlFile")!=null) {
		          if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority1.xml"))
		          {
		        	  storeId = properties.getProperty("storeId1");
		          }
		          else if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority2.xml"))
		          {
		        	  storeId =  properties.getProperty("storeId1");
		          }
		          else if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority3.xml"))
		          {
		        	  storeId =  properties.getProperty("storeId1"); 
		          }
		          else if(System.getProperty("suiteXmlFile").equalsIgnoreCase("deploymenttest.xml"))
		          {
		        	  storeId =  properties.getProperty("storeId1"); 
		          }
		          }
		          else {
		        	  storeId=  properties.getProperty("storeId1");
		          }
			//res=api.ReadAllDataFromUrl(properties.getProperty("Domain")+"/admin/services/collections/?businessId="+properties.getProperty("businessId")+"&serviceName=subscription","{ \"name\": \""+collectionName+"\", \"properties\": [], \"description\": \"collection description\", \"status\": \"ACTIVE\"}",sessionId,"","","");
			res=api.ReadAllDataFromUrl(properties.getProperty("APIDomain")+"/subscriptionservices/subscriptions?locale=en_US","{\"accountId\":1001,\"addressInfo\":{\"addressLine1\":\"string\",\"addressLine2\":\"string\",\"addressLine3\":\"string\",\"addressVersion\":0,\"canReceiveSMS\":false,\"city\":\"string\",\"companyName\":\"string\",\"country\":\"string\",\"county\":\"string\",\"email\":\"superadmin@skava.com\",\"firstName\":\"Superadmin\",\"lastName\":\"Skava\",\"middleName\":\"string\",\"mobile\":\"string\",\"phone\":\"string\",\"profileAddressId\":\"string\",\"state\":\"string\",\"storeId\":\"string\",\"zip\":\"string\"},\"firstDeliveryDate\":1565825133155,\"frequencyEndDate\":1567825133155,\"frequencyEndType\":\"NEVER\",\"frequencyPeriod\":1,\"frequencyStartDate\":1564725133155,\"frequencyType\":\"MONTHS\",\"itemInfo\":[{\"preparationPeriod\":1,\"price\":0,\"properties\":{\"name\":\"Superadmin\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"quantity\":2,\"skus\":[{\"id\":\""+skuId+"\",\"name\":\""+skuName+"\",\"optionalQuantity\":0,\"productId\":\"string\",\"type\":\"DEFAULT\"}]}],\"paymentInfo\":{\"name\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"properties\":{\"additionalProp1\":\"string\",\"additionalProp2\":\"string\",\"additionalProp3\":\"string\"},\"shippingInfo\":{\"deliveryPeriod\":1,\"instruction\":\"string\",\"method\":\"string\",\"type\":\"string\"},\"storeId\":\""+storeId+"\",\"type\":\"VARIABLE_MULTI_ORDER\"}","",Subcollectionid,"8.4.0",orderauthToken,"1");
			resp = res.get(0);
			System.out.println(resp);
			logInfo("createSubscription: "+resp);
			JSONObject root = new JSONObject(resp);
			return root; 
			
		}
	 
	 
	 
	 public void createMultiSubscription() throws JSONException{
			for(int i=1; i<=12;i++) {
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
				createSubscription("Mens_Collections"+timeStamp+driver.generateRandomNumber(9999),timeStamp+driver.generateRandomNumber(9999));
			}
		}
	
	

}
	



