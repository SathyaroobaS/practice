package com.skava.reusable.components;



import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.orderDetailPage;
import com.skava.object.repository.orderListingPage;

public class OrderDetailPageComponents extends OrderListingPageComponents 
{
	public void orderIdDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.orderId, "Order Id"))
			logPass("OrderId is displayed");
		else
			logFail("OrderId is not displayed");
	}
	
	public void orderDateDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.orderDate, "OrderDate"))
			logPass("OrderDate is displayed");
		else
			logFail("OrderDate is not displayed");
	}
	
	public void orderValueDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.orderValue, "OrderValue"))
			logPass("OrderValue is displayed");
		else
			logFail("OrderValue is not displayed");
	}
	
	public void customerInfoDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.customerInfo, "Customer Info"))
			logPass("CustomerInfo is displayed");
		else
			logFail("CustomerInfo is not displayed");
	}
	
	public void customerNameDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.customerName, "Customer Name"))
		{
			String cusName = driver.getText(orderDetailPage.customerName, "Customer Name");
			logPass("CustomerName is displayed :" +cusName);
		
		}
		
		else
			logFail("CustomerName is not displayed");
	}
	
	public void customerEmailDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.custInfoEmail, "Customer Email"))
		{
			String cusEmail = driver.getText(orderDetailPage.custInfoEmail, "Customer Email");
			logPass("CustomerEmail is displayed :" +cusEmail);
		}
		else
			logFail("CustomerEmail is not displayed");
	}
	
	public void customerPhoneDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.custInfoPhone, "Customer Phone"))
		{
			String cusPhone = driver.getText(orderDetailPage.custInfoPhone, "Customer Phone");
			logPass("CustomerPhone is displayed :" +cusPhone);
		}
		else
			logFail("CustomerPhone is not displayed");
	}
	
	public void paymentInfoDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.paymentInfo, "paymentInfo"))
			logPass("PaymentInfo is displayed");
		else
			logFail("PaymentInfo is not displayed");
	}
	
	public void taxDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.tax, "paymentTax")) {
			String tax = driver.getText(orderDetailPage.tax, "Payment tax");
			logPass("PaymentTax is displayed :" +tax);
		}
		else
			logFail("PaymentTax is not displayed");
	}
	
	public void orderSubTotalDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.orderSubTot, "OrderSubTotal")) {
			String subTotal = driver.getText(orderDetailPage.orderSubTot, "Order Sub Total");
			
			logPass("OrderSubTotal is displayed :" +subTotal);
		}
		else
			logFail("OrderSubTotal is not displayed");
	}
	
	public void shippingDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.shipping, "Shipping"))
		{
			String shipDi = driver.getText(orderDetailPage.shipping, "Shipping address");
			logPass("Shipping is displayed :" +shipDi);
		}
		else
			logFail("Shipping is not displayed");
	}
	
	public void discountDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.discount, "Discount"))
		{
			String discount = driver.getText(orderDetailPage.discount, "Discount");
			logPass("Discount is displayed :" +discount);
		}
		else
			logFail("Discount is not displayed");
	}
	
	public void orderGrandTotDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.orderGrandTot, "OrderGrandTot"))
		{
			String total = driver.getText(orderDetailPage.orderGrandTot, "Grand Total");
			logPass("OrderGrandTot is displayed :" +total);
		}
		
		else
			logFail("OrderGrandTot is not displayed");
	}
	
	public void payeeNameDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.payeeName, "PayeeName"))
			logPass("PayeeName is displayed");
		else
			logFail("PayeeName is not displayed");
	}
	
	public void cardDetailDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.cardDet, "CardDet"))
			logPass("CardDet is displayed");
		else
			logFail("CardDet is not displayed");
	}
	
	public void expDateDisplayed()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.expDate, "ExpDate"))
			logPass("ExpDate is displayed");
		else
			logFail("ExpDate is not displayed");
	}
	
	public void itemNameDisplayed()
	{
		if(driver.isElementDisplayed(orderDetailPage.itemName, "Item Name"))
			logPass("Item Name is displayed");
		else
			logFail("item Name is not displayed");
	}
	
	public void itemImgDisplayed()
	{
		if(driver.isElementDisplayed(orderDetailPage.itemImg, "Item Img"))
			logPass("Item Image is displayed");
		else
			logFail("item Image is not displayed");
	}
	
	public void itemDescDisplayed()
	{
		if(driver.isElementDisplayed(orderDetailPage.itemDesc, "Item Desc"))
			logPass("Item Desc is displayed");
		else
			logFail("item Desc is not displayed");
	}
	
	public void itemQtyDisplayed()
	{
		if(driver.isElementDisplayed(orderDetailPage.itemQty, "Item Qty"))
			logPass("Item Qty is displayed");
		else
			logFail("item Qty is not displayed");
	}
	
	public void itemSummaryDisplayed()
	{
		if(driver.isElementDisplayed(orderDetailPage.itemSummary, "Item Summary"))
			logPass("Item Summary is displayed");
		else
			logFail("item Summary is not displayed");
	}
	
	public void itemSubTotDisplayed()
	{
		if(driver.isElementDisplayed(orderDetailPage.itemSubTot, "Item SubTot"))
			logPass("Item SubTot is displayed");
		else
			logFail("item SubTot is not displayed");
	}
	
	public void itemTaxDisplayed()
	{
		if(driver.isElementDisplayed(orderDetailPage.itemTax, "Item Tax"))
			logPass("Item Tax is displayed");
		else
			logFail("item Tax is not displayed");
	}
	
	public void itemGrandTotDisplayed()
	{
		if(driver.isElementDisplayed(orderDetailPage.itemGrandTot, "Item GrandTot"))
			logPass("Item GrandTot is displayed");
		else
			logFail("item GrandTot is not displayed");
	}
	
	public void orderDetBreadCrumDisplayed()
	{
		if(driver.isElementDisplayed(orderDetailPage.orderSummaryBreadCrum, "OrderDet BreadCrum"))
			logPass("OrderDet BreadCrum is displayed");
		else
			logFail("OrderDet BreadCrum is not displayed");
	}
	
	public void verifyCreditCardDigit()
	{
		driver.clickElement(By.xpath("//*[@data-qa='view-payment-details-trigger']"), "payment details");
		int count = 0;
		String str = driver.getText(By.xpath("(//*[@data-qa='description-value'])//span[3]"), "Credit Card Detail");
		
		 for(int i = 0; i < str.length() ; i++)
		 {
			 if( Character.isDigit(str.charAt(i)) )
				 count++;
	     }
	            
		if(count == 4)
			logPass("Credit card digit is 4");
		else
			logFail("Credit card digit is not 4");
	}
	
	public void verifyExpDateFormat()
	{
		driver.clickElement(By.xpath("//*[@data-qa='view-payment-details-trigger']"), "payment details");
		String expDate = driver.getText(By.xpath("(//*[@data-qa='description-value'])//span[5]"), "ExpDate");
		String res = expDate.replaceAll("[^0-9/]","");
		if(res.indexOf("/") != -1)
		{
			logPass("Date has slash"+res);
			String month = res.split("/")[0];
			String year = res.split("/")[1];
			if(month.length() == 2 && year.length() == 4)
				logPass("ExpDate is displayed in MM/YY formate");
			else
				logFail("ExpDate is not displayed in MM/YY formate");
		}
		else
			logFail("ExpDate is not displayed in MM/YY formate");
	}
	public void clickPaymentInformtion()
	{
		if(driver.isElementDisplayed(By.xpath("//*[@data-qa='view-payment-details-trigger']"), "View Page Pop-up"))
		{
			driver.jsClickElement(By.xpath("//*[@data-qa='view-payment-details-trigger']"), "Click View Page");
			driver.isElementDisplayed(By.xpath("//*[@data-qa='payment-details-title']"), "Payment details");
			logPass("Payment pop-up is clicked");
		}
	   logFail("logFailed to display payment popup option");
	}
	
	public void verifyBillingAddress()
	{
		driver.isElementDisplayed(By.xpath("//*[@data-qa='billing-address-title']"), "Billing Address");
		logPass("Billing Address is displayed");
	}
	
	
	
	public void verifyOrderIDinDetailPage()
	{
		if(driver.getCurrentUrl().contains(properties.getProperty("orderId")))
		{
			String orderID = driver.getText(orderDetailPage.orderIDText, "Order ID elements");
			if(orderID.contains(properties.getProperty("orderId")))
			{
				logPass("Order id is displayed : " +orderID);
			}
		}
		else
			logFail("Order id not displayed");
	}
	
	public void orderPlacedDate()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.orderDateTime, "Order Place Date"))
		{
			String orderDate = driver.getText(orderDetailPage.orderDate, "Place Order date");
			if(orderDate.isEmpty()) {
				logFail("OrderDate is not displayed");
			}
			else
				logPass("Place Order Date is :" +orderDate);
		}
	}
	
	public void viewPaymentPopUp()
	{
	if(driver.explicitWaitforVisibility(orderDetailPage.viewPaymenttab, "Payent Tab"))
	{
			driver.jsClickElement(orderDetailPage.viewPaymenttab, "trigger paymenttab");
			
		if(driver.explicitWaitforVisibility(orderDetailPage.paymentdatatable, "Payment datatable displayed")) {
			logPass("payment pop-up get displayed");
		}
		else {
			logFail("payment pop-up not displayed");
		}
		
		
		if(driver.explicitWaitforVisibility(orderDetailPage.paymentPopupcard, "Payment card get displayed")) {
			logPass("Payment card get displayed");
		}
		else {
			logFail("Payment card not displayed");
		}
		
		if(driver.explicitWaitforVisibility(orderDetailPage.paymentPopupbilladd, "Payment billing address get displayed")) {
			logPass("Payment billing address get displayed");
		}
		else {
			logFail("Payment billing address not displayed");
		}
		
		if(driver.explicitWaitforVisibility(orderDetailPage.paymentPopupamt, "Payment amount get displayed")) {
			logPass("Payment amount get displayed");
		}
		else {
			logFail("Payment amount not displayed");
		}
		
		if(driver.explicitWaitforVisibility(orderDetailPage.paymentPopupdesc, "Payment description get displayed")) {
			logPass("Payment description get displayed");
		}
		else {
			logFail("Payment description not displayed");
		}
					
	}
	}
	
	public void verifyBreadcrumb()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.breadCrumb, "Order Detail in Breadcrumb"))
		{
			String storeName = driver.getText(orderDetailPage.breadCrumb, "Store Detail Name");
			logPass("Displayed name in Breadcrumb is :" +storeName);
		}
		else
		logFail("Store name not displayed in Breadcrumb");
	}
	
	public void orderBreadcrumb()
	{
		if(driver.explicitWaitforVisibility(orderDetailPage.orderBreadCrumb, "Order Details"))
		{
			String orderName = driver.getText(orderDetailPage.orderBreadCrumb, "Order Details");
			logPass("Order Name in Breadcrumb is :" +orderName);
		}
		else
		logFail("Order Name not displayed in Breadcrumb");
	}
	
	
	
}


