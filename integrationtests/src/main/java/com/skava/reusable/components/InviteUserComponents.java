package com.skava.reusable.components;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.ForgotPassword_8;
import com.skava.object.repository.InstanceTeamPage;
import com.skava.object.repository.InviteUserPage;

public class InviteUserComponents extends InstanceTeamComponents{
	
	public String email="";
	
	public void navigateToInviteUsersPage()
	{
		waitForLoaderToBeInvisible();
		driver.explicitWaitforVisibility(InstanceTeamPage.inviteBtn, "Invite Team Members");
		if(driver.clickElement(InstanceTeamPage.inviteBtn, "Invite Team Members"))
		{
			driver.waitForPageLoad(30);
			if(driver.isElementDisplayed(InviteUserPage.emailTxtBox,  "Email"))
				logPass("Navigated to Invite user page");
			else
				logFail("Failed to navigate to invite users page");
		}
	}
	
	
	public void fillAndRemoveEmailAddress()
	{
		 String email="abc@"+driver.generateRandomNumber(999)+"gmail.com,test@"+driver.generateRandomNumber(999)+"gmail.com,ecom@"+driver.generateRandomNumber(999)+"gmail.com";
		
		System.out.println(email);
		
		driver.actionEnterText(InviteUserPage.emailTxtBox,email , "Email");
		driver.actionTabKey(InviteUserPage.emailTxtBox, "Email");
		
		
		if(driver.getSize(InviteUserPage.emailRemoveIcon, "remove")==3)
			logPass("Email address entered");
		else
			logFail("Failed to enter email address");
		
		driver.actionClickElement(InviteUserPage.emailRemoveIcon, "Remove email");
		
		
		if(driver.getSize(InviteUserPage.emailRemoveIcon, "remove")==2)
			logPass("Email address removed");
		else
			logFail("Failed to remove email address");
			
		

		
	}
	
	public void verifyEmailFieldErrorMsg()
	{
		driver.scrollToElement(InviteUserPage.emailEmptyErrorMsg, "email error txt");
		if(driver.getText(InviteUserPage.emailEmptyErrorMsg, "Error msg").equals("Email Address field cannot be blank"))
			logPass("Email error message is displayed");
		else
			logFail("Failed to display email error message");
		
		driver.scrollToElement(InviteUserPage.teamBreadcrumb, "Email text box");
		if(driver.actionEnterText(InviteUserPage.emailTxtBox, "test", "Email text box"))
			logPass("Invalid email is entered");
		else
			logFail("Failed to enter invalid email");
		
		driver.actionTabKey(InviteUserPage.emailTxtBox, "Email");
		clickSendInvite();
		
		
		driver.scrollToElement(InviteUserPage.emailInvalidErrorMsg, "email error txt");
		if(driver.getText(InviteUserPage.emailInvalidErrorMsg, "Error msg").equals("The email address format seems to be invalid. Please check and try again"))
			logPass("Email error message is displayed");
		else
			logFail("Failed to display email error message");
		
	}
	
	public void fillEmailAddress()
	{
		email="testuser"+driver.generateRandomNumber(9999999)+"@gmail.com";
		driver.actionEnterText(InviteUserPage.emailTxtBox, email, "Email text box");
		driver.actionTabKey(InviteUserPage.emailTxtBox, email);
		
		if(driver.getSize(InviteUserPage.emailRemoveIcon, "remove")==1)
			logPass("Email address entered");
		else
			logFail("Failed to enter email address");
	}
	
	public void clickSendInvite()
	{
		driver.scrollToElement(InviteUserPage.sendInviteBtn, "Send invite button");
		if(driver.clickElement(InviteUserPage.sendInviteBtn, "Send invite button"))
			logPass("Invite button is clicked");
		else
			logFail("Failed to click Invite button");
			
	}
	
	public void instanceAdminToggle()
	{
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(driver.jsClickElement(InviteUserPage.instanceAdminBtn, "Instance admin button"))
			logPass("Instance admin toggle clicked");
		else
			logFail("Failed to click Insatnce admin toggle");
	}
	
	public void verifyInviteSuccessMsg()
	{
		driver.waitForPageLoad(20);
		waitForLoaderToBeInvisible();
		if(driver.getText(InviteUserPage.inviteSuccessMsg, "Invite success message").equals("You have successfully invited the user(s)!"))
			logPass("Invite Success message is displayed");
		else
			logFail("Failed to display invite success message");
	}
	
	public void clickBusiness()
	{	
		
		if(driver.jsClickElement(InviteUserPage.businessCheckbox, "business checkbox"))
			logPass("Business is selected");
		else
			logFail("Failed to select business");
	}
	
	public void clickBusinessAdminToggle()
	{
		if(driver.jsClickElement(InviteUserPage.businessAdminToggle, "Business admin toggle"))
			logPass("Business admin toggle is selected");
		else
			logFail("Failed to click business admin toggle");
		
	}
	
	public void verifyBusinessSectionDisabled()
	{
		driver.explicitWaitforInvisibilityWithoutLocating(By.id("id_loader"), 60,"loader");
		if(driver.isElementNotDisplayed(InviteUserPage.businessList, "business list"))
			logPass("Business section disabled");
		else
			logFail("Business section is enabled - instance toggle ON");
	}
	
	public void verifyBusinessSectionEnabled()
	{
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.explicitWaitforInvisibilityWithoutLocating(By.id("id_loader"),60, "loader");
		if(driver.isElementDisplayed(InviteUserPage.businessList,"business list"))
			logPass("Business section enabled");
		else
			logFail("Business section is disabled - instance toggle ON");
	}
	
	public void verifyRolesSectionDisabled()
	{
		if(driver.isElementDisplayed(InviteUserPage.inactiveRolesSection, "roles section"))
			logPass("Roles section is disabled on Business toggle ON");
		else
			logFail("Roles section is not disabled on Business toggle ON");
	}
	
	public void clickTeamBreadcrumb()
	{
		waitForLoaderToBeInvisible();
		driver.scrollToElement(InviteUserPage.teamBreadcrumb, "Team bread crumb");
		if(driver.jsClickElement(InviteUserPage.teamBreadcrumb, "Team bread crumb"))
			logPass("Team breadcrumb is clicked");
		else
			logFail("Failed to click team breadcrumb");
	}
	
	public void fillDuplicateEmailAddress()
	{
		driver.actionEnterText(InviteUserPage.emailTxtBox, email, "Email text box");
		driver.actionTabKey(InviteUserPage.emailTxtBox, email);
		
		if(driver.getSize(InviteUserPage.emailRemoveIcon, "remove")==1)
			logPass("Email address entered");
		else
			logFail("Failed to enter email address");
	}
	
	public void verifyUserExistingMsg()
	{
		driver.waitForPageLoad(20);
		waitForLoaderToBeInvisible();
		if(driver.getText(InviteUserPage.userExistingMsg, "Invite message").equals("The user(s) has been already invited."))
			logPass("User already invites message is displayed");
		else
			logFail("User already exists message is not displayed");
	}
	
	public void verifyInstanceToggleNotDisplayed()
	{
		if(driver.isElementNotDisplayed(InviteUserPage.instanceAdminBtn, "Instance toggle"))
			logPass("Instance toggle is not displayed");
		else
			logFail("Instance toggle is displayed");
	}
	
	public void clickBusiness2()
	{	
		
		if(driver.jsClickElement(InviteUserPage.businessCheckbox2, "business checkbox"))
			logPass("Business is selected");
		else
			logFail("Failed to select business");
	}
	
	public void doForgetPasswordLoginWithValidCrentials(String email)
	{
		if(driver.enterText(InstanceTeamPage.emailInput, email, "EmailAddress field"))
			logPass("Email Address is entered");
		else
			logFail("Failed to enter Emailaddress");
		if(driver.enterText(InstanceTeamPage.pwdInput, ExcelReader.getData("LoginCredentials", "NewPassword"), "Password field"))
			logPass("Password is entered");
		else
			logFail("Failed to enter Password");
		if(driver.jsClickElement(InstanceTeamPage.signInBtn, "SignIn Button"))
		{	
			
			logPass("SignIn button is clicked");
		}
		else
			logFail("SignIn button is not displayed");
	}
	
/** Entering  Registered Email Address in Forgot Password screen **/
	
	public void enteringUserRegisteredEmailAddress(String email) {
		if(driver.enterText(ForgotPassword_8.txt_Email, email, "Email Address field"))
			logPass("Entering Registered Email Address");
		else
			logFail("Failed to Enter email address");
	}

	
}
