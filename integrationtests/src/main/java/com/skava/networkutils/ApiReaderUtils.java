package com.skava.networkutils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.framework.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;

public class ApiReaderUtils {

	HttpClient client = new DefaultHttpClient();
	
	HttpHost proxy = null;

	 
	public CookieStore globalCookies = new BasicCookieStore();
	
	/**
	 * Set proxy by using given ip and port
	 */
	
	public void setProxy(String ipOrHost,int port,String protocol)
	{
//		proxy=new HttpHost(ipOrHost, port, protocol);
//		client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY,proxy);
	}
	
	/**
     * read the response from the given url
     * @param url
     * @return
     */
    public String readResponseFromUrls(String url,String sessionId)   //it takes the default scripts mobile domain as referer in header.
	{
    	
		String responseData="";
		try
		{
			
			URL domain=new URL(url);
			
			HttpHost target = null;
			
			if(url.contains("https"))
				target=new HttpHost(domain.getHost(), 443, "https");
			else
				target=new HttpHost(domain.getHost(), 80, "http");
//			target=new HttpHost(domain.getHost());
			url=url.replace("https://"+domain.getHost(), "");
			url=url.replace("http://"+domain.getHost(), "");
			
			HttpGet httpget = new HttpGet(url);
			httpget.addHeader("Referer","https://"+domain.getHost());
			httpget.addHeader("cookie","SESSIONID="+sessionId);
			HttpResponse response = client.execute(target,httpget);
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(),"utf-8"));
		      String line = "";
		      while ((line = rd.readLine()) != null)
		      {
			    //System.out.println(line);
			    responseData+=line;
		      }
		}
		catch(Exception e)
		{
			e.printStackTrace();
			responseData="Error";
		}
		System.out.println(responseData);
		return responseData;
	}
    
    /**
     * Read all details from post call
     * 
     * @param url
     * @param postData
     * @return
     */
    public List<String> readAllDataFromUrl(String url,List<NameValuePair> postData)   //it takes the default scripts mobile domain as reference in header.
	{
		/*
		 * **** input****
		 * url - url details
		 * postData- post data which has to be sent
		 * 
		 */
		
		String ResponseData="";
		List <String> result = new ArrayList<String>();
		try
			{
				URL domain=new URL(url);
				
				HttpHost target = null;
				
				if(url.contains("https"))
					target=new HttpHost(domain.getHost(), 443, "https");
				else
					target=new HttpHost(domain.getHost(), 80, "http");
								
				url=url.replace("https://"+domain.getHost(), "");
				url=url.replace("http://"+domain.getHost(), "");
				
								
				HttpPost httppost=new HttpPost(url);
				httppost.setHeader("Referer","http://"+domain.getHost());
				httppost.setHeader("accept", "application/json");
				httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
				//System.out.println("Post read from: "+postData.get(0));
				
				//httppost.setEntity(
				httppost.setEntity(new UrlEncodedFormEntity(postData));

				HttpResponse response = client.execute(target,httppost);
				
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			      String line = "";
			      while ((line = rd.readLine()) != null) {
			    //System.out.println(line);
			    ResponseData+=line;
			      }
			      
			      result.add(0,ResponseData);
			      Header[] cookies=response.getHeaders("Set-Cookie");
			      String cook="";
			      for(Header h:cookies)
			      {
			    	  //System.out.println(h.getName());
			    	  //System.out.println(h.getValue());
			    	  cook+=h.getValue()+",";
			      }
			      result.add(1, cook);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				ResponseData="Error";
			}
				
		return result;
	}
    
    /**
     * 
     * @param url
     * @param header
     * @param postData
     * @return
     */
    public List<String> readAllDataFromUrl(String url,List<Header> header,List<NameValuePair> postData)  
    {
    	String ResponseData="";
    	List <String> result = new ArrayList<String>();
    	try
    		{
    			URL domain=new URL(url);
    			
    			HttpHost target = null;
				
				if(url.contains("https"))
					target=new HttpHost(domain.getHost(), 443, "https");
				else
					target=new HttpHost(domain.getHost(), 80, "http");
    			
    			url=url.replace("https://"+domain.getHost(), "");
				url=url.replace("http://"+domain.getHost(), "");
    			
    			HttpPost httppost=new HttpPost(url);
    			HttpClientContext context = HttpClientContext.create();					
    			for (Header h : header) {
    				httppost.addHeader(h);
    				//System.out.println(h.getName()+" : "+h.getValue());
    				
    			}
    			//System.out.println("Post read from: "+postData.toString());
    			
    			//httppost.setEntity(
    		
    			httppost.setEntity(new UrlEncodedFormEntity(postData));
    			context.setAttribute(HttpClientContext.COOKIE_STORE, globalCookies);
    			
    			HttpResponse response = client.execute(target,httppost,context);
    			CookieStore cookieStore = context.getCookieStore();
    			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
    		      String line = "";
    		      while ((line = rd.readLine()) != null) {
//    		    //System.out.println(line);
    		    ResponseData+=line;
    		      }
    		      
    		      result.add(0,ResponseData);
    		      
    		      String cook="";
    		      if(!(cookieStore==null))
    		      {
    			      List<Cookie> cookies = cookieStore.getCookies();
    		    	  //System.out.println("Cookies array:"+cookies);
    			      for(Cookie h:cookies)
    			      {
    			    	  cook+=h.getName()+"="+h.getValue()+";";
    			    	  //System.out.println("cookie:"+cook);
    			      }
    		      }
    		      result.add(1, cook);
    		}
    		catch(Exception e)
    		{
    			e.printStackTrace();
    			ResponseData="Error";
    		}
    	System.out.println("result:"+result);
    	return result;
    }
    
    /**
     * 
     * @param url
     * @param header
     * @return
     */
    public String readResponseFromUrl(String url,List<Header> header)
	{
		String ResponseData="";
		try
		{
			URL domain=new URL(url);
			HttpHost target = null;
			if(url.contains("https"))
				target=new HttpHost(domain.getHost(), 443, "https");
			else
				target=new HttpHost(domain.getHost(), 80, "http");
			
			url=url.replace("https://"+domain.getHost(), "");
			url=url.replace("http://"+domain.getHost(), "");
			
			HttpGet httpget = new HttpGet(url);
			for (Header h : header)
			{
				httpget.addHeader(h);
			}
			
			HttpResponse response = client.execute(target,httpget);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	        String line = "";
	        while ((line = rd.readLine()) != null) 
	        {
			     //System.out.println(line);
			     ResponseData+=line;
	        }
	     
	        Header[] cookies=response.getHeaders("Set-Cookie");
	        for(Header h:cookies)
	        {
	    	   //System.out.println(h.getName());
	    	   //System.out.println(h.getValue());
	        }
		}
		catch(Exception e)
		{
			ResponseData="Error";
		}
		return ResponseData;
	}
    
    /**
     * Get Response Code from the Links
     */
    public int getHttpResponseCode(String url)
    {
    	//This method helps to image loaded successfully or not
		String ResponseData="";
		int responseheader = -1;
		try
		{
			URL domain=new URL(url);
			
			HttpHost target = null;
			
			if(url.contains("https"))
				target=new HttpHost(domain.getHost(), 443, "https");
			else
				target=new HttpHost(domain.getHost(), 80, "http");
			
			url=url.replace("https://"+domain.getHost(), "");
			url=url.replace("http://"+domain.getHost(), "");
			
			HttpGet httpget = new HttpGet(url);
			httpget.addHeader("Referer","http://"+domain.getHost());
			
			
			HttpResponse response = client.execute(target,httpget);
			responseheader=response.getStatusLine().getStatusCode();
			response.getEntity().consumeContent();
			//System.out.println("responseheader: "+responseheader);
		}
		catch(Exception e)
		{
			ResponseData="Error";
		}
		return responseheader;
    }
    
    /**
     * Get text from Json Response
     */
    public String readFromFile(String fileName)
    {
    	String response = "";
    	try
    	{
    	     response = IOUtils.toString(new FileInputStream(new File(fileName)),"UTF-8");
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    		response = "Error";
    	}
    	return response;
    }
    /*
     * **** input****
     * url - url details
     * postData- post data which has to be sent
     * 
     */
    
    
    public List<String> ReadAllDataFromUrl1(String url, String postData,String sessionid,String collectionId)   //it takes the default scripts mobile domain as reference in header.
    {
        String ResponseData="";
        List <String> result = new ArrayList<String>();
        if(!url.contains(".js"))
        {
            try
            {
                URL domain=new URL(url);
                if(!postData.equals(""))
                {
                    HttpPost httppost=new HttpPost(url);
                    httppost.setHeader("Referer", "https://" + domain.getHost());
                    httppost.setHeader("Content-Type", "application/json");
                    
                    if(!collectionId.equals(""))
                        httppost.setHeader("x-collection-id", collectionId);
                    if(!sessionid.equals(""))
                        httppost.setHeader("cookie", sessionid);
//                    httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                    HttpClientContext context = HttpClientContext.create();                 
                    
                    StringEntity entity = new StringEntity(postData);
                    httppost.setEntity(entity);
                    System.out.println("HTTPPOST: "+httppost);
                    HttpResponse response = client.execute(httppost);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httppost.releaseConnection();
                    }
                }
                else
                {
                    HttpGet httpget=new HttpGet(url);
                    httpget.addHeader("Referer", "https://" + domain.getHost());
                    HttpClientContext context = HttpClientContext.create();
                    HttpResponse response = client.execute(httpget,context);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        //System.out.println(line);
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httpget.releaseConnection();
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
                ResponseData="Error";
            }
        }
        return result;
    }

    
    
    
    public List<String> ReadAllDataFromUrl(String url, String postData,String sessionid)   //it takes the default scripts mobile domain as reference in header.
    {
        String ResponseData="";
        List <String> result = new ArrayList<String>();
        if(!url.contains(".js"))
        {
            try
            {
                URL domain=new URL(url);
                if(!postData.equals(""))
                {
                    HttpPost httppost=new HttpPost(url);
                    httppost.setHeader("Referer", "https://" + domain.getHost());
                    httppost.setHeader("accept", "application/json");
                    if(!sessionid.equals(""))
                    	httppost.setHeader("cookie", sessionid);
//                    httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                    HttpClientContext context = HttpClientContext.create();                 
                    
                    StringEntity entity = new StringEntity(postData);
                    httppost.setEntity(entity);
                    HttpResponse response = client.execute(httppost);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httppost.releaseConnection();
                    }
                }
                else
                {
                    HttpGet httpget=new HttpGet(url);
                    httpget.addHeader("Referer", "https://" + domain.getHost());
                    HttpClientContext context = HttpClientContext.create();
                    HttpResponse response = client.execute(httpget,context);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        //System.out.println(line);
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httpget.releaseConnection();
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
                ResponseData="Error";
            }
        }
        return result;
    }
    /*
     * read the response from the given url
     * @param url
     * @return
     */
    public String readResponseFromUrl(String url,String sessionId)   //it takes the default scripts mobile domain as referer in header.
    {
        String responseData = "";
        try 
        {
            URL domain = new URL(url);
            HttpGet httpget = new HttpGet(url);
            httpget.addHeader("Referer", "https://" + domain.getHost());
            httpget.addHeader("cookie", sessionId);
            HttpResponse response = client.execute(httpget);
            HttpEntity httpEntity = response.getEntity();
            BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent(), "utf-8"));
            String line = "";
            while ((line = rd.readLine()) != null) 
            {
                //System.out.println(line);
                responseData += line;
            }
            
            if(response.getEntity()!=null)
            {
                response.getEntity().consumeContent();
            }
            
            if(response.getEntity()!=null)
            {
                EntityUtils.consume(httpEntity);
                httpget.releaseConnection();
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            responseData = "Error";
        }
        //System.out.println(responseData);
        return responseData;
    }
    public List<String> patch(String url, String postData,String sessionid) throws ClientProtocolException, IOException
    {
    	 String ResponseData="";
         List <String> result = new ArrayList<String>();
         URL domain=new URL(url);
        HttpPatch httppost=new HttpPatch(url);
        httppost.setHeader("Referer", "https://" + domain.getHost());
        httppost.setHeader("accept", "application/json");
        if(!sessionid.equals(""))
        httppost.setHeader("cookie", sessionid);
//       httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        HttpClientContext context = HttpClientContext.create();                 
        
        StringEntity entity = new StringEntity(postData);
        httppost.setEntity(entity);
        HttpResponse response = client.execute(httppost);
        HttpEntity httpEntity = response.getEntity();
        CookieStore cookieStore = context.getCookieStore();
        BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
        String line = "";
        while ((line = rd.readLine()) != null) 
        {
            ResponseData+=line;
        }
        result.add(0,ResponseData);
        String cook="";
        if(!(cookieStore==null))
        {
            List<Cookie> cookies = cookieStore.getCookies();
            System.out.println("Cookies array:"+cookies);
            for(Cookie h:cookies)
            {
                //System.out.println(h.getName());
                //System.out.println(h.getValue());
                cook+=h.getName()+"="+h.getValue()+";";
                System.out.println("cookie:"+cook);
            }
        }
        result.add(1, cook);
        if(response.getEntity()!=null)
        {
            response.getEntity().consumeContent();
        }
        
        if(response.getEntity()!=null)
        {
            EntityUtils.consume(httpEntity);
            httppost.releaseConnection();
        }
    
    	return result;
    }
    
    public List<String> deleteattribute(String url,String collection)   //it takes the default scripts mobile domain as reference in header.
	{
	   String ResponseData="";
	   List <String> result = new ArrayList<String>();
	   if(!url.contains(".js"))
	   {
		   try
		   {
			   URL domain=new URL(url);

				   HttpDelete httppost=new HttpDelete(url);
				   httppost.setHeader("accept", "*/*");
				   httppost.setHeader("Content-Type", "application/json");
				   if(!collection.equals(""))
					   httppost.setHeader("x-collection-id", collection);
				   HttpClientContext context = HttpClientContext.create();

				   HttpResponse response = client.execute(httppost);
				   HttpEntity httpEntity = response.getEntity();
				   CookieStore cookieStore = context.getCookieStore();
				   BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
				   String line = "";
				   while ((line = rd.readLine()) != null)
				   {
					   ResponseData+=line;
				   }
				   result.add(0,ResponseData);
				   String cook="";
				   if(!(cookieStore==null))
				   {
					   List<Cookie> cookies = cookieStore.getCookies();
					   System.out.println("Cookies array:"+cookies);
					   for(Cookie h:cookies)
					   {
						   //System.out.println(h.getName());
						   //System.out.println(h.getValue());
						   cook+=h.getName()+"="+h.getValue()+";";
						   System.out.println("cookie:"+cook);
					   }
				   }
				   result.add(1, cook);
				   if(response.getEntity()!=null)
				   {
					   response.getEntity().consumeContent();
				   }

				   if(response.getEntity()!=null)
				   {
					   EntityUtils.consume(httpEntity);
					   httppost.releaseConnection();
				   }

		   }
		   catch(Exception e)
		   {
			   e.printStackTrace();
			   ResponseData="Error";
		   }
	   }
	   return result;
	}
    
    public List<String> ReadAllDataFromUrlserviceput(String url, String postData,String sessionid,String collection)   //it takes the default scripts mobile domain as reference in header.
    {
        String ResponseData="";
        List <String> result = new ArrayList<String>();
        if(!url.contains(".js"))
        {
            try
            {
                URL domain=new URL(url);
                if(!postData.equals(""))
                {
                    HttpPut httppost=new HttpPut(url);
//                    httppost.setHeader("Referer", "https://" + domain.getHost());
                    httppost.setHeader("accept", "*/*");
                    httppost.setHeader("Content-Type", "application/json");
                    if(!sessionid.equals(""))
                        httppost.setHeader("X-Auth-Token", sessionid);
                    if(!collection.equals(""))
                        httppost.setHeader("x-collection-id", collection);
                    httppost.setHeader("x-version", "8.0");
                    
//                    httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                    HttpClientContext context = HttpClientContext.create();                 
                    
                    StringEntity entity = new StringEntity(postData);
                    httppost.setEntity(entity);
                    HttpResponse response = client.execute(httppost);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httppost.releaseConnection();
                    }
                }
                else
                {
                    HttpGet httpget=new HttpGet(url);
                    httpget.addHeader("Referer", "https://" + domain.getHost());
                    HttpClientContext context = HttpClientContext.create();
                    HttpResponse response = client.execute(httpget,context);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        //System.out.println(line);
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httpget.releaseConnection();
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
                ResponseData="Error";
            }
        }
        return result;
    }
    public List<String> put(String url, String postData,String sessionid) throws ClientProtocolException, IOException
    {
    	 String ResponseData="";
         List <String> result = new ArrayList<String>();
         URL domain=new URL(url);
        HttpPut httppost=new HttpPut(url);
        httppost.setHeader("Referer", "https://" + domain.getHost());
        httppost.setHeader("accept", "application/json");
        httppost.setHeader("x-version","1");
        httppost.setHeader("Content-Type", "application/json");
        httppost.setHeader("X-Auth-Token", sessionid);
        if(!sessionid.equals(""))
        httppost.setHeader("cookie", sessionid);
//       httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        HttpClientContext context = HttpClientContext.create();                 
        
        StringEntity entity = new StringEntity(postData);
        httppost.setEntity(entity);
        HttpResponse response = client.execute(httppost);
        HttpEntity httpEntity = response.getEntity();
        CookieStore cookieStore = context.getCookieStore();
        BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
        String line = "";
        while ((line = rd.readLine()) != null) 
        {
            ResponseData+=line;
        }
        result.add(0,ResponseData);
        String cook="";
        if(!(cookieStore==null))
        {
            List<Cookie> cookies = cookieStore.getCookies();
            System.out.println("Cookies array:"+cookies);
            for(Cookie h:cookies)
            {
                //System.out.println(h.getName());
                //System.out.println(h.getValue());
                cook+=h.getName()+"="+h.getValue()+";";
                System.out.println("cookie:"+cook);
            }
        }
        result.add(1, cook);
        if(response.getEntity()!=null)
        {
            response.getEntity().consumeContent();
        }
        
        if(response.getEntity()!=null)
        {
            EntityUtils.consume(httpEntity);
            httppost.releaseConnection();
        }
    
    	return result;
    }
    
    public List<String> post(String url, String postData,String sessionid) throws ClientProtocolException, IOException
    {
    	 String ResponseData="";
         List <String> result = new ArrayList<String>();
         URL domain=new URL(url);
        HttpPost httppost=new HttpPost(url);
        httppost.setHeader("Referer", "https://" + domain.getHost());
        httppost.setHeader("accept", "application/json");
        httppost.setHeader("x-version", "1");
        httppost.setHeader("X-Auth-Token", sessionid);
        httppost.setHeader("Content-Type", "application/json");
//       httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        HttpClientContext context = HttpClientContext.create();                 
        
        StringEntity entity = new StringEntity(postData);
        httppost.setEntity(entity);
        HttpResponse response = client.execute(httppost);
        HttpEntity httpEntity = response.getEntity();
        CookieStore cookieStore = context.getCookieStore();
        BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
        String line = "";
        while ((line = rd.readLine()) != null) 
        {
            ResponseData+=line;
        }
        result.add(0,ResponseData);
        String cook="";
        if(!(cookieStore==null))
        {
            List<Cookie> cookies = cookieStore.getCookies();
            System.out.println("Cookies array:"+cookies);
            for(Cookie h:cookies)
            {
                //System.out.println(h.getName());
                //System.out.println(h.getValue());
                cook+=h.getName()+"="+h.getValue()+";";
                System.out.println("cookie:"+cook);
            }
        }
        result.add(1, cook);
        if(response.getEntity()!=null)
        {
            response.getEntity().consumeContent();
        }
        
        if(response.getEntity()!=null)
        {
            EntityUtils.consume(httpEntity);
            httppost.releaseConnection();
        }
    
    	return result;
    }
    public List<String> ReadAllDataFromUrlpayment(String url, String postData,String sessionid,String collection)   //it takes the default scripts mobile domain as reference in header.
    {
        String ResponseData="";
        List <String> result = new ArrayList<String>();
        if(!url.contains(".js"))
        {
            try
            {
                URL domain=new URL(url);
                if(!postData.equals(""))
                {
                    HttpPost httppost=new HttpPost(url);
//                    httppost.setHeader("Referer", "https://" + domain.getHost());
                    httppost.setHeader("accept", "*/*");
                    httppost.setHeader("Content-Type", "application/json");
                    if(!sessionid.equals(""))
                    	httppost.setHeader("X-Auth-Token", sessionid);
                    if(!collection.equals(""))
                    	httppost.setHeader("x-collection-id", collection);
                    httppost.setHeader("version", "8.0.0");
                    httppost.setHeader("x-user-id", "1");
                    
//                    httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                    HttpClientContext context = HttpClientContext.create();                 
                    
                    StringEntity entity = new StringEntity(postData);
                    httppost.setEntity(entity);
                    HttpResponse response = client.execute(httppost);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httppost.releaseConnection();
                    }
                }
                else
                {
                    HttpGet httpget=new HttpGet(url);
                    httpget.addHeader("Referer", "https://" + domain.getHost());
                    HttpClientContext context = HttpClientContext.create();
                    HttpResponse response = client.execute(httpget,context);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        //System.out.println(line);
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httpget.releaseConnection();
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
                ResponseData="Error";
            }
        }
        return result;
    }
    
    //Paramesh
    public List<String> ReadAllDataFromUrlAI(String url, String postData,String sessionid,String collectionId)   //it takes the default scripts mobile domain as reference in header.
    {
        String ResponseData="";
        List <String> result = new ArrayList<String>();
        if(!url.contains(".js"))
        {
            try
            {
                URL domain=new URL(url);
                if(!postData.equals(""))
                {
                    HttpPost httppost=new HttpPost(url);
                    httppost.setHeader("Referer", "https://" + domain.getHost());
                    httppost.setHeader("Content-Type", "application/json");
                    
                    if(!collectionId.equals(""))
                    	httppost.setHeader("x-collection-id", collectionId);
                    if(!sessionid.equals(""))
                    	httppost.setHeader("cookie", sessionid);
//                    httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                    HttpClientContext context = HttpClientContext.create();                 
                    
                    StringEntity entity = new StringEntity(postData);
                    httppost.setEntity(entity);
                    System.out.println("HTTPPOST: "+httppost);
                    HttpResponse response = client.execute(httppost);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httppost.releaseConnection();
                    }
                }
                else
                {
                    HttpGet httpget=new HttpGet(url);
                    httpget.addHeader("Referer", "https://" + domain.getHost());
                    HttpClientContext context = HttpClientContext.create();
                    HttpResponse response = client.execute(httpget,context);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        //System.out.println(line);
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httpget.releaseConnection();
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
                ResponseData="Error";
            }
        }
        return result;
    }
    public List<String> ReadAllDataFromUrl(String url, String postData,String sessionid, String collectionId, String version, String OrderauthToken)   //it takes the default scripts mobile domain as reference in header.
    {
        String ResponseData="";
        List <String> result = new ArrayList<String>();
        if(!url.contains(".js"))
        {
            try
            {
                URL domain=new URL(url);
                if(!postData.equals(""))
                {
                    HttpPost httppost=new HttpPost(url);
                    httppost.setHeader("Referer", "https://" + domain.getHost());
                 //   httppost.setHeader("accept", "application/json");
                    if(!sessionid.equals(""))
                        httppost.setHeader("cookie", sessionid);
                    if(!collectionId.equals(""))
                        httppost.setHeader("x-collection-id", collectionId);
                    if(!version.equals(""))
                        httppost.setHeader("x-version", version);
                    if(!OrderauthToken.equals(""))
                        httppost.setHeader("X-Auth-Token", OrderauthToken);
                    httppost.setHeader("Content-Type", "application/json");
                    HttpClientContext context = HttpClientContext.create();                 
                    
                    StringEntity entity = new StringEntity(postData);
                    httppost.setEntity(entity);
                    HttpResponse response = client.execute(httppost);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httppost.releaseConnection();
                    }
                }
                else
                {
                    HttpGet httpget=new HttpGet(url);
                    httpget.addHeader("Referer", "https://" + domain.getHost());
                    HttpClientContext context = HttpClientContext.create();
                    HttpResponse response = client.execute(httpget,context);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        //System.out.println(line);
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httpget.releaseConnection();
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
                ResponseData="Error";
            }
        }
        return result;
    }
    public List<String> ReadAllPutDataFromUrl(String url, String postData,String sessionid, String collectionId, String version)   //it takes the default scripts mobile domain as reference in header.
    {
    	ExtentTestManager.getTest().log(LogStatus.PASS, "url "+url);
    	ExtentTestManager.getTest().log(LogStatus.PASS, "postData "+postData);
    	ExtentTestManager.getTest().log(LogStatus.PASS, "sessionid "+sessionid);
    	ExtentTestManager.getTest().log(LogStatus.PASS, "version "+version);
        String ResponseData="";
        List <String> result = new ArrayList<String>();
        if(!url.contains(".js"))
        {
            try
            {
                URL domain=new URL(url);
                if(!postData.equals(""))
                {
                    HttpPut httpput=new HttpPut(url);
                    httpput.setHeader("Referer", "https://" + domain.getHost());
                    httpput.setHeader("accept", "application/json");
                    if(!sessionid.equals(""))
                        httpput.setHeader("cookie", sessionid);
                    if(!collectionId.equals(""))
                        httpput.setHeader("x-collection-id", collectionId);
                    if(!version.equals(""))
                        httpput.setHeader("x-version", version);
                    httpput.setHeader("Content-Type", "application/json");
                    HttpClientContext context = HttpClientContext.create();                 
                    
                    StringEntity entity = new StringEntity(postData);
                    httpput.setEntity(entity);
                    HttpResponse response = client.execute(httpput);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httpput.releaseConnection();
                    }
                }
                else
                {
                    HttpGet httpget=new HttpGet(url);
                    httpget.addHeader("Referer", "https://" + domain.getHost());
                    HttpClientContext context = HttpClientContext.create();
                    HttpResponse response = client.execute(httpget,context);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        //System.out.println(line);
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httpget.releaseConnection();
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
                ResponseData="Error";
            }
        }
        return result;
    }
    
    
    
    
    public List<String> ReadAllDataFromUrl(String url, String postData,String sessionid, String collectionId, String version, String OrderauthToken, String userInfo)   //it takes the default scripts mobile domain as reference in header.
    {
        String ResponseData="";
        List <String> result = new ArrayList<String>();
        if(!url.contains(".js"))
        {
            try
            {
                URL domain=new URL(url);
                if(!postData.equals(""))
                {
                    HttpPost httppost=new HttpPost(url);
                    httppost.setHeader("Referer", "https://" + domain.getHost());
                 //   httppost.setHeader("accept", "application/json");
                    if(!sessionid.equals(""))
                    	httppost.setHeader("cookie", sessionid);
                    if(!collectionId.equals(""))
                    	httppost.setHeader("x-collection-id", collectionId);
                    if(!version.equals(""))
                    	httppost.setHeader("x-version", version);
                    if(!userInfo.equals(""))
                    	httppost.setHeader("userInfo", userInfo);
                    if(!OrderauthToken.equals(""))
                    	httppost.setHeader("X-Auth-Token", OrderauthToken);
                    httppost.setHeader("Content-Type", "application/json");
                    HttpClientContext context = HttpClientContext.create();                 
                    
                    StringEntity entity = new StringEntity(postData);
                    httppost.setEntity(entity);
                    HttpResponse response = client.execute(httppost);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httppost.releaseConnection();
                    }
                }
                else
                {
                    HttpGet httpget=new HttpGet(url);
                    httpget.addHeader("Referer", "https://" + domain.getHost());
                    HttpClientContext context = HttpClientContext.create();
                    HttpResponse response = client.execute(httpget,context);
                    HttpEntity httpEntity = response.getEntity();
                    CookieStore cookieStore = context.getCookieStore();
                    BufferedReader rd = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
                    String line = "";
                    while ((line = rd.readLine()) != null) 
                    {
                        //System.out.println(line);
                        ResponseData+=line;
                    }
                    result.add(0,ResponseData);
                    String cook="";
                    if(!(cookieStore==null))
                    {
                        List<Cookie> cookies = cookieStore.getCookies();
                        System.out.println("Cookies array:"+cookies);
                        for(Cookie h:cookies)
                        {
                            //System.out.println(h.getName());
                            //System.out.println(h.getValue());
                            cook+=h.getName()+"="+h.getValue()+";";
                            System.out.println("cookie:"+cook);
                        }
                    }
                    result.add(1, cook);
                    
                    if(response.getEntity()!=null)
                    {
                        response.getEntity().consumeContent();
                    }
                    
                    if(response.getEntity()!=null)
                    {
                        EntityUtils.consume(httpEntity);
                        httpget.releaseConnection();
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
                ResponseData="Error";
            }
        }
        return result;
    }
    
    
    
    /*
     * read the response from the given url
     * @param url
     * @return
     */
}
