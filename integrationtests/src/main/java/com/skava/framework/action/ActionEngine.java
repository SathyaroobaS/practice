package com.skava.framework.action;

import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import net.lightbody.bmp.core.har.Har;

public interface ActionEngine 
{
	public void maximizeBrowser();

	public void navigateToUrl(String url);

	public boolean enterText(By textField, String textValue, String elementName);

	public boolean clickElement(By buttonField, String elementName);

	public String findElement(By txtusername);

	public boolean isElementPresent(By elementPath, String elementName);

	public boolean compareElementText(By path, String expectedText, String elementName);

	public boolean explicitWaitforVisibility(By path, String elementName);

	public boolean isElementDisplayed(By path, String elementName);

	public String getText(By path, String elementName);

	public String getElementAttribute(By path, String attrName, String elementName);

	public boolean compareElementAttribute(By path, String attrName, String expectedText, String elementName);

	public int generateRandomNumber(int limit);

	public int generateRandomNumberWithLimit(int maximum, int minimum);

	public boolean scrollToElement(By path, String elementName);

	public boolean isElementNotDisplayed(By path, String elementName);

	public boolean isElementEnabled(By path, String elementName);

	public void takeScreenShot(String elementName);

//	public void takeScreenShotandSendEmail(String elementName, String failureMessage);
	
	public boolean enterTextAndSubmit(By path, String value, String elementName);

	public void quit();

	public void takeScreenshotWithPath(String path, String elementName);

	public String getCurrentUrl();

	public int getSize(By path, String elementName);
	
	public boolean selectByIndex(By path, int indexField, String elementName);
	
	public Boolean tabKey(By buttonField, String elementName);
	
	public boolean waitForPageLoad(int secs);
	
	public boolean appendText(By textField, String textValue, String elementName);
	
	public boolean moveToElementAndClick(By path, String elementName);
	
	public boolean jsClickElement(By buttonField, String elementName);
	
	public boolean alertPopupVerification();
	
	public boolean waitforAlertPopup();
	
	public String alertPopupGetText();
	
	public boolean alertPopupAccept();

	public boolean explicitWaitforInVisibility(By path,String elementName);
	
	public boolean explicitWaitforClickable(By path,String elementName);
	
	public boolean SwitchtoIframe(By path,String elementName);
	
	public boolean SwitchtoParentFrame(String elementName);
	
	public boolean waitForPageLoad(By path, String elementName, String style);
	
	public boolean clearText(By path, String elementName);
	
	public String getCookie(String cookieName);

	public boolean navigateWithKeys(Keys value, int count, String elementName);
	
	public boolean changeFocus(int tabNumber);
	
	public void changeFocusToDefaultTab();
	
	public void driverMode(String view);
	
	public String getPageSource();

	public boolean isElementSelected(By path, String elementName);
	
	public boolean isElementNotSelected(By path, String elementName);
	
	public boolean waitForScriptLoad();

	public String takeScreenShotReturnFilePath();
	
	public String getElementAttributeWithoutVisibility(By path,String attrName);

	public void setProxyDomain(String domain);
	
	public void enableHarCaptureTypes();
	
	public Har getHar();
	
	public void abortProxy();
	
	public void saveNetworkLog(String fileName);
	
	public void pageReload();
	
	public String getPseudoValue(By path, String pseudoType, String elementName);
	
	public boolean switchToWidow();
	
	public void navigateBack();
	
	public boolean selectByValue(By path,String value,String elementName);

	public void PageEnd();

	public boolean compareElementTextWithIgnoreCase(By path, String expectedText, String elementName);
	
	public boolean setWindowSize(int width, int height);
	
	public String jsQueryExecutorById(By path);
   
	public void deleteAllCookies();
	
	public boolean moveToElement(By path, String elementName);
	
	public String jsQueryExecutorByClass(By path);
	
	public void checkLayout(String spec, String testCaseName);

	public boolean mouseHoverElement(By xpath, String string);
	
	public Map<String, String> getBrowserDetails();

	public void driverSize(int length, int breadth);

	public boolean waitForElementAttribute(By path, String attribute, String value, String elementName);
	

	

	WebElement element(By path);

	boolean actionClickElement(By path, String elementName);

	public boolean explicitWaitforInvisibilityWithoutLocating(By path, int seconds, String elementName);

	public boolean actionEnterText(By textField, String textValue, String elementName);

	public void actionTabKey(By textField, String elementName);

	public String generateRandomString();

	public void refreshPage();

	public String hiddenGetText(By path, String elementName);

}