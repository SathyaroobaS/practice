package com.skava.object.repository;

import org.openqa.selenium.By;

public class orderListingPage
{
	public static final By orderIdCol = By.xpath("//*[@data-qa='order-id-item']");
	
	public static final By orderDataCol = By.xpath("//*[@data-qa='order-date-trigger']");
	public static final By custNameCol = By.xpath("//*[@data-qa='customer-name-item']");
	public static final By accIdCol = By.xpath("//*[@data-qa='account-id-item']");
	public static final By orderVal = By.xpath("//*[@data-qa='order-value-item']");
	
	public static final By manageOrderTitle = By.xpath("//*[@data-qa='manage-orders-title']");
	
	public static final By ordersBreadCrum = By.xpath("//*[@data-qa='manage-orders-breadcrumb']");
	public static final By rowsDropDown = By.xpath("//*[@data-qa='page-length-select']");
	//rows-dropdown   page-length-select
	public static final By paginationCont = By.xpath("//*[@data-qa='pagination-trigger']");
	public static final By viewBtn = By.xpath("//*[@data-qa='view-trigger']");
	public static final By orderListRow = By.xpath("//*[@data-qa='order-record-row']");
	
	public static final By orderListCount = By.xpath("//*[@data-qa='order-id-value']");
	
	public static final By currentPage = By.xpath("//*[@data-qa='current-page-item']");
	public static final By orderDetTitle = By.xpath("//*[@data-qa='order-id-item']");
	
	
	public static final By orderIdFilter = By.xpath("//div[@label='Order ID']//button");
	public static final By orderIdSearchInp = By.xpath("//input[@label='Order ID']");
	public static final By searchBtn = By.xpath("//*[@data-qa='search-trigger']");
	public static final By resetBtn = By.xpath("//*[@data-qa='reset-trigger']");
	public static final By accIdFilter = By.xpath("//*[@label='Account ID']//button");
	public static final By accIdSearchInp = By.xpath("//input[@label='Account ID']");
	public static final By accIdTabVal = By.xpath("//*[@data-qa='account-id-value']");
	public static final By orderIdTabVal = By.xpath("//*[@data-qa='order-id-value']");
	public static final By orderDateTabVal = By.xpath("//*[@data-qa='order-date-value']");
	public static final By customerNameTabVal = By.xpath("//*[@data-qa='customer-name-value']");
	public static final By dateRangeBtn = By.xpath("//*[@label='Date Range']//button");
	public static final By dateRangeFilter = By.xpath("//*[@label='Date Range']//button");
	public static final By firstNameFilter = By.xpath("//*[@label='First Name']//button");
	public static final By firstNameInput = By.xpath("//input[@label='First Name']");
	public static final By lastNameFilter = By.xpath("//*[@label='Last Name']//button");
	public static final By lastNameInput = By.xpath("//input[@label='Last Name']");
	public static final By emailNameFilter = By.xpath("//*[@label='Email']//button");
	public static final By emailNameInput = By.xpath("//input[@label='Email']");
	public static final By dateRangeStartMonth = By.xpath("//*[@data-qa='date-range-start-month']//[@class='available in-range']");
	public static final By dateRangeEndMonth = By.xpath("//*[@data-qa='date-range-end-month']//[@class='available in-range']");
	public static final By addMoreFilter = By.xpath("//*[@data-qa='add-more-filter']//button");
	
	public static final By filterList = By.xpath("//*[@data-qa='add-more-filter']//ul"); 
	public static final By triggSkuId = By.xpath("//*[@data-qa='sku-id-trigger']");
	public static final By enterSkuId = By.xpath("//*[@data-qa='sku-id-input']");
	public static final By searchButton = By.xpath("//*[@data-qa='search-trigger']");
	public static final By triggCity = By.xpath("//*[@data-qa='city-trigger']");
	public static final By enterCity = By.xpath("//*[@data-qa='city-input']");
	public static final By triggCountry = By.xpath("//*[@data-qa='country-filter']");
	public static final By enterCountry = By.xpath("//*[@data-qa='country-input']");
	public static final By triggZip = By.xpath("startDateFilter//*[@data-qa='zipcode-filter']");
	public static final By enterZip = By.xpath("//*[@data-qa='zipcode-input']");
	
	public static final By triggState = By.xpath("//*[@data-qa='state-filter']");
	public static final By enterState = By.xpath("//*[@data-qa='state-input']");
	public static final By fulFillment  = By.xpath("//*[@data-qa='fullfillment-trigger']/span");
	
	public static final By firstName = By.xpath("//*[@elem='firstName']");
	public static final By triggerFirstName = By.xpath("//*[@elem='firstName']//span");
	public static final By filterFirstNameInput = By.xpath("//*[@elem='firstName']//input");
	public static final By customerNameTableVal = By.xpath("//*[@data-qa='customer-name-value']");
    public static final By orderValRes = By.xpath("//*[@data-qa='order-val-value']");
    public static final By noDataErr = By.xpath("//*[@data-qa='no-data-error-alert']");
    public static final By noDataToDisplay = By.xpath("//*[@class='dataTables_empty']");
    public static final By startDateFilter = By.xpath("(//table[@class='table-condensed'])[1]//td[@class='active start-date available']");
    public static final By endDateFilter = By.xpath("((//table[@class='table-condensed'])[2]//td[@class='weekend available'])[2]");
    public static final By calendarApply = By.xpath("(//*[@class='drp-buttons']//button)[2]");
    
	public static final By viewOrderTrigger = By.xpath("(//*[@data-qa='view-details-trigger'])[1]");
	public static final By firstRowSelect = By.xpath("(//*[@id='id_skManageOrders']//tbody//tr)[1]");
	
	
	
	
	public static final By DateRangeOLP = By.xpath("//*[@data-qa='date-range-filter-item']");
	public static final By MonthRangeOLP = By.xpath("//*[@class='drp-calendar left']//th[@class='month']//select[@class='monthselect']");
}

