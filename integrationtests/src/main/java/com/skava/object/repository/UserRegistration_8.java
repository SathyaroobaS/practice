package com.skava.object.repository;

import org.openqa.selenium.By;

public class UserRegistration_8 {

	public static final By sk_Logo = By.xpath("//*[@dtat-qa='sk-logo']");
	public static final By sk_ScreenName = By.xpath("//*[@data-qa='user-registration-page']");
	public static final By txt_FirstName = By.xpath("//*[@data-qa='first-name-input']");
	public static final By txt_LastName = By.xpath("//*[@data-qa='last-name-input']");
	public static final By txt_CreatePassword = By.xpath("//*[@data-qa='create-password']");
	public static final By txt_CrtPwdHint = By.xpath("//*[@data-qa='new-password-hint-txt']");
	public static final By txt_ConfirmPassword = By.xpath("//*[@data-qa='confirm-password']");
	public static final By butn_Register = By.xpath("//*[@data-qa='register-trigger']");
	public static final By content_SuccessMsg = By.xpath("//*[contains(text(),'registered')]");//By.xpath("//*[@data-qa='message-input']");
	public static final By inerror_FirstName = By.xpath("//*[@data-qa='first-name-label']/preceding::span");
	public static final By inerror_LastName = By.xpath("//*[@data-qa='last-name-label']/preceding::span");
	public static final By inerror_CreatePassord = By.xpath("//*[@data-qa='create-password-label']/preceding::span");
	public static final By inerror_ConfirmPassord = By.xpath("//*[@data-qa='confirm-password']/preceding::span");
	public static final By txt_CopyrightContent = By.xpath("//*[@data-qa='sk-copyright']");
	public static final By label_FirstName = By.xpath("//*[contains(text(),'First Name')]");
	public static final By label_LastName = By.xpath("//*[contains(text(),'Last Name')]");
	public static final By label_CreatePassord = By.xpath("//*[contains(text(),'Create Password')]");
	public static final By label_ConfirmPassord = By.xpath("//*[contains(text(),'Confirm Password')]");
	
	
	//public static final By inerror_PasswordMismatch = By.xpath("//*[@data-qa='password-mismatch-error']");
}
