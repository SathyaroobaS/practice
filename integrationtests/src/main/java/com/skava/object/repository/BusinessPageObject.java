package com.skava.object.repository;

import org.openqa.selenium.By;

public class BusinessPageObject 
{
	public static final By AddBusiness_Btn 						= By.xpath("//*[@data-qa='add-business-trigger']");
	public static final By Business_Name_Txt					= By.xpath("//*[@data-qa='business-name-input']");
	public static final By Business_Service_lnk					= By.xpath("//*[@data-qa='storage-service-input']");
	public static final By Business_CDN_lnk						= By.xpath("//*[@data-qa='cdn-input']");
	public static final By MicroService_Catelog_Chkbox			= By.xpath("//*[@data-qa='catalog-trigger']/following::label[1]");
	public static final By MicroService_Inventory_Chkbox		= By.xpath("//*[@data-qa='inventory-trigger']/following::label[1]");
	public static final By MicroService_Promotion_Chkbox		= By.xpath("//*[@data-qa='promotion-trigger']/following::label[1]");
	public static final By MicroService_Pricing_Chkbox			= By.xpath("//*[@data-qa='pricing-trigger']/following::label[1]");
	public static final By MicroService_Create_Btn				= By.xpath("//*[@data-qa='save-business-trigger']");
	
	// Success Pop-up after creating business
	public static final By	Business_Success_Popup				= By.xpath("//*[@data-qa='popup-cont']");
	public static final By	Business_Success_Msg				= By.xpath("//*[@data-qa='success-msg']");
	public static final By	Popup_Ok_Btn						= By.xpath("//*[@data-qa='ok-trigger']");
	
	// Update business - from breadcrumb
	public static final By	Business_lnk_breadcrumb				= By.xpath("//*[@data-qa='business-overview-page-breadcrum']");
	public static final By	Edit_Business_btn					= By.xpath("//*[@data-qa='view-listed-business-trigger']");
	public static final By	Edit_Business_Savebtn				= By.xpath("(//*[@class='card border-1 border-elevation-1']//*[@class='btn btn-sm btn-info waves-effect waves-light'])[1]");
	public static final By	Edit_List_Btn						= By.xpath("//*[@data-qa='view-listed-business-trigger']");
	
	
	// Add a Business 
	public static final By	Add_Business_Btn					= By.xpath("//*[@data-qa='add-business-trigger']");
	public static final By	CreateBusiness_Title				= By.xpath("//*[@data-qa='create-business-term']");
	public static final By	CreateBusiness_DOM					= By.xpath("//*[@id='id_skCrtBsnsFrm']");
	
	// Loop select in microservice
	public static final By Microservice_Multiselection_checkbox_loop		= By.xpath("(//*[@data-qa='micro-services-tile']/div/div[1]/div[2]/span)");
	public static final By	Business_Overview_Tab							= By.xpath("(//*[@data-qa='business-overview'])[1]");		
	
	public static final By	Businessid_Dropdown					= By.xpath("//*[@data-qa='business-id-filter']");
	public static final By	Businessid_txtbox					= By.xpath("//*[@data-qa='business-id-input']");
	public static final By	Search_btn							= By.xpath("//*[@data-qa='search-trigger']");
	public static final By	Business_id							= By.xpath("//*[@data-qa='id-item']");
	public static final By  BusinessName_Navigation           	= By.xpath("//*[@data-qa='business-title-item']");
	public static final By  Click_Business			           	= By.xpath("//*[@data-qa='view-listed-business-trigger']");
	
	
	
	
	
}