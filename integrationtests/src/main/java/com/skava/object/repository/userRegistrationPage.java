package com.skava.object.repository;

import org.openqa.selenium.By;

public class userRegistrationPage {

	public static final By sk_Logo = By.xpath("//*[@dtat-qa='sk-logo']");
	public static final By sk_ScreenName = By.xpath("//*[@data-qa='user-registration-name']");
	public static final By txt_FirstName = By.xpath("//*[@data-qa='first-name-input']");
	public static final By txt_LastName = By.xpath("//*[@data-qa='last-name-input']");
	public static final By txt_CreatePassword = By.xpath("//*[@data-qa='create-password']");
	public static final By txt_CrtPwdHint = By.xpath("//*[@data-qa='password-hint']");
	public static final By txt_ConfirmPassword = By.xpath("//*[@data-qa='confirm-password']");
	public static final By butn_Register = By.xpath("//*[@data-qa='register-trigger']");
	public static final By content_SuccessMsg = By.xpath("//*[@data-qa='message-input']");
	public static final By inerror_FirstName = By.xpath("//*[@data-qa='fname-error']");
	public static final By inerror_LastName = By.xpath("//*[@data-qa='lname-error']");
	public static final By inerror_CreatePassord = By.xpath("//*[@data-qa='password-error']");
	public static final By inerror_ConfirmPassord = By.xpath("//*[@data-qa='confirm-password-error']");
	public static final By txt_CopyrightContent = By.xpath("//*[@data-qa='sk-copyright']");
	public static final By inerror_PasswordMismatch = By.xpath("//*[@data-qa='password-mismatch-error']");
}
