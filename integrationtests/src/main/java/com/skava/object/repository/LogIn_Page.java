package com.skava.object.repository;

import org.openqa.selenium.*;
 
public class LogIn_Page 
{ 
	public static final By txtbx_UserName = By.xpath("//*[@data-qa='user-name-input']");
	public static final By txtbx_Password = By.xpath("//*[@data-qa='password-input']");
	public static final By btn_LogIn = By.xpath("//*[@data-qa='signin-trigger']"); 
}