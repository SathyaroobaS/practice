package com.skava.object.repository;

import org.openqa.selenium.By;

public class StoresObject 
{	
	public static final By StoreName 						= By.xpath("//*[@data-qa='store-name-input']");
	public static final By StoreMenu 						= By.xpath("//*[@data-qa='stores-menu']");
	public static final By Timezone 						= By.xpath("//*[@data-qa='time-zone-list']");
	public static final By Edit_Timezone 					= By.xpath("//*[@data-qa='time-zone-container']");	
	public static final By Timezone_Options 				= By.xpath("(//*[contains(text(),'(GMT-6:00) US/Central')])[1]");
	public static final By Timezone_Options_Canada			= By.xpath("(//*[contains(text(),'(GMT-6:00) Canada/Central')])[1]");
	public static final By AddStore_Btn		 				= By.xpath("//*[@data-qa='add-store-btn']");
	public static final By Locales 							= By.xpath("//*[@value='Locales']");
	public static final By Locales_Options 					= By.xpath("(//*[contains(text(),'United States - English (en_US)')])[1]");
	public static final By Locales_Options_Canada			= By.xpath("(//*[contains(text(),'Canada - English (en_CA)')])[1]");
	public static final By ShipToRegion						= By.xpath("//*[@value='Ship To Regions']");
	//public static final By ShipToRegion_Options 			= By.xpath("((//*[@value='en_US'])[1]/span/label)");
	public static final By ShipToRegion_Options 			= By.xpath("((//*[@value='US'])[1])");
	
	
	
	//public static final By ShipToRegion_Options 			= By.xpath("(//*[contains(text(),'United States')])[5]");
	public static final By Edit_ShipToRegion_Options_US	    = By.xpath("(//*[contains(text(),'United States')])[1]");
	
	//public static final By ShipToRegion_Options_Canada	 	= By.xpath("//*[@class='select-wrapper mdb-select cls_skSelectBox filled-checkbox checkbox-align-rg icon invalid']/ul/li//*[contains(text(),'Canada')]/label");
	//public static final By ShipToRegion_Options_Canada	 	= By.xpath("(//*[contains(text(),'Canada')])[23]");
	public static final By Currencies						= By.xpath("//*[@value='Currencies']");
	//public static final By Currencies_Options 			= By.xpath("(//*[contains(text(),'US Dollar (USD)')])[1]");
	public static final By Currencies_Options 				= By.xpath("((//*[@value='USD'])[1]/span/label)");
	public static final By Currencies_Options_Canada		= By.xpath("(//*[contains(text(),'Canadian Dollar (CAD)')])[1]");
	public static final By Microservices_Menu 				= By.xpath("(//*[@data-qa='microservices'])[1]");
	public static final By StoreName_Click	 				= By.xpath("(//span[@data-qa='store-name-value'])[1]");
	public static final By ServiceAssociation_Tab			= By.xpath("(//*[@data-qa='micro-service-tab'])[1]");
	public static final By SelectCollection					= By.xpath("//*[@value='Select Collection']");
	public static final By SelectCollection_Options 		= By.xpath("//*[@class='select-wrapper cls_skpromotionService']/ul/li[2]");
	public static final By SelectNewCollection_Options 		= By.xpath("//*[@class='select-wrapper cls_skpromotionService']/ul/li[3]");
	public static final By Associate_Savebtn				= By.xpath("(//*[@data-qa='save-trigger'])[2]");
	public static final By updte_collec_sve					= By.xpath("(//*[@data-qa='save-trigger'])[1]");
	
	public static final By Launch_Btn						= By.xpath("//*[@data-qa='quick-launch-trigger']");
	public static final By collectionName						= By.xpath("//*[@data-qa='collec-name']");
	public static final By EditStore_Btn		    		= By.xpath("//*[@data-qa='edit-store-trigger']");
	
	// Create multiple store
	public static final By Stores_Breadcrumb	    		= By.xpath("(//*[contains(text(),'Stores')])[1]");
	
	// Store Size
	//public static final By Multiple_Collection_Size   	= By.xpath("(//*[@value='Select Collection'])");
	public static final By Multiple_Collection_Size    		= By.xpath("(//td[@data-qa='collection-list'])");
	public static final By Collection_count_dropdown   		= By.xpath("(//*[@class='dropdown-content select-dropdown active']/li)");
	public static final By Goto_Dropdown			   		= By.xpath("//*[@data-qa='goto-list']");
	public static final By Storeops_Dropdown			   	= By.xpath("//*[@data-qa='admin-item']");
	public static final By ViewStore_Click				   	= By.xpath("(//*[@data-qa='view-store-trigger'])//i");
	public static final By Store_id                         = By.xpath("//*[@data-qa='store-id-item']");
    public static final By businessTitle                    = By.xpath("//*[@data-qa='business-title-item']");
    public static final By Store_Pancake                    = By.xpath("//*[@class='material-icons menu']");
    public static final By Store_OverviewTab                = By.xpath("//*[@data-qa='store-overview-menu']");
    public static final By Collection_Row	                = By.xpath("//*[@data-qa='collection-detail-row']");
  
    public static final By Storename		                = By.xpath("((//*[@data-qa='store-name-value'])/a)");
    public static final By Storeid_Dropdown              	= By.xpath("//*[@data-qa='store-id-filter']/div/button");
    public static final By Storeid_txtbox               	= By.xpath("//*[@data-qa='store-id-input']");
    public static final By Editstore_btn                 	= By.xpath("//*[@data-qa='edit-store-trigger']"); 

    // Priya - added on 28th Nov 2018
    public static final By Businessid_Dropdown		        = By.xpath("//*[@data-qa='business-id-filter']");
    public static final By Businessid_Textbox		        = By.xpath("//*[@data-qa='business-id-input']");
    public static final By Configure_Btn			        = By.xpath("(//*[@data-qa='configure-trigger'])");
   
    // Anushiya
    public static final By 	serviceName = By.xpath("(//*[@data-qa='service-name-val'])");
  //Paramesh
    public static final By     dropdownActive 				= By.xpath("//*[@class='dropdown-content select-dropdown multiple-select-dropdown active']");
    public static final By     dropdownActiveClose 			= By.xpath("//*[@class='file-field d-flex align-items-end row']");

    //Mohan - added updated store association
    public static final By storeNameVal					    = By.xpath("(//td[@data-qa='store-name-value'])");
    

}