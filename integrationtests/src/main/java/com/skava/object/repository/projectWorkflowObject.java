package com.skava.object.repository;

import org.openqa.selenium.By;

public class projectWorkflowObject 

{
	public static final By projectDropdown = By.xpath("//*[@data-qa='projects-menu-trigger']");
	public static final By viewAllProjectsButton = By.xpath("//*[@data-qa='viewallproject-trigger']");
	public static final By CreateNewProjectButton = By.xpath("//*[@data-qa='createnewproject-trigger']");
	public static final By searchButton = By.xpath("//*[@data-qa='project-search-trigger']");
	public static final By projModelCont = By.xpath("//*[@data-qa='project-model-cont']");
	public static final By projNameFilter = By.xpath("//*[@data-qa='project-name-search']");
	public static final By stateFilter = By.xpath("//*[@data-qa='project-status-list']");
	public static final By projNameCol = By.xpath("//*[@data-qa='projectname-header-label']");
	public static final By stateCol = By.xpath("//*[@data-qa='projectstate-header-label']");
	public static final By projNameInp = By.xpath("//*[@data-qa='project-name-search-input']");
	public static final By newProjNameInp = By.xpath("//*[@data-qa='newprojectname-value']");
	public static final By createProjBtn = By.xpath("//*[@data-qa='createproject-trigger']");
	public static final By cancelProjBtn = By.xpath("//*[@data-qa='cancelproject-trigger']");
	public static final By successMsg = By.xpath("//*[@data-qa='success-msg']");
	public static final By projListNameVal = By.xpath("//*[@data-qa='projectlist-name-label']");
	public static final By viewAllProjHeader = By.xpath("//*[@data-qa='view-all-project-header']");
	public static final By projStateVal = By.xpath("//*[@data-qa='projectlist-state-label']");
	public static final By projDetBtn = By.xpath("//*[@data-qa='projectdetails-trigger']");
	public static final By projEnterBtn = By.xpath("//*[@data-qa='projectenter-trigger']");
	public static final By noDataErr = By.xpath("//*[@data-qa='no-data-error-alert']");
	public static final By projExitBtn = By.xpath("//*[@data-qa='projectexit-trigger']");
}























