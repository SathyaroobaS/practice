package com.skava.object.repository;

import org.openqa.selenium.By;

public class LoginPageObject 
{	
	public static final By EmailId_Txtbox 		= By.xpath("//*[@data-qa='user-name-input']");
	public static final By Password_Txtbox 		= By.xpath("//*[@data-qa='password-input']");
	public static final By SignIn_Btn	 		= By.xpath("//*[@data-qa='signin-trigger']");
	
	public static final By logout_drpdown		= By.xpath("//*[@data-qa='support-list']");
	public static final By logout_btn			= By.xpath("(//*[@class='dropdown-item align-bottom waves-effect waves-light'])[2]");
	



}