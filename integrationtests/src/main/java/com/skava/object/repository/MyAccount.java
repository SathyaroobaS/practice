package com.skava.object.repository;

import org.openqa.selenium.By;

public class MyAccount {
	
	public static final By loggedUserName = By.xpath("//*[contains(text(),'Hi')]");
	public static final By currentPassword = By.xpath("//*[@data-qa='current-password']");
	public static final By newPassword = By.xpath("//*[@data-qa='new-password-label']");
	public static final By confirmNewPassword = By.xpath("//*[@data-qa='confirm-password']");
	public static final By currentPasswordInput = By.xpath("//*[@data-qa='current-password-input']");
	public static final By newPasswordInput = By.xpath("//*[@data-qa='new-password-input']");
	public static final By confirmNewPasswordInput = By.xpath("//*[@data-qa='confirm-new-password-input']");

	public static final By avatarImg =By.xpath("//*[@data-qa='avatar-item']");
	public static final By lastModified =By.xpath("//*[@data-qa='last-modified-item']");
	public static final By emailAddress =By.xpath("//*[@data-qa='email-input']");
	public static final By firstName =By.xpath("//*[@data-qa='first-name-input']");
	public static final By lastName =By.xpath("//*[@data-qa='last-name-input']");
	public static final By phoneNumber =By.xpath("//*[@data-qa='tele-phone-input']");
	public static final By languagePreference =By.xpath("//*[@data-qa='language-preference-list']");
	public static final By footer =By.xpath("//*[@data-qa='footer-item']");
	public static final By copyRight =By.xpath("//*[@data-qa='copyright-item']");
	public static final By saveBtn = By.xpath("//*[@data-qa='save-trigger']");
	public static final By firstnameerror = By.xpath("//*[@data-qa='first-name-label']/preceding::span[1]");
	public static final By lastnameerror = By.xpath("//*[@data-qa='last-name-label']/preceding::span[1]");
	public static final By phoneerror = By.xpath("//*[@data-qa='tele-phone-label']/preceding::span[1]");
	public static final By passwordCondition = By.xpath("//*[@data-qa='password-condition']");
	public static final By newpasswordfied = By.xpath("//*[@data-qa='new-password-label']/preceding::span[1]");
	public static final By confirmpasswordfied = By.xpath("//*[@data-qa='confirm-password']/preceding::span[1]");
	
	//Paramesh
	public static final By saveConfirmatonMsg = By.xpath("//*[@class='toast-message']");
	
	
	
}
