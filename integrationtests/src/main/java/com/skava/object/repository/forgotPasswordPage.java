package com.skava.object.repository;

import org.openqa.selenium.By;

public class forgotPasswordPage {
	public static final By forgotPassword_ScreenName = By.xpath("//*[@data-qa='forgot-password-txt']");
	public static final By forgotPassword_Link = By.xpath("//*[@data-qa='forgot-password-trigger']");
	public static final By txt_Email = By.xpath("//*[@data-qa='email-input']");
	public static final By button_SendLink = By.xpath("//*[@data-qa='sendlink-trigger']");
	public static final By inerror_NonRegisteredEmailAddress = By.xpath("//*[@data-qa='nonemail-address-error']");
	public static final By inerror_invalidEmailAddress = By.xpath("//*[@data-qa='email-address-error']");
	public static final By txt_SuccessMessage = By.xpath("//*[@data-qa='success-message-txt']");
	public static final By txt_NewPassord = By.xpath("//*[@data-qa='new-password-input']");
	public static final By txt_NewPasswordHint = By.xpath("//*[@data-qa='new-password-hint-txt']");
	public static final By txt_ConfirmNewPassword = By.xpath("//*[@data-qa='confirm-new-password-input']");
	public static final By button_Save = By.xpath("//*[@data-qa='save-trigger']");
	public static final By txt_SuccessReset = By.xpath("//*[@data-qa='rest-password-succses-txt']");
	public static final By inerror_NewPassword = By.xpath("//[@data-qa='new-password-error']");
	public static final By inerror_ConfirmNewPassword = By.xpath("//*[@data-qa='new-password-error']");
	public static final By button_successOk = By.xpath("//*[@data-qa='success-msg-ok-trigger']");
}
