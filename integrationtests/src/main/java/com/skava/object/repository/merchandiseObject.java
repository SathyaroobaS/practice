package com.skava.object.repository;

import org.openqa.selenium.*;
 
public class merchandiseObject 
{ 
	 public static final By txtEmailAddress = By.xpath("//*[@data-qa='user-name-input']");
	 public static final By txtPassword = By.xpath("//*[@data-qa='password-input']");
	 public static final By btnSignIn = By.xpath("//*[@data-qa='signin-trigger']");
	 public static final By profilemenu = By.xpath("//*[@data-qa='support-list']");
	 //public static final By profilemenu = By.xpath("//*[@data-qa='profile-menu-trigger']");
	 public static final By logout = By.xpath("//*[@class=''dropdown-item'][2]']");
	 
	 public static final By dropdownPrjct = By.xpath("//*[@data-qa='projects-menu-trigger']");
	 public static final By listCategories = By.xpath("//*[@data-qa='category-list-table']");
	 public static final By colCategoryId = By.xpath("//*[@data-qa='col-category-id']");
	 public static final By colCategoryName = By.xpath("//*[@data-qa='col-category-name']");
	 public static final By colCategoryStatus = By.xpath("//*[@data-qa='col-status']");
	 public static final By colCategoryIsVisible = By.xpath("//*[@data-qa='col-is-visible']");
	 public static final By categoryNameInp = By.xpath("//*[@data-qa='category-name-input']");
	 public static final By categoryIdInp = By.xpath("//*[@data-qa='category-id-input']");
	 public static final By btnArrow = By.xpath("//*[@data-qa='category-edit-trigger']");
	 public static final By btnEdit = By.xpath("//*[@data-qa='category-edit-trigger']");
	 public static final By btnDelete = By.xpath("//*[@data-qa='category-delete-trigger']");
	 public static final By delYesBtn = By.xpath("//*[@data-qa='delete-ok-trigger']");
	 public static final By delNoBtn = By.xpath("//*[@data-qa='delete-cancel-trigger']");
	 public static final By successMsgOfDel = By.xpath("//*[@data-qa='delete-success-msg']");
	 public static final By iconGreaterthan = By.xpath("//*[@data-qa='category-edit-trigger']");
	 
	 public static final By categoryID = By.xpath("//*[@data-qa='category-id']");
	 public static final By createCategory = By.xpath("//*[@data-qa='create-category-btn']");
	 public static final By txtCategoryName = By.xpath("//*[@data-qa='category-name']");
	 public static final By txtDescription = By.xpath("//*[@data-qa='category-description']");
	 public static final By dropdownType = By.xpath("//*[@data-qa='category-type']");
	 public static final By status = By.xpath("//*[@data-qa='edit-status-label']");
	 public static final By statusactive = By.xpath("//*[@data-qa='cat-status-active']");
	 public static final By statusInactive = By.xpath("//*[@data-qa='cat-status-inactive']");
	 public static final By dropdownIsVisible = By.xpath("//*[@data-qa='category-is-visible']");
	 public static final By txtSemanticID = By.xpath("//*[@data-qa='semantic-id']");
	 public static final By txtMetaTitle = By.xpath("//*[@data-qa='meta-title']");
	 public static final By txtMetaDesc = By.xpath("//*[@data-qa='meta-description']");
	 public static final By txtMetaKeywrd = By.xpath("//*[@data-qa='meta-keyword']");
	 public static final By btnAddSemanId = By.xpath("//*[@data-qa='btn-add-semantic']");
	 public static final By btnAddMetaTitle = By.xpath("//*[@data-qa='btn-add-metatitle']");
	 public static final By btnAddMetaDesc = By.xpath("//*[@data-qa='btn-add-metadescription']");
	 public static final By btnAddMetaKey = By.xpath("//*[@data-qa='btn-add-metakeyword']");
	 public static final By CreateCategory = By.xpath("//*[@data-qa='create-trigger']");
	 public static final By cancelCreate = By.xpath("//*[@data-qa='cancel-trigger']");
	 public static final By btnAddProperty = By.xpath("//*[@data-qa='btn-add-property']");
	 public static final By mandPropName = By.xpath("//*[@data-qa='mandatory-prop-name']");
	 public static final By mandPropValue = By.xpath("//*[@data-qa='mandatory-prop-value']");
	 public static final By propName = By.xpath("//*[@data-qa='optional-prop-name']");
	 public static final By propValue = By.xpath("//*[@data-qa='optional-prop-value']");
	 
	 public static final By btnCreateNewProject = By.xpath("//*[@data-qa='create-project-trigger']");
	 public static final By newProject = By.xpath("//*[@data-qa='my-new-project-input']");
	 public static final By createProject = By.xpath("//*[@data-qa='create-trigger']");
	 public static final By btnCancel = By.xpath("//*[@data-qa='cancel-trigger']");
	 public static final By projectListItem = By.xpath("//*[@data-qa='existing-project-item']");
	 public static final By txtSuccessMsg= By.xpath("//*[@data-qa='project-create-success']");
	 
	 public static final By editIcon = By.xpath("//*[@data-qa='category-edit-trigger']");
	 public static final By txtCategoryID = By.xpath("//*[@data-qa='category-id']");
	 public static final By categoryName = By.xpath("//*[@data-qa='category-name']");
	 public static final By description = By.xpath("//*[@data-qa='category-description']");
	 public static final By statusedit = By.xpath("//*[@data-qa='category-status']");
	 public static final By type = By.xpath("//*[@data-qa='category-Type']");
	 public static final By typeStatic = By.xpath("//*[@data-qa='cat-type-static']");
	 public static final By statusActive = By.xpath("//*[@data-qa='cat-status-active']");
	 public static final By isVisible = By.xpath("//*[@data-qa='category-is-visible']");
	 public static final By isVisibleYes = By.xpath("//*[@data-qa='cat-visible-yes']");
	 public static final By semanticID = By.xpath("//*[@data-qa='semantic-id']");
	 public static final By metaTitle = By.xpath("//*[@data-qa='meta-title']");
	 public static final By metaDesc = By.xpath("//*[@data-qa='meta-description']");
	 public static final By metaKeywrd = By.xpath("//*[@data-qa='meta-keyword']");
	 public static final By addSemanId = By.xpath("//*[@data-qa='btn-add-semantic']");
	 public static final By addMetaTitle = By.xpath("//*[@data-qa='btn-add-metatitle']");
	 public static final By adMetaDesc = By.xpath("//*[@data-qa='btn-add-metadescription']");
	 public static final By addMetaKey = By.xpath("//*[@data-qa='btn-add-metakeyword']");
	 public static final By cancelInEdit = By.xpath("//*[@id='btnCancel']");
	 public static final By addProperty = By.xpath("//*[@data-qa='btn-add-property']");
	 public static final By propNameEdit = By.xpath("//*[@data-qa='adv-prop-name']");
	 public static final By propValueEdit = By.xpath("//*[@data-qa='adv-prop-value']");
	 public static final By btnSave = By.xpath("//*[@data-qa='save-trigger']");
	 
	 public static final By btnDetails = By.xpath("//*[@data-qa='details-trigger\"']");
	 public static final By editProject = By.xpath("//*[@data-qa='projectdetails-edit-trigger']");
	 public static final By prjctDescription = By.xpath("//*[@data-qa='description-input']");
	 public static final By btnSaveProjectdetails = By.xpath("//*[@data-qa='projectdetails-save-trigger']");
	 public static final By txtPDPProjectName = By.xpath("//*[@data-qa='name-input']");
	 public static final By btnSubmit = By.xpath("//*[@data-qa='projectdetails-submit-trigger']");
	 public static final By btnApprove = By.xpath("//*[@data-qa='projectdetails-approve-trigger']");
	
	 public static final By projectNameInp = By.xpath("//*[@data-qa='project-name-input']");
	 
}