package com.skava.object.repository;

import org.openqa.selenium.By;

public class InviteUserPage {
	
	public static final By emailTxtBox=By.xpath("//*[@data-qa='email-input']");
	public static final By emailRemoveIcon=By.xpath("//*[@data-qa='remove-icon']");
	public static final By emailLabel=By.xpath("//*[@data-qa='email-label']");
	public static final By instanceAdminLabel=By.xpath("//*[@data-qa='instance-admin-label']");
	public static final By instanceAdminBtn=By.xpath("//*[@data-qa='instance-admin-trigger']");
	public static final By adminLLabel=By.xpath("//*[@data-qa='admin-label']");
	public static final By adminBtn=By.xpath("//*[@data-qa='admin-trigger']");
	public static final By businessList=By.xpath("//*[@data-qa='business-list-items']");
	public static final By businessCheckbox=By.xpath("//*[@data-qa='business-checkbox']");
	public static final By businessCheckbox2=By.xpath("(//*[@data-qa='business-checkbox'])[2]");
	public static final By businessName=By.xpath("//*[@data-qa='business-name-text']");
	public static final By rolesSection=By.xpath("//*[@data-qa='roles-container']");
	public static final By inactiveRolesSection=By.xpath("//*[contains(@class,'InactiveContainer')]");
	public static final By standardRolesTab=By.xpath("//*[@data-qa='standard-roles-tab']");
	public static final By customRolesTab=By.xpath("//*[@data-qa='custom-roles-tab']");
	public static final By sendInviteBtn=By.xpath("//*[@data-qa='send-invite-trigger']");
	public static final By inviteSuccessMsg=By.xpath("//*[@data-qa='invite-success-msg']");
	public static final By businessAdminToggle=By.xpath("//*[@data-qa='admin-trigger']");
	public static final By teamBreadcrumb=By.xpath("//*[@data-qa='breadcrumb-trigger']");
	public static final By emailInvalidErrorMsg=By.xpath("(//*[contains(@class,'EmailErrorTxt')])[2]");
    public static final By userExistingMsg=By.xpath("(//*[contains(@class,'ErrResMsg')])[2]");
    public static final By emailEmptyErrorMsg=By.xpath("(//*[contains(@class,'EmailErrorTxt')])[1]");
   
}
