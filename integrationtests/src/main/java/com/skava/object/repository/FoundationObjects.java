package com.skava.object.repository;
 
import org.openqa.selenium.By;
 
 public class FoundationObjects
 {
	 public static final By createCollectionBtn = By.xpath("//*[@data-qa='create-collection-trigger']");
	 public static final By collectionName = By.xpath("//*[@data-qa='collection-name-input']");
	 public static final By collectionDescription = By.xpath("//*[@data-qa='collection-desc-input']");
	 public static final By collectionStatus = By.xpath("//*[@data-qa='store-status-toggle']");
	 public static final By collectionSubmitButton = By.xpath("//*[@data-qa='save-trigger']");
	 public static final By collectionCreationSuccess=By.xpath("//*[@data-qa='success-msg']");
	 
	 public static final By collectionList = By.xpath("(//*[@data-qa='collection-detail-row'])");
	 public static final By btn_Launch = By.xpath("//*[@data-qa='quick-launch-trigger']");
	 public static final By loadingGuage = By.id("id_loader");
	 public static final By collectionNameContainer = By.xpath("//*[@data-qa='collection-name-filter']//*[@data-qa]");
	 public static final By txt_collectionName = By.xpath("//*[@data-qa='collection-name-input']");
	 public static final By btn_search = By.xpath("//*[@data-qa='search-trigger']");
}