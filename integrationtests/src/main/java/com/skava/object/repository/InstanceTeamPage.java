package com.skava.object.repository;

import org.openqa.selenium.By;

public class InstanceTeamPage {
	
	public static final By emailInput = By.xpath("//*[@data-qa='user-name-input']");
	public static final By pwdInput = By.xpath("//*[@data-qa='password-input']");
	public static final By signInBtn = By.xpath("//*[@data-qa='signin-trigger']");
	public static final By instanceTeamMenu = By.xpath("//*[@data-qa='instance-team']");
	public static final By instanceTeamTitle = By.xpath("//*[@data-qa='team-title']");
	public static final By firstNameDet = By.xpath("//*[@data-qa='user-record-row']//td[2]");
	public static final By lastNameDet = By.xpath("//*[@data-qa='user-record-row']//td[3]");
	public static final By emailDet = By.xpath("//*[@data-qa='user-record-row']//td[4]");
	public static final By businessDet = By.xpath("//*[@data-qa='user-record-row']//td[5]");
	public static final By firstNameCol = By.xpath("//*[@data-qa='fname-sort']");
	public static final By lastNameCol = By.xpath("//*[@data-qa='lname-sort']");
	public static final By emailCol = By.xpath("//*[@data-qa='email-label']");
	public static final By businessCol = By.xpath("//*[@data-qa='business-label']");
	public static final By statusCol = By.xpath("//*[@data-qa='status-label']");
	public static final By inviteBtn = By.linkText("Invite Members");
	public static final By fnameFilter = By.xpath("//*[@data-qa='firstname-list']");
	public static final By lnameFilter = By.xpath("//*[@data-qa='lastname-list']");
	public static final By emailFilter = By.xpath("//*[@data-qa='email-list']");
	public static final By fnameSearchInput = By.xpath("//*[@data-qa='firstname-input']");
	public static final By searchBtn = By.xpath("//*[@data-qa='search-trigger']");
	public static final By lnameSearchInput = By.xpath("//*[@data-qa='lastname-input']");
	public static final By emailSearchInput = By.xpath("//*[@data-qa='email-input']");
	public static final By paginationCont = By.xpath("//*[@data-qa='pagination-trigger']");
	public static final By rowDropDown = By.xpath("//*[@data-qa='row-list rows-list']");
	public static final By userRecordList = By.xpath("//*[@data-qa='user-record-row']");
	public static final By editUser=By.xpath("//*[@data-qa='view-trigger']");
	public static final By userInactiveStatus=By.xpath("//*[@data-qa='status-inactive']");
	public static final By userActiveStatus=By.xpath("//*[@data-qa='status-active']");

	public static final By userRole=By.xpath("//*[@data-qa='roles-text']");
	public static final By viewMoreLink=By.xpath("//*[contains(@class,'ViewMoreBusinessesLink')]");
	
	//Paramesh
	public static final By membersResetBtn=By.xpath("//*[@data-qa='reset-trigger']");
	public static final By loadingGauage=By.xpath("//*[@class='fixed-top w-100 h-100 text-center align-middle bg-light modal-backdrop show']");
}
