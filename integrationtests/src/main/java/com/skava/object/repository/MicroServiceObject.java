package com.skava.object.repository;

import org.openqa.selenium.By;

public class MicroServiceObject 
{	
	public static final By MicroService_Title	 		= By.xpath("//*[@data-qa='all-microservices-title']");
	public static final By MicroService_Size	 		= By.xpath("(//*[@data-qa='microservices-tile'])");
	public static final By Createcollection_Btn	 		= By.xpath("(//*[@data-qa='create-collection-trigger'])");
	public static final By Collection_Name		 		= By.xpath("//*[@data-qa='collection-name-input']");
	public static final By Collection_Desc		 		= By.xpath("//*[@data-qa='collection-desc-input']");
	public static final By Status_Toggle		 		= By.xpath("//*[@data-qa='store-status-toggle']");
	public static final By Collection_Create_Btn 		= By.xpath("//*[@data-qa='save-trigger']");
	public static final By Microservice_Breadcrumb 		= By.xpath("(//*[@data-qa='business-overview-page-breadcrum'])[2]");
	public static final By Configure_Btn		 		= By.xpath("(//*[contains(text(),'Configure')])");
	public static final By Update_Collection_Save_Btn	= By.xpath("//*[contains(text(),'Save')]");
	public static final By Microservices_breadcrumb		= By.xpath("(//*[@data-qa='business-overview-page-breadcrum'])[2]/a//*[contains(text(),'Microservices')]");
	public static final By Collection_Container			= By.xpath("//*[@class='dropdown-content select-dropdown active']");
	public static final By edit_microservice_brdcrumb	= By.xpath("(//*[contains(text(),'Microservices')])[2]");	
	public static final By UpdateCollection_Save_Btn	= By.xpath("//*[contains(text(),'Save')]");
	public static final By eint_collection_sve			= By.xpath("(//*[@data-qa='save-trigger'])");
	
	// Priya - Nov 30
	public static final By microservice_txt				= By.xpath("(//*[@data-qa='microservices-tile']/a/div/div/span)");
	
	// Dec 3rd
	public static final By loading_gauge				= By.xpath("//*[@class='fixed-top w-100 h-100 text-center align-middle bg-light modal-backdrop show']");
	
	
}