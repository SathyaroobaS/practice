package com.skava.object.repository;

import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;

public class promotionObject 

{

	public static final By CreatePromotionGroup = By.xpath("//*[@data-qa='createpromotiongroup-trigger']");
	public static final By groupnamevalue = By.xpath("//*[@data-qa='groupname-value']");
	public static final By daterange = By.xpath("//*[contains(@data-qa,'daterangeandtime-value')]");
	public static final By leftsiderange = By.xpath("//*[@class='drp-calendar left']");
	public static final By leftsideavail = By.xpath("//*[@class='drp-calendar left']//td[contains(@class,'available')]");
	public static final By rightsiderange = By.xpath("//*[@class='drp-calendar right']");
//	public static final By rightsideavail = By.xpath("//*[@class='drp-calendar right']//*[@class='available' or @class='in-range available' or @class='active start-date available']");
	public static final By rightsideavail = By.xpath("//*[@class='drp-calendar right']//td[contains(@class,'available')]");
	public static final By statuslabel = By.xpath("//*[@data-qa='status-label']");
	public static final By statuslist = By.xpath("//*[@data-qa='status-list']");
	public static final By statusactive =By.xpath("(//*[@data-qa='status-list']//ul//li)[2]");
	public static final By prioritylabel = By.xpath("//*[@data-qa='priority-label']");
	public static final By priorityvalue = By.xpath("//*[@data-qa='priority-value']//input");
	public static final By stopfurtherprocessinglabel = By.xpath("//*[@data-qa='stopfurtherprocessing-label']");
	public static final By stopfurtherprocessinglist = By.xpath("//*[@data-qa='stopfurtherprocessing-list']");
	public static final By savetrigger = By.xpath("//*[@data-qa='save-trigger']");
	public static final By canceltrigger = By.xpath("//*[@data-qa='cancel-trigger']");
	public static final By toastmessage = By.xpath("//*[@data-qa='toast-message']");
	public static final By promotionruletypelist = By.xpath("//*[contains(text(),'Offer On')]");
	public static final By promotionruletypelistinput = By.xpath("//*[contains(text(),'Offer On')]/preceding::input[1]");
	public static final By promotionactiontypelist = By.xpath("//*[contains(text(),'Offer Type')]");
	public static final By promotionactiontypelistinput = By.xpath("//*[contains(text(),'Offer Type')]/preceding::input[1]");
	
	public static final By createPromotionButton = By.xpath("//*[@data-qa='createpromotion-trigger']");
	public static final By applyButtonDatePicker = By.xpath("/html/body/div[3]/div[4]/button[2]");
	public static final By promotionGroupListHeader = By.xpath("//*[@data-qa='promotiongrouplistpromogroup-trigger']");
	
	//promotion creation
	
	public static final By promotionnamelabel = By.xpath("//*[@data-qa='promotionname-label']");
	public static final By promotionnameinput = By.xpath("//*[@data-qa='promotionname-label']//input");
	public static final By promotioncontianer = By.xpath("//*[@data-qa='promotion-container']");
	public static final By promotionsave = By.xpath("//*[@data-qa='promotionsave-trigger']");
	public static final By promotioncancel = By.xpath("//*[@data-qa='promotioncancel-trigger']");
	public static final By promotionstartdateandtimelabel = By.xpath("//*[@data-qa='promotionstartdatetime-label']");
	//public static final By promotionstartdateandtimetrigger = By.xpath("//*[@data-qa='promotionstartdatetime-datepicker']");
	//public static final By promotionenddateandtimelabel = By.xpath("//*[@data-qa='promotionenddatetime-label']");
	//public static final By promotionenddateandtimetrigger = By.xpath("//*[@data-qa='promotionenddatetime-datepicker']");
	//public static final By promotionstopfurtherprocessingtype = By.xpath("//*[@data-qa='promotionstopfurtherprocessing-label']");
	public static final By promotionstatuslabel = By.xpath("//*[@data-qa='promotionstatus-label']");
	public static final By promotionstatusdropdown = By.xpath("//*[@data-qa='promotionstatus-list']//input");
	public static final By promotionprioritylabel = By.xpath("//*[@data-qa='promotionpriority-label']");
	public static final By promotionprioritytextbox = By.xpath("//*[@data-qa='promotionpriority-value']/input");
	public static final By promotionmessagelabel = By.xpath("//*[@data-qa='promotionmessage-label']");
	public static final By promotionmessagetextbox = By.xpath("//*[@data-qa='promotionmessage-label']//textarea");
	public static final By promotiondescriptionlabel = By.xpath("//*[@data-qa='promotiondescription-label']");
	public static final By promotiondescriptiontextbox = By.xpath("//*[@data-qa='promotiondescription-label']//textarea");
	
	
	public static final By promotionEditButton = By.xpath("//*[@data-qa='editpromotion-trigger']");
	public static final By firstAddRuleButton = By.xpath("//div[@id='id_skQueryBuilder']//button[@data-add='rule']");
	
	public static final By promotionconditions = By.xpath("//*[@data-qa='promotionconditions-tab-trigger']");
	public static final By conditionEdit = By.xpath("(//button[text()='Edit'])[2]");
	public static final By createconditions = By.xpath("//*[@data-qa='createcondition-trigger']");
	public static final By promotionactions = By.xpath("//*[text()='Actions']");
	public static final By actionsEdit=By.xpath("(//button[text()='Edit'])[3]");
	public static final By createactions = By.xpath("//*[@data-qa='createaction-trigger']");
	public static final By promotionPromoCodes = By.xpath("//*[text()='Promo Codes']");
	public static final By promocodeEditButton = By.xpath("//*[@data-qa='promocodeedit-trigger']");
	public static final By promocodeType = By.xpath("//*[@data-qa='promocodetype-list']");
	public static final By promocodeList = By.xpath("//*[@data-qa='promoCodeFromList-list']");
	public static final By promocodemaxusage = By.xpath("//*[@data-qa='promocodecount-value']//input");
	public static final By promocodeNoOfDays = By.xpath("//*[@data-qa='promocodedayscount-value']//input");
	public static final By promocodeSave = By.xpath("//*[@data-qa='promocodesave-trigger']");
	
	
	public static final By rulesgroup = By.xpath("//*[@value='------']");
	public static final By actiongroup = By.xpath("(//*[@value='------'])[2]");
	public static final By addrule = By.xpath("//*[@data-add='rule']");
	public static final By actionrule = By.xpath("(//*[@data-add='rule'])[2]");
	
	public static final By promotionEditAndSave = By.xpath("(//*[text()='Save'])[1]");
	public static final By Save = By.xpath("(//*[text()='Save'])[2]");
	public static final By actionSave = By.xpath("(//*[text()='Save'])[2]");
	public static final By Update = By.xpath("(//button[text()='Edit'])[2]");
	//public static final By Updatevalue = By.xpath("//*[contains(text(),'sku_id')]");
	public static final By Updatevalue = By.xpath("(//*[contains(text(),'Condition')])[2]/following::div[1]");

	
	
	
	
	
	public static final By ORButton = By.xpath("//input[@value='OR']");
	public static final By ANDButton = By.xpath("//input[@value='AND']");
	public static final By actionORButton = By.xpath("(//input[@value='OR'])[2]");
	public static final By actionANDButton = By.xpath("(//input[@value='AND'])[2]");
	
	public static final By actionname = By.xpath("//*[@data-qa='actionname-value']");
	public static final By actiondescription = By.xpath("//*[@data-qa='actionmessage-value']");
	public static final By actionClassName = By.xpath("//*[text()='Action Type']/preceding::input[1]");
	public static final By offerValue = By.xpath("//*[@name='offerValue']");
	public static final By stopfurtherprocessing = By.xpath("//*[text()='Stop Further Processing']/preceding::input[1]");
	
	
	public static final By createPromocodeList = By.xpath("//*[@data-qa='createpromocodelist-trigger']");
	public static final By PromocodeListName = By.xpath("//*[@data-qa='promocodelist-value']//input");
	public static final By AddPromocode = By.xpath("//*[@data-qa='addpromocode-label']");
	public static final By DisplayCode = By.xpath("//*[@data-qa='displaycode-value']//preceding::input[1]");
	public static final By promocodelistpancakemenu = By.xpath("//*[@data-qa='pancakemenu-trigger']");
	public static final By promotionstab = By.xpath("//*[@data-qa='Promotions-menu-trigger']");
	
	public static final By createnewproject = By.xpath("//*[@data-qa='createnewproject-trigger']");
	public static final By newprojectname = By.xpath("//*[@data-qa='newprojectname-value']//input");
	public static final By createprojectBtn = By.xpath("//*[@data-qa='createproject-trigger']");
	
	
	
	//*[text()='Testing']/following::a[1]
	public static final By header = By.xpath("//*[@data-qa='servicename-label']");
	public static final By pancakemenu = By.xpath("//*[@data-qa='servicename-label']");
	public static final By projectnamelabel = By.xpath("//*[@data-qa='projectname-label']");
	public static final By promocodelistlabel = By.xpath("//*[@data-qa='promocodelist-label']");
	public static final By promocodelistmenu = By.xpath("//*[@data-qa='PromocodeList-menu-trigger']");
	public static final By chevron_right = By.xpath("//*[@data-qa='projectenter-trigger']//following::a[1]");
	public static final By submitProject = By.xpath("//*[@data-qa='projectdetails-submit-trigger']");
	public static final By projectdetails = By.xpath("//*[@data-qa='projectdetails-trigger']");
	public static final By approvedProject = By.xpath("//*[@data-qa='projectdetails-approved-trigger']");
	public static final By reopen = By.xpath("//*[@data-qa='projectdetails-reopen-trigger']");
	public static final By deny = By.xpath("//*[@data-qa='projectdetails-deny-trigger']");
	public static final By statelabel = By.xpath("//*[@data-qa='projectlist-state-label']");
	
	
	
	
	
	
	public static final By usernamelabel = By.xpath("//*[contains(text(),'Hi')]");
	public static final By HeaderHi=By.xpath("//*[@data-qa='support-list']");
	public static final By logout = By.xpath("//*[@data-qa='logout-trigger']");
	public static final By projectdetailsedit = By.xpath("//*[@data-qa='projectdetails-edit-trigger']");
	public static final By description_input = By.xpath("//*[@data-qa='description-input']");
	public static final By addnote = By.xpath("//a[text()='Notes']");
	public static final By addnotevalue = By.xpath("//*[@data-qa='displaycode-value']/input");
	public static final By addnotes = By.xpath("//*[@data-qa='add-notes-trigger']");
	public static final By projectdetailsave = By.xpath("//*[@data-qa='projectdetails-save-trigger']");

	public static final By promtionEditBtn = By.xpath("//*[@data-qa='editpromotion-trigger']");
	public static final By promtionMessage = By.xpath("//*[@data-qa='promotionmessage-value']");
	public static final By promtionDescription = By.xpath("//*[@data-qa='promotiondescription-value']");
	public static final By promtionStatusSelect = By.xpath("(//*[@class='select-wrapper mdb-select colorful-select dropdown-primary'])[1]");
	public static final By pmInactiveSelect = By.xpath("(//*[@class='filtrable'])[8]");
	public static final By promotionEditSave = By.id("id_skGeneralSave");
	public static final By existingProjectOpen = By.xpath("//*[@data-qa='projectenter-trigger']");
	public static final By existingProjectName = By.xpath("//*[@data-qa='project-name-value']");

	//Gokul
	public static final By action_click = By.xpath("//*[@class='btn btn-sm rounded black-text border waves-effect waves-light my-0']");
	public static final By delete_promotion = By.xpath("//*[@id='id_deletePromotion']");
	public static final By promo_click = By.xpath("//*[@data-qa='promotiongroupname-breadcrumb']");
	
	public static final By submit_notes = By.xpath("//*[@id='id_skAddNote']");
//	public static final By submit_notes = By.xpath("//*[@data-qa='projectId-txt']");
	public static final By buyitemquantity = By.xpath("//*[text()='Buy Item Quantity']//preceding::input[1]");
	public static final By getitemquantity = By.xpath("//*[text()='Get Item Quantity']//preceding::input[1]");
	public static final By giftproductid = By.xpath("//*[text()='Gift Product Id']//preceding::input[1]");
	public static final By giftskuid = By.xpath("//*[text()='Gift Sku Id']//preceding::input[1]");
	public static final By buyXItemQuantity = By.xpath("//*[text()='Buy X Item Quantity']//preceding::input[1]");
	public static final By buyYItemQuantity = By.xpath("//*[text()='Buy Y Item Quantity']//preceding::input[1]");
	
	
//promotion list	
	public static final By promotionList = By.xpath("//*[@data-qa='promotionlist-menu-trigger']");
	public static final By createListButton = By.xpath("//*[@data-qa='createlist-trigger']");
	public static final By newListName = By.xpath("//*[@data-qa='promotionlist-value']//input");
	public static final By listStatus = By.xpath("//*[@data-qa='promotionliststatus-list']");
	public static final By Liststatusactive =By.xpath("(//*[@data-qa='promotionliststatus-list']//ul//li)[2]");
	public static final By liststatusinactive =By.xpath("(//*[@data-qa='promotionliststatus-list']//ul//li)[3]");
	
	public static final By promotionlisttype = By.xpath("//*[@data-qa='listtype-list']");
	public static final By promotionlisttypeinput = By.xpath("(//*[@data-qa='listtype-list']//ul/li)");
	public static final By promotionfieldname = By.xpath("//*[@data-qa='fieldname-list']");
	public static final By promotionfieldnameinput = By.xpath("//*[@data-qa='fieldname-list']//ul//li");
	public static final By promotionCloneBtn = By.xpath("//*[@data-qa='clone-trigger']");
	public static final By promotiongrouplist = By.xpath("//*[@data-qa='promotiongroup-list']");
	
//update promotion list
	public static final By promotionlistchevron = By.xpath("//*[@data-qa='listaction-view-trigger']");
	public static final By promotionAddListItemBtn = By.xpath("//*[@data-qa='addListItem-label']//*[text()='Add List Item']");
	public static final By promotionlistItem = By.xpath("//*[@data-qa='listItem-value']//input");
	public static final By promotionAddistItemField = By.xpath("//*[@data-qa='addListItem-trigger']");
	public static final By promotionBreadCrumb=By.xpath("//*[@data-qa='promotiontext-breadcrumb']//*[contains(text(),'All Groups')]");
	public static final By actionButton=By.xpath("//*[@data-qa='promotiongrouplistaction-list']");
	public static final By actiondeleteButton=By.xpath("//*[@data-qa='promotiongrouplistaction-delete-trigger']");
	public static final By deleteYesButton=By.xpath("//*[@data-qa='yes-button']");
	public static final By ruledropdown=By.xpath("(//*[@class='rules-group-body']//input)[1]");
	public static final By ruleoperand=(By.xpath("(//*[@class='rules-group-body']//input)[2]"));
	public static final By purchaseQuantity=(By.xpath("//*[contains(@name,'purchaseQty')]"));
	public static final By actionAddUpdateButton=(By.xpath("//*[text()='Add / Update']"));
	public static final By localeDropDown=(By.xpath("(//*[contains(@class,'cls_skAddLocaleRow')]//*[contains(@class,'select-dropdown')])[3]"));
	public static final By localeDropDownSelect=(By.xpath("//*[contains(@class,'dropdown-primary invalid')]/ul/li[2]/span"));
	public static final By localeDisplayMessage=(By.xpath("(//*[@data-qa='promotionmessage-label']/textarea)[2]"));
	public static final By promotionListSaveWaiting=(By.xpath("//*[@data-qa='createpromotion-trigger']|//*[@data-qa='createlist-trigger']|//*[@data-qa='createpromocodelist-trigger']"));
	public static final By toastMessage=(By.xpath("//*[@class='toast-message']"));

	 
	//     (//*[@data-qa='listtype-list']//ul/li)//*[text()='Sku']
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}























