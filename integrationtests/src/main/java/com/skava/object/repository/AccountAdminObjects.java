package com.skava.object.repository;

import org.openqa.selenium.By;

public class AccountAdminObjects {

	public static final By tab 						= By.xpath("//*[@data-qa='microservices-item']");
	public static final By accounttitle 			= By.xpath("//*[@data-qa='account-title']");
	public static final By accounttab 				= By.xpath("//*[@data-qa='account-trigger']");
	public static final By accountName 				= By.xpath("//*[@data-qa='sort-account-name-trigger']");
	public static final By accountId 				= By.xpath("//*[@data-qa='sort-account-id-trigger']");
	public static final By accountType 				= By.xpath("//*[@data-qa='sort-account-type-trigger']");
	public static final By accounttypefilter 		= By.xpath("//*[@data-qa='type-search']");
	public static final By accountlistitem 			= By.xpath("//*[@data-qa='accountlist-item']");
	public static final By accountlistitems 			= By.xpath("//*[@data-qa='accountlist-item']//td");
		public static final By accountnodataalert 		= By.xpath("//*[@data-qa='no-data-error-alert']");
	public static final By accountstatusfilter 		= By.xpath("//*[@data-qa='status-search']");
	public static final By accountRep				= By.xpath("//*[@data-qa='sort-account-rep-trigger']");
	public static final By buyerAdmin				= By.xpath("//*[@data-qa='sort-buyer-admin-trigger']");
	public static final By paymentTerms				= By.xpath("//*[@data-qa='sort-account-payment-terms-trigger']");
	public static final By accountStatus			= By.xpath("//*[@data-qa='sort-account-status-trigger']");
	public static final By accountCredit			= By.xpath("//*[@data-qa='sort-account-credit-standing-trigger']");
	public static final By accountpagesearch		= By.xpath("//*[@data-qa='search-trigger']");

	public static final By searchCloseIcon			= By.xpath("//*[@data-qa='search-close-trigger']");
	public static final By footer					= By.xpath("//*[@data-qa='footer-item']");
	public static final By rowDropDown 				= By.xpath("//*[@data-qa='rows-list']");
	public static final By pagination 				= By.xpath("//*[@data-qa='pagination-item']");
	public static final By noOrders 				= By.xpath("//*[@data-qa='no-order-item']");
	
	// Orders Tab
	public static final By headerTab 				= By.xpath("//*[@data-qa='customers-tab-trigger']");
	public static final By ordersTab 				= By.xpath("//*[@data-qa='orders-tab-trigger']");
	public static final By orderTable 				= By.xpath("//*[@data-qa='orders-table']");
	
	public static final By allTypes 				= By.xpath("//*[@data-qa='all-types-list']");
	public static final By activeOrders 			= By.xpath("//*[@data-qa='active-orders-list]");
	public static final By filter3 					= By.xpath("//*[@data-qa='filter-3-list']");
	public static final By SearchBar 				= By.xpath("//*[@data-qa='search-input']");
	public static final By SearchButton 			= By.xpath("//*[@data-qa='search-trigger']");
	public static final By SearchInputCloseIcon 	= By.xpath("//*[@data-qa='search-close-trigger']");
	
	public static final By Customer 				= By.xpath("//*[@data-qa='sort-customer-list']");
	public static final By CustomerSort 			= By.xpath("//*[@data-qa='sort-customer-list']");
	public static final By AccountRep 				= By.xpath("//*[@data-qa='account-rep-container']");
	public static final By AccountRepSort 			= By.xpath("//*[@data-qa='sort-account-rep-list']");
	public static final By order 					= By.xpath("//*[@data-qa='sort-order-list']");
	public static final By orderSort 				= By.xpath("//*[@data-qa='sort-order-list']");
	public static final By PO 						= By.xpath("//*[@data-qa='sort-po-list']");
	public static final By POSort 					= By.xpath("//*[@data-qa='sort-po-list']");
	public static final By Created 					=  By.xpath("//*[@data-qa='sort-created-list']");
	public static final By CreatedSort 				= By.xpath("//*[@data-qa='sort-created-list']");
	public static final By Updated 					= By.xpath("//*[@data-qa='sort-updated-list']");
	public static final By UpdatedSort 				= By.xpath("//*[@data-qa='sort-updated-list']");
	public static final By orderStatus 				= By.xpath("//*[@data-qa='sort-order-status-list']");
	public static final By orderStatusSort 			= By.xpath("//*[@data-qa='sort-order-status-list']");
	public static final By paymentStatus 			= By.xpath("//*[@data-qa='sort-payment-status-list']");
	public static final By paymentStatusSort 		= By.xpath("//*[@data-qa='sort-payment-status-list']");	
	public static final By CreatedVal 				= By.xpath("//*[@data-qa='created-col-value']");
	public static final By UpdatedVal 				= By.xpath("//*[@data-qa='updated-col-value']");
	public static final By orderStatusVal 			= By.xpath("//*[@data-qa='order-status-col-value']");
	public static final By paymentStatusVal 		= By.xpath("//*[@data-qa='payment-status-col-value']");
	
	public static final By noOfRows 				= By.xpath("//*[@data-qa='no-of-rows-list']");
	public static final By previousPage 			= By.xpath("//*[@data-qa='previous-page-trigger']");
	public static final By nextPage 				= By.xpath("//*[@data-qa='next-page-trigger']");
	
	// Customer Tab
	public static final By customersTab 			= By.xpath("//*[@data-qa='customers-tab-trigger']");
	public static final By customersTable 			= By.xpath("//*[@data-qa='customers-table']");
	public static final By allCostCentres 			= By.xpath("//*[@data-qa='all-cost-centers-list']");
	public static final By allTeams 				= By.xpath("//*[@data-qa='all-teams-list']");
	public static final By allTotalSpends 			= By.xpath("//*[@data-qa='all-total-spends-list']");
	public static final By firstNameTitle 			= By.xpath("//*[@data-qa='sort-firstname-list']");
	public static final By firstNameSort 			= By.xpath("//*[@data-qa='sort-firstname-list']");
	//public static final By firstNameValue 		= By.xpath("//*[@data-qa='first-name-value']");
	public static final By lastNameTitle 			= By.xpath("//*[@data-qa='sort-lastname-list']");
	public static final By lastNameSort 			= By.xpath("//*[@data-qa='sort-lastname-list']");
	//public static final By lastNameValue 			= By.xpath("//*[@data-qa='last-name-value']");
	public static final By emailAddressTitle 		= By.xpath("//*[@data-qa='sort-emailaddress-list']");
	public static final By emailAddressSort 		= By.xpath("//*[@data-qa='sort-emailaddress-list']");
	//public static final By emailAddressValue 		= By.xpath("//*[@data-qa='email-address-value']");
	public static final By teamsTitle 				= By.xpath("//*[@data-qa='sort-teams-list']");
	public static final By teamsSort 				= By.xpath("//*[@data-qa='sort-teams-list']");
	public static final By teamsValue 				= By.xpath("//*[@data-qa='teams-value']");
	public static final By costCenterTitle 			= By.xpath("//*[@data-qa='sort-costcenter-list']");
	public static final By costCenterSort 			= By.xpath("//*[@data-qa='sort-costcenter-list']");
	public static final By costCenterValue 			= By.xpath("//*[@data-qa='cost-center-value']");
	public static final By totalSpendTitle 			= By.xpath("//*[@data-qa='sort-totalspend-list']");
	public static final By totalSpendSort 			= By.xpath("//*[@data-qa='sort-totalspend-list']");
	public static final By totalSpendValue 			= By.xpath("//*[@data-qa='total-spend-value']");
			
	// Overview tab
	public static final By summaryInformation 			= By.xpath("//*[@data-qa='summary-item']");
	public static final By totalAvailableCredit 		= By.xpath("//*[@data-qa='total-avail-credit']");
	public static final By totalAvailableCreditInput 	= By.xpath("//*[@data-qa='total-avail-credit-input']");
	public static final By totalOrderValue 				= By.xpath("//*[@data-qa='total-order-value']");
	public static final By totalOrderValueInput 		= By.xpath("//*[@data-qa='total-order-value-input']");
	public static final By totalOrderPastDue 			= By.xpath("//*[@data-qa='total-order-past-due']");
	public static final By totalOrderPastDueInput 		= By.xpath("//*[@data-qa='total-order-past-due-input']");
	
	public static final By accountView					= By.xpath("//*[@data-qa='account-view-trigger']");
	public static final By accountOverviewBreadcrumb 	= By.xpath("//*[@data-qa='breadcrumb-label']");
	public static final By notificationsSection			= By.xpath("//*[@data-qa='notifications-item']");
	public static final By managementSection 			= By.xpath("//*[@data-qa='management-item']");
	public static final By summarySection 				= By.xpath("//*[@data-qa='summary-item']");
	public static final By accountInformationSection 	= By.xpath("//*[@data-qa='account-information-item']");
	public static final By accountRepInput 				= By.xpath("//*[@data-qa='account-rep-container']//input");
	public static final By accountRepcontainer 			= By.xpath("//*[@data-qa='account-rep-container']");
	public static final By status 						= By.xpath("//*[@data-qa='status-container']");
	
	public static final By accountNameInput 			= By.xpath("//*[@data-qa='account-name-input']");
	public static final By taxIdInput 					= By.xpath("//*[@data-qa='taxid-input']");
	public static final By DUNSNumberInput 				= By.xpath("//*[@data-qa='duns-number-input']");
	public static final By buyerAdminNameInput 			= By.xpath("//*[@data-qa='buyer-admin-name-input']");
	public static final By buyerAdminEmailInput 		= By.xpath("//*[@data-qa='buyer-admin-email-input']");
	public static final By accountAddressInput 			= By.xpath("//*[@data-qa='account-address-input']");
	public static final By numOfCustomersInput 			= By.xpath("//*[@data-qa='number-of-customers-input']");
	public static final By accountTypeDropdown 			= By.xpath("//*[@data-qa='account-type-list']");
	
	public static final By accountNameErrMsg 			= By.xpath("//*[@data-qa='account-name-error-msg']");
	public static final By DUNSNumberErrMsg 			= By.xpath("//*[@data-qa='duns-number-error-msg']");
	public static final By buyerAdminNameErrMsg 		= By.xpath("//*[@data-qa='buyer-admin-name-error-msg']");
	public static final By buyerAdminEmailErrMsg 		= By.xpath("//*[@data-qa='buyer-admin-email-error-msg']");
	public static final By accountAddressErrMsg 		= By.xpath("//*[@data-qa='account-address-error-msg']");
	public static final By numOfCustomersErrMsg 		= By.xpath("//*[@data-qa='number-of-customers-error-msg']");
	
	public static final By overviewTabSaveButton 		= By.xpath("//*[@data-qa='save-trigger']");
	public static final By overviewTabCancelButton 		= By.xpath("//*[@data-qa='cancel-trigger']");

	public static final By unpaid_orders 				= By.xpath("//*[@data-qa='unpaid-orders-input']");
	public static final By new_customers 				= By.xpath("//*[@data-qa='new-customers-input']");
	public static final By credit_standing 				= By.xpath("//*[@data-qa='credit-standing-input']");
	public static final By view 						= By.xpath("//*[@data-qa='account-edit-trigger']");
	public static final By orders_tab_trigger 			= By.xpath("//*[@data-qa='orders-tab-trigger']");
	
	//Contract Tab
	public static final By overview						= By.xpath("//*[@data-qa='overview-item']");
	public static final By contract						= By.xpath("//*[@data-qa='contract-item']");
	public static final By contractInfoCont 			= By.xpath("//*[@data-qa='contract-info-item']");
	public static final By catalog 						= By.xpath("//*[@data-qa='catalog-term']");
	public static final By catalogDropDown 				= By.xpath("//*[@data-qa='catalog-container']//input");
	public static final By price 						= By.xpath("//*[@data-qa='price-term']");
	public static final By priceDropDown 				= By.xpath("//*[@data-qa='price-container']//input");
	public static final By paymentTerm 					= By.xpath("//*[@data-qa='payment-type-term']");
	public static final By paymentTermDropDown 			= By.xpath("//*[@data-qa='payment-terms-container']//input");
	public static final By countrieShippingTo 			= By.xpath("//*[@data-qa='countries-shipping-to-term']");
	public static final By locale 						= By.xpath("//*[@data-qa='locale-container']");
	public static final By uploadContractText 			= By.xpath("//*[@data-qa='upload-contract-term']");
	public static final By contractDocInput 			= By.xpath("//*[@data-qa='upload-contract-document-input']");
	public static final By expDatePicker 				= By.xpath("//*[@data-qa='expiration-datepicker]");
	public static final By existingContractText 		= By.xpath("//*[@data-qa='existing-contract-term']");
	public static final By tableExpDate 				= By.xpath("//*[@data-qa='expiration-datepicker']");
	public static final By tableContractFile 			= By.xpath("//*[@data-qa='upload-contract-document-input']");
	public static final By coutriesShippingToOptionOne 	= By.xpath("//*[@data-qa='coutries-shipping-to-container']//input");
//	public static final By coutriesShippingToOptionTwo 	= By.xpath("(//*[@data-qa='coutries-shipping-to-list'])[2]");
	public static final By creditLimit 					= By.xpath("//*[@data-qa='credit-limit-term']");
	public static final By creditLimitVal 				= By.xpath("//*[@data-qa='credit-limit-input']");
	public static final By contractFieldHint 			= By.xpath("//*[@data-qa='contract-field-hint']");
	public static final By contractFieldDoc 			= By.xpath("//*[@data-qa='upload-contract-document-input']");
//	public static final By errMsgForContractFile 		= By.xpath("//*[@data-qa='contract-file-err-msg-list']");
	public static final By downloadContractFile 		= By.xpath("//*[@data-qa='download-contract-file-trigger']");
	
	// Create account - SECOM-5461
	public static final By createAccountBtnTrigger 		= By.xpath("//*[@data-qa='create-account-trigger']");
	public static final By createAccountNameContainer 	= By.xpath("//*[@data-qa='accountname-container']");
	public static final By createAccountNameInput 		= By.xpath("//*[@data-qa='accountname-container']//input");
	public static final By createAccountBuyerEmailContainer 		= By.xpath("//*[@data-qa='buyername-container']");
	public static final By createAccountBuyerEmailInput 			= By.xpath("//*[@data-qa='buyername-container']//input");
	public static final By createAccountFirstNameContainer 			= By.xpath("//*[@data-qa='firstname-container']");
	public static final By createAccountFirstNameInput 				= By.xpath("//*[@data-qa='firstname-container']//input");
	public static final By createAccountLastNameContainer 			= By.xpath("//*[@data-qa='lastname-container']");
	public static final By createAccountLastNameInput 				= By.xpath("//*[@data-qa='lastname-container']//input");
	public static final By createAccountTypeContainer 				= By.xpath("//*[@data-qa='accounttye-container']");
	public static final By createAccountTypeInput 					= By.xpath("//*[@data-qa='accounttye-container']//input");
	public static final By createAccountSizeContainer 				= By.xpath("//*[@data-qa='accountsize-container']");
	public static final By createAccountSizeInput 					= By.xpath("//*[@data-qa='accountsize-container']//input");
	public static final By createAccountAddressContainer 			= By.xpath("//*[@data-qa='address-container']");
	public static final By createAccountAddressInput 				= By.xpath("//*[@data-qa='address-container']//input");
	public static final By createAccountCityContainer 				= By.xpath("//*[@data-qa='city-container']");
	public static final By createAccountCityInput 					= By.xpath("//*[@data-qa='city-container']//input");
	public static final By createAccountStateContainer 				= By.xpath("//*[@data-qa='state-container']");
	public static final By createAccountStateInput 					= By.xpath("//*[@data-qa='state-container']//input");
	public static final By createAccountCountryContainer 			= By.xpath("//*[@data-qa='country-container']");
	public static final By createAccountCountryInput 				= By.xpath("//*[@data-qa='country-container']//input");
	public static final By createAccountDUNSNumContainer 			= By.xpath("//*[@data-qa='duns-container']");
	public static final By createAccountDUNSNumInput 				= By.xpath("//*[@data-qa='duns-container']//input");
	public static final By createAccountTaxIDContainer 				= By.xpath("//*[@data-qa='taxid-container']");
	public static final By createAccountTaxIDInput 					= By.xpath("//*[@data-qa='taxid-container']//input");
	public static final By createAccountzipcodeContainer 			= By.xpath("//*[@data-qa='zipcode-container']");
	public static final By createAccountzipcodeInput 				= By.xpath("//*[@data-qa='zipcode-container']//input");
	public static final By submitBtnTrigger 						= By.xpath("//*[@data-qa='submit-trigger']");
	public static final By cancelBtnTrigger 						= By.xpath("//*[@data-qa='cancel-trigger']");
//	public static final By createAccountSuccessMsg 					= By.xpath("//*[@data-qa='create-account-success-msg']");
	public static final By accountReviewBtn 						= By.xpath("//*[@data-qa='account-review-trigger']");
	public static final By catalogDropDownCont 						= By.xpath("//*[@data-qa='catalog-container']");
	public static final By accountsTitle 							= By.xpath("//*[@data-qa='all-accounts-page']");
	public static final By accRepCont 								= By.xpath("//*[@data-qa='account-rep-container']");
	public static final By accounttypecontainer						= By.xpath("//*[@data-qa='accounttye-container']//input");
	public static final By accounttypedropdownlist 					= By.xpath("(//*[@data-qa='accounttye-container']//li)");
	public static final By countrydropdownlist 						= By.xpath("(//*[@data-qa='country-container']//li)");
	public static final By accountcountrycontainer 					= By.xpath("//*[@data-qa='country-container']");
	public static final By accountdetailpagename 					= By.xpath("//*[@data-qa='account-details-page']");
	public static final By overviewmanagementpane 					= By.xpath("//*[@data-qa='management-item']");
	public static final By overviewaccountrepcontainer 				= By.xpath("//*[@data-qa='account-rep-container']");
	public static final By accountrepselect 						= By.xpath("(//*[@data-qa='account-rep-container']/div/ul/li)");
	public static final By overviewstatuscontainer 					= By.xpath("//*[@data-qa='status-container']//input");
	public static final By overviewstatusActive 					= By.xpath("(//*[@data-qa='status-container']/div/ul/li)[2]");
	public static final By saveBtnTrigger 							= By.xpath("//*[@data-qa='save-trigger']");
//	public static final By accountlist_size 						= By.xpath("//*[text()='pending']/following::a");
	public static final By accountlist_size 						= By.xpath("(//*[@data-qa='account-review-trigger' or @data-qa='account-edit-trigger'])");
	public static final By active		 							= By.xpath("//*[@data-qa='account-'][contains(text(),'active')]");
	public static final By pending		 							= By.xpath("//*[@data-qa='account-'][contains(text(),'pending')]");
	public static final By edit			 							= By.xpath("//*[@data-qa='account-edit-trigger']");
	public static final By pendinglnk	 							= By.xpath("//*[@data-qa='account-review-trigger']");
	public static final By staterow		 							= By.xpath("//*[@data-qa='orders-table']");
	
	
	public static final By customerTitle = By.xpath("//*[@data-qa='cusomers-title']");
	public static final By addcustomerTrigger = By.xpath("//*[@data-qa='addcustomer-trigger']");
	public static final By firstnameInput = By.xpath("//*[@data-qa='firstname-input']");
	public static final By lastnameInput = By.xpath("//*[@data-qa='lastname-input']");
	public static final By emailInput = By.xpath("//*[@data-qa='email-input']");
	public static final By sendInviteTrigger = By.xpath("//*[@data-qa='send-invite-trigger']");
	public static final By addCustomerTitle = By.xpath("//*[@data-qa='add-customer-title']");
	public static final By closeTrigger = By.xpath("//*[@data-qa='close-trigger']");
	public static final By lastNameValue = By.xpath("//*[@data-qa='last-name-value']");
	public static final By firstNameValue = By.xpath("//*[@data-qa='first-name-value']");
	public static final By emailAddressValue = By.xpath("//*[@data-qa='emailaddress-value']");
	public static final By phoneValue = By.xpath("//*[@data-qa='phone-value']");
	public static final By customerIdValue = By.xpath("//*[@data-qa='cusomersid-value']");
	public static final By latestOrderValue = By.xpath("//*[@data-qa='latest-order-value']");
	public static final By statusValue = By.xpath("//*[@data-qa='status-value']"); 
	public static final By searchInput = By.xpath("//*[@data-qa='search-input']"); 
	public static final By searchTrigger = By.xpath("//*[@data-qa='search-trigger']");
	public static final By firstNameCloseTrigger = By.xpath("//*[@data-qa='first-name-close-trigger']");
	public static final By lastNameCloseTrigger = By.xpath("//*[@data-qa='last-name-close-trigger']");
	public static final By emailCloseTrigger = By.xpath("//*[@data-qa='email-close-trigger']");
	
	public static final By nameFilter = By.xpath("//*[@data-qa='Name-list']");
	public static final By nameSearch = By.xpath("//*[@data-qa='name-search']");
	public static final By statusFilter = By.xpath("//*[@data-qa='status-list']");
	public static final By statusSearch = By.xpath("//*[@data-qa='status-search']");
	public static final By addMoreFilter = By.xpath("//*[@data-qa='addmorefilter-trigger']");
	public static final By addressFilter = By.xpath("//*[@data-qa='address-list']");
	public static final By addressSearch = By.xpath("//*[@data-qa='address-search']");
	public static final By cityFilter = By.xpath("//*[@data-qa='city-list']");
	public static final By citySearch = By.xpath("//*[@data-qa='city-search']");
	public static final By dateFilter = By.xpath("//*[@data-qa='date-trigger']");
	public static final By datePickerFrom = By.xpath("//*[@data-qa='datepicker-from']");
	public static final By datePickerTo = By.xpath("//*[@data-qa='datepicker-to']");
			
	// Contract tab
	public static final By contractbtntrigger = By.xpath("//*[@data-qa='contract-item']");
	public static final By contractinfotitle = By.xpath("//*[@data-qa='contract-info-item']");
	public static final By catalogDropdown = By.xpath("//*[@data-qa='catalog-container']");
	public static final By catalogDropdownlist = By.xpath("//*[@data-qa='catalog-container']//ul/li[2]");
	public static final By pricelistDropdown = By.xpath("//*[@value='Select Price']");
	public static final By pricelistDropdownlist = By.xpath("(//*[@data-qa='price-container']/div/ul/li)");
	public static final By paymentMethoddropdown = By.xpath("//*[@value='Select Payment Method']");
	public static final By paymentMethoddropdownlist = By.xpath("(//*[@data-qa='payment-type-container']/div/ul/li)");
	public static final By paymentTermsdropdown = By.xpath("//*[@data-qa='payment-terms-container']");
	public static final By paymentTermsdropdownlist = By.xpath("//*[@data-qa='payment-terms-container']//ul/li[2]");
	
	public static final By uploadcontracttitle = By.xpath("//*[@data-qa='upload-contract-term']");
	public static final By uploadcontractinput = By.xpath("//*[@class='cls_skContUploadContract file-path primary-text']");
	public static final By contractexpirationdateselector = By.xpath("//*[@data-qa='expiration-datepicker']");
	public static final By contractcalander = By.xpath("//*[@data-qa='expiration-datepicker']");
	public static final By datepicker = By.xpath("//*[@class='calendar-table']//*[contains(@class,'active')]//following::td[1]");

	public static final By txtbx_UserName = By.xpath("//*[@data-qa='user-name-input']");//*[@data-qa='user-name-input']
	public static final By txtbx_Password = By.xpath("//*[@data-qa='password-input']");
	public static final By btn_LogIn = By.xpath("//*[@data-qa='signin-trigger']"); 
	public static final By keep_loggedin = By.xpath("//*[@data-qa = 'keepme-loggedin-checkbox']");
	public static final By forgot_password = By.xpath("//*[@data-qa = 'forgot-password-trigger']");
	public static final By support_list_header = By.xpath("//*[@data-qa ='support-list']");
	public static final By txtbx_Email = By.xpath("//*[@data-qa ='email-input']");
	public static final By emailLabel = By.xpath("//*[@data-qa='user-name-item']");
	public static final By passwordLabel = By.xpath("//*[@data-qa='password-item']");
	public static final By forgotPasswordScreen = By.xpath("//*[@data-qa='forgot-password-txt']");
	
	public static final By new_password = By.xpath("//*[@data-qa ='new-password-input']");
	public static final By confirm_new_password = By.xpath("//*[@data-qa ='confirm-new-password-input']");
	public static final By save = By.xpath("//*[@data-qa ='save-trigger']");
	public static final By new_password_error = By.xpath("//*[@data-qa ='new-password-error']");
	public static final By confirm_password_error = By.xpath("//*[@data-qa ='confirm-password-error']");
	public static final By tele_phone_input = By.xpath("//*[@data-qa ='tele-phone-input']");

	public static final By first_name = By.xpath("//*[@data-qa ='first-name-input']");
	public static final By last_name = By.xpath("//*[@data-qa ='last-name-input']");
	public static final By create_password = By.xpath("//*[@data-qa ='create-password']");
	public static final By confirm_password = By.xpath("//*[@data-qa ='confirm-password']");
	public static final By register = By.xpath("//*[@data-qa ='register-trigger']");
	
	public static final By signInUsernameTextbox = By.xpath("//*[@data-qa='user-name-input']");
    public static final By signInPasswordTextbox = By.xpath("//*[@data-qa='password-input']");
    public static final By signInSubmitBtn = By.xpath("//*[@data-qa='signin-trigger']");
	
    public static final By businesssearchtext = By.xpath("//*[@data-qa='business-id-filter']");
    public static final By businesssearchtextbox = By.xpath("//*[@data-qa='business-id-input']");
    public static final By businesssearchbtn = By.xpath("//*[@data-qa='search-trigger']");
    public static final By businessitemname = By.xpath("//*[@data-qa='business-name-item']");
    
    public static final By storetab = By.xpath("//*[@data-qa='stores-menu']");		
    public static final By storelabel = By.xpath("//*[@data-qa='stores-list-page-title']");
    public static final By storetrigger = By.xpath("//*[@data-qa='view-store-trigger']");
    public static final By storeops = By.xpath("//*[@data-qa='store-operations-trigger']");
    public static final By storeid = By.xpath("//*[@data-qa='store-id-item']");
    public static final By moduleassociation = By.xpath("(//*[contains(@data-qa,'association-tile')])//span[contains(text(), 'Accounts')]");

    //Added on Jan 8th
    public static final By pageload					 = By.xpath("//*[@data-qa='all-bussinesses-term']");
    public static final By storeidfilter			 = By.xpath("//*[@data-qa='store-id-filter']//i");
    public static final By storeidfilter_txtbox		 = By.xpath("//*[@data-qa='store-id-input']");
    public static final By storeidsearch			 = By.xpath("//button[@data-qa='search-trigger']");
    public static final By pricelist_selection		 = By.xpath("//*[@data-qa='price-container']//ul/li[2]");
    public static final By payment_method			 = By.xpath("//*[@data-qa='payment-type-container']//ul/li[2]");
    public static final By datepicker_apply			 = By.xpath("//*[@class='applyBtn btn btn-sm btn-primary']");
    public static final By btn_search			 	 = By.xpath("//button[@data-qa='type-search']");
    public static final By acc_type				 	 = By.xpath("//*[@label='Account Type']");
    public static final By defense_txt			 	 = By.xpath("(//*[@class='search form-control'])[1]");
    public static final By acc_btn				 	 = By.xpath("//*[@data-qa='status-search']/span");
    public static final By active_btn			 	 = By.xpath("(//*[@type='text'])[4]");
    
  
}
