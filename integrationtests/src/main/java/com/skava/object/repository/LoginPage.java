package com.skava.object.repository;

import org.openqa.selenium.By;

public class LoginPage 
{	
	public static final By txtEmailId = By.xpath("//*[@data-qa='user-name-input']");
	public static final By txtPassword = By.xpath("//*[@data-qa='password-input']");
	public static final By btnLogIn = By.xpath("//*[@data-qa='signin-trigger']");
	public static final By lblSignInSuccess = By.xpath("//*[@data-qa='all-bussinesses-term']");
	
}