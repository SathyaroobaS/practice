package com.skava.object.repository;
import org.openqa.selenium.By;
public class orderDetailPage
{
	public static final By orderId = By.xpath("//*[@data-qa='order-title']");
	public static final By orderDate = By.xpath("//*[@data-qa='order-date-item']");
	public static final By orderValue = By.xpath("//*[@data-qa='order-value-item']");
	public static final By customerInfo = By.xpath("//*[@data-qa='customer-info-item']");
	public static final By customerName = By.xpath("//*[@data-qa='customer-name-item']");
	public static final By custInfoEmail = By.xpath("//*[@data-qa='email-item']");
	public static final By custInfoPhone = By.xpath("//*[@data-qa='phone-item']");
	public static final By paymentInfo = By.xpath("//*[@data-qa='payment-item-tab']");
	public static final By orderSubTot = By.xpath("//*[@data-qa='order-subtotal-item']");
	public static final By tax = By.xpath("//*[@data-qa='tax-item']");
	public static final By shipping = By.xpath("//*[@data-qa='shipping-item']");
	public static final By discount = By.xpath("//*[@data-qa='discount-item']");
	public static final By orderGrandTot = By.xpath("//*[@data-qa='order-grand-total']");
	public static final By payeeName = By.xpath("//*[@data-qa='customer-name-item']");
	public static final By cardDet = By.xpath("//*[@data-qa='payment-item']");
	public static final By expDate = By.xpath("//*[@data-qa='payment-item']");
	public static final By itemName = By.xpath("//*[@data-qa='item-name-item']");
	public static final By itemImg = By.xpath("//*[@data-qa='item-image']");
	public static final By itemDesc = By.xpath("//*[@data-qa='item-description']");
	public static final By itemQty = By.xpath("//*[@data-qa='quantity-item']");
	public static final By itemSummary = By.xpath("//*[@data-qa='order-summary-item']");
	public static final By itemSubTot = By.xpath("//*[@data-qa='item-subtotal-item']");
	public static final By itemTax = By.xpath("//*[@data-qa='tax-item']");
	public static final By itemGrandTot = By.xpath("//*[@data-qa='item-grand-total-item']");
	public static final By orderSummaryBreadCrum = By.xpath("//*[@data-qa='order-summary-breadcrumb']");
    public static final By orderIDText = By.xpath("//*[@data-qa='order-title']");
    public static final By orderDateTime = By.xpath("//*[@data-createdtime]");
    public static final By viewPaymentInform = By.xpath("//*[@data-qa='view-payment-details-trigger']");
    public static final By viewPaymenttab = By.xpath("//*[@data-qa='payment-item-tab']");
    public static final By paymentPopup = By.xpath("//*[@data-qa='payment-details-title']");
    public static final By paymentdatatable = By.xpath("//*[@id='id_skPaymentTable']");
    public static final By paymentPopupcard = By.xpath("//*[@data-qa='payment-mode-value']");
    public static final By paymentPopupbilladd = By.xpath("//*[@data-qa='billing-address-value']");
    public static final By paymentPopupamt = By.xpath("//*[@data-qa='amount-value']");
    public static final By paymentPopupdesc = By.xpath("//*[@data-qa='description-value']");
    public static final By breadCrumb = By.xpath("(//*[@data-qa='breadcrumb_null'])[1]");
    public static final By orderBreadCrumb = By.xpath("(//*[@data-qa='breadcrumb_null'])[2]");
}

