package com.skava.object.repository;

import org.openqa.selenium.By;

public class CatalogPageObject
{
	public static final By Catalog_Title 						= By.xpath("//*[@data-qa='catalog-title']");
	public static final By Attributes_Tab 						= By.xpath("//*[@data-qa='Attributes-tab']");
	public static final By Attributes_Title 					= By.xpath("//*[@data-qa='attributes-info-title']");
	public static final By grp_tab1			 					= By.xpath("//*[@data-qa='group-tab-trigger']");

	public static final By Create_Attribute_Btn 				= By.xpath("//*[@data-qa='createattribute-trigger']");
	public static final By Attribute_Name_Input 				= By.xpath("//*[@data-qa='add-attributename-input']");
	public static final By Attribute_Id_Input 				    = By.xpath("//*[@data-qa='add-attributeid-input']");
	public static final By Field_Type_Input    				    = By.xpath("//*[@data-qa='add-attributefieldtype-list']//*[contains(@class,'select-dropdown')]");
	public static final By Field_Type_Dropdown    			    = By.xpath("//*[@data-qa='add-attributefieldtype-list']//*[contains(@class,'dropdown-content')]");
	public static final By Field_Type_Dropdown_List    		    = By.xpath("//*[@data-qa='add-attributefieldtype-list']//*[contains(@class,'filtrable')]");
	public static final By Field_Type_Number_List    		    = By.xpath("//*[@data-qa='add-attributefieldtype-list']//*[contains(@class,'filtrable')][contains(text(),'Number')]");
	public static final By Field_Type_String_List    		    = By.xpath("(//*[@data-qa='add-attributefieldtype-list']//*[contains(@class,'filtrable')][contains(text(),'String')])[2]");
	public static final By Min_Number_Input 				    = By.xpath("//*[@data-qa='validation-number-min-input']");
	public static final By Max_Number_Input 				    = By.xpath("//*[@data-qa='validation-number-max-input']");
	public static final By Create_Button 				        = By.xpath("//*[@data-qa='save-trigger']");
	public static final By Enter_Mastercatalog			        = By.xpath("(//*[@data-qa='view-trigger'])[1]");
	
	//Priya
	public static final By Projects_Dropdown			        = By.xpath("//*[@data-qa='projects-menu-trigger']");
	public static final By Project_Overlay				        = By.xpath("//*[@class='modal-content']");
	public static final By Existing_project_Row			        = By.xpath("//*[@data-qa='existing-project-item']");
	public static final By Project_Enter					    = By.xpath("(//*[@data-qa='enter-trigger'])[1]");
	public static final By Pancake							    = By.xpath("(//*[@class='d-inline-block align-middle']/a)");
	public static final By ProductsTab						    = By.xpath("//*[@data-qa='Products-tab']");
	public static final By AddItemOverlay					    = By.xpath("(//*[@data-qa='add-Item-popup']/div/div)[1]");
	public static final By Itemddropdown_Select				    = By.xpath("//*[@data-qa='item-type-list']");
	//public static final By Bundle_Options					    = By.xpath("(//*[@data-qa='item-type-list']//*[contains(text(),'Bundle')])[1]");
	public static final By Bundle_Options					    = By.xpath("//*[@data-qa='item-type-list']//ul//li[@value='bundle']");
	
	//public static final By Bundle_Options					    = By.xpath("//*[@class='dropdown-content select-dropdown']//*[contains(text(),'Bundle')]");
	public static final By BundleId_txtbox					    = By.xpath("//*[@data-qa='item-id-input']");
	public static final By BundleName_txtbox				    = By.xpath("//*[@data-qa='item-name-input']");
	public static final By Add_details_Btn					    = By.xpath("//*[@data-qa='item-details-trigger']");
	public static final By CreateItem_Btn					    = By.xpath("//*[contains(text(),'Create Item')]");
	public static final By Breadcrumb_Bundlename			    = By.xpath("(//*[@data-qa='productskuname-title']/span[2])");
	public static final By Edit_Bundlenametxtbox			    = By.xpath("(//*[@data-qa='productskuname-input'])");
	public static final By Edit_Bundle_Save					    = By.xpath("//*[@data-qa='productskusave-trigger']");
	public static final By Bundles_Tab						    = By.xpath("//*[@data-qa='Bundles-tab']");
	public static final By Add_Product_btn					    = By.xpath("//*[@data-qa='add-product-trigger']");
	public static final By Item_List_Container					= By.xpath("//*[@data-qa='itemcontainer-list']");
	public static final By Add_product_btn						= By.xpath("(//*[@data-qa='additem-trigger'])[1]");
	public static final By Add_product_btn2						= By.xpath("(//*[@data-qa='additem-trigger'])[2]");
	public static final By Close_overlay						= By.xpath("//*[@data-qa='close-trigger']");
	public static final By Update_Quantity_txtbox				= By.xpath("(//*[@id='id_bundleProductsDataTable']/tbody/tr/td[5]/input)[1]");
	public static final By Delete_Icon							= By.xpath("(//*[@id='id_bundleProductsDataTable']/tbody/tr/td[7]/button)[1]");
	public static final By add_row								= By.xpath("(//*[@id='id_bundleProductsDataTable']/tbody/tr)[1]");
	
	
	// Nov 26th -- Priya
	public static final By Update_productname					= By.xpath("//*[@data-qa='productskuname-input']");
	public static final By getproductid							= By.xpath("//*[@class='align-middle p-1 cls_productId']");
	public static final By DeleteIcon							= By.xpath("(//*[@class='align-middle p-1']/button)[1]");
	//public static final By CollectionOption						= By.xpath("(//*[@data-qa='item-type-list']//*[contains(text(),'Collection')])[1]");
	public static final By CollectionOption						= By.xpath("//*[@data-qa='item-type-list']//ul//li[@value='collection']");
	public static final By Collection_Tab					    = By.xpath("//*[@data-qa='Collections-tab']");
	//public static final By Random_Product_Select			    = By.xpath("(//*[@data-qa='products-container']/td[6]/a)");
	public static final By Random_Product_Select			    = By.xpath("(//*[@data-qa='view-trigger'])");
	public static final By Prod_Container					    = By.xpath("(//*[@id='id_productsDataTable'])");
	
	
	// Date picker
	public static final By StartDate						    = By.xpath("//*[@data-qa='productskustartdate-input']");
	public static final By StartDate_Selection				    = By.xpath("//*[@class='picker__box']//*[@role='presentation']");
	public static final By Select_StartDate						= By.xpath("//*[contains(@class,'selected picker__day')]//following::td[1]");
	public static final By EndDate						    	= By.xpath("//*[@data-qa='productskuenddate-input']");
	public static final By EndDate_Selection				    = By.xpath("(//div[contains(@class,'picker__day picker__day--infocus')])");
	public static final By SaveUpdatedProduct			    	= By.xpath("//*[@data-qa='productskusave-trigger']");
	public static final By Relations_Tab				    	= By.xpath("//*[@data-qa='Relations-tab']");
	public static final By Upsell_Addproduct_btn		    	= By.xpath("//*[@data-qa='add-upsellproduct-trigger']");
	//public static final By prd_add_upsell				    	= By.xpath("(//*[@data-qa='additem-trigger'])");
	public static final By prd_add_upsell				    	= By.xpath("(//*[@data-qa='additem-trigger'])");
	public static final By container_list				    	= By.xpath("(//*[@id='id_itemLists'])");
	public static final By deleteicon_count				    	= By.xpath("//*[@data-qa='delete-product-trigger']");
	
	public static final By Attribute_Created_Success_Msg        = By.xpath("//*[@data-qa='save-success-txt']");
	//public static final By Recently_Created_Attribute           = By.xpath("(//*[@data-qa='attribute-name-value'])[1]");
	public static final By Recently_Created_Attribute           = By.xpath("(//*[@data-qa='add-attributename-input'])[1]");
	
	public static final By Group_Attribute_Tab				    = By.xpath("//*[@data-qa='group-tab-trigger']");
	public static final By Create_Group_Btn 				    = By.xpath("//*[@id='id_createattributegroup']");
	public static final By Attribute_Group_Creation_Title	    = By.xpath("//*[@data-qa='attributegroup-manage-title']");
	public static final By Create_Variant_Group_Btn 			= By.xpath("//*[@id='id_createvariantattributegroup']");
	public static final By Attribute_Group_Name	                = By.xpath("//*[@data-qa='attributegroup-name-input']");
	public static final By Attribute_Group_Order_Id   	        = By.xpath("//*[@data-qa='attributegroup-orderid-input']");
	public static final By Variant_Group_Tab				    = By.xpath("//*[@data-qa='variant-group-tab-trigger']");
	public static final By Recently_Created_Group_Attr          = By.xpath("(//*[@data-qa='group-id-value'])[1]");
	
	
	public static final By Import_Tab        					= By.xpath("//*[@data-qa='Import-tab']");
	public static final By Browse_Button       					= By.xpath("//*[@data-qa='browse-button-trigger']");
	public static final By upload_File_Input       				= By.xpath("//*[@data-qa='draganddrop-label']//following::input[1]");
	public static final By Import_Button       				    = By.xpath("//*[@data-qa='startimport-button-trigger']");
	
	public static final By Locale_Dropdown       			    = By.xpath("//*[@data-qa='locale-dropdown']");
	public static final By store_Dropdown       			    = By.xpath("//*[@id='headerStore_dropdown']");
	public static final By store_Dropdown_Values       			= By.xpath("//*[@id='headerStore_dropdown']//following::div[@class='dropdown-item']/a");
	public static final By Master_Catalog_Edit_Icon			    = By.xpath("//*[@data-qa='view-trigger']");
	public static final By Master_Catalog_Page			    	= By.xpath("//*[@data-qa='master-catalog-caption-txt']");
	public static final By Master_Overview_Tab			    	= By.xpath("//*[@data-qa='Master-Overview-tab']");
	public static final By Catalog_Name_Input			    	= By.xpath("//*[@data-qa='add-catalogname-input']");
	public static final By Master_Catalog_Name                  = By.xpath("(//*[@data-qa='catalogid-value'])[1]");
	
	public static final By Projects_Menu     			    	= By.xpath("//*[@data-qa='projects-menu-trigger']");
	public static final By Create_New_Project_Btn     			= By.xpath("//*[@data-qa='create-project-trigger']");
	public static final By New_Project_Name         			= By.xpath("//*[@data-qa='my-new-project-input']");
	public static final By Project_Create_Button 			    = By.xpath("//*[@data-qa='create-trigger']");
	//public static final By Newly_Created_Project_Name     		= By.xpath("(//*[@data-qa='project-name-value'])[last()]");
	public static final By Newly_Created_Project_Name     		= By.xpath("(//*[@data-qa='project-name-value'])[1]");
	
	
	// Priya on Nov 27th
	public static final By Noitemfound			 			    = By.xpath("//*[@data-qa='noitemfound-text']");
	public static final By crosssell_Addproduct_btn		    	= By.xpath("//*[@data-qa='add-crosssellproduct-trigger']");
	
	///Arunraj
	
	public static final By MasterCatalogContainer				= By.xpath("//*[@data-qa='mastercatalog-container']");
	public static final By MasterCatalogName					= By.xpath("//*[@data-qa='mastercatalog-container']//*[@data-qa='catalogid-value']");
	public static final By MasterCatalogID						= By.xpath("//*[@data-qa='mastercatalog-container']//*[@data-qa='status-value']");
	public static final By MasterCatalogStatus					= By.xpath("//*[@data-qa='mastercatalog-container']//*[@data-qa='status-active']");
	public static final By MasterCatalogEditbutton				= By.xpath("//*[@data-qa='mastercatalog-container']//*[@data-qa='edit-trigger']");
	public static final By MasterCatalogEnter					= By.xpath("//*[@data-qa='mastercatalog-container']//*[@data-qa='view-trigger']");
	public static final By CatalogName							= By.xpath("(//*[@data-qa='catalogid-value'])");
	public static final By CatalogID							= By.xpath("(//*[@data-qa='status-value'])");
	public static final By CatalogEdit							= By.xpath("(//*[@data-qa='edit-trigger'])");
	public static final By CatalogView							= By.xpath("(//*[@data-qa='view-trigger'])");
	public static final By CatalogTitle							= By.xpath("(//*[@data-qa='catalogs-info-title'])");

	public static final By MasterProjectDropDown				= By.xpath("//*[@data-qa='projects-menu-trigger']/span");
	public static final By MasterProjectOverviewMenu			= By.xpath("//*[@data-qa='Master-Overview-tab']");
	public static final By MasterProjectProductsMenu			= By.xpath("//*[@data-qa='Products-tab']");
	public static final By MasterProjectSkuMenu					= By.xpath("//*[@data-qa='Skus-tab']"); 
	public static final By MasterProductsPageProductsContainer	= By.xpath("(//*[@data-qa='products-container'])");
	public static final By MasterSkuPageProductsContainer		= By.xpath("//*[@data-qa='skus-container']");
	public static final By MasterProductSkuFilterApproved		= By.xpath("//*[@data-qa='project-status-list']//*[@title='All Approved']");
	public static final By MasterSkuPageTitle				    = By.xpath("//*[@data-qa='skus-info-title']");	
	public static final By MasterproductPageTitle				= By.xpath("//*[@data-qa='master-products-title']");
	public static final By CreateCatalogButton					= By.xpath("//*[@data-qa='createcatalog-trigger']");
	public static final By CreateCatalogPageTitle				= By.xpath("//*[@data-qa='catalogs-info-title']");
	public static final By CreateCatalogName					= By.xpath("//*[@data-qa='add-catalogname-input']");
	public static final By CreateCatalogDescription				= By.xpath("//*[@data-qa='add-catalogdescription-input']");
	public static final By CreateCatalogID						= By.xpath("//*[@data-qa='add-catalogid-input']");
	public static final By CreateCatalogEndDate					= By.xpath("//*[@data-qa='add-catalogenddate-input']");
	public static final By CreateCatalogStartDate				= By.xpath("//*[@data-qa='add-catalogstartdate-input']");
	public static final By CreateCatalogCreateButton			= By.xpath("//*[@data-qa='save-trigger']");
	public static final By CreateCatalogCancelButton			= By.xpath("//*[@data-qa='cancel-trigger']");
	public static final By CreateCatalogSuccess					= By.xpath("//*[@data-qa='save-success-txt']");
	public static final By FeedRunInvisible						= By.xpath("//*[@class='importingImportContainer row justify-content-center mb-4']");
	public static final By FeedRuntextInvisible					= By.xpath("//*[@data-la-initdispnone='true' and @class='card-text text-center pt-2 uploaded-file-name']");
	
	
	
	
	public static final By CatlogIDFilter 						= By.xpath("//*[@data-qa='catalog-id-list']");
	public static final By CatlogIDFilterIputField				= By.xpath("//*[@data-qa='catalog-id-input']");
	public static final By CatlogFilterSearch 					= By.xpath("//*[@data-qa='search-trigger']");

	public static final By SalesCatalogprojectFeedMenu          = By.xpath("//*[@data-qa='Feed-Rules-tab']");
	public static final By SalesCatalogprojectFeedRunFeed       = By.xpath("//*[@data-qa='save-and-run-trigger']");
	public static final By SalesCatalogprojectFeedPageTitle     = By.xpath("//*[@data-qa='feed-rules-title']");

	//Nathiya
	public static final By Project_Enter_Icon			        = By.xpath("(//*[@data-qa='enter-trigger'])[last()]");
	public static final By Project_Edit_Icon			        = By.xpath("(//*[@data-qa='view-trigger'])[last()]");
	public static final By Project_Page			    	        = By.xpath("//*[contains(@class,'cls_projectName')]");
	
	public static final By File_Imported_Success_Msg 			= By.xpath("//*[contains(@class,'importCompletedContainer')]");
	public static final By Products_Tab           			    = By.xpath("//*[@data-qa='Products-tab']");
	public static final By Products_Container           	    = By.xpath("//*[@data-qa='products-container']");
	public static final By Skus_Tab           			        = By.xpath("//*[@data-qa='Skus-tab']");
	public static final By Skus_Container           	        = By.xpath("//*[@data-qa='skus-container']");
	public static final By ProjectDetailsDOM                    = By.xpath("//*[@id='id_projectDetails']");
    public static final By Submit_Project_Btn                   = By.xpath("//*[@data-qa='projectdetails-submit-trigger']");
    public static final By Submitted_Success_Msg                = By.xpath("//*[@class='toast-message']");
    public static final By Approve_Project_Btn                  = By.xpath("//*[@data-qa='projectdetails-approved-trigger']");
    public static final By Project_Reopen_Icon                  = By.xpath("//*[@class='cls_skApprovedUI']//*[contains(@class,'material-icons')]");
    public static final By Project_Reopen_Btn                   = By.xpath("//*[contains(@class,'btn-projectdetailsreopen')]");
    public static final By OverviewTab           			    = By.xpath("//*[@data-qa='Overview-tab']");
    public static final By OverviewPage          			    = By.xpath("//*[@data-qa='catalogs-info-title']");
    
    public static final By Min_String_Input 				    = By.xpath("//*[@data-qa='validation-string-min-input']");
	public static final By Max_String_Input 				    = By.xpath("//*[@data-qa='validation-string-max-input']");
	public static final By File_Import_In_Progress   			= By.xpath("//*[contains(@class,'import-inprogress')]");
	
	// On Dec 4th 2018
	public static final By Pancake_stores	 				    = By.xpath("//*[@data-qa='sideNav_Stores']");
	public static final By Pancake_storeoverview			    = By.xpath("//*[@data-qa='sideNav_Store Overview']");
	//public static final By Business_logo					    = By.xpath("//*[@class='navbar-brand']");
	public static final By Business_logo					    = By.xpath("//*[@data-qa='logo']");
	public static final By Enter_OpenProj				        = By.xpath("(//*[@data-qa='view-trigger'])[1]");
	public static final By Proj_Id_UI					        = By.xpath("(//*[@data-qa='projectId-txt'])");
	public static final By Newproj_lastitem				        = By.xpath("(//*[@class='modal-content']//*[@data-qa='view-trigger'])[last()]");
	public static final By PDP_nodatafound					    = By.xpath("//*[@data-qa='no-data-error-alert']");
	public static final By Status_dropdown					    = By.xpath("//*[@data-qa='project-status-list']");
	public static final By Approved_Chkbox					    = By.xpath("((//*[@class='filtrable'])[3]/label)");
	public static final By Approved_Chkbox_Selected			    = By.xpath("//*[@class='active selected']");
	public static final By productlist_count				    = By.xpath("(//*[@data-qa='view-trigger'])");
	//public static final By sku_active						    = By.xpath("(//*[@id='sku']//*[@data-qa='view-trigger'])");	
	public static final By sku_active						    = By.xpath("(//*[@data-qa='skus-container']//*[@data-qa='view-trigger'])");	
	
	public static final By product_active						= By.xpath("(//*[@id='id_productsDataTable']//*[@data-qa='view-trigger'])");		
	public static final By product_overview_sku				    = By.xpath("(//*[@class='nav-item'])[2]/a");
	public static final By proj_status_dropdown				    = By.xpath("(//*[@data-qa='project-status-list'])[1]");
	public static final By choose_created_proj				    = By.xpath("(//*[@class='dropdown-content select-dropdown active moreActive']/li)[2]");
	public static final By all_approved_proj				    = By.xpath("//*[@class='dropdown-content select-dropdown moreActive active']//*[contains(text(),'All Approved')]");
	public static final By enter_last_proj					    = By.xpath("(//*[@data-qa='view-trigger'])[last()]");
	public static final By search_overviewpage				    = By.xpath("(//*[@data-qa='search-trigger'])[2]");
	public static final By overview_status_dropdown			    = By.xpath("(//*[@data-qa='project-status-list'])[1]");
	public static final By active_status_click				    = By.xpath("//*[@data-qa='product-status-list']");
	public static final By select_active_option				    = By.xpath("//*[@value='ACTIVE']/span");
	
	// Dec 6th 
	public static final By Edited_item_txtbox				    = By.xpath("//*[@data-qa='productskuname-input']");
	public static final By Added_item_txtbox				    = By.xpath("//*[@data-qa='product-name-item']");
	public static final By Sku_Added_item_txtbox			    = By.xpath("//*[@data-qa='skuname-value']");
	public static final By attr_type_drodwn					    = By.xpath("//*[@data-qa='add-attributetype-list']//*[@class='select-dropdown']");
	public static final By attr_dynamic_option				    = By.xpath("//*[@class='dropdown-content select-dropdown active']//*[contains(text(),'Dynamic')]");
	public static final By Field_Type_ImgWithTxt_List    		= By.xpath("(//*[@data-qa='add-attributefieldtype-list']//*[contains(@class,'filtrable')][contains(text(),'ImageWithText')])[1]");
	public static final By Field_Type_Img_List    				= By.xpath("(//*[@data-qa='add-attributefieldtype-list']//*[contains(@class,'filtrable')][contains(text(),'Image')])[2]");
	public static final By imgwithtext_txtbox				    = By.xpath("//*[@data-qa='validation-dynamicimagewithtext-input']");
	public static final By img_txtbox						    = By.xpath("//*[@data-qa='validation-dynamicimage-input']");
	public static final By active_chkbox					    = By.xpath("//*[@data-qa='add-attributetype-list']//*[@class='select-dropdown valid active']");
	public static final By Assets_container					    = By.xpath("//*[@data-qa='productassets-title']");
	public static final By Assets_altimg					    = By.xpath("//*[@data-qa='product-image-trigger']");
	public static final By Assets_alttext					    = By.xpath("//*[@data-qa='product-id-trigger']");
	public static final By Assets_link					  	  	= By.xpath("//*[@data-qa='product-name-trigger']");
	
	// Dec7th
	public static final By Assets_tab					    	= By.xpath("//*[@data-qa='Assets-tab']");
	public static final By product_tab_SKU				    	= By.xpath("//*[@data-qa='SKUs-tab']");
	public static final By product_tab1_SKU				    	= By.xpath("//*[@class='admin-sidenav']/li//*[@data-qa='Relations-tab']");
	
	public static final By add_sku_hlptxt				    	= By.xpath("//*[@data-qa='noitemfound-text']");
	public static final By add_sku_btn					    	= By.xpath("//*[@data-qa='add-sku-trigger']");
	public static final By sku_item_container			    	= By.xpath("//*[@id='id_itemLists']");
	public static final By addsku_save_btn				    	= By.xpath("//*[@data-qa='save-trigger']");
	//public static final By get_skuid					    	= By.xpath("//*[@class='align-middle p-1 cls_skuId']");
	public static final By get_skuid					    	= By.xpath("(//*[@class='align-middle p-1 text-truncate cls_skuId'])[1]");	
	public static final By proj_dropdwn					    	= By.xpath("(//*[@data-qa='project-status-list'])[1]");
	public static final By approved_option				    	= By.xpath("//*[@class='dropdown-content select-dropdown active moreActive']//*[contains(text(),'All Approved')]");
	public static final By sku_id_drpdwn				    	= By.xpath("//*[@data-qa='sku-id-list']");
	public static final By sku_id_txbox_fr_search		    	= By.xpath("//*[@data-qa='sku-id-input']");
	public static final By skuid_value_after_search		    	= By.xpath("//*[@data-qa='skuid-value']");
	public static final By skuid_search_btn				    	= By.xpath("(//*[@data-qa='search-trigger'])[2]");
	public static final By attr_tab						    	= By.xpath("//*[@data-qa='Attributes-tab']");
	//public static final By attr_add_btn					    	= By.xpath("//*[@data-qa='attribute-list-trigger']");
	public static final By attr_add_btn					    	= By.xpath("//*[@data-qa='add-attributes-trigger']");
	public static final By attr_txtbox					    	= By.xpath("//*[@data-qa='item-attribute-input']");
	public static final By attr_select_option			    	= By.xpath("(//*[@class='list-group-item justify-content-between align-items-center border-left-0 border-right-0 rounded-0']/div)");
	public static final By attr_cancel_btn				    	= By.xpath("//*[@class='float-right']//*[@data-qa='cancel-trigger']");
	public static final By attr_save_btn				    	= By.xpath("//*[@class='float-right']//*[@data-qa='save-trigger']");
	public static final By desc_text					    	= By.xpath("//*[@data-qa='item-attribute-input']");
	public static final By addbtn_sce2					    	= By.xpath("//*[@class='btn btn-outline-secondary btn-sm save-btn float-right m-0 mr-4 mb-2 py-1 waves-effect waves-light']");
	//public static final By del_attr_btn					    = By.xpath("(//*[@class='delete-btn border-0 bg-transparent float-right'])");
	public static final By del_attr_btn					    	= By.xpath("(//*[@class='delete-btn border-0 bg-transparent'])");
	
	public static final By noproperty_txt				    	= By.xpath("//*[@id='id_noPropertiesMsg']");
	public static final By edit_img_asset				    	= By.xpath("(//*[@data-qa='edit-trigger'])[1]");
	public static final By edit_name_asset				    	= By.xpath("(//*[@data-qa='edit-trigger'])[2]");
	public static final By edit_prop_asset				    	= By.xpath("(//*[@data-qa='edit-trigger'])[3]");
	public static final By edit_desc_asset				    	= By.xpath("(//*[@data-qa='edit-trigger'])[4]");
	public static final By edit_details_asset				    = By.xpath("(//*[@data-qa='edit-trigger'])[5]");
	
	// Dec 13
	public static final By SEO_tab							    = By.xpath("//*[@data-qa='SEO-tab']");
	public static final By semanticid_txtbox				    = By.xpath("//*[@data-qa='productsemanticid-input']");
	public static final By semanticid_enterbtn				    = By.xpath("(//*[@data-qa='productskuEnter-trigger'])[1]");
	public static final By added_semantics_DOM				    = By.xpath("//*[@id='id_semantics']");
	public static final By keywrd_txtbox					    = By.xpath("//*[@data-qa='productkeyword-input']");
	public static final By keywrd_enterbtn				   		= By.xpath("(//*[@data-qa='productskuEnter-trigger'])[2]");
	public static final By desc_txtbox						    = By.xpath("//*[@data-qa='productdescription-input']");
	public static final By seo_savebtn					   		= By.xpath("(//*[@data-qa='productskuEnter-trigger'])[3]");
	public static final By keywords_DOM					   		= By.xpath("//*[@id='id_keywords']");
	public static final By pancake_menu					   		= By.xpath("//*[@data-qa='menu-icon']");
	
	//Added on Mar 15th by Priya
	public static final By basic_info_tab				   		= By.xpath("//*[@data-qa='Basic-Information-tab']");
	
	// Added on 19 March
	public static final By productIdList						= By.xpath("//*[@data-qa='product-id-list']");
	public static final By productIdSearchInput 				= By.xpath("//*[@data-qa='product-id-search-input']");
	public static final By searchBtn 							= By.xpath("(//*[@data-qa='search-trigger'])[2]");
	public static final By product_active_1						= By.xpath("(//*[@id='id_productsDataTable']//*[@data-qa='view-trigger'])[1]");	
	public static final By alrt_popup							= By.xpath("//*[@id='confirmationModal']/div/div[1]");	
	public static final By popup_yes							= By.xpath("//*[@name='continue']");	
	public static final By popup_no								= By.xpath("//*[@name='cancel']");	
	
	
	
	
}