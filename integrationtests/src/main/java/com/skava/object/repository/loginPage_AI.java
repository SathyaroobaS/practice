package com.skava.object.repository;

import org.openqa.selenium.By;

public class loginPage_AI {

	public static final By sk_Logo = By.xpath("//*[@dtat-qa='sk-logo']");
	public static final By txtbx_EmailAddress = By.xpath("//*[@data-qa='user-name-input']");
	public static final By txtbx_Password = By.xpath("//*[@data-qa='password-input']");
	public static final By keep_loggedin = By.xpath("//*[@data-qa = 'keepme-loggedin-checkbox']");
	public static final By btn_SIGNIN = By.xpath("//*[@data-qa='signin-trigger']"); 
    public static final By forgot_password = By.xpath("//*[@data-qa = 'forgot-password-trigger']");
    public static final By txt_CopyrightContent = By.xpath("//*[@data-qa='sk-copyright']");
	//public static final By support_list_header = By.xpath("//*[@data-qa ='support-list']");
	public static final By inerror_EmailAddress = By.xpath("//*[@data-qa='email-address-input']");
	public static final By inerror_Password = By.xpath("//*[@data-qa='password-input']");
	public static final By txt_Validation = By.xpath("//*[@data-qa='email-password-mismatch']");
	public static final By headerDropDown = By.xpath("//*[@data-qa='myaccount-list']");
	public static final By myaccountOption = By.xpath("//*[@data-qa='myaccount-trigger']");
	public static final By tele_phone_input = By.xpath("//*[@data-qa ='tele-phone-input']");
	public static final By first_name = By.xpath("//*[@data-qa ='first-name-input']");
	public static final By last_name = By.xpath("//*[@data-qa ='last-name-input']");
	public static final By create_password = By.xpath("//*[@data-qa ='create-password']");
	public static final By confirm_password = By.xpath("//*[@data-qa ='confirm-password']");
	public static final By register = By.xpath("//*[@data-qa ='register-trigger']");
	public static final By emailInput = By.xpath("//*[@data-qa='email-input']");
	public static final By forgotPasswordScreen = By.xpath("//*[@data-qa='forgot-password-txt']");
	public static final By emailLabel = By.xpath("//*[@data-qa='user-name-item']");
	public static final By passwordLabel = By.xpath("//*[@data-qa='password-item']");
	
	//Gokul s
	public static final By logout = By.xpath("(//*[@class='dropdown-item pb-0 waves-effect waves-light'])[2]");
}
