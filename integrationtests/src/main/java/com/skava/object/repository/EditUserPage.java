package com.skava.object.repository;

import org.openqa.selenium.By;

public class EditUserPage {
	
	public static final By firstname=By.xpath("//*[@data-qa='user-firstname']");
	public static final By lastname=By.xpath("//*[@data-qa='user-lastname']");
	public static final By email=By.xpath("//*[@data-qa='user-email']");
	public static final By InstanceAdminToggle=By.xpath("//*[@data-qa='instance-admin-trigger']");
	public static final By adminToggle=By.xpath("//*[@data-qa='admin-trigger']");
	public static final By businessList=By.xpath("//*[@data-qa='business-list-items']");
	public static final By businessCheckbox=By.xpath("//*[@data-qa='business-checkbox']");
	public static final By customRolesTab=By.xpath("//*[@data-qa='custom-roles-tab']");
	public static final By standardRolesTab=By.xpath("//*[@data-qa='standard-roles-tab']");
	public static final By customRoles=By.xpath("//*[@data-qa='custom-roles-item']");
	public static final By standardRoles=By.xpath("//*[@data-qa='standard-roles-item']");
	public static final By rolesSection=By.xpath("//*[@data-qa='roles-container']");
	public static final By updateUserBtn=By.xpath("//*[@data-qa='update-trigger']");
	public static final By statusToggleBtn=By.id("id_skUserActive");
	public static final By editUserSuccessMsg=By.xpath("//*[@data-qa='edit-user-success-msg']");
	public static final By editUserErrorMsg=By.xpath("//*[@data-qa='user-update-error-msg']");
	public static final By resendActivationBtn=By.xpath("//*[@data-qa='resend-activation-trigger']");
	public static final By resendActivationSuccessMsg=By.xpath("//*[@data-qa='user-update-success-msg']");
	public static final By rolesCollectionCheckbox=By.xpath("//*[@data-qa='collection-checkbox']");
	public static final By standardRolesAccordian=By.xpath("//*[@data-qa='collection-checkbox']");
	public static final By collectionAllCheckbox=By.xpath("//*[@data-qa='collection-all-checkbox']");
			
}
