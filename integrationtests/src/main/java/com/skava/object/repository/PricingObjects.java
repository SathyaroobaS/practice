package com.skava.object.repository;
 
import org.openqa.selenium.By;
 
 public class PricingObjects
 {
	 public static final By btn_createPriceList = By.xpath("//*[@data-qa='create-pricelist-btn-trigger']");
	 public static final By btn_savePriceList = By.xpath("//*[@data-qa='create-btn-trigger']");
	 public static final By btn_cancelPriceList = By.xpath("//*[@data-qa='cancel-btn-trigger']");
	 
	 public static final By lbl_allPriceList = By.xpath("//*[@data-qa='all-price-lists-label']");
	 public static final By lbl_createPriceList = By.xpath("//*[@data-qa='create-pricelist-label']");
	 
	 public static final By btn_settings = By.xpath("//*[@data-qa='settings-trigger']");
	 public static final By btn_reset = By.xpath("//*[@data-qa='reset-trigger']");
	 public static final By btn_search = By.xpath("//*[@data-qa='search-trigger']");
	 public static final By btn_search1 = By.xpath("(//*[@data-qa='search-trigger'])[2]");
	 public static final By txt_priceListName = By.xpath("//*[@data-qa='price-list-name-input']");
	 public static final By txt_priceListDesc = By.xpath("//*[@data-qa='description-input']");
	 public static final By dd_currencyContainer = By.xpath("//*[@data-qa='currency-container']");
	 public static final By dd_selectPriceType = By.xpath("//*[@value='Select price type']");
	 public static final By btn_AddPriceType = By.xpath("//*[@data-qa='add-price-type-btn-trigger']");
	 public static final By priceChkBox = By.xpath("(//*[@class='form-check-label'])[last()]");
	 public static final By currencyListContainer = By.xpath("//*[contains(@class,'dropdown-content select-dropdown')]");
	 public static final By currencyListValues = By.xpath("//*[@class='dropdown-content select-dropdown active'][last()]");
	 public static final By priceTypeListContainer = By.xpath("//*[contains(@class,'dropdown-content select-dropdown')]");
	 public static final By priceTypeListValues = By.xpath("//*[contains(@class,'pricetype')]//*[contains(@class,'dropdown-content select-dropdown')]//span[@class]");
	 public static final By btn_priceSaveOk = By.xpath("//*[@data-qa='price-type-name-save-trigger']");
	 //public static final By priceListSaveCotainer = By.xpath("//*[@class='modal-content']");
	 public static final By priceListSaveCotainer = By.xpath("//*[@data-qa='price-type-name-container']");
	 public static final By priceListSaveCotainerTitle = By.xpath("//*[@class='modal-title']");
	 public static final By priceListSaveSuccessMessage = By.xpath("(//*[@class='secondary-text'])[last()]");
	 public static final By btn_searchpriceList = By.xpath("//*[@data-qa='pricelist-name-search']");
	 public static final By btn_searchStatus = By.xpath("//*[@data-qa='pricelist-status-list']");
	 public static final By txt_searchPricedropDown = By.xpath("//*[@class='dropdown-menu input-dropdown show']");
	 public static final By txt_statusDropDown = By.xpath("//*[@data-qa='pricelist-status-filter']//*[contains(@class,'dropdown-content select-dropdown')][contains(@style,'block')]");
	 public static final By txt_searchPriceList = By.xpath("//*[@data-qa='pricelist-name-list']");
	 public static final By txt_statusValues = By.xpath("(//*[@data-qa='pricelist-status-filter']//*[contains(@class,'dropdown-content select-dropdown')][contains(@style,'block')]//*[@class='filtrable'])");
	 public static final By priceListPageSearchRowContainer = By.xpath("//*[contains(@class,'SearchElementsCont')]");
	 public static final By dd_Project = By.xpath("//*[@data-qa='project-tab-trigger']");
	 public static final By lnk_EditPriceList = By.xpath("//*[@data-qa='edit-btn-trigger']");
     public static final By lnk_EditPriceDetailsTab = By.xpath("//*[@data-qa='details-tab-trigger']");
     public static final By priceListRowContainer = By.xpath("//*[@data-qa='priceLists-rows-container']");
     // change //public static final By lnk_ViewPriceDetails = By.xpath("//*[@data-qa='view-btn-trigger']/a");
     
     public static final By price_delete_button=By.xpath("//*[@data-qa='delete-button-trigger'])[1]");
     
     public static final By price_delete_popup_submit=By.xpath("//*[@data-qa=\"price-type-name-save-trigger\" and contains(@class,'delete-price') and contains(text(),'Delete')]");
     public static final By lnk_ViewPriceDetails = By.xpath("//*[@data-qa='view-btn-trigger']");

     public static final By lnk_EditSkuDetailsTab = By.xpath("//*[@data-qa='skus-tab-trigger']");
     public static final By btn_ProjectDetails = By.xpath("//*[@data-qa='project-tab-trigger']");
 	 public static final By btn_CreateProject = By.xpath("//*[@data-qa='create-new-project-tigger']");
 	 public static final By txt_CreateProject = By.xpath("//*[@data-qa='project-name-container']");
 	 public static final By createProjectContainer = By.xpath("//*[@data-qa='project-model-container']");
 	 public static final By createdProjectList = By.xpath("(//*[@data-qa='project-row-container'])");
 	 public static final By createdProjectName = By.xpath("(//*[@data-qa='project-name-value'])");
 	 public static final By createdProjectCheveronIcon = By.xpath("//*[@data-qa='details-button-trigger']//*[@class]");
 	 public static final By createdProjectDetailsContainer = By.xpath("//*[@data-qa='details-label-container']");
 	 public static final By createdProjectEnterExitIcon = By.xpath("//*[@data-qa='exit-button-trigger']");
 	// change// public static final By btn_CreatedProjectEditDescription = By.xpath("//*[@data-qa='editdescription-button-trigger']");
 	 
 	 public static final By btn_CreatedProjectEditDescription = By.xpath("//*[@data-qa='project-edit-trigger']");

 	
 	//change// public static final By txt_EditDescriptionContainer = By.xpath("//*[@data-qa='description-textarea-container']");
 	 public static final By txt_EditDescriptionContainer = By.xpath("//*[@data-qa='project-description']");


 	
 	 public static final By txt_EditDescription1 = By.id("id_skPrjDescEditBox");
 	 public static final By txt_EditDescription = By.xpath("//*[@class='prjDescShort']");
 	 public static final By btn_ProjectSubmit = By.xpath("//*[@data-qa='project-submit-button']");
 	 public static final By txt_ProjectShortDesc = By.xpath("//*[@class='prjDescShort']");
 	 public static final By btn_NotesTab = By.xpath("//*[@data-qa='notes-tab-trigger']");
 	 public static final By txt_ProjectNotes = By.id("id_projectNotes");
 	 public static final By txt_ProjectNotesArea = By.id("id_noteTextarea");
 	 public static final By txt_AddedProjectNotes = By.xpath("(//*[@data-qa='notes-content-container'])");
 	 public static final By overViewSection = By.xpath("//*[@data-qa='overview-section-item']");
 	 public static final By btn_SkuTab = By.xpath("//*[@data-qa='skus-tab-trigger']");
 	 public static final By lbl_AllSku = By.xpath("//*[@data-qa='all-skus-label']");
 	 public static final By btn_addSKU = By.xpath("//*[@data-qa='add-button']");
 	 public static final By addSKUContainer = By.xpath("//*[@data-qa='add-sku-model-container']");
 	 public static final By txt_SkudId = By.xpath("(//*[@data-qa='skuid-input'])");
 	 public static final By txt_SkudId1 = By.xpath("(//*[@data-qa='skuid-input'])[2]");
 	 public static final By txt_TransactionPrice = By.id("id_skTranactionPrice");
 	 public static final By txt_SalePrice = By.id("transprice-input");
 	 public static final By btn_AddPricesToSKU = By.xpath("//*[@data-qa='addbutton-trigger']");
 	 public static final By addPriceContainer = By.xpath("//*[@data-qa='addprice-model-container']");
 	 public static final By addSKuPriceContainer = By.xpath("//*[@data-qa='add-sku-model-container']");
 	 public static final By btn_SaveAdditionalPricesToSKU = By.xpath("//*[@data-qa='addprice-button-trigger']");
 	 public static final By btn_EditSKU = By.xpath("//*[@data-qa='edit-button-trigger']");
 	 public static final By btn_AddSKUDetails = By.xpath("//*[@data-qa='add-button-trigger']");
 	 public static final By toastMessage = By.xpath("//*[@class='toast-message']");
 	 public static final By skuRowContainer = By.xpath("//*[@data-qa='skus-rows-container']");
 	 public static final By skuIdValue = By.xpath("//*[@data-qa='skuid-value']");
 	 public static final By pricesTab = By.xpath("//*[@data-qa='prices-tab']");
 	 public static final By txt_MinQty = By.xpath("//*[@data-qa='minqty-container']//input");
 	 public static final By txt_MaxQty = By.xpath("//*[@data-qa='maxqty-container']//input");
 	 //public static final By dd_PriceStatus = By.xpath("//*[contains(@class,'price-status')]");
 	 public static final By dd_PriceStatus = By.xpath("//*[contains(@class,'price-status')]//*[@value]");
 	 //public static final By dd_PriceActiveStatus = By.xpath("//select[contains(@class,'price-status')]");
 	 public static final By dd_PriceActiveStatus = By.xpath("//*[@data-qa='addprice-model-container']//*[contains(@class,'dropdown-content select-dropdown')]//span[@class]");
 	 public static final By txt_TransactionalPrice = By.xpath("//*[@data-qa='transprice-container']//input");
 	 public static final By lastSKUContainerRow1 = By.xpath("(//*[@data-qa='prices-rows-container'][last()])");
 	// public static final By lastSKUContainerRow = By.xpath("(//*[@data-qa='prices-rows-container'][1])");
 	 public static final By lastSKUContainerRow = By.xpath("(//*[@data-qa='prices-rows-container' and not(contains(@class,'evergreen_container') and not(contains(@class,'disable')))])[1]");

 	 
 	 
 	 public static final By txt_FloorPrice = By.xpath("//*[@data-qa='floorprice-container']//input");
 	 public static final By icon_StartDate = By.xpath("//*[@data-qa='addprice-model-container']//*[@class='input-group-append dateicon']");
 	 public static final By icon_EndDate = By.xpath("(//*[@data-qa='addprice-model-container']//*[@class='input-group-append dateicon'])[last()]");
 	 public static final By icon_AddSKUStartDate = By.xpath("//*[@data-qa='add-sku-model-container']//*[@class='input-group-append dateicon']");
	 public static final By icon_AddSKUEndDate = By.xpath("(//*[@data-qa='add-sku-model-container']//*[@class='input-group-append dateicon'])[last()]");
 	 public static final By datePickerContainer = By.xpath("//*[contains(@class,'show-calendar')][contains(@style,'block')]");
 	 public static final By startDateValue = By.xpath("//*[contains(@class,'starttcd tcdvalue')]");
 	 public static final By endDateValue = By.xpath("//*[contains(@class,'endtcd tcdvalue')]");
 	 public static final By btn_DateApply = By.xpath("//*[contains(@class,'show-calendar')][contains(@style,'block')]//*[contains(@class,'apply')]");
 	 public static final By dd_YearSelect = By.xpath("//*[contains(@class,'show-calendar')][contains(@style,'block')]//*[contains(@class,'yearselect')]");
 	 public static final By txt_AvailableDateSelect = By.xpath("//*[contains(@class,'show-calendar')][contains(@style,'block')]//*[@class='available']");
 	 public static final By txt_TransactionPriceValue = By.xpath("//*[@data-qa='transprice-value']");
 	 public static final By txt_MinQtyValue = By.xpath("//*[@data-qa='minqty-value']");
 	 public static final By txt_MaxQtyValue = By.xpath("//*[@data-qa='maxqty-value']");
 	 public static final By txt_StartDateValue = By.xpath("//*[@data-qa='startdate-container']//input");
 	 public static final By txt_EndDateValue = By.xpath("//*[@data-qa='enddate-container']//input");
 	 public static final By btn_ProjectApproval = By.xpath("//*[@data-qa='projectdetails-approved-trigger']");
 	 public static final By skuUpdatedTab = By.xpath("//*[@data-qa='updated-tab-trigger']");
 	 public static final By lastUpdatedSKUContainerRow = By.xpath("(//*[@data-qa='updated-rows-container'][last()])");
 	//change /// public static final By txt_ProjectName = By.xpath("//*[@data-qa='project-details-container']//input");
 	 public static final By txt_ProjectName = By.xpath("//*[@data-qa='project-details-container']");

 	 
 	 public static final By icon_CompletionDatePicker = By.xpath("//*[@data-qa='target-completion-datepicker-trigger']//*[contains(@class,'dateicon')]");
 	 public static final By editSKUTableContainer = By.id("id_skEdit_priceList_skus");
 	 public static final By reopenDenyElipses = By.xpath("//*[@class='material-icons valign']");
 	 public static final By btn_denyProject = By.xpath("//*[contains(@class,'btn-projectdetailsdeny')]");
 	 public static final By btn_reOpenProject = By.xpath("//*[contains(@class,'btn-projectdetailsreopen')]");
 	 public static final By btn_ProjectDetailsExit = By.xpath("//*[@data-qa='project-exit-button']");
 	 public static final By txt_EditPriceListName = By.xpath("//*[@data-qa='edit-price-list-name-input']");
	 public static final By txt_EditPriceListDesc = By.xpath("//*[@data-qa='edit-price-list-desc-input']");
	 public static final By btn_EditProjectStatus = By.xpath("//*[@class='pricelist-status']");
	 public static final By txt_ProjectStatus = By.xpath("//*[@class='pricelist-status-label']");
	 public static final By btn_editProjectSave = By.xpath("//*[@data-qa='edit-save-btn-trigger']");
	 public static final By btn_addDetails = By.xpath("//*[@data-qa='adddetails-button-trigger']");
	 public static final By txt_userName = By.xpath("//*[@data-qa='support-list']");
	 public static final By logout = By.xpath("//*[text()='Logout']");
	 public static final By dd_projectList = By.xpath("//*[@data-qa='pricelist-allapproved-list']");
	 public static final By importTab = By.xpath("//*[@data-qa='import-tab-trigger']");
	 public static final By dd_AllProjectContainer = By.xpath("(//*[@data-qa='pricelist-allapproved-filter']//*[contains(@class,'select-dropdown')][contains(@style,'block')])[last()]");
	 public static final By dd_AllProjectValues = By.xpath("(//*[@data-qa='pricelist-allapproved-filter']//*[contains(@class,'select-dropdown')][contains(@style,'block')])[last()]//*[@class='filtrable']");	 
	 public static final By txt_importFile = By.xpath("//*[@data-qa='draganddrop-container']//input");
	 public static final By btn_StartImport = By.xpath("//*[@data-qa='startimport-button-trigger']");
	 public static final By btn_ViewAllProjects = By.xpath("//*[@data-qa='view-all-projects-tigger']");
	 public static final By importSuccessContainer = By.xpath("//*[@class='importCompletedContainer']");
	 public static final By txt_AllProjects = By.xpath("//*[@data-qa='allprojects-heading-label']");
	 public static final By projectRowContainer = By.xpath("//*[@data-qa='pojects-rows-container']");
	 public static final By txt_projectName = By.xpath("//*[contains(@class,'prjName')]");
	
	 
 	
	 public static final By priceDetailButton = By.xpath("//*[@data-qa='edit-button-trigger']/i[1]");
		//public static final By skuRowContainer = By.xpath("//*[@data-qa='skus-rows-container']");
		public static final By deletePriceButton=By.xpath("(//*[@data-qa='delete-button-trigger' and not(contains(@class,'everGreenPrice')) and not(contains(@class,'priceDeleted'))])");
		public static final By deletePriceToast=By.xpath("//*[@class='toast-message']");
		
		public static final By addSkuPriceFacets=By.xpath("(//*[@data-qa=\"transprice-input\" and contains(@name,'facet')])");
		
		public static final By deletePriceContainer=By.xpath("//*[@id='confirm-price-delete']//*[@class='modal-content']");
		public static final By deletePriceContainer_Delete_Btn=By.xpath("(//*[@id='confirm-price-delete']//*[@data-qa='price-type-name-save-trigger'])[1]");
		public static final By deletePriceContainer_Cancel_Btn=By.xpath("(//*[@id='confirm-price-delete']//*[@data-qa='price-type-name-save-trigger'])[1]");

		public static By priceRowContainer = By.xpath("(//*[@data-qa='prices-rows-container'])");
		public static final By priceDeletedStatus=By.xpath("(//*[@data-qa='prices-rows-container']//*[contains(text(),'Deleted')])");

		
 	
	 
}