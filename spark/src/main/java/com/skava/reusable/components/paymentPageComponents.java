package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.UserProfilePage;
import com.skava.object.repository.customerLandingPage;
import com.skava.object.repository.orderPage;
import com.skava.object.repository.paymentPage;

public class paymentPageComponents extends orderPageComponents
{
	public void clickPaymentsTab() throws InterruptedException
	{
		if(driver.explicitWaitforVisibility(paymentPage.paymentsTab, 35, "Payments tab"))
		{
			if(driver.jsClickElement(paymentPage.paymentsTab, "Payments tab")) {
				Thread.sleep(7000);
				pass("Payments Tab is Clicked");
			}
			else
				fail("Payments Tab is not clicked");
		}
	}
	
	public void verifyAddNewPayments()
	{
		if(driver.explicitWaitforVisibility(paymentPage.addnewPaymentMethod, 35, "Payments Method"))
			pass("Add New Payment Method is displayed");
		else
			fail("Add New Payment Method is not displayed");
	}
	
	public void clickAddNewPayments()
	{
		if(driver.explicitWaitforVisibility(paymentPage.addnewPaymentMethod, 35, "Payments Method"))
		{	
			if(driver.clickElement(paymentPage.addnewPaymentMethod, "Payments Method"))
				pass("Add New Payment Method is Clicked");
			else
				fail("Add New Payment Method is not Clicked");
		}
	}
	
	public void verifyAddNewPaymentsSection()
	{
		if(driver.explicitWaitforVisibility(paymentPage.addCreditCardNumberLabel, 35, "add new credit card label"))
			pass("Add New Payment Section is displayed");
		else
			fail("Add New Payment Section is not displayed");
	}
	
	public void verifyListOfCards()
	{
		if(driver.explicitWaitforVisibility(paymentPage.existingCreditCard, 35, "existing card details"))
			pass("List of cards is displayed");
		else
			fail("List of cards is not displayed");
	}
	
	public void clickDownArrow()
	{
		if(driver.explicitWaitforVisibility(paymentPage.downArrow, 35, "downArrow"))
		{
			if(driver.jsClickElement(paymentPage.downArrow, "downArrow"))
				pass("Down Arrow is Clicked");
			else
				fail("Down Arrow is not Clicked");
		}
	}
	
	public void verifyCardDetails()
	{
		if(driver.explicitWaitforVisibility(paymentPage.nameonCardText, 35, "nameoncard"))
			pass("Name on the card is displayed");
		else
			fail("Name on the card is not displayed");
		if(driver.explicitWaitforVisibility(paymentPage.billingAddressText, 35, "billingAddressText"))
			pass("BillingAddress is displayed");
		else
			fail("BillingAddress is not displayed");
	}
	
	public void verifyDetailsOnCard()
	{
		if(driver.explicitWaitforVisibility(paymentPage.creditCardLogo, 35, "creditCardLogo"))
			pass("creditCardLogo is displayed");
		else
			fail("creditCardLogo is not displayed");
		if(driver.explicitWaitforVisibility(paymentPage.cardNumberText, 35, "cardNumberText"))
			pass("cardNumber is displayed");
		else
			fail("cardNumber is not displayed");
		if(driver.explicitWaitforVisibility(paymentPage.expiryDateText, 35, "expiryDate"))
			pass("ExpiryDate is displayed");
		else
			fail("ExpiryDate is not displayed");
	}
	
	public void clickEditButton()
	{
		if(driver.explicitWaitforVisibility(paymentPage.editBtn, 35, "Edit Button"))
		{
			pass("Edit button is displayed");
			if(driver.jsClickElement(paymentPage.editBtn, "Edit Btn"))
				pass("Edit button is clicked");
			else
				fail("Edit button is not clicked");
		}
	}
	
	public void verifyEditSection()
	{
		if(driver.explicitWaitforVisibility(paymentPage.editExpirationLabel, 35, "Edit Card Section"))
			pass("Edit section is displayed");
		else
			fail("Edit section is not displayed");
	}
	
	public void verifyCancelAndSaveBtn()
	{
		if(driver.explicitWaitforVisibility(paymentPage.saveChangesTrigger, 35, "save changes Button"))
			pass("save changes Btn is displayed");
		else
			fail("save changes Btn is not displayed");
		if(driver.explicitWaitforVisibility(paymentPage.editCancelTrigger, 35, "Edit Cancel Button"))
			pass("Edit Cancel Btn is displayed");
		else
			fail("Edit Cancel Btn is not displayed");
	}
	
	public void verifyRemoveBtn()
	{
		if(driver.explicitWaitforVisibility(paymentPage.removeBtn, 35, "remove btn"))
		{
			if(driver.jsClickElement(paymentPage.removeBtn, "remove Btn"))
				pass("Remove Btn is clicked");
			else
				fail("Remove Btn is Not clicked");
		}
	}
	
	public void verifyRemoveConfirmationPopup()
	{
		if(driver.explicitWaitforVisibility(paymentPage.removeCardConfirmationPopup, 35, "Confirmation Popup"))
			pass("Confirmation Popup is displayed");
		else
			fail("confirmation popup is not displayed");
	}
	
	public void verifyExpirationDateAndCvv()
	{
		if(driver.explicitWaitforVisibility(paymentPage.editExpirationLabel, 35, "expiration date"))
			pass("Expiration Date is displayed");
		else
			fail("Expiration date is not displayed");
		
		if(driver.explicitWaitforVisibility(paymentPage.editCvv, 35, "expiration date"))
			pass("CVV is displayed");
		else
			fail("CVV is not displayed");
	}
	
	public void selectExpirationDate()
	{
		if(driver.jsClickElement(paymentPage.selectExpirationDate, "expiration date"))
		{	
			pass("Edit Expiration Date is clicked");
			//int size = driver.getSize(By.xpath("(//*[@class='caret']//span)[3])//ul//li"), "expiration date");
			if(driver.jsClickElement(paymentPage.selectExpirationDate1,"expiration date")) {
				//String date = driver.getText(By.xpath("(//*[contains (@class,'select-wrapper cls_skPaymentTabDropDown cls_skPaymentEditExpirationDate')])[1]//ul//li[1]"), "Date");
				pass("Expiration Date is  selected" );
			}
			else
				fail("Expiration Date is not selected");
		}
		else
			fail("Edit Expiration date is not clicked");
		
	}
	
	public void selectExpirationYear()
	{
		if(driver.jsClickElement(paymentPage.selectExpirationYear, "expiration year"))
		{	
			pass("Edit Expiration Date is clicked");
			//int size = driver.getSize(By.xpath("(//*[@data-qa='edit-expiration-month'])//ul//li"), "expiration date");
			if(driver.jsClickElement(paymentPage.selectExpirationYear1,"expiration year")) {
				pass("Expiration year is  selected");
			}
			else
				fail("Expiration year is not selected");
		}
		else
			fail("Edit Expiration year is not clicked");
	}
	
	public void enterCVV()
	{
		if(driver.explicitWaitforVisibility(paymentPage.editCvv, 35, "Edit Cvv"))
		{
			if(driver.enterText(paymentPage.editCvv, "123", "Cvv"))
				pass("Cvv is editable");
			else
				fail("Cvv is not Editable");
		}
	}
	
	public void VerifyName()
	{
		if(driver.explicitWaitforVisibility(paymentPage.editCardFirstNameInput, 35, "Card Name"))
		{
			if(driver.enterText(paymentPage.editCardFirstNameInput, "Skava", "Card Name"))
				pass("Card Name is Editable");
			else
				fail("Card Name is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.editCardLastNameInput, 35, "Card Name"))
		{
			if(driver.enterText(paymentPage.editCardLastNameInput, "Skava", "Card Name"))
				pass("Card Name is Editable");
			else
				fail("Card Name is not Editable");
		}
	}
	
	public void VerifyBillingAddress()
	{
		if(driver.explicitWaitforVisibility(paymentPage.editAddressInput, 35, "editaddress"))
		{
			if(driver.enterText(paymentPage.editAddressInput, "Skava Systems", "address input"))
				pass("Edit Address is Editable");
			else
				fail("Edit Address is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.editAdddress2Input, 35, "editaddress"))
		{
			if(driver.enterText(paymentPage.editAdddress2Input, "Tidel Park", "address input"))
				pass("Edit Address2 is Editable");
			else
				fail("Edit Address2 is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.editCityInput, 35, "editaddress"))
		{
			if(driver.enterText(paymentPage.editCityInput, "Tidel Park", "address input"))
				pass("Edit City is Editable");
			else
				fail("Edit City is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.editCountryDropdown, 35, "editaddress"))
		{
			if(driver.enterText(paymentPage.editCountryDropdown, "IN", "address input"))
				pass("edit country  is Editable");
			else
				fail("edit country is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.editStateInput, 35, "editaddress"))
		{
			if(driver.enterText(paymentPage.editStateInput, "TN", "address input"))
				pass("edit State is Editable");
			else
				fail("edit State is not Editable");
		}
		driver.scrollToElement(paymentPage.editZipcodeInput, "editaddress");
		if(driver.explicitWaitforVisibility(paymentPage.editZipcodeInput, 35, "editaddress"))
		{
			if(driver.enterText(paymentPage.editZipcodeInput, "641014", "address input"))
				pass("edit zipcode is Editable");
			else
				fail("edit zipcode is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.editPhoneNumberInput, 35, "editaddress"))
		{
			if(driver.enterText(paymentPage.editPhoneNumberInput, "1234567890", "address input"))
				pass("edit PhoneNumber is Editable");
			else
				fail("edit PhoneNumber is not Editable");
		}
		
	}
	
	public void addSelectExpirationDate()
	{
		if(driver.jsClickElement(paymentPage.addSelectExpirationDate, "expiration date"))
		{	
			pass("Edit Expiration Date is clicked"); 
//			int size = driver.getSize(By.xpath("(//*[@data-qa='add-expiration-month'])//ul//li"), "expiration date");
			if(driver.jsClickElement(paymentPage.addSelectExpirationDate1,"expiration date"))
				pass("Expiration Date is  selected");
			else
				fail("Expiration Date is not selected");
		}
		else
			fail("Edit Expiration date is not clicked");
		
	}
	
	public void addSelectExpirationYear()
	{
		if(driver.jsClickElement(paymentPage.addSelectExpirationYear, "expiration date"))
		{	
			pass("Edit Expiration Date is clicked");
			int size = driver.getSize(paymentPage.addSelectExpirationYear1, "expiration date");
			if(driver.jsClickElement(paymentPage.addSelectExpirationYear2,"expiration date"))
				pass("Expiration Date is  selected");
			else
				fail("Expiration Date is not selected");
		}
		else
			fail("Edit Expiration date is not clicked");
	}
	
	
	public void enterCardNumber()
	{
		if(driver.explicitWaitforVisibility(paymentPage.addCreditCardNumberInput, 35, "add card Number"))
		{
			if(driver.enterText(paymentPage.addCreditCardNumberInput, "4111111111111111", "add card Number"))
				pass("Card Number is entered");
			else
				fail("Card Number is not entered");
		}
	}
	
	public void addVerifyBillingAddress()
	{
		if(driver.explicitWaitforVisibility(paymentPage.addAddressInput, 35, "addaddress"))
		{
			if(driver.enterText(paymentPage.addAddressInput, "Skava Systems", "address input"))
				pass("Edit Address is Editable");
			else
				fail("Edit Address is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.addAddress2Input, 35, "addaddress"))
		{
			if(driver.enterText(paymentPage.addAddress2Input, "Tidel Park", "address input"))
				pass("add Address2 is Editable");
			else
				fail("add Address2 is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.addCityInput, 35, "addaddress"))
		{
			if(driver.enterText(paymentPage.addCityInput, "Tidel Park", "address input"))
				pass("add City is Editable");
			else
				fail("add City is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.addZipcodeInput, 35, "addaddress"))
		{
			if(driver.enterText(paymentPage.addZipcodeInput, "641014", "address input"))
				pass("add Zipcode is Editable");
			else
				fail("add Zipcode is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.addStateDropdown, 35, "addaddress"))
		{
			if(driver.enterText(paymentPage.addStateDropdown, "TN", "address input"))
				pass("add State is Editable");
			else
				fail("add State is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.addCountryDropdown, 35, "addaddress"))
		{
			if(driver.enterText(paymentPage.addCountryDropdown, "IN", "address input"))
				pass("add Country is Editable");
			else
				fail("add Country is not Editable");
		}
		if(driver.explicitWaitforVisibility(paymentPage.addPhoneNumberInput, 35, "addaddress"))
		{
			if(driver.enterText(paymentPage.addPhoneNumberInput, "1234567890", "address input"))
				pass("add PhoneNumber is Editable");
			else
				fail("add PhoneNumber is not Editable");
		}
		
		
	}
	
	public void clickOkBtn()
	{
		if(driver.explicitWaitforVisibility(paymentPage.clickOkBtn, 35, "ok btn"))
		{	
			pass("Ok trigger is displayed");
			if(driver.jsClickElement(paymentPage.clickOkBtn1, "ok btn"))
				pass("Ok Btn is clikced");
			else
				fail("Ok btn is not clicked");
		}
		else
			fail("Ok trigger is not displayed");
	}
	
	public void addCVVInput()
    {
        if(driver.explicitWaitforVisibility(paymentPage.addCvvInput, 35, "add Cvv"))
        {
            if(driver.enterText(paymentPage.addCvvInput, "123", "Cvv"))
                pass("Cvv is added");
            else
                fail("Cvv is not added");
        }
    }
public void addCardNames()
    {
        if(driver.explicitWaitforVisibility(paymentPage.addCardFirstNameInput, 35, "Card Name"))
        {
            if(driver.enterText(paymentPage.addCardFirstNameInput, "Skava", "Card Name"))
                pass("Card Name is added");
            else
                fail("Card Name is not added");
        }
        if(driver.explicitWaitforVisibility(paymentPage.addCardLastNameInput, 35, "Card Name"))
        {
            if(driver.enterText(paymentPage.addCardLastNameInput, "Skava", "Card Name"))
                pass("Card Name is added");
            else
                fail("Card Name is not added");
        }
    }
public void addBillingNames()
    {
        if(driver.explicitWaitforVisibility(paymentPage.addFirstNameInput, 35, "Card Name"))
        {
            if(driver.enterText(paymentPage.addFirstNameInput, "first", "Card Name"))
                pass("Card Name is added");
            else
                fail("Card Name is not added");
        }
        if(driver.explicitWaitforVisibility(paymentPage.addLastNameInput, 35, "Card Name"))
        {
            if(driver.enterText(paymentPage.addLastNameInput, "last", "Card Name"))
                pass("Card Name is added");
            else
                fail("Card Name is not added");
        }
    }
public void clickAndSaveBtn() throws InterruptedException
    {
        if(driver.explicitWaitforClickable(paymentPage.addsaveChangesTrigger, 35, "save changes Button"))
        {
//      Thread.sleep(30);   
        if(driver.clickElement(paymentPage.addsaveChangesTrigger, "save changes Button"))
            pass("save changes Btn is Clicked");
        else
            
            fail("save changes Btn is not Clicked");
        }
    }
	
	

}