package com.skava.reusable.components;

import java.util.ArrayList;

import org.openqa.selenium.By;

import com.skava.object.repository.accountPage;
import com.skava.object.repository.customerLandingPage;
import com.skava.object.repository.NotificationLandingPage;
import com.skava.object.repository.UserProfilePage;


public class NotificationLandingPageComponents extends GeneralComponents
{
	
		public void navigateToCreateNotificationPage() {
			
			if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 45, "createBtn"))
			{
			
				driver.jsClickElement(NotificationLandingPage.createNotificationBtn, "createBtn");
				pass("Create Notification button clicked");
				
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
			pass("Create Notification page navigated successfully ");
			
		}
		else {
			fail("Create Notification page not displayed");
		}
			}
			else
			{
				fail("Create Notification button not displayed");
			}
			
			
		}
		
			
public void verfiyCreateNotificationPageFields() {
			if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 45, "createBtn"))
			{
			
				driver.jsClickElement(NotificationLandingPage.createNotificationBtn, "createBtn");
				pass("Create Notification button clicked");
				
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
			pass("Create Notification page navigated successfully ");
		
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
			pass("Create Notification input field available ");
			}
		else {
			fail("Create Notification input field not available");
		}
		
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationdescriptionInput, 45, "notificationDescriptionInput")) {
			pass("Create Notification description input field available ");
			}
		else {
			fail("Create Notification description input field not available");
		}
		
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationEventInput, 45, "notificationEventInput")) {
			pass("Create Notification event input field available ");
			}
		else {
			fail("Create Notification event input field not available");
		}
			
		if(driver.explicitWaitforVisibility(NotificationLandingPage.sendEmailCheckBox, 45, "notificationEmailField")) {
			pass("Create Notification send email field available ");
			}
		else {
			fail("Create Notification send email field not available");
		}
		
		if(driver.explicitWaitforVisibility(NotificationLandingPage.smsCheckBox, 45, "notificationSMSField")) {
			pass("Create Notification send sms field available ");
			}
		else {
			fail("Create Notification send sms field not available");
		}
		
		}
		else {
			fail("Create Notification page not displayed");
		}
			}
			else
			{
				fail("Create Notification button not displayed");
			}
			
		}
		
		
	public void enterNotificationName(String notificationName) {
		
		if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 45, "createBtn"))
		{
		
			driver.jsClickElement(NotificationLandingPage.createNotificationBtn, "createBtn");
			pass("Create Notification button clicked");
			
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
		pass("Create Notification page navigated successfully ");
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
		
		driver.enterText(NotificationLandingPage.notificationNameInput, notificationName, "notificationInput");
		pass("Create notification name entered successfully ");
		}
	else {
		fail("Create Notification input field not available");
	}
	
	
	}
	else {
		fail("Create Notification page not displayed");
	}
		}
		else
		{
			fail("Create Notification button not displayed");
		}
		
		
	}
	
public void enterNotificationDescription(String notificationDescription) {
		
		if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 45, "createBtn"))
		{
		
			driver.jsClickElement(NotificationLandingPage.createNotificationBtn, "createBtn");
			pass("Create Notification button clicked");
			
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
		pass("Create Notification page navigated successfully ");
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationdescriptionInput, 45, "notificationInput")) {
		
		driver.enterText(NotificationLandingPage.notificationdescriptionInput, notificationDescription, "notificationInput");
		pass("Notification description entered successfully ");
		}
	else {
		fail("Create Notification input field not available");
	}
	
	
	}
	else {
		fail("Create Notification page not displayed");
	}
		}
		else
		{
			fail("Create Notification button not displayed");
		}
		
		
	}


public void clickSMSCheckBox() {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 45, "createBtn"))
	{
	
		driver.jsClickElement(NotificationLandingPage.createNotificationBtn, "createBtn");
		pass("Create Notification button clicked");
		
if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
	pass("Create Notification page navigated successfully ");

if(driver.explicitWaitforVisibility(NotificationLandingPage.smsCheckBox, 45, "notificationInput")) {
	
	driver.clickElement(NotificationLandingPage.smsCheckBox, "notificationInput");
	pass("SMS checkbox clicked successfully ");
	}
else {
	fail("SMS checkbox cant able to click");
}


}
else {
	fail("Create Notification page not displayed");
}
	}
	else
	{
		fail("Create Notification button not displayed");
	}
	
	
}
	
	
public void clickSendEmailCheckBox() {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 90, "createBtn"))
	{
	
		driver.jsClickElement(NotificationLandingPage.createNotificationBtn, "createBtn");
		pass("Create Notification button clicked");
		
if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
	pass("Create Notification page navigated successfully ");

if(driver.explicitWaitforVisibility(NotificationLandingPage.sendEmailCheckBox, 45, "notificationInput")) {
	
	driver.clickElement(NotificationLandingPage.sendEmailCheckBox, "notificationInput");
	pass("Send email checkbox clicked successfully ");
	}
else {
	fail("Send email checkbox cant able to click");
}


}
else {
	fail("Create Notification page not displayed");
}
	}
	else
	{
		fail("Create Notification button not displayed");
	}
	
	
}
	

public void enterEmailTemplate(String Emailtemplate) throws InterruptedException {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 90, "createBtn"))
	{
	
		driver.jsClickElement(NotificationLandingPage.createNotificationBtn, "createBtn");
		pass("Create Notification button clicked");
		
if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
	pass("Create Notification page navigated successfully ");
if(driver.explicitWaitforVisibility(NotificationLandingPage.sendEmailCheckBox, 45, "notificationEvent")) {
		
		driver.jsClickElement(NotificationLandingPage.sendEmailCheckBox, "notificationEvent");
		pass("Email checkbox clicked successfully ");
		}
	else {
		fail(" Email checkbox not clicked");
	}
	

if(driver.explicitWaitforVisibility(NotificationLandingPage.emailTemplateInput, 45, "notificationInput")) {
	
	driver.enterText(NotificationLandingPage.emailTemplateInput, Emailtemplate, "notificationInput");
	pass("Email template entered successfully ");
	}
else {
	fail("Email template input field not available");
}


}
else {
	fail("Create Notification page not displayed");
}
	}
	else
	{
		fail("Create Notification button not displayed");
	}
	
	
}


public void enterSMSTemplate(String SMStemplate) {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 45, "createBtn"))
	{
	
		driver.jsClickElement(NotificationLandingPage.createNotificationBtn, "createBtn");
		pass("Create Notification button clicked");
		
if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
	pass("Create Notification page navigated successfully ");

if(driver.explicitWaitforVisibility(NotificationLandingPage.smsCheckBox, 45, "notificationEvent")) {
		
		driver.jsClickElement(NotificationLandingPage.smsCheckBox, "notificationEvent");
		pass("SMS checkbox clicked successfully ");
		}
	else {
		fail(" SMS checkbox not clicked");
	}	
	
if(driver.explicitWaitforVisibility(NotificationLandingPage.smsTemplateInput, 45, "notificationInput")) {
	
	driver.enterText(NotificationLandingPage.smsTemplateInput, SMStemplate, "notificationInput");
	pass("SMS template entered successfully ");
	}
else {
	fail("SMS template input field not available");
}


}
else {
	fail("Create Notification page not displayed");
}
	}
	else
	{
		fail("Create Notification button not displayed");
	}
	
	
}
public void configureEvent(String notificationName, String description, String event, String emailTemplate, String smsTemplate) {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 90, "createBtn"))
	{
	
		driver.jsClickElement(NotificationLandingPage.createNotificationBtn, "createBtn");
		pass("Create Notification button clicked");
		
if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
	pass("Create Notification page navigated successfully ");

if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
	
	driver.enterText(NotificationLandingPage.notificationNameInput, notificationName, "notificationInput");
	pass("Create notification name entered successfully ");
	}
else {
	fail("Create Notification input field not available");
}


if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationdescriptionInput, 45, "notificationDescription")) {
	
	driver.enterText(NotificationLandingPage.notificationdescriptionInput, description, "notificationDescription");
	pass("Notification description entered successfully ");
	}
else {
	fail(" Notification description input field not available");
}


if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationEventInput, 45, "notificationEvent")) {
	
	driver.enterText(NotificationLandingPage.notificationEventInput, event, "notificationEvent");
	pass("Notification event entered successfully ");
	}
else {
	fail(" Notification event input field not available");
}

if(driver.explicitWaitforVisibility(NotificationLandingPage.sendEmailCheckBox, 45, "notificationEvent")) {
	
	driver.jsClickElement(NotificationLandingPage.sendEmailCheckBox, "notificationEvent");
	pass("Email checkbox clicked successfully ");
	}
else {
	fail(" Email checkbox not clicked");
}

if(driver.explicitWaitforVisibility(NotificationLandingPage.emailTemplateInput, 45, "notificationEvent")) {
	
	driver.enterText(NotificationLandingPage.emailTemplateInput, emailTemplate, "notificationEvent");
	pass("Email template entered successfully ");
	}
else {
	fail(" Email template not entered");
}

if(driver.explicitWaitforVisibility(NotificationLandingPage.smsCheckBox, 45, "notificationEvent")) {
	
	driver.jsClickElement(NotificationLandingPage.smsCheckBox, "notificationEvent");
	pass("SMS checkbox clicked successfully ");
	}
else {
	fail(" SMS checkbox not clicked");
}


if(driver.explicitWaitforVisibility(NotificationLandingPage.smsTemplateInput, 45, "notificationEvent")) {
	
	driver.enterText(NotificationLandingPage.smsTemplateInput, smsTemplate, "notificationEvent");
	pass("SMS template entered successfully ");
	}
else {
	fail(" SMS template not entered");
}



if(driver.explicitWaitforVisibility(NotificationLandingPage.saveNotificationBtn, 45, "notificationEvent")) {
	
	driver.jsClickElement(NotificationLandingPage.saveNotificationBtn, "notificationEvent");
	pass("Save button clicked successfully ");
	}
else {
	fail("Save button not clicked");
}





}
else {
	fail("Create Notification page not displayed");
}
	}
	
	
	else
	{
		fail("Create Notification button not displayed");
	}
	
	
}




public void searchNotificationName(String notificationName) throws InterruptedException {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 90, "createBtn"))
	{
		pass("Create Notification button displayed");
		
		
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameSearch, 55, "notificationNameSearch")) {
		
		driver.jsClickElement(NotificationLandingPage.notificationNameSearch, "notificationEvent");
		pass("Notification name search option clicked successfully ");
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameSearchInput, 45, "notificationNameSearch")) {
		driver.enterText(NotificationLandingPage.notificationNameSearchInput, notificationName, "notificationEvent");
		pass("Notification name entered in notification search field ");
		}
		else {
			fail("Notification name not entered in notification search field");
		}
		
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationSearchBtn, 45, "notificationSearchBtn")) {
			driver.jsClickElement(NotificationLandingPage.notificationSearchBtn, "notificationSearchBtn");
			pass("Notification search button clicked successfully ");
		}
		else {
			fail("Notification search button not clicked");
		}
		
	}
	else
	{
		fail("Notification name search option not clicked");
	}
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationListName, 45, "notificationEvent")) {
		
		String actualnotificationName = driver.getText(NotificationLandingPage.notificationListName, "notificationEvent");
		//System.out.println("actualnotificationName-->"+actualnotificationName);
		//System.out.println("expectednotificationName-->"+notificationName);
		if(actualnotificationName.equalsIgnoreCase(notificationName)) {
		pass("Notification and Event configured successfully and displayed in notification list Page ");
		}
		else {
			fail("Notification and Event configured not displayed in notification list Page");
		}
		}


	else {
		fail("Notification list page not displayed");
	}
		
	}


	
	else
	{
		fail("Create Notification button not displayed");
	}
	

}

public void searchNotificationDescription(String notificationDescription) throws InterruptedException {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 90, "createBtn"))
	{
		pass("Notification Landing Page displayed");
		
		
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationDescriptionSearch, 45, "notificationNameSearch")) {
		
		driver.jsClickElement(NotificationLandingPage.notificationDescriptionSearch, "notificationEvent");
		pass("Notificationn description search option clicked successfully ");
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationDescriptionSearchInput, 45, "notificationNameSearch")) {
		driver.enterText(NotificationLandingPage.notificationDescriptionSearchInput, notificationDescription, "notificationEvent");
		pass("Notificationn description entered in notification search field ");
		}
		else {
			fail("Notificationn description not entered in notification search field");
		}
		
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationSearchBtn, 45, "notificationSearchBtn")) {
			driver.jsClickElement(NotificationLandingPage.notificationSearchBtn, "notificationSearchBtn");
			pass("Notificationn search button clicked successfully ");
		}
		else {
			fail("Notificationn search button not clicked");
		}
		
	}
	else
	{
		fail("Notification description search option not clicked");
	}
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationListDescription, 45, "notificationEvent")) {
		
		String actualnotificationDescription = driver.getText(NotificationLandingPage.notificationListDescription, "notificationEvent");
		//System.out.println("actualnotificationName-->"+actualnotificationDescription);
		//System.out.println("expectednotificationName-->"+notificationDescription);
		if(actualnotificationDescription.equalsIgnoreCase(notificationDescription)) {
		pass("Notification description search option working fine ");
		}
		else {
			fail("Notification and Event configured not displayed in notification list Page");
		}
		}


	else {
		fail("Notification list page not displayed");
	}
		
	}


	
	else
	{
		fail("Create Notification button not displayed");
	}
	

}


public void searchNotificationEvent(String notificationEvent) throws InterruptedException {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 90, "createBtn"))
	{
		pass("Notification Landing Page displayed");
		
		
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationEventSearch, 45, "notificationEventNameSearch")) {
		
		driver.jsClickElement(NotificationLandingPage.notificationEventSearch, "notificationEvent");
		pass("Notification event search option clicked successfully ");
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationEventSearchInput, 45, "notificationEventSearch")) {
		driver.enterText(NotificationLandingPage.notificationEventSearchInput, notificationEvent, "notificationEvent");
		pass("Notification event entered in event search field ");
		}
		else {
			fail("Notification event not entered in event search field");
		}
		
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationSearchBtn, 45, "notificationSearchBtn")) {
			driver.jsClickElement(NotificationLandingPage.notificationSearchBtn, "notificationSearchBtn");
			pass("Notification search button clicked successfully ");
		}
		else {
			fail("Notification search button not clicked");
		}
		
	}
	else
	{
		fail("Notification event search option not clicked");
	}
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationListEvent, 45, "notificationEvent")) {
		
		String actualnotificationDescription = driver.getText(NotificationLandingPage.notificationListEvent, "notificationEvent");
		//System.out.println("actualnotificationName-->"+actualnotificationDescription);
		//System.out.println("expectednotificationName-->"+notificationDescription);
		if(actualnotificationDescription.equalsIgnoreCase(notificationEvent)) {
		pass("Notification event search option working fine ");
		}
		else {
			fail("Notification and Event configured not displayed in notification list Page");
		}
		}


	else {
		fail("Notification list page not displayed");
	}
		
	}


	
	else
	{
		fail("Create Notification button not displayed");
	}
	

}




public void searchNotificationNameAndDescription(String notificationName, String description) throws InterruptedException {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 45, "createBtn"))
	{
		pass("Create Notification button displayed");
		
		
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameSearch, 45, "notificationNameSearch")) {
		
		driver.jsClickElement(NotificationLandingPage.notificationNameSearch, "notificationEvent");
		pass("Notificationn name search option clicked successfully ");
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameSearchInput, 45, "notificationNameSearch")) {
		driver.enterText(NotificationLandingPage.notificationNameSearchInput, notificationName, "notificationEvent");
		pass("Notificationn name entered in notification search field ");
		}
		else {
			fail("Notificationn name not entered in notification search field");
		}
		
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationSearchBtn, 45, "notificationSearchBtn")) {
			driver.jsClickElement(NotificationLandingPage.notificationSearchBtn, "notificationSearchBtn");
			pass("Notificationn search button clicked successfully ");
		}
		else {
			fail("Notificationn search button not clicked");
		}
		
	}
	else
	{
		fail("Notificationn name search option not clicked");
	}
	
	searchNotificationDescription(description);
	
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationListName, 45, "notificationEvent")) {
		
		String actualnotificationName = driver.getText(NotificationLandingPage.notificationListName, "notificationEvent");
		//System.out.println("actualnotificationName-->"+actualnotificationName);
		//System.out.println("expectednotificationName-->"+notificationName);
		if(actualnotificationName.equalsIgnoreCase(notificationName)) {
		pass("Notification name and description search option working fine ");
		}
		else {
			fail("Notification name and description search option not working");
		}
		}


	else {
		fail("Notification list page not displayed");
	}
		
	}


	
	else
	{
		fail("Create Notification button not displayed");
	}
	

}


public void searchNotificationNameAndEvent(String notificationName, String event) throws InterruptedException {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 45, "createBtn"))
	{
		pass("Create Notification button displayed");
		
		
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameSearch, 45, "notificationNameSearch")) {
		
		driver.jsClickElement(NotificationLandingPage.notificationNameSearch, "notificationEvent");
		pass("Notification name search option clicked successfully ");
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameSearchInput, 45, "notificationNameSearch")) {
		driver.enterText(NotificationLandingPage.notificationNameSearchInput, notificationName, "notificationEvent");
		pass("Notification name entered in notification search field ");
		}
		else {
			fail("Notification name not entered in notification search field");
		}
		
		if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationSearchBtn, 45, "notificationSearchBtn")) {
			driver.jsClickElement(NotificationLandingPage.notificationSearchBtn, "notificationSearchBtn");
			pass("Notification search button clicked successfully ");
		}
		else {
			fail("Notification search button not clicked");
		}
		
	}
	else
	{
		fail("Notification name search option not clicked");
	}
	
	searchNotificationEvent(event);
	
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationListName, 90, "notificationEvent")) {
		
		String actualnotificationName = driver.getText(NotificationLandingPage.notificationListName, "notificationEvent");
		//System.out.println("actualnotificationName-->"+actualnotificationName);
		//System.out.println("expectednotificationName-->"+notificationName);
		if(actualnotificationName.equalsIgnoreCase(notificationName)) {
		pass("Notification name and event search option working fine ");
		}
		else {
			fail("Notification name and event search option not working");
		}
		}


	else {
		fail("Notification list page not displayed");
	}
		
	}


	
	else
	{
		fail("Create Notification button not displayed");
	}
	

}




public void verifyNotificationLandingPageFields() {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 45, "createBtn"))
	{
		pass("Notification landing page displayed successfully");
		
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameHeading, 45, "notificationNameHeading"))
		{
			pass("Notification name field displayed in the screen");
		}
	else {
		fail("Notification name field not displayed");
	}
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationEventNameHeading, 45, "notificationDescriptionHeading"))
	{
		pass("Event name field displayed in the screen");
	}
else {
	fail("Event name field is not displayed");
}
		
}
	else {
		fail("Notification landing page not displayed");
	}

}


public void verifyPageLengthInNotificationLandingPage() {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 45, "createBtn"))
	{
		pass("Notification landing page displayed successfully");
		
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationPageLength, 45, "notificationPageLength"))
		{
			pass("No of rows drop down option available");
			
		}
	else {
		fail("No of rows drop down option not available");
	}
	

}
}


public void verifyPaginationLimitSelect() {
	
	if(driver.explicitWaitforVisibility(NotificationLandingPage.createNotificationBtn, 45, "createBtn"))
	{
		pass("Notification landing page displayed successfully");
		
	if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationPageLength, 45, "notificationPageLength"))
		{
			pass("No of rows drop down option available");
	driver.jsClickElement(NotificationLandingPage.notificationPageLength,  "notificationPageLength");
	
	
	int size = driver.getSize(NotificationLandingPage.notificationPageLengthSize, "notificationPageLengthSize");
	
	for(int i=1; i<=size;i++) {
		String text = driver.getText(By.xpath(NotificationLandingPage.notificationPageLengthSize.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"notificationPageLengthSize");
	}
		}
	else {
		fail("No of rows drop down option not available");
	}
	

}
}


public void clickNotificationViewPage() {
	if(driver.explicitWaitforVisibility(NotificationLandingPage.viewNotification, 45, "notificationViewPageBtn"))
	{
		
		driver.jsClickElement(NotificationLandingPage.viewNotification, "notificationViewPageBtn");
		pass("View notification clicked successfully");
}

	else {
		pass("View notification is not clicked");
	}
}

public void editNotification() {
	if(driver.explicitWaitforVisibility(NotificationLandingPage.editNotification, 45, "editNotificationButton"))
	{
		
		driver.jsClickElement(NotificationLandingPage.editNotification, "editNotificationButton");
		pass("Edit notification clicked successfully");
}

	else {
		pass("Edit notification is not clicked");
	}
}

public void EditEvent(String notificationName, String description, String event, String emailTemplate, String smsTemplate) {
	
		
if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
	pass("Edit Notification page navigated successfully ");

if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationNameInput, 45, "notificationInput")) {
	
	driver.enterText(NotificationLandingPage.notificationNameInput, notificationName, "notificationInput");
	pass("notification name entered successfully ");
	}
else {
	fail("Create Notification input field not available");
}


if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationdescriptionInput, 45, "notificationDescription")) {
	
	driver.enterText(NotificationLandingPage.notificationdescriptionInput, description, "notificationDescription");
	pass("Notification description entered successfully ");
	}
else {
	fail(" Notification description input field not available");
}


if(driver.explicitWaitforVisibility(NotificationLandingPage.notificationEventInput, 45, "notificationEvent")) {
	
	driver.enterText(NotificationLandingPage.notificationEventInput, event, "notificationEvent");
	pass("Notification event modified successfully ");
	}
else {
	fail(" Notification event input field not available");
}

/*if(driver.explicitWaitforVisibility(NotificationLandingPage.sendEmailCheckBox, 45, "notificationEvent")) {
	
	driver.jsClickElement(NotificationLandingPage.sendEmailCheckBox, "notificationEvent");
	pass("Email checkbox clicked successfully ");
	}
else {
	fail(" Email checkbox not clicked");
}*/

if(driver.explicitWaitforVisibility(NotificationLandingPage.emailTemplateInput, 45, "notificationEvent")) {
	
	driver.enterText(NotificationLandingPage.emailTemplateInput, emailTemplate, "notificationEvent");
	pass("Email template modified successfully ");
	}
else {
	fail(" Email template not entered");
}

/*if(driver.explicitWaitforVisibility(NotificationLandingPage.smsCheckBox, 45, "notificationEvent")) {
	
	driver.jsClickElement(NotificationLandingPage.smsCheckBox, "notificationEvent");
	pass("SMS checkbox clicked successfully ");
	}
else {
	fail(" SMS checkbox not clicked");
}*/


if(driver.explicitWaitforVisibility(NotificationLandingPage.smsTemplateInput, 45, "notificationEvent")) {
	
	driver.enterText(NotificationLandingPage.smsTemplateInput, smsTemplate, "notificationEvent");
	pass("SMS template modified successfully ");
	}
else {
	fail(" SMS template not entered");
}



if(driver.explicitWaitforVisibility(NotificationLandingPage.saveNotificationBtn, 45, "notificationEvent")) {
	
	driver.jsClickElement(NotificationLandingPage.saveNotificationBtn, "notificationEvent");
	pass("Save button clicked successfully ");
	}
else {
	fail("Save button not clicked");
}

}
else {
	fail("Edit Notification page not displayed");
}
	
	
	
}



	}


	


















