package com.skava.reusable.components;

import java.util.ArrayList;

import org.openqa.selenium.By;

import com.skava.object.repository.accountPage;
import com.skava.object.repository.customerLandingPage;
import com.skava.object.repository.UserProfilePage;
public class customerLandingPageComponents extends GeneralComponents
{
	String firstName = "";
	String lastName = "";
	String email = "";
	ArrayList<String> as = new ArrayList(); 
	

	public void navigateToUserProfilePage() throws InterruptedException
	{
//		String url = "http://euat.skavaone.com:32014/customeradmin/customers/2?collectionId=1";
//		driver.navigateToUrl(url);q
		if(driver.explicitWaitforVisibility(customerLandingPage.navUserProfile1, 45, "View"))
		{
		
			driver.mouseHoverElement(customerLandingPage.navUserProfile1, "customer value");
			driver.jsClickElement(customerLandingPage.navUserProfileView, "View");
			pass("View button clicked");
		}
		else
		{
			fail("View button not displayed");
		}
	}

	
	public void displayCustomerTitle()
	{
		
		if(driver.explicitWaitforVisibility(customerLandingPage.customerTitle, 35, "customerTitle"))
		{
			pass("Customer Title displayed");
		}
		else
		{
			fail("Customer Title not displayed");
		}
		
	}
	
	public void displayaddCustomertrigger()
	{
		
		if(driver.explicitWaitforClickable(customerLandingPage.addcustomerTrigger, 35, "addCustomerTriggerDisplay"))
		{
			pass("Add Customer button displayed");
		}
		else
		{
			fail("Add Customer button not displayed");
		}
		
	}
	
	public void clickaddCustomerTrigger()
	{
//		driver.explicitWaitforClickable(customerLandingPage.addcustomerTrigger, 50, "addCustomerTrigger");
		if(driver.jsClickElement(customerLandingPage.addcustomerTrigger, "addCustomerTrigger"))
		{
			pass("Add Customer button is clicked");
		}
		else
		{
			fail("Add Customer button is not clicked");
		}
		
	}
	
	public void displayaddCustomerTitle()
	{
		
		if(driver.explicitWaitforVisibility(customerLandingPage.addCustomerTitle, 35, "addCustomerTitle"))
		{
			pass("Add Customer Title Displayed");
		}
		else
		{
			fail("Add Customer Title not Displayed");
		}
		
	}
	public void selectOrderType()
	{
		if(driver.explicitWaitforClickable(customerLandingPage.ordertypelist, 35, "Order type list"))
		{
			if(driver.selectByIndex(customerLandingPage.ordertypelist, 1, "select Online")) {
				pass("order type select online");}
	
		}
		else
			fail("Order type not display");
		
	}
	public void ordertypeResult()
	{
		int size=driver.getSize(customerLandingPage.latestOrderValue, "");
		for(int i=1;i<=size;i++)
		{
			//String name=driver.getText(By.xpath("(//*[@data-qa='latest-order-value'])["+i+"]"), "");
			String name=driver.getText(By.xpath(customerLandingPage.ordertypeResult1.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"");
			if(name.equals("")) {
				pass("");}
			else {
				fail("");}
			
		}
		
	}
	public void selectStatus()
	{
		if(driver.explicitWaitforClickable(customerLandingPage.statuslist, 35, "Status list"))
		{
			if(driver.selectByIndex(customerLandingPage.statuslist, 1, "select Active")) {
				pass("Status select Active");}
	
		}
		else {
			fail("Status not display");}
		
	}
	
	public void selectExchange()
	{
		if(driver.explicitWaitforClickable(customerLandingPage.fulfillmentlist, 35, "Exchange list"))
		{
			if(driver.selectByIndex(customerLandingPage.fulfillmentlist, 1, "select fulfillment list")) {
				pass("Exchange select fulfillment");}
	
		}
		else {
			fail("Exchange not display");}
		
	}
	public void selectExchangeList()
	{/*
		if(driver.explicitWaitforClickable(customerLandingPage.fulfillmentlist, 10, "Exchange list"))
		{
			int size=driver.getSize(customerLandingPage.fulfillmentlist, "Fulfillment");
			for(int i=0;i<size;i++)
			{
			if(driver.selectByIndex(customerLandingPage.fulfillmentlist, i, "select fulfillment list"))
				pass("Exchange select fulfillment");
			ordertypeResult();
			}
	
		}
		else
			fail("Exchange not display");
		
	*/}
	public void selectDateFrom()
	{/*
		if(driver.explicitWaitforClickable(customerLandingPage.datePickerFrom, 10, "Exchange list"))
		{
			if(driver.clickElement(customerLandingPage.datePickerFrom,  "select date Picker From"))
				pass("Select date Picker From");
	
		}
		else
			fail("date Picker From not display");
		
	*/}
	public void selectDateTo()
	{/*
		if(driver.explicitWaitforClickable(customerLandingPage.datePickerTo, 10, "Exchange list"))
		{
			if(driver.clickElement(customerLandingPage.datePickerTo,  "select date Picker To"))
				pass("Select date Picker To");
	
		}
		else
			fail("date Picker From not display");
		
	*/}
	public void displayaddCustomerCloseTrigger()
	{
		
		if(driver.explicitWaitforClickable(customerLandingPage.closeTrigger, 35, "closeTriggerVisibility"))
		{
			pass("Close icon is clicked");
		}
		else
		{
			fail("Close icon is not clicked");
		}
	}
	
	public void clickaddCustomerCloseTrigger()
	{
		
		if(driver.jsClickElement(customerLandingPage.closeTrigger, "closeTrigger"))
		{
			pass("Close icon is clicked");
		}
		else
		{
			fail("Close icon is not clicked");
		}
	}
	
	public void displayCustomerFirstNameForAddCustomer()
	{
		
		if(driver.explicitWaitforClickable(customerLandingPage.firstnameInput, 35, "displayCustomerFirstName"))
		{
			pass("First Name field is displayed");
		}
		else
		{
			fail("First Name field is not displayed");
		}
		
	}
		
	public void enterCustomerFirstNameForAddCustomer()
	{
		firstName="skava";
		if(driver.enterText(customerLandingPage.firstnameInput, firstName, "firstNameInput"))
		{
			pass("First Name is entered");
		}
		else
		{
			fail("First Name is not entered");
		}
		
	}
	
	public void displayCustomerLastNameForAddCustomer()
	{
		
		if(driver.explicitWaitforClickable(customerLandingPage.lastnameInput, 35, "displayCustomerLastName"))
		{
			pass("Last Name field is displayed");
		}
		else
		{
			fail("Last Name field is not displayed");
		}
		
	}

	public void verifyDatePrev()
	{/*
		if(driver.explicitWaitforClickable(customerLandingPage.datePickerFrom, 10, "Date picker"))
		{
			//verify date picker 
			
		}
	*/}
	public void verifyDateCurrent()
	{/*
		if(driver.explicitWaitforClickable(customerLandingPage.datePickerFrom, 10, "Date picker"))
		{
			//verify date picker 
			
		}
	*/}
	public void verifyDateFuture()
	{/*
		if(driver.explicitWaitforClickable(customerLandingPage.datePickerFrom, 10, "Date picker"))
		{
			//verify date picker 
			
		}
	*/}
	public void enterCustomerLastNameForAddCustomer()
	{
		lastName="test";
		if(driver.enterText(customerLandingPage.lastnameInput, lastName, "lastNameInput"))
		{
			pass("Last Name is entered");
		}
		else
		{
			fail("Last Name is not entered");
		}
		
	}
	
	public void displayCustomerEmailForAddCustomer()
	{
		
		if(driver.explicitWaitforClickable(customerLandingPage.emailInput, 35, "displayCustomerEmail"))
		{
			pass("Email field is displayed");
		}
		else
		{
			fail("Email field is not displayed");
		}
		
	}
	public void verifyplacingcursor()
	{
		if(driver.explicitWaitforVisibility(customerLandingPage.firstnameInput, 30, "First Name"))
		{
			if(driver.jsClickElement(customerLandingPage.firstnameInput,  "First Name"))
			{
				String firstname=driver.getElementAttribute(customerLandingPage.firstnameInput, "value", "first name  place cursor");
				if(firstname.equals("")) {
					pass("first name place cursor remove");}
				else {
					fail("first name place cursor name not remove");}
				String lastname=driver.getElementAttribute(customerLandingPage.lastnameInput, "value", "last name place cursor");
				if(lastname.equals("")) {
					pass("last name place cursor remove");}
				else {
					fail("last name place cursor name not remove");}
				String email=driver.getElementAttribute(customerLandingPage.emailInput, "value", "email name place cursor");
				if(email.equals("")) {
					pass("email name place cursor remove");}
				else {
					fail("email name place cursor name not remove");}
			}
		}
	}
	
	public void enterCustomerEmailForAddCustomer()
	{
		
		driver.explicitWaitforClickable(customerLandingPage.emailInput, 35, "emilaInput");
		email="skava"+driver.generateRandomNumber(10000)+"@gmail.com";
		if(driver.enterText(customerLandingPage.emailInput, email, "emilaInput"))
		{
			pass("Email address is entered");
		}
		else
		{
			fail("Email address is not entered");
		}
		
	}
	
	public void displaySendInviteButton()
	{
		
		if(driver.explicitWaitforClickable(customerLandingPage.sendInviteTrigger, 35, "dsiplaySendInviteTrigger"))
		{
			pass("Send Invite button is clicked");
		}
		else {
			fail("Send Invite button is not clicked");}
		
	}
	
	public void clickSendInviteButton()
	{
		
		if(driver.jsClickElement(customerLandingPage.sendInviteTrigger, "sendInviteTrigger"))
		{
			pass("Send Invite button is clicked");
		}
		else {
			fail("Send Invite button is not clicked");}
		
	}
	
	public void displaySearchInput()
	{/*
		
		if(driver.explicitWaitforClickable(customerLandingPage.searchInput, 70, "displaySearchInput"))
		{
			pass("Search Input field is displayed");
		}
		else
		{
			fail("Search Input field is not displayed");
		}
		
	*/}
	
	public void enterSearchInput()
	{/*
		email="skava"+driver.generateRandomNumber(10000)+"test@gmail.com";
		if(driver.enterText(customerLandingPage.searchInput, email, "enterSearchInput"))
		{
			pass("Search input is entered");
		}
		else
		{
			fail("Search input is not entered");
		}
		
	*/}

	public void clickSearchTrigger()
	{
		
		if(driver.jsClickElement(customerLandingPage.searchTrigger, "clickSearchTrigger"))
		{
			pass("Search button is clicked");
		}
		else
		{
			fail("Search button is not clicked");
		}
		
	}
	
	public void compareSearchedEmailInResult()
	{/*
		if(driver.compareElementText(customerLandingPage.emailAddressValue, email, "searchEnteredEmailInResult"))
		{
			pass("Searched email address is displayed");	
		}
		else
		{
			fail("Searched email address is not displayed");
		}
	*/}
	
	public void displayStatusForCreatedUser()
	{
		String statusvalue = driver.getText(customerLandingPage.statusValue, "statusValue");
		if(statusvalue.contains("in"))
		{
			pass("Status is Inactive for newly created User");
		}
		else
		{
			pass("Status is not Inactive for newly created user");
		}
	}
	
	public void displayFirstNameCloseTrigger()
	{/*
		
		if(driver.explicitWaitforClickable(customerLandingPage.firstNameCloseTrigger, 70, "displayFirstNameCloseTrigger"))
		{
			pass("Close icon is displayed in First Name field");
		}
		else
		{
			pass("Close icon is not displayed in First Name field");
		}
		
	*/}
	
	public void clickFirstNameCloseTrigger()
	{
		
		/*if(driver.clickElement(customerLandingPage.firstNameCloseTrigger, "displayFirstNameCloseTrigger"))
		{
			pass("Close icon of First Name field is clicked");
		}
		else
		{
			pass("Close icon of First Name field is not clicked");
		}*/
		
	}
	
	public void displayLastNameCloseTrigger()
	{/*
		
		if(driver.explicitWaitforClickable(customerLandingPage.lastNameCloseTrigger, 70, "displayLastNameCloseTrigger"))
		{
			pass("Close icon is displayed in Last Name field");
		}
		else
		{
			pass("Close icon is not displayed in Last Name field");
		}
	*/}
	
		public void clickLastNameCloseTrigger()
		{/*
			
			if(driver.clickElement(customerLandingPage.lastNameCloseTrigger, "displayLastNameCloseTrigger"))
			{
				pass("Close icon of Last Name field is clicked");
			}
			else
			{
				pass("Close icon of Last Name field is not clicked");
			}
			
		*/}
		
		public void displayEmailCloseTrigger()
		{/*
			
			if(driver.explicitWaitforClickable(customerLandingPage.emailCloseTrigger, 70, "displayEmailCloseTrigger"))
			{
				pass("Close icon is displayed in Email field");
			}
			else
			{
				pass("Close icon is not displayed in Email field");
			}
		
		*/}
		
		public void clickEmailCloseTrigger()
		{/*
			
			if(driver.clickElement(customerLandingPage.emailCloseTrigger, "displayEmailCloseTrigger"))
			{
				pass("Close icon of Email field is clicked");
			}
			else
			{
				pass("Close icon of Email field is not clicked");
			}
			
		*/}
		public void nameFilterClick()
		{
			
				if(driver.explicitWaitforClickable(customerLandingPage.nameFilter, 35, "Name Drop Down"))
				{
					pass("Name filter drop down display");
					if(driver.jsClickElement(customerLandingPage.nameFilter, "Name Filter")) {
						pass("Name filter clicked");}
					else {
						fail("Name filter Not display");}
				}
				else {
					fail("Name filter drop down not display");}
				
		
			////UI CHANGED so newly add 
			firstNameSearch();
			lastnamefilter();
			lastNameSearch();
			emailfilter();
			emailNameSearch();
			
			
			
			
			
			
		}
		public void lastnamefilter()
		{
			
				if(driver.explicitWaitforVisibility(customerLandingPage.lastnameFilter, 35, "Name Drop Down"))
				{
					pass("last nameF  filter drop down display");
					if(driver.jsClickElement(customerLandingPage.lastnameFilter, "Name Filter")) {
						pass("Last filter clicked");}
					else {
						fail("Last filter Not display");}
				}
				else {
					fail("Last name filter drop down not display");}
				
						
		}
		public void emailfilter()
		{
			
				if(driver.explicitWaitforVisibility(customerLandingPage.emailFilter, 35, "Name Drop Down"))
				{
					pass("Email filter drop down display");
					if(driver.jsClickElement(customerLandingPage.emailFilter, "Name Filter")) {
						pass("Email filter clicked");}
					else {
						fail("Email filter Not display");}
				}
				else {
					fail("Email filter drop down not display");}
				
			
			
		}
		public void searchNameFilter()
		{
			
			
			// ui Changed so newly added
						
			
			
		}
		public void  firstNameSearch()
		{
			if(driver.explicitWaitforVisibility(customerLandingPage.nameFilter, 35, "Name Drop Down"))
			{				
				if(driver.explicitWaitforVisibility(customerLandingPage.nameSearch, 35,"Name Filter Search"))
				{
					if(driver.enterText(customerLandingPage.nameSearch, "Read sheet", "Name filter Search")) {
						pass("Name filter Searching  ");}
					
				}
					
				else {
					fail("Name filter Search Not display");}
			}
			else {
				fail("Name filter drop down not display");}
			
		}
		public void  lastNameSearch()
		{
			if(driver.explicitWaitforVisibility(customerLandingPage.lastnameFilter, 35, "Name Drop Down"))
			{				
				if(driver.explicitWaitforVisibility(customerLandingPage.lastnameSearch, 35,"Name Filter Search"))
				{
					if(driver.enterText(customerLandingPage.lastnameSearch, "Read sheet", "Name filter Search")) {
						pass("Last name filter Searching  ");}
					
				}
					
				else {
					fail("Last name filter Search Not display");}
			}
			else {
				fail("Last name filter drop down not display");}
			
			
		}
		public void  emailNameSearch()
		{
			if(driver.explicitWaitforVisibility(customerLandingPage.emailFilter, 35, "Name Drop Down"))
			{				
				if(driver.explicitWaitforVisibility(customerLandingPage.eamilSearch, 35,"Name Filter Search"))
				{
					if(driver.enterText(customerLandingPage.eamilSearch, "Read sheet", "Name filter Search")) {
						pass("Email filter Searching  ");}
					
				}
					
				else {
					fail("email filter Search Not display");}
			}
			else {
				fail("email filter drop down not display");}
			
		}
		
		public void statusFilterClick()
		{
			if(driver.explicitWaitforVisibility(customerLandingPage.addCustomerTitle, 35, "Customer Title"))
			{
				if(driver.explicitWaitforVisibility(customerLandingPage.statusFilter, 35, "Status Drop Down"))
				{
					pass("Name filter drop down display");
					if(driver.jsClickElement(customerLandingPage.statusFilter, "status Filter")) {
						pass("Status filter clicked");}
					else {
						fail("Status filter Not display");}
				}
				else {
					fail("Status filter drop down not display");}
				
			}
			
		}
	
		
		public void addmorefilterClick()
		{/*
			if(driver.explicitWaitforVisibility(customerLandingPage.addCustomerTitle, 10, "Customer Title"))
			{
				if(driver.explicitWaitforVisibility(customerLandingPage.addMoreFilter, 10, "Add more filter Drop Down"))
				{
					pass("Add more filter drop down display");
					if(driver.clickElement(customerLandingPage.addMoreFilter, "Add more filter"))
						pass("Add more filter clicked");
					else
						fail("Add more filter Not display");
				}
				else
					fail("Add more filter drop down not display");
				
			}
			
		*/}
		public void selectaddmoreFilter()
		{/*
			if(driver.explicitWaitforVisibility(customerLandingPage.addMoreFilter, 10, "Add More filter Drop Down"))
			{				
				if(driver.explicitWaitforVisibility(customerLandingPage.addMoreFilter,10, "Add More filter Search"))
				{
					if(driver.selectByIndex(customerLandingPage.addMoreFilter, 0, "Add More  filter Search"))
						pass("Status filter selected  ");
					if(driver.selectByIndex(customerLandingPage.addMoreFilter, 1, "Add More filter Search"))
						pass("Add More filter selected  ");
					if(driver.selectByIndex(customerLandingPage.addMoreFilter, 2, "Add More filter Search"))
						pass("Add More filter selected  ");
					
				}
					
				else
					fail("Add More  filter drop down list Not display");
			}
			else
				fail("Add More filter drop down not display");
					
		*/}
		
		public void verifyFilterOption()
		{/*
			if(driver.explicitWaitforVisibility(customerLandingPage.addressFilter, 10, "Address filter"))
			{	
				pass("Address filter display");
				if(driver.explicitWaitforClickable(customerLandingPage.cityFilter, 5, "City filter"))
					pass("City filter display");
				else
					fail("City filter not display");
				
				if(driver.explicitWaitforClickable(customerLandingPage.datePickerFrom, 5, "date filter"))
					pass("Date filter display");
				else
					fail("Date filter not display");
								
			}
			else
				fail("Address filter not display");
			
			
		*/}
	public void addressFilterSearchOption()
	{/*
		if(driver.explicitWaitforVisibility(customerLandingPage.addressFilter, 10, "Address"))
		{	

			if(driver.clickElement(customerLandingPage.addressFilter,  "Address filter"))
			{				
				pass("Address filter clicked");
			}
			else
				fail("City filter not display");
			
			if(driver.explicitWaitforClickable(customerLandingPage.addressSearch, 10, "address filter"))
			{
				pass("Address search filter display");
				if(driver.enterText(customerLandingPage.addressSearch, "Address", "Address"))
					pass("Address Searching  ");
			}
			else
				fail("Address search filter not display");
							
		}
		else
			fail("Address filter not display");
		
		
	*/}
	public void cityFilterSearchOption()
	{/*
		if(driver.explicitWaitforVisibility(customerLandingPage.cityFilter, 10, "City"))
		{	

			if(driver.clickElement(customerLandingPage.cityFilter,  "City filter"))
			{				
				pass("City filter clicked");
			}
			else
				fail("City filter not display");
			
			if(driver.explicitWaitforClickable(customerLandingPage.citySearch, 10, "City filter"))
			{
				pass("City search filter display");
				if(driver.enterText(customerLandingPage.citySearch, "City", "City"))
					pass("City Searching  ");
			}
			else
				fail("City search filter not display");
							
		}
		else
			fail("City filter not display");
		
		
	*/}
	public void dateFilterOption()
	{/*
		if(driver.explicitWaitforVisibility(customerLandingPage.datePickerFrom, 10, "Date filter"))
		{	

			if(driver.clickElement(customerLandingPage.dateFilter,  "Date filter"))
			{				
				pass("Date filter clicked");
			}
			else
				fail("Date filter not display");
			
		//need change date picker choose
			if(driver.explicitWaitforClickable(customerLandingPage.datePickerFrom, 10, "City filter"))
			{
				pass("Date search filter display");
				if(driver.enterText(customerLandingPage.citySearch, "Date", "Date"))
					pass("Date Searching  ");
			}
			else
				fail("Date search filter not display");
			
			if(driver.explicitWaitforClickable(customerLandingPage.datePickerFrom, 10, "City filter"))
			{
				pass("Date search filter display");
				if(driver.enterText(customerLandingPage.citySearch, "Date", "Date"))
					pass("Date Searching  ");
			}
			else
				fail("Date search filter not display");
							
		}
		else
			fail("Date filter not display");
		
		
	*/}
	public void navigatePancake()
	{
		if(driver.explicitWaitforClickable(accountPage.tab, 35, "Pancake"))
		{
			if(driver.jsClickElement(accountPage.tab, "Pancake menu")) {
				pass("Pancake clicked");}
			
		}
		else {
			fail("Pancake not display");}
		
	}
	public void navigateAccount()
	{
		if(driver.explicitWaitforClickable(accountPage.accounttab, 35, "Pancake"))
		{
			if(driver.jsClickElement(accountPage.accounttab, "Pancake menu")) {
				pass("Pancake clicked");}
			
		}
		else {
			fail("Pancake not display");}
		
	}
	public void verifyAccountTable()
	{
		if(driver.explicitWaitforClickable(accountPage.accountName, 35, "Pancake"))
		{
			pass("Account Name Table display");
			if(driver.explicitWaitforVisibility(accountPage.accountId, 35, "Account ID")) {
				pass("Account Id Display");}
			else {
				fail("Account Id Not display");}
			if(driver.explicitWaitforVisibility(accountPage.accountType, 35, "account Type")) {
				pass("account Type Display");}
			else {
				fail("account Type Not display");}
			if(driver.explicitWaitforVisibility(accountPage.accountRep, 35, "account Rep")) {
				pass("account Rep Display");}
			else {
				fail("account Rep Not display");}
			if(driver.explicitWaitforVisibility(accountPage.buyerAdmin, 35, "buyer Admin")) {
				pass("buyer Admin Display");}
			else {
				fail("buyer Admin Not display");}
			if(driver.explicitWaitforVisibility(accountPage.paymentTerms, 35, "payment Terms")) {
				pass("payment Terms Display");}
			else {
				fail("payment Terms Not display");}
			if(driver.explicitWaitforVisibility(accountPage.accountStatus, 35, "account Status")) {
				pass("account Status Display");}
			else {
				fail("account Status Not display");}
			if(driver.explicitWaitforVisibility(accountPage.accountCredit, 35, "account Credit")) {
				pass("account Credit Display");}
			else {
				fail("account Credit Not display");}
		

					
		}
		
	}

/** verifying customer table **/

	public void customerDataTable() {
		if(driver.explicitWaitforVisibility(customerLandingPage.customerDataInTable, 35, "Customer Data able")) 
		{
			logPass("Customer table displayed");
			//String noCustomers = driver.getText(customerLandingPage.customerTableNoData, "No Customers Found");
			int size;
			size = driver.getSize(customerLandingPage.customerDataInTable, "No of Customers in Data Table");
			if(size==0) {
				driver.explicitWaitforVisibility(customerLandingPage.customerDataInTable, 35, "No Customers Found");

			}
			else
			{
				driver.explicitWaitforVisibility(customerLandingPage.customerDataInTable, 35, "Customer Data is present");
				driver.isElementDisplayed(customerLandingPage.threeDots, 35, "Three Dots is displayed");
				logPass("Three Dots element is Present in data table");
			}
		}
		else
			logFail("customer table not displayed");

	}


	/** Verifying User Profile Page **/
	public void userProfilePage() {
		int values = 0;	
		String userProfileCustomerIdvalue;
		String customerIDvalue;
		values = driver.getSize(customerLandingPage.customerDataInTable, "datatable size");												 
		for(int i=0 ;i<=values ;i++)
		{
			customerIDvalue = driver.getText(By.xpath("(//*[@data-qa='customerIdatatable'])["+(i+1)+"]"), "Customer size");	 
			as.add("customerIDvalue");	
		}
		driver.jsClickElement(customerLandingPage.threeDots, "customerId");
		userProfileCustomerIdvalue = driver.getText(UserProfilePage.customerId,"CustomerId value");	
		if(as.contains("userProfileCustomerIdvalue"))																	 
		{																												 
			logPass("Clicked User Profile Page gets displayed");																 
		}																												 
		else																											 
		{																												 
			logFail("User Profile doesn't exisits");																 
		}																												 

	}

	/** verifying  all fields in customer profile page**/
	public void verifyUserProfilePage() {
		String cusIdAttribute = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(cusIdAttribute.contentEquals("true"))
		{
			logPass("Customer ID readonly value is displayed" +cusIdAttribute);
		}
		else
		{
			logFail("Customer ID is not readonly value displayed");
		}

	}

	public void firstNameField() {
		String firstName = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(firstName.contentEquals("true"))
		{
			logPass("First Name value is readonly" +firstName);
		}
		else
		{
			logFail("First Name value is not readonly");
		}

	}

	public void lastNameField() {
		String lastName = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(lastName.contentEquals("true"))
		{
			logPass("Last Name value is readonly" +lastName);
		}
		else
		{
			logFail("Last Name value is not readonly");
		}
	}

	public void createdDate() {
		String createDate = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(createDate.contentEquals("true"))
		{
			logPass("Created Date value is readonly" +createDate);
		}
		else
		{
			logFail("Create Date value is not readonly");
		}
	}

	public void birthDate() {
		String dateOfBirth = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(dateOfBirth.contentEquals("true"))
		{
			logPass("Date of birth value is readonly" +dateOfBirth);
		}
		else
		{
			logFail("Date of birth value is not readonly");
		}
	}

	public void genderField() {
		String genderDetails = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(genderDetails.contentEquals("true"))
		{
			logPass("Gender is readonly field" +genderDetails);
		}
		else
		{
			logFail("Gender is not readonly");
		}
	}

	public void emailField() {
		String emailId = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(emailId.contentEquals("true"))
		{
			logPass("Email Id is readonly" +emailId);
		}
		else
		{
			logFail("Email Id is not readonly");
		}
	}

	public void phoneNoField() {
		String phnumber = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(phnumber.contentEquals("true"))
		{
			logPass("Phone Number value is readonly" +phnumber);
		}
		else
		{
			logFail("Phone Number value is not readonly");
		}
	}

	public void statusOptions() {
		String statusValue = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(statusValue.contentEquals("true"))
		{
			logPass("Status is readonly" +statusValue);
		}
		else
		{
			logFail("Status is not readonly");
		}
	}

	/** Verifying Edit button **/

	public void editProfileButton() {
		if(driver.isElementDisplayed(UserProfilePage.editProfileTrigger, 35, "EditProfilebutton")) {
			logPass("Edit Profile button is displayed");
		}
		else {
			logFail("Edit Profile Not displayed");
		}
	}

	public void selectStatusList()
	{
//		if(driver.explicitWaitforClickable(customerLandingPage.statuslist, 10, "Status list"))
//		{
			int  size=driver.getSize(UserProfilePage.selectStatusList1, "Size ");
			int radom=driver.generateRandomNumberWithLimit(size, 2);
			
			if(driver.jsClickElement(UserProfilePage.selectStatusList2, "status type Input")) {
				pass("Fill status type clicked " );}
			else {
				fail(" status type  Field not Display");}
			//String name1=driver.getText(By.xpath("(//*[@label='Status']//ul//li)["+radom+"]//span"), "Select value");
			String name1=driver.getText(By.xpath(UserProfilePage.selectStatusList3.toString().replace("By.xpath: ", "").concat("[" +radom+ "]//span")),"select value");
			//if(driver.jsClickElement(By.xpath("(//*[@label='Status']//ul//li)["+radom+"]"), "Select "+name1))
			if(driver.jsClickElement(By.xpath(UserProfilePage.selectStatusList3.toString().replace("By.xpath: ", "").concat("[" +radom+ "]")),"Select "+name1)) {
				pass("status Type  Selected "+name1);}
			else {
				fail("status type  Field not Display");}
	
//		}
//		else
//			fail("Status not display");
		
	}
	
	
	public void selectStatusFilter()
		{
//			if(driver.explicitWaitforVisibility(customerLandingPage.statusFilter, 10, "Status Drop Down"))
//			{				
//				if(driver.explicitWaitforVisibility(customerLandingPage.statusSearch,10, "Status Filter Search"))
//				{
			int  size=driver.getSize(UserProfilePage.selectStatusFilter1, "Size ");
			int radom=driver.generateRandomNumberWithLimit(size, 2);
			
			if(driver.jsClickElement(UserProfilePage.selectStatusFilter2, "status type Input")) {
				pass("Fill status type clicked " );}
			else {
				fail(" status type  Field not Display");}
			//String name1=driver.getText(By.xpath("(//*[@label='Status']//ul//li)["+radom+"]//span"), "Select value");
			String name1=driver.getText(By.xpath(UserProfilePage.selectStatusList3.toString().replace("By.xpath: ", "").concat("[" +radom+ "]//span")),"select value");
			//if(driver.jsClickElement(By.xpath("(//*[@label='Status']//ul//li)["+radom+"]"), "Select "+name1))
			if(driver.jsClickElement(By.xpath(UserProfilePage.selectStatusList3.toString().replace("By.xpath: ", "").concat("[" +radom+ "]")),"Select "+name1)) {
				pass("status Type  Selected "+name1);}
			else {
				fail("status type  Field not Display");}
			
			
			/*if(driver.selectByIndex(customerLandingPage.statusSearch, 0, "Status filter Search"))
						pass("Status filter selected  ");
					if(driver.selectByIndex(customerLandingPage.statusSearch, 1, "Status filter Search"))
						pass("Status filter selected  ");*/
					
					
//				}
//					
//				else
//					fail("Status filter drop down list Not display");
//			}
//			else
//				fail("Status filter drop down not display");
			
			
		}			
		
		
	}


	


















