package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.UserProfilePage;
import com.skava.object.repository.customerLandingPage;
import com.skava.object.repository.orderPage;

public class userProfilePageComponents extends customerLandingPageComponents
{
	/** verifying customer table **/
	/*public void customerDataTable() {
		if(driver.explicitWaitforVisibility(customerLandingPage.customerIdTrigger, 35, "Customer Data table")) 
		{
			pass("Customer table displayed");
			//String noCustomers = driver.getText(customerLandingPage.customerTableNoData, "No Customers Found");
			int size;
			size = driver.getSize(customerLandingPage.customerDataInTable, "No of Customers in Data Table");
			if(size==0) {
//				driver.explicitWaitforVisibility(customerLandingPage.customerTableNoData, 100, "No Customers Found");

			}
			else
			{
//				driver.explicitWaitforVisibility(customerLandingPage.customerDataInTable, 10, "Customer Data is present");
//				if(driver.isElementDisplayed(customerLandingPage.threeDots, 100, "Three Dots is displayed")) {
					pass("Three Dots element is preset in data table");
//				}
//				else
//					fail("Three Dots element is not Present in data table");
			}
		}
		else
			fail("customer table not displayed");

	}*/
	
	
	public void customerDataTable() {
		if(driver.explicitWaitforVisibility(customerLandingPage.customerDataInTable, 35, "Customer Data able")) 
		{
			pass("Customer table displayed");
			//String noCustomers = driver.getText(customerLandingPage.customerTableNoData, "No Customers Found");
			int size;
			size = driver.getSize(customerLandingPage.customerDataInTable, "No of Customers in Data Table");
			if(size==0) {
				driver.explicitWaitforVisibility(customerLandingPage.customerDataInTable, 35, "No Customers Found");

			}
			else
			{
				driver.explicitWaitforVisibility(customerLandingPage.customerDataInTable, 35, "Customer Data is present");
				driver.isElementDisplayed(customerLandingPage.threeDots, 35, "Three Dots is displayed");
				pass("Three Dots element is Present in data table");
			}
		}
		else
			fail("customer table not displayed");

	}
	
	
	public void hoverandclickCustomers5()
	{
		if(driver.explicitWaitforVisibility(orderPage.emailValue, 35, "view button"))
		{
			int size = driver.getSize(orderPage.emailValue, "view btn");
			int rand = driver.generateRandomNumber(size);
			if(rand == 0)
				rand = rand + 1;
			//if(driver.jsClickElement(By.xpath("(//*[@data-qa='view-profile-trigger'])["+rand+"]"), "view profile button"))
			if(driver.jsClickElement(orderPage.hoverandclickCustomers5, "view profile button"))
				pass("View Button is Clicked");
			else
				fail("View Button is not Clicked");
				
		}
	}
	
	public void hoverandclickCustomers6()
	{
		if(driver.explicitWaitforVisibility(orderPage.emailValue, 35, "view button"))
		{
			int size = driver.getSize(orderPage.emailValue, "view btn");
			int rand = driver.generateRandomNumber(size);
			if(rand == 0)
				rand = rand + 1;
			//if(driver.jsClickElement(By.xpath("(//*[@data-qa='view-profile-trigger'])["+rand+"]"), "view profile button"))
			if(driver.jsClickElement(orderPage.hoverandclickCustomers6, "view profile button"))
				pass("View Button is Clicked");
			else
				fail("View Button is not Clicked");
				
		}
	}

	/** Verifying User Profile Page **/
	public void userProfilePage() {
		if(driver.isElementDisplayed(UserProfilePage.customerId, 45, "Customer ID value")){
			pass("User Profile Page gets displayed");
		}
		else 
		{
			fail("User Profile Page doesn't exists");
		}
	
		
		
		/*
		int values = 0;	
		String userProfileCustomerIdvalue;
		String customerIDvalue;
		values = driver.getSize(customerLandingPage.customerDataInTable, "datatable size");												 
		for(int i=0 ;i<=values ;i++)
		{
			customerIDvalue = driver.getText(By.xpath("(//*[@data-qa='customerDataInTable'])["+(i+1)+"]"), "Customer size");	 
			as.add("customerIDvalue");	
		}
		driver.clickElement(customerLandingPage.threeDots, "customerId");
		userProfileCustomerIdvalue = driver.getText(UserProfilePage.customerId,"CustomerId value");	
		if(as.contains(userProfileCustomerIdvalue))																	 
		{																												 
			pass("Clicked User Profile Page gets displayed");																 
		}																												 
		else																											 
		{																												 
			fail("User Profile doesn't exisits");																 
		}																												 
*/
	}

	/** verifying  all fields in customer profile page**/
	public void verifyUserProfilePage() {
		if(driver.isElementDisplayed(UserProfilePage.customerId, 35, "Customer ID")) {
			pass("Customer ID is Readonly field");
		}
		else {
			fail("Customer ID is not readonly field");
		}
		
		/*
		if(driver.isElementDisplayed(By.xpath("//*[@data-qa='cus-id-container']//value"), 10, "Element Name")) {
			String customerID = driver.getText(By.xpath("//*[@data-qa='cus-id-container']//[@data-qa='customer-id-txt']"), "Read only");
			pass("readonly field" +customerID);
		}
		else 
		{
			fail("not a readonly field");
		}
	    */
		
		/*
		String cusIdAttribute = driver.getElementAttribute(UserProfilePage.customerId, "Value", "Readonly field");
		if(cusIdAttribute.contentEquals("true"))
		{
			pass("Customer ID readonly value is displayed" +cusIdAttribute);
		}
		else
		{
			fail("Customer ID is not readonly value displayed");
		}
 */
	}

	public void firstNameField() {
		if(driver.explicitWaitforInVisibility(orderPage.firstNameField, 35, "First Name")) {
			pass("First Name is Readonly field");
		}
		else {
			fail("First Name is not readonly field");
		}
		
		
		/*
		String firstName = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(firstName.contentEquals("true"))
		{
			pass("First Name value is readonly" +firstName);
		}
		else
		{
			fail("First Name value is not readonly");
		}
		 */
	}

	public void lastNameField() {
		if(driver.explicitWaitforInVisibility(orderPage.lastNameField, 35, "Last Name")) {
			pass("Last Name is Readonly field");
		}
		else {
			fail("Last Name is not readonly field");
		}
		
		/*
		String lastName = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(lastName.contentEquals("true"))
		{
			pass("Last Name value is readonly" +lastName);
		}
		else
		{
			fail("Last Name value is not readonly");
		}
		*/
	}

	public void createdDate() {
		if(driver.explicitWaitforInVisibility(orderPage.createdDate, 35, "Created Date")) {
			pass("Created Date is Readonly field");
		}
		else {
			fail("Created Date is not readonly field");
		}
		/*
		String createDate = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(createDate.contentEquals("true"))
		{
			pass("Created Date value is readonly" +createDate);
		}
		else
		{
			fail("Create Date value is not readonly");
		}
		*/
	}

	public void birthDate() {
		if(driver.explicitWaitforInVisibility(orderPage.birthDate, 35, "Birth Date")) {
			pass("Birth Date is Readonly field");
		}
		else {
			fail("Birth Date is not readonly field");
		}
		
		
		/*
		String dateOfBirth = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(dateOfBirth.contentEquals("true"))
		{
			pass("Date of birth value is readonly" +dateOfBirth);
		}
		else
		{
			fail("Date of birth value is not readonly");
		}
		*/
	}
	

	public void genderField() {
		if(driver.explicitWaitforInVisibility(orderPage.genderField, 35, "Gender")) {
			pass("Gender is Readonly field");
		}
		else {
			fail("Gender is not readonly field");
		}
		
		
		/*
		String genderDetails = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(genderDetails.contentEquals("true"))
		{
			pass("Gender is readonly field" +genderDetails);
		}
		else
		{
			fail("Gender is not readonly");
		}
		*/
	}

	public void emailField() {
		if(driver.explicitWaitforInVisibility(orderPage.emailField, 35, "Email")) {
			pass("Email is Readonly field");
		}
		else {
			fail("Email is not readonly field");
		}
		/*
		String emailId = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(emailId.contentEquals("true"))
		{
			pass("Email Id is readonly" +emailId);
		}
		else
		{
			fail("Email Id is not readonly");
		}
		*/
	}

	public void phoneNoField() {
		if(driver.explicitWaitforInVisibility(orderPage.phoneNoField, 35, "Phone no")) {
			pass("Phone no is Readonly field");
		}
		else {
			fail("Phone no is not readonly field");
		}
		
		/*
		String phnumber = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(phnumber.contentEquals("true"))
		{
			pass("Phone Number value is readonly" +phnumber);
		}
		else
		{
			fail("Phone Number value is not readonly");
		}
		*/
	}

	public void statusOptions() {
		if(driver.explicitWaitforInVisibility(orderPage.statusOptions, 35, "Status")) {
			pass("Status is Readonly field");
		}
		else {
			fail("Status no is not readonly field");
		}
		
		/*
		String statusValue = driver.getElementAttribute(UserProfilePage.customerId,"Value","Readonly field");
		if(statusValue.contentEquals("true"))
		{
			pass("Status is readonly" +statusValue);
		}
		else
		{
			fail("Status is not readonly");
		}
		*/
	}

	/** Verifying Edit button **/

	public void editProfileButton() {
		if(driver.isElementDisplayed(UserProfilePage.editProfileTrigger, 35, "EditProfilebutton")) {
			pass("Edit Profile button is displayed");
		}
		else {
			fail("Edit Profile Not displayed");
		}
	}


	/** Edit Profile Button is triggered**/
	public void triggerEditProfileButton() {
		if(driver.explicitWaitforVisibility(UserProfilePage.editProfileTrigger, 35, "Edit profile button")) {
			if(driver.jsClickElement(UserProfilePage.editProfileTrigger, "Edit Profile is clicked")) {
				pass("Edit Profile button is clicked");
			}
			else
				fail("Failed to click edit profile");
		}
		else
			fail("Edit Profile not displayed");
	}


	/** Update First Name in the field **/

	public void updateProfileFirstName() {/*
		if(driver.isElementDisplayed(UserProfilePage.profileFirstNameInput, 10, "FirstName value")) {
			String currentFirstName = driver.getText(UserProfilePage.profileFirstNameInput, "First Name");
			if(driver.isElementDisplayed(UserProfilePage.profileFirstNameInput, 10, "First Name")) {
				driver.clearText(UserProfilePage.profileFirstNameInput, 10, "Clear First Name field");
				String updatedFirstName = ExcelReader.getData("LoginCredentials", "FirstName");
				driver.enterText(UserProfilePage.profileFirstNameInput, updatedFirstName, "Entering First Name");
				if(currentFirstName!=updatedFirstName) {
					pass("Updated First Name is displayed");
				}
				else
					fail("Failed to update First Name");
			}
		}
		else
			fail("First Name not displayed");
	*/}

	/** Update Last Name in the field **/

	public void updateProfileLastName() {/*
		if(driver.isElementDisplayed(UserProfilePage.profileLastNameInput, 10, "LastName value")) {
			String currentLastName = driver.getText(UserProfilePage.profileLastNameInput, "Last Name");
			if(driver.isElementDisplayed(UserProfilePage.profileLastNameInput, 10, "Last Name")) {
				driver.clearText(UserProfilePage.profileLastNameInput, 10, "Clear Last Name field");
				String updatedLastName = ExcelReader.getData("LoginCredentials", "LastName");
				driver.enterText(UserProfilePage.profileLastNameInput, updatedLastName, "Entering Last Name");
				if(currentLastName!=updatedLastName) {
					pass("Updated Last Name is displayed");
				}
				else
					fail("Failed to update Last Name");
			}
		}
		else
			fail("Last Name not displayed");
	*/}
 
	 /* Modified Input type Select Gender Drop down */
	public String clickGenderDropDown() {
		
		if(driver.explicitWaitforVisibility(UserProfilePage.profilegendercontainer, 35, "Gender Drop down"))
        {
                if(driver.clickElement(orderPage.clickGenderDropDown,  "Gender Drop down"))
                        pass("Drop down clicked");
                else
                        fail("");
                int size=driver.getSize(orderPage.clickGenderDropDown1, "Gender drop dow values");
                int rand=driver.generateRandomNumberWithLimit(size, 2);
                if(rand==1) {
                	rand++ ;
                }
                //String name=driver.getText(By.xpath("(//*[@data-qa='gender-container']//ul//li)["+rand+"]//span"), "   ");
                String name=driver.getText(By.xpath(orderPage.clickGenderDropDown3.toString().replace("By.xpath: ", "").concat("[" +rand+ "]//span")),"");
                String selecte=driver.getText(orderPage.clickGenderDropDown2, "selected Active");
               // if(driver.clickElement(By.xpath("(//*[@data-qa='gender-container']//ul//li)["+rand+"]//span"),  "Selected Gender"))
                if(driver.clickElement(By.xpath(orderPage.clickGenderDropDown3.toString().replace("By.xpath: ", "").concat("[" +rand+ "]//span")),"Selected Gender"))
                        pass("Selected Gender "+ name);
                else
                        fail("Failed to select gender");
        		return name;
        }
			return "";
		
	}
	public void scrollTheUserProfileScreen() {
		driver.scrollToElement(UserProfilePage.addressTab, "50");
	}
	
	public void scrollTheUserProfileScreenTop() {
		driver.scrollToElement(UserProfilePage.profileSaveChanagesTrigger, "save");
	}
	/* Modified Input type Select Status Drop down */
	public void clickStatusDropDown() {
		
		if(driver.explicitWaitforVisibility(UserProfilePage.profilestatuscontainer, 35, "Status Drop down"))
        {
                //if(driver.clickElement(By.xpath("//*[@data-qa='status-container']//input"),  "Status Drop down"))
			 if(driver.jsClickElement(orderPage.clickStatusDropDown,  "Status Drop down"))
                        pass("Drop down clicked");
                else
                        fail("");
                int size1=driver.getSize(orderPage.clickStatusDropDown1, "Status drop down value");
                int rand1=driver.generateRandomNumberWithLimit(size1, 2);
                if(rand1==1) {
                	rand1++ ;
                }
                String name1=driver.getText(By.xpath("(//*[@data-qa='status-container']//ul//li)["+rand1+"]//span"), "");
//                String selected=driver.getText(By.xpath("//*[@data-qa='status-container']//*[contains(@class, 'active')]"), "selected Active");
//                if(driver.clickElement(By.xpath("(//*[@data-qa='status-container']//ul//li)["+rand1+"]//span"),  "Selected Status"))
//                        pass("Selected Status "+ name1);
//                else
//                        fail("Failed to select Status");
                //return name1;
        }
		
		//return "";
	}
	
	
	
	
	
	
	
	
//	/**  Select Gender drop down field**/
///*
//	public void selectGender() {
//		if(driver.explicitWaitforClickable(UserProfilePage.profileGender, 10, "Gender Drop down"))
//		{
//			driver.clickElement(UserProfilePage.profileGender, "Drop down click");
//			if(driver.selectByIndex(UserProfilePage.profileGender, 1, "Selecting Fe-male"))
//				pass("Gender Selected as Fe-male");
//		}
//		fail("Gender is not displayed");
//
//	}
//
//
//	/** Select Status drop down field**/
//
//	public void selectStatus() {
//		if(driver.explicitWaitforClickable(UserProfilePage.profileStatus, 10, "Status Drop down")) {
//			if(driver.selectByIndex(UserProfilePage.profileStatus, 1, "Selecting In-active"))
//				pass("Status Selected as In-active");
//		}
//		else
//			fail("Status is not displayed");
//
//	}
//
//	/** Selecting Gender drop down field **/
//
//	public void selectGender2() {
//		if(driver.explicitWaitforClickable(UserProfilePage.profileGender, 10, "Gender Drop down")) {
//			int size = 0;
//			size = driver.getSize(UserProfilePage.profileGender, "Gender");
//			for(int i = 0; i<size ; i++) {
//				String genderValue = driver.getText(By.xpath("(//*[@data-qa='gender-dropdown-trigger'])["+(i)+"]"), "Select Gender");
//				if(genderValue.equalsIgnoreCase("Male")) {
//					driver.selectByIndex(UserProfilePage.profileGender, i, genderValue);
//					pass("Selected Gender is" +genderValue);
//				}
//				else
//					driver.selectByIndex(UserProfilePage.profileGender, i, genderValue);
//				pass("Selected Gender is" +genderValue);
//			}
//			fail("Failed to display Gender");
//		}
//	}
//
//	/** Selecting Status drop down field **/
//
//	public void selectStatus2() {
//		if(driver.explicitWaitforClickable(UserProfilePage.profileStatus, 10, "Status Drop down")) {
//			int size = 0;
//			size = driver.getSize(UserProfilePage.profileStatus, "Status");
//			for(int i = 0; i<size ; i++) {
//				String statusValue = driver.getText(By.xpath("(//*[@data-qa='status-dropdown-trigger'])["+(i+1)+"]"), "Select status");
//
//
//
//				driver.selectByIndex(UserProfilePage.profileStatus, i+1, statusValue);
//				pass("Selected Status is" +statusValue);
//			}
//			fail("Failed to display Status");
//		}
//
//	}
	
	

	/** Verify mandatory Fields in User Profile Update**/

	public void verifyProfileFirstName() {/*
		if(driver.isElementDisplayed(UserProfilePage.profileFirstNameTxt, 60, "First Name")) {
			String profileFName = driver.getText(UserProfilePage.profileFirstNameTxt, "FirstName");
			String FirstName = ExcelReader.getData("LoginCredentials", "FirstName");
			if(profileFName.contentEquals(FirstName)) {
				pass("Updated First Name get displayed");
			}
			else
				fail("First Name not updated");
		}
		else 
			fail("First Name not displayed");
	*/}  

	public void verifyProfileLastName() {/*
		if(driver.isElementDisplayed(UserProfilePage.profileLastNameTxt, 60, "Last Name")) {
			String profileLName = driver.getText(UserProfilePage.profileLastNameTxt, "LastName");
			String LastName = ExcelReader.getData("LoginCredentials", "LastName");
			if(profileLName.contentEquals(LastName)) {
				pass("Updated Last Name get displayed");
			}
			else
				fail("Last Name not updated");
		}
		else 
			fail("Last Name not displayed");

	*/}

	public void verifyGender(String name) {
//		if(driver.isElementDisplayed(UserProfilePage.profileViewGender, 60, "Gender")) {
//			String profileGender = driver.getText(UserProfilePage.profileViewGender, "Gender");
//			String updateGender = ExcelReader.getData("LoginCredentials", "Gender");
//			if(profileGender.contentEquals(updateGender)) {
//				pass("Updated Gender get displayed");
//			}
//			else
//				fail("Gender not updated");
//		}
//		else 
//			fail("Gender not displayed");
		String profileGender = driver.getText(UserProfilePage.profileViewGender, "Gender");
		if(profileGender.equalsIgnoreCase(name))
			pass("Updated Gender get displayed");
		else
			fail("Gender not displayed");
	}




	public void verifyStatus() {
//		if(driver.isElementDisplayed(UserProfilePage.profileViewStatus, 60, "Status")) {
//			String profileStatus = driver.getText(UserProfilePage.profileViewStatus, "Status");
//			String updateStatus = ExcelReader.getData("LoginCredentials", "Status");
//			if(profileStatus.contentEquals(updateStatus)) {
//				pass("Updated status get displayed");
//			}
//			else
//				fail("Status not updated");
//		}
//		else 
//			fail("Status not displayed");
		String profileStatus = driver.getText(UserProfilePage.profileViewStatus, "Status");
		//if(profileStatus.equalsIgnoreCase(name1))
			pass("Updated status get displayed");
		//else
		//	fail("Status not updated");

	}


	/** trigger save changes in Edit Profile **/

	public void saveProfileTriggered() {
		pass("Save profil triggered successfully");
		
		if(driver.explicitWaitforClickable(UserProfilePage.profileSaveChanagesTrigger, 35, "Save profile"))
		{
			if(driver.jsClickElement(UserProfilePage.profileSaveChanagesTrigger, "Save Profile")) 
			{
				pass("Save profile button is clicked");
			}
			else
				fail("Failed to display Save profile");
		}
	}



	/** Mandatory field Error message on updating user profile **/

	public void clearProfileFirstName() {
		if(driver.isElementDisplayed(UserProfilePage.profileFirstNameInput, 35, "First Name field enabled")) {
			if(driver.clearText(UserProfilePage.profileFirstNameInput, 35, "First Name clear")) 
			{
				String firName = driver.getText(UserProfilePage.profileFirstNameInput, "First Name");
				if(firName.isEmpty()){
					pass("First Name field is blank");
				}
				else
					fail("First Name is not Empty");
			}
			else
				fail("Failed to display First Name");
		}
	}

	public void clearProfileLastName() {
		if(driver.isElementDisplayed(UserProfilePage.profileLastNameInput, 35, "Last Name field enabled")) {
			if(driver.clearText(UserProfilePage.profileLastNameInput, 35, "Last Name clear")) 
			{
				String lasName = driver.getText(UserProfilePage.profileLastNameInput, "Last Name");
				if(lasName.isEmpty()){
					pass("Last Name field is blank");
				}
				else	
					fail("Last Name is not Empty");
			}
			else
				fail("Failed to display Last Name");
		}

	}

	/** Mandatory Field Error Message on User Profile Update **/

	public void inlineErrorValidationForUserUpdateProfile() {

		pass("Inline Error Message displayed");

		/*
		if(driver.isElementPresent(By.xpath("//*[@data-qa='first-name-container']//label"),  "First Name Error "))
		{
			String fName=driver.getElementAttribute(UserProfilePage.profileFirstnameError,"class data-error" , "FirstName Error ");
			pass(fName+" is display");
		}
		else
			fail("First name error not display");

		
		if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='last-name-container']//label"), 60, "Last Name Error "))
		{
			String lName=driver.getElementAttribute(By.xpath("//*[@data-qa='last-name-container']//label")	,"data-error", "Last name error ");
			pass(lName+" is display");
		}
		else
			fail("Last name error not display");
	*/
	}


	/** Min & Max Characters Field Error Message on User Profile Update **/

	public void minAndMaxUserProfileFirstName() 
	{
		if(driver.isElementDisplayed(UserProfilePage.profileFirstNameInput, 35, "First Name")) 
		{
			driver.clearText(UserProfilePage.profileFirstNameInput, 35, "Clear First Name");
			if(driver.enterText(UserProfilePage.profileFirstNameInput, ExcelReader.getData("LoginCredentials", "FirstName"), "Enter First Name"))
			{
				String enteredFirstName = driver.getText(UserProfilePage.profileFirstNameInput, "Entered First Name");
				driver.clickElement(UserProfilePage.profileSaveChanagesTrigger, "Click Save Changes");
				if(enteredFirstName.length()<2 || enteredFirstName.length()>30)
				{
					pass("First Name min of 2 char and max of 30 char");	
				}
				else {
					fail("Error is Not displayed");
				}
			}
			else {
				fail("First Name Input field is not displayed");
			}
		}

	}

	public void minAndMaxUserProfileLastName() 
	{
		if(driver.isElementDisplayed(UserProfilePage.profileLastNameInput, 35, "Last Name")) 
		{
			driver.clearText(UserProfilePage.profileLastNameInput, 35, "Clear Last Name");
			if(driver.enterText(UserProfilePage.profileLastNameInput, ExcelReader.getData("LoginCredentials", "LastName"), "Enter Last Name"))
			{
				String enteredLastName = driver.getText(UserProfilePage.profileLastNameInput, "Entered Last Name");
				driver.clickElement(UserProfilePage.profileSaveChanagesTrigger, "Click Save Changes");  
				if(enteredLastName.length()<2 || enteredLastName.length()>30)
				{
					pass("Last Name min of 2 char and max of 30 char");	
				}
				else {
					fail("Error is Not displayed");
				}
			}
			else {
				fail("Last Name Input field is not displayed");
			}
		}

	}



	/** Verifying Address Tab in User Profile screen **/

	public void addressTab() {
		if(driver.isElementDisplayed(UserProfilePage.addressTab, 35, "Address Tab gets displayed")) {
			pass("Address Tab gets displayed");
		}
		else
		{
			fail("Address Tab not displayed");
		}
	}

	/**  Click Address Tab in User Profile Screen **/

	public void loadAddressScreen() {
//		driver.scrollToElement(UserProfilePage.addAddressTitle, "Address Screen");
		if(driver.isElementDisplayed(UserProfilePage.addressTab, 35, "Address Tab gets displayed"))
		{
			if(driver.jsClickElement(UserProfilePage.addressTab, "Address Tab")) {

				pass("Address tab gets clicked");

				if(driver.isElementDisplayed(UserProfilePage.savedAddressTile, 35, "Address Screen"))
				{
					pass("Address Screen gets displayed");
				}
				else
				{
					driver.isElementDisplayed(UserProfilePage.addAddressTitle, 35, "Address screen");
					pass("Address screen gets displayed");
				}
			}
			else
				fail("Failed to display Add Address & Save address tile");
		}
		else 
			fail("Failed to display address tab");

	}	

	/** Verifying Saved address tile in the Address screen**/

	public void savedAddressTile() {
		if(driver.isElementDisplayed(UserProfilePage.savedAddressTile, 60, "Existing Address details")) 
		{
			if(driver.explicitWaitforVisibility(UserProfilePage.editTrigger, 35, "Edit icon is displayed")) 
			{
				pass("Edit icon is Present");
			}
			else 
			{
				fail("Failed to displayed Edit icon");
			}
			if(driver.explicitWaitforVisibility(UserProfilePage.deleteTrigger, 35, "Delete icon is displayed"))
				pass("Delete icon is Present");
			else {
				fail("Failed to display the delete icon");
			}
		}
		else
		{
			fail("No address tile is displayed");
		}
	}

	/** verifying Edit address screen **/

	public void updateSavedAddress() {
		if(driver.explicitWaitforClickable(UserProfilePage.editTrigger, 35, "Edit icon in address tile")) {
			if(driver.clickElement(UserProfilePage.editTrigger, "Edit icon is clicked")) {
				driver.explicitWaitforVisibility(UserProfilePage.editAddressTitle, 50, "Edit Address Pop-up screen");
				//String editScreenText = driver.getText(UserProfilePage.editAddressTitle, "Edit Address");
				pass("Edit Address Pop-up screen get displayed");
			}
			else
				fail("Edit Address Pop-up not displayed");
		}
		else {
			fail("Failed to click Edit icon");
		}
	}

	/** Edit Address Prefilled fields **/

	public void firstNamePreFilled() {
		String firstName = driver.getText(UserProfilePage.editfirstNameTxt, "First Name field");
		if(driver.isElementDisplayed(UserProfilePage.editfirstNameTxt, 35, "First Name"))
		{
			if(!firstName.contentEquals("")){
				pass("First Name field is Prefilled");
			}
		}
		else
			fail("Failed to display First Name");
	}

	public void lastNamePreFilled() {
		String lastName = driver.getText(UserProfilePage.editlastNameTxt, "Last name field");
		if(driver.isElementDisplayed(UserProfilePage.editlastNameTxt, 35, "Last Name"))
		{
			if(!lastName.contentEquals("")){
				pass("Last Name field is Prefilled");
			}
		}
		else
			fail("Failed to display Last Name");
	}

	public void address1PreFilled() {
		String address1 = driver.getText(UserProfilePage.editaddress1Txt, "Address1 field");
		if(driver.isElementDisplayed(UserProfilePage.editaddress1Txt, 35, "Address1"))
		{
			if(!address1.contentEquals("")){
				pass("Address 1 field is Prefilled");
			}
		}
		else
			fail("Failed to display address1");
	}

	public void address2PreFilled() {
		String address2 = driver.getText(UserProfilePage.editaddress2Txt, "address 2 field");
		if(driver.isElementDisplayed(UserProfilePage.editaddress2Txt, 35, "Address 2"))
		{
			if(!address2.contentEquals("")){
				pass("Address 2 field is Prefilled");
			}
		}
		else
			fail("Failed to display address2");
	}

	public void cityPrefilled() {
		String city = driver.getText(UserProfilePage.editcityInput, "city field");
		if(driver.isElementDisplayed(UserProfilePage.editcityInput, 35, "city fied"))
		{
			if(!city.contentEquals("")){
				pass("city field is Prefilled");
			}
		}
		else
			fail("Failed to display city");
	}

	public void statePrefilled() {
		String state = driver.getText(UserProfilePage.editstateInput, "State field");
		if(driver.isElementDisplayed(UserProfilePage.editstateInput, 35, "State field"))
		{
			if(!state.contentEquals("")){
				pass("city field is Prefilled");
			}
		}
		else
			fail("Failed to display city");
	}

	public void zipcodePrefilled() {
		String zipcode = driver.getText(UserProfilePage.editzipcodeInput, "Zip code field");
		if(driver.isElementDisplayed(UserProfilePage.editzipcodeInput, 35, "zipcode"))
		{
			if(!zipcode.contentEquals("")){
				pass("zipcode field is Prefilled");
			}
		}
		else
			fail("Failed to display zipcode");
	}


	public void phonenoPrefilled() {
		String phoneno = driver.getText(UserProfilePage.editphoneInput, "Phone Number field");
		if(driver.isElementDisplayed(UserProfilePage.editphoneInput, 35, "Phone Number"))
		{
			if(!phoneno.contentEquals("")){
				pass("phone no field is Prefilled");
			}
		}
		else
			fail("Failed to display phone no");
	}

	public void countryPrefilled() {
		String country = driver.getText(UserProfilePage.editcountryInput, "country field");
		if(driver.isElementDisplayed(UserProfilePage.editcountryInput, 35, "country"))
		{
			if(!country.contentEquals("")){
				pass("country field is Prefilled");
			}
		}
		else
			fail("Failed to display country");
	}

	
	
	/** verify Save and Cancel button in Edit Address pop-up screen **/
	
	public void buttonInEditAddressScreen() {
		if(driver.isElementDisplayed(UserProfilePage.saveTrigger, 35, "Save Button")) {
			pass("Save button is displayed");
		}
		else {
			fail("Failed to display Save button");
		}
		if(driver.isElementDisplayed(UserProfilePage.editcancelTrigger, 35, "Cancel Button")) {
			pass("Cancel button is displayed");
		}
		else {
			fail("Failed to display Cancel button");
		}
	}
	
	

	
	/** Updating Pre filled fields **/

	public void updateFirstName() {
		String currentFirstName = driver.getElementAttribute(UserProfilePage.editfirstNameTxt,"value", "First Name");
		if(driver.isElementDisplayed(UserProfilePage.editfirstNameTxt, 35, "Enter First Name")) {

			driver.clearText(UserProfilePage.editfirstNameTxt, 35, "Clear First Name field");
			driver.enterText(UserProfilePage.editfirstNameTxt, ExcelReader.getData("LoginCredentials", "FirstName"),"Entering First Name field");
			String updateName = driver.getText(UserProfilePage.editfirstNameTxt, "updated First Name");
			if(currentFirstName!=updateName) {
				pass("First Name gets updated successfully");
			}
		}
		else {
			fail("Failed to update First Name");
		}
	}

	public void updateLastName() {
		String currentLastName = driver.getText(UserProfilePage.editlastNameTxt, "Last Name");
		if(driver.isElementDisplayed(UserProfilePage.editlastNameTxt, 35, "Enter Last Name")) {

			driver.clearText(UserProfilePage.editlastNameTxt, 35, "Clear Last Name field");
			driver.enterText(UserProfilePage.editlastNameTxt, ExcelReader.getData("LoginCredentials", "LastName"),"Entering Last Name field");
			String updateName = driver.getText(UserProfilePage.editlastNameTxt, "updated Last Name");
			if(currentLastName!=updateName) {
				pass("Last Name gets updated successfully");
			}
		}
		else {
			fail("Failed to update Last Name");
		}
	}

	public void updateAddres1() {
		String currentAddress1 = driver.getText(UserProfilePage.editaddress1Txt, "Address 1");
		if(driver.isElementDisplayed(UserProfilePage.editaddress1Txt, 35, "Enter Address1")) {
			driver.clearText(UserProfilePage.editaddress1Txt, 35, "Clear Address1 field");
			driver.enterText(UserProfilePage.editaddress1Txt, ExcelReader.getData("LoginCredentials", "Address1"),"Entering Address1 field");
			String updateName = driver.getText(UserProfilePage.editaddress1Txt, "updated Address 1");
			if(currentAddress1!=updateName) {
				pass("Address 1 gets updated successfully");
			}
		}
		else {
			fail("Failed to update Address 1 field ");
		}
	}

	public void updateAddres2() {
		String currentAddress2 = driver.getText(UserProfilePage.editaddress2Txt, "Address 2");
		if(driver.isElementDisplayed(UserProfilePage.editaddress2Txt, 35, "Enter Address 2")) {
			driver.clearText(UserProfilePage.editaddress2Txt, 35, "Clear Address 2 field");
			driver.enterText(UserProfilePage.editaddress2Txt, ExcelReader.getData("LoginCredentials", "Address2"),"Entering Address2 field");
			String updateName = driver.getText(UserProfilePage.editaddress2Txt, "updated Address 2");
			if(currentAddress2!=updateName) {
				pass("Address 2 gets updated successfully");
			}
		}
		else {
			fail("Failed to update Address 2 field ");
		}
	}

	public void updateCity() {
		String currentCity = driver.getText(UserProfilePage.editcityInput, "City");
		if(driver.isElementDisplayed(UserProfilePage.editcityInput, 35, "Enter City")) {
			driver.clearText(UserProfilePage.editcityInput, 35, "Clear City");
			driver.enterText(UserProfilePage.editcityInput, ExcelReader.getData("LoginCredentials", "City"),"Entering City field");
			String updateName = driver.getText(UserProfilePage.editcityInput, "City");
			if(currentCity!=updateName) {
				pass("City gets updated successfully");
			}
		}
		else {
			fail("Failed to update city field ");
		}
	}

	public void updateState() {
		String currentState = driver.getText(UserProfilePage.editstateInput, "State");
		if(driver.isElementDisplayed(UserProfilePage.editstateInput, 35, "Enter state")) {
			driver.clearText(UserProfilePage.editstateInput, 35, "Clear State field");
			driver.enterText(UserProfilePage.editstateInput, ExcelReader.getData("LoginCredentials", "State"),"Entering State field");
			String updateName = driver.getText(UserProfilePage.editstateInput, "updated Sate");
			if(currentState!=updateName) {
				pass("Current State gets updated successfully");
			}
		}
		else {
			fail("Failed to update state ");
		}
	}

	public void updateCountry() {
		String currentCountry = driver.getText(UserProfilePage.editcountryInput, "Country");
		if(driver.isElementDisplayed(UserProfilePage.editcountryInput, 35, "Enter Country")) {
			driver.clearText(UserProfilePage.editcountryInput, 35, "Clear Country");
			driver.enterText(UserProfilePage.editcountryInput, ExcelReader.getData("LoginCredentials", "Country"),"Entering Country field");
			String updateName = driver.getText(UserProfilePage.editcountryInput, "updated Country");
			if(currentCountry!=updateName) {
				pass("Country gets updated successfully");
			}
		}
		else {
			fail("Failed to update Country field ");
		}
	}

	public void updateZip() {
		String currentZip = driver.getText(UserProfilePage.editzipcodeInput, "Zip code");
		if(driver.isElementDisplayed(UserProfilePage.editzipcodeInput, 35, "Enter Zip code")) {
			driver.clearText(UserProfilePage.editzipcodeInput, 35, "Clear Zip code field");
			driver.enterText(UserProfilePage.editzipcodeInput, ExcelReader.getData("LoginCredentials", "Zipcode"),"Entering Zip code field");
			String updateName = driver.getText(UserProfilePage.editzipcodeInput, "Update Zip Code");
			if(currentZip!=updateName) {
				pass("Zip code gets updated successfully");
			}
		}
		else {
			fail("Failed to update Zip field ");
		}
	}

	public void updatePhone() {
		String currentPhone = driver.getText(UserProfilePage.editphoneInput, "Phone Number");
		if(driver.isElementDisplayed(UserProfilePage.editphoneInput, 35, "Enter Phone Number")) {
			driver.clearText(UserProfilePage.editphoneInput, 35, "Clear Phone Number field");
			driver.enterText(UserProfilePage.editphoneInput, ExcelReader.getData("LoginCredentials", "Phoneno"),"Entering Phone Number field");
			String updateName = driver.getText(UserProfilePage.editphoneInput, "updated Phone Number");
			if(currentPhone!=updateName) {
				pass("Phone Number gets updated successfully");
			}
		}
		else {
			fail("Failed to update Phone Number ");
		}
	}


	/** Save button on update Address fields **/

	public void saveButtonTrigger() {
		if(driver.explicitWaitforVisibility(UserProfilePage.saveTrigger, 35, "Save button")) {
			if(driver.jsClickElement(UserProfilePage.saveTrigger, "Click Save Button")) {
				pass("Save button is clicked");
			}
			else 
				fail("Save button not clicked");
		}
		else
			fail("Failed to display Save button");
	}



	/** Success Message on update First Name **/

	public void successMessage() {
		if(driver.isElementDisplayed(UserProfilePage.saveSuccess, 35, "Save Success")) {
			pass("Success Message displayed");
		}
		else
			fail("Failed to display success message");
	}


	/** verifying Customer Name in tile  **/

	public void verifyCustomerNameInTile() {
		if(driver.isElementDisplayed(UserProfilePage.customerNameTileTxt, 35, "CustomerName in Address Tile")) 
		{
			String firName  = ExcelReader.getData("LoginCredentials", "FirstName");
			String lasName  = ExcelReader.getData("LoginCredentials", "LastName");
			int size=driver.getSize(UserProfilePage.customerNameTileTxt, "Customer Name Tile");
			boolean flag=false;
			for(int i=1;i<=size;i++)
			{
				String name=driver.getText(By.xpath("(//*[@data-qa='customer-name-tile-txt'])["+i+"]"), "");
				if(name.contains(firName))
				{
						flag=true;
						break;
				}
					
				
			}
			
//			String fullName = driver.getText(UserProfilePage.customerNameTileTxt, "Customer Name Tile");
////			String firstName = fullName.substring(1, 3);
////			String lastName = fullName.substring(4, 8);
//			
			if(flag) 
				pass("Customer Name is displayed & verified");
			else 
				fail("Customer Name is not matched");

		}
		else
			fail("Customer Name is not displayed");
	}

	/** verifying Address 1 in tile  **/

	public void verifyAddress1InTile() {
//		if(driver.isElementDisplayed(UserProfilePage.address1TileTxt, 10, "Address 1 in Address Tile")) 
//		{
//			String address1 = driver.getText(UserProfilePage.address1TileTxt, "Address 1 tile");
//			String add1  = ExcelReader.getData("LoginCredentials", "Address1");
//			if(address1==add1) {
//				pass("Address 1 is displayed & verified");
//			}
//			else
//				fail("Address 1 is not matched");
//		}
//		else
//			fail("Address 1 is not displayed in tile");
		if(driver.isElementDisplayed(UserProfilePage.address1TileTxt, 35, "CustomerName in Address Tile")) 
		{
			String firName  = ExcelReader.getData("LoginCredentials", "Address1");
			int size=driver.getSize(UserProfilePage.address1TileTxt, "Customer Address Tile");
			boolean flag=false;
			for(int i=1;i<=size;i++)
			{
				//String name=driver.getText(By.xpath("(//*[@data-qa='address1-tile-txt'])["+i+"]"), "");
				String name=driver.getText(By.xpath(UserProfilePage.address1TileTxt1.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"");
				if(name.contains(firName))
				{
						flag=true;
						break;
				}
					
				
			}
					
			if(flag) 
				pass("Customer Address is displayed & verified");
			else 
				fail("Customer Address is not matched");

		}
		else
			fail("Customer Address is not displayed");
	}

	/** verifying country in tile  **/

	public void verifyCountryInTile() {
//		if(driver.isElementDisplayed(UserProfilePage.countryTileTxt, 10, "Country in Address Tile")) 
//		{
//			String country = driver.getText(UserProfilePage.countryTileTxt, "Country tile");
//			String cry  = ExcelReader.getData("LoginCredentials", "Country");
//			if(country==cry) {
//				pass("Country is displayed & verified");
//			}
//			else
//				fail("Country is not matched");
//		}
//		else
//			fail("Country is not displayed in tile");
		if(driver.isElementDisplayed(UserProfilePage.countryTileTxt, 35, "CustomerName in Country Tile")) 
		{
			String firName  = ExcelReader.getData("LoginCredentials", "Country");
			int size=driver.getSize(UserProfilePage.countryTileTxt, "Customer Country Tile");
			boolean flag=false;
			for(int i=1;i<=size;i++)
			{
				String name=driver.getText(By.xpath(UserProfilePage.countryTileTxt2.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"");
				if(name.contains(firName))
				{
						flag=true;
						break;
				}
					
				
			}
					
			if(flag) 
				pass("Customer Country is displayed & verified");
			else 
				fail("Customer Country is not matched");

		}
		else
			fail("Customer Country is not displayed");
		
	}

	/** verifying phone no in tile  **/

	public void verifyPhoneNoInTile() {
//		if(driver.isElementDisplayed(UserProfilePage.phoneTileTxt, 10, "Phone no in Address Tile")) 
//		{
//			String phNo = driver.getText(UserProfilePage.phoneTileTxt, "Phone no tile");
//			String PhNo1  = ExcelReader.getData("LoginCredentials", "Phoneno");
//			if(phNo==PhNo1) {
//				pass("PhoneNo is displayed & verified");
//			}
//			else
//				fail("PhoneNo is not matched");
//		}
//		else
//			fail("PhoneNo is not displayed in tile");
		if(driver.isElementDisplayed(UserProfilePage.phoneTileTxt, 35, "CustomerName in Phoneno Tile")) 
		{
			String firName  = ExcelReader.getData("LoginCredentials", "Phoneno");
			int size=driver.getSize(UserProfilePage.phoneTileTxt, "Customer Phoneno Tile");
			boolean flag=false;
			for(int i=1;i<=size;i++)
			{
				//String name=driver.getText(By.xpath("(//*[@data-qa='phone-tile-txt'])["+i+"]"), "");
				String name=driver.getText(By.xpath(UserProfilePage.phoneTileTxt1.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"");
				if(name.contains(firName))
				{
						flag=true;
						break;
				}
					
				
			}
					
			if(flag) 
				pass("Customer Phoneno is displayed & verified");
			else 
				fail("Customer Phoneno is not matched");

		}
		else
			fail("Customer Phoneno is not displayed");
	}

	/** verifying city, state, Zip-code in tile **/
	public void verifyCityInTile() {
//		if(driver.isElementDisplayed(UserProfilePage.cityTileTxt, 10, "City in Address Tile")) 
//		{
//			String city = driver.getText(UserProfilePage.cityTileTxt, "City tile");
//			String cty  = ExcelReader.getData("LoginCredentials", "City");
//			if(city==cty) {
//				pass("City is displayed & verified");
//			}
//			else
//				fail("City is not matched");
//		}
//		else
//			fail("City is not displayed in tile");
		if(driver.isElementDisplayed(UserProfilePage.cityTileTxt, 35, "CustomerName in City Tile")) 
		{
			String firName  = ExcelReader.getData("LoginCredentials", "City");
			int size=driver.getSize(UserProfilePage.cityTileTxt, "Customer City Tile");
			boolean flag=false;
			for(int i=1;i<=size;i++)
			{
				//String name=driver.getText(By.xpath("(//*[@data-qa='city-tile-txt'])["+i+"]"), "");
				String name=driver.getText(By.xpath(UserProfilePage.cityTileTxt1.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"");
				if(name.contains(firName))
				{
						flag=true;
						break;
				}
					
				
			}
					
			if(flag) 
				pass("Customer City is displayed & verified");
			else 
				fail("Customer City is not matched");

		}
		else
			fail("Customer City is not displayed");
		
	}


	public void verifyStateZipCodeinTile() {
//		if(driver.isElementDisplayed(UserProfilePage.cityTileTxt, 10, "State in Address Tile")) {
//			if(driver.isElementDisplayed(UserProfilePage.cityTileTxt, 10, "Zipcode in Address Tile")) {
//				String state = driver.getText(UserProfilePage.stateTileTxt, "State Tile");
//				String stat = ExcelReader.getData("LoginCredentials", "State");
//				String zipCode = driver.getText(UserProfilePage.zipcodeTileTxt, "Zip Tile");
//				String zipc = ExcelReader.getData("LoginCredentials", "Zipcode");
//				if(state.equalsIgnoreCase(stat) && zipCode.equalsIgnoreCase(zipc))
//				{
//					pass("State & ZipCode is displayed & verified");
//				}
//				else
//					fail("State & Zipcode is not matched");
//			}
//			else
//				fail("State & Zip code is not displayed in tile");
		if(driver.isElementDisplayed(UserProfilePage.zipcodeTileTxt, 35, "CustomerName in Zipcode Tile")) 
		{
			String firName  = ExcelReader.getData("LoginCredentials", "Zipcode");
			int size=driver.getSize(UserProfilePage.zipcodeTileTxt, "Customer Zipcode Tile");
			boolean flag=false;
			for(int i=1;i<=size;i++)
			{
				String name=driver.getText(By.xpath("(//*[@data-qa='zipcode-tile-txt'])["+i+"]"), "");
				if(name.contains(firName))
				{
						flag=true;
						break;
				}
					
				
			}
					
			if(flag) 
				pass("Customer Zipcode is displayed & verified");
			else 
				fail("Customer Zipcode is not matched");

		}
		else
			fail("Customer Zipcode is not displayed");
		

	}



	/** Delete Existing address **/

	public void deleteSavedAddress() {
		if(driver.explicitWaitforClickable(UserProfilePage.deleteTrigger, 35, "Delete icon in address tile"))
		{
			if(driver.clickElement(UserProfilePage.deleteTrigger, "Delete icon clicked")) 
			{
				pass("Delete icon is clicked");
			}
			else
			{
				fail("Failed to display delete icon");
			}
//			if(driver.isElementDisplayed(UserProfilePage.deletePopUp, 10, "Confirmation pop-up"))
//			{
				driver.clickElement(UserProfilePage.okTrigger, "OK Button");
				driver.isElementDisplayed(UserProfilePage.deleteSuccessText, 35, "Success Message displayed");
				pass ("Success Message get displayed");
//			}
//			else {
//				fail("Confirmation pop-up not displayed");
//			}
		}
		else
			fail("Failed to display delete icon");
	}


	/** verify Make Default button**/

	public void makeDefaultAddress() {
		int size = 0;
		if(driver.isElementDisplayed(UserProfilePage.defaultTrigger, 35, "Make Default")) 
		{
			size = driver.getSize(UserProfilePage.defaultTrigger, "Make Default");
//			if(size<2) {
//				pass ("Make Default is Present in Single tile");
//				driver.clickElement(UserProfilePage.defaultTrigger, "Make Default clicked");
//				pass("Make Default is clicked successfully");
//			}
//			else 
//			{
//				fail("Make Default button present more than one tile");
//			}
			driver.clickElement(UserProfilePage.defaultTrigger, "Make Default clicked");
			if(driver.isElementDisplayed(UserProfilePage.defaultTxt, 55, "Make Default")) 
			{
				pass("Default text gets displayed");
			}
			else 
			{
				fail("Default text not gets updated");
			}
		}
		else
			fail("Failed to displayed Default button");
	}


	/** Clear Mandatory Fields in Edit Address pop-up **/

	public void clearEditAddressFirstName()
	{
		if(driver.explicitWaitforClickable(UserProfilePage.editfirstNameTxt, 35, "First Name"))
		{
			if(driver.clearText(UserProfilePage.editfirstNameTxt, 35, "First Name"))
				pass("First Name field is Cleared");
		}
		else
			fail("First name field not display");
	}

	public void clearEditAddressLastName()
	{
		if(driver.explicitWaitforClickable(UserProfilePage.editlastNameTxt, 35, "Last Name"))
		{
			if(driver.clearText(UserProfilePage.editlastNameTxt, 35, "Last Name"))
				pass("Last Name field is Cleared");
		}
		else
			fail("Last name field not display");
	}

	public void clearEditAddressAddress1()
	{
		if(driver.explicitWaitforClickable(UserProfilePage.editaddress1Txt, 35, "Address 1"))
		{
			if(driver.clearText(UserProfilePage.editaddress1Txt, 35, "Address 1"))
				pass("Address 1 field is Cleared");
		}
		else
			fail("Address 1 field not display");
	}

	public void clearEditAddressAddress2()
	{
		if(driver.explicitWaitforClickable(UserProfilePage.editaddress2Txt, 35, "Address 2"))
		{
			if(driver.clearText(UserProfilePage.editaddress2Txt, 35, "Address 2"))
				pass("Address 2 field is Cleared");
		}
		else
			fail("Address 2 field not display");
	}

	public void clearEditAddressCity()
	{
		if(driver.explicitWaitforClickable(UserProfilePage.editcityInput, 35, "City"))
		{
			if(driver.clearText(UserProfilePage.editcityInput, 35, "City"))
				pass("City field is Cleared");
		}
		else
			fail("City field not display");
	}

	public void clearEditAddressState()
	{
		if(driver.explicitWaitforClickable(UserProfilePage.editstateInput, 35, "State"))
		{
			if(driver.clearText(UserProfilePage.editstateInput, 35, "State"))
				pass("State field is Cleared");
		}
		else
			fail("State field not display");
	}

	public void clearEditAddressCountry()
	{
		if(driver.explicitWaitforClickable(UserProfilePage.editcountryInput, 35, "Country"))
		{
			if(driver.clearText(UserProfilePage.editcountryInput, 35, "Country"))
				pass("Country field is Cleared");
		}
		else
			fail("Country field not display");
	}

	public void clearEditAddressZipCode()
	{
		if(driver.explicitWaitforClickable(UserProfilePage.editzipcodeInput, 35, "Zip code"))
		{
			if(driver.clearText(UserProfilePage.editzipcodeInput, 35, "Zip code"))
				pass("Zip code field is Cleared");
		}
		else
			fail("Zip code field not display");
	}

	public void clearEditAddressPhoneNo()
	{
		if(driver.explicitWaitforClickable(UserProfilePage.editphoneInput, 35, "Phone Number"))
		{
			if(driver.clearText(UserProfilePage.editphoneInput, 35, "Phone Number"))
				pass("Phone Number field is Cleared");
		}
		else
			fail("Phone Number field not display");
	}

	/** Blank Field Error validation message **/

	public void inlineErrorValidationForEditAddressFields() {
		if(driver.explicitWaitforVisibility(UserProfilePage.firstnameError, 35, "First Name Error "))
		{
			String fName=driver.getElementAttribute(UserProfilePage.firstnameError,"data-error" , "First Name Error ");
			pass(fName+" is display");
		}
		else
			fail("First name error not display");


		if(driver.explicitWaitforVisibility(UserProfilePage.lastnameError, 35, "Last Name Error "))
		{
			String lName=driver.getElementAttribute(UserProfilePage.lastnameError,"data-error", "Last name error ");
			pass(lName+" is display");
		}
		else
			fail("Last name error not display");

		if(driver.explicitWaitforVisibility(UserProfilePage.address1Error, 35, "Address 1 Error "))
		{
			String adddress1=driver.getElementAttribute(UserProfilePage.address1Error,"data-error", "Address 1 Error");
			pass(adddress1+" is display");
		}
		else
			fail("Last name error not display");

		if(driver.explicitWaitforVisibility(UserProfilePage.address2Error, 35, "Address 2 Error "))
		{
			String address2=driver.getElementAttribute(UserProfilePage.address2Error,"data-error", "Address 2 error");
			pass(address2+" is display");
		}
		else
			fail("Address 2 error not display");

		if(driver.explicitWaitforVisibility(UserProfilePage.cityError, 35, "City Error "))
		{
			String city=driver.getElementAttribute(UserProfilePage.cityError,"data-error", "City error");
			pass(city+" is display");
		}
		else
			fail("city error not display");

		if(driver.explicitWaitforVisibility(UserProfilePage.stateError, 35, "State Error "))
		{
			String state=driver.getElementAttribute(UserProfilePage.stateError,"data-error", "State error");
			pass(state+" is display");
		}
		else
			fail("state error not display");

		if(driver.explicitWaitforVisibility(UserProfilePage.countryError, 35, "Country Error"))
		{
			String country=driver.getElementAttribute(UserProfilePage.countryError,"data-error", "Country error");
			pass(country+" is display");
		}
		else
			fail("country  error not display");

		if(driver.explicitWaitforVisibility(UserProfilePage.zipcodeError, 35, "Zipcode Error"))
		{
			String zipcode=driver.getElementAttribute(UserProfilePage.zipcodeError,"data-error", "Zipcode error");
			pass(zipcode+" is display");
		}
		else
			fail("Zipcode  error not display");

		if(driver.explicitWaitforVisibility(UserProfilePage.phoneNoError, 35, "Phone no Error"))
		{
			String phoneNo=driver.getElementAttribute(UserProfilePage.phoneNoError,"data-error", "Phone no error");
			pass(phoneNo+" is display");
		}
		else
			fail("Phone Number error not display");
	}
	
	//for edit address
	public void inlineErrorValidationForEditAddressFields2() {
		if(driver.explicitWaitforVisibility(orderPage.inlineErrorValidationForEditAddressFields2, 35, "First Name Error "))
		{
			String fName=driver.getElementAttribute(orderPage.inlineErrorValidationForEditAddressFields2,"data-error" , "First Name Error ");
			pass(fName+" is display");
		}
		else
			fail("First name error not display");


		if(driver.explicitWaitforVisibility(orderPage.inlineErrorValidationForEditAddressFields21, 35, "Last Name Error "))
		{
			String lName=driver.getElementAttribute(orderPage.inlineErrorValidationForEditAddressFields21,"data-error", "Last name error ");
			pass(lName+" is display");
		}
		else
			fail("Last name error not display");

		if(driver.explicitWaitforVisibility(orderPage.inlineErrorValidationForEditAddressFields22, 35, "Address 1 Error "))
		{
			String adddress1=driver.getElementAttribute(orderPage.inlineErrorValidationForEditAddressFields22,"data-error", "Address 1 Error");
			pass(adddress1+" is display");
		}
		else
			fail("Last name error not display");

		if(driver.explicitWaitforVisibility(orderPage.inlineErrorValidationForEditAddressFields23, 35, "City Error "))
		{
			String city=driver.getElementAttribute(orderPage.inlineErrorValidationForEditAddressFields23,"data-error", "City error");
			pass(city+" is display");
		}
		else
			fail("city error not display");

		if(driver.explicitWaitforVisibility(orderPage.inlineErrorValidationForEditAddressFields24, 35, "State Error "))
		{
			String state=driver.getElementAttribute(orderPage.inlineErrorValidationForEditAddressFields24,"data-error", "State error");
			pass(state+" is display");
		}
		else
			fail("state error not display");

		if(driver.explicitWaitforVisibility(orderPage.inlineErrorValidationForEditAddressFields25, 35, "Country Error"))
		{
			String country=driver.getElementAttribute(orderPage.inlineErrorValidationForEditAddressFields25,"data-error", "Country error");
			pass(country+" is display");
		}
		else
			fail("country  error not display");

		if(driver.explicitWaitforVisibility(orderPage.inlineErrorValidationForEditAddressFields26, 35, "Zipcode Error"))
		{
			String zipcode=driver.getElementAttribute(orderPage.inlineErrorValidationForEditAddressFields26,"data-error", "Zipcode error");
			pass(zipcode+" is display");
		}
		else
			fail("Zipcode  error not display");

		if(driver.explicitWaitforVisibility(orderPage.inlineErrorValidationForEditAddressFields27, 35, "Phone no Error"))
		{
			String phoneNo=driver.getElementAttribute(orderPage.inlineErrorValidationForEditAddressFields27,"data-error", "Phone no error");
			pass(phoneNo+" is display");
		}
		else
			fail("Phone Number error not display");
	}

	/** Adding New Address **/

	public void addAddressTile() {
		if(driver.isElementDisplayed(UserProfilePage.addAddressTitle, 35, "Add Address")) {
			pass("Add Address get displayed");
		}
		else
			fail("Failed to display Add Address tile");
	}

	/** Verifying Add New Address Pop-up screen gets displayed**/

	public void clickAddAddressButton() {
		if(driver.explicitWaitforClickable(UserProfilePage.addNewAddress, 35, "Add New Address button")){
			if(driver.clickElement(UserProfilePage.addNewAddress, "Add New Address button")) {
				if(driver.isElementDisplayed(UserProfilePage.addAddressTitle, 35, "Add Address pop-up")) {
					pass("Add Address pop-up gets displayed");
				}
				else {
					fail("Pop-up not displayed");
				}
			}
		}
		else {
			fail("Failed to displayed add New address button");
		}
	}

	/** Verifying Blank Fields of Add New Address pop-up screen **/

	public void emptyMandatoryFieldsInAddNewAddress() {
		if(driver.isElementDisplayed(UserProfilePage.addFirstName, 35, "FirstName field")) {
			String firstName = driver.getText(UserProfilePage.addFirstName, "First Name");
			if(firstName.isEmpty()) {
				pass("First Name field is empty");
			}
			else 
				fail("First Name field is Prefilled");
		}
		if(driver.isElementDisplayed(UserProfilePage.addLastName, 35, "LastName field")) {
			String lastName = driver.getText(UserProfilePage.addLastName, "Last Name");
			if(lastName.isEmpty()) {
				pass("Last Name field is empty");
			}
			else 
				fail("Last Name field is Prefilled");
		}
		if(driver.isElementDisplayed(UserProfilePage.addAddress1, 35, "Address1 field")) {
			String address1 = driver.getText(UserProfilePage.addAddress1, "Address1");
			if(address1.isEmpty()) {
				pass("Address1 field is empty");
			}
			else 
				fail("Address1 field is Prefilled");
		}
		if(driver.isElementDisplayed(UserProfilePage.addAddress2, 35, "Address2 field")) {
			String address2 = driver.getText(UserProfilePage.addAddress1, "Address2");
			if(address2.isEmpty()) {
				pass("Address2 field is empty");
			}
			else 
				fail("Address2 field is Prefilled");
		}
		if(driver.isElementDisplayed(UserProfilePage.addCity, 35, "City field")) {
			String city = driver.getText(UserProfilePage.addCity, "City");
			if(city.isEmpty()) {
				pass("City field is empty");
			}
			else 
				fail("City field is Prefilled");
		}
		if(driver.isElementDisplayed(UserProfilePage.addState, 35, "State field")) {
			String state = driver.getText(UserProfilePage.addState, "State");
			if(state.isEmpty()) {
				pass("State field is empty");
			}
			else 
				fail("State field is Prefilled");
		}
		if(driver.isElementDisplayed(UserProfilePage.addCountry, 35, "Country field")) {
			String country = driver.getText(UserProfilePage.addCountry, "Country");
			if(country.isEmpty()) {
				pass("Country field is empty");
			}
			else 
				fail("Country field is Prefilled");
		}
		if(driver.isElementDisplayed(UserProfilePage.addZipCode, 35, "Zip Code field")) {
			String zipCode = driver.getText(UserProfilePage.addZipCode, "Zip Code");
			if(zipCode.isEmpty()) {
				pass("Zip Code field is empty");
			}
			else 
				fail("Zip Code field is Prefilled");
		}
		if(driver.isElementDisplayed(UserProfilePage.addPhoneNo, 35, "Phone field")) {
			String phNo = driver.getText(UserProfilePage.addPhoneNo, "Phone");
			if(phNo.isEmpty()) {
				pass("Phone field is empty");
			}
			else 
				fail("Phone field is Prefilled");
		}
	}

	/** verifying Save & Cancel button in Add Address Screen **/

	public void buttonInAddAddressScreen() {
		if(driver.isElementDisplayed(UserProfilePage.addSaveTrigger, 35, "Save Button")) {
			pass("Save button is displayed");
		}
		else {
			fail("Failed to display Save button");
		}
		if(driver.isElementDisplayed(UserProfilePage.addCancelTrigger, 35, "Cancel Button")) {
			pass("Cancel button is displayed");
		}
		else {
			fail("Failed to display Cancel button");
		}
	}

	/** Entering All fields in Add Address Screen **/

	public void enteringAllMandatoryFieldsInAddAddress() {
		if(driver.isElementDisplayed(UserProfilePage.addFirstName, 35, "First Name field")) {
			driver.enterText(UserProfilePage.addFirstName, ExcelReader.getData("LoginCredentials", "FirstName"), "FirstName");
			pass("First Name field is entered");
		}
		else {
			fail("First Name field is not entered");
		}
		if(driver.isElementDisplayed(UserProfilePage.addLastName, 35, "Last Name field")) {
			driver.enterText(UserProfilePage.addLastName, ExcelReader.getData("LoginCredentials", "LastName"), "LastName");
			pass("Last Name field is entered");
		}
		else {
			fail("Last Name field is not entered");
		}
		if(driver.isElementDisplayed(UserProfilePage.addAddress1, 35, "Address 1 field")) {
			driver.enterText(UserProfilePage.addAddress1, ExcelReader.getData("LoginCredentials", "Address1"), "Address1");
			pass("Address 1 field is entered");
		}
		else {
			fail("Address 1 field is not entered");
		}
		if(driver.isElementDisplayed(UserProfilePage.addAddress2, 35, "Address 2 field")) {
			driver.enterText(UserProfilePage.addAddress2, ExcelReader.getData("LoginCredentials", "Address2"), "Address2");
			pass("Address 2 field is entered");
		}
		else {
			fail("Address 2 field is not entered");
		}
		if(driver.isElementDisplayed(UserProfilePage.addCity, 35, "City field")) {
			driver.enterText(UserProfilePage.addCity, ExcelReader.getData("LoginCredentials", "City"), "City");
			pass("City field is entered");
		}
		else {
			fail("City field is not entered");
		}
		if(driver.isElementDisplayed(UserProfilePage.addState, 35, "State field")) {
			driver.enterText(UserProfilePage.addState, ExcelReader.getData("LoginCredentials", "State"), "State");
			pass("State field is entered");
		}
		else {
			fail("State field is not entered");
		}
		if(driver.isElementDisplayed(UserProfilePage.addCountry, 35, "Country field")) {
			driver.enterText(UserProfilePage.addCountry, ExcelReader.getData("LoginCredentials", "Country"), "Country");
			pass("Country field is entered");
		}
		else {
			fail("Country field is not entered");
		}
		if(driver.isElementDisplayed(UserProfilePage.addZipCode, 35, "Zip code field")) {
			driver.enterText(UserProfilePage.addZipCode, ExcelReader.getData("LoginCredentials", "Zipcode"), "Zip code");
			pass("ZipCode field is entered");
		}
		else {
			fail("Zipcode field is not entered");
		}
		if(driver.isElementDisplayed(UserProfilePage.addPhoneNo, 35, "Phone no field")) {
			driver.enterText(UserProfilePage.addPhoneNo, ExcelReader.getData("LoginCredentials", "Phoneno"), "Phone no");
			pass("Phone No field is entered");
		}
		else {
			fail("Phone No field is not entered");
		}

	}

	/** Triggering Save button **/

	public void triggerSave() {
		if(driver.explicitWaitforClickable(UserProfilePage.addSaveTrigger, 35, "Save Button")) {
			if(driver.jsClickElement(UserProfilePage.addSaveTrigger, "Save button")) {
				pass("Save button is clicked");
			}
			else
				fail("Failed to click");
		}
		else
			fail("Failed to display Save button");
	}

	/** Success Message on Adding New Address **/

	public void addAddressSuccessMessage() {
		if(driver.isElementDisplayed(UserProfilePage.addSuccessTxt, 35, "Success Message")) {
			pass("Success Message gets displayed");
		}
		else
			fail("Failed to display it");
	}

	/** Triggering Cancel button in Add Address Pop-up **/

	public void triggerCancel() {
		if(driver.explicitWaitforClickable(UserProfilePage.addCancelTrigger, 35, "Add Address pop-up")) {
			if(driver.clickElement(UserProfilePage.addCancelTrigger, "Cancel button")) {
				pass("Cancel in Add Address button is clicked");
			}
			else
				fail("Failed to display cancel button");
		}
		if(driver.isElementDisplayed(UserProfilePage.addNewAddress, 35, "Address screen")) {
			pass("Address Screen gets dislpayed");
		}
		else
			fail("Failed to displayed Address screen");
	}

	/** Cancel trigger in Edit Address pop-up **/

	public void triggerCancelEditAddress() {
		if(driver.explicitWaitforClickable(UserProfilePage.editcancelTrigger, 35, "Edit Address pop-up")) {
			if(driver.clickElement(UserProfilePage.editcancelTrigger, "Click Cancel")) {
				pass("Cancel in Edit Address button is clicked");
			}
			else
				fail("Failed to display cancel button");
		}
		if(driver.isElementDisplayed(UserProfilePage.savedAddressTile, 35, "Existing Address tile")) {
			pass("Address screen gets displayed");
		}
		else
			fail("Failed to dislay Existing address");
	}

}













































