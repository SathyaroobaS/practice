package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.UserProfilePage;
import com.skava.object.repository.customerLandingPage;
import com.skava.object.repository.orderPage;

public class orderPageComponents extends userProfilePageComponents
{
	public void hoverandclickCustomers()
	{
		if(driver.explicitWaitforVisibility(orderPage.emailValue, 35, "view button"))
		{
			int size = driver.getSize(orderPage.emailValue, "view btn");
			int rand = driver.generateRandomNumber(size);
			if(rand == 0)
				rand = rand + 1;
			//if(driver.jsClickElement(By.xpath("(//*[@data-qa='view-profile-trigger'])["+rand+"]"), "view profile button"))
			if(driver.jsClickElement(orderPage.hoverandclickCustomers2, "view profile button")) {
				pass("View Button is Clicked");}
			else {
				fail("View Button is not Clicked");}
				
		}
	}
	
	public void hoverandclickCustomers1()
	{
		if(driver.explicitWaitforVisibility(orderPage.emailValue, 35, "view button"))
		{
			int size = driver.getSize(orderPage.emailValue, "view btn");
			int rand = driver.generateRandomNumber(size);
			if(rand == 0)
				rand = rand + 1;
			//if(driver.jsClickElement(By.xpath("(//*[@data-qa='view-profile-trigger'])["+rand+"]"), "view profile button"))
			if(driver.jsClickElement(orderPage.hoverandclickCustomers9, "view profile button")) {
				pass("View Button is Clicked");}
			else {
				fail("View Button is not Clicked");}
				
		}
	}
	
	public void hoverandclickCustomers10()
	{
		if(driver.explicitWaitforVisibility(orderPage.emailValue, 35, "view button"))
		{
			int size = driver.getSize(orderPage.emailValue, "view btn");
			int rand = driver.generateRandomNumber(size);
			if(rand == 0)
				rand = rand + 1;
			//if(driver.jsClickElement(By.xpath("(//*[@data-qa='view-profile-trigger'])["+rand+"]"), "view profile button"))
			if(driver.jsClickElement(orderPage.hoverandclickCustomers9, "view profile button")) {
				pass("View Button is Clicked");}
			else {
				fail("View Button is not Clicked");}
				
		}
	}
	
	public void hoverandclickCustomers5()
	{
		if(driver.explicitWaitforVisibility(orderPage.emailValue, 35, "view button"))
		{
			int size = driver.getSize(orderPage.emailValue, "view btn");
			int rand = driver.generateRandomNumber(size);
			if(rand == 0)
				rand = rand + 1;
			//if(driver.jsClickElement(By.xpath("(//*[@data-qa='view-profile-trigger'])["+rand+"]"), "view profile button"))
			if(driver.jsClickElement(orderPage.hoverandclickCustomers7, "view profile button")) {
				pass("View Button is Clicked");}
			else {
				fail("View Button is not Clicked");}
				
		}
	}
	
	public void hoverandclickCustomers6()
	{
		if(driver.explicitWaitforVisibility(orderPage.emailValue, 35, "view button"))
		{
			int size = driver.getSize(orderPage.emailValue, "view btn");
			int rand = driver.generateRandomNumber(size);
			if(rand == 0)
				rand = rand + 1;
			//if(driver.jsClickElement(By.xpath("(//*[@data-qa='view-profile-trigger'])["+rand+"]"), "view profile button"))
			if(driver.jsClickElement(orderPage.hoverandclickCustomers6, "view profile button")) {
				pass("View Button is Clicked");}
			else {
				fail("View Button is not Clicked");}
				
		}
	}
	
	public void clickOrdersTab()
	{
		if(driver.explicitWaitforVisibility(orderPage.ordersTab, 35, "orders tab"))
		{
			if(driver.jsClickElement(orderPage.ordersTab, "orders tab")) {
				pass("Orders Tab is Clicked");}
			else {
				fail("Orders Tab is not clicked");}
		}
	}
	
	public void verifyOrderList()
	{
		if(driver.explicitWaitforVisibility(orderPage.orderValue, 45, "order list")) {
			pass("Order List is displayed");}
		else {
			fail("Order List is not displayed");}
	}
	
	public void verifyHeaders()
	{
		if(driver.explicitWaitforVisibility(orderPage.ordersIdTitle, 35, "order id")) {
			pass("Order Id title is displayed");}
		else {
			fail("Order Id title is not displayed");}
		if(driver.explicitWaitforVisibility(orderPage.orderPlacedDateTitle, 35, "order placed date")) {
			pass("order placed date title is displayed");}
		else {
			fail("order placed date title is not displayed");}
		if(driver.explicitWaitforVisibility(orderPage.orderValueTitle, 35, "order value")) {
			pass("order value title is displayed");}
		else {
			fail("order value title is not displayed");}
	}
	
	public void hoverandVerifyViewBtn()
	{
		if(driver.explicitWaitforVisibility(orderPage.orderValue, 35, "order value"))
		{	
			int size = driver.getSize(orderPage.orderValue, "order value");
			int rand = driver.generateRandomNumber(size);
			if(rand == 0)
				rand = rand +1;
			//driver.mouseHoverElement(By.xpath("(//*[@data-qa='order-value'])["+rand+"]"), "order value");
			driver.mouseHoverElement(By.xpath(orderPage.hoverandVerifyViewBtn.toString().replace("By.xpath: ", "").concat("[" +rand+ "]")),"order value");
			if(driver.explicitWaitforVisibility(orderPage.viewOrderBtn, 35, "view btn")) {
				pass("View Btn is displayed");}
			else {
				fail("View Btn is displayed");}
		}
		
	}
	
	public void verifyPagination()
	{
		if(driver.explicitWaitforVisibility(orderPage.pagination, 35, "pagination")) {
			pass("Pagination is displayed");}
		else {
			fail("Pagination is not displayed");}
	}
	
	public void verifyRowsDropdown()
	{
		if(driver.explicitWaitforVisibility(orderPage.rowsDropdown, 35, "pagination")) {
			pass("rowsDropdown is displayed");}
		else {
			fail("rowsDropdown is not displayed");}
	}
	
	public void clickViewBtn()
	{
		if(driver.jsClickElement(orderPage.viewOrderBtn,"view btn")) {
			pass("View Order is Clicked");}
		else {
			fail("View Order is not clicked");}
	}
	
	public void VerifyOrderAdmin()
	{
		driver.changeFocus(1);
		if(driver.explicitWaitforVisibility(orderPage.VerifyOrderAdmin, 35, "order"))
		{	
			String text = driver.getCurrentUrl();
			if(text.indexOf("orders")!=-1) {
				pass("Order Admin is Displayed");}
			else {
				fail("Order Admin is not Displayed");}
		}
	}
	
	public void verifyOrderIdFilter()
	{
		if(driver.explicitWaitforVisibility(orderPage.orderIdFilter, 35, "orderIdFilter"))
		{
			pass("OrderId Filter is Displayed");
			if(driver.clickElement(orderPage.orderIdFilter, "orderIdFilter")) {
				pass("OrderId Filter is Clicked");}
			else {
				fail("OrderId Filter is not clicked");}
		}
	}
	
	public void EnterOrderIdandVerifyOrderIdresults() throws InterruptedException
	{
		driver.explicitWaitforVisibility(orderPage.ordersIdValue, 35, "orderId Value");
		String text = driver.getText(orderPage.ordersIdValue, "orderId Value");
		driver.explicitWaitforVisibility(orderPage.orderIdInput, 35, "orderId Input");
		if(driver.enterText(orderPage.orderIdInput, text, "orderId Input"))
			pass("OrderId is Entered");
		else
			fail("OrderId is not Entered ");
		driver.explicitWaitforVisibility(orderPage.searchBtn, 35, "orderId Input");
		if(driver.jsClickElement(orderPage.searchBtn, "search Btn"))
			pass("Search Btn is Clicked");
		else
			fail("Search Btn is not clicked");
		Thread.sleep(6000);
		if(driver.explicitWaitforVisibility(orderPage.ordersIdValue, 35, "orderIdValue"))
		{
			int size = driver.getSize(orderPage.ordersIdValue, "orderId Value ");
			String text1 = driver.getText(orderPage.ordersIdValue, "orderId Value");
			if(size == 1 && text1.equals(text))
				pass("Search results are verified");
			else
				fail("Search results are not verified");
		}
	}
	
	public void EnterOrderId()
	{
		if(driver.enterText(orderPage.orderIdInput, ExcelReader.getData("Keywords", "Keyword_1"), "orderId Input"))
			pass("Order Id is Entered");
		else
			fail("Order Id is Not Entered");
		driver.jsClickElement(orderPage.searchBtn, "search btn");
	}
	
	public void selectstartdate()
	{
		if(driver.explicitWaitforVisibility(orderPage.fromDatePicker, 60, "daterange"))
		{
			int size=driver.getSize(orderPage.selectstartdate, "Start date");
			int random=driver.generateRandomNumber(size)+1;
			//String text=driver.getText(By.xpath("(//*[@class='drp-calendar left']//*[contains (@class, 'available')])["+random+"]"), "Start date");
			String text=driver.getText(By.xpath(orderPage.selectstartdate1.toString().replace("By.xpath: ", "").concat("[" +random+ "]")),"Start date");
			if(driver.clickElement(By.xpath(orderPage.selectstartdate1.toString().replace("By.xpath: ", "").concat("[" +random+ "]")), "Select"+text))
				pass("Selected date "+text);
			else
				fail("date field not clickable");
			
		}
	}
	public void selectenddate()
	{
		if(driver.explicitWaitforVisibility(orderPage.toDatePicker, 45, "daterange"))
		{
//			int size=driver.getSize(By.xpath("//*[@class='drp-calendar right']//*[@class='available']"), "End date");
//			int random=driver.generateRandomNumber(size)+1;
//			String text=driver.getText(By.xpath("(//*[@class='drp-calendar right']//*[@class='available'])["+random+"]"), "End date");
//			if(driver.clickElement(By.xpath("(//*[@class='drp-calendar right']//*[@class='available'])["+random+"]"), "Select"+text))
//				pass("Selected date "+text);
//			else
//				fail("date field not clickable");
			driver.jsClickElement(orderPage.searchBtn, "search btn");
			driver.jsClickElement(orderPage.selectenddate, "Click Apply button");
			
		}
	}
	
	public void clickDatePicker()
	{
		if(driver.explicitWaitforVisibility(orderPage.dateRangeFilter, 35, "date range"))
		{	
			pass("date range filter is displayed");
			if(driver.jsClickElement(orderPage.dateRangeFilter, "date picker"))
				pass("Date Picker is Clicked");
			else
				fail("Date Picker is not Clicked");
		}
		else
			fail("date range filter is not displayed");
	}
	
	public void clickPaymentsTab() throws InterruptedException
	{
		if(driver.explicitWaitforVisibility(orderPage.paymentsTab, 35, "orders tab"))
		{
			if(driver.jsClickElement(orderPage.paymentsTab, "orders tab"))
				pass("Payments Tab is Clicked");
			else
				fail("Payments Tab is not clicked");
		}
	}
	
	public void clickAddressTab()
	{
		if(driver.explicitWaitforVisibility(orderPage.addressTab, 35, "orders tab"))
		{
			if(driver.jsClickElement(orderPage.addressTab, "orders tab"))
				pass("Payments Tab is Clicked");
			else
				fail("Payments Tab is not clicked");
		}
	}
	
	public void VerifyNoResults()
	{
		if(driver.explicitWaitforVisibility(orderPage.noDataFound, 35, "orders tab"))
		{	
			int size = driver.getSize(orderPage.ordersIdValue, "orderId Value ");
			if(size == 0)
				pass("No results Found");
			else
				fail("Results are displayed");
		}
	}
	
	public void verifyResults()
	{
		if(driver.explicitWaitforVisibility(orderPage.ordersIdValue, 35, "orderIdValue"))
		{
			int size = driver.getSize(orderPage.ordersIdValue, "orderId Value ");
			if(size >= 1)
				pass("Search results are verified");
			else
				fail("Search results are not verified");
		}
	}
	
	public void verifyLatestOrder()
	{
		String text = driver.getText(orderPage.verifyLatestOrder, "first order");
		//String text1 = driver.getText(By.xpath("(//*[@data-qa='order-placed-date-title'])[2]"), "first order");
		pass(text);
		String[] date1=text.split("-");
		int size = driver.getSize(orderPage.verifyLatestOrder1, "orders");
		//String text1 = driver.getText(By.xpath("(//*[@data-qa='order-placed-date-value'])["+size+"]"), "first order");
		String text1 = driver.getText(By.xpath(orderPage.verifyLatestOrder2.toString().replace("By.xpath: ", "").concat("[" +size+ "]")),"first order");
		String[] date2=text1.split("-");
		if(Integer.parseInt(date1[2]) > Integer.parseInt(date2[2]))
		{
			pass("Latest order is displayed in top");
		}
		else if(Integer.parseInt(date1[0]) > Integer.parseInt(date2[0]))
		{
			pass("Latest order is displayed in top");
		}
		else if(Integer.parseInt(date1[1]) >= Integer.parseInt(date2[1]))
			pass("Latest order is displayed in top");
		else
			fail("Latest order is not displayed in top");
	}
	
}