package com.skava.reusable.components;

import com.skava.object.repository.CartDetailPage;


public class cartPageComponents extends editProfileComponents {

	
	public void clickCartTab()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartTab, 35, "Cart Tab "))
		{	
			pass("Cart Tab Option is displayed");
			if(driver.jsClickElement(CartDetailPage.customerCartTab, "Cart Tab")) {
				pass("Cart Tab Option is Clicked");
			}
			else {
				fail("Cart Tab Option is not Clicked");
			}
		}
		else {
			fail("Cart Tab Option is not displayed");
		}
	}
	
	public void verifyCartTab()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartTab, 35, "Cart Tab "))
		{	
			pass("Cart Tab Option is displayed");
		}
		else {
			fail("Cart Tab Option is not displayed");
		}
	}
	
	public void clickCartViewProfileButton() {
		
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartViewProfileButton, 35, "View Profile "))
		{	
			pass("View Profile Option is displayed");
			if(driver.jsClickElement(CartDetailPage.customerCartViewProfileButton, "View Profile")) {
				pass("View Profile Option is Clicked");
			}
			else {
				fail("View Profile Option is not Clicked");
			}
		}
		else {
			fail("View Profile Option is not displayed");
		}
		
	}
	
	
	public void verifyCartProductHeader()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductHeader, 35, "Cart Product Header "))
		{	
			pass("Cart Product Header is displayed");
		}
		else {
			fail("Cart Product Header is not displayed");
		}
	}
	
	public void verifyCartQuantityHeader()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartQuantityHeader, 35, "Cart Quantity Header "))
		{	
			pass("Cart Quantity Header is displayed");
		}
		else {
			fail("Cart Quantity Header is not displayed");
		}
	}
	
	public void verifyCartItemPriceHeader()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartItemPriceHeader, 35, "Cart Item Price Header "))
		{	
			pass("Cart ItemPrice Header is displayed");
		}
		else {
			fail("Cart ItemPrice Header is not displayed");
		}
	}
	
	public void verifyCartTotalHeader()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartTotalHeader, 35, "Cart Item Price Header "))
		{	
			pass("Cart Total Header is displayed");
		}
		else {
			fail("Cart Total Header is not displayed");
		}
	}
	
	
	public void verifyCartProductImage()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductImage, 35, "Cart Product Image "))
		{	
			pass("Cart Product Image is displayed");
		}
		else {
			fail("Cart Product Image is not displayed");
		}
	}
	
	public void verifyCartProductName()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductName, 35, "Cart Product Name "))
		{	
			pass("Cart Product Name is displayed");
		}
		else {
			fail("Cart Product Name is not displayed");
		}
	}
	
	public void verifyCartProductDescription()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductDescription, 35, "Cart Product Description "))
		{	
			pass("Cart Product Description is displayed");
		}
		else {
			fail("Cart Product Description is not displayed");
		}
	}
	
	public void verifyCartProductSize()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductSize, 35, "Cart Product Size "))
		{	
			pass("Cart Product Size is displayed");
		}
		else {
			fail("Cart Product Size is not displayed");
		}
	}
	
	public void verifyCartProductColor()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductColor, 35, "Cart Product Color "))
		{	
			pass("Cart Product Color is displayed");
		}
		else {
			fail("Cart Product Color is not displayed");
		}
	}
	
	public void verifyCartProductItemId()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductItemId, 35, "Cart Product ItemId "))
		{	
			pass("Cart Product ItemId is displayed");
		}
		else {
			fail("Cart Product ItemId is not displayed");
		}
	}
	
	public void verifyCartProductQuantity()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductQuantity, 35, "Cart Product Quantity "))
		{	
			pass("Cart Product Quantity is displayed");
		}
		else {
			fail("Cart Product Quantity is not displayed");
		}
	}
	
	public void verifyCartProductSellingPrice()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductSellingPrice, 35, "Cart Product Selling Price "))
		{	
			pass("Cart Product Selling Price is displayed");
		}
		else {
			fail("Cart Product Selling Price is not displayed");
		}
	}
	
	public void verifyCartProductItemPrice()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductItemPrice, 35, "Cart Product Item Price "))
		{	
			pass("Cart Product Item Price is displayed");
		}
		else {
			fail("Cart Product Item Price is not displayed");
		}
	}
	
	public void verifyCartProductTotalPrice()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductTotalPrice, 35, "Cart Product Total Price "))
		{	
			pass("Cart Product Total Price is displayed");
		}
		else {
			fail("Cart Product Total Price is not displayed");
		}
	}
	
	public void verifyCartProductYouSavePrice()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductYouSavePrice, 35, "Cart Product YouSave Price "))
		{	
			pass("Cart Product YouSave Price is displayed");
		}
		else {
			fail("Cart Product YouSave Price is not displayed");
		}
	}
	
	public void verifyCartProductSubTotalHeading()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductSubTotalHeading, 35, "Cart Product SubTotal Heading "))
		{	
			pass("Cart Product SubTotal Heading is displayed");
		}
		else {
			fail("Cart Product SubTotal Heading is not displayed");
		}
	}
	
	public void verifyCartProductSubTotalPrice()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartProductSubTotalPrice, 35, "Cart Product SubTotal Price "))
		{	
			pass("Cart Product SubTotal Price is displayed");
		}
		else {
			fail("Cart Product SubTotal Price is not displayed");
		}
	}
	
	
	public void verifyCartRemoveButton()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartRemoveButton, 35, "Cart Remove Button "))
		{	
			pass("Cart Remove Button is displayed");
		}
		else {
			fail("Cart Remove Button is not displayed");
		}
	}
	
	public void clickCartRemoveButton()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartRemoveButton, 35, "Cart Remove Button "))
		{	
			pass("Cart Remove Button is displayed");
			if(driver.jsClickElement(CartDetailPage.customerCartRemoveButton, "Cart Remove")) {
				pass("Cart Remove Button is Clicked");
			}
			else {
				fail("Cart Remove Button is not Clicked");
			}
		}
		else {
			fail("Cart Remove Button is not displayed");
		}
	}
	
	public void verifyCartRemoveSuccessMessage()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartRemoveSuccess, 35, "Cart Remove Success "))
		{	
			pass("Cart Remove Success Message is displayed");
		}
		else {
			fail("Cart Remove Success Message is not displayed");
		}
	}
	
	public void verifyCartRemoveFailureMessage()
	{
		if(driver.explicitWaitforVisibility(CartDetailPage.customerCartRemoveFailure, 35, "Cart Remove Failure "))
		{	
			pass("Cart Remove Failure Message is displayed");
		}
		else {
			fail("Cart Remove Failure Message is not displayed");
		}
	}
	
}
