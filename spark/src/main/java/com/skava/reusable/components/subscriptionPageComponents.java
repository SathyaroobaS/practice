package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.skava.object.repository.subscriptionPage;

public class subscriptionPageComponents extends customerLandingPageComponents
{
	
	
	public void subscriptionListPageVerification() {
		if(driver.explicitWaitforVisibility(subscriptionPage.subscriptionListHeading, Element_Wait_Time, "SubscriptionAdminHeading")) {
			pass("Subscription List Page Displayed Successfully ");
			
		}
		else {
			fail("Subscription List Page Not Displayed ");
		}
	}
	
	
	public void subscriptionAdminVerification() {
		if(driver.explicitWaitforVisibility(subscriptionPage.subscriptionHeading, Element_Wait_Time, "SubscriptionListHeading")) {
			pass("Subscription Admin Page Displayed Successfully ");
			
		}
		else {
			fail("Subscription Admin Page Not Displayed ");
		}
	}
	
	
	public void subscriptionListHeadingVerification() {
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionId, Element_Wait_Time, "Subscription Id"))
			pass("Subscription Id Heading is Displayed");
		else
			fail("Subscription Id Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionSkuId, Element_Wait_Time, "Subscription Sku Id"))
			pass("Subscription Sku Id Heading is Displayed");
		else
			fail("Subscription Sku Id Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionSkuName, Element_Wait_Time, "Subscription Sku Name"))
			pass("Subscription Sku Name Heading is Displayed");
		else
			fail("Subscription Sku Name Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionRequestDate, Element_Wait_Time, "Subscription Request Date"))
			pass("Subscription Request Date Heading is Displayed");
		else
			fail("Subscription Request Date Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionUserName, Element_Wait_Time, "Subscription User Name"))
			pass("Subscription User Name Heading is Displayed");
		else
			fail("Subscription User Name Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionEmail, Element_Wait_Time, "Subscription Email"))
			pass("Subscription Email Heading is Displayed");
		else
			fail("Subscription Email Heading is Not Displayed");
		
		if (driver.explicitWaitforVisibility(subscriptionPage.subscriptionFrequency, Element_Wait_Time, "Subscription Frequency"))
			pass("Subscription Frequency Heading is Displayed");
		else
			fail("Subscription Frequency Heading is Not Displayed");
		
	}
	
	
	
	
	public void subscriptionListSize() {
		if(driver.explicitWaitforVisibility(subscriptionPage.subscriptionRowTrigger, Element_Wait_Time, "SubscriptionListRow")) {
			pass("Subscription List Row is Displayed ");
		int suscriptionListSize = driver.getSize(subscriptionPage.subscriptionRowTrigger, "Subscription List Row");
		
		if(suscriptionListSize>0) {
			pass("Customer Subscription List is Available ");
		}
		
		else {
			fail("Customer Subscription List is Not Available");
		}
		
		}
		else {
			fail("Subscription Admin Page Not Displayed ");
		}
	}
	
	
	
	public void searchSubscriptionId(String subId) {
		if(driver.explicitWaitforClickable(subscriptionPage.subIDSearchClick, Element_Wait_Time, "SubscriptionId Search")) {
			pass("Subscription Id Search is Displayed");
			driver.jsClickElement(subscriptionPage.subIDSearchClick, "SubscriptionId Search");
		if(driver.explicitWaitforVisibility(subscriptionPage.subIDSearchClick, Element_Wait_Time, "SubscriptionId Search Input")) {
			pass("Subscription Id Search Input is Displayed");
			driver.enterText(subscriptionPage.subIDSearchClick, subId, "SubscriptionId Search Input");
		}
		else
			fail("Subscription Id Search Input is Not Displayed");
		}
		else
			fail("Subscription Id Search is Not Displayed");
	}
	
	
	
	public void searchSubscriptionEmail(String Email) {
		if(driver.explicitWaitforClickable(subscriptionPage.subEmailSearchClick, Element_Wait_Time, "Subscription Email Search")) {
			pass("Subscription Email Search is Displayed");
			driver.jsClickElement(subscriptionPage.subEmailSearchClick, "Subscription Email Search");
		if(driver.explicitWaitforVisibility(subscriptionPage.subEmailSearchInput, Element_Wait_Time, "Subscription Email Search Input")) {
			pass("Subscription Email Search Input is Displayed");
			driver.enterText(subscriptionPage.subEmailSearchInput, Email, "Subscription Email Search Input");
		}
		else
			fail("Subscription Email Search Input is Not Displayed");
		}
		else
			fail("Subscription Email Search is Not Displayed");
	}
	
	public void searchSubscriptionSkuId(String SkuId) {
		if(driver.explicitWaitforClickable(subscriptionPage.subSkuIdSearchClick, Element_Wait_Time, "Subscription SkuId Search")) {
			pass("Subscription SkuId Search is Displayed");
			driver.jsClickElement(subscriptionPage.subSkuIdSearchClick, "Subscription SkuId Search");
		if(driver.explicitWaitforVisibility(subscriptionPage.subSkuIdSearchInput, Element_Wait_Time, "Subscription SkuId Search Input")) {
			pass("Subscription SkuId Search Input is Displayed");
			driver.enterText(subscriptionPage.subSkuIdSearchInput, SkuId, "Subscription SkuId Search Input");
		}
		else
			fail("Subscription SkuId Search Input is Not Displayed");
		}
		else
			fail("Subscription SkuId Search is Not Displayed");
	}
	
	
	public void searchSubscriptionSkuName(String SkuName) {
		if(driver.explicitWaitforClickable(subscriptionPage.subSkuNameSearchClick, Element_Wait_Time, "Subscription SkuName Search")) {
			pass("Subscription SkuName Search is Displayed");
			driver.jsClickElement(subscriptionPage.subSkuNameSearchClick, "Subscription SkuName Search");
		if(driver.explicitWaitforVisibility(subscriptionPage.subSkuNameSearchInput, Element_Wait_Time, "Subscription SkuName Search Input")) {
			pass("Subscription SkuName Search Input is Displayed");
			driver.enterText(subscriptionPage.subSkuNameSearchInput, SkuName, "Subscription SkuName Search Input");
		}
		else
			fail("Subscription SkuName Search Input is Not Displayed");
		}
		else
			fail("Subscription SkuName Search is Not Displayed");
	}
	
	
	
	public void selectLeftYear(String year) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, Element_Wait_Time, "Subscription Year")) {
			pass("Subscription Year Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription Year");
			driver.selectByValue(subscriptionPage.subLeftYearClick, year, "Subscription Year");
			pass("Subscription Year is Clicked");
		}
		else
			fail("Subscription Year Element is Not Displayed");
	}
	
	
	public void selectRightYear(String year) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, Element_Wait_Time, "Subscription Year")) {
			pass("Subscription Year Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription Year");
			driver.selectByValue(subscriptionPage.subRightYearClick, year, "Subscription Year");
			pass("Subscription Year is Clicked");
		}
		else
			fail("Subscription Year Element is Not Displayed");
	}
	
	
	public void selectLeftMonth(String month) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, Element_Wait_Time, "Subscription month")) {
			pass("Subscription month Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription month");
			driver.selectByValue(subscriptionPage.subLeftMonthClick, month, "Subscription month");
			pass("Subscription month is Clicked");
		}
		else
			fail("Subscription month Element is Not Displayed");
	}
	
	
	public void selectRightMonth(String month) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, Element_Wait_Time, "Subscription month")) {
			pass("Subscription month Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription month");
			driver.selectByValue(subscriptionPage.subRightMonthClick, month, "Subscription month");
			pass("Subscription month is Clicked");
		}
		else
			fail("Subscription month Element is Not Displayed");
	}
	
	
	
	public void selectLeftDate(String date) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, Element_Wait_Time, "Subscription Date")) {
			pass("Subscription Date Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription date");
			String a ="//*[@class='drp-calendar left']//child::tbody//td[contains(@data-title,'r')][text()='"+date+"']";
			System.out.println(a);
			driver.clickElement(By.xpath(subscriptionPage.subLeftDateSelect.toString().replace("By.xpath: ", "").concat("'" +date+ "']")),"Subscription Date");
			//driver.clickElement(By.xpath("//*[@class='drp-calendar left']//child::tbody//td[contains(@data-title,'r')][text()='"+date+"']"),"Subscription Date");
			pass("Subscription Date is Clicked");
		}
		else
			fail("Subscription Date Element is Not Displayed");
	}
	
	
	
	public void selectRightDate(String date) {
		if(driver.explicitWaitforClickable(subscriptionPage.subRDClick, Element_Wait_Time, "Subscription Date")) {
			pass("Subscription Date Element is Displayed");
			driver.jsClickElement(subscriptionPage.subRDClick, "Subscription date");
			driver.clickElement(By.xpath(subscriptionPage.subRightDateSelect.toString().replace("By.xpath: ", "").concat("'" +date+ "']")),"Subscription Date");
			pass("Subscription Date is Clicked");
		}
		else
			fail("Subscription Date Element is Not Displayed");
	}
	
	
	public void cancelSubscription() {
		if(driver.explicitWaitforClickable(subscriptionPage.subCancelBtn6, Element_Wait_Time, "Subscription Cancel")) {
			pass("Subscription Cancel Button is Displayed");
			driver.jsClickElement(subscriptionPage.subCancelBtn6, "Subscription Cancel");
			pass("Subscription Cancel Button is Clicked");
		if(driver.explicitWaitforClickable(subscriptionPage.subConfirmBtn, Element_Wait_Time, "Subscription Confirm")) {
			pass("Subscription Confrimation Button is Displayed");
			driver.jsClickElement(subscriptionPage.subConfirmBtn, "Subscription Confirm");
		
		if(driver.explicitWaitforVisibility(subscriptionPage.subCancelConfirm, Element_Wait_Time, "Subscription Confirm")) {
			String status = driver.getText(subscriptionPage.subCancelConfirm, "Cancel Confirm");
			if(status.equalsIgnoreCase("CANCELED")) {
			pass("Subscription Status Changed to Cancel");
			}
		}
		
		else
			fail("Subscription Status Not Changed");
		}
		else
			fail("Subscription Confrimation Button is Not Displayed");
		}
		else
			fail("Subscription Cancel Button is Not Displayed");
	}
	

}













































