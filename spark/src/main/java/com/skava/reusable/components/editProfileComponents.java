package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.UserProfilePage;
import com.skava.object.repository.customerLandingPage;
import com.skava.object.repository.orderPage;
import com.skava.object.repository.paymentPage;

public class editProfileComponents extends paymentPageComponents
{
	public void clickEditProfile() throws InterruptedException
	{
		Thread.sleep(8000);
		if(driver.explicitWaitforVisibility(UserProfilePage.editProfileTrigger, 60, "edit profile "))
		{	
			pass("Edit Profile Button is displayed");
			if(driver.jsClickElement(UserProfilePage.editProfileTrigger, "edit profile")) {
				pass("Edit Profile Button is Clicked");}
			else {
				fail("Edit Profile Button is not Clicked");}
		}
		else {
			fail("Edit Profile Button is not displayed");}
	}
	
	public void changeStatusFreeze()
	{
		driver.jsClickElement(UserProfilePage.changeStatusFreeze1, "status");
		int size = driver.getSize(UserProfilePage.changeStatusFreeze2,"status");
		for(int i=1;i<=size;i++)
		{	
			//String text = driver.getText(By.xpath("((//*[contains (@class,'cls_profile_mdb-select')])[3]//li)["+i+"]"), "status");
			String text = driver.getText(By.xpath(UserProfilePage.changeStatusFreeze5.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"status");
			if(text.equalsIgnoreCase("freeze"))
			{		
				//if(driver.jsClickElement(By.xpath("((//*[contains (@class,'cls_profile_mdb-select')])[3]//li)["+i+"]//span"), "status"))
				if(driver.jsClickElement(By.xpath(UserProfilePage.changeStatusFreeze5.toString().replace("By.xpath: ", "").concat("[" +i+ "]//span")), "status")) {
					pass("Freeze is clicked");}
				else {
					fail("Freeze is not clicked");}
			}
		}

	}
	
	public void changeStatusunFreeze()
	{
		driver.jsClickElement(UserProfilePage.changeStatusFreeze3, "status");
		int size = driver.getSize(UserProfilePage.changeStatusFreeze4,"status");
		for(int i=1;i<=size;i++)
		{	
			//String text = driver.getText(By.xpath("((//*[contains (@class,'cls_profile_mdb-select')])[3]//li)["+i+"]"), "status");
			String text = driver.getText(By.xpath(UserProfilePage.changeStatusFreeze5.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"status");
			if(text.equalsIgnoreCase("active"))
			{		
				//if(driver.jsClickElement(By.xpath("((//*[contains (@class,'cls_profile_mdb-select')])[3]//li)["+i+"]//span"), "status"))
				if(driver.jsClickElement(By.xpath(UserProfilePage.changeStatusFreeze5.toString().replace("By.xpath: ", "").concat("[" +i+ "]//span")), "status")) {
					pass("unFreeze is clicked");}
				else {
					fail("unFreeze is not clicked");}
			}
		}
	}
	
	public void changeStatusDelete()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.deleteAccountTrigger, 35, "status"))
		{	
			if(driver.jsClickElement(UserProfilePage.deleteAccountTrigger, "status")) {
				pass("Delete Button is clicked");}
			else {
				fail("Delete Button is not clicked");}
		}
		

	}
	public void clickSaveChangesButton()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.saveTrigger, 35, "status"))
		{
			if(driver.clickElement(UserProfilePage.saveTrigger, "status")) {
				pass("Save Changes is clicked");}
			else {
				fail("Save Changes is not clicked");}
		}
	}
	
	public void clickSaveChangesButton1()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.saveTrigger1, 35, "status"))
		{
			if(driver.clickElement(UserProfilePage.saveTrigger1, "status")) {
				pass("Save Changes is clicked");}
			else {
				fail("Save Changes is not clicked");}
		}
	}
	
	public void verifySuccessTxt()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.saveSuccess, 35, "success")) {
			pass("Status changes updated successfully");}
		else {
			fail("Status changes not updated");}
	}
	public void verifySuccessTxt1()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.verifySuccessTxt1, 35, "success")) {
			pass("Status changes updated successfully");}
		else {
			fail("Status changes not updated");}
	}
	
	public void searchandclickFrozenCustomers() throws InterruptedException
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.searchandclickFrozenCustomers1, 45, "search"))
		{
			driver.jsClickElement(UserProfilePage.searchandclickFrozenCustomers2, "search");
			int size = driver.getSize(UserProfilePage.searchandclickFrozenCustomers3, "search");
			for(int i = 1;i<=size;i++)
			{
				//String txt = driver.getText(By.xpath("(//*[@data-qa='status-search']//following-sibling::div//li)["+i+"]"), "frozen");
				String txt = driver.getText(By.xpath(UserProfilePage.searchandclickFrozenCustomers7.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"frozen");
				if(txt.equalsIgnoreCase("frozen"))
				{
					//if(driver.jsClickElement(By.xpath("(//*[@data-qa='status-search']//following-sibling::div//li)["+i+"]"), "frozen"))
					if(driver.jsClickElement(By.xpath(UserProfilePage.searchandclickFrozenCustomers7.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"frozen")) {
						pass("Frozen is selected");}
					else {
						fail("Frozen is not selected");}
				}
			}
			if(driver.explicitWaitforVisibility(UserProfilePage.searchandclickFrozenCustomers4, 35, "search")) {
			if(driver.jsClickElement(UserProfilePage.searchandclickFrozenCustomers4, "search btn")) {
				pass("Search Button is Clicked");}
			else {
				fail("Search Button is not clicked");}
			}
			Thread.sleep(8000);
			if(driver.explicitWaitforVisibility(UserProfilePage.searchandclickFrozenCustomers5, 45, "search"))
			{	
				if(driver.jsClickElement(UserProfilePage.searchandclickFrozenCustomers6, "view profile button")) {
					pass("View Button is Clicked");}
				else {
					fail("View Button is not Clicked");}
			}
		}
			
	}
	
	public void clickFrozenCust() {
		if(driver.explicitWaitforVisibility(UserProfilePage.selectFreezeStatus1, 35, "search"))
		{	
			if(driver.jsClickElement(UserProfilePage.selectFreezeStatus8, "view profile button")) {
				pass("View Button is Clicked");}
			else {
				fail("View Button is not Clicked");}
		}
	}
	
	public void editNotDisplayed()
	{
		if(driver.explicitWaitforInVisibility(paymentPage.editBtn, 35, "edit btn")) {
			pass("Edit is not displayed");}
		else {
			fail("Edit is displayed");}
	}
	
	public void deletenotDisplayed()
	{
		if(driver.explicitWaitforInVisibility(paymentPage.deleteBtn, 35, "delete btn")) {
			pass("delete not dispplayed");}
		else {
			fail("delete is displayed");}
	}
	
	public void verifyfreezePopupTxt()
	{
		String txt = driver.getText(UserProfilePage.deletePopUp, "popup txt");
		if(txt.equals("Please note that once you freeze customer's account, they will not be able to login to the account. To unfreeze the account, please inform the customer that they need to contact customer service.")) {
			pass("appropriate txt is displayed");}
		else {
			fail("appropriate txt is not displayed");}
	}
	
	public void verifydeletePopupTxt()
	{
		String txt = driver.getText(UserProfilePage.deletePopUp, "popup txt");
		if(txt.equals("Please note that this an irreversible action. Are you sure you want to delete this customer account?")) {
			pass("appropriate txt is displayed");}
		else {
			fail("appropriate txt is not displayed");}
	}
	
	public void makedefaultnotdisplayed()
	{
		if(driver.explicitWaitforInVisibility(paymentPage.makeDefaultBtn, 35, "make default")) {
			pass("Make default is not displayed");}
		else {
			fail("Make default is displayed");}
	}
	
	public void clickFreezePopupBtn()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.clickFreezePopupBtn1, 35, "freeze button"))
		{	
			if(driver.jsClickElement(UserProfilePage.clickFreezePopupBtn1, "freeze button")) {
				pass("freeze button is clicked");}
			else {
				fail("freeze button is not clicked");}
		}
		else {
			fail("freeze button is not displayed");}
	}
	
	public void clickDeletePopupBtn()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.clickDeletePopupBtn1, 35, "delete button"))
		{	
			if(driver.jsClickElement(UserProfilePage.clickDeletePopupBtn1, "delete button")) {
				pass("delete button is clicked");}
			else {
				fail("delete button is not clicked");}
		}
		else {
			fail("delete button is not displayed");}
	}
	
	public void verifyCustomerTitle()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.verifyCustomerTitle1, 35, "Customer Title")) {
			pass("Customer Title is displayed");}
		else {
			fail("Customer Title is not displayed");}
	}
	
	public String getProfileName()
	{
		String txt = "";
		if(driver.explicitWaitforVisibility(UserProfilePage.getProfileName1, 35, "id"))
		{
			txt = driver.getText(UserProfilePage.getProfileName1, "id");
			
		}
		return txt;
	}
	
	public void searchDeleteCustomer(String txt)
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.searchDeleteCustomer1, 35, "search"))
		{
			driver.jsClickElement(UserProfilePage.searchDeleteCustomer1, "search");
			if(driver.enterText(UserProfilePage.searchDeleteCustomer2, txt, "name search")) {
				pass("Name is Entered");}
			else {
				fail("Name is not Entered");}
		}
		if(driver.explicitWaitforVisibility(UserProfilePage.searchDeleteCustomer3, 35, "searchBtn")) {
		if(driver.jsClickElement(UserProfilePage.searchDeleteCustomer3, "search btn")) {
			pass("Search Button is Clicked");}
		else {
			fail("Search Button is not clicked");}
		}
	}
	
	public void noDataFound()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.noDataFound1, 35, "no data found")) {
			pass("No data Found - account is deleted");}
		else {
			fail("Account is not deleted");}
	}
	
	public void verifyFrozenTxt()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.verifyFrozenTxt1, 35, "frozen"))
		{
			String txt = driver.getText(UserProfilePage.verifyFrozenTxt1, "frozen");
			if(txt.equalsIgnoreCase("FROZEN")) {
				pass("Text is displayed as Frozen");}
			else {
				fail("Text is not displayed as Frozen");}
		}
	}
	
	public void  searchaFrozenCustomers()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.searchaFrozenCustomers1, 35, "search"))
		{
			driver.jsClickElement(UserProfilePage.searchaFrozenCustomers1, "search");
			int size = driver.getSize(UserProfilePage.searchaFrozenCustomers2, "search");
			for(int i = 1;i<=size;i++)
			{
				//String txt = driver.getText(By.xpath("(//*[@data-qa='status-search']//following-sibling::div//li)["+i+"]"), "frozen");
				String txt = driver.getText(By.xpath(UserProfilePage.searchandclickFrozenCustomers7.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"frozen");
				if(txt.equalsIgnoreCase("frozen"))
				{
					//if(driver.jsClickElement(By.xpath("(//*[@data-qa='status-search']//following-sibling::div//li)["+i+"]"), "frozen"))
					if(driver.jsClickElement(By.xpath(UserProfilePage.searchandclickFrozenCustomers7.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"frozen")) {
						pass("Frozen is selected");}
					else {
						fail("Frozen is not selected");}
				}
			}
			if(driver.jsClickElement(UserProfilePage.searchaFrozenCustomers3, "search btn")) {
				pass("Search Button is Clicked");}
			else {
				fail("Search Button is not clicked");}
		}
			
	}
	
	public void updatePhoneNumber()
	{
		if(driver.explicitWaitforVisibility(UserProfilePage.phoneNumberInput, 35, "phone number"))
		{
			driver.clearText(UserProfilePage.phoneNumberInput, 35, "phone number");
			if(driver.enterText(UserProfilePage.phoneNumberInput, ExcelReader.getData("Keywords", "Keyword_1"), "phone number")) {
				pass("Phone number is entered");}
			else {
				fail("Phone number is not entered");}
		}
	}
	
	public void validateInlineError()
	{
		String txt = driver.getText(UserProfilePage.inlineError, "inline error");
		if(txt.equals("Please enter a valid phone number")) {
			pass("Inline error is shown");}
		else {
			fail("Inline error is not shown");}

	}
	
	public void selectFreezeStatus() throws InterruptedException {
		if(driver.explicitWaitforVisibility(UserProfilePage.selectFreezeStatus1, 35, "search"))
		{	
			if(driver.jsClickElement(UserProfilePage.selectFreezeStatus8, "view profile button")) {
				pass("View Button is Clicked");}
			else {
				fail("View Button is not Clicked");}
		}
		else {
			fail("Search Button is not displayed");
		}
		
		Thread.sleep(8000);
		if(driver.explicitWaitforVisibility(UserProfilePage.editProfileTrigger, 50, "edit profile "))
		{	
			pass("Edit Profile Button is displayed");
			if(driver.jsClickElement(UserProfilePage.editProfileTrigger, "edit profile")) {
				pass("Edit Profile Button is Clicked");}
			else {
				fail("Edit Profile Button is not Clicked");}
		}
		else {
			fail("Edit Profile Button is not displayed");}
		
		driver.jsClickElement(UserProfilePage.selectFreezeStatus3, "status");
		//driver.clickElement(By.xpath("(//*[@data-qa='status-container']//input"), "statusDropdown");
		//int size = driver.getSize(By.xpath("(//*[contains (@class,'cls_profile_mdb-select')])[3]//li"),"status");
		
		int size = driver.getSize(UserProfilePage.selectFreezeStatus4,"statusSize");
		
		for(int i=1;i<=size;i++)
		{	
			//String text = driver.getText(By.xpath("//*[@data-qa='status-container']//ul/li["+i+"]"), "statusName");
			String text = driver.getText(By.xpath(UserProfilePage.selectFreezeStatus5.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"statusName");
			if(text.equalsIgnoreCase("freeze"))
			{		
				//if(driver.jsClickElement(By.xpath("//*[@data-qa='status-container']//ul/li["+i+"]//span"), "statusSelect"))
				if(driver.jsClickElement(By.xpath(UserProfilePage.selectFreezeStatus5.toString().replace("By.xpath: ", "").concat("[" +i+ "]//span")),"statusSelect")) {
					pass("Freeze is clicked");}
				else {
					fail("Freeze is not clicked");}
			}
			
		}
		
		
		
		if(driver.explicitWaitforVisibility(UserProfilePage.saveTrigger1, 35, "status"))
		{
			if(driver.clickElement(UserProfilePage.saveTrigger1, "status")) {
				pass("Save Changes is clicked");}
			else {
				fail("Save Changes is not clicked");}
		}
		
		if(driver.explicitWaitforVisibility(UserProfilePage.FreeeAccountButton, 35, "freezeAccount"))
		{
			if(driver.clickElement(UserProfilePage.FreeeAccountButton, "freezeAccount")) {
				pass("Freeze Account button is clicked");}
			else {
				fail("Freeze Account button is not clicked");}
		}
		
		if(driver.explicitWaitforVisibility(UserProfilePage.customerBreadcrumbButton, 35, "status"))
		{
			if(driver.jsClickElement(UserProfilePage.customerBreadcrumbButton, "status")) {
				pass("BusinessName breadcrumb is clicked");}
			else {
				fail("BusinessName breadcrumb is not clicked");}
		}
	}
	
	public void selectViewProfButton() {
		if(driver.explicitWaitforVisibility(UserProfilePage.selectViewProfButton1, 35, "search"))
		{	
			if(driver.jsClickElement(UserProfilePage.selectViewProfButton2, "view profile button")) {
				pass("View Button is Clicked");}
			else {
				fail("View Button is not Clicked");}
		}
	}
	
	public void selectFreezeToActiveStatus() {
		/*if(driver.explicitWaitforVisibility(By.xpath("//*[@data-qa='view-profile-trigger']"), 120, "search"))
		{	
			if(driver.jsClickElement(By.xpath("(//*[@data-qa='view-profile-trigger'])[1]"), "view profile button"))
				pass("View Button is Clicked");
			else
				fail("View Button is not Clicked");
		}*/
		
		if(driver.explicitWaitforVisibility(UserProfilePage.editProfileTrigger, 35, "edit profile "))
		{	
			pass("Edit Profile Button is displayed");
			if(driver.jsClickElement(UserProfilePage.editProfileTrigger, "edit profile")) {
				pass("Edit Profile Button is Clicked");}
			else {
				fail("Edit Profile Button is not Clicked");}
		}
		else {
			fail("Edit Profile Button is not displayed");}
		
		driver.jsClickElement(UserProfilePage.selectFreezeToActiveStatus1, "status");
		//driver.clickElement(By.xpath("(//*[@data-qa='status-container']//input"), "statusDropdown");
		//int size = driver.getSize(By.xpath("(//*[contains (@class,'cls_profile_mdb-select')])[3]//li"),"status");
		
		int size = driver.getSize(UserProfilePage.selectFreezeToActiveStatus2,"statusSize");
		
		for(int i=1;i<=size;i++)
		{	
			//String text = driver.getText(By.xpath("//*[@data-qa='status-container']//ul/li["+i+"]"), "statusName");
			String text = driver.getText(By.xpath(UserProfilePage.selectFreezeStatus5.toString().replace("By.xpath: ", "").concat("[" +i+ "]")),"statusName");
			if(text.equalsIgnoreCase("active"))
			{		
				//if(driver.jsClickElement(By.xpath("//*[@data-qa='status-container']//ul/li["+i+"]//span"), "statusSelect"))
				if(driver.jsClickElement(By.xpath(UserProfilePage.selectFreezeStatus5.toString().replace("By.xpath: ", "").concat("[" +i+ "]//span")),"statusSelect")) {
					pass("Freeze is clicked");}
				else {
					fail("Freeze is not clicked");}
			}
		}
		
		
		
		if(driver.explicitWaitforVisibility(UserProfilePage.saveTrigger1, 35, "status"))
		{
			if(driver.clickElement(UserProfilePage.saveTrigger1, "status")) {
				pass("Save Changes is clicked");}
			else {
				fail("Save Changes is not clicked");}
		}
		
		/*if(driver.explicitWaitforVisibility(UserProfilePage.FreeeAccountButton, 60, "freezeAccount"))
		{
			if(driver.clickElement(UserProfilePage.FreeeAccountButton, "freezeAccount"))
				pass("Freeze Account button is clicked");
			else
				fail("Freeze Account button is not clicked");
		}*/
		
		if(driver.explicitWaitforVisibility(UserProfilePage.customerBreadcrumbButton, 35, "status"))
		{
			if(driver.jsClickElement(UserProfilePage.customerBreadcrumbButton, "status")) {
				pass("BusinessName breadcrumb is clicked");}
			else {
				fail("BusinessName breadcrumb is not clicked");}
		}
	}
	
}