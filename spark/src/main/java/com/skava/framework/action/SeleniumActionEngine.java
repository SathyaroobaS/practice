package com.skava.framework.action;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.framework.reporting.BaseClass;
import com.framework.reporting.ExtentManager;
import com.framework.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.LogStatus;
import com.skava.frameworkutils.Constants;
import com.skava.frameworkutils.loggerUtils;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.CaptureType;

public class SeleniumActionEngine implements ActionEngine 

{
	private WebDriver driver;
	private static ChromeDriverService chromeDriverService;
	
	public static final String saucelabAPI_Web = "http://"+BaseClass.properties.getProperty("SaucelabUserName")+":"+BaseClass.properties.getProperty("SaucelabAccessKey")+"@ondemand.saucelabs.com:80/wd/hub";
	public static final String saucelabAPI_US = "https://us1.appium.testobject.com/wd/hub";
	public static final String saucelabAPI_EU = "https://eu1.appium.testobject.com/wd/hub";
	ArrayList<String> tabs;

	private static int port=System.getProperty("port")==null?8081:Integer.parseInt(System.getProperty("port"));
	
	//private int port = 2105;
	
	public BrowserMobProxy proxy = new BrowserMobProxyServer();
	
	public SeleniumActionEngine(int browserType) 
	{
		init(browserType);
	}

	private void init(int browserType) 
	{
		String testCaseName = Thread.currentThread().getStackTrace()[5].getClassName().substring(20);
		String strBrowserName=(String) BaseClass.json.getJSONArray(testCaseName).get(2);
		String strBrowserVersion=(String) BaseClass.json.getJSONArray(testCaseName).get(3);
		String strDeviceName=(String) BaseClass.json.getJSONArray(testCaseName).get(5);
		String strDeviceOS=(String) BaseClass.json.getJSONArray(testCaseName).get(6);
		
		DesiredCapabilities caps;
		
		switch (browserType) 
		{
			case Constants.LOCAL_BROWSER_FIREFOX:
				System.setProperty("webdriver.gecko.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"geckodriver.exe");
				FirefoxOptions fOptions=new FirefoxOptions();
				if(BaseClass.properties.getProperty("RunWithHeadless").equalsIgnoreCase("Yes"))
				{
					fOptions.addArguments("--headless");
				}
				
				if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
                {
	             // start the proxy
					////System.out.println(port);
					synchronized (SeleniumActionEngine.class) {						
						proxy.start(port);
						port++;
					}
					 // get the Selenium proxy object
				    Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
				    fOptions.setCapability(CapabilityType.PROXY, seleniumProxy);
                }
				
				this.driver = new FirefoxDriver(fOptions);
				
				break;
			
			case Constants.LOCAL_BROWSER_CHROME:
				
				/*if("Linux".equalsIgnoreCase(System.getProperty("os.name")))
	        	{				
				   	System.setProperty("webdriver.chrome.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"chromedriverlnx");
	        	}
			    else
			    {
			    	System.setProperty("webdriver.chrome.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"chromedriver.exe");
			    }*/
			    
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("useAutomationExtension", false);
				options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));
				options.addArguments("--disable-gpu");
				options.addArguments("--no-sandbox");
				options.addArguments("--proxy-server='direct://'");
				options.addArguments("--proxy-bypass-list=*");
				
				// save password notification handle
				
                if(BaseClass.properties.getProperty("RunWithHeadless").equalsIgnoreCase("Yes"))
				{
                	options.addArguments("--headless");
					
				}
                
                if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
                {
	             // start the proxy
					////System.out.println(port);
					//proxy.start(port);
                	proxy.start(); 
                	synchronized (SeleniumActionEngine.class) {						
                		port++;
					}
					////System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Run with proxy port increment : " + port);
					 // get the Selenium proxy object
				    Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
				    options.setCapability(CapabilityType.PROXY, seleniumProxy);
                }
        		
                /* ChromeDriverService service=ChromeDriverService.
                  service.
                this.driver = new ChromeDriver(options);
                ChromeDriverService service = null;
                ChromeDriverService.Builder serviceBuilder = null;
			
				try {
					service = new ChromeDriverService(new File(System.getProperty("webdriver.chrome.driver")), 8888,null,null);
				} catch (IOException e1) {
					TODO Auto-generated catch block
					e1.printStackTrace();
				}
                
                serviceBuilder = new ChromeDriverService.Builder();
            	serviceBuilder.usingPort(port++);
            	service = serviceBuilder.build();
                
               	this.driver=new ChromeDriver(service, options);
                this.driver.manage().window().setSize(new Dimension(1300, 900));*/
                
                
                //===============
                // Port Issue fix
                //===============
                
                if("Linux".equalsIgnoreCase(System.getProperty("os.name")))
	        	{				
                	 if(chromeDriverService == null){
                		 synchronized (SeleniumActionEngine.class) {						
                			 ChromeDriverService.Builder serviceBuilder = new ChromeDriverService.Builder()
                					 .usingDriverExecutable(new File(BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"chromedriverlnx"))
                					 .usingPort(port++);
                			 chromeDriverService = serviceBuilder.build();
                			 try {
                				 chromeDriverService.start();
                			 }
                			 catch(IOException ioe) {
                				 ////System.out.println("Error: Unable to start chrome dirver service");
                			 }
						}
                		 ////System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!Linux port increment : " + port);
					 }
                	 ////System.out.println("!!!!!!!!!!!!!!!!!!!!!!Initiating chrome drive !!!!!!!!!!!!!!!!");
                	 options.addArguments("--disable-dev-shm-usage");
                   this.driver = new RemoteWebDriver(chromeDriverService.getUrl(), options);
                	// this.driver = new ChromeDriver(chromeDriverService,options);
                             
	        	}
			    else
			    {
			    	if(chromeDriverService==null){
				    	synchronized (SeleniumActionEngine.class) {
				    	 ChromeDriverService.Builder serviceBuilder = new ChromeDriverService.Builder()
			                        .usingDriverExecutable(new File(BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"chromedriver.exe"))
			                        .usingPort(port++);
				    	 chromeDriverService = serviceBuilder.build();
				    	 try {
            				 chromeDriverService.start();
            			 }
            			 catch(IOException ioe) {
            				 ////System.out.println("Error: Unable to start chrome dirver service");
            			 }
				    	}
			    	}
			    	
			    	//this.driver = new ChromeDriver(chromeDriverService, options);
			    	this.driver = new RemoteWebDriver(chromeDriverService.getUrl(), options);
			    }
               
                this.driver.manage().deleteAllCookies();
            break;
				
			case Constants.LOCAL_BROWSER_IE:
				System.setProperty("webdriver.ie.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"IEDriverServer.exe");
				InternetExplorerOptions ieOption=new InternetExplorerOptions();
				ieOption.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				
				this.driver = new InternetExplorerDriver(ieOption);
				
				break;
				
			case Constants.LOCAL_BROWSER_SAFARI:
				this.driver = new SafariDriver();
				
				break;
				
			case Constants.LOCAL_BROWSER_EDGE:
				System.setProperty("webdriver.edge.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"MicrosoftEdgeDriver.exe");
				this.driver = new EdgeDriver();
				break;
				
			case Constants.SAUCE_DESKTOP_BROWSER_CHROME:
				caps = DesiredCapabilities.chrome();
				caps.setCapability("platform", strDeviceOS.trim());
				caps.setCapability("version", strBrowserVersion.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;	
				
			case Constants.SAUCE_DESKTOP_BROWSER_FIREFOX:
				caps = DesiredCapabilities.firefox();
				caps.setCapability("platform", strDeviceOS.trim());
				caps.setCapability("version", strBrowserVersion.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.SAUCE_DESKTOP_BROWSER_IE:
				caps = DesiredCapabilities.internetExplorer();
				caps.setCapability("platform", strDeviceOS.trim());
				caps.setCapability("version", strBrowserVersion.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.SAUCE_DESKTOP_BROWSER_EDGE:
				caps = DesiredCapabilities.edge();
				caps.setCapability("platform", strDeviceOS.trim());
				caps.setCapability("version", strBrowserVersion.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.SAUCE_DESKTOP_BROWSER_SAFARI:
				caps = DesiredCapabilities.safari();
				caps.setCapability("platform", strDeviceOS.trim());
				caps.setCapability("version", strBrowserVersion.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
			
			case Constants.SAUCE_MOBILE_ANDROID_BROWSER_CHROME:
				caps = new DesiredCapabilities();
				caps.setCapability("testobjectApiKey", BaseClass.properties.getProperty("TestobjectApiKey"));
				caps.setCapability("platformName", "Android");
				caps.setCapability("platformVersion", strDeviceOS.replace("Android ", ""));
				caps.setCapability("deviceName", strDeviceName);
				caps.setCapability("privateDevicesOnly", "false	");
				caps.setCapability("deviceType", "mobile");
				caps.setCapability("testobject_app_id", "1");
				caps.setCapability("browserName", "Chrome");
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				caps.setCapability("testobject_session_creation_timeout", "900000");
				caps.setCapability("noReset", "false");
				caps.setCapability("appiumVersion", "1.7.1");
				try 
				{
					if(BaseClass.properties.getProperty("RealDeviceDataCenter").equalsIgnoreCase("US"))
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_US), caps);
					else
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_EU), caps);
				}
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}   
				break;
				
			case Constants.SAUCE_MOBILE_EMULATOR_ANDROID_BROWSER_CHROME:
				caps = DesiredCapabilities.android();
				caps.setCapability("appiumVersion", "1.7.1");
				caps.setCapability("deviceName",strDeviceName);
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("deviceType", "mobile");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("platformVersion",strDeviceOS.replace("Android ", ""));
				caps.setCapability("platformName","Android");
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				  
			case Constants.SAUCE_MOBILE_IOS_BROWSER_SAFARI:
				caps = new DesiredCapabilities();
				caps.setCapability("testobjectApiKey", BaseClass.properties.getProperty("TestobjectApiKey"));
				caps.setCapability("platformName", "iOS");
				caps.setCapability("platformVersion", strDeviceOS.replace("iOS ", ""));
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("deviceName", strDeviceName);
				caps.setCapability("testobject_app_id", "1");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				caps.setCapability("testobject_session_creation_timeout", "900000");
				caps.setCapability("appiumVersion", "1.7.1");
				try 
				{
					if(BaseClass.properties.getProperty("RealDeviceDataCenter").equalsIgnoreCase("US"))
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_US), caps);
					else
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_EU), caps);
				}
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}   
				break;
				
			case Constants.SAUCE_MOBILE_SIMULATOR_IOS_BROWSER_SAFARI:
				caps = DesiredCapabilities.iphone();
				caps.setCapability("appiumVersion", "1.7.1");
				caps.setCapability("deviceName",strDeviceName);
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("platformVersion",strDeviceOS.replace("iOS ", ""));
				caps.setCapability("platformName", "iOS");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.SAUCE_TABLET_EMULATOR_ANDROID_BROWSER_CHROME:
				caps = DesiredCapabilities.android();
				caps.setCapability("appiumVersion", "1.7.1");
				caps.setCapability("deviceName",strDeviceName);
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("deviceType", "tablet");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("platformVersion",strDeviceOS.replace("Android ", ""));
				caps.setCapability("platformName","Android");
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.SAUCE_TABLET_ANDROID_BROWSER_CHROME:
				caps = new DesiredCapabilities();
				caps.setCapability("testobjectApiKey", BaseClass.properties.getProperty("TestobjectApiKey"));
				caps.setCapability("platformName", "Android");
				caps.setCapability("platformVersion", strDeviceOS.replace("Android ", ""));
				caps.setCapability("deviceName", strDeviceName);
				caps.setCapability("privateDevicesOnly", "false	");
				caps.setCapability("deviceType", "tablet");
				caps.setCapability("testobject_app_id", "1");
				caps.setCapability("browserName", "Chrome");
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				caps.setCapability("testobject_session_creation_timeout", "900000");
				caps.setCapability("noReset", "false");
				caps.setCapability("appiumVersion", "1.7.1");
				try 
				{
					if(BaseClass.properties.getProperty("RealDeviceDataCenter").equalsIgnoreCase("US"))
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_US), caps);
					else
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_EU), caps);
				}
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}   
				break;
				
			case Constants.SAUCE_TABLET_IOS_BROWSER_SAFARI:
				caps = new DesiredCapabilities();
				caps.setCapability("testobjectApiKey", BaseClass.properties.getProperty("TestobjectApiKey"));
				caps.setCapability("platformName", "iOS");
				caps.setCapability("platformVersion", strDeviceOS.replace("iOS ", ""));
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("deviceName", strDeviceName);
				caps.setCapability("testobject_app_id", "1");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				caps.setCapability("testobject_session_creation_timeout", "900000");
				caps.setCapability("appiumVersion", "1.7.1");
				try 
				{
					if(BaseClass.properties.getProperty("RealDeviceDataCenter").equalsIgnoreCase("US"))
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_US), caps);
					else
						driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_EU), caps);
				}
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}   
				break;
				
			case Constants.SAUCE_TABLET_SIMULATOR_IOS_BROWSER_SAFARI:
				caps = DesiredCapabilities.ipad();
				caps.setCapability("appiumVersion", "1.7.1");
				caps.setCapability("deviceName",strDeviceName);
				caps.setCapability("deviceOrientation", "portrait");
				caps.setCapability("platformVersion",strDeviceOS.replace("iOS ", ""));
				caps.setCapability("platformName", "iOS");
				caps.setCapability("browserName", strBrowserName.trim());
				caps.setCapability("name", BaseClass.properties.getProperty("ProjectName")+"_"+testCaseName);
				try 
				{
					driver = new RemoteWebDriver(new java.net.URL(saucelabAPI_Web), caps);
				} 
				catch (MalformedURLException e) 
				{
					loggerUtils.stackTracePrint(e);
				}
				break;
				
			case Constants.LOCAL_Emulator_IOS_Chrome:
				if("Linux".equalsIgnoreCase(System.getProperty("os.name")))
	        	{				
				   	System.setProperty("webdriver.chrome.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"chromedriver");
	        	}
			    else
			    {
			    	System.setProperty("webdriver.chrome.driver", BaseClass.UserDir+BaseClass.properties.getProperty("DriversPath")+"chromedriver.exe");
			    }
                
                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "iPhone 8 Plus");
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
                chromeOptions.addArguments("disable-infobars");
                if(BaseClass.properties.getProperty("RunWithHeadless").equalsIgnoreCase("Yes"))
				{
                	chromeOptions.addArguments("--headless");
				}
                
                if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
                {
	                //start the proxy
					////System.out.println(port);
					//proxy.start(port);
					proxy.start();
					synchronized (SeleniumActionEngine.class) {						
						port++;
					}
					 // get the Selenium proxy object
				    Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
				    chromeOptions.setCapability(CapabilityType.PROXY, seleniumProxy);
                }
                
                this.driver = new ChromeDriver(chromeOptions);
                this.driver.manage().deleteAllCookies();
                
                break;
				
			default:
				//throw new InvalidBrowserTypeException();
			break;
		}
	}



	@Override
	public boolean enterText(By textField, String textValue,String elementName)
	{
		Boolean valid = false;
		 try
		 {
			Boolean visibility = explicitWaitforVisibility(textField,10,elementName);
			if( visibility && !textValue.equals(""))
			{
				WebElement element = this.driver.findElement(textField);
				element.clear();
				element.sendKeys(textValue);
				valid = true;
				loggerUtils.passLog(elementName+" is entered in the field");
			}
			else
				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+textField.toString()+" is not visible");
		 }
		 catch(Exception e)
		 {
			 loggerUtils.stackTracePrint(e);
			 ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+textField.toString()+" is not present");
			 loggerUtils.failLog(elementName+"is not present");
			 //takeScreenShot(elementName);			 
		 }
		 return valid;
	}
	
	@Override
	public boolean appendText(By textField, String textValue,String elementName)
	{
		Boolean valid = false;
		 try
		 {
			Boolean visibility = explicitWaitforVisibility(textField,10,elementName);
			if( visibility && !textValue.equals(""))
			{
				WebElement element = this.driver.findElement(textField);
				element.sendKeys(textValue);
				valid = true;
			}
		 }
		 catch(Exception e)
		 {
			 loggerUtils.stackTracePrint(e);
			// takeScreenShot(elementName);
		 }
		 return valid;
	}
	@Override
	public void actionEnterText(By textField, String textValue,String elementName)
	{
		try
		{
			Actions actions = new Actions(driver);
			WebElement path = driver.findElement(textField);
			actions.moveToElement(path);
			actions.click();
			actions.sendKeys(textValue);
			actions.build().perform();
			loggerUtils.passLog(elementName+" is entered in the field");
		}
		catch (Exception e) 
		{
			 loggerUtils.stackTracePrint(e);
			 ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+textField.toString()+" is not present");
			 loggerUtils.failLog(elementName+"is not present");
			 //takeScreenShot(elementName);
		}
	}
	
	@Override
	public int getSize(By path,String elementName)
	{
		return driver.findElements(path).size();
	}

	@Override
	public void actionEnterTextandSubmit(By textField, String textValue,String elementName)
	{
		try
		{
			Actions actions = new Actions(driver);
			WebElement path = driver.findElement(textField);
			actions.moveToElement(path);
			actions.click();
			actions.sendKeys(textValue);
			actions.sendKeys(Keys.ENTER);
			actions.build().perform();
			loggerUtils.passLog(elementName+" is entered in the field");
		}
		catch (Exception e) 
		{
			 loggerUtils.stackTracePrint(e);
			 ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+textField.toString()+" is not present");
			 loggerUtils.failLog(elementName+"is not present");
			// takeScreenShot(elementName);
		}
	}

	
	@Override
	public boolean clickElement(By buttonField,String elementName)
	{
		Boolean valid = false;
		try
		{	
			if(explicitWaitforClickable(buttonField,10,elementName))
			{
				WebElement element = this.driver.findElement(buttonField);
				element.click();
				valid = true;
				loggerUtils.passLog(elementName+" is clicked");
			}
			else
				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+buttonField.toString()+" is not visible");
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+buttonField.toString()+" is not present");
			loggerUtils.failLog(elementName+"is not present");
			//takeScreenShot(elementName);
		}
		return valid;
	}

	@Override 
	public boolean mouseHoverElement(By buttonField,String elementName)
	{
		Boolean valid = false;
		try
		{
			if(explicitWaitforVisibility(buttonField,10,elementName))
			{
				Actions act=new Actions(driver);
				act.moveToElement(driver.findElement(buttonField)).build().perform();
//				act.perform();
				valid = true;
				loggerUtils.passLog(elementName+" is hovered");
			}
			else
				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+buttonField.toString()+" is not visible");
		}
		catch (Exception e) 
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+buttonField.toString()+" is not present");
			loggerUtils.failLog(elementName+"is not present");
			//takeScreenShot(elementName);
		}
		return valid;
	}
	
	@Override
	public boolean actionClickElement(By path,String elementName)
	{
		Boolean valid = false;
		try
		{
			Actions click = new Actions(this.driver);
			WebElement webPath = driver.findElement(path);
			click.moveToElement(webPath).click().build().perform();
			valid = true;
			loggerUtils.passLog(elementName+" is clicked");
		}	
		catch (Exception e) 
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			loggerUtils.failLog(elementName+"is not present");
			//takeScreenShot(elementName);
		}
		return valid;
	}
			
	@Override
	public boolean isElementPresent(By path,String elementName)
	{
		return !this.driver.findElements(path).isEmpty();
	}	 

	@Override
	public boolean compareElementText(By path,String expectedText,String elementName)
	{
		Boolean valid = false;
			try
			{
				if(explicitWaitforVisibility(path,10,elementName))
				{
					scrollToElement(path, elementName);
					String currentText = getText(path, elementName).trim();
					expectedText=Jsoup.parse(expectedText).text();
					if(currentText.equals(expectedText))
					{
						loggerUtils.passLog(elementName+" is verified");
						valid = true;
					}
					else
					{
						//Logging
						loggerUtils.failLog(elementName+" is mismatched");
					}
				}
				else
					ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not visible");
			}
			catch(Exception e)
			{
				loggerUtils.stackTracePrint(e);
				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
			return valid;
	}

	@Override
	public boolean explicitWaitforVisibility(By path,int seconds,String elementName)
	{
			try
			{
				WebDriverWait wait = new WebDriverWait(this.driver,seconds);
				wait.until(ExpectedConditions.visibilityOfElementLocated(path));
				return true;
			}
			catch(Exception e)
			{
				loggerUtils.stackTracePrint(e);
				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
				return false;
			}
	}

	@Override
	public boolean isElementDisplayed(By path,int seconds,String elementName)
	{
		Boolean valid = false;
			try
			{
				if(explicitWaitforVisibility(path,seconds,elementName))
				{
					WebElement element = this.driver.findElement(path);
					valid = element.isDisplayed();
					if(valid)
						loggerUtils.passLog(elementName+" is displayed");
					else
						loggerUtils.failLog(elementName+" is not displayed");
				}
				else
					ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not visible");
			}
			catch(Exception e)
			{
				loggerUtils.stackTracePrint(e);
				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
			return valid;
	}

	@Override
	public boolean isElementNotDisplayed(By path,String elementName)
	{
		Boolean valid = true;
			if(isElementPresent(path, elementName))
			{
				WebElement element = this.driver.findElement(path);
				valid = !element.isDisplayed();
				if(!valid) {
					//takeScreenShot(elementName);
				}
			}
		return valid;
	}

	@Override
	public boolean isElementEnabled(By path,String elementName)
	{
		Boolean valid = false;
		try
		{
			if(explicitWaitforVisibility(path,10,elementName))
			{
				WebElement element = this.driver.findElement(path);
				valid = element.isEnabled();
				if(valid)
					loggerUtils.passLog(elementName+" is enabled");
				else
					loggerUtils.failLog(elementName+" is not enabled");
			}
			else
				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not visible");
		}
		catch (Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			loggerUtils.failLog(elementName+" is not present");
			//takeScreenShot(elementName);
		}
		return valid;
	}

	@Override
	public String getText(By path,String elementName)
	{
		String text = "";
			try
			{
				if(explicitWaitforVisibility(path,10,elementName))
				{
					WebElement element = this.driver.findElement(path);
					text = element.getText();
				}
			}
			catch(Exception e)
			{
				loggerUtils.stackTracePrint(e);
				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
			return text;
	}

	@Override
	public String getElementAttribute(By path,String attrName,String elementName)
	{
		String text = "";
			try
			{
				if(explicitWaitforVisibility(path,10,elementName))
				{
					WebElement element = this.driver.findElement(path);
					text = element.getAttribute(attrName);
				}
			}
			catch(Exception e)
			{
				loggerUtils.stackTracePrint(e);
				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
			return text;
	}

	@Override
	public boolean compareElementAttribute(By path,String attrName,String expectedText,String elementName)
	{
		Boolean valid = false;
			try
			{
				if(explicitWaitforVisibility(path,10,elementName))
				{
					WebElement element = this.driver.findElement(path);
					if(element.getAttribute(attrName).equals(expectedText))
					{
						valid = true;
						loggerUtils.passLog(elementName+" is verified");
					}
					else
					{
						//Logging
						loggerUtils.failLog(elementName+" is mismatched");
					}
				}
				else
					ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not visible");
			}
			catch(Exception e)
			{
				loggerUtils.stackTracePrint(e);
				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not visible");
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
		return valid;
	}

	/*@Override
    public void takeScreenShot(String elementName)
    {
      try
      {
    	  DateFormat dateFormat = new SimpleDateFormat("h_m_s");
		  Date date = new Date();
    	  TakesScreenshot scrShot =((TakesScreenshot)this.driver);
    	  File sourceFile=scrShot.getScreenshotAs(OutputType.FILE);
    	  FileUtils.copyFile(sourceFile,new File(ExtentManager.screenshotPath+"/"+elementName+"_"+dateFormat.format(date)+".png"));
      }
      catch (Exception e)
      {
    	  loggerUtils.stackTracePrint(e);
      }
    }*/

	@Override
    public void takeScreenshotWithPath(String path,String elementName)
    {
      try
      {
    	  DateFormat dateFormat = new SimpleDateFormat("h_m_s");
		  Date date = new Date();
    	  TakesScreenshot scrShot =((TakesScreenshot)this.driver);
    	  File sourceFile=scrShot.getScreenshotAs(OutputType.FILE);
    	  FileUtils.copyFile(sourceFile,new File(path+elementName+"_"+dateFormat.format(date)+".png"));
      }
      catch (Exception e)
      {
    	  loggerUtils.stackTracePrint(e);
      }
    }
	@Override
	public int generateRandomNumber(int limit)
	{
		Random rand = new Random();
		return rand.nextInt(limit);
	}
	
	@Override
	public int generateRandomNumberWithLimit(int maximum,int minimum)
	{
		Random rand = new Random();
		return rand.nextInt(maximum) + minimum;
	}
	
	@Override
	public boolean scrollToElement(By path,String elementName)
	{
		Boolean valid = false;
		try
		{
			WebElement element = this.driver.findElement(path);
			((JavascriptExecutor) this.driver).executeScript("arguments[0].scrollIntoView();", element);
			valid = true;
			loggerUtils.passLog("scrolled to "+elementName);
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			loggerUtils.failLog(elementName+" is not present");
			//takeScreenShot(elementName);
			valid = false;
		}
		return valid;
	}

	@Override
	public void maximizeBrowser() 
	{
//		driver.manage().window().maximize();
		driver.manage().window().setSize(new Dimension(1280,720));
	}

	@Override
	public void navigateToUrl(String url) 
	{
		driver.get(url);
		loggerUtils.passLog(url+" is opened");
	}
	
	@Override
	public boolean enterTextAndSubmit(By path,String value,String elementName)
	{
		 boolean valid=false;
		 try
		 {
			 if(explicitWaitforVisibility(path,10,elementName) && !value.equals(""))
			 {
				 WebElement element = driver.findElement(path);
				 element.clear();
				 element.sendKeys(value);
				 element.sendKeys(Keys.ENTER);
				 valid = true;
				 loggerUtils.passLog(value+" is entered in "+elementName);
			 }
			 else
					ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not visible");
		 }
		 catch(Exception e)
		 {
			 loggerUtils.stackTracePrint(e);
			 ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			 loggerUtils.failLog(elementName+"is not present");
			 //takeScreenShot(elementName);
		 }
		return valid;
	}

	@Override
	public void quit() 
	{
		driver.close();
		driver.quit();
		
	}
		
	@Override
	public void navigateToForward()
	{
		driver.navigate().forward();
	}

	@Override
	public String getCurrentUrl()
	{
		return driver.getCurrentUrl();
	}
	
	@Override
	public boolean selectByIndex(By path,int indexField,String elementName)
	{
		Boolean valid = false;
		try
		{	
				WebElement element = this.driver.findElement(path);
				Select dropdown= new Select(element);
				dropdown.selectByIndex(indexField);
				valid = true;
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			//takeScreenShot(elementName);
		}
		return valid;
	}
	
	@Override
	public boolean isElementSelected(By path, String elementName)
	{
		boolean valid=false;
		try{
			
			if(explicitWaitforVisibility(path, 10, elementName))
			{
				WebElement element = this.driver.findElement(path);
				valid=element.isSelected();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			//takeScreenShot(elementName);
		}
		
		return valid;
	}
	
	@Override
	public boolean isElementNotSelected(By path, String elementName)
	{
		boolean valid=true;
		try{
			
			if(explicitWaitforVisibility(path, 10, elementName))
			{
				WebElement element = this.driver.findElement(path);
				valid=element.isSelected();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			//takeScreenShot(elementName);
		}
		
		return valid;
	}
	
	@Override
	public boolean explicitWaitforInVisibility(By path,int seconds,String elementName)
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(this.driver,seconds);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(path));
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			//takeScreenShot(elementName);
			return false;
		}
	}
	
	@Override
	public Boolean tabKey(By buttonField,String elementName)
	{
		Boolean valid = false;
		try
		{	
			if(explicitWaitforVisibility(buttonField,30,elementName))
			{
				WebElement element = this.driver.findElement(buttonField);
				element.sendKeys(Keys.TAB);
				valid = true;
			}
			else
				ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+buttonField.toString()+" is not visible");
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+buttonField.toString()+" is not visible");
			//takeScreenShot(elementName);
		}
		return valid;
	}
	
	@Override
	public boolean waitForPageLoad(int secs)
	{
		boolean valid=false;
		try
		{
			driver.manage().timeouts().pageLoadTimeout(secs, TimeUnit.SECONDS);
			valid=true;
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
		}
		
		return valid;
	}

	@Override
	public boolean moveToElementAndClick(By path, String elementName)
	{
		Boolean valid = false;
		try
		{
			WebElement element = driver.findElement(path);
			Actions act = new Actions(this.driver);
			act.moveToElement(element).click().build().perform();
			valid = true;
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
           // takeScreenShot(elementName);
            
    	}
		return valid;
	}
	
	@Override
	public boolean jsClickElement(By buttonField,String elementName)
	{
		boolean valid=false;
		try
		{
			 WebElement element = driver.findElement(buttonField);
			 JavascriptExecutor executor = (JavascriptExecutor) driver;
			 executor.executeScript("arguments[0].click();", element);
			 valid=true;
			 loggerUtils.passLog(elementName+" is clicked");
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+buttonField.toString()+" is not present");
			//takeScreenShot(elementName);
		}
		
		return valid;
	}
	
	@Override
	public boolean alertPopupVerification()
	{ 
		    try 
		    { 
		        driver.switchTo().alert(); 
		        ////System.out.println(driver.switchTo().alert().getText());
		        return true; 
		    }    
		    catch (Exception e) 
		    { 
		    	//takeScreenShot("Chrome popup");
		        return false; 
		    }     
	}
	
	
	@Override
	public boolean waitforAlertPopup() 
	{
		boolean valid = false;
		try
		{
			 int i=0;
			   while(i++<5)
			   {
			        try
			        {
			            Alert alert = driver.switchTo().alert();
			            ////System.out.println(alert);
			            valid = true;
			            break;
			        }
			        catch(NoAlertPresentException e)
			        {
				        Thread.sleep(1000);
				        valid = false;
				        continue;
			        }
			   }
		}
		catch (Exception e)
		{
			//takeScreenShot("Chrome popup");
			valid = false;
		}
		
		return valid;
	}
	
	@Override
	public String alertPopupGetText()
	{
		String text = "";
			    try 
			    { 
			        driver.switchTo().alert(); 
			        text = (driver.switchTo().alert().getText());
			    }    
			    catch (Exception e) 
			    { 
			    	//takeScreenShot("Chrome popup");
			    }     
	    return text; 
	}

	@Override
	public boolean alertPopupAccept()
	{ 
		    try 
		    { 
		        driver.switchTo().alert().accept();; 
		        return true; 
		    }    
		    catch (Exception e) 
		    { 
		    	//takeScreenShot("Chrome popup");
		        return false; 
		    }     
	}	 
	
	@Override
	public boolean explicitWaitforClickable(By path,int seconds,String elementName)
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(this.driver,seconds);
			wait.until(ExpectedConditions.elementToBeClickable(path));
			return true;
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			//takeScreenShot(elementName);
			return false;
		}
	}
	
	@Override
	public boolean SwitchtoIframe(By path,int seconds,String elementName)
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(this.driver,seconds);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(path));
			return true;
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			//takeScreenShot(elementName);
			return false;
		}
	}
	
	@Override
	public boolean SwitchtoParentFrame(String elementName)
	{
		try
		{
			driver.switchTo().parentFrame();
			return true;
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			//takeScreenShot(elementName);
			return false;
		}
	}
	@Override
	public boolean clearText(By path, int seconds, String elementName)
	{
		Boolean valid = false;
		try
		{
			if(explicitWaitforVisibility(path,10,elementName))
			 {
				WebElement element  = driver.findElement(path);
				element.clear();
				valid  = true;
			 }
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
            //takeScreenShot(elementName);
            
    	}
		
		return valid;
	}
	
	
	@Override
	public boolean waitForPageLoad(By path, int seconds, String elementName, String style)
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(this.driver,seconds);
			wait.until(ExpectedConditions.attributeContains(path, "style", style));
			return true;		
		}
		catch(Exception e)
		{
			e.printStackTrace();
           // takeScreenShot(elementName);
			return false;
		}
	}
	
	@Override
	public boolean waitForPageLoad1(By path, By path1, int seconds, String elementName)
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(this.driver,seconds);
			wait.until(ExpectedConditions.and(
					ExpectedConditions.attributeContains(path, "style", "none"),
					ExpectedConditions.visibilityOfElementLocated(path1)
					)
			);
			return true;		
		}
		catch(Exception e)
		{
		e.printStackTrace();
           // takeScreenShot(elementName);
			return false;
		}
	}
	
	@Override
	public String getCookie(String cookieName)
	{
		/*
		 * input - CookieName -> the cookie name which need to get the value
		 * 
		 * output - CookieValue -> it returns the cookie value for success scenario. For error scenario it returns the NOTFOUND keyword
		 */
		String CookieValue="";
		try
		{
			CookieValue=driver.manage().getCookieNamed(cookieName).getValue();
		}
		catch(Exception e)
		{
			CookieValue="NOTFOUND";
		}
		return CookieValue;
	}
	
		
	@Override
	public boolean navigateWithKeys(Keys value, int count, String elementName)
	{
		Boolean valid = false;
		try
		{
			Actions act = new Actions(this.driver);
			for(int i = 0; i<count; i++)
			{
				act.sendKeys(value).build().perform();
			}
			valid = true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
           // takeScreenShot(elementName);
            
    	}
		return valid;
	}

	@Override
	public boolean waitForScriptLoad(int secs)
	{
		boolean valid = false;
		try
		{
			driver.manage().timeouts().setScriptTimeout(secs, TimeUnit.SECONDS);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return valid;
	}

	@Override
	public String takeScreenShotReturnFilePath() 
	{
		String scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BASE64);
		return "data:image/jpg;base64," + scrFile;
	}
	
	@Override
	public void changeFocus(int tabNumber)
	{
		try
		{
			tabs = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(tabs.get(tabNumber));
		}
		catch (Exception e)
		{
			loggerUtils.stackTracePrint(e);
			//takeScreenShot("Tab focus");
		}
	}
	
	@Override
	public void changeFocusToDefaultTab()
	{
		try
		{
		    driver.close();
		    driver.switchTo().window(tabs.get(0));
		}
		catch (Exception e)
		{
			loggerUtils.stackTracePrint(e);
			//takeScreenShot("Tab focus to default");
		}
	}
	
	@Override
	public void driverMode(String view)
	{
		if(view.equalsIgnoreCase("mobile"))
		{
			driver.manage().window().setSize(new Dimension(500,650));
		}
		else if(view.equalsIgnoreCase("tablet"))
		{
			driver.manage().window().setSize(new Dimension(1000,650));
		}
		else
		{
			maximizeBrowser();
		}
	}

	@Override
	public Set<Cookie> getAllcookies() {
		// TODO Auto-generated method stub
		Set<Cookie> cookie = driver.manage().getCookies();
		return cookie;
	}
	@Override
	public void setProxyDomain(String domain)
	{
		  if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
          {
			  proxy.newHar(domain);
          }
	}
	
	@Override
	public void enableHarCaptureTypes()
	{	
		if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
    	{
			proxy.enableHarCaptureTypes(CaptureType.REQUEST_CONTENT, CaptureType.RESPONSE_HEADERS,CaptureType.RESPONSE_CONTENT);
    	}
	}
	
	@Override
	public Har getHar()
	{
		Har har = null;
		if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
        {		
			har=proxy.getHar();
        }
		return har;
	}
	
	@Override
	public void abortProxy()
	{
		if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
        {
			proxy.abort();
        }
	}
	
	@Override
	public void saveNetworkLog(String fileName)
	{
		if(BaseClass.properties.getProperty("RunWithProxy").equalsIgnoreCase("YES"))
        {
			File files = new File(BaseClass.currentRunReportPath+"\\networkLogs");
			files.mkdirs();
			String filePath=BaseClass.currentRunReportPath+"\\networkLogs\\"+fileName;
			Har har=proxy.getHar();
			File harFile = new File(filePath);
			try {
				har.writeTo(harFile);
			} catch (IOException ex) {
				 ex.printStackTrace();
			}
        }
	}

	@Override
	public String getElementAttributeWithoutVisibility(By path,String attrName)
	{
		String text = "";
		try
		{
			WebElement element = this.driver.findElement(path);
			text = element.getAttribute(attrName);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return text;
	}
	
	
	
	@Override
	public Map<String,String> getBrowserDetails()
	{
		Map<String,String> browserDetails = new HashMap<String, String>();
		try
		{
			Capabilities cap = ((RemoteWebDriver) this.driver).getCapabilities();
		    String browserName = cap.getBrowserName().toUpperCase();
		    browserDetails.put("Browser Name", browserName);
		    String browserVersion = cap.getVersion();
		    browserDetails.put("Browser Version", browserVersion);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return browserDetails;
		
	}
	
	public void refreshPage()
	{
		try
		{
			driver.navigate().refresh();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public String getPseudoValue(By path, String pseudoType, String elementName)
	{
		String pseudoValue = "";
		try
		{
			WebElement element = this.driver.findElement(path);
			pseudoValue = ((JavascriptExecutor)driver)
			        .executeScript("return window.getComputedStyle(arguments[0], ':"+pseudoType+"').getPropertyValue('content');",element).toString();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return pseudoValue;
	}
	
	@Override
	public boolean switchToWidow(int seconds) 
	{
		boolean valid = false;
		try
		{
			WebDriverWait wait = new WebDriverWait(driver,seconds);
			wait.until(ExpectedConditions.numberOfWindowsToBe(2));
			Set<String>ids= driver.getWindowHandles();
			Iterator<String> it =ids.iterator();
			String parentId=it.next();
			////System.out.println(parentId);
			String childId=it.next();
			driver.switchTo().window(childId);
			////System.out.println(childId);
			valid = true;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			valid = false;
		}
		return valid;
	}
	
	@Override
	public void  navigateBack() 
	{
		driver.navigate().back();
	}
	
	@Override
	public boolean selectByValue(By path,String value,String elementName)
	{
		Boolean valid = false;
		try
		{	
			WebElement element = this.driver.findElement(path);
			Select dropdown= new Select(element);
			dropdown.selectByValue(value);
			valid = true;
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			//takeScreenShot(elementName);
		}
		return valid;
	}
	
	@Override
	public boolean compareElementTextWithIgnoreCase(By path,String expectedText,String elementName)
	{
		Boolean valid = false;
			try
			{
				if(explicitWaitforVisibility(path,10,elementName))
				{
					scrollToElement(path, elementName);
					String currentText = getText(path, elementName).trim();
					expectedText=Jsoup.parse(expectedText).text();
					if(currentText.equalsIgnoreCase(expectedText))
					{
						loggerUtils.passLog(elementName+" is verified");
						valid = true;
					}
					else
					{
						//Logging
						loggerUtils.failLog(elementName+" is mismatched");
					}
				}
			}
			catch(Exception e)
			{
				loggerUtils.stackTracePrint(e);
				loggerUtils.failLog(elementName+" is not present");
				//takeScreenShot(elementName);
			}
			return valid;
	}
	
	@Override
	public boolean setWindowSize(int width, int height)
	{
		Boolean valid = false;
		try
		{
			driver.manage().window().setSize(new Dimension(width, height));
			valid = true;
		}
		catch (Exception e) 
		{
			loggerUtils.stackTracePrint(e);
			e.printStackTrace();
		}
		return valid;
	}
	
	@Override
	public void deleteAllCookies()
	{
		Set<Cookie> allCookies = driver.manage().getCookies();
		for (Cookie cookie : allCookies) 
		{
			////System.out.println(cookie.getName());
		    driver.manage().deleteCookieNamed(cookie.getName());
		}
		
		driver.manage().deleteAllCookies();
		driver.manage().deleteCookieNamed("JSESSIONID");
	}
	
	@Override
	public boolean moveToElement(By path, String elementName)
	{
		Boolean valid = false;
		try
		{
			WebElement element = driver.findElement(path);
			Actions act = new Actions(this.driver);
			act.moveToElement(element).build().perform();
			valid = true;
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
            //takeScreenShot(elementName);
            
    	}
		return valid;
	}

	@Override
	public boolean jsScrollup() {

		boolean valid=false;
		try
		{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("scroll(0, -250);");
		valid=true;
		loggerUtils.passLog("Scrolled up");
		}
		catch(Exception e)
		{
		loggerUtils.stackTracePrint(e);
		ExtentTestManager.getTest().log(LogStatus.INFO," scroll");
		//takeScreenShot("Scoll up");
		}
		return valid;
	}
	
	@Override
	public boolean jsScrollDown() {

		boolean valid=false;
		try
		{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("scroll(0, 250);");
		valid=true;
		loggerUtils.passLog("Scrolled up");
		}
		catch(Exception e)
		{
		loggerUtils.stackTracePrint(e);
		ExtentTestManager.getTest().log(LogStatus.INFO," scroll");
		//takeScreenShot("Scoll up");
		}
		return valid;
	}
	
	@Override
	public String getCSSProprty(By path,String property,String elementName)
	{
		String cssVal="";
		try{
			
			if(explicitWaitforVisibility(path, 10, elementName))
			{
				WebElement element = this.driver.findElement(path);
				cssVal = element.getCssValue(property);
			}
		}
		catch(Exception e)
		{
			loggerUtils.stackTracePrint(e);
			ExtentTestManager.getTest().log(LogStatus.INFO,elementName+" xpath "+path.toString()+" is not present");
			//takeScreenShot(elementName);
		}
		return cssVal;
	}

	@Override
	public void navigateBack(String url) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List findElement(By path) {
		// TODO Auto-generated method stub
		return null;
	}

	public void cleanUp() {
		chromeDriverService.stop();
		
	}
	
	
}