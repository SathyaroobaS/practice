package com.skava.framework.action;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import net.lightbody.bmp.core.har.Har;

public interface ActionEngine 
{
	public void maximizeBrowser();

	public void navigateToUrl(String url);
	public void navigateBack(String url);
	

	public boolean enterText(By textField, String textValue, String elementName);

	public boolean clickElement(By buttonField, String elementName);

	public boolean isElementPresent(By elementPath, String elementName);

	public boolean compareElementText(By path, String expectedText, String elementName);

	public boolean explicitWaitforVisibility(By path, int seconds, String elementName);

	public boolean isElementDisplayed(By path, int seconds, String elementName);

	public String getText(By path, String elementName);

	public String getElementAttribute(By path, String attrName, String elementName);

	public boolean compareElementAttribute(By path, String attrName, String expectedText, String elementName);

	public int generateRandomNumber(int limit);

	public int generateRandomNumberWithLimit(int maximum, int minimum);

	public boolean scrollToElement(By path, String elementName);

	public boolean isElementNotDisplayed(By path, String elementName);

	public boolean isElementEnabled(By path, String elementName);

	//public void takeScreenShot(String elementName);

	public boolean enterTextAndSubmit(By path, String value, String elementName);

	public void quit();

	public void takeScreenshotWithPath(String path, String elementName);

	public String getCurrentUrl();

	public int getSize(By path, String elementName);
	public List findElement(By path);
	
	public boolean selectByIndex(By path, int indexField, String elementName);
	
	public Boolean tabKey(By buttonField, String elementName);
	
	public boolean waitForPageLoad(int secs);
	
	public boolean appendText(By textField, String textValue, String elementName);
	
	public boolean moveToElementAndClick(By path, String elementName);
	
	public boolean jsClickElement(By buttonField, String elementName);
	
	public boolean alertPopupVerification();
	
	public boolean waitforAlertPopup();
	
	public String alertPopupGetText();
	
	public boolean alertPopupAccept();

	public boolean explicitWaitforInVisibility(By path,int seconds,String elementName);
	
	public boolean explicitWaitforClickable(By path,int seconds,String elementName);
	
	public boolean SwitchtoIframe(By path,int seconds,String elementName);
	
	public boolean SwitchtoParentFrame(String elementName);
	
	public boolean waitForPageLoad(By path, int seconds, String elementName, String style);
	
	public boolean waitForPageLoad1(By path, By path1, int seconds, String elementName);
	
	public boolean clearText(By path, int seconds, String elementName);
	
	public String getCookie(String cookieName);

	public boolean navigateWithKeys(Keys value, int count, String elementName);
	
	public void changeFocus(int tabNumber);
	
	public void changeFocusToDefaultTab();
	
	public void driverMode(String view);
	
	public boolean isElementSelected(By path, String elementName);
	
	public boolean isElementNotSelected(By path, String elementName);

	public boolean actionClickElement(By path, String elementName);
	
	public Set<Cookie> getAllcookies();	

	public boolean waitForScriptLoad(int secs);

	public String takeScreenShotReturnFilePath();
	
	public String getElementAttributeWithoutVisibility(By path,String attrName);

	public void setProxyDomain(String domain);

	public void actionEnterText(By textField, String textValue, String elementName);

	public void actionEnterTextandSubmit(By textField, String textValue, String elementName);

	public boolean mouseHoverElement(By buttonField, String elementName);

	public void navigateToForward();
	
	public void enableHarCaptureTypes();
	
	public Har getHar();
	
	public void abortProxy();
	
	public void saveNetworkLog(String fileName);
	
	public Map<String,String> getBrowserDetails();
	
	public void refreshPage();
	
	public String getPseudoValue(By path, String pseudoType, String elementName);
	
	public boolean switchToWidow(int seconds);
	
	public void navigateBack();
	
	public boolean selectByValue(By path,String value,String elementName);
	
	public boolean compareElementTextWithIgnoreCase(By path, String expectedText, String elementName);
	
	public boolean setWindowSize(int width, int height);
	
	public void deleteAllCookies();
	
	public boolean moveToElement(By path, String elementName);
	
	public boolean jsScrollup();
	
	public boolean jsScrollDown();
		
	public String getCSSProprty(By path, String property, String elementName);
	
	public void cleanUp();

	
}