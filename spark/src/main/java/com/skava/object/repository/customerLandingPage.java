package com.skava.object.repository;

import org.openqa.selenium.By;

import com.skava.reusable.components.GeneralComponents;

public class customerLandingPage 
{

	public static final By customerTitle = By.xpath("//*[@data-qa='cusomers-title']");
	public static final By addcustomerTrigger = By.xpath("//*[@data-qa='add-customer-trigger']");
	public static final By firstnameInput = By.xpath("//*[@data-qa='firstname-input']");
	public static final By lastnameInput = By.xpath("//*[@data-qa='lastname-input']");
	public static final By emailInput = By.xpath("//*[@data-qa='email-input']");
	public static final By sendInviteTrigger = By.xpath("//*[@data-qa='send-invite-trigger']");
	public static final By addCustomerTitle = By.xpath("//*[@data-qa='add-customer-title']");
	public static final By closeTrigger = By.xpath("//*[@data-qa='close-trigger']");
	public static final By lastNameValue = By.xpath("//*[@data-qa='last-name-value']");
	public static final By firstNameValue = By.xpath("//*[@data-qa='first-name-value']");
	public static final By emailAddressValue = By.xpath("//*[@data-qa='emailaddress-value']");
	public static final By phoneValue = By.xpath("//*[@data-qa='tele-phone-value']");
	public static final By customerIdValue = By.xpath("//*[@data-qa='cusomers-id-value']");
	public static final By latestOrderValue = By.xpath("//*[@data-qa='latest-order-value']");
	public static final By statusValue = By.xpath("//*[@data-qa='status-value']"); 
	public static final By searchInput = By.xpath("//*[@data-qa='search-input']"); 
	public static final By searchTrigger = By.xpath("//*[@data-qa='search-trigger']");
	public static final By firstNameCloseTrigger = By.xpath("//*[@data-qa='first-name-close-trigger']");
	public static final By lastNameCloseTrigger = By.xpath("//*[@data-qa='last-name-close-trigger']");
	public static final By emailCloseTrigger = By.xpath("//*[@data-qa='email-close-trigger']");
	public static final By ordertypelist = By.xpath("//*[@data-qa='ordertype-list']");
	public static final By statuslist = By.xpath("//*[@data-qa='status-list']");
	public static final By fulfillmentlist = By.xpath("//*[@data-qa='fulfillment-type-list']");
	public static final By datepickerfrom = By.xpath("//*[@data-qa='datepicker-from']");
	public static final By datepickerto = By.xpath("//*[@data-qa='datepicker-to']");
	
	
	public static final By nameFilter = By.xpath("//*[@data-qa='first-name-search']");
	public static final By nameSearch = By.xpath("//*[@data-qa='first-name-list']");
	
	public static final By lastnameFilter = By.xpath("//*[@data-qa='last-name-search']");
	public static final By lastnameSearch = By.xpath("//*[@data-qa='last-name-list']");
	public static final By emailFilter = By.xpath("//*[@data-qa='email-search']");
	public static final By eamilSearch = By.xpath("//*[@data-qa='email-list']");
	
	public static final By statusFilter = By.xpath("//*[@data-qa='status-list']");
	public static final By statusSearch = By.xpath("//*[@data-qa='status-list']");
	public static final By addMoreFilter = By.xpath("//*[@data-qa='addmorefilter-trigger']");
	public static final By addressFilter = By.xpath("//*[@data-qa='address-list']");
	public static final By addressSearch = By.xpath("//*[@data-qa='address-search']");
	public static final By cityFilter = By.xpath("//*[@data-qa='city-list']");
	public static final By citySearch = By.xpath("//*[@data-qa='city-search']");
	public static final By dateFilter = By.xpath("//*[@data-qa='date-trigger']");
	public static final By datePickerFrom = By.xpath("//*[@data-qa='datepicker-from']");
	public static final By datePickerTo = By.xpath("//*[@data-qa='datepicker-to']");

	public static final By customerDataInTable = By.xpath("//*[@data-qa='cusomers-id-value']");
	public static final By customerTableNoData = By.xpath("//*[@data-qa='customer-no-data-text']");
	public static final By threeDots = By.xpath("//*[@class='material-icons valign']"); 
	public static final By customerIdTrigger = By.xpath("//*[@data-qa='customers-id-trigger']");
	
	public static final By navUserProfile1 = By.xpath("//*[@data-qa='cusomers-id-value']");
	public static final By navUserProfileView = By.xpath("//*[@data-qa='view-profile-trigger']");
	
	public static final By loginUserName = By.xpath("//*[@data-qa='user-name-input']");
	public static final By loginPwd = By.xpath("//*[@data-qa='password-input']");
	public static final By loginSiginBtn = By.xpath("//*[@data-qa='signin-trigger']");
	public static final By extentReportVersionUpdate1 = By.xpath(" //*[@id='main']//summary/span");
	public static final By extentReportVersionUpdate2 = By.xpath(" //*[@id='main']//summary/span");
	
	public static final By ordertypeResult1 = By.xpath("(//*[@data-qa='latest-order-value'])");
	
	
	public static final By notificationTitle = By.xpath("//*[@data-qa='breadcrumb_null']");
	public static final By loginmfa = By.xpath("//*[@name='loginOTP']");
			
}










