package com.skava.object.repository;

import org.openqa.selenium.By;

public class NotificationLandingPage {


	public static final By createNotificationBtn = By.xpath("//*[@data-qa='create-notification-trigger']");
	public static final By sendEmailCheckBox = By.xpath("//*[@data-qa='sendemail-input-switch']/following-sibling::span[1]");
	public static final By smsCheckBox = By.xpath("//*[@data-qa='sendsms-input-switch']/following-sibling::span[1]");
	public static final By notificationNameInput = By.xpath("//*[@id='notificationname']");
	public static final By notificationdescriptionInput = By.xpath("//*[@id='description']");
	public static final By notificationEventInput = By.xpath("//*[@id='eventname']");
	public static final By cancelNotificationBtn = By.xpath("//*[@data-qa='notification-cancel-button-trigger']");
	public static final By saveNotificationBtn = By.xpath("//*[@data-qa='notification-save-button-trigger']");
	public static final By emailTemplateInput = By.xpath("//*[@id='emailtemplate']");
	public static final By smsTemplateInput = By.xpath("//*[@id='smstemplate']");
	public static final By notificationListName = By.xpath("//*[@data-qa='notification-name']");
	public static final By notificationListDescription = By.xpath("//*[@data-qa='status-value']");
	public static final By notificationListEvent = By.xpath("//*[@data-qa='notification-eventname']");
	public static final By notificationNameSearch = By.xpath("//*[@data-qa='notification-name-search']");
	public static final By notificationDescriptionSearch = By.xpath("//*[@data-qa='notification-description-search']");
	public static final By notificationEventSearch = By.xpath("//*[@data-qa='notification-eventname-search']");
	public static final By notificationStatusSearch = By.xpath("//*[@data-qa='status-search']");
	public static final By notificationNameSearchInput = By.xpath("//*[@data-qa='notification-name-list']");
	public static final By notificationSearchBtn = By.xpath("//*[@data-qa='search-trigger']");
	public static final By notificationDescriptionSearchInput = By.xpath("//*[@data-qa='notification-description-list']");
	public static final By notificationEventSearchInput = By.xpath("//*[@data-qa='notification-eventname-list']");
	public static final By notificationStatusSearchClick = By.xpath("//*[@data-qa='notification-description-search']");
	public static final By notificationNameHeading = By.xpath("//*[@data-qa='notification-name-trigger']");
	public static final By notificationEventNameHeading = By.xpath("//*[@data-qa='notification-event-trigger']");
	public static final By notificationStatusHeading = By.xpath("//*[@data-qa='notification-status-trigger']");
	public static final By notificationPageLength = By.xpath("//*[@data-qa='page-length-select']//input");
	public static final By notificationPageLengthSize = By.xpath("//*[@data-qa='page-length-select']//li");
	public static final By notificationPageLengthSizeSpan = By.xpath("//*[@data-qa='page-length-select']//li/span");
	public static final By viewNotification = By.xpath("//*[@data-qa='view-profile-trigger']");
	public static final By editNotification = By.xpath("//*[@data-qa='notification-edit-button-trigger']");
	
	
	
}
