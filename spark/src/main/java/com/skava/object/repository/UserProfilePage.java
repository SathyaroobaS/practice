package com.skava.object.repository;

import org.openqa.selenium.By;

public class UserProfilePage 
{

	public static final By customerId = By.xpath("//*[@data-qa='customer-id-txt']");
	public static final By editProfileTrigger = By.xpath("//*[@data-qa='edit-profile-trigger']");
	public static final By phoneNumberInput = By.xpath("//*[@data-qa='update-phone-number-input']");
	public static final By inlineError = By.xpath("//*[@data-qa='update-phone-number-input']//following-sibling::span");
	
	public static final By profileFirstNameTxt = By.xpath("//*[@data-qa='firstname-txt']");
	public static final By profileLastNameTxt = By.xpath("//*[@data-qa='lastname-txt']");
	public static final By profileViewStatus = By.xpath("//*[@data-qa='status-txt']");
	public static final By profileViewGender = By.xpath("//*[@data-qa='gender-txt']");
	public static final By profileViewCreatedDate = By.xpath("//*[@data-qa='created-date-txt']");
	public static final By profileViewBirthDate = By.xpath("//*[@data-qa='birthdate-txt']");
	public static final By profileViewEmail = By.xpath("//*[@data-qa='email-txt']");
	public static final By profileViewPhoneNo = By.xpath("//*[@data-qa='phone-txt']");
	
	public static final By addCustomerButton = By.xpath("//*[@data-qa='add-customer-trigger']");
	public static final By customerBreadcrumbButton = By.xpath("(//*[@id='id_customerprofilecontainer']//a/span)[2]");
	public static final By FreeeAccountButton = By.xpath("//*[@id=\"id_skFreezeUserModal\"]//*[@class = \"btn btn-sm btn-primary cls_skFreezeUser\"]");
	
	

	public static final By profilegendercontainer = By.xpath("//*[@data-qa='gender-container']");
	public static final By profilestatuscontainer = By.xpath("//*[@data-qa='status-container']");
	
	public static final By profileFirstNameInput = By.xpath("//*[@data-qa='firstname-input']");
	public static final By profileLastNameInput =  By.xpath("//*[@data-qa='lastname-input']");
	public static final By profileStatus = By.xpath("//*[@data-qa='status-dropdown-trigger']");
	public static final By profileGender = By.xpath("//*[@data-qa='gender-dropdown-trigger']");
	public static final By profileSaveChanagesTrigger = By.xpath("//*[@data-qa='save-changes-trigger']");
	
	public static final By profileFirstnameError = By.xpath("//*[@data-qa='first-name-container']");
	public static final By profileLastnameError = By.xpath("//*[@data-qa='last-name-error']//label");
	
	public static final By addressTab = By.xpath("//*[@data-qa='address-tab-trigger']");
	public static final By addAddressTitle = By.xpath("//*[@data-qa='add-new-address-title']");
	public static final By savedAddressTile = By.xpath("//*[@data-qa='saved-address-tile']");
	public static final By editTrigger = By.xpath("//*[@data-qa='edit-icon-trigger']");
	public static final By deleteTrigger = By.xpath("//*[@data-qa='delete-icon-trigger']");
	
	public static final By defaultTrigger = By.xpath("//*[@data-qa='make-default-trigger']");
	public static final By defaultTxt = By.xpath("//*[@data-qa='default-txt']");
	
	public static final By editAddressTitle = By.xpath("//*[@data-qa='edit-address-txt']");
	public static final By firstNameTxt = By.xpath("//*[@data-qa='firstname-input']");
	public static final By lastNameTxt = By.xpath("//*[@data-qa='lastname-input']");
	public static final By address1Txt = By.xpath("//*[@data-qa='address1-input']");
	public static final By address2Txt = By.xpath("//*[@data-qa='address2-input']");
	public static final By cityInput = By.xpath("//*[@data-qa='city-input']");
	public static final By stateInput = By.xpath("//*[@data-qa='state-input']");
	public static final By countryInuput = By.xpath("//*[@data-qa='country-input']");
	public static final By zipcodeInput = By.xpath("//*[@data-qa='zipcode-input']");
	public static final By phoneInput = By.xpath("//*[@data-qa='phone-input']");
	public static final By saveTrigger = By.xpath("//*[@data-qa='save-trigger']");
	public static final By saveTrigger1 = By.xpath("//*[@data-qa='save-changes-trigger']");
	public static final By saveSuccess= By.xpath("//*[@data-qa='save-success-txt']");
	public static final By editcancelTrigger = By.xpath("//*[@data-qa='cancel-trigger']");
	public static final By deleteAccountTrigger = By.xpath("//*[@data-qa='delete-user-trigger']");
	
	public static final By customerNameTileTxt = By.xpath("//*[@data-qa='customer-name-tile-txt']");
	public static final By address1TileTxt = By.xpath("//*[@data-qa='address1-tile-txt']");
	public static final By countryTileTxt = By.xpath("//*[@data-qa='country-tile-txt']");
	public static final By phoneTileTxt  = By.xpath("//*[@data-qa='phone-tile-txt']");
	public static final By cityTileTxt  = By.xpath("//*[@data-qa='city-tile-txt']");
	public static final By stateTileTxt  = By.xpath("//*[@data-qa='state-tile-txt']");
	public static final By zipcodeTileTxt  = By.xpath("//*[@data-qa='zipcode-tile-txt']");
	
	public static final By deletePopUp = By.xpath("//*[@class = 'modal-body']");
	public static final By freezePopUp = By.xpath("(//*[@class = 'modal-body'])[2]");
	public static final By okTrigger = By.xpath("//*[@data-qa='delete-ok-trigger']");
	public static final By cancelTrigger = By.xpath("//*[@data-qa='delete-cancel-trigger']");
	public static final By deleteSuccessText = By.xpath("//*[@data-qa='delete-success-txt']");
	
	public static final By firstnameError = By.xpath("//*[@data-qa='first-name-error']");
	public static final By lastnameError = By.xpath("//*[@data-qa='last-name-error']");
	public static final By address1Error = By.xpath("//*[@data-qa='address1-error']");
	public static final By address2Error = By.xpath("//*[@data-qa='address2-error']");
	public static final By cityError = By.xpath("//*[@data-qa='city-error']");
	public static final By stateError = By.xpath("//*[@data-qa='state-error']");
	public static final By countryError = By.xpath("//*[@data-qa='country-error']");
	public static final By zipcodeError = By.xpath("//*[@data-qa='zip-code-error']");
	public static final By phoneNoError = By.xpath("//*[@data-qa='phone-no-error']");
	
	public static final By addNewAddress = By.xpath("//*[@data-qa='add-new-address-trigger']");
	public static final By addNewAddressTitle = By.xpath("//*[@data-qa='add-new-address-title']");
	public static final By addFirstName = By.xpath("//*[@data-qa='add-firstname-input']");
	public static final By addLastName = By.xpath("//*[@data-qa='add-lastname-input']");
	public static final By addAddress1 = By.xpath("//*[@data-qa='add-address1-input']");
	public static final By addAddress2 = By.xpath("//*[@data-qa='add-address2-input']");
	public static final By addCity = By.xpath("//*[@data-qa='add-city-input']");
	public static final By addState = By.xpath("//*[@data-qa='add-state-input']");
	public static final By addCountry = By.xpath("//*[@data-qa='add-country-input']");
	public static final By addZipCode = By.xpath("//*[@data-qa='add-zipcode-input']");
	public static final By addPhoneNo = By.xpath("//*[@data-qa='add-phone-input']");
	public static final By addCancelTrigger = By.xpath("//*[@data-qa='cancel-trigger']");
	public static final By addSaveTrigger = By.xpath("//*[@data-qa='save-trigger']");
	public static final By addSuccessTxt = By.xpath("//*[@data-qa='save-success-txt']");

	
	//for edit address
	public static final By editfirstNameTxt = By.xpath("//*[@data-qa='edit-firstname-input']");
	public static final By editlastNameTxt = By.xpath("//*[@data-qa='edit-lastname-input']");
	public static final By editaddress1Txt = By.xpath("//*[@data-qa='edit-address1-input']");
	public static final By editaddress2Txt = By.xpath("//*[@data-qa='edit-address2-input']");
	public static final By editcityInput = By.xpath("//*[@data-qa='edit-city-input']");
	public static final By editstateInput = By.xpath("//*[@data-qa='edit-state-input']");
	public static final By editcountryInput = By.xpath("//*[@data-qa='edit-country-input']");
	public static final By editzipcodeInput = By.xpath("//*[@data-qa='edit-zipcode-input']");
	public static final By editphoneInput = By.xpath("//*[@data-qa='edit-phone-input']");
	
	public static final By changeStatusFreeze1 = By.xpath("(//*[contains (@class,'cls_profile_mdb-select')]//input)[2]");
	public static final By changeStatusFreeze2 = By.xpath("(//*[contains (@class,'cls_profile_mdb-select')])[3]//li");
	public static final By changeStatusFreeze3 = By.xpath("(//*[contains (@class,'cls_profile_mdb-select')]//input)[2]");
	public static final By changeStatusFreeze4 = By.xpath("(//*[contains (@class,'cls_profile_mdb-select')])[3]//li");
	public static final By verifySuccessTxt1 = By.xpath("//*[contains (@class ,'toast-message')]");
	public static final By searchandclickFrozenCustomers1 = By.xpath("//*[@data-qa='status-search']");
	public static final By searchandclickFrozenCustomers2 = By.xpath("//*[@data-qa='status-search']");
	public static final By searchandclickFrozenCustomers3 = By.xpath("//*[@data-qa='status-search']//following-sibling::div//li");
	public static final By searchandclickFrozenCustomers4 = By.xpath("//*[@data-qa='search-trigger']");
	public static final By searchandclickFrozenCustomers5 = By.xpath("//*[@data-qa='view-profile-trigger']");
	public static final By searchandclickFrozenCustomers6 = By.xpath("(//*[@data-qa='view-profile-trigger'])[1]");
	public static final By selectStatusList1 = By.xpath("//*[@label='Status']//ul//li");
	public static final By selectStatusList2 = By.xpath("//*[@label='Status']//input");
	public static final By selectStatusFilter1 = By.xpath("//*[@label='Status']//ul//li");
	public static final By selectStatusFilter2 = By.xpath("//*[@label='Status']//input");
	
	public static final By clickFreezePopupBtn1 = By.xpath("//*[contains (@class,'cls_skFreezeUser')]");
	public static final By clickDeletePopupBtn1 = By.xpath("//*[contains (@class,'cls_skDeleteUser')]");
	public static final By verifyCustomerTitle1 = By.xpath("//*[@data-qa='cusomers-title']");
	public static final By getProfileName1 = By.xpath("//*[@data-qa='fullname-txt']");
	public static final By searchDeleteCustomer1 = By.xpath("//*[@data-qa='first-name-search']");
	public static final By searchDeleteCustomer2 = By.xpath("//*[@data-qa='first-name-list']");
	public static final By searchDeleteCustomer3 = By.xpath("//*[@data-qa='search-trigger']");
	public static final By noDataFound1 = By.xpath("//*[@data-qa='no-data-error-alert']");
	public static final By verifyFrozenTxt1 = By.xpath("//*[@data-qa='status-value']");
	public static final By searchaFrozenCustomers1 = By.xpath("//*[@data-qa='status-search']");
	public static final By searchaFrozenCustomers2 = By.xpath("//*[@data-qa='status-search']//following-sibling::div//li");
	public static final By searchaFrozenCustomers3 = By.xpath("//*[@data-qa='search-trigger']");
	public static final By selectFreezeStatus1 = By.xpath("//*[@data-qa='view-profile-trigger']");
	public static final By selectFreezeStatus2 = By.xpath("(//*[@data-qa='view-profile-trigger'])[5]");
	public static final By selectFreezeStatus8 = By.xpath("(//*[@data-qa='view-profile-trigger'])[8]");
	public static final By selectFreezeStatus3 = By.xpath("(//*[contains (@class,'cls_profile_mdb-select')]//input)[2]");
	public static final By selectFreezeStatus4 = By.xpath("//*[@data-qa='status-container']//ul/li");
	public static final By selectViewProfButton1 = By.xpath("//*[@data-qa='view-profile-trigger']");
	public static final By selectViewProfButton2 = By.xpath("(//*[@data-qa='view-profile-trigger'])[1]");
	public static final By selectFreezeToActiveStatus1 = By.xpath("(//*[contains (@class,'cls_profile_mdb-select')]//input)[2]");
	public static final By selectFreezeToActiveStatus2 = By.xpath("//*[@data-qa='status-container']//ul/li");
	
	public static final By selectStatusList3 = By.xpath("(//*[@label='Status']//ul//li)");
	public static final By selectStatusList4 = By.xpath("(//*[@label='Status']//ul//li)");
	public static final By changeStatusFreeze5 = By.xpath("((//*[contains (@class,'cls_profile_mdb-select')])[3]//li)");
	public static final By changeStatusFreeze6 = By.xpath("((//*[contains (@class,'cls_profile_mdb-select')])[3]//li)");
	public static final By changeStatusFreeze7 = By.xpath("((//*[contains (@class,'cls_profile_mdb-select')])[3]//li)");
	public static final By searchandclickFrozenCustomers7 = By.xpath("(//*[@data-qa='status-search']//following-sibling::div//li)");
	public static final By selectFreezeStatus5 = By.xpath("//*[@data-qa='status-container']//ul/li");
	
	public static final By address1TileTxt1 = By.xpath("(//*[@data-qa='address1-tile-txt'])");
	public static final By countryTileTxt2 = By.xpath("(//*[@data-qa='country-tile-txt'])");
	public static final By phoneTileTxt1  = By.xpath("(//*[@data-qa='phone-tile-txt'])");
	public static final By cityTileTxt1  = By.xpath("(//*[@data-qa='city-tile-txt'])");
	
	
	
}










