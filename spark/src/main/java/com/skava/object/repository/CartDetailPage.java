package com.skava.object.repository;

import org.openqa.selenium.By;

public class CartDetailPage {
	
	public static final By customerCartTab = By.xpath("//*[@data-qa='cart-tab-trigger']");
	public static final By customerCartProductHeader = By.xpath("//*[@data-qa='cart-product-heading-txt']");
	public static final By customerCartQuantityHeader = By.xpath("//*[@data-qa='cart-quantity-heading-txt']");
	public static final By customerCartItemPriceHeader = By.xpath("//*[@data-qa='cart-itemprice-heading-txt']");
	public static final By customerCartTotalHeader = By.xpath("//*[@data-qa='cart-total-heading-txt']");
	public static final By customerCartProductImage = By.xpath("//*[@data-qa='cart-productimage-img']");
	public static final By customerCartProductName = By.xpath("//*[@data-qa='cart-product-name-txt']");
	public static final By customerCartProductDescription = By.xpath("//*[@data-qa='cart-product-description-txt']");
	public static final By customerCartProductSize = By.xpath("//*[@data-qa='cart-product-size-txt']");
	public static final By customerCartProductColor = By.xpath("//*[@data-qa='cart-product-color-txt']");
	public static final By customerCartProductItemId = By.xpath("//*[@data-qa='cart-product-itemid-txt']");
	public static final By customerCartProductQuantity = By.xpath("//*[@data-qa='cart-product-quantity-txt']");
	public static final By customerCartProductSellingPrice = By.xpath("//*[@data-qa='cart-product-sellprice-txt']");
	public static final By customerCartProductItemPrice = By.xpath("//*[@data-qa='cart-product-itemprice-txt']");
	public static final By customerCartProductTotalPrice = By.xpath("//*[@data-qa='cart-product-totalprice-txt']");
	public static final By customerCartProductYouSavePrice = By.xpath("//*[@data-qa='cart-product-yousave-txt']");
	public static final By customerCartProductSubTotalHeading = By.xpath("//*[@data-qa='cart-product-subtotal-txt']");
	public static final By customerCartProductSubTotalPrice = By.xpath("//*[@data-qa='cart-product-subtotalprice-txt']");
	public static final By customerCartRemoveButton = By.xpath("//*[@data-qa='cart-product-removebutton-btn']");
	public static final By customerCartRemoveSuccess = By.xpath("//*[@data-qa='cart-product-removesuccess-txt']");
	public static final By customerCartRemoveFailure = By.xpath("//*[@data-qa='cart-product-removefailure-txt']");
	public static final By customerCartViewProfileButton = By.xpath("(//*[@data-qa='view-profile-trigger'])[1]");

}
