package com.skava.object.repository;

import org.openqa.selenium.By;

public class subscriptionPage 
{

	public static final By subscriptionHeading = By.xpath("//*[contains(@data-qa,'breadcrumb_null')][text()='Subscriptions']");
	public static final By subscriptionListHeading = By.xpath("//h2[text()='Subscriptions']");
	public static final By subscriptionId = By.xpath("//*[@data-qa='subscription-id-trigger']");
	public static final By subscriptionSkuId = By.xpath("//*[contains(@data-qa,'subscription-event-trigger')][contains(text(),'SKU ID')]");
	public static final By subscriptionSkuName = By.xpath("//*[contains(@data-qa,'subscription-event-trigger')][contains(text(),'SKU Name')]");
	public static final By subscriptionRequestDate = By.xpath("//*[contains(@data-qa,'subscription-event-trigger')][contains(text(),'Subscription request date')]");
	public static final By subscriptionUserName = By.xpath("//*[contains(@data-qa,'subscription-name-trigger')][contains(text(),'User name')]");
	public static final By subscriptionEmail = By.xpath("//*[contains(@data-qa,'subscription-name-trigger')][contains(text(),'E-Mail')]");
	public static final By subscriptionFrequency = By.xpath("//*[contains(@data-qa,'subscription-name-trigger')][contains(text(),'Frequency')]");
	public static final By subscriptionRowTrigger = By.xpath("//*[contains(@data-qa,'view-profile-trigger')]");
	
	public static final By subIDSearchClick = By.xpath("//button[@data-qa='requestId-search']/span[text()='Subscription Id']");
	public static final By subIDSearchInput = By.xpath("//input[@data-qa='requestId-search'][@type='text']");
	public static final By subEmailSearchClick = By.xpath("//button[@data-qa='emailId-search']/span[text()='E-Mail']");
	public static final By subEmailSearchInput = By.xpath("//input[@data-qa='emailId-search'][@type='text']");
	public static final By subSkuIdSearchClick = By.xpath("//button[@data-qa='skuId-search']/span[text()='SKU ID']");
	public static final By subSkuIdSearchInput = By.xpath("//input[@data-qa='skuId-search'][@type='text']");
	public static final By subSkuNameSearchClick = By.xpath("//button[@data-qa='skuname-search']/span[text()='SKU Name']");
	public static final By subSkuNameSearchInput = By.xpath("//input[@data-qa='skuname-search'][@type='text']");
	public static final By subLeftMonthClick = By.xpath("//*[@class='drp-calendar left']//child::select[@class='monthselect']");
	public static final By subRDClick = By.xpath("//*[@data-qa='requestplacementdate-search']/span[contains(text(),'Subscription request')]");
	public static final By subRightMonthClick = By.xpath("//*[@class='drp-calendar right']//child::select[@class='monthselect']");
	public static final By subLeftYearClick = By.xpath("//*[@class='drp-calendar left']//child::select[@class='yearselect']");
	public static final By subRightYearClick = By.xpath("//*[@class='drp-calendar right']//child::select[@class='yearselect']");
	public static final By subLeftDateSelect = By.xpath("//*[@class='drp-calendar left']//child::tbody//td[contains(@data-title,'r')][text()=");
	public static final By subRightDateSelect = By.xpath("//*[@class='drp-calendar right']//child::tbody//td[contains(@data-title,'r')][text()=");
	
	public static final By subCancelBtn6 = By.xpath("(//*[@data-qa='view-profile-trigger']//*[@title='Cancel'][text()='cancel'])[6]");
	public static final By subConfirmBtn = By.xpath("//*[@data-qa='subscription-cancel-ok-trigger']");
	public static final By subCancelConfirm = By.xpath("//*[@data-qa='view-profile-trigger']//*[@data-qa='status-CANCELED']");
	
	
	
}










