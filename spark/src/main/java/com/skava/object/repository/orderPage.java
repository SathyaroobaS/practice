package com.skava.object.repository;

import org.openqa.selenium.By;

import com.skava.reusable.components.GeneralComponents;

public class orderPage 
{
	public static final By ordersTab = By.xpath("//*[@data-qa='orders-tab']");
	public static final By viewBtn = By.xpath("//*[@data-qa='view-profile-trigger']");
	public static final By emailValue = By.xpath("//*[@data-qa='email-value']");
	public static final By orderValue = By.xpath("//*[@data-qa='order-value']");
	public static final By ordersIdTitle = By.xpath("//*[@data-qa='order-id-title']");
	public static final By orderPlacedDateTitle = By.xpath("//*[@data-qa='order-placed-date-title']");
	public static final By orderValueTitle = By.xpath("//*[@data-qa='order-value-title']");
	public static final By ordersIdValue = By.xpath("//*[@data-qa='order-id-value']");
	public static final By orderPlacedDateValue = By.xpath("//*[@data-qa='order-placed-date-value']");
	public static final By viewOrderBtn = By.xpath("//*[@data-qa='view-trigger']");
	public static final By pagination = By.xpath("//*[@data-qa='pagination-trigger']");
	public static final By rowsDropdown = By.xpath("//*[@data-qa='page-length-select']");
	public static final By orderIdFilter = By.xpath("//*[@data-qa='order-id-trigger']");
	public static final By orderIdInput = By.xpath("//*[@data-qa='order-id-input']");
	public static final By searchBtn = By.xpath("//*[@data-qa='search-trigger']");
	public static final By fromDatePicker = By.xpath("//*[@class='drp-calendar left']");
	public static final By toDatePicker = By.xpath("//*[@class='drp-calendar right']");
	public static final By dateRangeFilter = By.xpath("//*[@data-qa='date-input']");
	public static final By paymentsTab = By.xpath("//*[@data-qa='payments-tab']");
	public static final By addressTab = By.xpath("//*[@data-qa='address-tab-trigger']");
	public static final By noDataFound = By.xpath("//*[@data-qa='no-data-error-alert']");
	
	public static final By hoverandclickCustomers1 = By.xpath("(//*[@data-qa='view-profile-trigger'])[2]");
	public static final By hoverandclickCustomers9 = By.xpath("(//*[@data-qa='view-profile-trigger'])[9]");
	public static final By hoverandclickCustomers10 = By.xpath("(//*[@data-qa='view-profile-trigger'])[10]");
	public static final By hoverandclickCustomers7 = By.xpath("(//*[@data-qa='view-profile-trigger'])[7]");
	public static final By hoverandclickCustomers6 = By.xpath("(//*[@data-qa='view-profile-trigger'])[6]");
	public static final By VerifyOrderAdmin = By.xpath("//*[@data-qa='order-id-item']");
	public static final By selectstartdate = By.xpath("//*[@class='drp-calendar left']//*[contains (@class, 'available')]");
	public static final By verifyLatestOrder = By.xpath("(//*[@data-qa='order-placed-date-value'])[1]");
	public static final By verifyLatestOrder1 = By.xpath("(//*[@data-qa='order-placed-date-value'])");
	
	
	public static final By hoverandclickCustomers5 = By.xpath("(//*[@data-qa='view-profile-trigger'])[7]");
	public static final By firstNameField = By.xpath("(//*[@data-qa='firstname-txt'])//input");
	public static final By lastNameField = By.xpath("(//*[@data-qa='lastname-txt'])//input");
	public static final By createdDate = By.xpath("(//*[@data-qa='created-date-txt'])//input");
	public static final By birthDate = By.xpath("(//*[@data-qa='birthdate-txt'])//input");
	public static final By genderField = By.xpath("(//*[@data-qa='gender-txt'])//input");
	public static final By emailField = By.xpath("(//*[@data-qa='email-txt'])//input");
	public static final By phoneNoField = By.xpath("(//*[@data-qa='phone-txt'])//input");
	public static final By statusOptions = By.xpath("(//*[@data-qa='status-txt'])//input");
	public static final By clickGenderDropDown = By.xpath("//*[@data-qa='gender-container']//input");
	public static final By clickGenderDropDown1 = By.xpath("//*[@data-qa='gender-container']//ul//li");
	public static final By clickGenderDropDown2 = By.xpath("//*[@data-qa='gender-container']//*[contains(@class, 'active')]");
	public static final By clickStatusDropDown = By.xpath("(//*[contains (@class,'cls_profile_mdb-select')]//input)[2]");
	public static final By clickStatusDropDown1 = By.xpath("//*[@data-qa='status-container']//ul//li");
	public static final By inlineErrorValidationForEditAddressFields2 = By.xpath("(//*[@data-qa='first-name-error'])");
	public static final By inlineErrorValidationForEditAddressFields21 = By.xpath("(//*[@data-qa='last-name-error'])");
	public static final By inlineErrorValidationForEditAddressFields22 = By.xpath("(//*[@data-qa='address1-error'])");
	public static final By inlineErrorValidationForEditAddressFields23 = By.xpath("(//*[@data-qa='city-error'])");
	public static final By inlineErrorValidationForEditAddressFields24 = By.xpath("(//*[@data-qa='state-error'])");
	public static final By inlineErrorValidationForEditAddressFields25 = By.xpath("(//*[@data-qa='country-error'])");
	public static final By inlineErrorValidationForEditAddressFields26 = By.xpath("(//*[@data-qa='zip-code-error'])");
	public static final By inlineErrorValidationForEditAddressFields27 = By.xpath("(//*[@data-qa='phone-no-error'])");
	
	public static final By hoverandclickCustomers2 = By.xpath("(//*[@data-qa='view-profile-trigger'])[1]");
	public static final By selectenddate = By.xpath("//*[@class='drp-buttons']//button[2]");
	public static final By hoverandVerifyViewBtn = By.xpath("(//*[@data-qa='order-value'])");
	public static final By selectstartdate1 = By.xpath("(//*[@class='drp-calendar left']//*[contains (@class, 'available')])");
	public static final By verifyLatestOrder2 = By.xpath("(//*[@data-qa='order-placed-date-value'])");
	
	public static final By clickGenderDropDown3 = By.xpath("(//*[@data-qa='gender-container']//ul//li)");
}