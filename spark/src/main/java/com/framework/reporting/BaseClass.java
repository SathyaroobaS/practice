package com.framework.reporting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.net.ntp.TimeStamp;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.relevantcodes.extentreports.LogStatus;
import com.skava.framework.action.ActionEngine;
import com.skava.framework.action.ActionEngineFactory;
import com.skava.frameworkutils.Constants;
import com.skava.frameworkutils.ExcelReader;
import com.skava.frameworkutils.loggerUtils;

import net.sf.json.JSONObject;

public class BaseClass 
{	
	public ActionEngine driver;
	public static Properties properties = new Properties();
	public static Properties data = new Properties();
	public static String UserDir=Paths.get(".").toAbsolutePath().normalize().toString();
	String ConfigFilePath=Paths.get(".").toAbsolutePath().normalize().toString()+ "/Resources/Config.ini";
	public static JSONObject json = new JSONObject();
	public static String currentRunReportPath;
	public static String screenshotPath;
	public static String version = "";
	public static String cmdUrl=System.getProperty("url");
	public static String appUrl="";
	public static String msUrl=System.getProperty("msurl");
	public static String orchUrl=System.getProperty("orchurl");
	
	public static int Element_Wait_Time;
	
	public static synchronized ActionEngine initiTest(String tcName) throws IOException 
	{		
		return ActionEngineFactory.getActionEngine(BaseClass.getBrowser(tcName));
	}
	
	public void setDriver(ActionEngine driver) 
	{
		this.driver = driver;
	}
	
	public static int getBrowser(String testCaseName) 
	{
		String strExectuionChannel=(String) json.getJSONArray(testCaseName).get(1);
		String strBrowserName=(String) json.getJSONArray(testCaseName).get(2);
		String strDeviceType=(String) json.getJSONArray(testCaseName).get(4);		
		String strDeviceName=(String) json.getJSONArray(testCaseName).get(5);
		//String testDesc =(String) json.getJSONArray(testCaseName).get(7);
		int return_type = 0;
		
		if(strExectuionChannel.equalsIgnoreCase("LOCAL"))
		{
			switch (strBrowserName) 
			{
				case "Chrome" :
					return_type = Constants.LOCAL_BROWSER_CHROME;
					break;
					
				case "Firefox" :
					return_type = Constants.LOCAL_BROWSER_FIREFOX;
					break;
					
				case "InternetExplorer":
					return_type = Constants.LOCAL_BROWSER_IE;
					break;
					
				case "Safari":
					return_type = Constants.LOCAL_BROWSER_SAFARI;
					break;
					
				case "MicrosoftEdge":
					return_type = Constants.LOCAL_BROWSER_EDGE;
					break;
					
				case "MobileIosChrome":
					return_type = Constants.LOCAL_Emulator_IOS_Chrome;
					break;
					
				default :
					return_type = Constants.LOCAL_BROWSER_CHROME;
			}
		}
		else
		{
			if(strDeviceType.equalsIgnoreCase("Desktop"))
			{
				switch (strBrowserName) 
				{
					case "Chrome" :
						return_type = Constants.SAUCE_DESKTOP_BROWSER_CHROME;
						break;
						
					case "Firefox" :
						return_type = Constants.SAUCE_DESKTOP_BROWSER_FIREFOX;
						break;
						
					case "InternetExplorer":
						return_type = Constants.SAUCE_DESKTOP_BROWSER_IE;
						break;
						
					case "Safari":
						return_type = Constants.SAUCE_DESKTOP_BROWSER_SAFARI;
						break;
						
					case "MicrosoftEdge":
						return_type = Constants.SAUCE_DESKTOP_BROWSER_EDGE;
						break;
						
					default :
						return_type = Constants.SAUCE_DESKTOP_BROWSER_CHROME;	
						break;
				}	
			}
			else if(strDeviceType.equals("Mobile"))
			{
				if(strDeviceName.contains("iPhone"))
				{
					if(strDeviceName.contains("Simulator"))
						return_type = Constants.SAUCE_MOBILE_SIMULATOR_IOS_BROWSER_SAFARI;
					else
						return_type = Constants.SAUCE_MOBILE_IOS_BROWSER_SAFARI;		
				}
				else
				{
					if(strDeviceName.contains("Emulator"))
						return_type = Constants.SAUCE_MOBILE_EMULATOR_ANDROID_BROWSER_CHROME;
					else
						return_type = Constants.SAUCE_MOBILE_ANDROID_BROWSER_CHROME;
				}
			}
			else if(strDeviceType.equalsIgnoreCase("Tablet"))
			{
				if(strDeviceName.contains("iPad"))
				{
					if(strDeviceName.contains("Simulator"))
						return_type = Constants.SAUCE_TABLET_SIMULATOR_IOS_BROWSER_SAFARI;
					else
						return_type = Constants.SAUCE_TABLET_IOS_BROWSER_SAFARI;		
				}
				else
				{
					if(strDeviceName.contains("Emulator"))
						return_type = Constants.SAUCE_TABLET_EMULATOR_ANDROID_BROWSER_CHROME;
					else
						return_type = Constants.SAUCE_TABLET_ANDROID_BROWSER_CHROME;	
				}
			}
		}
		return return_type;
	}
	
	public static synchronized JSONObject initBatchExec() throws IOException 
	{	
		Map<String, List> hashMap = new LinkedHashMap<String, List>();
		hashMap=ExcelReader.getBatchExecInfo(UserDir+properties.getProperty("TestRunnerPath"), properties.getProperty("RunConfigSheetName"));
		json.accumulateAll(hashMap);
		return json;
	}
	
	@BeforeSuite
	public void beforeSuite() throws IOException 
	{
		//Load properties file		
		 File file = new File(ConfigFilePath);		  
		 FileInputStream fileInput = null;
			try 
			{
				fileInput = new FileInputStream(file);
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			}				
			
			try 
			{
				properties.load(fileInput);
				Properties tempProperties = new Properties();
				tempProperties=(Properties) properties.clone();
				
				Set<Object> keys=tempProperties.keySet();
				//System.out.println(keys.toString());
				for(Object k:keys)
				{
					String key=(String)k;
					//System.out.println(key);
					if(key.startsWith(properties.getProperty("RunningEnvironment")))
					{
						String data=properties.getProperty(key);
						properties.remove(key);
						key=key.replace(properties.getProperty("RunningEnvironment")+"_", "");
						properties.put(key, data);
					}
					
					Element_Wait_Time=Integer.parseInt(properties.getProperty("ElementWaitTime"));						
					//Microservce url parameterization fix
			           if(msUrl==null) {
			               msUrl = properties.getProperty("MSUrlDefault");
			           }
			           properties.put("APIDomain", msUrl);
			           
			           //Orchestration url parameterization fix
			           if(orchUrl==null) {
			               orchUrl = properties.getProperty("OrchUrlDefault");
			           }
			           properties.put("Domain", orchUrl);
					
					
					/*** Appication url ***/
			          String configURL = properties.getProperty("ApplicationUrl");
			          
			          if(cmdUrl != null)
			          {
			            configURL = configURL.replace("<<domain>>",cmdUrl);
			            properties.put("ApplicationUrl",configURL);
			          }
			          else
			          {
			            configURL = configURL.replace("<<domain>>",properties.getProperty("Domain"));
			            properties.put("ApplicationUrl",configURL);
			          }
			          if(System.getProperty("suiteXmlFile")!=null) 
			          {
			          if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority1.xml"))
			          {
			        	  configURL=configURL.replace("<<storeId>>", properties.getProperty("storeId1"));
			          }
			          else if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority2.xml"))
			          {
			        	  configURL=configURL.replace("<<storeId>>", properties.getProperty("storeId1"));
			          }
			          else if(System.getProperty("suiteXmlFile").equalsIgnoreCase("priority3.xml"))
			          {
			        	  configURL=configURL.replace("<<storeId>>", properties.getProperty("storeId1")); 
			          }
			          else if(System.getProperty("suiteXmlFile").equalsIgnoreCase("deploymenttest.xml"))
			          {
			        	  configURL=configURL.replace("<<storeId>>", properties.getProperty("storeId1")); 
			          }
			          }
			          else {
			        	  configURL=configURL.replace("<<storeId>>", properties.getProperty("storeId1"));
			          }
			          
			          configURL=configURL.replace("<<businessId>>", properties.getProperty("businessId"));
			          
			          properties.put("ApplicationUrl",configURL);
			          
			          
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			//Read test runner file for test run configurations
			initBatchExec();
			
			//Setup reports path for current run 
			TimeStamp time=new TimeStamp(System.currentTimeMillis());	
			currentRunReportPath=UserDir+ properties.getProperty("ReportPath")+time;
			File files = new File(currentRunReportPath+"/Screenshots");
			files.mkdirs();
			ExtentManager.filePath=currentRunReportPath+"/Summary.html";
			screenshotPath=currentRunReportPath+"/Screenshots";	
			
			ExtentManager.sysInfo.put("Domain",properties.getProperty("Domain"));
			ExtentManager.sysInfo.put("Running environment",properties.getProperty("RunningEnvironment"));
			ExtentManager.sysInfo.put("User Name", "Aimia Automation Team");
			
			loggerUtils.setLogFile();
	}
	
	@BeforeMethod
    public void beforeMethod(Method method) 
	{
		ExtentTestManager.startTest(method.getName());
    }
    
    @AfterMethod
    protected void afterMethod(ITestResult result) 
    {
        if (result.getStatus() == ITestResult.FAILURE) 
        {
            ExtentTestManager.getTest().log(LogStatus.FAIL, result.getThrowable());
        } 
        else if (result.getStatus() == ITestResult.SKIP) 
        {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
        } 
        else 
        {
            ExtentTestManager.getTest().log(LogStatus.PASS, "Test Completed");
        }
        
        ExtentManager.getReporter().endTest(ExtentTestManager.getTest());        
        ExtentManager.getReporter().flush();
    }
    
    @AfterSuite
    public void afterSuite()
    {
    	driver.cleanUp();
        ExtentManager.getReporter().endTest(ExtentTestManager.getTest());        
        ExtentManager.getReporter().flush();
    }
    
    protected String getStackTrace(Throwable t) 
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        return sw.toString();
    }
}