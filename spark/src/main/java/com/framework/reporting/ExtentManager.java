package com.framework.reporting;

import java.util.HashMap;
import java.util.Map;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager 
{
	static ExtentReports extent;
    static String filePath;
    public static String screenshotPath;
    static String projectName = "Mc Admin";
    
    public static Map<String, String> sysInfo = new HashMap<String, String>();
    public synchronized static ExtentReports getReporter() 
    {
    	if (extent == null) 
    	{
            extent = new ExtentReports(filePath, true);
            if(sysInfo.size()>0) 
            extent.addSystemInfo(sysInfo);
            extent.assignProject(projectName);
    	}
        return extent;
    }
}