package com.skava.testcases;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.AfterClass; 
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.NotificationLandingPageComponents;
import com.skava.reusable.components.subscriptionPageComponents;
import com.skava.reusable.components.userProfilePageComponents;


public class SECOM_40471 extends subscriptionPageComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void SECOM40471() throws Exception 
	{
		launchUrl();
		subscriptionAdminVerification();
		
	} 
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}
