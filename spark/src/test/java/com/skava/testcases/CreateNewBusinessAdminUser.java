package com.skava.testcases;

import java.io.IOException;


import org.testng.annotations.AfterClass; 
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.userProfilePageComponents;


public class CreateNewBusinessAdminUser extends userProfilePageComponents
{
	@BeforeClass
	public void initTest() throws IOException
	{		
		driver = initiTest(this.getClass().getSimpleName());		
		driver.maximizeBrowser();
	}
	
	@Test
	public void CreateNewBAUser() throws Exception 
	{
		generatesessionId();
		inviteBusinessAdmin();
		generateActivationParam();
		registerUser();
		 
	}
	
	@AfterClass
	public synchronized void tearDown()
	{		
		if (driver != null) 
		{
			driver.quit();
		}
	}
}